package views;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidKeyMetastate;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.internal.TouchAction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Generic app functions to help with overall app control
 */
public class AppFunctions {
    private AppiumDriver driver;

    public AppFunctions(AppiumDriver driver) {
        this.driver = driver;
    }

    /**
     * Tells the driver to implicitly wait for the set amount of time. Implicit wait stops at the end of
     * the timer, or when the next driver action is successfully completed (whichever comes first).
     * @param seconds Seconds to wait
     */
    public void implicitWaitSeconds(int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    /**
     * Stops the app, clears its data, starts the app
     */
    public void resetApp() {
        driver.resetApp();
    }

    /**
     * Clears the app's data and launches it. Seems to be almost the same thing as resetApp()
     */
    public void launchApp() {
        driver.launchApp();
    }

    /**
     * Uninstalls the MyHyundai app from the phone
     */
    public void uninstallMyHyundaiAndroid() {
        driver.removeApp("com.stationdm.bluelink");
    }

    /**
     * Uninstalls the Genesis app from the phone
     */
    public void uninstallGenesisAndroid() {
        driver.removeApp("com.stationdm.genesis");
    }

    /**
     * Installs the chosen app to the phone
     * @param appFileName full file name of the app to install
     */
    public void installApp(String appFileName) {
        Path workingDirectory = Paths.get(".").toAbsolutePath();
        driver.installApp(workingDirectory.toString() + "\\apps\\" + appFileName);
    }

    /**
     * Runs the app in the background for the selected amount of time in seconds
     * @param seconds Amount of time to run in background
     */
    public void backgroundApp(long seconds) {
        driver.runAppInBackground(Duration.ofSeconds(seconds));
    }

    /**
     * Takes a screenshot and stores it locally on the device
     */
    public void takeScreenShot() {
        // may need an ios and android implementation
        // should get calling method name (specifically for tests)

        File file=driver.getScreenshotAs(OutputType.FILE);
       try {
           FileUtils.copyFile(file, new File("myScreenshot.png"));
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }
        // log that x screenshot was taken
    }


    /**
     * Kills the appium/selenium driver.
     * This is run after all tests are run to immediately end all testing on the whole system.
     */
    public void stopDriver() {
        driver.quit();
    }

    /**
     * (Android only) presses the standard android 'back' button
     */
    public void tapAndroidBackButton() {
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.BACK);
    }

    /**
     * (Android only) presses the standard android 'backspace' button
     */
    public void tapAndroidKeypadBackSpace() {
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.BACKSPACE);
    }

    /**
     * (Android only) presses the standard android 'back' button after a Thread.sleep(sleepTime)
     * @param sleepTime How long to thread.sleep for, in milliseconds.
     */
    public void tapAndroidBackButtonAfterSleep(long sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException ie) {
            //
        }
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.BACK);
    }

    /**
     * (Android only) presses the standard android 'back' button after a Thread.sleep(sleepTime)

     */
    public void tapAndroidKeyboardSearchButton() {
try{
    Thread.sleep(2000);
}
catch(Exception e){
    System.out.println("exception");
        }

        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_SEARCH);
    }

    /**
     * (Android only) presses the standard android 'ENETR' button after a Thread.sleep(sleepTime)

     */
    public void tapAndroidKeyboardEnterButton() {
        try{
            Thread.sleep(2000);
        }
        catch(Exception e){
            System.out.println("exception");
        }

        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
    }


    /**
     * Attempts to wait for (numberOfWaits * millisecondsToWait) milliseconds. This is done by doing
     * numberOfWait iterations, where each iteration runs driver.getPageSource() and then does a thread sleep for
     * millisecondsToWait milliseconds. Only use if absolutely necessary.
     * @param numberOfWaits Number of times to iterate through calling driver.getPageSource() and thread.sleeping.
     * @param millisecondsToWait How long to thread.sleep in each iteration
     */
    public void pauseTestExecution(int numberOfWaits, int millisecondsToWait) {
        for (int i = 0; i < numberOfWaits; i++) {
            try {
                Thread.sleep(millisecondsToWait);
            } catch (InterruptedException ie) {
                // do nothing
            }
            if (i < numberOfWaits - 1) {
                driver.getPageSource();
            }
        }
    }
}