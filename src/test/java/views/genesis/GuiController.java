package views.genesis;

import com.sun.org.apache.xpath.internal.objects.XString;
import config.Account;
import config.Vehicle;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import library.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import views.AppController;
import views.genesis.popups.*;
import views.genesis.screens.*;

import java.time.Duration;
import java.util.List;

/**
 * Holds all screens and popups for Genesis.
 */
public class GuiController {
    private AppiumDriver driver;
    private AppController appController;

    // Standard screens
    public AboutGenesisScreen aboutGenesisScreen;
    public AccessoriesScreen accessoriesScreen;
    public ActivityTutorialScreen activityTutorialScreen; 
    public AlertSettingsScreen alertSettingsScreen;
    public CarCareSchedulingScreen carCareSchedulingScreen;
    public ChangePinEnterNewPinScreen changePinEnterNewPinScreen;
    public ChangePinEnterPasswordScreen changePinEnterPasswordScreen;
    public ChangePinEnterSecurityAnswerScreen changePinEnterSecurityAnswerScreen;
    public CurfewAlertScreen curfewAlertScreen;
    public DemoChooseCarScreen demoChooseCarScreen;

    public EmailAppSupportScreen emailAppSupportScreen;
    public EnterPinForCarFinderScreen enterPinForCarFinderScreen;
    public EnterPinScreen enterPinScreen;
    public EventScreen eventScreen;
    public EditSecondaryDriverInformationScreen_Gen2Point5 editSecondaryDriverInformationScreen_gen2Point5;
    public EditVehicleNickNameScreen_Gen2Point5 editVehicleNickNameScreen_gen2Point5;
    public EditPersonalInformationScreen_Gen2Point5 editPersonalInformationScreen_gen2Point5;
    public EditContactInformationScreen_Gen2Point5 editContactInformationScreen_gen2Point5;
    public EditEmergencyInformationScreen_Gen2Point5 editEmergencyInformationScreen_gen2Point5;
    public GenesisFAQScreen genesisFAQScreen;
    public GenesisMotorsUSAScreen genesisMotorsUSAScreen;
    public GenesisOwnerBenefitsScreen genesisOwnerBenefitsScreen;
    public GenesisResourcesScreen genesisResourcesScreen;
    public GeoFenceAlertScreen geoFenceAlertScreen;
    public GmailComposeScreen gmailComposeScreen;
    public GooglePlayStoreScreen googlePlayStoreScreen;
    public HomeScreen homeScreen;
    public IntroductionScreen introductionScreen;
    public views.genesis.screens.LoginScreen loginScreen;
    public MapScreen mapScreen;
    public MenuScreen menuScreen;
    public MonthlyVehicleReportScreen monthlyVehicleReportScreen;
    public OwnersManualScreen ownersManualScreen;
    public ParkingMeterScreen parkingMeterScreen;
    public PreferredContactInformationScreen preferredContactInformationScreen;
    public ProfileScreen profileScreen;
    public ProfileScreen_Gen2Point5 profileScreen_gen2Point5;
    public RegisteredDevicesScreen registeredDevicesScreen;
    public RegistrationScreen registrationScreen;
    public RemoteFeaturesScreen remoteFeaturesScreen;
    public RemoteFeaturesScreenGen2Point5 remoteFeaturesScreenGen2Point5;
    public RemoteStartSettingsScreen remoteStartSettingsScreen;
    public RemoteStartSettingsScreenGen2Point5 remoteStartSettingsScreenGen2Point5;
    public RemotePresetsScreen remotePresetsScreen;
    public RequestHistoryScreen requestHistoryScreen;
    public ScheduleServiceScreen scheduleServiceScreen;
    public SelectVehicleScreen selectVehicleScreen;
    public ServiceHistoryToolScreen serviceHistoryToolScreen;
    public SettingsScreen settingsScreen;
    public SettingsScreenGen2Point5 settingsScreenGen2Point5;
    public SpeedAlertScreen speedAlertScreen;
    public SupportScreen supportScreen;
    public SecondaryDriverInvitationScreen_Gen2Point5 secondaryDriverInvitationScreen_gen2Point5;
    public SecondaryDriverScreen_Gen2Point5 secondaryDriverScreen_gen2Point5;
    public TermsAndConditionsScreen termsAndConditionsScreen;
    public TutorialAddEventScreen tutorialAddEventScreen;
    public ValetAlertScreen valetAlertScreen;
    public views.myhyundai.screens.LoginScreen myHyundaiLoginScreen;

    // Standard popups
    public AndroidEmailPopup androidEmailPopup;
    public AlertSavedPopup alertSavedPopup;
    public CallGenesisConnectedServicesPopup callGenesisConnectedServicesPopup;
    public CallRoadsideAssistancePopup callRoadsideAssistancePopup;
    public CantConnectPopup cantConnectPopup;
    public ChangePinSuccessfulPopup changePinSuccessfulPopup;
    public DeleteConfirmationPopup deleteConfirmationPopup;

    public FeatureNotAvailableInDemoPopup featureNotAvailableInDemoPopup;
    public GenesisServiceValetPopup genesisServiceValetPopup;
    public GooglePlayTosPopup googlePlayTosPopup;
    public IncorrectPinCarFinderPopup incorrectPinCarFinderPopup;
    public IncorrectPinPopup incorrectPinPopup;
    public InstallMyHyundaiPopup installMyHyundaiPopup;
    public InvalidPasswordPopup invalidPasswordPopup;
    public LogoutPopup logoutPopup;
    public MyHyundaiRedirectPopup myHyundaiRedirectPopup;
    public OutOfRangePopup outOfRangePopup;
    public ParkingMeterAlertTimeGreaterThanMeterTimePopup parkingMeterAlertTimeGreaterThanMeterTimePopup;
    public ParkingMeterTimerAboutToExpirePopup parkingMeterTimerAboutToExpirePopup;
    public ParkingMeterTimerExpiredPopup parkingMeterTimerExpiredPopup;
    public ParkingMeterTimerResetPopup parkingMeterTimerResetPopup;
    public PinLockedPopup pinLockedPopup;
    public PleaseEnterUsernameForAccountPopup pleaseEnterUsernameForAccountPopup;
    public RegistrationEmailAlreadyRegisteredPopup registrationEmailAlreadyRegisteredPopup;
    public RegistrationPleaseEnterYourEmailAddressPopup registrationPleaseEnterYourEmailAddressPopup;
    public RegistrationResetPasswordPopup registrationResetPasswordPopup;
    public RemoteCommandResultPopup remoteCommandResultPopup;
    public TermsAndConditionsPopup termsAndConditionsPopup;
    public views.myhyundai.popups.PermissionsPopup permissionsPopup;
    public RoadsideAssistanceScreen roadsideAssistanceScreen;

    /**
     * GuiController constructor
     * @param driver Appium driver
     */
    public GuiController(AppiumDriver driver, AppController appController) {
        this.driver = driver;
        this.appController = appController;

        // Standard screens
        aboutGenesisScreen = new AboutGenesisScreen(driver);
        accessoriesScreen = new AccessoriesScreen(driver);
        activityTutorialScreen = new ActivityTutorialScreen(driver);
        alertSettingsScreen = new AlertSettingsScreen(driver);
        carCareSchedulingScreen = new CarCareSchedulingScreen(driver);
        changePinEnterNewPinScreen = new ChangePinEnterNewPinScreen(driver);
        changePinEnterPasswordScreen = new ChangePinEnterPasswordScreen(driver);
        changePinEnterSecurityAnswerScreen = new ChangePinEnterSecurityAnswerScreen(driver);
        curfewAlertScreen = new CurfewAlertScreen(driver);
        demoChooseCarScreen = new DemoChooseCarScreen(driver);
        emailAppSupportScreen = new EmailAppSupportScreen(driver);
        enterPinForCarFinderScreen = new EnterPinForCarFinderScreen(driver);
        enterPinScreen = new EnterPinScreen(driver);
        eventScreen = new EventScreen(driver);
        editSecondaryDriverInformationScreen_gen2Point5 = new EditSecondaryDriverInformationScreen_Gen2Point5(driver);
        editVehicleNickNameScreen_gen2Point5 = new EditVehicleNickNameScreen_Gen2Point5(driver);
        editPersonalInformationScreen_gen2Point5 = new EditPersonalInformationScreen_Gen2Point5(driver);
        editContactInformationScreen_gen2Point5 = new EditContactInformationScreen_Gen2Point5(driver);
        editEmergencyInformationScreen_gen2Point5 = new EditEmergencyInformationScreen_Gen2Point5(driver);
        genesisFAQScreen = new GenesisFAQScreen(driver);
        genesisMotorsUSAScreen = new GenesisMotorsUSAScreen(driver);
        genesisOwnerBenefitsScreen = new GenesisOwnerBenefitsScreen(driver);
        genesisResourcesScreen = new GenesisResourcesScreen(driver);
        geoFenceAlertScreen = new GeoFenceAlertScreen(driver);
        gmailComposeScreen = new GmailComposeScreen(driver);
        googlePlayStoreScreen = new GooglePlayStoreScreen(driver);
        homeScreen = new HomeScreen(driver);
        introductionScreen = new IntroductionScreen(driver);
        loginScreen = new views.genesis.screens.LoginScreen(driver);
        mapScreen = new MapScreen(driver);
        menuScreen = new MenuScreen(driver);
        monthlyVehicleReportScreen = new MonthlyVehicleReportScreen(driver);
        ownersManualScreen = new OwnersManualScreen(driver);
        parkingMeterScreen = new ParkingMeterScreen(driver);
        preferredContactInformationScreen = new PreferredContactInformationScreen(driver);
        profileScreen = new ProfileScreen(driver);
        profileScreen_gen2Point5 = new ProfileScreen_Gen2Point5(driver);
        registeredDevicesScreen = new RegisteredDevicesScreen(driver);
        registrationScreen = new RegistrationScreen(driver);
        remoteFeaturesScreen = new RemoteFeaturesScreen(driver);
        remoteFeaturesScreenGen2Point5=new RemoteFeaturesScreenGen2Point5(driver);
        remoteStartSettingsScreen = new RemoteStartSettingsScreen(driver);
        remoteStartSettingsScreenGen2Point5=new RemoteStartSettingsScreenGen2Point5(driver);
        requestHistoryScreen = new RequestHistoryScreen(driver);
        remotePresetsScreen = new RemotePresetsScreen(driver);
        scheduleServiceScreen = new ScheduleServiceScreen(driver);
        selectVehicleScreen = new SelectVehicleScreen(driver);
        serviceHistoryToolScreen = new ServiceHistoryToolScreen(driver);
        settingsScreen = new SettingsScreen(driver);
        settingsScreenGen2Point5=new SettingsScreenGen2Point5(driver);
        speedAlertScreen = new SpeedAlertScreen(driver);
        supportScreen = new SupportScreen(driver);
        secondaryDriverInvitationScreen_gen2Point5= new SecondaryDriverInvitationScreen_Gen2Point5(driver);
        secondaryDriverScreen_gen2Point5 = new SecondaryDriverScreen_Gen2Point5(driver);
        termsAndConditionsScreen = new TermsAndConditionsScreen(driver);
        tutorialAddEventScreen = new TutorialAddEventScreen(driver);
        valetAlertScreen = new ValetAlertScreen(driver);
        myHyundaiLoginScreen = new views.myhyundai.screens.LoginScreen(driver);


        // Standard popups
        alertSavedPopup = new AlertSavedPopup(driver);
        androidEmailPopup = new AndroidEmailPopup(driver);
        callGenesisConnectedServicesPopup = new CallGenesisConnectedServicesPopup(driver);
        callRoadsideAssistancePopup = new CallRoadsideAssistancePopup(driver);
        cantConnectPopup = new CantConnectPopup(driver);
        changePinSuccessfulPopup = new ChangePinSuccessfulPopup(driver);
        deleteConfirmationPopup = new DeleteConfirmationPopup(driver);

        featureNotAvailableInDemoPopup = new FeatureNotAvailableInDemoPopup(driver);
        genesisServiceValetPopup = new GenesisServiceValetPopup(driver);
        googlePlayTosPopup = new GooglePlayTosPopup(driver);
        incorrectPinCarFinderPopup = new IncorrectPinCarFinderPopup(driver);
        incorrectPinPopup = new IncorrectPinPopup(driver);
        installMyHyundaiPopup = new InstallMyHyundaiPopup(driver);
        invalidPasswordPopup = new InvalidPasswordPopup(driver);
        logoutPopup = new LogoutPopup(driver);
        outOfRangePopup = new OutOfRangePopup(driver);
        myHyundaiRedirectPopup = new MyHyundaiRedirectPopup(driver);
        parkingMeterAlertTimeGreaterThanMeterTimePopup = new ParkingMeterAlertTimeGreaterThanMeterTimePopup(driver);
        parkingMeterTimerAboutToExpirePopup = new ParkingMeterTimerAboutToExpirePopup(driver);
        parkingMeterTimerExpiredPopup = new ParkingMeterTimerExpiredPopup(driver);
        parkingMeterTimerResetPopup = new ParkingMeterTimerResetPopup(driver);
        pinLockedPopup = new PinLockedPopup(driver);
        pleaseEnterUsernameForAccountPopup = new PleaseEnterUsernameForAccountPopup(driver);
        registrationEmailAlreadyRegisteredPopup = new RegistrationEmailAlreadyRegisteredPopup(driver);
        registrationPleaseEnterYourEmailAddressPopup = new RegistrationPleaseEnterYourEmailAddressPopup(driver);
        registrationResetPasswordPopup = new RegistrationResetPasswordPopup(driver);
        remoteCommandResultPopup = new RemoteCommandResultPopup(driver);
        termsAndConditionsPopup = new TermsAndConditionsPopup(driver);
        permissionsPopup = new views.myhyundai.popups.PermissionsPopup(driver);
        roadsideAssistanceScreen= new RoadsideAssistanceScreen(driver);
    }

    /**
     * @param powerType the power type to get the appropriate home screen for
     * @return Home screen based on vehicle's power type (gas, hybrid, or electric)
     */
    public views.genesis.screens.HomeScreen getHomeScreenByPowerType(Vehicle.PowerType powerType) {
        switch (powerType) {
            case GAS:
                return homeScreen;

            default:
                // shouldn't really hit this case, but assuming gas if we do.
                return homeScreen;
        }
    }

    /**
     * Logs in with chosen account, selects chosen vehicle, accepts terms and conditions, then ends at home page.
     * Uses standard wait time.
     * @param account Account to log in with
     * @param vin VIN of vehicle to select at select vehicle screen
     */
    public void loginAndGoToHomePageWithSelectVehicle(Account account, String vin) {
        loginAndGoToHomePageWithSelectVehicle(account, vin, 5);
    }

    /**
     * Logs in with chosen account, selects chosen vehicle, accepts terms and conditions, then ends at home page
     * @param account Account to log in with
     * @param vin VIN of vehicle to select at select vehicle screen
     * @param vehicleSelectionWaitTime Time (in seconds) to implicitly wait when selecting a vehicle
     */
    public void loginAndGoToHomePageWithSelectVehicle(Account account, String vin, long vehicleSelectionWaitTime) {

        loginScreen.loginWithAccount(account);

        appController.appFunctions.pauseTestExecution(1, 2000);

        if(termsAndConditionsPopup.acceptButton.elementExists(30))
        {
            System.out.println(termsAndConditionsPopup.acceptButton.elementExists());
            termsAndConditionsPopup.acceptButton.tap(20);
            termsAndConditionsPopup.exitButton.tap(10);
            termsAndConditionsPopup.allowbutton.tap();//allow calendar
            //termsAndConditionsPopup.denybutton.tap();//deny location
        }

       else{

           selectVehicleScreen.tapVehicleButtonByVinIfThisScreenIsPresent(vin, vehicleSelectionWaitTime);
            termsAndConditionsPopup.acceptButton.tap(20);
        }

        termsAndConditionsPopup.exitButton.tap();
       termsAndConditionsPopup.allowbutton.tap();
       //termsAndConditionsPopup.denybutton.tap();


        appController.appFunctions.pauseTestExecution(1, 2000);
        genesisServiceValetPopup.tapGotItIfThisPopupPresent(10);
        eventScreen.tapGotItIfThisPopupPresent(10);
        //selectVehicleScreen.tapVehicleButtonByVinIfThisScreenIsPresent(vin, vehicleSelectionWaitTime);
        System.out.println("NOTE: Above 'element not found' message is not an error if there is only 1 car on account.");
        //appController.appFunctions.pauseTestExecution(1, 2000);

        //dissmiss covid popup
       /* termsAndConditionsPopup.covidDismissButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);*/
    }

    /**
     * Return Text of Element located by Xpath
     * @param xpath The xpath of the Element

     */
    public String getTextUsingXpath(String xpath)
    {

        return driver.findElementByXPath(xpath).getText();
    }

    public List<MobileElement> getTextsUsingXpath(String xpath)
    {
        return driver.findElementsByXPath(xpath);

    }


    /**
     * Clear Text of Element located by Xpath
     * @param xpath The xpath of the Element
     */
    public void clearTextField(String xpath)
    {
         driver.findElementByXPath(xpath).clear();
    }

    /**
     * Swipes down a scroll wheel for the chosen number of scroll wheel entries
     * @param scrollWheelXpath The xpath of the scroll wheel element
     * @param entriesToScroll Number of entries to scroll on the scroll wheel
     */
    public void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll) {
        Rectangle dimensions = driver.findElementByXPath(scrollWheelXpath).getRect();

        int xCoordinate = dimensions.x + (int)(dimensions.width * 0.5);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.6);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.30);

        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
    }

    /**
     * Swipes up a scroll wheel for the chosen number of scroll wheel entries
     * @param scrollWheelXpath The xpath of the scroll wheel element
     * @param entriesToScroll Number of entries to scroll on the scroll wheel
     */
    public void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll) {
        Rectangle dimensions = driver.findElementByXPath(scrollWheelXpath).getRect();

        int xCoordinate = dimensions.x + (int)(dimensions.width * 0.5);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.40);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.70);

        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
    }

    public void waitForElementToLoadWithProvidedTime(MobileElement element, int limit) throws InterruptedException {
        boolean flag = false;
        for (int i = 0; i <= limit; i++) {
            if (!flag) {
                try {
                    wait(2000);
                    String isVisible = element.getAttribute("visible");
                    flag = isVisible.equalsIgnoreCase("true") ? true : false;
                    flag = true;
                } catch (Exception e) {
                    wait(1000);
                    flag = false;
                }
            } else {
                break;
            }
        }
    }
}
