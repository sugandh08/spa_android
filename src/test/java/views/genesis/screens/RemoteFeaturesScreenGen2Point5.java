package views.genesis.screens;
 
import java.time.Duration;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import library.Element;
import library.TouchActions;
import views.Screen;

import io.appium.java_client.MobileElement;

/**
 * This is the Remote Features screen that is accessible through the Home screen.
 */
public class RemoteFeaturesScreenGen2Point5 extends Screen{
	
	private AppiumDriver<MobileElement> driver;

    public Element menuButton;
    public Element titleTextView;
    public Element requestHistoryButton;
    public Element lastUpdateDateTextView;
    public Element refreshButton;
    public Element refreshOverlayButton;

    //Vehicle status
    public Element engineStatus;
    public Element doorsStatus;
    public Element Locked;
    public Element doorwarning;
    public Element doorwarningtxt;
    public Element climateStatus;
    public Element frontDefrostStatus;
    public Element Heating;
    public Element seats;

    public Element Engine;
    public Element fluidlevels;
    public Element smartkey;
    public Element oilStatus;
    public Element brakeStatus;
    public Element washerStatus; 
    public Element smartKeyBatteryStatus;

    public Element Lightexpandbutton;
    public Element Lightsexpandtext;
    public Element fluidlevelexpandbutton;
    public Element smartkeyexpandbutton;
    public Element lockButton;
    public Element doorLocksButton;
    public Element unlockButton;
    public Element hornLightOptionsButton;
    public Element requestStatusTextView;
    public Element start_stopButton;
    public Element startbutton;
    public Element addevent;
    public Element HomescreenPOI;
    public Element weathericon;
    public Element location;
    public Element weatherTemp;
    public Element weatherinfo;
    public Element remotefeaturescreen;
    public Element messageCentre;
    public Element contactus;
    public Element ContactusPage;
    public Element addeventPromptmsg;
    public Element CancelButton;
    public Element AddeventButton;
    public Element gotitbutton;
    public Element Addeventtitle;
    public Element POIsearchlisttitle;
    public Element AllowDenyPopup;
    public Element Quickstarttutorial;
    public Element Gotosettings;
    public Element Remoteactioninprogress;
    public Element Remoteactioninprogresscontent;
    public Element DismissButton;
    public Element Okbutton;
    public Element Remotestartevent;
    public Element eventname;
    public Element nextbuttonevent;
    public Element Eventnext;
    public Element Callroadsideassistance;
    public Element RemoteHistoryicon;

    //Fly Menu dashboard
    public Element Maps;
    public Element MVR;
    public Element Schedule;
    public Element Remote;

    //Fly Menu under Maps
    public Element POI;
    public Element ChargeFuelstations;
    public Element DealerLocator;
    public Element CarFinder;
    public Element Gpsicon;
    public Element AddPOI;

    //Fly Menu under Remote
    public Element start;
    public Element stop;
    public Element lock;
    public Element unlock;

    public Element Remotestatus;
    public Element Pending;
    public Element Success;
    public Element Homescreenbutton;
    public Element HomePOISearchIcon;
    public Element ClimateIcon;
    public Element DoorIcon;




    // Can only access after tapping the hornLightOptionsButton;
    public Element flashLightsButton;
    public Element hornAndLightsButton;
    public Element forAddEventBtn;
    public Element addEventCurrentDateBtn;
    public Element eventSaveBtn;
    public Element addEventTitel;
    public Element eventDateAndTime;
    public Element valetMode;


    public RemoteFeaturesScreenGen2Point5(AppiumDriver driver){
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        Homescreenbutton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
        requestHistoryButton = new Element(driver,"title_right_icon");
        lastUpdateDateTextView = new Element(driver,"tvRefreshTime");
        refreshButton = new Element(driver,"com.stationdm.genesis:id/iv_holo_update");
        refreshOverlayButton = new Element(driver,"iv_holo_update");
        engineStatus = new Element(driver,"vehicle_engine_txt");
        doorsStatus = new Element(driver,"vehicle_door_txt","");
        Locked = new Element(driver,"","LOCKED");
        doorwarning = new Element(driver,"ivWarning");
        doorwarningtxt = new Element(driver,"tvContent");
        climateStatus = new Element(driver,"remote_vehicle_status_car_climate_temperature");
        frontDefrostStatus = new Element(driver,"remote_vehicle_status_car_climate_defrost");
        POIsearchlisttitle = new Element(driver,"tv_placename");
        AllowDenyPopup = new Element(driver,"permission_message");
        Remoteactioninprogress = new Element(driver,"","REMOTE ACTION IN PROGRESS");
        DismissButton = new Element(driver,"tvDismiss");
        Remoteactioninprogresscontent = new Element(driver,"tvContent");
        Remotestatus = new Element(driver,"tvRemoteStatus");
        Pending = new Element(driver,"","Pending");
        Success = new Element(driver,"","Success");
        Okbutton = new Element(driver,"button1");
        Heating = new Element(driver,"remote_vehicle_status_car_heat_status");
        seats = new Element(driver,"vehicle_seat_txt");
        Remotestartevent = new Element(driver,"iae_remotestart_btn");
        eventname = new Element(driver,"tv_event_content3","LAKERS VS. JAZZ");
        nextbuttonevent = new Element(driver,"iae_next_btn");
        Eventnext = new Element(driver,"iaetut_gotit_btn");
        Callroadsideassistance = new Element(driver,"alertTitle");
        RemoteHistoryicon = new Element(driver,"title_right_icon");

        //fly menu Remote
        start = new Element(driver,"","Start");
        stop = new Element(driver,"","Stop");
        lock = new Element(driver,"","Lock");
        unlock = new Element(driver,"","Unlock");

        start_stopButton=new Element(driver,"home_remote_startstop_bar");

        doorLocksButton = new Element(driver,"home_remote_lockunlock_bar");

        startbutton = new Element(driver,"setting_icon");

        lockButton=new Element(driver,"","Lock");
        unlockButton = new Element(driver,"","Unlock");
        hornLightOptionsButton = new Element(driver,"","Horn and Lights");
        flashLightsButton = new Element(driver,"","Flash Lights");
        hornAndLightsButton = new Element(driver,"","Horn and Lights");

        lockButton=new Element(driver,"Lock");
        unlockButton = new Element(driver,"ivUnlockIcon");
        hornLightOptionsButton = new Element(driver,"" , "Horn and Lights");
        flashLightsButton = new Element(driver,"ll_light_switch");
        hornAndLightsButton = new Element(driver,"ll_horn_switch");

        requestStatusTextView = new Element(driver,"ripple_sending_txt");
        messageCentre = new Element(driver,"main_title_message_icon");
        contactus = new Element(driver,"msg_center_contact_btn");
        ContactusPage = new Element(driver,"","Consumer Assistance Center");
        addeventPromptmsg = new Element(driver,"","Add an event with location and start time in calendar app to get Genesis alert.");

        Lightexpandbutton = new Element(driver,"ivLightExpand");
        Lightsexpandtext = new Element(driver,"remote_vehicle_status_light_status");
        fluidlevelexpandbutton = new Element(driver,"ivFluidExpand");

        smartkeyexpandbutton = new Element(driver,"ivSmartKeyExpand");
        Engine = new Element(driver,"vehicle_engine_icon");
        fluidlevels = new Element(driver,"remote_vehicle_status_fluid_level_title");
        smartkey = new Element(driver,"remote_vehicle_status_smart_key_title");
        oilStatus=new Element(driver,"remote_vehicle_status_oil_status");
        brakeStatus=new Element(driver,"remote_vehicle_status_brake_status");
        washerStatus=new Element(driver,"remote_vehicle_status_fluid_level_washer_status");
        smartKeyBatteryStatus=new Element(driver,"remote_vehicle_status_smart_key_status");
        addevent = new Element(driver,"","add event");
        HomescreenPOI = new Element(driver,"home_search_edit");
        weathericon = new Element(driver,"tv_home_weather");
        location = new Element(driver,"tv_home_city");
        weatherTemp = new Element(driver,"tv_home_temperature");
        weatherinfo = new Element(driver,"iv_home_weather");
        remotefeaturescreen = new Element(driver,"title_title");
        AddeventButton = new Element(driver,"button1");
        CancelButton = new Element(driver,"button2");
        gotitbutton = new Element(driver,"iaetut_gotit_btn");
        Addeventtitle = new Element(driver,"alertTitle");
        Quickstarttutorial = new Element(driver,"tvRemoteStartContent");
        Gotosettings = new Element(driver,"btnGoToSetting");
        HomePOISearchIcon = new Element(driver, "home_search_img");
        ClimateIcon = new Element(driver, "vehicle_climate_icon");
        DoorIcon = new Element(driver, "vehicle_door_icon");

        //Fly Menu under Dashboard
        Maps = new Element(driver,"mapActivity");
        MVR = new Element(driver,"iv_home_add");
        Schedule = new Element(driver,"iv_home_alarm");
        Remote = new Element(driver,"remoteFragment","REMOTE");

        //Fly Menu under Maps
        POI = new Element(driver,"home_map_second_bar");
        ChargeFuelstations = new Element(driver,"home_map_fourth_bar");
        DealerLocator= new Element(driver,"home_map_fifth_bar");
        CarFinder= new Element(driver,"home_map_sixth_bar");
        Gpsicon = new Element(driver,"map_currentlocation_img");
        AddPOI = new Element(driver,"marker_detail_middle");
        forAddEventBtn = new Element(driver,"iaetut_addevent_btn");
        addEventCurrentDateBtn= new Element(driver,"","2022-1-19");
        eventSaveBtn = new Element(driver,"smallLabel","");
        addEventTitel =new Element(driver,"title","Title");
        eventDateAndTime =new  Element(driver,"timeTextView","17:00 - 18:00");
    }

    public final String light_Panel_XPATH = "/" +
            "/android.view.ViewGroup[@resource-id='com.stationdm.genesis:id/remote_vehicle_status_light_layout']";
    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (titleTextView.getTextValue().toLowerCase().equals("REMOTE FEATURES"));
    }


    public boolean isOilStatusOk()
    {
        return oilStatus.getTextValue().equals("OK");
    }

    public boolean isBrakeStatusOk()
    {
        return brakeStatus.getTextValue().equals("OK");
    }

    public boolean isWasherStatusOk()
    {
        return washerStatus.getTextValue().equals("OK");
    }
    public boolean isSmartKeyBatteryStatusOk()
    {
        return smartKeyBatteryStatus.getTextValue().equals("OK");
    }

    String ClosestPOI = "*[@class='android.widget.RelativeLayout']";

    public boolean isClosestPOIlistpresent()
    {
        return elementExists.byXpath(ClosestPOI);
    }

    //Under Maps
    public String POISearch = "//*[@class='android.widget.TextView' and @text='POI SEARCH']";
    public void TapPOISearchinmaps() { tap.elementByXpath(POISearch);}

    public String Favorites = "//*[@class='android.widget.TextView' and @text='FAVORITES']";
    public void TapFavoritesinmaps() { tap.elementByXpath(Favorites);}

    public String Tap="//*[@class='ccb' and @x='916' and @y='2046']";
    public void TAPMAGNIFYGLASS() { tap.elementByXpath(Tap);}

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent(int Longwait){
        return isPresent(5);
    }
    public void TapOkbutton() {tap.elementByXpath(button1);}

    public String button1="//*[@class='android.widget.Button' and @resource-id='android:id/button1']";
    public String button2="//*[@class='android.widget.Button' and @resource-id='android:id/button2']";

    public boolean CallbuttonisPresent()
    {
        return elementExists.byXpath(button1);
    }
    public boolean CancelbuttonisPresent()
    {
        return elementExists.byXpath(button2);
    }
//    String eventButton = "//*[@index='7']";
//    public void TapeventButton() { tap.elementByXpath(eventButton);}

    
}

