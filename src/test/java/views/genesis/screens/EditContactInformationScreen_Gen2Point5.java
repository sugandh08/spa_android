package views.genesis.screens;

import views.Screen;
import io.appium.java_client.AppiumDriver;
import library.Element;

import java.util.Random;

public class EditContactInformationScreen_Gen2Point5 extends Screen {

    public Element subTitleTextView;
    public Element phoneConsentCheckBox;
    public Element closeIcon ;
    public Element saveButton ;
    public Element phoneTypeDropDown ;

    public EditContactInformationScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "title_title", "EDIT CONTACT INFORMATION");
        phoneConsentCheckBox= new Element(driver, "item_profile_phone_consent_checkbox","");
        closeIcon= new Element(driver, "title_left_icon","");
        saveButton= new Element(driver, "save_txt","SAVE");
        phoneTypeDropDown= new Element(driver, "item_profile_phone_type_pickertext","");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("EDIT CONTACT INFORMATION");
    }

    private String getXpathOfContactInformationTextFields(String textField)
    {
        return "/" +"/android.widget.TextView[@text='"+textField+"']"
                +
                "/.." +"/android.widget.EditText[@resource-id='com.stationdm.genesis:id/pickertext_content_txt']"
                ;
    }

    public void updateContactInformation(String addressLine1)
    {
        clearTextField.clearTextByXpath(getXpathOfContactInformationTextFields("Address Line 1*"));
        enterText.byXpath(getXpathOfContactInformationTextFields("Address Line 1*"), addressLine1);
        phoneTypeDropDown.tap();
        phoneTypeDropDown.tap();
        saveButton.tap();
    }
}
