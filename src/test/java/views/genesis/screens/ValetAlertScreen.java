package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Valet Alert screen that is accessed through the Alert Settings screen.
 */
public class ValetAlertScreen extends Screen {
    public Element backButton;
    public Element titleTextView;

    public Element informationButton;

    public Element distanceLimitButton;

    public Element distanceLimitTextView;
    public Element saveButton;

    public Element cancelButton;
    public Element doneButton;
public Element valetLimitTextView;
    public final String valetLimitWheelXpath = "/" + "/android.view.View[@resource-id='com.stationdm.genesis:id/valet_wheel']";

    public final String distanceLimitWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/valet_wheel']";

    public ValetAlertScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
        informationButton = new Element(driver,"title_right_icon");

        distanceLimitButton = new Element(driver,"alert_valet_ll");
        distanceLimitTextView = new Element(driver,"alert_valet_value_txt");
        saveButton = new Element(driver,"alert_valet_save_ll");

        cancelButton = new Element(driver,"cancel");
        doneButton = new Element(driver,"done");
        valetLimitTextView = new Element(driver, "tv_valet_txt2");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("Valet Alert") && distanceLimitTextView.elementExists();
    }
}
