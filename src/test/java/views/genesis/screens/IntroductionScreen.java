package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * Teh Introduction screen displayed before logging into the app.
 */
public class IntroductionScreen extends Screen
{
    private static final String RESOURCE_ID_PREFIX = "com.stationdm.genesis:id/";

    public Element loginButton;
    public Element demoButton;
    public Element pagerImage;

    /**
     * LoginScreen constructor
     * @param driver Appium driver
     */
    public IntroductionScreen(AppiumDriver driver) {
        super(driver);

        loginButton = new Element(driver,RESOURCE_ID_PREFIX + "login_btn", "LOGIN");
        demoButton = new Element(driver,RESOURCE_ID_PREFIX + "demo_login_btn", "DEMO");
        pagerImage = new Element(driver,RESOURCE_ID_PREFIX + "pager_img", "");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return
            loginButton.elementExists() &&
            demoButton.elementExists();
    }
}
