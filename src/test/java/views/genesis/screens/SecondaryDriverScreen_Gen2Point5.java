package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SecondaryDriverScreen_Gen2Point5 extends Screen {

    public Element subTitleTextView;
    public Element profileTextView;
    public Element secondaryDriverTextView;
    public Element addSecondaryDriver1;
    public Element addSecondaryDriver2;
    public Element noSecondaryDriverText;
    public Element driverNameText;
    public Element driverInvitationStatusText;
    public Element driverDeleteButton;
    public Element deleteInviteTextView;
    public Element yesButton;
    public Element cancelButton;
    public Element driverEditButton;
    public Element driverStatusTimeText;


    public SecondaryDriverScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "title_title", "PROFILE");
        profileTextView = new Element(driver, "", "Profile");
        secondaryDriverTextView = new Element(driver, "", "Secondary Drivers");
        addSecondaryDriver1=new Element(driver,"sd_drivers_add_ll");
        noSecondaryDriverText=new Element(driver,"","No Secondary Driver has been added to your account");
        driverNameText=new Element(driver,"sditem_driver_name_txt");
        driverInvitationStatusText=new Element(driver,"sditem_driver_status_txt");
        addSecondaryDriver2=new Element(driver,"sd_drivers_bottomadd_ll");
        driverDeleteButton = new Element(driver, "sditem_delete_btn", "Delete");
        yesButton=new Element(driver,"","Yes");
        cancelButton=new Element(driver,"","Cancel");
        deleteInviteTextView=new Element(driver,"message","Are you sure you want to Delete Invitation?");
        driverEditButton=new Element(driver,"sditem_edit_btn","Edit");
        driverStatusTimeText=new Element(driver,"sditem_driver_status_time_txt","");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("SECONDARY DRIVER INVITATION");

    }
    public void clickOnDriverEditButton(String firstName, String lastName)
    {
        tap.elementByXpath("//*[@text='Edit' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='"+firstName+" "+ lastName+"']]]");
    }

    public String getXpathOfSecondaryDriverNameText(String firstName, String lastName)
    {
        return "/" +
                "/android.widget.ImageView[@resource-id='com.stationdm.genesis:id/sditem_driver_img']" +
                "/.." +
                "/android.widget.TextView[@text='"+firstName+" "+ lastName+"']";
    }

    public void clickOnSecondaryDriverDeleteButton(String firstName, String lastName) throws InterruptedException {
       tap.elementByXpath("//*[@text='Delete' and ./parent::*[(./preceding-sibling::* | " +
               "./following-sibling::*)[@text='"+firstName+" "+ lastName+"']]]");
       yesButton.tap();

    }
}