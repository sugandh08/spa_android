package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the password entry screen that is part of the PIN reset process.
 */
public class ChangePinEnterPasswordScreen extends Screen {
    public Element titleTextView;
    public Element passwordTitleTextView;
    public Element enterPasswordEditText;
    public Element submitButton;

    public ChangePinEnterPasswordScreen(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver,"title_title");
        passwordTitleTextView = new Element(driver,"","GENESIS PASSWORD");
        enterPasswordEditText = new Element(driver,"password");
        submitButton = new Element(driver,"next_txt");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("CHANGE PIN") && passwordTitleTextView.elementExists();
    }
}
