package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Owner's Manual PDF view screen that is accessible through the About Genesis screen
 */
public class OwnersManualScreen extends Screen {
    public Element titleTextView;

    public OwnersManualScreen(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver,"title_title");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("OWNER’S MANUAL");
    }
}
