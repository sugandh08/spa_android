package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class EditSecondaryDriverInformationScreen_Gen2Point5 extends Screen {
    public Element subTitleTextView;
    public Element secondaryDriverTextView;
    public Element invitedUserName;
    public Element firstNameText;
    public Element lastNameText;
    public Element emailText;
    public Element deleteInvitationButton;
    public Element backIcon;
    public Element yesButton;

    public EditSecondaryDriverInformationScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "title_title", "PROFILE");
        secondaryDriverTextView = new Element(driver, "sd_invite_detail_name_primary_txt", "Secondary Driver");
        invitedUserName = new Element(driver, "sd_invite_detail_name_txt", "");
        firstNameText = new Element(driver, "sd_invite_detail_firstname_txt", "");
        lastNameText = new Element(driver, "sd_invite_detail_lastname_txt", "");
        emailText = new Element(driver, "sd_invite_detail_email_txt", "");
        deleteInvitationButton = new Element(driver, "sd_invite_detail_bottom_txt", "DELETE INVITATION");
        backIcon = new Element(driver, "title_left_icon", "");
        yesButton=new Element(driver,"","Yes");
    }
    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return secondaryDriverTextView.elementExists(waitTime) && secondaryDriverTextView.getTextValue().equals("Secondary Driver");

    }

    public void secondaryDriverEditInformation()
    {
        deleteInvitationButton.tap();
        yesButton.tap();
    }

}
