package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;



/**
 * This is the curfew alert screen that is accessible through the Alert Settings screen.
 */
public class CurfewAlertScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element informationButton;

    public Element curfewFromButton;
    public Element curfewFromTextView;
    public Element curfewToButton;
    public Element curfewToTextView;
    public Element deleteButton;
    public Element saveButton;

    public Element cancelButton;
    public Element doneButton;

    public final String dayWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/weekdays_wheel']";
    public final String hourWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/hours_wheel']";
    public final String minuteWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/mins_wheel']";
    public final String meridianWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/ampm_wheel']";

    public CurfewAlertScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon","");
        titleTextView = new Element(driver,"title_title","Curfew Alert");
        informationButton = new Element(driver,"title_right_icon");

        curfewFromButton = new Element(driver,"","From");
        curfewFromTextView = new Element(driver,"curfew_from_value_txt");
        curfewToButton = new Element(driver,"curfew_to_value_ll");
        curfewToTextView = new Element(driver,"curfew_to_value_txt");
        saveButton = new Element(driver,"alert_curfew_save_ll");

        cancelButton = new Element(driver,"cancel");
        deleteButton = new Element(driver, "alert_curfew_delete_ll");
        doneButton = new Element(driver,"done");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("Curfew Alert") && curfewFromButton.elementExists();
    }
}
