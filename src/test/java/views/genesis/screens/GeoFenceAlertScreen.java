package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Geo-Fence alert screen that is accessible through the Alert Settings screen.
 */
public class GeoFenceAlertScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element informationButton;

    public Element searchLocationEditText;
    public Element searchLocationButton;
    public Element currentLocationButton;

    public Element geoFenceNameEditText;
    public Element milesEditText;
    public Element inclusiveRadioButton;
    public Element exclusiveRadioButton;
    public Element circleRadioButton;
    public Element rectangleRadioButton;
    public Element deleteButton;
    public Element saveButton;

    public GeoFenceAlertScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        deleteButton = new Element(driver, "alert_geofence_delete_ll");
        titleTextView = new Element(driver,"title_title");
        informationButton = new Element(driver,"title_right_icon");

        searchLocationEditText = new Element(driver,"geofence_search_edit");
        searchLocationButton = new Element(driver,"geofence_search_icon");
        currentLocationButton = new Element(driver,"geofence_currentloc");

        geoFenceNameEditText = new Element(driver,"geofence_name_edit");
        milesEditText = new Element(driver,"geofence_miles_edit");
        inclusiveRadioButton = new Element(driver,"geofence_inclusive_rb");
        exclusiveRadioButton = new Element(driver,"geofence_exclusive_rb");
        circleRadioButton = new Element(driver,"geofence_circle_rb");
        rectangleRadioButton = new Element(driver,"geofence_rect_rb");

        saveButton = new Element(driver,"alert_geofence_save_ll");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("Geo-Fence Alert") && geoFenceNameEditText.elementExists();
    }
}
