package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Schedule Service web site that is accessible through the Monthly Vehicle Report screen.
 */
public class ScheduleServiceScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element txtDealerName;
    public Element schedulevaletpickup;
    public Element scheduleservicevaletscreen;
    public Element scheduledropoff;
    
    public ScheduleServiceScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        txtDealerName = new Element(driver,"" , "Hyundai Genesis Of La Quinta home page");
        schedulevaletpickup = new Element(driver,"tvPickUp");
        scheduleservicevaletscreen =  new Element(driver,"title_title","SCHEDULE SERVICE VALET");
        scheduledropoff = new Element(driver,"tvDropOff");

    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time to wait for screen
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return titleTextView.getTextValue(waitTime).equals("Car Care Scheduling");
    }

    public boolean isScheduleservicescreenpresent() {
        return elementExists.byXpath("//*[@class='android.view.View' and @text='SCHEDULE A SERVICE APPOINTMENT']");
//        return elementExists.byXpath("//*[@class='android.view.View' and @resource-id='ocas-header']|//*[@class='android.view.View' and @text='SCHEDULE A SERVICE APPOINTMENT']|//*[@class='android.view.View' and @resource-id='root']|[@class='android.widget.TextView|//*[@class='android.view.View' and @text='SCHEDULE A SERVICE CHECK IN']|//*[@class='android.view.View' and @resource-id='ext-element-31']|//*[@class='android.view.View' and @text='SCHEDULE A SERVICE APPOINTMENT NORTH PALM HYUNDAI']");
    }
}
