package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class ProfileScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element profileTab;
    public Element secondaryDriversTab;
    public Element profileNameTextView;
    public Element profileOwnerTextView;
    public Element vehicleNickname;
    public Element vehicleNicknameEditButton;
    public Element vehicleName;
    public Element vin;
    public Element vehicleSecondaryDriver;
    //public Element connectedCarePackageExpiration;
    //public Element remotePackageExpiration;
    //public Element guidancePackageExpiration;
    public Element profilePrefix;
    public Element profileFirstName;
    public Element profileLastName;
    public Element profileMiddleInitial;
    public Element profileSufix;
    public Element personalInformationEditButton;
    public Element addressTextView;
    public Element phoneMobileTextView;
    public Element phoneLandlineTextView;
    public Element accountEmailTextView;
    public Element notificationEmailTextView;
    public Element contactInformationEditButton;
    public Element emergencyContactFirstNameTextView;
    public Element emergencyContactLastNameTextView;
    public Element emergencyContactRelationshipTextView;
    public Element emergencyContactPhoneTypeTextView;
    public Element emergencyContactMobilePhoneNumberTextView;
    public Element emergencyContactLandlinePhoneNumberTextView;
    public Element emergencyContactEmailTextView;
    public Element emergencyContactInformationEditButton;
    public Element genesisAccessories;
    public Element profileButton;
    public Element allowPicturePopUp;
    public Element dateFormat;
    public Element myDriverSettingsButton;
    public Element calendarSettingButton;
    public Element generalButton;
    public Element soundSettingButton;
    public Element vehicleSettingButton;
    public Element unitSettingButton;



    public ProfileScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        profileTab = new Element(driver,"menu_profile_ll","");
        secondaryDriversTab = new Element(driver,"","Secondary Drivers");
        profileNameTextView = new Element(driver,"profile_name_txt");
        profileOwnerTextView = new Element(driver,"profile_name_primary_txt");
        vehicleNickname = new Element(driver,"profile_nickname_txt");
        vehicleNicknameEditButton = new Element(driver,"profile_vehicle_edit");
        vehicleName = new Element(driver,"profile_vehicle_txt");
        vin = new Element(driver,"profile_vin_txt");
        vehicleSecondaryDriver = new Element(driver,"profile_seceondary_driver_txt");
        //connectedCarePackageExpiration = new Element(driver,"item_profile_connected_service_time");
        //remotePackageExpiration = new Element(driver,"item_profile_connected_service_time");
        //guidancePackageExpiration = new Element(driver,"item_profile_connected_service_time");
        profilePrefix = new Element(driver,"profile_prefix_txt");
        profileFirstName = new Element(driver,"profile_first_name_txt");
        profileLastName = new Element(driver,"profile_last_name_txt");
        profileMiddleInitial = new Element(driver,"profile_middle_initial_txt");
        profileSufix = new Element(driver,"profile_suffix_txt");
        personalInformationEditButton = new Element(driver,"profile_personal_edit");
        addressTextView = new Element(driver,"profile_contact_address_txt");
        phoneMobileTextView = new Element(driver,"profile_contact_mobile_txt");
        phoneLandlineTextView = new Element(driver,"profile_contact_landline_txt");
        accountEmailTextView = new Element(driver,"profile_contact_account_email_txt");
        notificationEmailTextView = new Element(driver,"profile_contact_notification_email_txt");
        contactInformationEditButton = new Element(driver,"profile_contact_edit");
        emergencyContactFirstNameTextView = new Element(driver,"profile_emergency_firstname_txt");
        emergencyContactLastNameTextView = new Element(driver,"profile_emergency_lastname_txt");
        emergencyContactRelationshipTextView = new Element(driver,"profile_emergency_relationship_txt");
        emergencyContactPhoneTypeTextView = new Element(driver,"profile_emergency_mobile_title_txt");
        emergencyContactMobilePhoneNumberTextView = new Element(driver,"profile_emergency_mobile_txt");
        emergencyContactLandlinePhoneNumberTextView = new Element(driver,"profile_emergency_mobile_txt");
        emergencyContactEmailTextView = new Element(driver,"profile_emergency_email_txt");
        emergencyContactInformationEditButton = new Element(driver,"profile_emergency_edit");
        genesisAccessories = new Element(driver,"profile_accessories_txt");
        profileButton  = new Element(driver,"profile_img");
        allowPicturePopUp = new Element(driver,"permission_message");
        dateFormat = new Element(driver,"llExpiredDate");
        myDriverSettingsButton = new Element(driver,"profile_profile_setting_txt");
        calendarSettingButton = new Element(driver,"","Calendar Settings");
        generalButton = new Element(driver,"","General");
        soundSettingButton = new Element(driver,"","Sound Settings");
        vehicleSettingButton = new Element(driver,"","Vehicle Settings");
        unitSettingButton = new Element(driver,"","Unit Settings");
    }

    /**
     * Scroll to the middle of the screen.
     */
    public void scrollDownHalf() {
        swipe.upCustomPercentage(1,
                .75,
                .3,
                .5);
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     * Note: the scrolling is only done in 3 blocks: top (default view), middle, and bottom. A more accurate
     * scrolling method might need to be built to work with all screen sizes and scaling.
     * @param element element to look for in this screen.
     */
    public void scrollToElementSection(Element element) {
        int scrolls = 0;
        while(!element.elementExists(1) && scrolls < 5){
            scrollDownHalf();
            System.out.println("\nNOTE: above element not found is likely not an error.\n");
            scrolls++;
        }
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return titleTextView.getTextValue().equals("PROFILE");
    }

    public boolean isPresent(long waitTime) {
        return titleTextView.getTextValue(waitTime).equals("PROFILE");
    }

    public String addDriverBtn ="(//*[@id='sd_drivers_bottomadd_ll']/*[@class='android.widget.ImageView'])[1]";
    public void tapAddDriver() {tap.elementByXpath(addDriverBtn);}

}
