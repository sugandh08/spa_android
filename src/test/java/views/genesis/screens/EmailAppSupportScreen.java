package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Email App Support screen that is accessed through the Support screen.
 */
public class EmailAppSupportScreen extends Screen {
    public Element backButton;
    public Element vehicleVinSelectionText; // pull VIN's from account
    public Element incidentFeatureNameSelectionText;
    public Element dateSelectionText;
    public Element timeSelectionText;
    public Element keyLocationSelectionText;
    public Element connectionTypeSelectionText;
    public Element notesEditText;
    public Element submitButton;

    public EmailAppSupportScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon","");
        vehicleVinSelectionText = new Element(driver,"tv_vehicle_vin","Vehicle");
        incidentFeatureNameSelectionText = new Element(driver,"tv_feature_name","Incident Feature Name");
        dateSelectionText = new Element(driver,"tv_date","Date");
        timeSelectionText = new Element(driver,"tv_time","Time");
        keyLocationSelectionText = new Element(driver,"tv_location","Key Location");
        connectionTypeSelectionText = new Element(driver,"tv_connection_type","Connection Type");
        notesEditText = new Element(driver,"et_email_notes","Notes");
        submitButton = new Element(driver,"tv_email_submit","");
    }

    /**
     * Used to swipe between 'email app support' options
     */
    public void swipeLeft(){
        swipe.left(1);
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        // if incident feature name and key location exists
        return (vehicleVinSelectionText.elementExists() && incidentFeatureNameSelectionText.elementExists());
    }
}
