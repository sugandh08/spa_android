package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Settings screen that is accessed through the Side menu.
 */
public class SettingsScreenGen2Point5 extends Screen {
    public final String automaticCollisionNotificationText = "AUTOMATIC COLLISION NOTIFICATION (ACN)";
    public final String sosEmergencyAssistanceText = "SOS EMERGENCY ASSISTANCE";
    public final String automaticDtcText = "AUTOMATIC DTC";
    public final String monthlyVehicleHealthReportText = "MONTHLY VEHICLE HEALTH REPORT";
    public final String maintenanceAlertText = "MAINTENANCE ALERT";

    public final String panicNotificationText = "PANIC NOTIFICATION";
    public final String alarmNotificationText = "ALARM NOTIFICATION";
    public final String hornAndLightsText =  "HORN AND LIGHTS/LIGHTS ONLY";
    public final String remoteEngineStartStopText = "REMOTE ENGINE START/STOP";
    public final String remoteDoorLockUnlockText = "REMOTE DOOR LOCK/UNLOCK";
    public final String curfewAlertText = "CURFEW ALERT";
    public final String valetAlertText = "VALET ALERT";
    public final String geofenceAlertText = "GEO-FENCE ALERT";
    public final String speedAlertText = "SPEED ALERT";

    public Element backButton;
    public Element titleTextView;
    public Element changePinButton;
    public Element updateSecurityQuestion;
    public Element notificationsTextView;
    public Element notificationDeviceNameTextView;
    public Element receiveNotificationsOnDeviceToggle;
    public Element registeredDevicesButton;
    public Element connectedCareExpandButton;
    public Element connectedCareToggleAllText;
    public Element connectedCareToggleAllEmail;
    public Element connectedCareToggleAllApp;
   // public Element remoteExpandButton;
    public Element remoteToggleAllText;
    public Element remoteToggleAllEmail;
    public Element remoteToggleAllApp;

    public Element preferredContactInformationButton;

    public SettingsScreenGen2Point5(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver, "title_left_icon");
        titleTextView = new Element(driver, "title_title");
        changePinButton = new Element(driver, "settings_changepin");
        updateSecurityQuestion = new Element(driver,"settings_update_question_txt");
        notificationsTextView = new Element(driver, "tv_user_title");
        notificationDeviceNameTextView = new Element(driver, "notification_device_name");
        receiveNotificationsOnDeviceToggle = new Element(driver, "notification_switch");
        registeredDevicesButton = new Element(driver, "notification_account_txt");

        //connectedCareExpandButton = new Element(driver, "notification_connected_title_arrow");
        connectedCareExpandButton = new Element(driver, "ivExpandArrow","Connected Care");
        connectedCareToggleAllText = new Element(driver, "notification_group_content_allsms");
        connectedCareToggleAllEmail = new Element(driver, "notification_group_content_allemail");
        connectedCareToggleAllApp=new Element(driver,"notification_group_content_allapp");
        //remoteExpandButton = new Element(driver, "notification_remote_title_arrow");
       // remoteExpandButton = new Element(driver, "","Remote");
        remoteToggleAllText = new Element(driver, "notification_group_content_allsms");
        remoteToggleAllEmail = new Element(driver, "notification_group_content_allemail");
        remoteToggleAllApp=new Element(driver,"notification_group_content_allapp");

        preferredContactInformationButton = new Element(driver, "notification_refered_contact_txt");
    }

    /**
     * Builds an xpath to the email toggle element using the setting name
     * @param settingName The relative setting to the toggle element
     * @return The xpath to the toggle element
     */
    private String getXpathOfEmailToggle(String settingName) {
        return "/" +
                "/android.widget.TextView[@text='" + settingName + "']" +
                "/.." +
                "/android.widget.ImageView[@resource-id='com.stationdm.genesis:id/notification_setting_item_email']";
    }

    /**
     * Builds an xpath to the app toggle element using the setting name
     * @param settingName The relative setting to the toggle element
     * @return The xpath to the toggle element
     */
    private String getXpathOfAppToggle(String settingName) {
        return "/" +
                "/android.widget.TextView[@text='" + settingName + "']" +
                "/.." +
                "/android.widget.ImageView[@resource-id='com.stationdm.genesis:id/notification_setting_item_app']";
    }

    /**
     * Builds an xpath to the sms toggle element using the setting name
     * @param settingName The relative setting to the toggle element
     * @return The xpath to the toggle element
     */
    private String getXpathOfSmsToggle(String settingName) {
        return "/" +
                "/android.widget.TextView[@text='" + settingName + "']" +
                "/.." +
                "/android.widget.ImageView[@resource-id='com.stationdm.genesis:id/notification_setting_item_sms']";
    }

    /**
     * Checks if the chosen email notification element exists
     * @param settingName The relative setting to the email notification element
     * @return True if the element exists, false otherwise
     */
    public boolean checkIfEmailNotificationStateOfSettingExists(String settingName) {
        return elementExists.byXpath(getXpathOfEmailToggle(settingName));
    }

    /**
     * Checks if the chosen app notification element exists
     * @param settingName The relative setting to the app notification element
     * @return True if the element exists, false otherwise
     */
    public boolean checkIfAppNotificationStateOfSettingExists(String settingName) {
        return elementExists.byXpath(getXpathOfAppToggle(settingName));
    }


    /**
     * Checks if the chosen sms notification element exists
     * @param settingName The relative setting to the sms notification element
     * @return True if the element exists, false otherwise
     */
    public boolean checkIfSmsNotificationStateOfSettingExists(String settingName) {
        return elementExists.byXpath(getXpathOfSmsToggle(settingName));
    }

    /**
     * Gets the selected or unselected state of the chosen email notification element
     * @param settingName The relative setting to the email notification element
     * @return true or false that corresponds to the notification being enabled or disabled
     */
    public boolean getEmailNotificationStateOfSetting(String settingName) {
        return attribute.getSelectedValueByXpath(getXpathOfEmailToggle(settingName));
    }

    /**
     * Gets the selected or unselected state of the chosen App notification element
     * @param settingName The relative setting to the App notification element
     * @return true or false that corresponds to the notification being enabled or disabled
     */
    public boolean getAppNotificationStateOfSetting(String settingName) {
        return attribute.getSelectedValueByXpath(getXpathOfAppToggle(settingName));
    }

    /**
     * Gets the selected or unselected state of the chosen sms notification element
     * @param settingName The relative setting to the sms notification element
     * @return true or false that corresponds to the notification being enabled or disabled
     */
    public boolean getSmsNotificationStateOfSetting(String settingName) {
        return attribute.getSelectedValueByXpath(getXpathOfSmsToggle(settingName));
    }

    /**
     * Taps the selected app notification element to toggle its value
     * @param settingName The relative setting to the app notification element
     */
    public void toggleAppNotificationOfSetting(String settingName) {
        tap.elementByXpath(getXpathOfAppToggle(settingName));
    }

    /**
     * Taps the selected email notification element to toggle its value
     * @param settingName The relative setting to the email notification element
     */
    public void toggleEmailNotificationOfSetting(String settingName) {
        tap.elementByXpath(getXpathOfEmailToggle(settingName));
    }

    /**
     * Taps the selected sms notification element to toggle its value
     * @param settingName The relative setting to the sms notification element
     */
    public void toggleSmsNotificationOfSetting(String settingName) {
        tap.elementByXpath(getXpathOfSmsToggle(settingName));
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return titleTextView.getTextValue(waitTime).equals("SETTINGS") &&
                notificationsTextView.getTextValue().equals("Notifications");
    }

    public String remoteExpandButton ="(//android.widget.ImageView[contains(@resource-id,'com.stationdm.genesis:id/ivExpandArrow')])[2]";
    public void tapRemoteExpandButton() {tap.elementByXpath(remoteExpandButton);}

}
