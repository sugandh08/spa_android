package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Alert Settings screen that is reached through the Side menu.
 */
public class AlertSettingsScreen extends Screen {

    public Element speedAlertValue;
    public Element menuButton;
    public Element titleTextView;


    public Element speedAlertToggle;
    public Element valetAlertToggle;
    public Element curfewAlertToggle;
    public Element geoFenceAlertToggle;

    public Element speedAlertButton;
    public Element valetAlertButton;
    public Element curfewAlertButton;
    public Element addNewCurfewAlertButton;
    public Element geoFenceAlertButton;
    public Element addNewGeoFenceAlertButton;
    public Element geofenceAlertText;

    public Element saveButton;
    public Element valetAlertValue;

    //Geofence alert screen
    public Element Geofencesearchbar;
    public Element Geofencesearchicon;
    public Element Geofenncealertname;
    public Element Geofencealertmiles;
    public Element GeofenceInclusive;
    public Element GeofenceExclusive;
    public Element GeofenceCirclefromcenter;
    public Element GeofenceRectangularfromcenter;
    public Element GeofenceSave;
    public Element Geofencealerttitle;
    public Element Geofencealertlocationtitle;
    public Element Geofencealertmilestitle;
    public Element GeofencealertiIclusiveEclusivetitle;
    public Element SuccessfullySavedOkbutton;
    public Element GeofenceDeletebutton;
    public Element ExistingCurfewAlert;
    public Element Geofencealert;
    public Element chooseAlert;

    public AlertSettingsScreen(AppiumDriver driver) {
        super(driver);

        //Geofence alert screen
        Geofencesearchbar = new Element(driver, "geofence_search_edit");
        Geofencesearchicon = new Element(driver, "geofence_search_icon");
        Geofenncealertname = new Element(driver, "geofence_name_edit");
        Geofencealertmiles = new Element(driver, "geofence_miles_edit");
        GeofenceInclusive = new Element(driver, "geofence_inclusive_rb");
        GeofenceExclusive = new Element(driver, "geofence_exclusive_rb");
        GeofenceCirclefromcenter = new Element(driver, "geofence_circle_rb");
        GeofenceRectangularfromcenter = new Element(driver, "geofence_rect_rb");
        GeofenceSave = new Element(driver, "alert_geofence_save_ll");
        SuccessfullySavedOkbutton = new Element(driver, "button1");
        GeofenceDeletebutton = new Element(driver, "alert_geofence_delete_ll");

        speedAlertValue = new Element(driver, "tv_speed_txt2");
        menuButton = new Element(driver, "title_left_icon");
        titleTextView = new Element(driver, "title_title");

        speedAlertToggle = new Element(driver, "iv_speed_tick");
        valetAlertToggle = new Element(driver, "iv_valet_tick");
        curfewAlertToggle = new Element(driver, "iv_curfew_tick");
        geoFenceAlertToggle = new Element(driver, "iv_geofence_tick");

        chooseAlert = new Element(driver, "geofence_item_ll");


        speedAlertButton = new Element(driver, "tv_speed_txt");
        valetAlertButton = new Element(driver, "tv_valet_txt");
        curfewAlertButton = new Element(driver, "", "Set up an alert for when the vehicle should not be driven.");
        addNewCurfewAlertButton = new Element(driver, "curfew_add_txt");
        geoFenceAlertButton = new Element(driver, "", "Geo-Fence Alert");
        addNewGeoFenceAlertButton = new Element(driver, "geofence_item_footer_txt");
        geofenceAlertText = new Element(driver, "geofence_title");
        Geofencealerttitle = new Element(driver, "geofence_item_place_txt");
        Geofencealertlocationtitle = new Element(driver, "geofence_item_address_txt");
        Geofencealertmilestitle = new Element(driver, "", "40 MILES");
        Geofencealert = new Element(driver, "", "60 MILES");
        GeofencealertiIclusiveEclusivetitle = new Element(driver, "geofence_item_shape_txt");
        ExistingCurfewAlert = new Element(driver, "curfew_info_ll");

        saveButton = new Element(driver, "alerts_save_ll");
        valetAlertValue = new Element(driver, "tv_valet_txt2");
    }


    /**
     * Scroll to the middle of the screen.
     */
    public void scrollDownHalf() {
        swipe.upCustomPercentage(1,
                .75,
                .3,
                .5);
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     *
     * @param element element to look for in this screen.
     */
    public void scrollToElementSection(Element element) {
        int scrolls = 0;
        while (!element.elementExists(1) && scrolls < 5) {
            scrolls++;
            scrollDownHalf();
            System.out.println("\nNOTE: above element not found is likely not an error.\n");
        }
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     * Searches for 2 elements.
     *
     * @param element1 element to look for in this screen.
     * @param element2 element to look for in this screen.
     */
    public Element scrollToElementSection(Element element1, Element element2) {
        int scrolls = 0;
        while (scrolls < 5) {
            if (element1.elementExists(1)) {
                return element1;
            }

            if (element2.elementExists(1)) {
                return element2;
            }

            scrolls++;
            scrollDownHalf();
            System.out.println("\nNOTE: above element not found is likely not an error.\n");
        }

        // The return doesn't matter at this point since
        // none of the elements could be found, thus the
        // test requiring this method will fail.
        return element1;
    }

    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     *
     * @param waitTime Time to wait for screen
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return titleTextView.elementExists() && titleTextView.getTextValue(waitTime).equals("ALERT SETTINGS");
    }

    public boolean getGeoFenceAlertLimit(int atIndex) {
        return elementExists.byXpath(xpathOfExisitingGeoFenceAlert(atIndex));
    }

    public String xpathOfExisitingGeoFenceAlert(int atIndex) {
        return "(//*[@resource-id='com.stationdm.genesis:id/geofence_recyclerview']/*[./*[@resource-id='com.stationdm.genesis:id/geofence_item_ll']])[" + atIndex + "]";
    }

    public void clickOnGeoFenceAlert(int atIndex) {
        tap.elementByXpath(xpathOfExisitingGeoFenceAlert(atIndex));
    }

    public String xpathOfExistingCurfewAlert = "(//android.widget.RelativeLayout//android.widget.LinearLayout[@id = 'curfew_info_ll'])[1]";


    public boolean checkIfCurfewAlertAlreadyAdded() {
        return elementExists.byXpath(xpathOfExistingCurfewAlert);
    }

    public String xpathOfFromCurfewAlert = "(//*[@resource-id='com.stationdm.bluelink:id/curfew_recyclerview']/*/*/*[@resource-id='com.stationdm.bluelink:id/curfew_from_txt'])[1]";

    public void clickOnCurfewAlert() {
        tap.elementByXpath(xpathOfExistingCurfewAlert);
    }

    public String xpathOfExistingSpeedAlert = "//android.widget.TextView[@id = 'tv_speed_txt2']";

    public boolean checkIfSpeedAlertAlreadyAdded() {
        return elementExists.byXpath(xpathOfExistingSpeedAlert);
    }

    public String xpathOfExistingValetAlert = "//android.widget.TextView[@id = 'tv_valet_txt2']";

    public boolean checkIfValetAlertAlreadyAdded() {
        return elementExists.byXpath(xpathOfExistingValetAlert);
    }

    // OK BUTTON
    public String successfullysaved = "//*[@resource-id='android:id/button1']";

    public boolean okbutton() {
        return elementExists.byXpath(successfullysaved);
    }

    public void Tapokbutton() {
        tap.elementByXpath(successfullysaved);
    }

    //DELETE BUTTON
    public String DeleteButton = "//*[@resource-id='android:id/button1']";

    public boolean deletebuttn() {
        return elementExists.byXpath(DeleteButton);
    }

    public void TapDeletebutton() {
        tap.elementByXpath(DeleteButton);
    }


    /*public String Geofencealerttitle ="(//android.widget.ImageView[contains(@resource-id,'com.stationdm.genesis:id/ivExpandArrow')])[2]";
    public void tapGeofenceAlert() {tap.elementByXpath(Geofencealerttitle);}
}*/
}