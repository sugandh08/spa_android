package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Activity Tutorial screen that appears when loading the Home screen in the Demo.
 */
public class ActivityTutorialScreen extends Screen{
    private Element activityTutorialScreen;
    public Element gotItButton;

    public ActivityTutorialScreen(AppiumDriver driver){
        super(driver);

        activityTutorialScreen = new Element(driver,"activity_tutcontain_ie");
        gotItButton = new Element(driver,"iaetut_gotit_btn");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return activityTutorialScreen.elementExists();
    }
}
