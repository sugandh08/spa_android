package views.genesis.screens;

import config.Account;
import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Enter New PIN screen that is part of the PIN reset process.
 */
public class ChangePinEnterNewPinScreen extends Screen {
    // There are 3 screens: Confirm PIN, New PIN, Confirm PIN
    public Element backButton;
    public Element titleTextView;

    // Numpad
    private Element zeroButton;
    private Element oneButton;
    private Element twoButton;
    private Element threeButton;
    private Element fourButton;
    private Element fiveButton;
    private Element sixButton;
    private Element sevenButton;
    private Element eightButton;
    private Element nineButton;

    public Element cancelButton;
    public Element backspaceButton;
    public Element saveButton;

    public ChangePinEnterNewPinScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");

        zeroButton = new Element(driver,"","0");
        oneButton = new Element(driver,"","1");
        twoButton = new Element(driver,"","2");
        threeButton = new Element(driver,"","3");
        fourButton = new Element(driver,"","4");
        fiveButton = new Element(driver,"","5");
        sixButton = new Element(driver,"","6");
        sevenButton = new Element(driver,"","7");
        eightButton = new Element(driver,"","8");
        nineButton = new Element(driver,"","9");

        cancelButton = new Element(driver,"","CANCEL");
        backspaceButton = new Element(driver,"deleteImgae");
        saveButton = new Element(driver,"","SAVE");
    }

    public void enterPin(String pin) {
        String[] digits = pin.split("");

        // For each digit found
        for(String digit : digits) {
            tapDigit(digit);
        }
    }

    private void tapDigit(String digit) {
        switch(digit) {
            case "0":
                zeroButton.tap();
                break;
            case "1":
                oneButton.tap();
                break;
            case "2":
                twoButton.tap();
                break;
            case "3":
                threeButton.tap();
                break;
            case "4":
                fourButton.tap();
                break;
            case "5":
                fiveButton.tap();
                break;
            case "6":
                sixButton.tap();
                break;
            case "7":
                sevenButton.tap();
                break;
            case "8":
                eightButton.tap();
                break;
            case "9":
                nineButton.tap();
                break;
        }
    }

    // Return 9876 unless the account's PIN is 9876. If it is, return 9875.
    public String getIncorrectPin(Account account) {
        return account.pin.equals("9876") ? "9875" : "9876";
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return titleTextView.getTextValue().equals("CHANGE PIN");
    }
}