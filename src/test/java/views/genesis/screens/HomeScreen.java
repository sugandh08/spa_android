package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Home screen that is displayed after logging in to the app.
 */
public class HomeScreen extends Screen {

    // Accessible elements
    public Element menuButton;
    public Element poiSearchEdit;
    public Element poiSearchTap;
    public Element genesisAccessories;
    public Element genesisAccessoriesCloseButton;
    public Element addEvent;
    public Element startQuickTip;
    public Element closeStartQuickTip;
    public Element startButton;
    public Element city;
    public Element car;
    public Element temperature;
    public Element heat;
    public Element dew;
    public Element weather;
    public Element hamburgerbutton;

    //public Element demoModeLabel;
    public Element mapButton;
    public Element mvrButton;
    public Element scheduleButton;
    public Element remoteButton;
    public Element vehiclestatus;
    public Element profilepichamburger;

    //user profile screen
    public Element profileimg;
    public Element adddriver;



    // Accessible sub elements of elements
    //public CarCareOptions carCareOptions;
    //public HomeOptions homeOptions;
    //public MapOptions mapOptions;

    /**
     * HomeScreen constructor
     * @param driver Appium driver
     */
    public HomeScreen(AppiumDriver driver) {
        super(driver);

        menuButton = new Element(driver,"home_menu_icon") {
            @Override
            public void tap() {
                super.waitThenTap();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // do nothing, test will fail
                }
            }
        };

        poiSearchEdit = new Element(driver,"home_search_edit");
        hamburgerbutton = new Element(driver,"title_left_icon");
        poiSearchTap = new Element(driver,"home_search_img");
        genesisAccessories = new Element(driver,"accessories_bg_img");
        genesisAccessoriesCloseButton = new Element(driver,"accessorie_x_img");
        addEvent = new Element(driver,"addevent_img","add event");
        startQuickTip = new Element(driver,"iaetut_start_tip");
        closeStartQuickTip = new Element(driver,"iaetut_close_tip");
        startButton = new Element(driver,"setting_icon");
        city = new Element(driver,"tv_home_city");
        temperature = new Element(driver,"tv_home_temperature");
        heat = new Element(driver,"home_weather_heat_txt");
        dew = new Element(driver,"home_weather_dew_txt");
        weather = new Element(driver,"tv_home_weather");
        //demoModeLabel = new Element(driver, "demo_mode_lable");
        mapButton = new Element(driver,"mapActivity","");
        mvrButton = new Element(driver,"iv_home_add");
        scheduleButton = new Element(driver,"smallLabel","REMOTE");
        remoteButton = new Element(driver,"remoteFragment");
        vehiclestatus = new Element(driver,"btnVehicleStatus","VEHICLE STATUS");
        profilepichamburger = new Element(driver,"menu_profile_img");
        profileimg = new Element(driver,"profile_img");
        adddriver = new Element(driver,"sd_drivers_bottomadd_ll");

        /*carCareButton = new Element(driver,"sdm_left_img") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        homeButton = new Element(driver,"sdm_mid_img");
        mapButton = new Element(driver,"sdm_right_img") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };*/

        //carCareOptions = new CarCareOptions(driver);
        //homeOptions = new HomeOptions(driver);
        //mapOptions = new MapOptions(driver);
    }

    /**
     * Open the menuButton screen by swiping.
     */
    public void openMenuBySwipe() {
        swipe.fromLeftEdge(1);
    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */


    public String secondarydriverprofilescreen = "//*[@class='android.widget.TextView' and @text='Secondary Drivers']";
    public void TapSecondarydriver() {tap.elementByXpath(secondarydriverprofilescreen);}

    public String carCare = "//*[@id='icon' and ./parent::*[@id='carCareFragment']]";
    public void tapCarCare() {tap.elementByXpath(carCare);}
    public String mvrBtn = "//*[@id='ivRemoteButton' and (./preceding-sibling::* | ./following-sibling::*)[@text='MVR']]";
    public void tapMvrBtn(){tap.elementByXpath(mvrBtn);}

    public String mapBtn ="//*[@id='icon' and ./parent::*[@id='mapActivity']]";
    public void tapMapBtn() {tap.elementByXpath(mapBtn);}

    public boolean isPresent(long waitTime) {
        return addEvent.elementExists(waitTime) && mapButton.elementExists() && remoteButton.elementExists();
    }
    }


