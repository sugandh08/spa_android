package views.genesis.screens;

import config.Account;
import io.appium.java_client.AppiumDriver;
import library.Element;
import sun.awt.windows.ThemeReader;
import views.AppController;
import views.Screen;

/**
 * This is the Login screen that is displayed after launching the app, which allows users to
 * enter login data or enter Demo mode.
 */
public class LoginScreen extends Screen {
    public Element usernameEditText;
    public Element passwordEditText;
    public Element keepMeLoggedInCheckbox;
    public Element demoLoginButton;
    public Element loginButton;
    public Element registerButton;
    public Element allowButton;
    public Element allowLocationButton;
    public Element DenyButton;

    private AppController appController;



    private IntroductionScreen introductionScreen;

    /**
     * LoginScreen constructor
     * @param driver Appium driver
     */
    public LoginScreen(AppiumDriver driver) {
        super(driver);

        appController=new AppController(driver);

        usernameEditText = new Element(driver,"login_username","");
        passwordEditText = new Element(driver,"login_password","");
        keepMeLoggedInCheckbox = new Element(driver,"login_remember_img","");
        demoLoginButton = new Element(driver,"demo_login_btn","");
        loginButton = new Element(driver,"login_btn","");
        registerButton = new Element(driver,"register_btn","");
        allowButton = new Element(driver,"permission_allow_button");
        DenyButton = new Element(driver,"permission_deny_button");
        allowLocationButton = new Element(driver,"permission_allow_foreground_only_button");
        introductionScreen = new IntroductionScreen(driver);
    }

    /**
     * Log into the application with a username and password
     * @param username Username
     * @param password Password
     */
    private void loginWithUsernameAndPassword(String username, String password) {
        if(introductionScreen.isPresent()) {
            introductionScreen.loginButton.tap();

            allowLocationButton.tap();// ALLOW the location
            allowButton.tap();// allow phone media
            allowButton.tap();// allow manage phone call
            allowButton.tap();
            //DenyButton.tap();//Deny the Location

        }
        try{
            Thread.sleep(2000);
            this.usernameEditText.enterText(username);
            Thread.sleep(2000);
           // appController.appFunctions.tapAndroidBackButton();
            this.passwordEditText.enterText(password);
           // appController.appFunctions.tapAndroidBackButton();
            Thread.sleep(2000);
            loginButton.tap();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    /**
     * Login with credentials defined in an Account object with an option to check 'keep me logged in'
     * @param account The Account object containing the username and password
     */
    public void loginWithAccount(Account account) {
        // Get username and password from the account
        String username = account.email;
        String password = account.password;

        this.loginWithUsernameAndPassword(username, password);
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (usernameEditText.elementExists() && passwordEditText.elementExists());
    }
}