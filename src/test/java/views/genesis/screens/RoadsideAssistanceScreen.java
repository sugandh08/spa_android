package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class RoadsideAssistanceScreen extends Screen {

    public Element callRoadsidePopUp;
    public Element cancelBtn;
    public Element callBtn;


    public RoadsideAssistanceScreen(AppiumDriver driver) {
        super(driver);
        callRoadsidePopUp = new Element(driver,"message");
        cancelBtn  = new Element(driver,"button2");
        callBtn = new Element(driver,"button1");

    }

    @Override
    public boolean isPresent() {
        return false;
    }
}
