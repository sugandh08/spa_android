package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the security answer entry screen that is part of the PIN reset process
 */
public class ChangePinEnterSecurityAnswerScreen extends Screen {
    public Element titleTextView;
    public Element answerTitleTextView;
    public Element enterSecurityAnswerEditText;
    public Element submitButton;

    public ChangePinEnterSecurityAnswerScreen(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver,"title_title");
        answerTitleTextView = new Element(driver,"question");
        enterSecurityAnswerEditText = new Element(driver,"answer");
        submitButton = new Element(driver,"next_txt");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("CHANGE PIN") && answerTitleTextView.elementExists();
    }
}
