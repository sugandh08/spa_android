package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Parking Meter screen that is accessible through the Map screen by tapping the
 * Parking Meter button.
 */
public class ParkingMeterScreen extends Screen {
    public Element closeButton;

    public Element meterTimeTextView;
    //public Element meterTimeValueTextView;
    public Element reminderTextView;
    //public Element reminderValueTextView;

    public Element notesEditText;
    public Element cameraButton;
    public Element clearMeterButton;
    public Element saveButton;

    // Xpaths for each of the time scroll wheels because the interactions we have with them are
    // only done through GuiController, not here. The only thing we care about is using these Xpaths
    // to get the selenium Rectangle object of the scroll wheel.
    public final String METER_TIME_HOUR_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/metertime_hourwheel']";
    public final String METER_TIME_MINUTE_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/metertime_minwheel']";
    public final String REMINDER_HOUR_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/alert_hourwheel']";
    public final String REMINDER_MINUTE_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/alert_minwheel']";

    public ParkingMeterScreen(AppiumDriver driver) {
        super(driver);

        closeButton = new Element(driver,"meter_x_img");

        meterTimeTextView = new Element(driver, "", "SET METER TIME");
        //meterTimeValueTextView = new Element(driver, "tv_meter_time_value");
        reminderTextView = new Element(driver, "", "REMINDER");
        //reminderValueTextView = new Element(driver, "tv_alert_time_value");

        notesEditText = new Element(driver,"et_parking_meter_note");
        cameraButton = new Element(driver,"meter_camera_img");
        clearMeterButton = new Element(driver,"rl_parking_meter_clear");
        saveButton = new Element(driver,"rl_parking_meter_save");
    }

    public boolean isPresent() {
        return (notesEditText.elementExists() &&
                clearMeterButton.elementExists() &&
                saveButton.elementExists());
    }
}
