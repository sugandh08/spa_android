package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Genesis Resources screen accessed through the Support screen.
 */
public class GenesisResourcesScreen extends Screen {

    public Element backButton;
    public Element titleTextView;
    public Element webPreviousImageView;
    public Element webNextImageView;
    public Element webRefreshImageView;
    public Element webStopImageView;

    public GenesisResourcesScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        webPreviousImageView = new Element(driver,"iv_webview_previous");
        webNextImageView = new Element(driver,"iv_webview_next");
        webRefreshImageView = new Element(driver,"iv_webview_refresh");
        webStopImageView = new Element(driver,"iv_webview_stop");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return titleTextView.elementExists() && titleTextView.getTextValue().equals("GENESIS RESOURCES");
    }
}
