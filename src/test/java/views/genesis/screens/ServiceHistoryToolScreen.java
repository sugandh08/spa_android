package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Service History Tool screen that is accessible from the Side menu.
 */
public class ServiceHistoryToolScreen extends Screen{
    public Element menuButton;
    public Element titleTextView;
    public Element addServiceHistoryButton;

    public ServiceHistoryToolScreen(AppiumDriver driver){
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
        addServiceHistoryButton = new Element(driver,"title_right_icon");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("SERVICE HISTORY TOOL");
    }
}
