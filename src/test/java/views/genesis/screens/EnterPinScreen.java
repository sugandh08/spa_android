package views.genesis.screens;

import config.Account;
import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The PIN entry screen that is used when entering remote_genesis_old commands. Also used in miscellaneous parts
 * of the app. NOTE that there are other Enter PIN screens with slightly different configurations.
 * If any specialized Enter PIN screens exist for the test you are writing, use those instead. If no
 * other specialized Enter PIN screens exist, use this one. If there are problems with the test
 * that stem from the PIN entry, first check to see that you are using the correct screen.
 */
public class EnterPinScreen extends Screen {
    public Element remotePINTitle;
    public Element enterPINText;
    public Element enterPinscreen;

    // Numpad
    private Element zeroButton;
    private Element oneButton;
    private Element twoButton;
    private Element threeButton;
    private Element fourButton;
    private Element fiveButton;
    private Element sixButton;
    private Element sevenButton;
    private Element eightButton;
    private Element nineButton;
    public Element cancelButton;
    public Element deleteButton;

    public EnterPinScreen(AppiumDriver driver) {
        super(driver);

        remotePINTitle = new Element(driver,"tv_remote_pin_title");
        enterPINText = new Element(driver, "tv_enter_pin");

        enterPinscreen= new Element(driver,"enter_pin_txt");

        zeroButton = new Element(driver, "", "0");
        oneButton = new Element(driver, "", "1");
        twoButton = new Element(driver, "", "2");
        threeButton = new Element(driver, "", "3");
        fourButton = new Element(driver, "", "4");
        fiveButton = new Element(driver, "", "5");
        sixButton = new Element(driver, "", "6");
        sevenButton = new Element(driver, "", "7");
        eightButton = new Element(driver, "", "8");
        nineButton = new Element(driver, "", "9");
        cancelButton = new Element(driver,"","CANCEL");
        deleteButton = new Element(driver,"deleteImgae");
    }

    public void enterPin(String pin) {
        String[] digits = pin.split("");

        // For each digit found
        for(String digit : digits) {
            tapDigit(digit);
        }
    }

    private void tapDigit(String digit) {
        switch(digit) {
            case "0":
                zeroButton.tap();
                break;
            case "1":
                oneButton.tap();
                break;
            case "2":
                twoButton.tap();
                break;
            case "3":
                threeButton.tap();
                break;
            case "4":
                fourButton.tap();
                break;
            case "5":
                fiveButton.tap();
                break;
            case "6":
                sixButton.tap();
                break;
            case "7":
                sevenButton.tap();
                break;
            case "8":
                eightButton.tap();
                break;
            case "9":
                nineButton.tap();
                break;
        }
    }

    // Return 9876 unless the account's PIN is 9876. If it is, return 9875.
    public String getIncorrectPin(Account account) {
        return account.pin.equals("9876") ? "9875" : "9876";
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time to wait for screen
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return enterPINText.getTextValue(waitTime).equals("Enter PIN");
    }
}