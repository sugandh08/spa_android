package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Gmail composition screen. This is in the Gmail app that is called from the Genesis app,
 * mostly from the Email App Support section.
 * Note that this screen will only show properly if the Gmail app is installed and set as the default
 * email app on the phone.
 */
public class GmailComposeScreen extends Screen {
    public Element fromEditText;
    public Element toEditText;
    public Element subjectEditText;
    public Element emailBodyEditText;

    public GmailComposeScreen(AppiumDriver driver) {
        super(driver);

        fromEditText = new Element(driver, "com.google.android.gm:id/from_account_name");
        toEditText = new Element(driver, "com.google.android.gm:id/to");
        subjectEditText = new Element(driver, "com.google.android.gm:id/subject");
        emailBodyEditText = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.RelativeLayout[@resource-id='com.google.android.gm:id/body_wrapper']" +
                    "/android.widget.LinearLayout[@index='0']" +
                    "/android.webkit.WebView[@index='0']" +
                    "/android.webkit.WebView[@index='0']" +
                    "/android.view.View[@index='0']";

            @Override
            public void tap() {
                emailBodyEditText.tap.elementByXpath(xpath);
            }

            @Override
            public String getTextValue() {
                return emailBodyEditText.attribute.getTextValueByXpath(xpath);
            }
        };
    }

    public boolean isPresent() {
        return fromEditText.elementExists() && toEditText.elementExists();
    }
}
