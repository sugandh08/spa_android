package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.springframework.util.Assert;
import views.Screen;

public class EditEmergencyInformationScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public Element closeIcon;
    public Element saveButton;
    public Element phoneTypeDropDown;
    public Element emergencyFirstName;
    public Element emergencyLastName;
    public Element emergencyRelationship;
    public Element emergencyPhoneNumber;
    public Element emergencyEmail;


    public EditEmergencyInformationScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "title_title", "EDIT EMERGENCY INFORMATION");
        closeIcon = new Element(driver, "title_left_icon", "");
        saveButton = new Element(driver, "save_txt", "SAVE");
        phoneTypeDropDown = new Element(driver, "item_profile_phone_type_pickertext", "");
        emergencyFirstName = new Element(driver, "profile_emergency_firstname", "");
        emergencyLastName = new Element(driver, "profile_emergency_lastname", "");
        emergencyRelationship = new Element(driver, "profile_emergency_relationship", "");
        emergencyPhoneNumber = new Element(driver, "item_profile_phone_input_pickertext", "");
        emergencyEmail = new Element(driver, "profile_emergency_email", "");

    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("EDIT EMERGENCY INFORMATION");
    }

    private String getXpathOfEmergencyInformationTextFields(String textField)
    {
        return "/" +
                "/android.widget.TextView[@text='"+textField+"']" +
                "/.." +
                "/android.widget.EditText[@resource-id='com.stationdm.genesis:id/pickertext_content_txt']";
    }

    public  void updateEmergencyInformation(String firstName, String lastName, String relationship, String email)
    {
        String phoneNum= "1234567890";
        clearTextField.clearTextByXpath(getXpathOfEmergencyInformationTextFields("First Name*"));
        enterText.byXpath(getXpathOfEmergencyInformationTextFields("First Name*"),firstName);

        clearTextField.clearTextByXpath(getXpathOfEmergencyInformationTextFields("Last Name*"));
        enterText.byXpath(getXpathOfEmergencyInformationTextFields("Last Name*"),lastName);

        clearTextField.clearTextByXpath(getXpathOfEmergencyInformationTextFields("Relationship*"));
        enterText.byXpath(getXpathOfEmergencyInformationTextFields("Relationship*"),relationship);

        clearTextField.clearTextByXpath(getXpathOfEmergencyInformationTextFields("Mobile*"));
        enterText.byXpath(getXpathOfEmergencyInformationTextFields("Mobile*"), phoneNum);

        clearTextField.clearTextByXpath(getXpathOfEmergencyInformationTextFields("Email*"));
        enterText.byXpath(getXpathOfEmergencyInformationTextFields("Email*"),email);

        phoneTypeDropDown.tap();
        phoneTypeDropDown.tap();

        saveButton.tap();


    }
}
