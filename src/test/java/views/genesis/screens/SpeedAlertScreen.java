package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Speed Alert screen that is accessed through the Alert Settings screen.
 */
public class SpeedAlertScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element informationButton;

    public Element speedLimitButton;
    public Element speedLimitTextView;
    public Element saveButton;

    public Element cancelButton;
    public Element doneButton;

    public final String speedLimitWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/speed_wheel']";

    public SpeedAlertScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
        informationButton = new Element(driver,"title_right_icon");

        speedLimitButton = new Element(driver,"alert_speed_ll");
        speedLimitTextView = new Element(driver,"alert_speed_value_txt");
        saveButton = new Element(driver,"alert_speed_save_ll");

        cancelButton = new Element(driver,"cancel");
        doneButton = new Element(driver,"done");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("Speed Alert") && speedLimitTextView.elementExists();
    }
}
