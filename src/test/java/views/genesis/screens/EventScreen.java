package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class EventScreen extends Screen{
    public Element gotItButton;
    public Element viewEventDetailButton;

    public EventScreen(AppiumDriver driver){
        super(driver);

        gotItButton = new Element(driver,"iaetut_gotit_btn");
        viewEventDetailButton = new Element(driver,"iaetut_viewdetail_btn");
    }

    public void tapGotItIfThisPopupPresent(long waitTime){
        if(isPresent(waitTime)){
            gotItButton.tap();
        }
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (gotItButton.elementExists() & viewEventDetailButton.elementExists());
    }

    public boolean isPresent(long waitTime) {
        return (gotItButton.elementExists(waitTime) & viewEventDetailButton.elementExists(waitTime));
    }
}
