package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Remote Start settings screen that is accessible through the Home screen or
 * through the Remote Features screen.
 */  
public class RemoteStartSettingsScreen extends Screen{
    // Temperature isn't exposed

    public Element menuButton;
    public Element engineDurationTimeTextView;
    public Element temperatureToggleButton;
    public Element defrostToggleButton;
    public Element submitButton;
    public Element startButton;
    public Element stopButton;
    public Element lockButton; 
    public Element unlockButton;
    public Element hornLightButton;
    public Element renameButton;
    public Element presetNameText;
    public Element defaultPresetText;
    public Element defaultPresetToggle;
    public Element mirrordefrost;
    public Element seatdefrost;
    public Element frontdefrost;
    public Element seatDownImage;
    public Element heatedSeat_1;
    public Element ventSeat_1;
    public Element rearandsidemirrorsdefroster;
    public Element heatedSteeringWheel;
    public Element SUBMIT;
    public Element Temperature;
    public Element TempSign;
    public Element PresetStart;
    public Element RemoteStatus;

    // Xpaths for each of the scroll wheels because the interactions we have with them are
    // only done through GuiController, not here. The only thing we care about is using these Xpaths
    // to get the selenium Rectangle object of the scroll wheel.
    public final String ENGINE_DURATION_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/engine_wheel']";
    public final String TEMPERATURE_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/temperature_wheel']";

    public RemoteStartSettingsScreen(AppiumDriver driver)
    {
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        engineDurationTimeTextView = new Element(driver,"tv_remote_engine_setting_value");
        temperatureToggleButton = new Element(driver,"temperature_switch");
        defrostToggleButton = new Element(driver,"defrost_switch");
        submitButton = new Element(driver,"ll_save_container");
        startButton = new Element(driver,"home_remote_start_bar");
        stopButton = new Element(driver,"home_remote_stop_bar");
        lockButton = new Element(driver,"home_remote_lock_bar");
        unlockButton = new Element(driver,"home_remote_unlock_bar");
        hornLightButton = new Element(driver,"home_remote_lights_bar");
        presetNameText = new Element(driver, "etPresetName","");
        renameButton = new Element(driver, "tvRename","Rename");
        defaultPresetText = new Element(driver, "tvDefaultTitle","Default Preset");
        defaultPresetToggle = new Element(driver, "sbChangeDefault","");
        mirrordefrost = new Element(driver,"ivMirrorsDefroster");
        seatdefrost = new Element(driver,"ivSeatTemp");
        frontdefrost = new Element(driver,"ivFrontDefroster");
        seatDownImage=new Element(driver,"seats_down_img");
        heatedSeat_1=new Element(driver,"seat_heat_img");
        ventSeat_1=new Element(driver,"seat_vent_img");
        rearandsidemirrorsdefroster = new Element(driver,"mirrors_defroster_switch");
        heatedSteeringWheel = new Element(driver,"steering_wheel_switch");
        SUBMIT = new Element(driver,"ll_save_container");
        Temperature = new Element(driver,"tvTempValue");
        TempSign = new Element(driver,"tvTempSign");
        PresetStart = new Element(driver, "tvPresetStart");
        RemoteStatus = new Element(driver,"tvRemoteStatus");
    }

    /**
     * Sets the toggle states for Temperature and Defrost.
     * @param temperatureToggle State of Temperature
     * @param defrostToggle State of Defrost
     */
    public void setStartSettingsStates(boolean temperatureToggle, boolean defrostToggle){
        if(temperatureToggleButton.getCheckedValue() ^ temperatureToggle){
            temperatureToggleButton.tap();
        }

        if(defrostToggleButton.getCheckedValue() ^ defrostToggle){
            defrostToggleButton.tap();
        }
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (engineDurationTimeTextView.elementExists() && temperatureToggleButton.elementExists());
    }

    public void renamePresets(){
        String renameText = "TestRename";
        clearTextField.clearTextById("etPresetName");
        presetNameText.enterText(renameText);
        submitButton.tap();
    }

    public void setDefaultPresetState(boolean Toggle) {
        if (defaultPresetToggle.getCheckedValue() ^ Toggle) {
            defaultPresetToggle.tap();
            submitButton.tap();
        }
        else {
            submitButton.tap();
        }
    }

    public boolean getseattempstatus(String presetname)
    {
        return elementExists.byXpath("//*[@class='android.widget.RelativeLayout' and ./*[@id='ivSeatTemp'] and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[./*[@text='"+presetname+"']]]]");
    }

}
