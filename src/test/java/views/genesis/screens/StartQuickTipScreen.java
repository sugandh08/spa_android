package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Start Quick Tip screen that is reached through the Support screen.
 */
public class StartQuickTipScreen extends Screen{
    public Element exitButton;

    private Element screenTest;

    public StartQuickTipScreen(AppiumDriver driver){
        super(driver);

        exitButton = new Element(driver,"iaetut_exit_ll");

        screenTest = new Element(driver,"","Tap here to add events to your default calendar app.");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (screenTest.elementExists());
    }
}
