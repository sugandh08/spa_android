package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Tutorial Add Event screen that is reached through the Support screen.
 */
public class TutorialAddEventScreen extends Screen{
    public Element exitButton;
    public Element nextButton;
    public Element gotItButton;
    public Element addEventText;

    public TutorialAddEventScreen(AppiumDriver driver){
        super(driver);

        exitButton = new Element(driver,"iaetut_exit_ll");
        nextButton = new Element(driver,"iaetut_next_btn");
        gotItButton = new Element(driver,"iaetut_gotit_btn");
        addEventText = new Element(driver,"","Tap here to add events to your default calendar app. " +
                "Genesis app will seamlessly connect to Genesis Intelligence Assistant to prompt actions.");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (addEventText.elementExists());
    }
}
