package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import  tests.genesis_gen2Point5.remote.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SecondaryDriverInvitationScreen_Gen2Point5 extends Screen {

    public Element subTitleTextView;
    public Element driverFirstNameText;
    public Element driverLastNameText;
    public Element driverEmailText;
    public Element driverSendInvitationButton;
    public Element driverSelectFromContactsText;

    public RemoteTestsHelper remoteTestsHelper;

    public SecondaryDriverInvitationScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "title_title", "SECONDARY DRIVER INVITATION");
        driverFirstNameText = new Element(driver, "", "FIRST NAME");
        driverLastNameText = new Element(driver, "", "LAST NAME");
        driverEmailText=new Element(driver,"", "EMAIL");
        driverSendInvitationButton=new Element(driver,"", "SEND INVITATION");
         driverSelectFromContactsText=new Element(driver,"sd_invite_select_contacts", "Select From Contacts");
    }

    private String getXpathOfSecondaryDriverTextFields(String textField)
    {
        return "/" +
                "/android.widget.TextView[@text='"+textField+"']" +
                "/.." +
                "/android.widget.EditText[@resource-id='com.stationdm.genesis:id/pickertext_content_txt']";
    }


    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("SECONDARY DRIVER INVITATION");

    }

    public void sendAdditionalDriverInvite(String fName,String lName, String email) throws ParseException {

        driverFirstNameText.tap();

        enterText.byXpath(getXpathOfSecondaryDriverTextFields("First Name*"),fName);

        driverLastNameText.tap();
        enterText.byXpath(getXpathOfSecondaryDriverTextFields("Last Name*"),lName);

        driverEmailText.tap();

        enterText.byXpath(getXpathOfSecondaryDriverTextFields("Email*"),email);
         driverSendInvitationButton.tap();


    }
}