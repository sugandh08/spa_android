package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Terms & Conditions screen that is a view of the Terms & Conditions document and is
 * accessed by tapping to view the Terms & Conditions when logging into the app or through the Support screen.
 */
public class TermsAndConditionsScreen extends Screen {
    public Element back;
    public Element pageTitle;

    public TermsAndConditionsScreen(AppiumDriver driver) {
        super(driver);

        back = new Element(driver,"title_left_icon","");
        pageTitle = new Element(driver,"title_title");
    }

    public void swipeDown() {
        swipe.down(3);
    }

    public void swipeUp() {
        swipe.up(3);
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (pageTitle.getTextValue().equals("TERMS & CONDITIONS"));
    }
}
