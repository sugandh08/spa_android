package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Registered Devices screen that is accessible through the Settings screen, which is
 * accessible through the Side menu.
 */
public class RegisteredDevicesScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element registeredDevicesTitleTextView;
    public Element emailTextView;

    public RegisteredDevicesScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        registeredDevicesTitleTextView = new Element(driver, "", "REGISTERED DEVICES");
        emailTextView = new Element(driver, "setting_devices_email_txt");
    }

    public boolean isPresent() {
        return registeredDevicesTitleTextView.elementExists();
    }

    public boolean checkIfDeviceOnList(String deviceName) {
        return elementExists.byXpath("//android.widget.TextView[@text='" + deviceName + "']");
    }
}
