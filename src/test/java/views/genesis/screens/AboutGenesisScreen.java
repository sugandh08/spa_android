package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
 
/**
 * This is the About Genesis screen, which is mainly reached through the Side menu.
 */
public class AboutGenesisScreen extends Screen{
    public Element menuButton;
    public Element genesisOwnerBenefits;
    public Element visitGenesisMotors;
    public Element genesisAccessories;
    public Element genesisVirtualGuide;
    public Element genesisFAQ;
    public Element ownersManual; 
    public Element txtGeneralInfo;
    public Element headerAboutAndSupport;
    public Element headerGuides;
    public Element txtGettingStartedGuide;
    public Element txtIndicatorGuide;
    public Element txtMaintenanceInfo;
    public Element txtWarrantyInfo;
    public Element txtVirtualGuide;
    public Element headerContactUs;
    public Element txtEmailAppSupport;
    public Element txtConsumerAssistanceCentre;
    public Element txtFAQ;
    public Element txtTermsCondition;
    public Element txtPrivacyPolicy;
    public Element txtgenesisResources;
    public Element txtconnectedservices;
    public Element txtmygenesiscom;
    public Element emailappsupportpage;
    public Element incidentinformation;
    public Element termsandconditionpage;
    public Element txtGettingstarted;
    public Element txtVehicleHealth;
    public Element txtManualandWarranties;
    public Element txtBluetoothAssistance;
    public Element txtTechnologyandNavigation;
    public Element txtAccessoriesandParts;
    public Element getGenesisFAQ;
    public Element pagetitle;
    public Element txtGuides;
    public Element txtLinks;
    public Element txtContactus;
    public Element txtversion;
    public Element txtGenesisUSA;
    public Element txtGenesisCollissionCenter;
    public Element Vehicleincidentinformation;
    public Element backbuttonapp;
    public Element Vehicletext;
    public Element Submit;
    public Element previewicon;
    public Element VehicleVinEmailAppSupport;

    public AboutGenesisScreen(AppiumDriver driver){
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        genesisOwnerBenefits = new Element(driver,"benefits_txt"){
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        visitGenesisMotors = new Element(driver,"tv_visit"){
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        genesisAccessories = new Element(driver,"","Genesis Accessories");
        genesisVirtualGuide = new Element(driver,"tv_virtual_guide");
        genesisFAQ = new Element(driver,"","GENESIS FAQ");
        ownersManual = new Element(driver,"","Owner's Manual");
        txtGeneralInfo = new Element(driver, "", "General Information");
        headerAboutAndSupport = new Element(driver, "title_title", "ABOUT & SUPPORT");
        headerGuides = new Element(driver, "", "Guides");
        txtGettingStartedGuide = new Element(driver, "", "Getting Started Guide");
        txtIndicatorGuide = new Element(driver, "", "Indicator Guide");
        txtMaintenanceInfo = new Element(driver, "", "Maintenance information");
        txtWarrantyInfo = new Element(driver, "", "Warranty Info");
        txtVirtualGuide = new Element(driver, "", "Genesis Virtual Guide");
        headerContactUs =new Element(driver,"","Contact Us");
        txtEmailAppSupport =new Element(driver,"","Email App Support");
        txtConsumerAssistanceCentre =new Element(driver,"","Consumer Assistance Center");
        txtFAQ =new Element(driver,"","FAQ");
        txtTermsCondition =new Element(driver,"","Terms & Conditions");
        txtPrivacyPolicy =new Element(driver,"","Privacy Policy");
        txtgenesisResources = new Element(driver,"","Genesis Resources");
        txtconnectedservices = new Element(driver,"","Connected Services");
        txtmygenesiscom = new Element(driver,"","MyGenesis.com");
        emailappsupportpage = new Element(driver,"","EMAIL APP SUPPORT");
        incidentinformation = new Element(driver,"tv_email_title","INCIDENT INFORMATION");
        termsandconditionpage = new Element(driver,"toc_title","TERMS & CONDITIONS");
        getGenesisFAQ = new Element(driver,"title_title");
        pagetitle = new Element(driver,"title_title");
        txtGuides = new Element(driver,"","Guides");
        txtLinks = new Element(driver,"","Links");
        txtContactus = new Element(driver,"","Contact Us");
        txtversion = new Element(driver,"","Version 2.9.0 Prod");
        txtGenesisUSA = new Element(driver,"","Genesis USA");
        txtGenesisCollissionCenter = new Element(driver,"","Genesis Collision Center");
        txtGettingstarted = new Element(driver,"","Getting Started");
        txtVehicleHealth = new Element(driver,"","Vehicle Health");
        txtManualandWarranties = new Element(driver,"","Manuals & Warranties");
        txtBluetoothAssistance = new Element(driver,"","Bluetooth Assistance");
        txtTechnologyandNavigation = new Element(driver,"","Technology and Navigation");
        txtAccessoriesandParts = new Element(driver,"","Accessories and Parts");
        Vehicleincidentinformation = new Element(driver,"rl_vehicle_vin");
        backbuttonapp = new Element(driver,"title_left_icon");
        Vehicletext = new Element(driver,"text1");
        Submit = new Element(driver,"tv_email_submit");
        previewicon = new Element(driver,"sem_chooser_preview_icon");
        VehicleVinEmailAppSupport = new Element(driver,"tv_vehicle_vin");
        txtversion =new Element(driver,"","Version 2.9.6 Prod");

    }

    /**
     * Scroll to the top of the screen.
     */
    public void scrollToTop() {
        swipe.downCustomPercentage(4,.3,.3,.6);
    }

    /**
     * Scroll to the middle of the screen.
     */
    public void scrollFromTopToMiddle() {
        swipe.upCustomPercentage(2,.7,.3,.6);
    }

    /**
     * Scroll to the bottom of the screen.
     */
    public void scrollToBottom() {
        swipe.upCustomPercentage(4,.7,.3,.6);
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     * Note: the scrolling is only done in 3 blocks: top (default view), middle, and bottom. A more accurate
     * scrolling method might need to be built to work with all screen sizes and scaling.
     * @param element element to look for in this screen.
     */
    public void scrollToElementSection(Element element) {
        if (!element.elementExists()) {
            scrollFromTopToMiddle();
            if (!element.elementExists()) {
                scrollToBottom();
            }
        }
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (genesisOwnerBenefits.elementExists() && visitGenesisMotors.elementExists());
    }
    String txtOwnerManual = "//*[@text=concat('Owner', \"'\", 's Manual')]";
    String title="//*[@resource-id='com.stationdm.genesis:id/title_title']";

    public boolean isOwnersManualDisplayed()
    {
        return elementExists.byXpath(txtOwnerManual);
    }

    String GenesisVirtualApp ="//*[@resource-id='com.android.vending:id/0_resource_name_obfuscated' and @text='Genesis Virtual Guide']";

    String GenesisVirtual="//*[@text='Genesis Virtual Guide']";

    public void TapOwnerManual() { tap.elementByXpath(txtOwnerManual);}
    public boolean OwnerManualisPresent() { return elementExists.byXpath(title);}
    public void TapGenesisVirtualGuide() {tap.elementByXpath(GenesisVirtual);}
    public boolean GenesisVirtualGuideAPPisPresent() {return elementExists.byXpath(GenesisVirtualApp);}
    //String Submit ="//*[@class='android.widget.RelativeLayout' and @id='tv_email_submit']";
    //public void  Tapsubmit() { tap.elementByXpath(Submit);}

}
