package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the demo choose car screen that is accessed through the Guest option from the login page.
 * 2017 Genesis G80 is the Gen 2 demo.
 */
public class DemoChooseCarScreen extends Screen {
    public Element back;
    public Element genesisG80;
    public Element demoVIN;

    /**
     * DemoChooseCarScreen constructor
     * @param driver Appium driver
     */
    public DemoChooseCarScreen(AppiumDriver driver) {
        super(driver);

        back = new Element(driver,"title_left_icon","");
        genesisG80 = new Element(driver,"","GENESIS G80");
        demoVIN = new Element(driver,"","VIN: DEMO1");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (genesisG80.elementExists() && demoVIN.elementExists());
    }
}
