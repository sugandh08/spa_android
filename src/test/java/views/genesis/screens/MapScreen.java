package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.genesis.popups.DeleteConfirmationPopup;

/** 
 * This is the Map screen that is accessible through the Home screen.
 */
public class MapScreen extends Screen {
    public Element menuButton;
    public Element titleTextView;
    public Element searchEditText;
    public Element searchButton;
    public Element parkingMeterButton;
    public Element parkingMeterTextView;
    public Element searchList; 
    public Element poiSearchButton;
    public Element myPoiButton;
    public Element nearbyGasStationButton;
    public Element dealerLocatorButton;
    public Element carFinderButton;
    public Element favoritesTab; 
    public Element headerDealerLocator;
    public Element detailArrow;
    public Element setAsPreferredIcon;
    public Element dealerNameTxt;
    public Element gasstationname;
    public Element dealerLocation;
    public Element scheduleServiceBtn;
    public Element sendToCarBtn;
    public Element Cancel;
    public Element Call;
    public Element Callpopup;
    public Element ShopAccessories;
    public Element mapcurrentlocation;
    public Element POIsearchlist;
    public Element DoorsandTrunkClosed;
    public Element SVMbar;
<<<<<<< HEAD
    public Element nevigateLoacationBtn;
    public Element destinationCoornidates;
=======
    public Element AddPoi;
    public Element detailArrowPoi;
    public Element SendtoCar;
    public Element OpenFav;
    public Element FavItem;
    public Element DeletePoi;
    public Element NavMap;
    public Element MyPoiSearch;
>>>>>>> a40e2a6d1da6661a1b905cf283d688a78bb80ae9


    public MapScreen(AppiumDriver driver) {
        super(driver);

        menuButton = new Element(driver,"title_left_icon");

        // POI Search: "POI SEARCH"
        // My Favorites: "MY FAVORITES"
        // Gas Station: "GAS STATION"
        // Dealer Locator: "DEALER LOCATOR"
        // Car Finder: "CAR FINDER"
        titleTextView = new Element(driver,"title_title");

        // POI Search: "POI Search"
        // My Favorites: "My POI"
        // Gas Station: "Gas Station"
        // Dealer Locator: "Enter Zip"
        // Car Finder: no search box
        searchEditText = new Element(driver,"map_search_et","Gas Station");
        searchButton = new Element(driver,"map_search_icon");
        parkingMeterButton = new Element(driver,"map_meter_img");
        parkingMeterTextView = new Element(driver,"map_meter_txt");
        searchList = new Element(driver,"ivExpandCloseList");
        poiSearchButton = new Element(driver,"home_map_second_bar");
        myPoiButton = new Element(driver,"home_map_third_bar");
        nearbyGasStationButton = new Element(driver,"home_map_fourth_bar");
        dealerLocatorButton = new Element(driver,"home_map_fifth_bar");
        carFinderButton = new Element(driver,"home_map_sixth_bar");
        favoritesTab = new Element(driver, "","FAVORITES");
        headerDealerLocator = new Element(driver,"title_title","RETAILER LOCATOR");
        detailArrow = new Element(driver,"ivDetailArrow");
        detailArrowPoi = new Element(driver,"marker_detail_arrow");

        setAsPreferredIcon =new Element(driver,"marker_detail_middle");
        dealerNameTxt =new Element(driver,"marker_detail_title");

        setAsPreferredIcon =new Element(driver,"tvPreferredRetailer");
        dealerNameTxt =new Element(driver,"tvDetailTitle");

        gasstationname = new Element(driver,"marker_detail_title");
        dealerLocation =new Element(driver,"tvDetailAddress");
        scheduleServiceBtn =new Element(driver,"btnScheduleService");
        sendToCarBtn =new Element(driver,"ivSend2Car");
        Cancel = new Element(driver,"tvCancel");
        Call = new Element(driver,"tvDetailCall");
        Callpopup = new Element(driver,"button1","Call");
        ShopAccessories = new Element(driver,"marker_detail_accessories_txt");
        mapcurrentlocation = new Element(driver,"map_currentlocation_img");
        POIsearchlist = new Element(driver,"tv_placename");
        DoorsandTrunkClosed = new Element(driver,"svmDetail_ok_txt");
        SVMbar = new Element(driver,"home_map_svm_bar");
<<<<<<< HEAD
        nevigateLoacationBtn= new Element(driver,"tvDetailNav");
        destinationCoornidates= new Element(driver,"","34.0642660,-117.2568660");
=======
        AddPoi = new Element(driver,"", "Add POI");
        SendtoCar = new Element(driver,"marker_detail_send2car_btn");
        OpenFav = new Element(driver,"ivExpandCloseList");
        FavItem = new Element(driver,"", "90-001 Lodz Poland");
        DeletePoi = new Element(driver,"", "Delete POI");
        NavMap = new Element(driver,"marker_detail_navi");
        MyPoiSearch = new Element(driver,"map_search_et");
>>>>>>> a40e2a6d1da6661a1b905cf283d688a78bb80ae9
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (parkingMeterButton.elementExists() && poiSearchButton.elementExists() && nearbyGasStationButton.elementExists());
    }

    public void clickSearchKeyPadBtn()
    {
        tap.tapAtCoordinate(916,2046);
    }
}