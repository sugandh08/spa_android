package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.openqa.selenium.By;
import views.Screen;

public class EditPersonalInformationScreen_Gen2Point5 extends Screen {
    public Element subTitleTextView;
    public Element firstNameField;
    public Element lastNameField;
    public Element saveButton;
    public Element closeIcon;
    public Element suffixField;
    public Element prefixField;


    public EditPersonalInformationScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "title_title", "EDIT PERSONAL INFORMATION");
        firstNameField = new Element(driver, "", "First Name*");
        lastNameField = new Element(driver, "", "Last Name*");
        saveButton = new Element(driver, "save_txt", "SAVE");
        closeIcon = new Element(driver, "title_left_icon", "");
        suffixField = new Element(driver, "", "Suffix");
        prefixField = new Element(driver, "", "Prefix");

    }
    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("EDIT PERSONAL INFORMATION");

    }

    private String getXpathOfPersonalInformationTextFields(String textField)
    {
        return "/" +"/android.widget.TextView[@text='"+textField+"']"
                +
                "/.." +"/android.widget.EditText[@resource-id='com.stationdm.genesis:id/pickertext_content_txt']"
                  ;
    }

public void updatePersonalInformation(String firstName, String lastName)
{
    tap.elementByXpath(getXpathOfPersonalInformationTextFields("First Name*"));
     clearTextField.clearTextByXpath(getXpathOfPersonalInformationTextFields("First Name*"));
    enterText.byXpath(getXpathOfPersonalInformationTextFields("First Name*"),firstName);

    tap.elementByXpath(getXpathOfPersonalInformationTextFields("Last Name*"));
    clearTextField.clearTextByXpath(getXpathOfPersonalInformationTextFields("Last Name*"));
    enterText.byXpath(getXpathOfPersonalInformationTextFields("Last Name*"),lastName);

    suffixField.tap();
    suffixField.tap();
}
}
