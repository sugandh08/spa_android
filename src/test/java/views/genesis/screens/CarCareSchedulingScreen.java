package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Car Care Scheduling web site that is reached through the Home screen.
 */
public class CarCareSchedulingScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element imNewHere;
    public Element findMe;
    public Element logMeIn;

    public CarCareSchedulingScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "sub_title_txt");
        imNewHere = new Element(driver,"ext-element-28","I'm New Here");
        findMe = new Element(driver,"ext-element-31","Find Me");
        logMeIn = new Element(driver,"ext-element-33","Log Me In");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return titleTextView.elementExists() && titleTextView.getTextValue().equals("Car Care Scheduling");
    }
}
