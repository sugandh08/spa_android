package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Registration screen that is accessible through the Login screen.
 */
public class RegistrationScreen extends Screen{
    public Element backButton;
    public Element titleTextView;
    public Element firstNameTextView;
    public Element lastNameTextView;
    public Element emailTextView;
    public Element confirmEmailTextView;
    public Element passwordTextView;
    public Element confirmPasswordTextView;
    public Element zipCodeTextView;
    public Element nextButton;

    public RegistrationScreen(AppiumDriver driver){
        super(driver);

        titleTextView = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
        firstNameTextView = new Element(driver,"title_title");
        lastNameTextView = new Element(driver,"title_title");
        emailTextView = new Element(driver,"title_title");
        confirmEmailTextView = new Element(driver,"title_title");
        passwordTextView = new Element(driver,"title_title");
        confirmPasswordTextView = new Element(driver,"title_title");
        zipCodeTextView = new Element(driver,"title_title");
        nextButton = new Element(driver,"title_title");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (titleTextView.getTextValue().toLowerCase().equals("MYGENESIS REGISTRATION"));
    }
}
