package views.genesis.screens;

import io.appium.java_client.AppiumDriver;

import library.Element;

import views.Screen;




/**
 * This is the Remote Start settings screen that is accessible through the Home screen or
 * through the Remote Features screen.
 */
public class RemoteStartSettingsScreenGen2Point5 extends Screen{
    // Temperature isn't exposed

    public Element menuButton;
    public Element engineDurationTimeTextView;
    public Element temperatureToggleButton;
    public Element defrostToggleButton;
    public Element heatedFeaturesToggleButton;
    public Element seatsToggleButton;
    public Element submitButton;
    public Element startButton;
    public Element stopButton;
    public Element lockButton;
    public Element unlockButton;
    public Element hornLightButton;
    public Element seatDownImage;
    public Element heatedSeat_1;
    public Element ventSeat_1;
    public Element rearandsidemirrorsdefroster;
    public Element heatedSteeringWheel;


    // Xpaths for each of the scroll wheels because the interactions we have with them are
    // only done through GuiController, not here. The only thing we care about is using these Xpaths
    // to get the selenium Rectangle object of the scroll wheel.
    public final String ENGINE_DURATION_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/engine_wheel']";
    public final String TEMPERATURE_XPATH = "/" +
            "/android.view.View[@resource-id='com.stationdm.genesis:id/temperature_wheel']";





    public RemoteStartSettingsScreenGen2Point5(AppiumDriver driver)
    {
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        engineDurationTimeTextView = new Element(driver,"tv_remote_engine_setting_value");
        temperatureToggleButton = new Element(driver,"temperature_switch");
        defrostToggleButton = new Element(driver,"defrost_switch");
        heatedFeaturesToggleButton=new Element(driver,"heated_switch");
        seatsToggleButton=new Element(driver,"seat_switch");
        submitButton = new Element(driver,"ll_save_container");

        startButton = new Element(driver,"","Start");
        stopButton = new Element(driver,"","Stop");
        lockButton = new Element(driver,"","Lock");
        unlockButton = new Element(driver,"","Unlock");
        hornLightButton = new Element(driver,"","Horn and Lights");


        stopButton = new Element(driver," " , "Stop");
        lockButton = new Element(driver,"home_remote_lock_bar");
        unlockButton = new Element(driver,"home_remote_unlock_bar");
        hornLightButton = new Element(driver,"home_remote_lights_bar");

        seatDownImage=new Element(driver,"seats_down_img");
        heatedSeat_1=new Element(driver,"seat_heat_img");
        ventSeat_1=new Element(driver,"seat_vent_img");
        rearandsidemirrorsdefroster = new Element(driver,"", "FRONT DEFROSTER");
        heatedSteeringWheel = new Element(driver,"" , "SEATS");
    }

    /**
     * Sets the toggle states for Temperature and Defrost.
     * @param temperatureToggle State of Temperature
     * @param defrostToggle State of Defrost
     */
    public void setStartSettingsStates(boolean temperatureToggle, boolean defrostToggle){
        if(temperatureToggleButton.getCheckedValue() ^ temperatureToggle){
            temperatureToggleButton.tap();
        }

        if(defrostToggleButton.getCheckedValue() ^ defrostToggle){
            defrostToggleButton.tap();
        }
    }


    /**
     * Sets the toggle states for Temperature, Defrost, Heated Features and Seats
     * @param temperatureToggle State of Temperature
     * @param defrostToggle State of Defrost
     * @param Rearandsidetoggle State of Temperature
     * @param seatsToggle State of Defrost
     */
    public void setStartWithSeatsSettingsStates(boolean temperatureToggle, boolean defrostToggle,boolean Rearandsidetoggle, boolean seatsToggle){
        if(temperatureToggleButton.getCheckedValue() ^ temperatureToggle){
            temperatureToggleButton.tap();
        }

        if(defrostToggleButton.getCheckedValue() ^ defrostToggle){
            defrostToggleButton.tap();
        }
        if(rearandsidemirrorsdefroster.getCheckedValue() ^  Rearandsidetoggle){
            rearandsidemirrorsdefroster.tap();
        }

        if(seatsToggleButton.getCheckedValue() ^ seatsToggle){
            seatsToggleButton.tap();
        }
    }




    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (engineDurationTimeTextView.elementExists() && temperatureToggleButton.elementExists());
    }




}
