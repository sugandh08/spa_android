package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Request History screen that is accessible from the Remote Features screen by tapping the button
 * in the top right.
 */
public class RequestHistoryScreen extends Screen {
        public Element menuButton;
        public Element titleTextView;
        public  Element  remotehistorytop ;

    // root xpath
        private String mostRecentCommandXpathRoot = "/" +
                "/*[@resource-id='com.stationdm.genesis:id/request_history_list']" +
                "/android.widget.RelativeLayout[@index='0']" +
                "/android.widget.LinearLayout[@resource-id='com.stationdm.genesis:id/remote_history_container']" +
                "/android.widget.RelativeLayout[@resource-id='com.stationdm.genesis:id/remote_history_top']";
        // datetime xpaths
        private String mostRecentCommandDateTimeContainerXpath = mostRecentCommandXpathRoot +
                "/android.widget.LinearLayout[@resource-id='com.stationdm.genesis:id/ll_datetime']";
        private String mostRecentCommandDateTextXpath = mostRecentCommandDateTimeContainerXpath +
                "/android.widget.TextView[@resource-id='com.stationdm.genesis:id/tv_date']";
        private String mostRecentCommandTimeTextXpath = mostRecentCommandDateTimeContainerXpath +
                "/android.widget.TextView[@resource-id='com.stationdm.genesis:id/tv_time']";
        // status xpaths
        private String mostRecentCommandStatusContainerXpath = mostRecentCommandXpathRoot +
                "/android.widget.LinearLayout[@resource-id='com.stationdm.genesis:id/remote_status_ll']";
        private String mostRecentCommandStatusTextXpath = mostRecentCommandStatusContainerXpath +
                "/android.widget.TextView[@resource-id='com.stationdm.genesis:id/tv_status']";
        private String mostRecentCommandStatusIconXpath = mostRecentCommandStatusContainerXpath +
                "/android.widget.ImageView[@resource-id='com.stationdm.genesis:id/remote_status_icon']";

        public RequestHistoryScreen(AppiumDriver driver){
            super(driver);
            menuButton = new Element(driver,"title_left_icon");
            titleTextView = new Element(driver,"title_title");
            remotehistorytop = new Element(driver, "remote_history_top");
        }

        /**
         * Grabs the text value of the command name of the first entry in the list. If the element can't be found, returns
         * empty string.
         * @return Name of the most recent command, or empty string if not found.
         */
        public String getMostRecentCommandDate() {
            return elementExists.byXpath(mostRecentCommandDateTextXpath) ?
                    attribute.getTextValueByXpath(mostRecentCommandDateTextXpath) : "";
        }

        /**
         * Grabs the text value of the command name of the first entry in the list. If the element can't be found, returns
         * empty string.
         * @return Name of the most recent command, or empty string if not found.
         */
        public String getMostRecentCommandTime() {
            return elementExists.byXpath(mostRecentCommandTimeTextXpath) ?
                    attribute.getTextValueByXpath(mostRecentCommandTimeTextXpath) : "";
        }

        /**
         * Grabs the text value of the status of hte first entry in the list. If the element can't be found, returns
         * empty string. If the element is the failure icon, returns "Fail".
         * @return Status of the most recent command ("Fail" if it's a failed command), or empty string if not found.
         */
        public String getMostRecentCommandStatus() {
            if (elementExists.byXpath(mostRecentCommandStatusTextXpath)) {
                return attribute.getTextValueByXpath(mostRecentCommandStatusTextXpath);
            }
            else if (elementExists.byXpath(mostRecentCommandStatusIconXpath)) {
                // need a custom return because it shows an icon, not text, for some reason
                return "PendingOrFail";
            }
            else {
                // test case will fail, elements could not be found on screen
                return "";
            }
        }

        /**
         * Test if the screen is present.
         * @return Whether or not the screen is present
         */
        public boolean isPresent() {
            return isPresent(5);
        }

        /**
         * Test if the screen is present.
         * @param waitTime Time to wait for screen
         * @return Whether or not the screen is present
         */
        public boolean isPresent(long waitTime) {
            return (titleTextView.getTextValue(waitTime).equals("REQUEST HISTORY"));
        }
    }


