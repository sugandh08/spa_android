package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the MVR screen which is accessible through the Home screen.
 */
public class MonthlyVehicleReportScreen extends Screen {
    public Element backButton;
    public Element titleTextView;

    // MVR Tab
    public Element mvrTab;
    public Element vehicleNameTextView;
    public Element vinTextView;
    public Element mileageTextView;
    public Element reportDateTextView;
    public Element genesisAccessories;
    public Element actionStatusTextView;
    public Element scheduleServiceButton;
    public Element smartCruiseControlTextView;
    public Element electronicPowerBrakeTextView;
    public Element electronicallyControlledSuspensionTextView;
    public Element electronicPowerSteeringTextView;
    public Element laneDepartureWarningSystemTextView;
    public Element tirePressureMonitoringSystem;
    public Element transmissionTextView;
    public Element engineTextView;
    public Element airBagTextView;
    public Element antiLockBrakingSystemTextView;

    // History Tab
    public Element historyTab;
    // The container/parent view for each entry in the history list.
    public Element oldMvrFirstEntryContainerView;

    public Element oldMvrMonthTextView;
    public Element oldMvrDateTextView;

    // Diagnostic Tab
    public Element diagnosticTab;
    public Element systemStatus;

    public MonthlyVehicleReportScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");

        // Need to add functionality for items requiring action (i.e. engine), need drop down arrow reference
        // MVR Tab
        mvrTab = new Element(driver,"","MVR");
        vehicleNameTextView = new Element(driver,"genesis_title");
        vinTextView = new Element(driver,"genesis_vin");
        mileageTextView = new Element(driver,"odo_meter");
        reportDateTextView = new Element(driver,"genesis_report_date");
        genesisAccessories = new Element(driver,"mvr_header_accessories_txt");
        actionStatusTextView = new Element(driver,"tv_action_txt");
        scheduleServiceButton = new Element(driver,"ll_schedule_btn");
        smartCruiseControlTextView = new Element(driver,"","Smart Cruise Control");
        electronicPowerBrakeTextView = new Element(driver,"","Electronic Power Brake");
        electronicallyControlledSuspensionTextView = new Element(driver,"","Electronically Controlled Suspension");
        electronicPowerSteeringTextView = new Element(driver,"","Electronic Power Steering ");
        laneDepartureWarningSystemTextView = new Element(driver,"","Lane Departure Warning System");
        tirePressureMonitoringSystem = new Element(driver,"","Tire Pressure Monitoring System");
        transmissionTextView = new Element(driver,"","Transmission ");
        engineTextView = new Element(driver,"","Engine");
        airBagTextView = new Element(driver,"","Air Bag");
        antiLockBrakingSystemTextView = new Element(driver,"","Anti-Lock Braking System");

        // History Tab
        historyTab = new Element(driver,"","HISTORY");
        oldMvrFirstEntryContainerView = new Element(driver,"") {
            String xpath = "//*[@class='android.view.ViewGroup' and ./*[@text='01-03-2022']]";

            // override elementExists() method to use a specific xpath to the first entry in the list.
            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        oldMvrMonthTextView = new Element(driver, "iv_car_health_history_month");
        oldMvrDateTextView = new Element(driver, "tv_car_health_history_time");

        // Diagnostic Tab
        diagnosticTab = new Element(driver,"","DIAGNOSTIC");
        systemStatus = new Element(driver,"tv_no_data");
    }

    /**
     * Scroll to the top of the screen.
     */
    public void scrollToTop() {
        swipe.downCustomPercentage(4,.3,.3,.6);
    }

    /**
     * Scroll to the bottom of the screen.
     */
    public void scrollToBottom() {
        swipe.upCustomPercentage(4,.75,.3,.6);
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     * @param element element to look for in this screen.
     */
    public void scrollToElement(Element element) {
        int scrolls = 0;
        while(!element.elementExists(1) && scrolls < 5){
            scrolls++;
            scrollToBottom();
            System.out.println("\nNOTE: above element not found is likely not an error.\n");
        }
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (mvrTab.elementExists() && historyTab.elementExists());
    }
}