package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the menu screen that is accessible by pressing the hamburger icon (three horizontal bars)
 * at the top left of the app while in the home screen, or by swiping right from the left edge of the
 * app while in the home screen.
 * NOTE that this is also called the "Side" menu.
 */
public class MenuScreen extends Screen {
    public Element menuButton;
    public Element alertSettingsButton;
    public Element serviceHistoryToolButton;
    public Element accessoriesButton;
    public Element dealerLocatorButton;
    public Element roadsideAssistanceButton;
    public Element supportButton;
    public Element aboutGenesisButton;
    public Element settingsButton;
    public Element switchVehicleButton;
    public Element logoutButton;
    public Element profileButton;
    public Element profileNickname;
    public Element vin;
    public Element vinUnderProfile;
    public Element aboutAndSupport;
    public Element servicehistoryrecord;
    public Element Settingpage;
    public Element Quickstarttoggle;
    public Element backbutton;
    public Element Menubackheadderbutton;
    public Element ServiceValet;
    public Element alertSettings;


    //public Element

    /**
     * MenuScreen constructor
     * @param driver Appium driver
     */
    public MenuScreen(AppiumDriver driver) {
        super(driver);

        menuButton = new Element(driver, "menu_close_icon","");
        alertSettingsButton = new Element(driver,"","ALERT SETTINGS");
        serviceHistoryToolButton = new Element(driver,"","SERVICE HISTORY TOOL");
        accessoriesButton = new Element(driver,"","ACCESSORIES");
        dealerLocatorButton = new Element(driver,"","DEALER LOCATOR");
        roadsideAssistanceButton = new Element(driver,"","ROADSIDE ASSISTANCE");
        supportButton = new Element(driver,"","SUPPORT");
        aboutGenesisButton = new Element(driver,"","ABOUT GENESIS");
        settingsButton = new Element(driver, "", "SETTINGS");
        switchVehicleButton = new Element(driver,"","SWITCH VEHICLE");
        logoutButton = new Element(driver,"","LOGOUT");
        profileButton = new Element(driver,"menu_profile_ll","");
        profileNickname = new Element(driver,"menu_profile_nickname");
        vin = new Element(driver,"menu_profile_vin","KMHGN4JE9HU170195");
        vinUnderProfile =new Element(driver,"profile_vin_txt");
        aboutAndSupport =new Element(driver,"","ABOUT & SUPPORT");
        alertSettings =new Element(driver,"","ALERT SETTINGS");
        servicehistoryrecord = new Element(driver,"title_title","SERVICE HISTORY TOOL");
        Settingpage = new Element(driver,"","SETTINGS");
        Quickstarttoggle = new Element(driver,"sbEnableQuickStart");
        backbutton = new Element(driver,"back");
        Menubackheadderbutton = new Element(driver,"menu_header_image");
        ServiceValet = new Element(driver,"","Service Valet");
        serviceHistoryToolButton = new Element(driver,"menu_txt","SERVICE HISTORY TOOL");
    }

    /**
     * Scroll to bottom of the menuButton to make sure Switch Vehicle and/or Logout options are available.
     */
    public void scrollToBottom() {
        swipe.upCustomPercentage(1,.75,.3,.3);
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (alertSettingsButton.elementExists() && serviceHistoryToolButton.elementExists());
    }

    public boolean isAccessoriespageDisplayed()
    {
        return elementExists.byXpath("//*[@id='title_title' and @text='ACCESSORIES']");
    }
}
