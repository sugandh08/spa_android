package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;


/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */

public class ProfileScreen_Gen2Point5 extends Screen {

    public Element subTitleTextView;
    public Element profileTextView;
    public Element secondaryDriverTextView;
    public Element profilePrimaryText;
    public Element vehicleInformationText;
    public Element vehicleInformationNickNameText;
    public Element vehicleInformationEditButton;
    public Element myDriverSettingsButton;
    public Element connectedServicesPackageText;
    public Element profileInformationText;
    public Element profileFirstNameText;
    public Element profileLastNameText;
    public Element personalInformationEditButton;
    public Element contactInformationText;
    public Element contactInformationEditButton;
    public Element addressContactInformation;
    public Element emergencyContactInformationText;
    public Element emergencyContactInformationEditButton;
    public Element emergencyContactInformationFirstName;

    public ProfileScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "title_title", "PROFILE");
        profileTextView = new Element(driver, "", "Profile");
        secondaryDriverTextView = new Element(driver, "", "Secondary Drivers");
        profilePrimaryText = new Element(driver, "profile_name_primary_txt", "Primary Owner");
        vehicleInformationText = new Element(driver, "", "Vehicle Information");
        vehicleInformationEditButton = new Element(driver, "profile_vehicle_edit", "");
        myDriverSettingsButton = new Element(driver, "profile_profile_setting_txt", "My Driver Settings");
        connectedServicesPackageText = new Element(driver, "","Connected Services Package");
        profileInformationText = new Element(driver, "","Personal Information");
        profileFirstNameText = new Element(driver, "profile_first_name_txt","");
        profileLastNameText = new Element(driver, "profile_last_name_txt","");
        personalInformationEditButton = new Element(driver, "profile_personal_edit","");
        contactInformationText = new Element(driver, "","Contact Information");
        contactInformationEditButton = new Element(driver, "profile_contact_edit","");
        emergencyContactInformationText = new Element(driver, "","Emergency Contact Information");
        emergencyContactInformationEditButton = new Element(driver, "profile_emergency_edit","");
        vehicleInformationNickNameText= new Element(driver, "profile_nickname_txt","");
        addressContactInformation = new Element(driver, "profile_contact_address_txt","");
        emergencyContactInformationFirstName = new Element(driver, "profile_emergency_firstname_txt","");
    }


    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("Profile");

    }

    public void clickOnVehicleInformationEditButton()
    {
       if( vehicleInformationText.elementExists()) {
           vehicleInformationEditButton.tap();
       }
    }

}

