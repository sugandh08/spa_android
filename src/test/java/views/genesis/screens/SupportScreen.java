package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Support screen that is accessed through the Side menu.
 */
public class SupportScreen extends Screen {
    public Element menuButton;
    public Element titleTextView;
    public Element genesisResources;
    public Element userTutorial;
    public Element emailAppSupport;
    public Element callGenesisConnectedServices;
    public Element termsAndConditions;

    public SupportScreen(AppiumDriver driver) {
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        genesisResources = new Element(driver,"tv_genesis_resources","GENESIS RESOURCES") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        userTutorial = new Element(driver,"tv_tutorial","USER TUTORIAL");
        emailAppSupport = new Element(driver,"tv_email","EMAIL APP SUPPORT");
        callGenesisConnectedServices = new Element(driver,"tv_call","CALL GENESIS CONNECTED SERVICES");
        termsAndConditions = new Element(driver, "tv_terms","TERMS & CONDITIONS") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
    }

    /**
     * Scroll to the top of the screen.
     */
    public void scrollToTop() {
        swipe.downCustomPercentage(4,.3,.3,.6);
    }

    /**
     * Scroll to the middle of the screen.
     */
    public void scrollFromTopToMiddle() {
        swipe.upCustomPercentage(2,.7,.3,.6);
    }

    /**
     * Scroll to the bottom of the screen.
     */
    public void scrollToBottom() {
        swipe.upCustomPercentage(4,.7,.3,.6);
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     * Note: the scrolling is only done in 3 blocks: top (default view), middle, and bottom. A more accurate
     * scrolling method might need to be built to work with all screen sizes and scaling.
     * @param element element to look for in this screen.
     */
    public void scrollToElementSection(Element element) {
        if (!element.elementExists()) {
            scrollFromTopToMiddle();
            if (!element.elementExists()) {
                scrollToBottom();
            }
        }
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (titleTextView.getTextValue().equals("SUPPORT"));
    }
}