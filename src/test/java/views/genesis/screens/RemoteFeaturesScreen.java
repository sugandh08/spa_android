package views.genesis.screens;
 
import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Remote Features screen that is accessible through the Home screen.
 */
public class RemoteFeaturesScreen extends Screen{
    public Element menuButton;
    public Element titleTextView;
    public Element requestHistoryButton;
    public Element lastUpdateDateTextView;
    public Element refreshButton;
    public Element refreshOverlayButton;
    public Element engineStatus;
    public Element doorsStatus;
    public Element climateStatus;
    public Element frontDefrostStatus;
    public Element startButton;
    public Element stopButton;
    public Element lockButton;
    public Element unlockButton; 
    public Element hornLightOptionsButton;
    public Element requestStatusTextView;

    // Can only access after tapping the hornLightOptionsButton;
    public Element flashLightsButton;
    public Element hornAndLightsButton;


    public RemoteFeaturesScreen(AppiumDriver driver){
        super(driver);

        menuButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
        requestHistoryButton = new Element(driver,"title_right_icon");
        lastUpdateDateTextView = new Element(driver,"tv_update_lable");
        refreshButton = new Element(driver,"iv_refresh_button");
        refreshOverlayButton = new Element(driver,"iv_holo_update");
        engineStatus = new Element(driver,"vehicle_engine_txt");
        doorsStatus = new Element(driver,"vehicle_door_txt");
        climateStatus = new Element(driver,"vehicle_climate_txt");
        frontDefrostStatus = new Element(driver,"vehicle_front_defrost_txt");
        startButton = new Element(driver,"home_remote_start_bar");
        stopButton = new Element(driver,"home_remote_stop_bar");
        lockButton = new Element(driver,"home_remote_lock_bar");
        unlockButton = new Element(driver,"home_remote_unlock_bar");
        hornLightOptionsButton = new Element(driver,"home_remote_lights_bar");
        flashLightsButton = new Element(driver,"ll_light_switch");
        hornAndLightsButton = new Element(driver,"ll_horn_switch");
        requestStatusTextView = new Element(driver,"ripple_sending_txt");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (titleTextView.getTextValue().toLowerCase().equals("REMOTE FEATURES"));
    }
}
