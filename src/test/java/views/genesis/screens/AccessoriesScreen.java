package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Genesis Accessories web site that is reached through the About Genesis screen, Home screen,
 * Side Menu, and Monthly Vehicle Report screen.
 */
public class AccessoriesScreen extends Screen {

    public Element backButton;
    public Element titleTextView;
    public Element webPreviousImageView;
    public Element webNextImageView;
    public Element webRefreshImageView;
    public Element webStopImageView;

    public AccessoriesScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        webPreviousImageView = new Element(driver,"iv_webview_previous");
        webNextImageView = new Element(driver,"iv_webview_next");
        webRefreshImageView = new Element(driver,"iv_webview_refresh");
        webStopImageView = new Element(driver,"iv_webview_stop");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time to wait for screen
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return titleTextView.getTextValue(waitTime).equals("ACCESSORIES");
    }
}
