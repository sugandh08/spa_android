package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
 
public class RemotePresetsScreen extends Screen {
    public Element subTitleTextView;
    public Element remoteStartWithoutPresetsButton;
    public Element remoteStartWithoutPresetsText;

    public RemotePresetsScreen(AppiumDriver driver) {
        super(driver);
        subTitleTextView= new Element(driver, "","Remotely start your vehicle with a preset below, or edit settings to your preference.");
        remoteStartWithoutPresetsButton = new Element(driver, "clStartWithoutPreset","");
        remoteStartWithoutPresetsText = new Element(driver, "","Start Vehicle Without Presets");
    }

    public boolean isPresent() {
        return subTitleTextView.elementExists() && remoteStartWithoutPresetsText.getTextValue().equals("Start vehicle without presets");
    }
 
    public boolean isRemoteStartPresetsPresent(int Index){
        return  elementExists.byXpath("(//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup'])["+Index+"]");
    }

    public void editRemotePresets(int Index){
        tap.elementByXpath("//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/ivEditPreset']"); }

    public void clickOnPresetStartButton(int Index){
        tap.elementByXpath("//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/tvPresetStart']");
    }

    public String xpathOfPresetsName(int Index){
        return"//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/tvPresetName']";
    }

    public String xpathOfFrontDefrosterIcon(int Index){
        return "(//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/ivFrontDefroster'])";
    }

    public String xpathOfHeatedFeaturesIcon(int Index){
        return "(//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/ivMirrorsDefroster'])";
    }

    public String xpathOfTemperatureValue(int Index){
        return  "(//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/tvTempValue'])";
    }

    public String xpathOfSeatdefrosticon(int Index){
        return "(//*[@resource-id='com.stationdm.genesis:id/rvPresettingList']//*[@class='android.view.ViewGroup']["+Index+"]//*[@resource-id='com.stationdm.genesis:id/ivSeatTemp'])";
    }

    public boolean getPresetsSettingsStates(boolean temperatureState, boolean frontDefrosterState, int Index) {

        if(attribute.getSelectedValueByXpath(xpathOfTemperatureValue(Index))==temperatureState){
            return true;
        }

        if(attribute.getSelectedValueByXpath(xpathOfFrontDefrosterIcon(Index))==frontDefrosterState){
            return true;
        }
        else
            return false;
    }
    public boolean getPresetsSettingsState(boolean temperatureState, boolean frontDefrosterState, boolean seatState,boolean mirrorState,int Index) {

        if(attribute.getSelectedValueByXpath(xpathOfTemperatureValue(Index))==temperatureState){
            return true;
        }

        if(attribute.getSelectedValueByXpath(xpathOfFrontDefrosterIcon(Index))==frontDefrosterState){
            return true;
        }
        if(attribute.getSelectedValueByXpath(xpathOfSeatdefrosticon(Index))==seatState){
            return true;
        }
        if (attribute.getSelectedValueByXpath(xpathOfHeatedFeaturesIcon(Index))==mirrorState){
            return true;
        }
        else
            return false;
    }
}
