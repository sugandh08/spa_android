package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class EditVehicleNickNameScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public Element nickNameField;
    public Element saveButton;
    public Element closeIcon;

    public EditVehicleNickNameScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "title_title", "EDIT VEHICLE NICKNAME");
        nickNameField = new Element(driver, "nickname_edit", "");
        saveButton = new Element(driver, "save_txt", "SAVE");
        closeIcon = new Element(driver, "title_left_icon", "");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("EDIT VEHICLE NICKNAME");

    }

}
