package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Select Vehicle screen that is accessed by logging into an account with 2+ vehicles on it.
 * This screen should not show if the account only has 1 vehicle attached.
 */
public class SelectVehicleScreen extends Screen {
    public Element backButton;
    public Element titleTextView;
    public Element usernameTextView;
    public Element emailTextView;
    public Element addVehicleButton;

    /**
     * SelectVehicleScreen constructor
     * @param driver
     */
    public SelectVehicleScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver, "title_title");
        usernameTextView = new Element(driver,"vehicle_select_name");
        emailTextView = new Element(driver,"vehicle_select_email");
        addVehicleButton = new Element(driver,"vehicle_select_itemadd_ll");
    }

    public void tapVehicleButtonByVin(String vin) {
        tap.elementByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']");
    }

    public void tapVehicleButtonByVinIfThisScreenIsPresent(String vin) {
        tapVehicleButtonByVinIfThisScreenIsPresent(vin,5);
    }

    public void tapVehicleButtonByVinIfThisScreenIsPresent(String vin, long screenLoadWaitTime) {
        if (isPresent(screenLoadWaitTime)) {
            scrollRightIfVinNotPresent(vin);
            tapVehicleButtonByVin(vin);
        }else {
            System.out.println("NOTE: Above 'element not found' message is not an error if there is only 1 car on account.");
        }
    }

    public boolean checkIfVinIsPresent(String vin) {
        return elementExists.byXpath("//android.widget.TextView[@text='VIN: " + vin + "']");
    }

    public String getPackageTextByVin(String vin) {
        return attribute.getTextValueByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']" +
                "/.." +
                "/android.widget.RelativeLayout[@index='1']" +
                "/android.widget.TextView[@resource-id='com.stationdm.genesis:id/vehicle_select_item_pkgnames']");
    }

    public String getModelTextByVin(String vin) {
        return attribute.getTextValueByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']" +
                "/.." +
                "/.." +
                "/.." +
                "/android.widget.LinearLayout[@index='1']" +
                "/android.widget.TextView[@resource-id='com.stationdm.genesis:id/vehicle_select_item_mode']");
    }

    public String getNicknameTextByVin(String vin) {
        return attribute.getTextValueByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']" +
                "/.." +
                "/.." +
                "/.." +
                "/android.widget.LinearLayout[@index='1']" +
                "/android.widget.TextView[@resource-id='com.stationdm.genesis:id/vehicle_select_item_nickname']");
    }

    /**
     * Checks if the selected VIN is present. If not, scrolls screen to right.
     * @param vin VIN to check.
     */
    public void scrollRightIfVinNotPresent(String vin) {
        if (!checkIfVinIsPresent(vin)) {
            swipe.fromRightEdge(1);
        }
    }

    /**
     * Checks a string to see if it matches the package settings for a car.
     * @param packageText Text to check for packages
     * @param hasConnectedCare Whether it should contain Connected Care
     * @param hasRemote Whether it should contain Remote
     * @param hasGuidance Whether it should contain Guidance
     * @return False if text contains a package it shouldn't, or if it doesn't contain a package it should. True otherwise.
     */
    public boolean checkIfPackageTextIsCorrect(String packageText, boolean hasConnectedCare, boolean hasRemote, boolean hasGuidance) {
        return ((packageText.contains("Connected Care") == hasConnectedCare) &&
                (packageText.contains("Remote") == hasRemote) &&
                (packageText.contains("Guidance") == hasGuidance));
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return (titleTextView.getTextValue(waitTime).equals("SELECT VEHICLE") &&
                usernameTextView.elementExists() &&
                emailTextView.elementExists());
    }
}