package views.genesis.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Genesis Motors USA web site accessed through the About Genesis screen.
 */
public class GenesisMotorsUSAScreen extends Screen{
    public Element backButton;
    public Element titleTextView;

    public GenesisMotorsUSAScreen(AppiumDriver driver){
        super(driver);

        backButton = new Element(driver,"title_left_icon");
        titleTextView = new Element(driver,"title_title");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (titleTextView.getTextValue().equals("GENESIS MOTORS USA"));
    }
}
