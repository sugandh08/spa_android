package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when a parking meter is active, when the reminder time is reached.
 */
public class ParkingMeterTimerAboutToExpirePopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public ParkingMeterTimerAboutToExpirePopup(AppiumDriver driver) {
        super(driver);

        // The expected message needs to be modified to include time
        // Your parking meter is about to expire in 2 minutes.
        messageTextView = new Element(driver,"android:id/message");
        okButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists. Uses standard wait time
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().contains("Your parking meter is about to expire in");
    }

    /**
     * Test if the popup exists.
     * @param waitTime Time (in seconds) to implicitly wait.
     * @return Whether or not the popup is present
     */
    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).contains("Your parking meter is about to expire in");
    }
}
