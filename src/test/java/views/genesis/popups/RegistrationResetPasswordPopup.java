package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when submitting an already registered email in the Registration screen
 * and tap Forget Password on the RegistrationEmailAlreadyRegisteredPopup.
 */
public class RegistrationResetPasswordPopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element goButton;

    public RegistrationResetPasswordPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        cancelButton = new Element(driver, "android:id/button2");
        goButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().contains("Please Go To Genesis.com To Reset You Password");
    }
}
