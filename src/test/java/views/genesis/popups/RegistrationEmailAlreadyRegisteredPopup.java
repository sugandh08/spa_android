package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when submitting an already registered email in the Registration screen.
 */
public class RegistrationEmailAlreadyRegisteredPopup extends Screen {
    public Element messageTextView;
    public Element forgotPasswordButton;
    public Element cancelButton;
    public Element loginButton;

    public RegistrationEmailAlreadyRegisteredPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        forgotPasswordButton = new Element(driver, "android:id/button3");
        cancelButton = new Element(driver, "android:id/button2");
        loginButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().contains("This Email Is Already Registered");
    }
}
