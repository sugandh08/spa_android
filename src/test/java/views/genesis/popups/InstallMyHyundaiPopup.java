package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when you log in to a Hyundai vehicle and don't have the MyHyundai app installed.
 */
public class InstallMyHyundaiPopup extends Screen{
    public Element messageTextView;
    public Element closeButton;
    public Element installButton;

    public InstallMyHyundaiPopup(AppiumDriver driver){
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        closeButton = new Element(driver,"android:id/button2");
        installButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equalsIgnoreCase("Your vehicle is not supported by the Genesis App. " +
                "Would you like to install the MyHyundai app for your vehicle?");
    }
}
