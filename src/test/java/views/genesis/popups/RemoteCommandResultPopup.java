package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that is used for the result of all remote_genesis_old commands. Can sometimes take up to 3
 * minutes to show up (wicked fast!)
 */
public class RemoteCommandResultPopup extends Screen {
    public Element messageTextView;
    public Element okButton;
    public Element message;

    public RemoteCommandResultPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
        message = new Element(driver,"message");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).contains("Your request for Remote");
    }
    public String popUpMessage ="///android.widget.TextView[contains(@resource-id,'android:id/message')]";
    public boolean ispopUpMessageDisplayed() {return elementExists.byXpath(popUpMessage);}
}
