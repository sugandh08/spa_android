package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows along the bottom of the screen when the app tries to redirect to an email
 * app. It gives the selection of different apps to use to send the message.
 * We only care about the Gmail button, which will only show if Gmail is installed and set up on the phone.
 * Pressing the Gmail button opens the Gmail app.
 */
public class AndroidEmailPopup extends Screen {
    public Element titleTextView;
    public Element gmailButton;

    public AndroidEmailPopup(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver, "android:id/title");
        gmailButton = new Element(driver, "", "Gmail");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equalsIgnoreCase("Choose an Email client :");
    }
}
