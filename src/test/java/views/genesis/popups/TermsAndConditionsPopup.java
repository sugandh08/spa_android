package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows after selecting a vehicle from the vehicle select screen after logging in to the app.
 */
public class TermsAndConditionsPopup extends Screen {
    public Element messageTextView;
    public Element viewButton;
    public Element acceptButton;
    public Element exitButton;
    public Element allowbutton;
    public Element denybutton;
    public Element covidDismissButton;

    /**
     * TermsAndConditionsPopup constructor
     * @param driver Appium driver
     */
    public TermsAndConditionsPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        viewButton = new Element(driver,"android:id/button2");
        acceptButton = new Element(driver,"","Accept");
        exitButton = new Element(driver, "tvClose");
        allowbutton = new Element(driver,"permission_allow_button");
        denybutton = new Element(driver,"permission_deny_button");
        covidDismissButton = new Element(driver,"","Dismiss");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equalsIgnoreCase("Do you agree to the Genesis Terms and Conditions?");
     }

     public String Cancel = "//*[@resource-id='android:id/button2']";
     public void TapCancel()
     {
        tap.elementByXpath(Cancel);
     }

}
