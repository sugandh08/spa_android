package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when you select the Roadside Assistance option in the side menu.
 */
public class CallRoadsideAssistancePopup extends Screen {
    public Element alertTitleTextView;
    public Element messageTextView;
    public Element cancelButton;
    public Element callButton;

    /**
     * CallRoadsideAssistancePopup constructor
     * @param driver Appium driver
     */
    public CallRoadsideAssistancePopup(AppiumDriver driver) {
        super(driver);

        alertTitleTextView = new Element(driver, "com.stationdm.genesis:id/alertTitle");
        messageTextView = new Element(driver,"android:id/message");
        cancelButton = new Element(driver,"android:id/button2");
        callButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Would you like to exit the app and call Roadside Assistance?");
    }
}
