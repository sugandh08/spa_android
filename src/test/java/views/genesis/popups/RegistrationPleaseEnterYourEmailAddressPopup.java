package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when first entering the Registration screen.
 */
public class RegistrationPleaseEnterYourEmailAddressPopup extends Screen{
    public Element messageTextView;
    public Element editText;
    public Element cancelButton;
    public Element submitButton;

    public RegistrationPleaseEnterYourEmailAddressPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"alertTitle");
        editText = new Element(driver,"dialog_input_edit");
        cancelButton = new Element(driver,"android:id/button2");
        submitButton = new Element(driver,"android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().contains("Please Enter Your Email Address");
    }
}
