package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when you select the Call Genesis Connected Services option in the Support screen.
 */
public class CallGenesisConnectedServicesPopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element callButton;

    /**
     * CallRoadsideAssistancePopup constructor
     * @param driver Appium driver
     */
    public CallGenesisConnectedServicesPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        cancelButton = new Element(driver,"android:id/button2");
        callButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Would you like to contact Genesis Connected Services?");
    }
}
