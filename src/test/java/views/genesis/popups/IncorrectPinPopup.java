package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the incorrect PIN popup that shows when entering an incorrect PIN during a remote_genesis_old command
 * or other common command. When using this, make sure there is no specialized incorrect PIN popup
 * that is specific to the test. If there is, use the specialized popup instead.
 */
public class IncorrectPinPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public IncorrectPinPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time to wait for screen
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).contains("Incorrect PIN,");
    }
}
