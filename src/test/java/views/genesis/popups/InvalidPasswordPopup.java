package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when entering an invalid username or password in the login page.
 */
public class InvalidPasswordPopup extends Screen {
    public Element messageTextView;
    public Element resetPasswordButton;
    public Element okButton;

    /**
     * InvalidPasswordPopup constructor
     * @param driver Appium driver
     */
    public InvalidPasswordPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        resetPasswordButton = new Element(driver, "android:id/button2");
        okButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Incorrect username or password [IDM_401_1]");
    }
}
