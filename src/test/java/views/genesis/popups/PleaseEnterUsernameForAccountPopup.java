package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when entering an invalid username in the login page.
 */
public class PleaseEnterUsernameForAccountPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    /**
     * PleaseEnterUsernamePopup constructor
     * @param driver Appium driver
     */
    public PleaseEnterUsernameForAccountPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        okButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Please enter your email used for your account.");
    }
}
