package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class DeleteConfirmationPopup extends Screen {

    public Element messageTextView;
    public Element okButton;
    public Element cancelButton;
    public Element deleteButton;

    public DeleteConfirmationPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "message");
        okButton = new Element(driver, "button1","Delete");
        cancelButton = new Element(driver, "button2","Cancel");
        deleteButton = new Element(driver, "button1","Delete");
    }

         public String Deletebutton = "(//android.widget.Button[@text='Delete'])";
    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.elementExists() && messageTextView.getTextValue().contains("want to delete");  }

    public void clickOnDeleteButton(){
        tap.elementByXpath(Deletebutton);
    }
}


