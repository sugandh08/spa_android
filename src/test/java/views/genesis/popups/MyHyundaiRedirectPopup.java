package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * Popup that shows when logging in to an account with only Hyundai vehicles, or when selecting a
 * Hyundai vehicle in the vehicle selection page.
 */
public class MyHyundaiRedirectPopup extends Screen {
    public Element messageTextView;
    public Element closeButton;
    public Element openButton;

    /**
     * MyHyundaiRedirectPopup
     * @param driver Appium driver
     */
    public MyHyundaiRedirectPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        closeButton = new Element(driver,"android:id/button2");
        openButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {

               return messageTextView.getTextValue().equals("Your vehicle is not supported by the Genesis App. Would you like to install the MyHyundai app for your vehicle?");
    }
}
