package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when launching Google Play for the first time on an account, or
 * if the account never accepted the Google Play TOS. Best to make sure any test accounts have this
 * setup properly beforehand so that this popup never shows.
 */
public class GooglePlayTosPopup extends Screen {
    public Element titleTextView;
    public Element declineButton;
    public Element acceptButton;

    public GooglePlayTosPopup(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver, "com.android.vending:id/instant_apps_tos_title");
        declineButton = new Element(driver, "com.android.vending:id/negative_button");
        acceptButton = new Element(driver, "com.android.vending:id/positive_button");
    }

    public boolean isPresent() {
        return titleTextView.getTextValue().equals("Terms of Service");
    }
}
