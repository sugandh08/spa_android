package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when using the Car Finder feature from more than 1 mile away from
 * the vehicle.
 */
public class OutOfRangePopup extends Screen {
    public Element titleTextView;
    public Element messageTextView;
    public Element cancelButton;
    public Element dropPinButton;

    public OutOfRangePopup(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver, "alertTitle");
        messageTextView = new Element(driver, "android:id/message");
        cancelButton = new Element(driver, "android:id/button2");
        dropPinButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).equals("Your vehicle is out of 1 mile range. You must be within 1 mile radius to locate vehicle.");
    }
}
