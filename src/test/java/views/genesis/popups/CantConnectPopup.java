package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class CantConnectPopup extends Screen{
    public Element closeButton;

    public CantConnectPopup(AppiumDriver driver){
        super(driver);

        closeButton = new Element(driver,"button1");
    }

    public void tapCloseIfThisPopupPresent(long waitTime){
        if(isPresent(waitTime)){
            closeButton.tap();
        }
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return closeButton.elementExists();
    }

    public boolean isPresent(long waitTime) {
        return closeButton.elementExists(waitTime);
    }
}
