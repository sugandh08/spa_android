package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the incorrect PIN popup that shows when user enters an incorrect PIN in the
 * Car Finder feature.
 */
public class IncorrectPinCarFinderPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public IncorrectPinCarFinderPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button2");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().contains("Incorrect PIN, ");
    }
}
