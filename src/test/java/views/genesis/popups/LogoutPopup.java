package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when logging out of an account.
 */
public class LogoutPopup extends Screen
{
    public Element messageTextView;
    public Element cancelButton;
    public Element okButton;

    /**
     * LogoutPopup constructor
     * @param driver Appium driver
     */
    public LogoutPopup(AppiumDriver driver)
    {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        cancelButton = new Element(driver,"android:id/button2");
        okButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent()
    {
        return messageTextView.getTextValue().equals("Logging out will delete any saved credentials and will require password to be re-entered. Are you sure you want to continue?");
    }
}
