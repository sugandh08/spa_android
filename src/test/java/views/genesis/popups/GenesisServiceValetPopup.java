package views.genesis.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when reaching the home screen for the first time on an account.
 * Will probably not last for too many versions, as it is just highlighting a new feature for
 * this version.
 */
public class GenesisServiceValetPopup extends Screen {
    public Element gotItButton;

    public GenesisServiceValetPopup(AppiumDriver driver) {
        super(driver);

        gotItButton = new Element(driver, "redcap_preview_gotit_btn");
    }

    public void tapGotItIfThisPopupPresent(long waitTime){
        if(isPresent(waitTime)){
            gotItButton.tap();
        }
    }

    public boolean isPresent() {
        return gotItButton.elementExists();
    }

    public boolean isPresent(long waitTime) {
        return gotItButton.elementExists(waitTime);
    }
}
