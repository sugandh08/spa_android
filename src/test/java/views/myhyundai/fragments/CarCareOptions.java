package views.myhyundai.fragments;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.myhyundai.baseviews.HomeScreen;

/**
 * Holds accessible sub elements for the car care button element, which is accessible from the home page.
 */
public class CarCareOptions extends Screen {
    public Element closeButton;
    public HomeScreen homeScreen;
    public Element diagnostics;


    /**
     * CarCareOptions constructor
     * @param driver Appium driver
     */
    public CarCareOptions(AppiumDriver driver) {
        super(driver);
        closeButton = new Element(driver,"closeButton","");
        homeScreen= new HomeScreen(driver);
        diagnostics = new Element(driver,"buttonName","Diagnostics");

    }

    /**
     * Test if the options exist.
     * @return Whether or not the options are present
     */
    public boolean isPresent() {
        return closeButton.elementExists();
    }

    public boolean tapOnCarCareOptionButton(String carCareOptionName)
    {
        return tap.elementByXpath("//android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/buttonIconLayout' and (./preceding-sibling::* | ./following-sibling::*)[@text='"+ carCareOptionName +"']]");
    }

    public void clickOnDiagnosticsButton()
    {
        homeScreen.clickOnCarCareButton();
        tapOnCarCareOptionButton("Diagnostics");
    }

    public void clickOnMonthlyReportButton()
    {
        homeScreen.clickOnCarCareButton();
        tapOnCarCareOptionButton("Monthly Report");
    }

    public void clickOnScheduleServiceButton()
    {
        homeScreen.clickOnCarCareButton();
        tapOnCarCareOptionButton("Schedule Service");
    }

    public void clickOnBluetoothButton()
    {
        homeScreen.clickOnCarCareButton();
        tapOnCarCareOptionButton("Bluetooth");
    }



    private String dealerServiceOffersXpath= "//android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/buttonIconLayout'"
            +" and (./preceding-sibling::* | ./following-sibling::*)[@text='Dealer Service Offers']]";

    private String OwnersManualXpath= "(//androidx.recyclerview.widget.RecyclerView[@resource-id='com.stationdm.bluelink:id/actionButtonList']"
            +"/*/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/buttonName'])[6]";

    public boolean dealerServiceOffersIsDisplayed()
    {
      return elementExists.byXpath(dealerServiceOffersXpath);
    }
private String dealerServiceOfferIcon="//*[@id='buttonIcon' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='Dealer Service Offers']]]";

    public void TapDealerServiceOffer()
    {
        tap.elementByXpath(dealerServiceOffersXpath);
    }
    private String xpathDealerServicePage ="//*[@text='Service Offers' and @class='left-content-text']";
    public boolean ServiceOfferPageisPresent()
    {
        return elementExists.byXpath(xpathDealerServicePage);
    }


    public boolean ownersManualIsDisplayed()
    {
        return elementExists.byXpath(OwnersManualXpath);
    }
}
