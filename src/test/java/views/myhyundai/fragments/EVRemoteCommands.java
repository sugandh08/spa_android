package views.myhyundai.fragments;

import io.appium.java_client.AppiumDriver;
import library.Element;

/**
 * Holds accessible sub elements for the homeButton element. This class is
 * only to be used by PHEV and AEEV vehicles.
 */
public class EVRemoteCommands {
    public Element unlockButton;
    public Element lockButton;
    public Element vehicleStatusButton;
    public Element climateControlButton;
    public Element hornAndLightsButton;
    public Element lightsButton;

    /**
     * HomeOptions constructor
     * @param driver Appium driver
     */
    public EVRemoteCommands(AppiumDriver driver) {
        unlockButton = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.FrameLayout[@resource-id='android:id/content']" +
                    "/android.widget.FrameLayout[@index='4']";

            @Override
            public void tap() {
                tap.elementByXpath(xpath);
            }

            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        lockButton = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.FrameLayout[@resource-id='android:id/content']" +
                    "/android.widget.FrameLayout[@index='1']";

            @Override
            public void tap() {
                tap.elementByXpath(xpath);
            }

            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        vehicleStatusButton = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.FrameLayout[@resource-id='android:id/content']" +
                    "/android.widget.FrameLayout[@index='2']";

            @Override
            public void tap() {
                tap.elementByXpath(xpath);
            }

            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        climateControlButton = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.FrameLayout[@resource-id='android:id/content']" +
                    "/android.widget.FrameLayout[@index='3']";

            @Override
            public void tap() {
                tap.elementByXpath(xpath);
            }

            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        hornAndLightsButton = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.FrameLayout[@resource-id='android:id/content']" +
                    "/android.widget.FrameLayout[@index='5']";

            @Override
            public void tap() {
                tap.elementByXpath(xpath);
            }

            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        lightsButton = new Element(driver, "") {
            String xpath = "/" +
                    "/android.widget.FrameLayout[@resource-id='android:id/content']" +
                    "/android.widget.FrameLayout[@index='6']";

            @Override
            public void tap() {
                tap.elementByXpath(xpath);
            }

            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
    }
}
