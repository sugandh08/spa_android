package views.myhyundai.fragments;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.myhyundai.baseviews.HomeScreen;

/**
 * Holds accessible elements for remoteCommands controls, used on Gas or HEV vehicles.
 * NOT used on PHEV or AEEV vehicles.
 */
public class RemoteCarControls extends Screen {

    public Element closeButton;
    public HomeScreen homeScreen;

    /**
     * RemoteCarControls constructor
     * @param driver Appium driver
     */
    public RemoteCarControls(AppiumDriver driver) {
        super(driver);

        closeButton= new Element(driver, "closeButton","");
        homeScreen = new HomeScreen(driver);
    }

    public boolean isPresent() {
        return closeButton.elementExists();
    }

    public boolean tapOnRemoteCommandButton(String remoteCommandName)
    {
        return tap.elementByXpath("//android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/buttonIconLayout' and (./preceding-sibling::* | ./following-sibling::*)[@text='"+ remoteCommandName +"']]");
    }

    public void clickOnRemoteStartButton()
    {
        homeScreen.clickOnRemoteActionsButton();
        tapOnRemoteCommandButton("Climate Start");
    }

    public void clickOnRemoteStopButton()
    {
        homeScreen.clickOnRemoteActionsButton();
        tapOnRemoteCommandButton("Climate Stop");
    }

    public void clickOnRemoteLockButton()
    {
        homeScreen.clickOnRemoteActionsButton();
        tapOnRemoteCommandButton("Remote Lock");
    }

    public void clickOnRemoteUnlockButton()
    {
        homeScreen.clickOnRemoteActionsButton();
        tapOnRemoteCommandButton("Remote Unlock");
    }

    public void clickOnRemoteFlashLightsButton()
    {
        homeScreen.clickOnRemoteActionsButton();
        tapOnRemoteCommandButton("Flash Lights");
    }

    public void clickOnRemoteHornAndLightsButton()
    {
        homeScreen.clickOnRemoteActionsButton();
        tapOnRemoteCommandButton("Horn and Lights");
    }


}
