package views.myhyundai.fragments;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.myhyundai.baseviews.HomeScreen;

/**
 * Holds accessible sub elements for the mapButton element.
 */
public class MapOptions extends Screen {
    public Element closeButton;
    public Element gpsButton;
    HomeScreen homeScreen;


    /**
     * MapOptions constructor
     * @param driver Appium driver
     */
    public MapOptions(AppiumDriver driver) {
        super(driver);
        closeButton = new Element(driver, "closeButton","");
        gpsButton = new Element(driver,"locateButton");
        homeScreen = new HomeScreen(driver);
    }

    /**
     * Test if the options exist.
     * @return Whether or not the options are present
     */
    public boolean isPresent() {
        return (closeButton.elementExists());
    }

    public boolean tapOnMapsOptionButton(String mapsOptionName)
    {
        return tap.elementByXpath("//android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/buttonIconLayout' and (./preceding-sibling::* | ./following-sibling::*)[@text='"+ mapsOptionName +"']]");
    }

    public void clickOnFavoritesButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("Favorites");
    }

    public void clickOnDealersButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("Dealers");
    }
    public void clickOnCollisionRepairCenterButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("Collision Repair Center");
    }


    public void clickOnChargeStationsButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("Charge Stations");
    }

    public void clickOnNearbyFuelButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("Nearby Fuel");
    }
    public void clickOnPOISearchButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("POI Search");
    }
    public void clickOnCarFinderButton()
    {
        homeScreen.clickOnMapsButton();
        tapOnMapsOptionButton("Car Finder");
    }
}
