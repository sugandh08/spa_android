package views.myhyundai.fragments;

import io.appium.java_client.AppiumDriver;
import library.Element;

/**
 * Holds accessible elements for remoteCommands controls, used on Gas or HEV vehicles.
 * NOT used on PHEV or AEEV vehicles.
 */
public class RemoteCarControlsGen2Point5 {

    public Element start_stopButton;
    public Element startButton;
    public Element stopButton;
    public Element doorLocksButton;
    public Element lockButton;
    public Element unlockButton;
    public Element lightsButton;
    public Element hornAndLightsButton;
    public Element trunkButton;
    public  Element openTrunkButton;
    public Element closeTrunkButton;
    public Element openWindowsButton;
    public Element sunRoofButton;


    /**
     * RemoteCarControls constructor
     * @param driver Appium driver
     */
    public RemoteCarControlsGen2Point5(AppiumDriver driver) {
        start_stopButton = new Element(driver,"iv_remote_startstop");
        startButton=new Element(driver,"home_remote_option_left_img");
        stopButton=new Element(driver,"home_remote_option_right_img");
        doorLocksButton = new Element(driver,"iv_remote_door");
        lockButton=new Element(driver,"home_remote_option_left_img");
        unlockButton=new Element(driver,"home_remote_option_right_img");
        lightsButton = new Element(driver,"iv_remote_lights");
        hornAndLightsButton = new Element(driver,"iv_remote_hornlights");
        trunkButton=new Element(driver,"iv_remote_trunk");
        openTrunkButton=new Element(driver,"home_remote_option_left_img");
        closeTrunkButton=new Element(driver,"home_remote_option_right_img");
        openWindowsButton=new Element(driver,"iv_remote_windows");
        sunRoofButton=new Element(driver,"iv_remote_sunroof");


    }
}
