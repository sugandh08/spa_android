package views.myhyundai;

import config.Account;
import config.Vehicle;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import library.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.SearchContext;
import tests.myh_app_refresh_regression_suite.LoginVariations.GuestLogin;
import views.AppController;
import views.Screen;
import views.genesis.screens.IntroductionScreen;
import views.myhyundai.baseviews.HomeScreen;
import views.myhyundai.baseviews.MapScreen;
import views.myhyundai.fragments.CarCareOptions;
import views.myhyundai.fragments.MapOptions;
import views.myhyundai.fragments.RemoteCarControls;
import views.myhyundai.popups.*;
import views.myhyundai.screens.*;

import java.time.Duration;
import java.util.List;

/**
 * Holds all screens and popups for MyHyundai.
 */
public class GuiController {
    private AppiumDriver driver;
    private AppController appController;

    // Standard screens
    public AboutAndSupportScreen aboutAndSupportScreen;
    public AccessoriesScreen accessoriesScreen;
    public AlertSettingsScreen alertSettingsScreen;
    public BlueLinkHowToVideosScreen blueLinkHowToVideosScreen;
    public BlueLinkInformationScreen blueLinkInformationScreen;
    public BluetoothScreen bluetoothScreen;
    public BluetoothVideosScreen bluetoothVideosScreen;
    public BlueLinkScreen blueLinkScreen;
    public BluetoothAssistanceScreen bluetoothAssistanceScreen;
    public CarFinderScreen carFinderScreen;
    public ChangePinEnterNewPinScreen changePinEnterNewPinScreen;
    public ChangePinEnterPasswordScreen changePinEnterPasswordScreen;
    public ChangePinEnterSecurityAnswerScreen changePinEnterSecurityAnswerScreen;
    public ChargeStationScreen chargeStationScreen;
    public ClimateSettingsScreen climateSettingsScreen;
    public CurfewAlertScreen curfewAlertScreen;
    public CarCareOptions carCareOptions;
    public DealerLocatorScreen dealerLocatorScreen;
    public DemoChooseCarScreen demoChooseCarScreen;
    public DiagnosticReportScreen diagnosticReportScreen;
    public DevicePermissionPopup devicePermissionPopup;
    public ElectricHomeScreen electricHomeScreen;
    public EditPermissionScreen_Gen2Point5 editPermissionScreen_gen2Point5;
    public EditChargeScheduleScreen editChargeScheduleScreen;
    public OSElectricHomeScreen osElectricHomeScreen;
    public ChargeManagementScreen chargeManagementScreen;
    public EmailAppSupportScreen emailAppSupportScreen;
    public FaqScreen faqScreen;
    public FavoritePlacesScreen_Gen2Point5 favoritePlacesScreen_gen2Point5;
    public GuestloginScreen GuestloginScreen;
    public GasHomeScreen gasHomeScreen;
    public GasHomeScreenGen2Point5 gasHomeScreenGen2Point5;
    public GeoFenceAlertScreen geoFenceAlertScreen;
    public GmailComposeScreen gmailComposeScreen;
    public GooglePlayStoreScreen googlePlayStoreScreen;
    public GettingStartedScreen gettingStartedScreen;
    public HybridHomeScreen hybridHomeScreen;
    public HyundaiHopeOnWheelsScreen hyundaiHopeOnWheelsScreen;
    public HyundaiPartsAndCollisionInfoScreen hyundaiPartsAndCollisionInfoScreen;
    public HyundaiRewardsScreen hyundaiRewardsScreen;
    public HyundaiUSAScreen hyundaiUSAScreen;
    public HomeScreen homeScreen;
    public IndicatorGuideScreen indicatorGuideScreen;
    public InviteAdditionalDriverScreen_Gen2Point5 inviteAdditionalDriverScreen_gen2Point5;
    public LoginScreen loginScreen;
    public MaintenanceInformationScreen maintenanceInformationScreen;
    public MenuScreen menuScreen;
    public MessageCenterScreen_Gen2Point5 messageCenterScreen_gen2Point5;
    public MonthlyVehicleReportHistoryScreen monthlyVehicleReportHistoryScreen;
    public MonthlyVehicleReportScreen monthlyVehicleReportScreen;
    public MyDriverSettingsScreen_Gen2Point5 myDriverSettingsScreen_gen2Point5;
    public MyHyundaiComScreen myHyundaiComScreen;
    public MyGenesisRedirectPopup myGenesisRedirectPopup;
    public MultimediaAndNavigationScreen multimediaAndNavigationScreen;
    public FavoritesPOIScreen favoritesPOIScreen;
    public ManualsAndWarrantiesScreen manualsAndWarrantiesScreen;
    public MapOptions mapOptions;
    public NearbyGasScreen nearbyGasScreen;
    public OwnersManualScreen ownersManualScreen;
    public OffPeakScheduleScreen offPeakScheduleScreen;
    public ParkingMeterNotesScreen parkingMeterNotesScreen;
    public ParkingMeterScreen parkingMeterScreen;
    public ParkingMeterTimeSetScreen parkingMeterTimeSetScreen;
    public EnterPinScreen enterPinScreen;
    public PoiSearchScreen poiSearchScreen;
    public PoiAddToFavoritePlacesScreen_Gen2Point5 poiAddToFavoritePlacesScreen_gen2Point5;
    public PreferredContactInformationScreen preferredContactInformationScreen;
    public ProfileScreen profileScreen;
    public ProfileScreen_Gen2Point5 profileScreen_gen2Point5;
    public QuickReferenceGuideScreen quickReferenceGuideScreen;
    public RegisteredDevicesScreen registeredDevicesScreen;
    public RemoteHistoryScreen remoteHistoryScreen;
    public RemoteStartSettingsScreen remoteStartSettingsScreen;
    public RemotePresetsScreen remotePresetsScreen;
    public RemoteStartSettingsScreenGen2Point5 remoteStartSettingsScreenGen2Point5;
    public RemoteCarControls remoteCarControls;
    public ScheduleServiceScreen scheduleServiceScreen;
    public SelectVehicleScreen selectVehicleScreen;
    public SetChargeScheduleScreen setChargeScheduleScreen;
    public SetDepartureTimeScheduleScreen setDepartureTimeScheduleScreen;
    public SettingsScreen settingsScreen;
    public SettingsScreenGen2Point5 settingsScreenGen2Point5;
    public SpeedAlertScreen speedAlertScreen;
    public StartDrivingSettingsScreen startDrivingSettingsScreen;
    public TermsAndConditionsScreen termsAndConditionsScreen;
    public ValetAlertScreen valetAlertScreen;
    public VehicleStatusScreen vehicleStatusScreen;
    public VehicleStatusScreenGen2Point5 vehicleStatusScreenGen2Point5;
    public VehicleHealthScreen vehicleHealthScreen;
    public WarrantyInfoScreen warrantyInfoScreen;
    public YouTubeScreen youTubeScreen;
    public IntroductionScreen genesisIntroductionScreen;

    // Standard popups
    public AlertSavedPopup alertSavedPopup;
    public AndroidEmailPopup androidEmailPopup;
    public AlertModificationPopup alertModificationPopup;
    public CallRoadsideAssistancePopup callRoadsideAssistancePopup;
    public CallThisNumberPopup callThisNumberPopup;
    public CarFinderNotFoundGen1Popup carFinderNotFoundGen1Popup;
    public ChangePinSuccessfulPopup changePinSuccessfulPopup;
    public ChargeScheduleNotSetPopup chargeScheduleNotSetPopup;
    public DeleteConfirmationPopup deleteConfirmationPopup;
    public DepartureSetschedulePopup departureSetschedulePopup;
    public FeatureNotAvailableInDemoPopup featureNotAvailableInDemoPopup;
    public FeatureNotSupportedPopup featureNotSupportedPopup;
    public ForgotPinPopup forgotPinPopup;
    public GenesisRedirectPopup genesisRedirectPopup;
    public GooglePlayTosPopup googlePlayTosPopup;
    public IncorrectPinPopup incorrectPinPopup;
    public IncorrectPinCarFinderPopup incorrectPinCarFinderPopup;
    public InstallGenesisPopup installGenesisPopup;
    public InvalidPasswordPopup invalidPasswordPopup;
    public InvalidUsernamePopup invalidUsernamePopup;
    public LocationServicesDisabledPopup locationServicesDisabledPopup;
    public LogoutPopup logoutPopup;

    public OutOfRangePopup outOfRangePopup;
    public ParkingMeterAlertTimeGreaterThanMeterTimePopup parkingMeterAlertTimeGreaterThanMeterTimePopup;
    public ParkingMeterTimerAboutToExpirePopup parkingMeterTimerAboutToExpirePopup;
    public ParkingMeterTimerExpiredPopup parkingMeterTimerExpiredPopup;
    public ParkingMeterTimerResetPopup parkingMeterTimerResetPopup;
    public PinLockedPopup pinLockedPopup;
    public PinLockedCarFinderPopup pinLockedCarFinderPopup;
    public PriorFeatureRequestPopup priorFeatureRequestPopup;
    public RemoteCommandResultPopup remoteCommandResultPopup;
    public RemoteStopOnlyDuringRemoteStartPopup remoteStopOnlyDuringRemoteStartPopup;
    public RequestSentPopup requestSentPopup;
    public SetChargeScheduleSuccessPopup setChargeScheduleSuccessPopup;
    public SaveChargeSchedulePopup saveChargeSchedulePopup;
    public TalkToBlueLinkAgentPopup talkToBlueLinkAgentPopup;
    public TermsAndConditionsPopup termsAndConditionsPopup;



    /**
     * GuiController constructor
     * @param driver Appium driver
     */
    public GuiController(AppiumDriver driver, AppController appController) {
        this.driver = driver;
        this.appController = appController;

        // Standard screens
        aboutAndSupportScreen = new AboutAndSupportScreen(driver);

        accessoriesScreen = new AccessoriesScreen(driver);
        alertSettingsScreen = new AlertSettingsScreen(driver);
        blueLinkHowToVideosScreen = new BlueLinkHowToVideosScreen(driver);
        blueLinkInformationScreen = new BlueLinkInformationScreen(driver);
        bluetoothScreen = new BluetoothScreen(driver);
        bluetoothVideosScreen = new BluetoothVideosScreen(driver);
        blueLinkScreen = new BlueLinkScreen(driver);
        bluetoothAssistanceScreen =new BluetoothAssistanceScreen(driver);
        carFinderScreen = new CarFinderScreen(driver);
        changePinEnterNewPinScreen = new ChangePinEnterNewPinScreen(driver);
        changePinEnterPasswordScreen = new ChangePinEnterPasswordScreen(driver);
        changePinEnterSecurityAnswerScreen = new ChangePinEnterSecurityAnswerScreen(driver);
        chargeStationScreen = new ChargeStationScreen(driver);
        climateSettingsScreen = new ClimateSettingsScreen(driver);
        curfewAlertScreen = new CurfewAlertScreen(driver);
        carCareOptions = new CarCareOptions(driver);
        dealerLocatorScreen = new DealerLocatorScreen(driver);
        demoChooseCarScreen = new DemoChooseCarScreen(driver);
        diagnosticReportScreen = new DiagnosticReportScreen(driver);
        devicePermissionPopup = new DevicePermissionPopup(driver);
        electricHomeScreen = new ElectricHomeScreen(driver);
        editPermissionScreen_gen2Point5=new EditPermissionScreen_Gen2Point5(driver);
        osElectricHomeScreen = new OSElectricHomeScreen(driver);
        chargeManagementScreen = new ChargeManagementScreen(driver);
        emailAppSupportScreen = new EmailAppSupportScreen(driver);
        enterPinScreen = new EnterPinScreen(driver);
        editChargeScheduleScreen = new EditChargeScheduleScreen(driver);
        faqScreen = new FaqScreen(driver);
        favoritePlacesScreen_gen2Point5 = new FavoritePlacesScreen_Gen2Point5(driver);
        GuestloginScreen = new GuestloginScreen(driver);
        gasHomeScreen = new GasHomeScreen(driver);
        gasHomeScreenGen2Point5 = new GasHomeScreenGen2Point5(driver);
        geoFenceAlertScreen = new GeoFenceAlertScreen(driver);
        gmailComposeScreen = new GmailComposeScreen(driver);
        googlePlayStoreScreen = new GooglePlayStoreScreen(driver);
        gettingStartedScreen = new GettingStartedScreen(driver);
        hybridHomeScreen = new HybridHomeScreen(driver);
        hyundaiHopeOnWheelsScreen = new HyundaiHopeOnWheelsScreen(driver);
        hyundaiPartsAndCollisionInfoScreen = new HyundaiPartsAndCollisionInfoScreen(driver);
        hyundaiRewardsScreen = new HyundaiRewardsScreen(driver);
        hyundaiUSAScreen = new HyundaiUSAScreen(driver);
        homeScreen = new HomeScreen(driver);
        indicatorGuideScreen = new IndicatorGuideScreen(driver);
        inviteAdditionalDriverScreen_gen2Point5=new InviteAdditionalDriverScreen_Gen2Point5(driver);
        loginScreen = new LoginScreen(driver);
        maintenanceInformationScreen = new MaintenanceInformationScreen(driver);
        menuScreen = new MenuScreen(driver);
        messageCenterScreen_gen2Point5=new MessageCenterScreen_Gen2Point5(driver);
        monthlyVehicleReportHistoryScreen = new MonthlyVehicleReportHistoryScreen(driver);
        monthlyVehicleReportScreen = new MonthlyVehicleReportScreen(driver);
        myDriverSettingsScreen_gen2Point5=new MyDriverSettingsScreen_Gen2Point5(driver);
        myHyundaiComScreen = new MyHyundaiComScreen(driver);
        myGenesisRedirectPopup = new MyGenesisRedirectPopup(driver);
        favoritesPOIScreen = new FavoritesPOIScreen(driver);
        manualsAndWarrantiesScreen = new ManualsAndWarrantiesScreen(driver);
        multimediaAndNavigationScreen = new MultimediaAndNavigationScreen(driver);
        mapOptions = new MapOptions(driver);
        nearbyGasScreen = new NearbyGasScreen(driver);
        ownersManualScreen = new OwnersManualScreen(driver);
        offPeakScheduleScreen = new OffPeakScheduleScreen(driver);
        parkingMeterNotesScreen = new ParkingMeterNotesScreen(driver);
        parkingMeterScreen = new ParkingMeterScreen(driver);
        parkingMeterTimeSetScreen = new ParkingMeterTimeSetScreen(driver);
        poiSearchScreen = new PoiSearchScreen(driver);
        poiAddToFavoritePlacesScreen_gen2Point5=new PoiAddToFavoritePlacesScreen_Gen2Point5(driver);
        preferredContactInformationScreen = new PreferredContactInformationScreen(driver);
        profileScreen = new ProfileScreen(driver);
        profileScreen_gen2Point5=new ProfileScreen_Gen2Point5(driver);
        quickReferenceGuideScreen = new QuickReferenceGuideScreen(driver);
        registeredDevicesScreen = new RegisteredDevicesScreen(driver);
        remoteHistoryScreen = new RemoteHistoryScreen(driver);
        remoteStartSettingsScreen = new RemoteStartSettingsScreen(driver);
        remoteStartSettingsScreenGen2Point5=new RemoteStartSettingsScreenGen2Point5(driver);
        remoteCarControls = new RemoteCarControls(driver);
        scheduleServiceScreen = new ScheduleServiceScreen(driver);
        selectVehicleScreen = new SelectVehicleScreen(driver);
        setChargeScheduleScreen = new SetChargeScheduleScreen(driver);
        settingsScreen = new SettingsScreen(driver);
        settingsScreenGen2Point5=new SettingsScreenGen2Point5(driver);
        speedAlertScreen = new SpeedAlertScreen(driver);
        setDepartureTimeScheduleScreen = new SetDepartureTimeScheduleScreen(driver);
        startDrivingSettingsScreen = new StartDrivingSettingsScreen(driver);
        termsAndConditionsScreen = new TermsAndConditionsScreen(driver);
        valetAlertScreen = new ValetAlertScreen(driver);
        vehicleStatusScreen = new VehicleStatusScreen(driver);
        vehicleStatusScreenGen2Point5=new VehicleStatusScreenGen2Point5(driver);
        vehicleHealthScreen = new VehicleHealthScreen(driver);
        warrantyInfoScreen = new WarrantyInfoScreen(driver);
        youTubeScreen = new YouTubeScreen(driver);
        genesisIntroductionScreen = new IntroductionScreen(driver);

        // Standard popups
        alertSavedPopup = new AlertSavedPopup(driver);
        androidEmailPopup = new AndroidEmailPopup(driver);
        alertModificationPopup = new AlertModificationPopup(driver);
        callRoadsideAssistancePopup = new CallRoadsideAssistancePopup(driver);
        callThisNumberPopup = new CallThisNumberPopup(driver);
        carFinderNotFoundGen1Popup = new CarFinderNotFoundGen1Popup(driver);
        changePinSuccessfulPopup = new ChangePinSuccessfulPopup(driver);
        chargeScheduleNotSetPopup = new ChargeScheduleNotSetPopup(driver);
        deleteConfirmationPopup = new DeleteConfirmationPopup(driver);
        departureSetschedulePopup = new DepartureSetschedulePopup(driver);
        featureNotAvailableInDemoPopup = new FeatureNotAvailableInDemoPopup(driver);
        featureNotSupportedPopup = new FeatureNotSupportedPopup(driver);
        forgotPinPopup = new ForgotPinPopup(driver);
        genesisRedirectPopup = new GenesisRedirectPopup(driver);
        googlePlayTosPopup = new GooglePlayTosPopup(driver);
        incorrectPinPopup = new IncorrectPinPopup(driver);
        incorrectPinCarFinderPopup = new IncorrectPinCarFinderPopup(driver);
        installGenesisPopup = new InstallGenesisPopup(driver);
        invalidPasswordPopup = new InvalidPasswordPopup(driver);
        invalidUsernamePopup = new InvalidUsernamePopup(driver);
        locationServicesDisabledPopup = new LocationServicesDisabledPopup(driver);
        logoutPopup = new LogoutPopup(driver);
        parkingMeterAlertTimeGreaterThanMeterTimePopup = new ParkingMeterAlertTimeGreaterThanMeterTimePopup(driver);
        parkingMeterTimerAboutToExpirePopup = new ParkingMeterTimerAboutToExpirePopup(driver);
        parkingMeterTimerExpiredPopup = new ParkingMeterTimerExpiredPopup(driver);
        parkingMeterTimerResetPopup = new ParkingMeterTimerResetPopup(driver);
        pinLockedPopup = new PinLockedPopup(driver);
        pinLockedCarFinderPopup = new PinLockedCarFinderPopup(driver);
        priorFeatureRequestPopup = new PriorFeatureRequestPopup(driver);
        outOfRangePopup = new OutOfRangePopup(driver);
        remoteCommandResultPopup = new RemoteCommandResultPopup(driver);
        remoteStopOnlyDuringRemoteStartPopup = new RemoteStopOnlyDuringRemoteStartPopup(driver);
        requestSentPopup = new RequestSentPopup(driver);
        setChargeScheduleSuccessPopup = new SetChargeScheduleSuccessPopup(driver);
        saveChargeSchedulePopup = new SaveChargeSchedulePopup(driver);
        talkToBlueLinkAgentPopup = new TalkToBlueLinkAgentPopup(driver);
        termsAndConditionsPopup = new TermsAndConditionsPopup(driver);
        remotePresetsScreen = new RemotePresetsScreen(driver);
    }

    /**
     * @param powerType the power type to get the appropriate home screen for
     * @return Home screen based on vehicle's power type (gas, hybrid, or electric)
     */
    public HomeScreen getHomeScreenByPowerType(Vehicle.PowerType powerType) {
        switch (powerType) {
            case GAS:
                return gasHomeScreen;

            case HEV:
                return gasHomeScreen;

            case PHEV:
                return hybridHomeScreen;

            case AEEV:
                return electricHomeScreen;

            case OSEV:
                return osElectricHomeScreen;



            default:
                // shouldn't really hit this case, but assuming gas if we do.
                return gasHomeScreen;
        }
    }

    /**
     * Logs in with chosen account, selects chosen vehicle, accepts terms and conditions, then ends at home page.
     * Uses standard wait time
     * @param account Account to log in with 
     * @param vin VIN of vehicle to select at select vehicle screen
     */
    public void loginAndGoToHomePageWithKeepmeloggedinSelectVehicle(Account account, String vin) {

        loginAndGoToHomePageWithkeeepmeloggedinSelectVehicle(account, vin, 10);
    }

    public void
    loginAndGoToHomePageWithkeeepmeloggedinSelectVehicle(Account account, String vin, long vehicleSelectionWaitTime) {

        loginScreen.loginWithkeepmeloggedinAccount(account);

        appController.appFunctions.pauseTestExecution(3, 6000);

        if(termsAndConditionsPopup.acceptButton.elementExists(30))
        {
            System.out.println(termsAndConditionsPopup.acceptButton.elementExists());
            termsAndConditionsPopup.acceptButton.tap(20);
            termsAndConditionsPopup.usingtheapp.tap(20);
        }

        else{
            selectVehicleScreen.tapVehicleButtonByVinIfThisScreenIsPresent(vin, vehicleSelectionWaitTime);
            appController.appFunctions.pauseTestExecution(1, 1000);
            termsAndConditionsPopup.acceptButton.tap(20);
        }
        appController.appFunctions.pauseTestExecution(2, 1000);

      /* if(termsAndConditionsPopup.dismissButton.elementExists(3)){

           termsAndConditionsPopup.dismissButton.tap(1);
       }
       else{
           termsAndConditionsPopup.usingtheapp.tap();
           System.out.println("");
       }*/
    }

    public void loginAndGoToHomePageWithSelectVehicle(Account account, String vin) {

        loginAndGoToHomePageWithSelectVehicle(account, vin, 10);
    } 

    /**
     * Logs in with chosen account, selects chosen vehicle, accepts terms and conditions, then ends at home page
     * @param account Account to log in with
     * @param vin VIN of vehicle to select at select vehicle screen
     * @param vehicleSelectionWaitTime Time (in seconds) to implicitly wait when selecting a vehicle
     */

    public void
    loginAndGoToHomePageWithSelectVehicle(Account account, String vin, long vehicleSelectionWaitTime) {

        loginScreen.loginWithAccount(account);

        appController.appFunctions.pauseTestExecution(3, 6000);

        if(termsAndConditionsPopup.acceptButton.elementExists(30))
        {
            System.out.println(termsAndConditionsPopup.acceptButton.elementExists());
            termsAndConditionsPopup.acceptButton.tap(20);
            termsAndConditionsPopup.usingtheapp.tap(20);
            termsAndConditionsPopup.dismissButton.tap();
        }

        else{
            selectVehicleScreen.tapVehicleButtonByVinIfThisScreenIsPresent(vin, vehicleSelectionWaitTime);
            appController.appFunctions.pauseTestExecution(1, 1000);
            termsAndConditionsPopup.acceptButton.tap(20);
            termsAndConditionsPopup.dismissButton.tap();
        }
        appController.appFunctions.pauseTestExecution(2, 1000);

      /* if(termsAndConditionsPopup.dismissButton.elementExists(3)){

           termsAndConditionsPopup.dismissButton.tap(1);
       }
       else{
           termsAndConditionsPopup.usingtheapp.tap();
           System.out.println("");
       }*/
    }

    public void login(Account account, String vin) {

        login(account, vin, 10);
    }

    public void
    login(Account account, String vin, long vehicleSelectionWaitTime) {

        loginScreen.loginWithAccount(account);

        appController.appFunctions.pauseTestExecution(3, 6000);

        if (termsAndConditionsPopup.acceptButton.elementExists(30));

    }

        /**
         * Swipes down a scroll wheel for the chosen number of scroll wheel entries
         * @param scrollWheelXpath The xpath of the scroll wheel element
         * @param entriesToScroll Number of entries to scroll on the scroll wheel
         */
    public void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll) {
        Rectangle dimensions = driver.findElementByXPath(scrollWheelXpath).getRect();

        int xCoordinate = dimensions.x + (int)(dimensions.width * 0.5);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.54);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.30);

        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
    }

//     public void swipeScrollWheelRight(String scrollWheelXpath, int entriesToScroll) {
//        Rectangle dimensions = driver.findElementByXPath(scrollWheelXpath).getRect();
//
//        int xCoordinate = dimensions.x + (int)(dimensions.width * 1.5);
//        int xCoordinateStart = dimensions.x + (int)(dimensions.width * 00.00);
//        int xCoordinateEnd = dimensions.x + (int)(dimensions.width * .30);
//
//        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, xCoordinateStart);
//        PointOption pointEnd = new PointOption().withCoordinates(0,xCoordinateEnd );
//        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));
//
//        TouchAction touchAction = new TouchAction(driver);
//
//        for (int i = 0; i < entriesToScroll; i++) {
//            touchAction
//                    .press(pointStart)
//                    .waitAction(waitOptions)
//                    .moveTo(pointEnd)
//                    .release()
//                    .perform();
//            try {
//                Thread.sleep(250);
//            } catch (InterruptedException ie) {
//                //
//            }
//        }
//    }
    /**
     * Swipes up a scroll wheel for the chosen number of scroll wheel entries
     * @param scrollWheelXpath The xpath of the scroll wheel element
     * @param entriesToScroll Number of entries to scroll on the scroll wheel
     */
    public void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll) {
        Rectangle dimensions = driver.findElementByXPath(scrollWheelXpath).getRect();

        int xCoordinate = dimensions.x + (int)(dimensions.width * 0.5);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.44);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.65);

        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
    }



    /**
     * Resets the PIN on the account to its standard PIN again to reset the lockout timer and state.
     * Must be at a home screen for this to work.
     * @param account The current logged in account
     * @param vin The VIN to find the current home screen
     */
    public void resetLockedPinFromHomeScreen(Account account, String vin) {
        HomeScreen homeScreen = getHomeScreenByPowerType(account.vehicles.get(vin).powerType);

        homeScreen.clickOnHamburgerMenuButton();
        menuScreen.settingsButton.tap();
        settingsScreen.changePinButton.tap();
        enterPinScreen.enterPin(account.pin);
        forgotPinPopup.changePinButton.tap();
        changePinEnterPasswordScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        changePinEnterPasswordScreen.enterPasswordEditText.enterText(account.password);
        changePinEnterPasswordScreen.submitButton.tap();
        changePinEnterSecurityAnswerScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        changePinEnterSecurityAnswerScreen.enterSecurityAnswerEditText.enterText(account.securityAnswer);
        changePinEnterSecurityAnswerScreen.submitButton.tap();
        changePinEnterNewPinScreen.enterPin(account.pin);
        changePinEnterNewPinScreen.enterPin(account.pin);
        changePinEnterNewPinScreen.saveButton.tap();
        changePinSuccessfulPopup.okButton.tap();
    }

    /**
     * Opens Car Care or Map expandable list from a Home screen and selects an entry in it. If the entry cannot be
     * found due to Appium/driver issues, the home screen is refreshed and the entry is attempted again up to 5 times.
     * @param homeScreen The home screen of the current vehicle.
     * @param listToOpen Either homeScreen.carCareButton or homeScreen.mapButton
     * @param entryToSelect Any of the elements under homeScreen.carCareOptions or homeScreen.mapOptions
     */
    public void selectExpandableItemSafelyFromHomeScreen(HomeScreen homeScreen, Element listToOpen, Element entryToSelect) {
        final int NUMBER_OF_TRIES = 5;
        for (int i = 0; i < NUMBER_OF_TRIES; i++) {
            // check if homescreen is present. This is to make sure the screen is 'active' before trying to open a list.
            homeScreen.isPresent();

            // open either Car Care or Map
            listToOpen.tap();

            // Sleep for a short amount of time to make sure list animations are completely settled.
            appController.appFunctions.pauseTestExecution(1, 1500);

            // Check if the list entry is detectable by the driver
            if (entryToSelect.elementExists()) {
                // tap the list entry
                entryToSelect.tap();
                // wait for the screen to load to avoid stale objects
                appController.appFunctions.pauseTestExecution(1, 1000);
                break;
            } else {
                if (i < NUMBER_OF_TRIES - 1) {
                    System.out.println("NOTE: Above 'element not found' message does not necessarily indicate test failure");
                }
                // If the button can't be found, interact with the screen
                // to get the elements to reappear to UIAutomator
                appController.appFunctions.tapAndroidBackButton();
                homeScreen.clickOnHamburgerMenuButton();
                appController.appFunctions.tapAndroidBackButton();
            }
        }
    }

    /**
     * Opens Car Care or Map expandable list from a Home screen and selects an entry in it. If the entry cannot be
     * found due to Appium/driver issues, the home screen is refreshed and the entry is attempted again up to 5 times.
     * @param homeScreen The home screen of the current vehicle.
     * @param listToOpen Either homeScreen.carCareButton or homeScreen.mapButton
     * @param entryToSelect Any of the elements under homeScreen.carCareOptions or homeScreen.mapOptions
     * @param destinationScreen Screen that should be present if all navigation is successful. If isPresent() check
     *                          fails, method will retry again.
     */
    public void selectExpandableItemSafelyFromHomeScreen(
            HomeScreen homeScreen, Element listToOpen, Element entryToSelect, Screen destinationScreen) {
        final int NUMBER_OF_TRIES = 5;
        for (int i = 0; i < NUMBER_OF_TRIES; i++) {
            // check if homescreen is present. This is to make sure the screen is 'active' before trying to open a list.
            homeScreen.isPresent();

            // open either Car Care or Map
            listToOpen.tap();

            // Sleep for a short amount of time to make sure list animations are completely settled.
            appController.appFunctions.pauseTestExecution(1, 1500);

            // Check if the list entry is detectable by the driver
            if (entryToSelect.elementExists()) {
                // tap the list entry
                entryToSelect.tap();
                // wait for the screen to load to avoid stale objects
                appController.appFunctions.pauseTestExecution(1, 1000);
                // make sure the screen actually loaded
                if (destinationScreen.isPresent()) {
                    break;
                }
            }
            else {
                // back out of the list so we are at the home screen again. We only need to do this if the element was
                // not found. If the element was found but the destination failed to open, we are at the home screen.
                appController.appFunctions.tapAndroidBackButton();
            }

            // open the menu screen, then close it to cycle the home page.
            homeScreen.clickOnHamburgerMenuButton();
            appController.appFunctions.tapAndroidBackButton();
        }
    }

    /**
     * Opens Car Care or Map expandable list from a Map screen and selects an entry in it. If the entry cannot be
     * found due to Appium/driver issues, the home screen is refreshed and the entry is attempted again up to 5 times.
     * @param mapScreen The map screen of the current vehicle.
     * @param listToOpen Either mapScreen.carCareButton or mapScreen.mapButton
     * @param entryToSelect Any of the available elements under mapScreen.carCareOptions or mapScreen.mapOptions
     */
    public void selectExpandableItemSafelyFromMapScreen(MapScreen mapScreen, Element listToOpen, Element entryToSelect) {
        final int NUMBER_OF_TRIES = 5;
        for (int i = 0; i < NUMBER_OF_TRIES; i++) {
            // check if homescreen is present. This is to make sure the screen is 'active' before trying to open a list.
            mapScreen.isPresent();

            // open either Car Care or Map
            listToOpen.tap();

            // Sleep for a short amount of time to make sure list animations are completely settled.
            appController.appFunctions.pauseTestExecution(1, 1500);

            // Check if the list entry is detectable by the driver
            if (entryToSelect.elementExists()) {
                // tap the list entry
                entryToSelect.tap();
                // wait for the screen to load to avoid stale objects
                appController.appFunctions.pauseTestExecution(1, 1000);
                break;
            } else {
                // If the button can't be found, interact with the screen
                // to get the elements to reappear to UIAutomator
                appController.appFunctions.tapAndroidBackButton();
                mapScreen.findMyCarButton.tap();
                appController.appFunctions.tapAndroidBackButton();
            }
        }
    }

    /**
     * Opens Car Care or Map expandable list from a Map screen and selects an entry in it. If the entry cannot be
     * found due to Appium/driver issues, the home screen is refreshed and the entry is attempted again up to 5 times.
     * @param mapScreen The map screen of the current vehicle.
     * @param listToOpen Either mapScreen.carCareButton or mapScreen.mapButton
     * @param entryToSelect Any of the available elements under mapScreen.carCareOptions or mapScreen.mapOptions
     * @param destinationScreen Screen that should be present if all navigation is successful. If isPresent() check
     *                          fails, method will retry again.
     */
    public void selectExpandableItemSafelyFromMapScreen(
            MapScreen mapScreen, Element listToOpen, Element entryToSelect, Screen destinationScreen) {
        final int NUMBER_OF_TRIES = 5;
        for (int i = 0; i < NUMBER_OF_TRIES; i++) {
            // check if homescreen is present. This is to make sure the screen is 'active' before trying to open a list.
            mapScreen.isPresent();

            // open either Car Care or Map
            listToOpen.tap();

            // Sleep for a short amount of time to make sure list animations are completely settled.
            appController.appFunctions.pauseTestExecution(1, 1500);

            // Check if the list entry is detectable by the driver
            if (entryToSelect.elementExists()) {
                // tap the list entry
                entryToSelect.tap();
                // wait for the screen to load to avoid stale objects
                appController.appFunctions.pauseTestExecution(1, 1000);
                // make sure the screen actually loaded
                if (destinationScreen.isPresent()) {
                    break;
                }
            }
            else {
                // back out of the list so we are at the home screen again. We only need to do this if the element was
                // not found. If the element was found but the destination failed to open, we are at the home screen.
                appController.appFunctions.tapAndroidBackButton();
            }

            // open the menu screen, then close it to cycle the home page.
            mapScreen.findMyCarButton.tap();
            appController.appFunctions.tapAndroidBackButton();
        }
    }


    /**
     * Return Text of Element located by Xpath
     * @param xpath The xpath of the Element

     */
    public String getTextUsingXpath(String xpath)
    {

        return driver.findElementByXPath(xpath).getText();
    }

    public List<MobileElement> getTextsUsingXpath(String xpath)
    {

        return driver.findElementsByXPath(xpath);
        
    }

    public boolean getxpathCheckedValue(String Xpath) {
        By e=By.xpath(Xpath);
        if (e.findElement((SearchContext) By.xpath(Xpath)).getAttribute("checked").equals(true)) {
            return true;
        }
        else{
            return false;
        }
    }


}