package views.myhyundai.baseviews;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.myhyundai.fragments.CarCareOptions;
import views.myhyundai.fragments.MapOptions;

/**
 * Holds common elements accessible from the map screens
 */
public abstract class MapScreen extends Screen {

    public Element backButton;

    public Element searchButton;
    public Element searchEditText;
    public Element listButton;
    public Element searchResultsListView;
    public Element listEntryTitleTextView;

    public Element findMyCarButton;
    public Element currentLocationButton;

    public Element carCareButton;
    public Element homeButton;
    public Element mapButton;

    // Accessible sub elements of elements
    public CarCareOptions carCareOptions;
    public MapOptions mapOptions;

    public MapScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        searchButton = new Element(driver, "ivSearch");
        searchEditText = new Element(driver, "tvSearchText");
        listButton = new Element(driver, "ivMapListIcon");
        searchResultsListView = new Element(driver, "map_search_xlist");
        listEntryTitleTextView = new Element(driver, "map_search_item_title");
        findMyCarButton = new Element(driver,"map_main_findmycar");
        currentLocationButton = new Element(driver,"map_main_currentloc");
        carCareButton = new Element(driver,"sdm_left_img");
        homeButton = new Element(driver,"sdm_mid_img");
        mapButton = new Element(driver,"sdm_right_img");
        carCareOptions = new CarCareOptions(driver);
        mapOptions = new MapOptions(driver);
    }

    public void ClickOnbackbutton()
    {
        tap.elementByXpath("//*[@contentDescription='Navigate up']");
    }
}
