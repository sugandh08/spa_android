package views.myhyundai.baseviews;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.myhyundai.fragments.CarCareOptions;
import views.myhyundai.fragments.MapOptions;

/**
 * Holds accessible elements that are common to home screens.
 */
public class HomeScreen extends Screen {

    // Accessible elements
    //public Element menuButton;
    public Element remoteHistoryButton;
    public Element vehicleModelName;
    public Element carCareButton;
    public Element remoteActionsButton;
    public Element mapsButton;
    public Element messageCenterButton;
    public Element poiSearchTextbox;
    public Element poiSearchGlassIcon;
    public Element greetingText;
    public Element weatherIcon;
    public Element weatherTemperature;
    public Element remoteCommandStatusLabel;
    public Element Remotelock;
    public Element successfulpopup;
    public Element vehiclenickname;
    public Element DKicon;
    public Element ConnectivityWarning;
    public Element SafetyWarningtitle;
    public Element SafetywarningContent;
    public Element Dismiss;
    public Element LearnMore;

    //public Element Chatboxbutton;


    /**
     * HomeScreen constructor
     *
     * @param driver Appium driver
     */
    public HomeScreen(AppiumDriver driver) {
        super(driver);
        remoteHistoryButton = new Element(driver, "ivRemoteHistory");
        vehicleModelName = new Element(driver, "tvIntroduce");
        carCareButton = new Element(driver, "carCareFragment");
        remoteActionsButton = new Element(driver, "remoteFragment");
        mapsButton = new Element(driver, "mapFragment");
        messageCenterButton = new Element(driver, "ivNotification");
        poiSearchTextbox = new Element(driver, "tvSearchText", "Search for a point of interest");
        poiSearchGlassIcon = new Element(driver, "ivSearch", "");
        greetingText = new Element(driver, "tvGreetText", "");
        weatherIcon = new Element(driver, "ivWeather", "");
        weatherTemperature = new Element(driver, "tvTemperature", "");
        remoteCommandStatusLabel = new Element(driver, "tvRemoteStatus");
        Remotelock = new Element(driver, "btnRemoteLock");
        successfulpopup = new Element(driver, "message");
        vehiclenickname = new Element(driver, "tvIntroduce");
        DKicon = new Element(driver, "ivOpenApp");
        ConnectivityWarning = new Element(driver,"clConnectedWarning");
        SafetyWarningtitle = new Element(driver,"alertTitle");
        SafetywarningContent = new Element(driver,"message","Due to battery issue identified with your vehicle, the max charge limit of your vehicle has been limited to 80%. Please ensure your charge limit does not exceed 80%. Tap learn more for more details.");
        Dismiss = new Element(driver,"android:id/button2","Dismiss");
        LearnMore = new Element(driver,"android:id/button1","Learn More");
    }


    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Checks if this screen is currently showing on the app.
     *
     * @param waitTime Time (in seconds) to implicitly wait
     * @return True if it is showing, false if it isn't. Default return is false.
     */
    public boolean isPresent(long waitTime) {
        return remoteHistoryButton.elementExists(waitTime);
    }

    public void clickOnHamburgerMenuButton() {
        tap.elementByXpath("//*[@content-desc='Navigate up']", 7000);
    }

    public void clickOnRemoteActionsButton() {
        remoteActionsButton.tap(3000);
    }

    public void clickOnCarCareButton() {
        carCareButton.tap(3000);
    }

    public String greetingTextXpath = "//*[@class='android.widget.TextView' and ./parent::*[@resource-id='com.stationdm.bluelink:id/tvGreetText']]";

    public void clickOnMapsButton() {
        mapsButton.tap(3000);
    }

    public boolean isHyundaiVirtualAssistantHeaderPresent() {
        return elementExists.byXpath("//*[@class='android.view.View' and @text='Hyundai Virtual Assistant']");
    }

    public boolean isSecondaryDriverFirstNamePresent() {
        return elementExists.byXpath("(*//*[@class='android.widget.TextView' and @text='First Name:'])[1]");
    }

    public boolean isTermsofUsePagePresent() {
        return elementExists.byXpath("//*[@class='android.view.View' and @text='Terms of Use']");
    }

    public void clickonTermsofUse() {
        tap.elementByXpath("//*[@class='android.view.View' and @text='Terms of Use']");
    }

    public boolean isTermsofUse() {
        return elementExists.byXpath("//*[@class='android.view.View' and @text='Terms of Use']");
    }

    public boolean bluelinkbutton() {
        return elementExists.byXpath("//*[@class='android.view.View' and @text='Blue Link']");
    }

    public void clickOnBlueLink() {
        tap.elementByXpath("//*[@class='android.view.View' and @text='Blue Link']");
    }

    public String blueLinkContent = "//*[@class='android.view.View' and contains(text(),'Please select from the below topics to learn more')]";

    public String blueLinkContentText() {
        System.out.println(blueLinkContent);
        return blueLinkContent;
    }

    public String LogoRemoteActions = "(//android.widget.LinearLayout[@id='remoteFragment']//*[@class='android.widget.ImageView'])";

    public boolean RemoteActionLogoPresent() { return elementExists.byXpath(LogoRemoteActions); }

    public String ConnectivityWarningText="//*[@class='android.widget.TextView' and contains(text(),'Bluelink')']";

    public Boolean ConnectivityWarningBluelinkpresent(){
        return elementExists.byXpath(ConnectivityWarningText);
    }

    public String SafetyWarningText="//android.widget.TextView[@resource-id='android:id/message' and @text='Due to battery issue identified with your vehicle, the max charge limit of your vehicle has been limited to 80%. Please ensure your charge limit does not exceed 80%. Tap learn more for more details.']";

    public boolean SafetyWarningContentisPresent(){
        return elementExists.byXpath(SafetyWarningText);
    }
}
