package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the MVR screen which is accessible through the Car Care menu in the home screen.
 */
public class MonthlyVehicleReportScreen extends Screen {
    public Element backButton;
    public Element previousReportsButton;
    public Element vehicleNameTextView;
    public Element vinTextView;
    public Element mileageTextView;
    public Element reportDateTextView;
    public Element actionStatusTextView;
    public Element scheduleServiceButton;
    public Element smartCruiseControlTextView;
    public Element electronicallyControlledSuspensionTextView;
    public Element electronicPowerBrakeTextView;
    public Element electronicPowerSteeringTextView;
    public Element laneDepartureWarningSystemTextView;
    public Element electricVehicleTextView; // Electric vehicle only
    public Element airBagTextView;
    public Element antiLockBrakingSystemTextView;
    public Element transmissionTextView;
    public Element engineTextView;
    public Element subTitleText;
    public Element vehicleHealthData;
    public Element scheduleService;

    public MonthlyVehicleReportScreen(AppiumDriver driver) {
        super(driver);
        subTitleText = new Element(driver, "subTitleTxt","Monthly Vehicle Health Report");
        vehicleHealthData = new Element(driver, "tvNoData","No Monthly Vehicle Health Report Available");
        backButton = new Element(driver,"main_title_left_icon");
        previousReportsButton = new Element(driver,"main_title_right_icon");
        vehicleNameTextView = new Element(driver,"mvr_header_title_txt");
        vinTextView = new Element(driver,"mvr_header_vin_txt");
        mileageTextView = new Element(driver,"mvr_header_distance_txt");
        reportDateTextView = new Element(driver,"mvr_header_report_date_txt");
        actionStatusTextView = new Element(driver,"mvr_header_status_txt");
        scheduleServiceButton = new Element(driver,"iv_dr_scheduleservice");
        smartCruiseControlTextView = new Element(driver,"","SMART CRUISE CONTROL");
        electronicallyControlledSuspensionTextView = new Element(driver,"","ELECTRONICALLY CONTROLLED SUSPENSION");
        electronicPowerBrakeTextView = new Element(driver, "", "ELECTRONIC POWER BRAKE");
        electronicPowerSteeringTextView = new Element(driver,"","ELECTRONIC POWER STEERING ");
        laneDepartureWarningSystemTextView = new Element(driver, "", "LANE DEPARTURE WARNING SYSTEM");
        electricVehicleTextView = new Element(driver,"","ELECTRIC VEHICLE");
        airBagTextView = new Element(driver,"","AIR BAG");
        antiLockBrakingSystemTextView = new Element(driver,"","ANTI-LOCK BRAKING SYSTEM");
        transmissionTextView = new Element(driver,"","TRANSMISSION ");
        engineTextView = new Element(driver,"","ENGINE");
        scheduleService = new Element(driver, "tvTitle");
    }

    /**
     * Scroll to the top of the screen.
     */
    public void scrollToTop() {
        swipe.downCustomPercentage(4,.3,.3,.6);
    }

    /**
     * Scroll to the bottom of the screen.
     */
    public void scrollToBottom() {
        swipe.upCustomPercentage(1,.75,.5,.5);
    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return subTitleText.elementExists();
    }
}