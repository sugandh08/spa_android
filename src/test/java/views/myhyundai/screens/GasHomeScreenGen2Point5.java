package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.HomeScreen;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

/**
 * The home screen that is displayed when logged in to a Gas vehicle or an HEV vehicle. This screen
 * will not display if logged in to an AEEV or PHEV vehicle.
 */
public class GasHomeScreenGen2Point5 extends HomeScreen {
    public RemoteCarControlsGen2Point5 remoteCommands;
    public Element parkingMeterStatusTextView;
    public Element vehicleStatusButton;
    public Element RemoteStartButton;

    /**
     * GasHomeScreen constructor
     * @param driver Appium driver
     */
    public GasHomeScreenGen2Point5(AppiumDriver driver) {
        super(driver);

        remoteCommands = new RemoteCarControlsGen2Point5(driver);
        parkingMeterStatusTextView = new Element(driver,"home_parking_set_txt");
        vehicleStatusButton = new Element(driver, "center_img");
        RemoteStartButton = new Element(driver,"btnRemoteStart");
    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        // Is different from electric because elements are accessed from the home screen, not the home button.
        return isPresent(5);
    }

    /**
     * Test if the screen is present
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        // avoid stale element
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            //
        }
        return remoteCommands.start_stopButton.elementExists(waitTime);
    }
}