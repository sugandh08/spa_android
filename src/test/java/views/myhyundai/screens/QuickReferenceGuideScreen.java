package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Quick Reference guide PDF view screen that is accessible through the About & Support screen
 */
public class QuickReferenceGuideScreen extends Screen {
    public Element subtitle;

    public QuickReferenceGuideScreen(AppiumDriver driver) {
        super(driver);

        subtitle = new Element(driver, "sub_title_txt");
    }

    public boolean isPresent() {
        return subtitle.getTextValue().equalsIgnoreCase("quick reference guide");
    }
}
