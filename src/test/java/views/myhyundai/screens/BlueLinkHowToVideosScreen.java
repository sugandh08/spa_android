package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Blue Link How-To videos screen that is reached through the About & Support screen.
 */
public class BlueLinkHowToVideosScreen extends Screen {
    public Element subtitle;
    public Element roadsideAssistance;
    public Element carFinder;
    public Element stolenVehicleRecovery;
    public Element destinationSearchPoweredByGoogle;
    public Element emergencyAssistance;
    public Element accountSettings;
    public Element remoteLockUnlockAndStartInHotWeather;
    public Element remoteLockUnlockAndStartInColdWeather;

    public BlueLinkHowToVideosScreen(AppiumDriver driver) {
        super(driver);
        subtitle = new Element(driver, "subTitleTxt","Blue Link How-To Videos");
        roadsideAssistance = new Element(driver,"", "Roadside Assistance");
        carFinder = new Element(driver,"","Car Finder");
        stolenVehicleRecovery = new Element(driver,"","Stolen Vehicle Recovery");
        destinationSearchPoweredByGoogle = new Element(driver,"","Destination Search powered by Google™");
        emergencyAssistance = new Element(driver,"","Emergency Assistance");
        accountSettings = new Element(driver,"","Account Settings");
        remoteLockUnlockAndStartInHotWeather = new Element(driver,"","Remote Lock, Unlock and Start in Hot Weather");
        remoteLockUnlockAndStartInColdWeather = new Element(driver,"","Remote Lock, Unlock and Start in Cold Weather");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subtitle.getTextValue().equals("Blue Link How-To Videos");
    }
}
