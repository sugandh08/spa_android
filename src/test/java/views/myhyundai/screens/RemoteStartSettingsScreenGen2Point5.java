package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Remote Start settings screen that is accessible on Gen 2 Gas or HEV vehicles by
 * tapping the Remote Start button.
 * This screen is NOT available on PHEV or AEEV vehicles.
 */
public class RemoteStartSettingsScreenGen2Point5 extends Screen {
    public Element backButton;
    public Element subtitleTextView;
    public Element engineDurationTimeTextView;
    public Element engineDurationExpandButton;
    public Element temperatureToggleButton;
    public Element frontDefrosterToggleButton;
    public Element heatedFeaturesToggleButton;
    public Element seatsToggleButton;
    public Element heatedSeat_1;
    public Element ventseat_1;
    public Element seatAllOffButton;
    public Element seatDownImage;
    public Element seatarrow;

    public Element submitButton;

    // Xpaths for each of the scroll wheels because the interactions we have with them are
    // only done through GuiController, not here. The only thing we care about is using these Xpaths
    // to get the selenium Rectangle object of the scroll wheel.
    public final String ENGINE_DURATION_XPATH = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/engine_wheel']" +
            "/android.widget.LinearLayout[@index='0']" +
            "/android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/optionspicker']" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    public final String TEMPERATURE_XPATH = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/tempwheel']" +
            "/android.widget.LinearLayout[@index='0']" +
            "/android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/optionspicker']" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";

    public RemoteStartSettingsScreenGen2Point5(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        subtitleTextView = new Element(driver, "sub_title_txt");
        engineDurationTimeTextView = new Element(driver,"tv_remote_engine_duration_time");
        engineDurationExpandButton = new Element(driver, "remote_start_engine_check_img");
        temperatureToggleButton = new Element(driver, "temperature_switch");
        frontDefrosterToggleButton = new Element(driver, "remote_start_defrost_switch");
        heatedFeaturesToggleButton = new Element(driver, "remote_start_heat_switch");
        seatsToggleButton=new Element(driver,"seats_switch");
        seatDownImage=new Element(driver,"seats_down_img");
        heatedSeat_1=new Element(driver,"seat_heat_img");
        ventseat_1 = new Element(driver,"seat_vent_img");
        seatAllOffButton=new Element(driver,"vehicle_seat_all_off_txt");
        submitButton = new Element(driver, "remote_setting_submit");
        seatarrow = new Element(driver,"ivSeatsDown");
    }

    /**
     * Sets the toggle states for Temperature, Front Defroster, and Heated Features.
     * @param temperatureToggle State of Temperature
     * @param frontDefrosterToggle State of Front Defroster
     * @param heatedFeaturesToggle State of Heated Features
     */
    public void setStartSettingsStates(boolean temperatureToggle, boolean frontDefrosterToggle, boolean heatedFeaturesToggle){
        if(temperatureToggleButton.getCheckedValue() ^ temperatureToggle){
            temperatureToggleButton.tap();
        }

        if(frontDefrosterToggleButton.getCheckedValue() ^ frontDefrosterToggle){
            frontDefrosterToggleButton.tap();
        }

        if(heatedFeaturesToggleButton.getCheckedValue() ^ heatedFeaturesToggle){
            heatedFeaturesToggleButton.tap();
        }
    }

    /**
     * Sets the toggle states for Temperature, Defrost, Heated Features and Seats
     * @param temperatureToggle State of Temperature
     *  @param frontDefrosterToggle State of Temperature
     * @param heatedFeaturesToggle State of Temperature
     * @param seatsToggle State of Defrost
     */
    public void setStartWithSeatsSettingsStates(boolean temperatureToggle,boolean frontDefrosterToggle, boolean heatedFeaturesToggle, boolean seatsToggle) {
        if (temperatureToggleButton.getCheckedValue() ^ temperatureToggle) {
            temperatureToggleButton.tap();
        }

        if (frontDefrosterToggleButton.getCheckedValue() ^ frontDefrosterToggle) {
            frontDefrosterToggleButton.tap();
        }

        if (heatedFeaturesToggleButton.getCheckedValue() ^ heatedFeaturesToggle) {
            heatedFeaturesToggleButton.tap();
        }

        if (seatsToggleButton.getCheckedValue() ^ seatsToggle) {
            seatsToggleButton.tap();
        }
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equalsIgnoreCase("Remote Start Settings");
    }
}
