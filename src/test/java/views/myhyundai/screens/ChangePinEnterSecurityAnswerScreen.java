package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the security answer entry screen that is part of the PIN reset process
 */
public class ChangePinEnterSecurityAnswerScreen extends Screen {
    public Element subtitleTextView;
    public Element answerTitleTextView;
    public Element enterSecurityAnswerEditText;
    public Element submitButton;

    public ChangePinEnterSecurityAnswerScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "sub_title_txt");
        answerTitleTextView = new Element(driver, "", "ANSWER");
        enterSecurityAnswerEditText = new Element(driver, "answer");
        submitButton = new Element(driver, "remote_setting_submit");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Change PIN") && answerTitleTextView.elementExists();
    }
}
