package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Preferred Contact Information Screen that is accessible through the Settings screen,
 * which is accessible through the Side menu.
 */
public class PreferredContactInformationScreen extends Screen {
    public Element subTitleTextView;
    public Element screenTitleTextView;
    public Element preferredContactNicknameTextView;
    public Element preferredContactEmailTextView;
    public Element preferredContactPhoneNumberTextView;

    public PreferredContactInformationScreen(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "sub_title_txt");
        screenTitleTextView = new Element(driver, "", "PREFERRED CONTACT INFORMATION");
        preferredContactNicknameTextView = new Element(driver, "prefered_contact_nickname");
        preferredContactEmailTextView = new Element(driver, "prefered_contact_email");
        preferredContactPhoneNumberTextView = new Element(driver, "prefered_contact_phone");
    }

    public boolean isPresent() {
        return screenTitleTextView.elementExists();
    }
}
