package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Charge Schedule set screen that is accessible on PHEV and AEEV vehicles by tapping
 * the Charge Schedule on the home screen.
 */
public class ChargeManagementScreen extends Screen {

    public Element subtitleTextView;
    public Element submitButton;
    public Element startDrivingSection;
    public Element departureTime1Text;
    public Element climateToggle;
    public Element offPeakChargingToggle;
    public Element offPeakSchedule;
    public Element offPeakWeekend;
    public Element scheduleWeekday;
    public Element scheduleWeekend;

    public ChargeManagementScreen(AppiumDriver driver) {
        super(driver);
        scheduleWeekday = new Element(driver,"tvScheduleWeekdaysTimeArea");
        scheduleWeekend = new Element(driver,"tvScheduleWeekendsTimeArea");
        subtitleTextView = new Element(driver, "subTitleTxt");
        submitButton = new Element(driver, "rippleSubmit");
        startDrivingSection = new Element(driver, "departure_time_ll");
        departureTime1Text = new Element(driver, "departure_time1_txt");
        climateToggle = new Element(driver, "climate_title_switch");
        offPeakChargingToggle = new Element(driver, "offpeak_title_switch");
        offPeakSchedule = new Element(driver,"sbOffPeakSchedule");
        offPeakWeekend = new Element(driver,"tvScheduleWeekendsTitle");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subtitleTextView.getTextValue(waitTime).contains("Schedule");
    }

    public void setClimateAndOffPeakChargingStates(boolean climateToggleState, boolean offPeakChargingToggleState) {
        if (climateToggle.getCheckedValue() ^ climateToggleState) {
            climateToggle.tap();
        }
        if (offPeakChargingToggle.getCheckedValue() ^ offPeakChargingToggleState) {
            offPeakChargingToggle.tap();
        }


    }

}
