package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Speed Alert screen that is accessed through the Alert Settings screen.
 */
public class SpeedAlertScreen extends Screen {
    public Element subtitleTextView;
    public Element informationButton;
    public Element speedLimitButton;
    public Element speedLimitTextView;
    public Element saveButton;
    public Element cancelButton;
    public Element doneButton;

    public final String speedLimitWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";

    public SpeedAlertScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "subTitleTxt");
        informationButton = new Element(driver, "sub_title_right_icon");

        speedLimitButton = new Element(driver, "vSpeedClickView");
        speedLimitTextView = new Element(driver, "tvSpeedValue");
        saveButton = new Element(driver, "llSave");
        cancelButton = new Element(driver, "cancel");
        doneButton = new Element(driver, "done");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Speed Alert") && speedLimitButton.elementExists();
    }
}
