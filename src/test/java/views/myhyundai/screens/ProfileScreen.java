package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class ProfileScreen extends Screen {
    // Note: Emergency contact uses a recycler view, may need to redo the calls
    // Figure out how to make the emergency references

    public Element subTitleTextView;
    public Element backButton;
    public Element usernameTextView;
    public Element streetAddressTextView;
    public Element cityStatePostalCodeTextView;
    public Element phoneLandlineTextView;
    public Element phoneMobileTextView;
    public Element emergencyContactNameTextView;
    public Element emergencyContactLandlineTextView;
    public Element emergencyContactMobileTextView;
    public Element emergencyContactEmailTextView;
    public Element hyundaiRewardsTextView;
    public Element hyundaiAccessoriesTextView;

    public ProfileScreen(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "sub_title_txt");
        backButton = new Element(driver,"main_title_left_icon");
        usernameTextView = new Element(driver, "tv_user_name");
        streetAddressTextView = new Element(driver,"tv_street");
        cityStatePostalCodeTextView = new Element(driver,"tv_city_region_postalCode");
        phoneLandlineTextView = new Element(driver,"tv_user_phone_num1");
        phoneMobileTextView = new Element(driver,"tv_user_phone_num2");
        emergencyContactNameTextView = new Element(driver,"ecitem_contact_name");
        emergencyContactLandlineTextView = new Element(driver,"ecitem_user_phone_num1");
        emergencyContactMobileTextView = new Element(driver,"ecitem_user_phone_num2");
        emergencyContactEmailTextView = new Element(driver,"ecitem_user_email");
        hyundaiRewardsTextView = new Element(driver,"tv_user_rewards");
        hyundaiAccessoriesTextView = new Element(driver,"profile_accessories_txt");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().equals("MyHyundai Profile");
    }
}
