package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import views.Screen;

/**
 * This is the Bluetooth & Multimedia Support web site that is reached through the About & Support screen.
 * It is loaded in an external internet browser.
 */
public class BluetoothAndMultimediaSupportScreen extends Screen {
    public BluetoothAndMultimediaSupportScreen(AppiumDriver driver) {
        super(driver);
    }

    public boolean isPresent() {
        return false;
    }
}
