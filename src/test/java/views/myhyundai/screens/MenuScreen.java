package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.apache.tools.ant.taskdefs.Echo;
import views.Screen;

/**
 * This is the menu screen that is accessible by pressing the hamburger icon (three horizontal bars)
 * at the top left of the app while in the home screen, or by swiping right from the left edge of the
 * app while in the home screen.
 * NOTE that this is also called the "Side" menu.
 */
public class MenuScreen extends Screen 
{
    public Element closeButton;

    public Element alertSettingsButton;
    public Element scheduleServiceButton;
    public Element Bluelinksubscription;
    public Element accessoriesButton;
    public Element parkingMeterTimerButton;
    public Element callRoadsideButton;
    public Element aboutAndSupportButton;
    public Element settingsButton;
    public Element switchVehicleButton;
    public Element signOutButton;
    public Element viewProfileButton;
    public Element odometerTextView;
    public Element VirtualAssistant;
    public Element settings;
    public Element DigitalKey;

    public Element userNameTextView;
    public Element vinTextView;
    public Element vehicleNameTextView;
    public  Element userProfileImage;
    public Element CallButton;
    public Element CallingScreen;
    public Element CancelButton;
    public Element MyDealer;
    public Element EditBluelinkPackage;

    /**
     * MenuScreen constructor
     * @param driver Appium driver
     */
    public MenuScreen(AppiumDriver driver) {
        super(driver);

        closeButton = new Element(driver, "ivClose");

        alertSettingsButton = new Element(driver,"tvAlertSettings","Alert Settings");
        scheduleServiceButton = new Element(driver,"tvScheduleService","Schedule Service");
        Bluelinksubscription = new Element(driver,"","Bluelinksubscription");
        accessoriesButton = new Element(driver,"tvAccessories","Accessories");
        parkingMeterTimerButton= new Element(driver, "tvParkingMeterTimer","Parking Meter Timer");
        callRoadsideButton = new Element(driver,"btnCallRoadside","Call Roadside");
        aboutAndSupportButton = new Element(driver,"tvAboutAndSupport","About & Support");
        settingsButton = new Element(driver, "tvSettings", "Settings");
        switchVehicleButton = new Element(driver,"tvSwitchVehicle","Switch Vehicle");
        signOutButton = new Element(driver,"tvSignOut","Sign Out");
        viewProfileButton = new Element(driver,"tvViewProfile","View Profile");
        userNameTextView = new Element(driver, "tvUserName");
        vinTextView = new Element(driver, "tvVin");
        vehicleNameTextView = new Element(driver, "tvVehicleName","");
        userProfileImage = new Element(driver, "ivHeadImage","");
        odometerTextView =new Element(driver,"tvVin");
        VirtualAssistant = new Element(driver,"tvVirtualAssistant");
        settings = new Element(driver,"subTitleTxt");
        DigitalKey = new Element(driver,"tvDigitalKey");
        CallButton = new Element(driver,"button1","Call");
        CancelButton = new Element(driver,"button2");
        MyDealer = new Element(driver,"tvPreferDealer");
        EditBluelinkPackage = new Element(driver,"tvbluelinkEdit");
    }

    /**
     * Scroll to bottom of the menuButton to make sure call roadside button is available.
     */
  //  public void scrollToBottom() {
    //    swipe.upCustomPercentage(1,.75,.3,.3);
    //}

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (viewProfileButton.elementExists() && viewProfileButton.getTextValue().equals("View Profile"));
    }

    public boolean CallRoadsideScreenisPresent(){

        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='Call Roadside Assistance']");
    }


private String xpathOfCall ="//*[@class='android.widget.Button' and @text='Call']";
    public void TapCallButton()
    {
        tap.elementByXpath(xpathOfCall);
    }
    public boolean CallBtnExists()
    {
        return elementExists.byXpath(xpathOfCall);
    }
private String xpathOfCancel ="//*[@class='android.widget.Button' and @text='Cancel']";
    public void TapCancelButton() {tap.elementByXpath(xpathOfCancel);}

    public boolean CancelBtnExists()
    {
        return elementExists.byXpath(xpathOfCancel);
    }
    private String xpathOfCallingScreen ="//*[@class='android.widget.TextView' and @text='Calling…']";
    public boolean callingScreenExists()
    {
        return elementExists.byXpath(xpathOfCallingScreen);
    }
}
