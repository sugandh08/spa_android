package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class EditPermissionScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public Element backButton;
    public Element submitButton;
    public Element cancelButton;


    public EditPermissionScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "subTitleTxt");
        backButton = new Element(driver, "main_title_left_icon");
        submitButton = new Element(driver, "btnSubmit");
        cancelButton = new Element(driver, "btnCancel");

    }


    /**
     * Builds an xpath to the Yes Radio button element using the Setting Name    *
     *
     * @param settingName The Setting Name
     *                    * @return The xpath to the Yes Radio button element
     */
    public String getXpathOfYesToggle(String settingName) {
        return "//*[@resource-id='com.stationdm.bluelink:id/rbPermissionYes' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='"+settingName+"']]]";
    }

    /**
     * Builds an xpath to the No Radio button element using the Setting Name    *
     *
     * @param settingName The Setting Name
     *                    * @return The xpath to the No Radio button element
     */
    public String getXpathOfNoToggle(String settingName) {
        return "//*[@resource-id='com.stationdm.bluelink:id/rbPermissionNo' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='"+settingName+"']]]";
    }

    public String clickYesNoRadioButton(String settingName) {


        if (attribute.getCheckedValueByXpath(getXpathOfYesToggle(settingName))) {

            tap.elementByXpath(getXpathOfNoToggle(settingName));

            return "no";


        } else {
            tap.elementByXpath(getXpathOfYesToggle(settingName));
            return "yes";
        }
    }


     public boolean getCheckedValueOfYesRadioButton(String settingName)
     {
         return attribute.getCheckedValueByXpath(getXpathOfYesToggle(settingName));
     }

    public boolean getCheckedValueOfNoRadioButton(String settingName)
    {
        return attribute.getCheckedValueByXpath(getXpathOfNoToggle(settingName));
    }


    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().contains("Edit Permission");
    }


}
