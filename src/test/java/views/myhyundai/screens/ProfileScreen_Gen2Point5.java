package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.openqa.selenium.By;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class ProfileScreen_Gen2Point5 extends Screen 
{
    // Note: Emergency contact uses a recycler view, may need to redo the calls
    // Figure out how to make the emergency references

    public Element subTitleTextView;
    public Element backButton;
    public Element usernameTextView;
    public Element streetAddressTextView;
    public Element cityStatePostalCodeTextView;
    public Element phoneLandlineTextView;
    public Element phoneMobileTextView;
    public Element emergencyContactNameTextView;
    public Element emergencyContactLandlineTextView;
    public Element emergencyContactMobileTextView;
    public Element emergencyContactEmailTextView;
    public Element hyundaiRewardsTextView;
    public Element hyundaiAccessoriesTextView;
    public Element driversArrowButton;
    public Element driverNameTextView;
    public Element driverAddressAndEmailTextView;
    public Element myDriverSettingsButton;
    public Element inviteAdditionalDriverButton;
    public Element primaryDriverAddress;
    public Element ReadMoreTxt;
    public Element BluelinkPackages;
    public Element renewalDateTxt;
    public Element Vehiclestatus;
    public Element Vehiclestatusscreen;
    public Element DigitalKey;
    public Element EmergencyContactInformation;
    public Element EditEmergencyContact;
    public Element editVehicleinformation;
    public Element Vehiclenickname;
    public Element save;
    public Element stateoptions;
    public Element state;
    public Element editcontactinformation;


    //Contact Information Elements:
    public Element notificationEmail;
    public Element mobileNumber;

   public Element deleteInviteTextView;
   public Element yesButton;
   public Element cancelButton;
   public Element editPermission;
   public Element additionalDriverCount;



    public ProfileScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "subTitleTxt", "MyHyundai Profile");
        backButton = new Element(driver, "main_title_left_icon");
        usernameTextView = new Element(driver, "tv_user_name", "Hata Test");
        streetAddressTextView = new Element(driver, "tv_street");
        cityStatePostalCodeTextView = new Element(driver, "tv_city_region_postalCode");
        phoneLandlineTextView = new Element(driver, "tv_user_phone_num1");
        phoneMobileTextView = new Element(driver, "tv_user_phone_num2");
        emergencyContactNameTextView = new Element(driver, "ecitem_contact_name");
        emergencyContactLandlineTextView = new Element(driver, "ecitem_user_phone_num1");
        emergencyContactMobileTextView = new Element(driver, "ecitem_user_phone_num2");
        emergencyContactEmailTextView = new Element(driver, "ecitem_user_email");
        hyundaiRewardsTextView = new Element(driver, "tv_user_rewards");
        hyundaiAccessoriesTextView = new Element(driver, "profile_accessories_txt");
        primaryDriverAddress = new Element(driver, "tvUserAddress","");

        driversArrowButton = new Element(driver, "profile_settings_arrow_img");
        driverNameTextView = new Element(driver, "item_profile_settings_user_name_txt");
        driverAddressAndEmailTextView = new Element(driver, "item_profile_settings_address_txt");
        myDriverSettingsButton = new Element(driver, "btnDriverSetting");
        inviteAdditionalDriverButton=new Element(driver,"btnInviteDrivers");
        editPermission = new Element(driver,"btnEditPermission");
        additionalDriverCount=new Element(driver,"tvNum");
        ReadMoreTxt=new Element(driver, "tvReadMore");
        BluelinkPackages= new Element(driver, "","BLUELINK PACKAGES");
        renewalDateTxt=new Element(driver,"tvContent");
        Vehiclestatus = new Element(driver, "btnVehicleStatus");
        Vehiclestatusscreen = new Element(driver,"subTitleTxt","Vehicle Status");
        DigitalKey = new Element(driver,"ivOpenApp");
        EmergencyContactInformation = new Element(driver,"tvTitle");
        EditEmergencyContact = new Element(driver,"tvEmergencyEdit");
        editVehicleinformation = new Element(driver,"tvVehicleEdit");
        Vehiclenickname = new Element(driver,"etNickName");
        save = new Element(driver,"tvSave");
        stateoptions = new Element(driver,"options1");
        editcontactinformation = new Element(driver,"tvContactEdit");
        state = new Element(driver,"","State*");

        //Contact info
        notificationEmail = new Element(driver, "tvNotificationEmailInput","");
        mobileNumber = new Element(driver, "tvMobileInput","");

        deleteInviteTextView=new Element(driver,"message","Are you sure you want to Delete Invitation?");
        yesButton=new Element(driver,"","Yes");
        cancelButton=new Element(driver,"","Cancel");


    }


    /**
     * Test if the screen is present. Uses standard wait time
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     *
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("MyHyundai Profile");

    }
    public void ClickonHamburgerMenuButton()
    {
        tap.elementByXpath("//*[@class='android.widget.ImageButton'][1]");
    }


    /**
     * Builds an xpath to the driver Address and Email element using the driver type(primary/secondary)
     *
     * @param driverType The relative driver type to the Address and email element
     * @return The xpath to the driver Address and Email element
     */
    public String getXpathOfDriverAddressAndEmail(String driverType) {

        return "/" +
                "/android.widget.TextView[@text='" + driverType + "']" +
                "/.." +
                "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/item_profile_settings_address_txt']";
    }

    /**
     * Builds an xpath to the Additional Driver Email element using the Additional Driver Number     *
     * @param additionalDriverNumber The additional Driver number
     * * @return The xpath to the Additional driver Email element
     */
    public String getXpathOfAdditionalDriverEmail(String additionalDriverNumber) {

        return "/" +
                "/android.widget.TextView[@text='" + additionalDriverNumber + "']" +
                "/.." +"/android.widget.LinearLayout/android.widget.LinearLayout"+
                "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvEmail']";
    }

    /**
     * Builds an xpath to the Additional Driver Edit Permissions button element using the Additional Driver Number     *
     * @param additionalDriverNumber The additional Driver number
     * * @return The xpath to the Additional driver  Edit Permissions button  element
     */
    private String getXpathOfAdditionalDriverEditPermissionsButton(String additionalDriverNumber) {

        return "/" +
                "/android.widget.TextView[@text='" + additionalDriverNumber + "']" +
                "/.." +"/android.widget.LinearLayout"+
                "/android.widget.Button[@resource-id='com.stationdm.bluelink:id/btnEditPermission']";
    }

    /**
     * Builds an xpath to the Additional Driver Remove Driver button element using the Additional Driver Number     *
     * @param additionalDriverNumber The additional Driver number
     * * @return The xpath to the Additional driver Remove Driver button  element
     */
    private String getXpathOfAdditionalDriverRemoveDriverButton(String additionalDriverNumber) {

        return "/" +
                "/android.widget.TextView[@text='" + additionalDriverNumber + "']" +
                "/.."+"/android.widget.LinearLayout"+
                "/android.widget.Button[@resource-id='com.stationdm.bluelink:id/bt_remove_driver']";
    }
    private String getXpathOfAdditionalDriverRemoveDriverButtonUsingEmail(String email) {

        return "/" +
                "/android.widget.TextView[@text='"+email+"']" +
                "/.."+"/.."+"/.."+"/android.widget.LinearLayout"+
                "/android.widget.Button[@resource-id='com.stationdm.bluelink:id/bt_remove_driver']";
    }
    public void clickEditPermissionsButton(String additionalDriverNumber)
    {
       tap.elementByXpath(getXpathOfAdditionalDriverEditPermissionsButton(additionalDriverNumber));
    }

    public void clickRemoveDriverButton(String additionalDriverNumber)
    {
        tap.elementByXpath(getXpathOfAdditionalDriverRemoveDriverButton(additionalDriverNumber));
    }

    public void removeAdditionalDriverIfAlreadyMaxDriverAdded(){

        if(elementExists.byXpath("//android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvNum' and @text='4'])")){
            tap.elementByXpath("//*[@resouce-id='com.stationdm.bluelink:id/btnRemoveDriver']");
        }
   }

    public void clickRemoveDriverButtonUsingEmail(String email)
    {
        tap.elementByXpath(getXpathOfAdditionalDriverRemoveDriverButtonUsingEmail(email));
    }

    public void clickRemoveDriverButton()
    {
        if(elementExists.byXpath("//^[@text='4']"))
            {
            tap.elementByXpath("//android.widget.Button[@resource-id='com.stationdm.bluelink:id/btnRemoveDriver' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='4']]]");
    }
    }

    public String getxPathOfIndexUsingEmail(String email)
    {
        return "/" +
                "/android.widget.TextView[@text='"+email+"']" +
                "/.."+"/.."+"/.."+
                "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvNum']";
    }
    private String androidBackBtn ="//*[@class='android.widget.ImageButton']";
    public void clickOnAndroidBtn()
    {
        tap.elementByXpath(androidBackBtn);
    }
}

