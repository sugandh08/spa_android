package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Email App Support screen that is accessed through the About & Support screen.
 */
public class EmailAppSupportScreen extends Screen {
    public Element backButton;
    public Element subtitleTextView;

    public Element vehicleVinSelectionText; // pull VIN's from account
    public Element incidentFeatureNameSelectionText;
    public Element dateSelectionText;
    public Element timeSelectionText;
    public Element keyLocationSelectionText;
    public Element connectionTypeSelectionText;
    public Element notesEditText;
    public Element sendButton;

    public Element nextButton;
    public Element finishButton;

    public EmailAppSupportScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        subtitleTextView = new Element(driver, "sub_title_txt");

        vehicleVinSelectionText = new Element(driver,"tv_vehicle_vin");
        incidentFeatureNameSelectionText = new Element(driver,"tv_feature_name");
        dateSelectionText = new Element(driver,"tv_date");
        timeSelectionText = new Element(driver,"tv_time");
        keyLocationSelectionText = new Element(driver,"tv_key_location");
        connectionTypeSelectionText = new Element(driver,"tv_connection_type");
        notesEditText = new Element(driver,"et_notes");
        sendButton = new Element(driver,"ll_email_submit");

        nextButton = new Element(driver, "ll_bottom_next");
        finishButton = new Element(driver, "ll_bottom_finish");
    }

    public boolean isPresent() {
        return (subtitleTextView.getTextValue().equals("Email App Support") &&
                vehicleVinSelectionText.elementExists());
    }
}
