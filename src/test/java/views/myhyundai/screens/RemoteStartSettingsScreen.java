package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Remote Start settings screen that is accessible on Gen 2 Gas or HEV vehicles by
 * tapping the Remote Start button.
 * This screen is NOT available on PHEV or AEEV vehicles.
 */
public class RemoteStartSettingsScreen extends Screen {
    public Element subtitleTextView;
    public Element engineDurationTimeTextView;
    public Element engineDurationExpandButton;
    public Element temperatureToggleButton;
    public Element frontDefrosterToggleButton;
    public Element heatedFeaturesToggleButton;
    public Element submitButton;
    public Element preConditionButton;
    public Element engineDurationWheel;
    public Element temperatureWheel;
    public Element oK;
    public Element renameButton;
    public Element presetNameText;
    public Element defaultPresetText;
    public Element defaultPresetToggle;
    public Element presetstartbuttn;
    public Element seatdownbutton;
    public Element seatstogglebutton;
    public Element giaseatstogglebutton;
    public Element preconditionpage;
    public Element precondtions;


    // Xpaths for each of the scroll wheels because the interactions we have with them are
    // only done through GuiController, not here. The only thing we care about is using these Xpaths
    // to get the selenium Rectangle object of the scroll wheel.
    public final String ENGINE_DURATION_XPATH = "//android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/opvEngineWheel']";
    public final String TEMPERATURE_XPATH = "//android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/tempwheel']";

    public RemoteStartSettingsScreen(AppiumDriver driver) {
        super(driver);
        subtitleTextView = new Element(driver, "subTitleTxt", "Remote Start Settings");
        engineDurationTimeTextView = new Element(driver, "tvEngineTime");
        engineDurationExpandButton = new Element(driver, "ivEngineCheck");
        temperatureToggleButton = new Element(driver, "sbTempSwitch");
        frontDefrosterToggleButton = new Element(driver, "sbDefrostSwitch");
        heatedFeaturesToggleButton = new Element(driver, "sbHeatSwitch");
        submitButton = new Element(driver, "rippleSubmit", "");
        preConditionButton = new Element(driver, "tvPreConditions", "View pre-conditions for remote start");
        engineDurationWheel = new Element(driver, "opvEngineWheel", "");
        temperatureWheel = new Element(driver, "tempwheel", "");
        oK = new Element(driver, "btnOK");
        renameButton = new Element(driver, "tvRename", "Rename");
        presetNameText = new Element(driver, "etPresetName", "");
        defaultPresetText = new Element(driver, "tvDefaultTitle", "Default Preset");
        defaultPresetToggle = new Element(driver, "sbDefaultSwitch", "");
        presetstartbuttn = new Element(driver, "btnPresetStart");
        seatdownbutton = new Element(driver, "ivSeatsDown");
        seatstogglebutton = new Element(driver, "sbSeatsSwitch");
        giaseatstogglebutton = new Element(driver, "seat_switch");
        preconditionpage = new Element(driver,"tvHeader");
        precondtions = new Element(driver,"tvTextLine");
    }

    /**
     * Sets the toggle states for Temperature, Front Defroster, and Heated Features.
     *
     * @param temperatureToggle    State of Temperature
     * @param frontDefrosterToggle State of Front Defroster
     * @param giaseattogglebutton  State of Heated Features
     */
    public void setStartSettingsStates(boolean temperatureToggle, boolean frontDefrosterToggle, boolean giaseattogglebutton) {
        if (temperatureToggleButton.getCheckedValue() ^ temperatureToggle) {
            temperatureToggleButton.tap();
        }

        if (frontDefrosterToggleButton.getCheckedValue() ^ frontDefrosterToggle) {
            frontDefrosterToggleButton.tap();
        }

        if (giaseatstogglebutton.getCheckedValue() ^ giaseattogglebutton) {
            heatedFeaturesToggleButton.tap();
        }
    }

    public void setStartSettingsState(boolean temperatureToggle, boolean frontDefrosterToggle, boolean heatedFeaturesToggle) {
        if (temperatureToggleButton.getCheckedValue() ^ temperatureToggle) {
            temperatureToggleButton.tap();
        }

        if (frontDefrosterToggleButton.getCheckedValue() ^ frontDefrosterToggle) {
            frontDefrosterToggleButton.tap();
        }

        if (heatedFeaturesToggleButton.getCheckedValue() ^ heatedFeaturesToggle) {
            heatedFeaturesToggleButton.tap();
        }
    }

    public boolean isPresent() {

        return subtitleTextView.getTextValue().equalsIgnoreCase("Remote Start Settings");
    }

    public void clickOnBackButton() {
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate Up']");
    }

    public void renamePresets() {
        String renameText = "TestRename";
        renameButton.tap();
        clearTextField.clearTextById("etPresetName");
        presetNameText.enterText(renameText);
        renameButton.tap();
        submitButton.tap();
    }

    public void setDefaultPresetState(boolean Toggle) {
        if (defaultPresetToggle.getCheckedValue() ^ Toggle) {
            defaultPresetToggle.tap();
            submitButton.tap();
        } else {
            submitButton.tap();
        }
    }

    public String LeftHeatbutton = "//*[@id='seat_heat_img' and ./parent::*[./parent::*[./parent::*[@id='seatSettingViewFL']]]]";


    public boolean LeftHeatbuttonisPresent() {
        return elementExists.byXpath(LeftHeatbutton);
    }

    public boolean RightHeatbuttonisPresent() {
        return elementExists.byXpath("//*[@id='seat_heat_img' and ./parent::*[./parent::*[./parent::*[@id='seatSettingViewFR']]]]");
    }

    public boolean LeftcoldbuttonisPresent() {
        return elementExists.byXpath("//*[@id='seat_vent_img' and ./parent::*[./parent::*[./parent::*[@id='seatSettingViewFL']]]]");
    }

    public boolean RightcoldbuttonisPresent() {
        return elementExists.byXpath("//*[@id='seat_vent_img' and ./parent::*[./parent::*[./parent::*[@id='seatSettingViewFR']]]]");
    }

    public String submit = "//*[@text='Save']";

    public void clickonSubmit() {
        tap.elementByXpath(submit);
    }

    public boolean SavebuttonPresent(){
        return elementExists.byXpath(submit);
    }
}

