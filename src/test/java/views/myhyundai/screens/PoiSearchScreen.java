package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.apache.tools.ant.taskdefs.Echo;
import views.myhyundai.baseviews.MapScreen;

import java.security.interfaces.ECKey;

/**
 * This is the POI Search screen that is accessible from the map menus.
 */
public class PoiSearchScreen extends MapScreen {
    public Element poiSearchEditTextbox;
    public Element poiSearchGlassIcon;
    public Element mapListIcon;
    public Element pointOfInterestLabelText;
    public Element carLocateButton;
    public Element mapLocateButton;
    public Element arrowIcon;
    public Element poiLocationText;
    public Element poiLocationDistanceText;
    public Element poiFavoriteIcon;
    public Element address;
    public Element phoneNumberText;
    public Element phoneNumberIcon;
    public Element timeIcon;
    public Element timeText;
    public Element sendButton1;
    public Element sendButton2;
    public Element shareButton;
    public Element openInMapsLink;
    public Element saveAndDeleteButton;
    public Element saveAndDeleteButtonText;

    public PoiSearchScreen(AppiumDriver driver) {
        super(driver);
        poiSearchEditTextbox = new Element(driver, "etQueryWords","POI Search");
        poiSearchGlassIcon = new Element(driver, "mapMainSearchIcon","");
        mapListIcon = new Element(driver, "ivMapListIcon","");
        pointOfInterestLabelText = new Element(driver, "tvSearchText","Points of Interest");
        carLocateButton = new Element(driver, "carLocateButton","");
        mapLocateButton = new Element(driver, "locateButton","");
        arrowIcon = new Element(driver, "iv_popdetail_arrow","");
        poiLocationText = new Element(driver, "tvTitle","");
        poiLocationDistanceText = new Element(driver, "tvDistance","");
        poiFavoriteIcon = new Element(driver, "favorCheckbox","");
        address = new Element(driver, "address","");
        phoneNumberText= new Element(driver, "tvPhone","");
        phoneNumberIcon = new Element(driver, "tvPhoneIcon","");
        timeIcon= new Element(driver, "tvTimeIcon","");
        timeText = new Element(driver, "tvTime");
        sendButton1 = new Element(driver, "rl_popdetail_bottom","");
        sendButton2 = new Element(driver, "sendToCarSmallButton","");
        shareButton = new Element(driver, "tvShare","Share");
        openInMapsLink = new Element(driver, "tvDetailOpenLink","Open in Maps");
        saveAndDeleteButton = new Element(driver, "map_detail_delete_ll","");
        saveAndDeleteButtonText = new Element(driver, "saveOrDeleteTextView","");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return pointOfInterestLabelText.elementExists();
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }
}
