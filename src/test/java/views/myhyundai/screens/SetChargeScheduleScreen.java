package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Charge Schedule set screen that is accessible on PHEV and AEEV vehicles by tapping
 * the Charge Schedule on the home screen.
 */
public class SetChargeScheduleScreen extends Screen {
    public Element backButton;
    public Element subtitleTextView;
    public Element submitButton;

    public Element schedule1Toggle;
    public Element schedule2Toggle;
    public Element scheduleOffPeakToggle;
    public Element schedule1Time;
    public Element schedule2Time;
    public Element schedule1Meridian;
    public Element schedule2Meridian;
    public Element ChargeLimitSetting;
    public Element ChargeLimitText;
    public Element ACChargeLimit;
    public Element DCChargeLimit;
    public Element SetSchedulearrow;
    public Element StartDrivingone;
    public Element Save;
    public Element Sunday;
    public Element Monday;
    public Element Tuesday;
    public Element Wednesday;
    public Element Thursday;
    public Element Friday;
    public Element Saturday;
    public Element Climatetoggle;
    public Element StartDrivingtime;
    public Element offpeakChargingtoggle;
    public Element offpeaktimetext;
    public Element offpeakChargingpriority;
    public Element offpeakChargingOnly;
    public Element DCChargeLimittext;
    public Element ACChargeLimittext;
    public Element Usetheslidertext;
    public Element SafetyWarning;
    public Element Taptoset;

    public SetChargeScheduleScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver, "main_title_left_icon");
        subtitleTextView = new Element(driver, "sub_title_txt");
        submitButton = new Element(driver, "osev_schedule_submit_txt");

        schedule1Toggle = new Element(driver, "ae_schedule_1_switch");
        schedule2Toggle = new Element(driver, "ae_schedule_2_switch");
        scheduleOffPeakToggle = new Element(driver, "ae_schedule_offpeak_switch");
        schedule1Time = new Element(driver, "ae_schedule_1_time_txt");
        schedule2Time = new Element(driver, "ae_schedule_2_time_txt");
        schedule1Meridian = new Element(driver, "ae_schedule_1_timeA_txt");
        schedule2Meridian = new Element(driver, "ae_schedule_2_timeA_txt");
        ChargeLimitSetting = new Element(driver, "settings_soc_title_txt");
        ChargeLimitText = new Element(driver, "mSOCInfoTextView");
        ACChargeLimit = new Element(driver,"settings_soc_ac_ll");
        DCChargeLimit = new Element(driver,"settings_soc_dc_ll");
        SetSchedulearrow = new Element(driver,"trigger_layout_container");
        StartDrivingone = new Element(driver,"departure_edit_title_switch","Start Driving 1");
        Save = new Element(driver,"mSaveTextView");
        Sunday = new Element(driver,"sunday");
        Monday = new Element(driver,"monday");
        Tuesday = new Element(driver,"tuesday");
        Wednesday = new Element(driver,"wednesday");
        Thursday = new Element(driver,"thursday");
        Friday = new Element(driver,"friday");
        Saturday = new Element(driver,"saturday");
        Climatetoggle = new Element(driver,"climate_title_switch");
        StartDrivingtime = new Element(driver,"departure_time1_txt");
        offpeakChargingtoggle = new Element(driver,"offpeak_title_switch");
        offpeaktimetext = new Element(driver,"offpeak_time_txt");
        offpeakChargingpriority = new Element(driver,"mPriorityCheckImage");
        offpeakChargingOnly = new Element(driver,"mOnlyCheckImage");
        DCChargeLimittext = new Element(driver,"","DC Charge Limit");
        ACChargeLimittext = new Element(driver,"","AC Charge Limit");
        Usetheslidertext = new Element(driver,"tvPrompt");
        SafetyWarning = new Element(driver,"tvPrompt2");
        Taptoset = new Element(driver,"offpeak_empty_txt");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subtitleTextView.getTextValue(waitTime).equals("Set Schedule");
    }

    public  boolean isDCChargeLimitPresent()
    {
        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='DC Charge Limit']");
    }

    public boolean isACChargeLimitPresent()
    {
        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='AC Charge Limit']");
    }

    public String xpath_DrivingHour(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    }

    public String xpath_DrivingMinute(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options2']";
    }

    public String xpath_DrivingAmPm(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options3']";
    }

    public String xpath_StartDriving2 = "(//android.widget.CompoundButton[@id='departure_edit_title_switch'])[2]";

    public void tapStartDriving2()
    {
        tap.elementByXpath(xpath_StartDriving2);
    }

    public String xpathNavigateup ="//*[@class='android.widget.ImageButton' and @content-desc='Navigate up']";

    public void tapBackbutton(){
        tap.elementByXpath(xpathNavigateup);
    }
}
