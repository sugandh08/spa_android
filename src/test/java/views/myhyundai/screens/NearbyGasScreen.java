package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.MapScreen;

/**
 * This is the Nearby Gas screen that is accessible through the map menus
 */
public class NearbyGasScreen extends MapScreen {
    public Element nearbyGasStationsLabelText;
    public Element arrowIcon;
    public Element gasLocationText;
    public Element sendButton1;
    public Element sendButton2;
    public Element saveAndDeleteButton;
    public Element saveAndDeleteButtonText;
    public Element gasSearchEditTextbox;
    public Element gasSearchGlassIcon;
    public Element gasListIcon;
    public Element address;

    public NearbyGasScreen(AppiumDriver driver) {
        super(driver);
        nearbyGasStationsLabelText = new Element(driver, "tvNote","Nearby Gas Stations");
        arrowIcon = new Element(driver, "iv_popdetail_arrow","");
        gasLocationText = new Element(driver, "titleTextView","");
        sendButton1 = new Element(driver, "rl_popdetail_bottom","");
        sendButton2 = new Element(driver, "sendToCarSmallButton","");
        saveAndDeleteButton = new Element(driver, "map_detail_delete_ll","");
        saveAndDeleteButtonText = new Element(driver, "saveOrDeleteTextView","");
        gasSearchEditTextbox = new Element(driver, "etQueryWords","POI Search");
        gasSearchGlassIcon = new Element(driver, "mapMainSearchIcon","");
        gasListIcon = new Element(driver, "ivMapListIcon","");
        address = new Element(driver, "addressTextView");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent()
    {
        return searchEditText.getTextValue().equals("Gas Station");
    }
}
