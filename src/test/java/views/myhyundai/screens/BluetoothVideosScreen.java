package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Bluetooth Videos screen that is reached from the About & Support screen
 * or from the Car Care menu on the home screen.
 */
public class BluetoothVideosScreen extends Screen {

    public Element backButton;
    public Element subtitleTextView;

    // GEN 1
    public Element audioMenuButton;
    public Element touchScreenNavigationMenuButton;

    // GEN 2
    public Element displayAudioMenuButton;
    public Element navigationMenuButton;
    public Element standardMenuButton;

    // Videos that are part of multiple sections
    public Element usingTheHyundaiBluetoothSupportSiteButton;

    // AUDIO videos
    public Element downloadingPhoneContactsForTheStandardAudioSystemButton;
    public Element makingACallCallHistoryUsingTheStandardAudioSystemButton;
    public Element callingUsingTheStandardAudioSystemButton;
    public Element managingMultiplePhonesUsingStandardAudioSystemButton;
    public Element pairingAPhoneToStandardAudioSystemButton;
    public Element voiceCommandsCallingOptionsUsingStandardAudioSystemButton;

    // TOUCH SCREEN NAVIGATION videos
    public Element makingACallUsingTheTouchScreenNavigationMultimediaSystemButton;
    public Element pairingAPhoneUsingTheTouchScreenNavigationMultimediaSystemButton;
    public Element voiceCommandExamplesUsingTheTouchScreenNavigationMultimediaSystemButton;

    // DISPLAY AUDIO videos
    public Element howToPairBluetoothOnAnAndroidUsingDisplayAudioButton;
    public Element howToPairBluetoothOnAnIphoneUsingDisplayAudioButton;
    public Element howToConnectAndUseAndroidAutoVoiceCommandsButton;
    public Element howToMakeACallUsingVoiceCommandsUsingDisplayAudioButton;
    public Element howToConnectAndUseAppleCarPlayUsingVoiceCommandsButton;

    // NAVIGATION videos
    public Element howToPairBluetoothOnAnAndroidUsingNavigationButton;
    public Element howToPairBluetoothOnAnIphoneUsingNavigationButton;
    public Element howToConnectAndUseAndroidAutoUsingVoiceCommandsButton;
    public Element howToMakeACallUsingVoiceCommandsButton;
    public Element howToConnectAndUseAppleCarPlayVoiceCommandsButton;
    public Element howToEnterADestinationUsingVoiceCommandButton;

    // STANDARD videos
    public Element howToPairBluetoothOnAnAndroidUsingStandardAudioButton;
    public Element howToPairBluetoothOnAnIphoneUsingStandardAudioButton;
    public Element howToMakeACallUsingVoiceCommandsUsingStandardAudioButton;

    public BluetoothVideosScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        subtitleTextView = new Element(driver, "sub_title_txt");

        // GEN 1
        audioMenuButton = new Element(driver, "", "AUDIO");
        touchScreenNavigationMenuButton = new Element(driver, "", "TOUCH SCREEN NAVIGATION");

        // GEN 2
        displayAudioMenuButton = new Element(driver, "", "DISPLAY AUDIO");
        navigationMenuButton = new Element(driver, "", "NAVIGATION");
        standardMenuButton = new Element(driver, "", "STANDARD");

        // Videos that are part of multiple sections
        usingTheHyundaiBluetoothSupportSiteButton = new Element(driver, "", "Using the Hyundai Bluetooth Support Site");

        // AUDIO videos
        downloadingPhoneContactsForTheStandardAudioSystemButton = new Element(driver, "", "Downloading Phone Contacts for the Standard Audio System");
        makingACallCallHistoryUsingTheStandardAudioSystemButton = new Element(driver, "", "Making a Call / Call History - using the Standard Audio System");
        callingUsingTheStandardAudioSystemButton = new Element(driver, "", "Making a Call / Call History - using the Standard Audio System");
        managingMultiplePhonesUsingStandardAudioSystemButton = new Element(driver, "", "Managing Multiple Phones - using the Standard Audio System");
        pairingAPhoneToStandardAudioSystemButton = new Element(driver, "", "Pairing a Phone to the Standard Audio System");
        voiceCommandsCallingOptionsUsingStandardAudioSystemButton = new Element(driver, "", "Voice Commands Calling options using the Standard Audio System");

        // TOUCH SCREEN NAVIGATION videos
        makingACallUsingTheTouchScreenNavigationMultimediaSystemButton = new Element(driver, "", "Making a Call using the touch screen Navigation Multimedia System");
        pairingAPhoneUsingTheTouchScreenNavigationMultimediaSystemButton = new Element(driver, "", "Pairing a Phone using the touch screen Navigation Multimedia System");
        voiceCommandExamplesUsingTheTouchScreenNavigationMultimediaSystemButton = new Element(driver, "", "Voice Command examples using the touch screen Navigation Multimedia System");

        // DISPLAY AUDIO videos
        howToPairBluetoothOnAnAndroidUsingDisplayAudioButton = new Element(driver, "", "How to Pair Bluetooth on an Android Using Display Audio");
        howToPairBluetoothOnAnIphoneUsingDisplayAudioButton = new Element(driver, "", "How to Pair Bluetooth on an iPhone Using Display Audio");
        howToConnectAndUseAndroidAutoVoiceCommandsButton = new Element(driver, "", "How to Connect and Use Android Auto (Voice Commands)");
        howToMakeACallUsingVoiceCommandsUsingDisplayAudioButton = new Element(driver, "", "How to Make a Call Using Voice Commands Using Display Audio");
        howToConnectAndUseAppleCarPlayUsingVoiceCommandsButton = new Element(driver, "", "How to Connect and Use Apple CarPlay Using Voice Commands");

        // NAVIGATION videos
        howToPairBluetoothOnAnAndroidUsingNavigationButton = new Element(driver, "", "How to Pair Bluetooth on an Android Using Navigation");
        howToPairBluetoothOnAnIphoneUsingNavigationButton = new Element(driver, "", "How to Pair Bluetooth on an iPhone Using Navigation");
        howToConnectAndUseAndroidAutoUsingVoiceCommandsButton = new Element(driver, "", "How to Connect and Use Android Auto Using Voice Commands");
        howToMakeACallUsingVoiceCommandsButton = new Element(driver, "", "How to Make a Call Using Voice Commands");
        howToConnectAndUseAppleCarPlayVoiceCommandsButton = new Element(driver, "", "How to Connect and Use Apple CarPlay (Voice Commands) ");
        howToEnterADestinationUsingVoiceCommandButton = new Element(driver, "", "How to Enter a Destination Using Voice Command ");

        // STANDARD videos
        howToPairBluetoothOnAnAndroidUsingStandardAudioButton = new Element(driver, "", "How to Pair Bluetooth on an Android Using Standard Audio");
        howToPairBluetoothOnAnIphoneUsingStandardAudioButton = new Element(driver, "", "How to Pair Bluetooth on an iPhone Using Standard Audio ");
        howToMakeACallUsingVoiceCommandsUsingStandardAudioButton = new Element(driver, "", "How to Make a Call Using Voice Commands Using Standard Audio");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Bluetooth");
    }
}
