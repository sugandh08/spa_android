package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Hyundai Rewards website that is accessible through the About & Support screen.
 */
public class HyundaiRewardsScreen extends Screen {
    // Web element, only need bar functionality

    public Element backButton;
    public Element screenTitleTextView;

    public HyundaiRewardsScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        screenTitleTextView = new Element(driver,"sub_title_txt");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        // Does not test web view
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
            //
        }
        return screenTitleTextView.getTextValue().equals("Hyundai Rewards");
    }
}
