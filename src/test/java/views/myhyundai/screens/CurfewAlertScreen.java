package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the curfew alert screen that is accessible through the Alert Settings screen.
 */
public class CurfewAlertScreen extends Screen {
    public Element subtitleTextView;
    public Element informationButton;

    public Element curfewFromButton;
    public Element curfewToButton;
    public Element curfewFromTextView;
    public Element curfewToTextView;
    public Element saveButton;
    public Element deleteButton;
    public Element cancelButton;
    public Element doneButton;
    public Element getFromCurfewAlertValue;
    public Element getToCurfewAlertValue;

    public final String dayWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    public final String hourWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options2']";
    public final String minuteWheelXpath = "/" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options3']";
    public final String meridianWheelXpath = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/ampm_wheel']";

    public CurfewAlertScreen(AppiumDriver driver) {
        super(driver);
        getFromCurfewAlertValue = new Element(driver,"tvFromValue");
        getToCurfewAlertValue = new Element(driver,"tvToValue");

        subtitleTextView = new Element(driver, "subTitleTxt");
        informationButton = new Element(driver, "subTitleRightIcon");

        curfewFromButton = new Element(driver, "tvFromValue");
        curfewToButton = new Element(driver, "tvToValue");
        curfewFromTextView = new Element(driver, "curfew_from_value_txt");
        curfewToTextView = new Element(driver, "curfew_to_value_txt");
        saveButton = new Element(driver, "llSave");
        deleteButton = new Element(driver, "llDelete","");
        cancelButton = new Element(driver, "cancel");
        doneButton = new Element(driver, "done");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Curfew Alert") && curfewFromButton.elementExists();
    }
}
