package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.MapScreen;

/**
 * This is the Car Finder screen that is accessed through the Map menus.
 */
public class CarFinderScreen extends MapScreen {
    public Element titleTextView;
    public Element findingCarLoaderText;
    public Element carNameLabelText;
    public Element headerLastUpdatedText;
    public Element clearButton;
    public Element meterButton;
    public Element hornAndLightsButton;
    public Element loaderMessage;

    public CarFinderScreen(AppiumDriver driver) {
        super(driver);
        findingCarLoaderText= new Element(driver, "dialogTitle");
        titleTextView = new Element(driver,"tvNote","Car Finder");
        carNameLabelText = new Element(driver, "map_mycar_detail_title");
        headerLastUpdatedText = new Element(driver, "subTitleUpdatedTxt");
        clearButton = new Element(driver, "map_mycar_detail_delete_or_save_ll");
        meterButton = new Element(driver, "map_mycar_detail_meter_ll");
        hornAndLightsButton = new Element(driver, "map_mycar_detail_hornlight_ll");
        loaderMessage = new Element(driver,"dialogSubtitle");
    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return titleTextView.getTextValue(waitTime).equals("Car Finder");
    }
    public void clickOnCarFinderIcon(){
        tap.elementByXpath("//*[@contentDescription='Car. ']");
    }
}