package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Valet Alert screen that is accessed through the Alert Settings screen.
 */
public class ValetAlertScreen extends Screen {
    public Element subtitleTextView;
    public Element informationButton;

    public Element distanceLimitButton;
    public Element distanceLimitTextView;
    public Element saveButton;

    public Element cancelButton;
    public Element doneButton;

    public final String distanceLimitWheelXpath = "/" +
            "/";

    public ValetAlertScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "subTitleTxt");
        informationButton = new Element(driver, "sub_title_right_icon");

        distanceLimitButton = new Element(driver, "vValetClickView");
        distanceLimitTextView = new Element(driver, "alert_valet_value_txt");
        saveButton = new Element(driver, "llSave");

        cancelButton = new Element(driver, "cancel");
        doneButton = new Element(driver, "done");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Valet Alert") && distanceLimitButton.elementExists();
    }
}
