package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Hyundai Parts & Collision info web site that is accessible through the About & Support page
 */
public class HyundaiPartsAndCollisionInfoScreen extends Screen {
    private Element titleTextView;

    public HyundaiPartsAndCollisionInfoScreen(AppiumDriver driver) {
        super(driver);
        titleTextView = new Element(driver,"subTitleTxt","Hyundai Parts & Collision Info");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (titleTextView.getTextValue().equals("Hyundai Parts & Collision Info"));
    }
}