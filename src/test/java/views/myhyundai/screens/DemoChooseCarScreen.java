package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the demo choose car screen that is accessed through the Demo option from the login page.
 * 2013 Santa Fe is the Gen 1 demo.
 * 2015 SONATA is the Gen 2 demo.
 */
public class DemoChooseCarScreen extends Screen {
    public Element santaFe2013;
    public Element sonata2015;
    public Element back;
    public Element selectVehicle;
    public Element demo;

    /**
     * DemoChooseCarScreen constructor
     * @param driver Appium driver
     */
    public DemoChooseCarScreen(AppiumDriver driver) {
        super(driver);

        santaFe2013 = new Element(driver,"","2013 Santa Fe");
        sonata2015 = new Element(driver,"","2015 SONATA");
        back = new Element(driver,"main_title_left_icon","");
        selectVehicle = new Element(driver,"demo_skip_selectvec_btn","");
        demo = new Element(driver,"","DEMO");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (santaFe2013.elementExists() && sonata2015.elementExists() && demo.elementExists());
    }
}
