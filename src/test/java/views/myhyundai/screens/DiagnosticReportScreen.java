package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The diagnostics report screen that is accessed through the Car Care menu on the home screen.
 */
public class DiagnosticReportScreen extends Screen {
    public Element systemsStatus;
    public Element subTitleText;

    public DiagnosticReportScreen(AppiumDriver driver) {
        super(driver);
        subTitleText = new Element(driver, "subTitleTxt","Diagnostic Report");
        systemsStatus = new Element(driver, "tvAllRight", "All systems normal");
}
    public boolean isPresent() {
        return subTitleText.elementExists();
    }
}
