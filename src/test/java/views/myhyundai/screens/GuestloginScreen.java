package views.myhyundai.screens;

import config.Account;
import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;
import views.myhyundai.baseviews.HomeScreen;

/**
 * This is the Login screen that is displayed after launching the app, which allows users to
 * enter login data or enter Demo mode.
 */
public class GuestloginScreen extends Screen {


    private static final String RESOURCE_ID_PREFIX = "com.stationdm.bluelink:id/";

    GuestloginScreen GuestloginScreen;
    public Element guestLoginButton;
    public Element loginButton;
    public Element guestvehicle;
    public Element guestVehicleyearlist;
    public Element guestVehiclenamelist;
    public Element guestselectvehicle;
    public Element guestvehiclename;
    public Element guestvehicleyear;
    public Element Displayaudio;
    public Element next;
    public Element guestheader;
    public Element POISearch;
    public Element searchbar;
    public Element POIsearchglass;
    public Element OwnersManual;
    public Element OwnersManualHeader;
    public Element DealerLocator;
    public Element CallRoadside;
    public Element ScheduleService;
    public Element DealerLocatorheader;
    public Element Callroadsideassistance;
    public Element CarCare;
    public Element Home;
    public Element Map;
    public Element AboutandSupport;
    public Element BluelinkInformation;
    public Element BluetoothVideos;
    public Element FAQ;
    public Element Bluetooth;
    public Element NearbyGas;
    public Element NearbyGasStations;
    public Element DashboardMaps;
    public Element mapsButton;
    public Element Remindmelater;
    public Element Ok;
    public Element Accessories;
    public Element Accessoriespageheader;
    public Element AccessoriespageChoosehyundai;
    public Element MyDealer;
    public Element Signout;
    public Element Close;

    /**
     * LoginScreen constructor
     * @param driver Appium driver
     */

    public GuestloginScreen(AppiumDriver driver) {
        super(driver);


        /*  guestLoginButton = new Element(driver,"guestLoginButton");
        loginButton = new Element(driver,"loginButton");*/



        guestLoginButton = new Element(driver,RESOURCE_ID_PREFIX + "guestLoginButton","Guest Login");
        loginButton = new Element(driver,RESOURCE_ID_PREFIX + "loginButton","Login");
        guestvehicle = new Element(driver,"ivCircleBg");
        guestVehicleyearlist = new Element(driver,"ccSelectVehicleHeaderlist");
        guestVehiclenamelist = new Element(driver,"mExpandableListView");
        guestselectvehicle = new Element(driver,"demo_skip_selectvec_btn");
        guestvehiclename = new Element(driver,"","ELANTRA");
        guestvehicleyear = new Element(driver,"","2016");
        Displayaudio = new Element(driver,"cc_headunit_item_img");
        next = new Element(driver,"tvRight");
        guestheader = new Element(driver,"tvLeftSubTitle");
        POISearch = new Element(driver,"tvSearchText");
        searchbar = new Element(driver,"etQueryWords");
        POIsearchglass = new Element(driver,"mapMainSearchIcon");
        OwnersManual = new Element(driver,"tvOwnersManual");
        OwnersManualHeader = new Element(driver,"tvLeftSubTitle");
        DealerLocator = new Element(driver,"","Dealer Locator");
        CallRoadside = new Element(driver,"","Call Roadside");
        ScheduleService = new Element(driver,"","Schedule Service");
        DealerLocatorheader = new Element(driver,"tvNote");
        Callroadsideassistance = new Element(driver,"alertTitle");
        CarCare = new Element(driver,"sdm_left_img");
        Home = new Element(driver,"sdm_mid_img");
        Map = new Element(driver,"sdm_right_img");
        AboutandSupport = new Element(driver,"","About & Support");
        BluelinkInformation = new Element(driver,"tvInformation");
        BluetoothVideos= new Element(driver,"tvBluetoothVideo");
        FAQ = new Element(driver,"tvFAQ");
        Bluetooth = new Element(driver,"","Bluetooth");
        NearbyGas = new Element(driver,"","Nearby Gas");
        NearbyGasStations = new Element(driver,"","Nearby Gas Stations");
        DashboardMaps = new Element(driver,"mapFragment");
        mapsButton = new Element(driver, "mapFragment");
        Remindmelater = new Element(driver,"","Remind me later");
        Ok = new Element(driver,"","OK");
        Accessories = new Element(driver,"","Accessories");
        Accessoriespageheader = new Element(driver,"tvLeftSubTitle");
        AccessoriespageChoosehyundai = new Element(driver,"","CHOOSE YOUR HYUNDAI");
        MyDealer = new Element(driver,"","My Dealer");
        Signout = new Element(driver,"tvSignOut");
        Close = new Element(driver,"ivClose");
    }



    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    public void clickSearchKeyPadBtn()
    {
        tap.tapAtCoordinate(916,2045);
    }

    public void clickOnHamburgerMenuButton() {
        tap.elementByXpath("//*[@content-desc='Navigate up']", 7000);
    }

    public String hamburger="//*[@content-desc='Navigate up']";
    public boolean HamburgerMenuisPresent()
    {
        return elementExists.byXpath(hamburger);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */

    public boolean isPresent(long waitTime) {
        /* return (usernameEditText.elementExists(waitTime) && passwordEditText.elementExists(waitTime));*/
        return guestLoginButton.elementExists() && loginButton.elementExists();
    }

    public String Guestlogin ="//android.widget.TextView[@text='GUEST']";
    public boolean GuestloginPagepresent()
    {
        return elementExists.byXpath(Guestlogin);
    }

    public String Switchvehicle = "(//*[@id='ivCircleBg'])[2]";
    public void tap2NDVEHICLE()
    {
        tap.elementByXpath(Switchvehicle);
    }

    public String button1="//*[@class='android.widget.Button' and @resource-id='android:id/button1']";
    public String button2="//*[@class='android.widget.Button' and @resource-id='android:id/button2']";

    public boolean CallbuttonisPresent()
    {
        return elementExists.byXpath(button1);
    }
    public boolean CancelbuttonisPresent()
    {
        return elementExists.byXpath(button2);
    }

    public boolean tapOnMapsOptionButton(String mapsOptionName)
    {
        return tap.elementByXpath("//android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/buttonIconLayout' and (./preceding-sibling::* | ./following-sibling::*)[@text='"+ mapsOptionName +"']]");
    }

    public void clickOnMapsButton() {
        mapsButton.tap(3000);
    }

    public void clickOnPOISearchButton()
    {
        tapOnMapsOptionButton("POI Search");
    }

    public void clickOnDealersButton()
    {
        tapOnMapsOptionButton("Dealers");
    }

    public String POISearchMaps = "//*[@class='android.widget.RelativeLayout' and ./*[@text='POI Search'] and ./*[@id='buttonIconLayout']]";
    public void tapPOISearch() { tap.elementByXpath(POISearchMaps);}

}