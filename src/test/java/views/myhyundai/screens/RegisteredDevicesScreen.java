package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Registered Devices screen that is accessible through the Settings screen, which is
 * accessible through the Side menu.
 */
public class RegisteredDevicesScreen extends Screen {
    public Element subtitleTextView;
    public Element registeredDevicesTitleTextView;
    public Element emailTextView;

    public RegisteredDevicesScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "sub_title_txt");
        registeredDevicesTitleTextView = new Element(driver, "", "REGISTERED DEVICES");
        emailTextView = new Element(driver, "setting_devices_email_txt");
    }

    public boolean isPresent() {
        return registeredDevicesTitleTextView.elementExists();
    }

    public boolean checkIfDeviceOnList(String deviceName) {
        return elementExists.byXpath("//android.widget.TextView[@text='" + deviceName + "']");
    }
}
