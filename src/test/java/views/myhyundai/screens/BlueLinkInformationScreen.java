package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Blue Link information screen that is reached through the About & Support screen.
 */
public class BlueLinkInformationScreen extends Screen {
    public Element subtitleTextView;
    public Element blueLinkLogo;

    public BlueLinkInformationScreen(AppiumDriver driver) {
        super(driver);
        subtitleTextView = new Element(driver,"subTitleTxt","Blue Link Info");
        blueLinkLogo = new Element(driver,"subTitleIcon");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Blue Link Info") && blueLinkLogo.elementExists();
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }
}
