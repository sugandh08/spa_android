package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the MVR history screen, which is accessible through the MVR screen by the icon in the top right.
 */
public class MonthlyVehicleReportHistoryScreen extends Screen {
    public Element subtitleTextView;
    // this button exists on the base MVR page, but should not exist on the MVR history page.
    public Element nonexistentMvrHistoryButton;

    // the container/parent view for each entry in the history list.
    public Element oldMvrFirstEntryContainerView;

    public Element oldMvrMonthTextView;
    public Element oldMvrDateTextView;

    public MonthlyVehicleReportHistoryScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "sub_title_txt");
        nonexistentMvrHistoryButton = new Element(driver, "main_title_right_icon");

        //"/android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/content_container']" +
        //                    "/android.widget.LinearLayout[@index='0']" +
        oldMvrFirstEntryContainerView = new Element(driver, "") {
            String xpath = "/" +
                    "/android.support.v7.widget.RecyclerView[@resource-id='com.stationdm.bluelink:id/lv_car_health_status_list']" +
                    "/android.widget.LinearLayout[@index='0']";

            // override elementExists() method to use a specific xpath to the first entry in the list.
            @Override
            public boolean elementExists() {
                return elementExists.byXpath(xpath);
            }
        };
        oldMvrMonthTextView = new Element(driver, "iv_car_health_history_month");
        oldMvrDateTextView = new Element(driver, "tv_car_health_history_time");
    }

    // test that the subtitle says the correct thing, but also make sure it is not the standard MVR screen by
    // testing that the history button doesn't exist.
    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Monthly Vehicle Health Report") &&
                !nonexistentMvrHistoryButton.elementExists();
    }
}
