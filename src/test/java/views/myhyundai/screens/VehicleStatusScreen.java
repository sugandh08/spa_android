package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Vehicle Status screen that is accessed on Gen 2 Gas or HEV vehicles by tapping the
 * button in the center of the home page, or on PHEV or AEEV vehicles by tapping the (i)/Information
 * button in the Remote Commands menu on the home page.
 */
public class VehicleStatusScreen extends Screen {
    public Element subtitleTextView;
    public Element vehicleStatusTextView;
    public Element lastUpdateDateTextView;
    public Element refreshButton;
    public Element climate;
    public Element engine;
    public Element heating;
    public Element frontdefrost;

    public VehicleStatusScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "sub_title_txt");
        vehicleStatusTextView = new Element(driver, "vehicle_status_txt");
        lastUpdateDateTextView = new Element(driver, "tv_last_update_date");
        refreshButton = new Element(driver, "vehicle_bottom_refresh_img");
        climate = new Element(driver, "tvVehicleClimate","");
        engine = new Element(driver,"tvVehicleEngine");
        heating = new Element(driver,"tvVehicleParkStatus");
        frontdefrost = new Element(driver,"tvVehicleFrontDefrost");


    }

    public boolean isPresent() {
        return vehicleStatusTextView.elementExists() && subtitleTextView.getTextValue().equals("Vehicle Status");
    }
}
