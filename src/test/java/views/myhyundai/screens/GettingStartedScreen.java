package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import tests.myhyundai.ev.EVLogin;
import views.Screen;

public class GettingStartedScreen extends Screen {
    public Element subTitleText;
    /**
     * Standard Screen constructor.
     *
     * @param driver The Appium driver for automation controls. Each screen has custom actions that need access
     *               to this driver.
     */
    public GettingStartedScreen(AppiumDriver driver) {
        super(driver);
        subTitleText = new Element(driver, "subTitleTxt","Getting Started");
    }

    public boolean isPresent() {
        return subTitleText.elementExists();
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }
}
