package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Climate Settings screen that is accessible from the remote_genesis_old commands options while
 * logged into a PHEV or AEEV vehicle.
 */
public class ClimateSettingsScreen extends Screen {
    public Element backButton;
    public Element subtitleTextView;
    public Element startButton;
    public Element stopButton;
    public Element frontDefrosterToggleButton;
    public Element heatedFeaturesToggleButton;
    public Element submitButton;
    public Element EngineDuration;

    public final String TEMPERATURE_XPATH = "//android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/tempwheel']";

    public ClimateSettingsScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver, "main_title_left_icon");
        subtitleTextView = new Element(driver, "sub_title_txt");

        startButton = new Element(driver, "", "Start");
        stopButton = new Element(driver, "", "Stop");
        frontDefrosterToggleButton = new Element(driver, "sbDefrostSwitch");
        heatedFeaturesToggleButton = new Element(driver, "sbHeatSwitch");
        submitButton = new Element(driver, "tvSubmit");
        EngineDuration = new Element(driver,"","Engine Duration");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subtitleTextView.getTextValue(waitTime).equals("Climate Settings");
    }

    public void setStartSettingsStates(boolean frontDefrosterToggle, boolean heatedFeaturesToggle) {

        if (frontDefrosterToggleButton.getCheckedValue() ^ frontDefrosterToggle) {
            frontDefrosterToggleButton.tap();
        }

        if (heatedFeaturesToggleButton.getCheckedValue() ^ heatedFeaturesToggle) {
            heatedFeaturesToggleButton.tap();
        }

    }
    public boolean EngineDurationIsPresent()
    {
        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='Engine Duration']");
    }

    public void ClickonSubmitButton()
    {
        tap.elementByXpath("//*[@class='android.widget.LinearLayout' and ./*[@text='Submit']]");
    }
}
