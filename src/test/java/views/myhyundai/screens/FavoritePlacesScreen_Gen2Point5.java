package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class FavoritePlacesScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public Element backButton;
    public Element settingsPanel;
    public Element favoritePlacesPanel;
    public Element saveButton;
    public Element cancelButton;
    public Element homeTitle;
    public Element workTitle;



    public FavoritePlacesScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "subTitleTxt");
        saveButton = new Element(driver, "btnSave");
        cancelButton = new Element(driver, "btnCancel");
        backButton = new Element(driver, "main_title_left_icon");
        settingsPanel = new Element(driver, "", " SETTINGS ");
        favoritePlacesPanel = new Element(driver, "", " FAVORITE PLACES ");
        homeTitle=new Element(driver,"","Home");
        workTitle=new Element(driver,"","Work");

    }


    private String getXpathOfFavoritePlacesEditText(String titleType) {
        return "//*[@text='"+titleType+"']/../..//*[@text='Edit']";
    }


    private String getXpathOfFavoritePlacesClearText(String titleType) {
        return "//*[@text='Home']/../..//*[@text='clear']";
    }



    public String getXpathOfFavoritePlacesAddressText(String titleType) {
        return "//*[@text='"+titleType+"']/../..//*[@resource-id='com.stationdm.bluelink:id/tvAddress']";
    }



    private String getXpathOfAddIcon(String titleType) {
        return "//*[@text='"+titleType+"']/../..//*[@resource-id='com.stationdm.bluelink:id/ivAdd']";
    }



    public String getXpathOfFavoritePlaceTitleByIndex(String index) {
        return "//*[@text='Favorite "+index+"']";
    }

    private String getXpathOfFavoritePlacesEditTitleIcon(String titleType) {
        return "/" +
                "/android.widget.EditText[@text='" + titleType + "']" +
                "/.." +"/.."+
                "/android.widget.FrameLayout[@resource-id='com.stationdm.bluelink:id/favourite_places_item_icon_edit_title']";
    }

    public boolean isAddIconDisplayed(String titleType)
    {

        return attribute.getClickableValueByXpath(getXpathOfAddIcon(titleType));
    }



    public boolean isEditTextDisplayed(String titleType)
    {

        return attribute.getClickableValueByXpath(getXpathOfFavoritePlacesEditText(titleType));
    }




    public boolean isClearTextDisplayed(String titleType)
    {

        return attribute.getClickableValueByXpath(getXpathOfFavoritePlacesClearText(titleType));
    }


    public void  editAddress(String titleType)
    {
        tap.elementByXpath(getXpathOfFavoritePlacesEditText(titleType));

    }



    public void clickAddIcon(String titleType)
    {
        tap.elementByXpath(getXpathOfAddIcon(titleType));
    }



    public void clickClearText(String titleType)
    {
        tap.elementByXpath(getXpathOfFavoritePlacesClearText(titleType));
    }


    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().contains("Driver Settings");
    }
}
