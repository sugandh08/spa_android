package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This is the Bluetooth & Multimedia Support web site that is reached through the About & Support screen.
 * It is loaded in an external internet browser.
 */
public class BluetoothScreen extends Screen {

    public Element subTitleText;
    public Element popUpText;
    public Element okButton;

    public BluetoothScreen(AppiumDriver driver) {
        super(driver);
        subTitleText = new Element(driver,"subTitleTxt","Bluetooth");
        popUpText = new Element(driver, "android:id/message","Content unavailable at this time.");
        okButton = new Element(driver, "android:id/button1","OK");
    }

    public boolean isPresent() {
        return subTitleText.elementExists();
    }

    public void bluetoothPageRedirectedSuccessfully(){
        if(popUpText.elementExists()){
            System.out.println("Content Unavailable at this time");
            okButton.tap();
        }
        else {
            isPresent();
        }
    }
}
