package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import library.ElementLocation;
import views.Screen;

/**
 * The screen found by tapping the Parking Meter button from the home page, once the parking
 * meter is already set.
 */
public class ParkingMeterTimeSetScreen extends Screen {
    public Element backButton;
    public Element timeDisplayed;
    public Element notesEditText;
    public Element resetButton;
    public Element saveButton;
    public  Element subTitleTextView;
    public  Element meterTimeSetText;
    public Element reminderTimeSetText;
    public Element yesButton;
    public Element noButton;

    public ParkingMeterTimeSetScreen(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "sub_title_txt","Parking Meter");
        backButton = new Element(driver,"main_title_left_icon");
        timeDisplayed = new Element(driver, "tv_time");
        notesEditText = new Element(driver,"edit_notes");
        meterTimeSetText = new Element(driver, "tv_meter_mins","");
        reminderTimeSetText = new Element(driver, "tv_alert_mins","");
        yesButton = new Element(driver, "button1","Yes");
        noButton = new Element(driver, "button2","No");
        resetButton = new Element(driver,"ll_parking_meter_show_reset") {
            // Overriding the getTextValue method because the clickable button area and the text view are different
            @Override
            public String getTextValue() {
                return resetButton.attribute.getTextValueByXpath("/" +
                        "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/map_mycar_detail_delete_or_save']");
            }
        };
        saveButton = new Element(driver,"ll_parking_meter_show_save") {
            // Overriding the getTextValue method because the clickable button area and the text view are different
            @Override
            public String getTextValue() {
                return resetButton.attribute.getTextValueByXpath("/" +
                        "/android.widget.RelativeLayout[@id='ll_parking_meter_show_save']" +
                        "/android.widget.TextView[@index='0']");
            }
        };
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return timeDisplayed.elementExists() &&
                notesEditText.elementExists() &&
                resetButton.elementExists();
    }

    public void resetParkingMeter()
    {
        resetButton.tap();
        yesButton.tap();
    }
}
