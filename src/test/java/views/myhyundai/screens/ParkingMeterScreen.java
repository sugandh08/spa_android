package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Parking Meter screen that is accessible through the home screen by tapping the
 * Parking Meter button.
 */
public class ParkingMeterScreen extends Screen {
    public Element subtitleTextView;
    public Element backButton;

    public Element meterTimeTextView;
    public Element meterTimeValueTextView;
    public Element reminderTextView;
    public Element reminderValueTextView;

    public Element clearButton;
    public Element saveButton;
    public Element notesEditText;

    // Xpaths for each of the time scroll wheels because the interactions we have with them are
    // only done through GuiController, not here. The only thing we care about is using these Xpaths
    // to get the selenium Rectangle object of the scroll wheel.
    public final String METER_TIME_HOUR_XPATH = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/meter_time_picker']" +
            "/android.widget.LinearLayout[@index='0']" +
            "/android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/optionspicker']" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    public final String METER_TIME_MINUTE_XPATH = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/meter_time_picker']" +
            "/android.widget.LinearLayout[@index='0']" +
            "/android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/optionspicker']" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options2']";
    public final String REMINDER_HOUR_XPATH = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/alert_time_picker']" +
            "/android.widget.LinearLayout[@index='0']" +
            "/android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/optionspicker']" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    public final String REMINDER_MINUTE_XPATH = "/" +
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/alert_time_picker']" +
            "/android.widget.LinearLayout[@index='0']" +
            "/android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/optionspicker']" +
            "/android.view.View[@resource-id='com.stationdm.bluelink:id/options2']";

    public ParkingMeterScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver,"sub_title_txt");
        backButton = new Element(driver,"main_title_left_icon");

        meterTimeTextView = new Element(driver, "", "Meter time");
        meterTimeValueTextView = new Element(driver, "tv_meter_time_value");
        reminderTextView = new Element(driver, "", "Reminder");
        reminderValueTextView = new Element(driver, "tv_alert_time_value");

        clearButton = new Element(driver,"parkingmeter_clear_ripple");
        saveButton = new Element(driver,"parkingmeter_save_ripple");
        notesEditText = new Element(driver,"parkingmeter_notes_ripple");
    }

    public boolean isPresent() {
        return (clearButton.elementExists() &&
                saveButton.elementExists() &&
                notesEditText.elementExists());
    }

    public void setParkingTimeAndReminder()
    {
        swipe.up(2);
    }
}
