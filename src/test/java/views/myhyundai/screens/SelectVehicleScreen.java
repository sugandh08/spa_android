package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Select Vehicle screen that is accessed by logging into an account with 2+ vehicles on it.
 * This screen should not show if the account only has 1 vehicle attached.
 */
public class SelectVehicleScreen extends Screen {
    public Element subtitleTextView;
    public Element usernameTextView;
    public Element emailTextView;

    /**
     * SelectVehicleScreen constructor
     * @param driver
     */
    public SelectVehicleScreen(AppiumDriver driver) {
        super(driver);
        subtitleTextView = new Element(driver, "tvLeftSubTitle","Select Vehicle");
        usernameTextView = new Element(driver,"tvUsername");
        emailTextView = new Element(driver,"tvEmail");
    }

    public void tapVehicleButtonByVin(String vin) {
        tap.elementByXpath("//android.widget.TextView[@text='VIN: "+vin+"']/.." +
                "//android.widget.ImageView[@resource-id='com.stationdm.bluelink:id/ivCircleBg']");

  /*      tap.elementByXpath("//android.widget.TextView[@text='VIN: "+vin+"']/..//android.widget.ImageView[@resource-id='com.stationdm.bluelink:id/ivCircleBg']");
  */
    //    tap.elementByXpath(Vinxpath);
    }

    public String Vinxpath= "//android.widget.TextView[@text='VIN: \"+vin+\"']/..//android.widget.ImageView[@resource-id='com.stationdm.bluelink:id/ivCircleBg']";



    /**
     * Checks if Select Vehicle screen is present, then taps vehicle button by looking for the VIN.
     * Uses standard wait time.
     * @param vin VIN to look for
     */

    /*public void tapVehicleButtonByVinIfThisScreenIsPresent(String vin) {
        tapVehicleButtonByVinIfThisScreenIsPresent(vin, 5);
    }*/

    /**
     * Checks if Select Vehicle screen is present, then taps vehicle button by looking for the VIN
     * @param vin VIN to look for
     * @param screenLoadWaitTime Time (in seconds) to wait for this screen to load
     */
    public void tapVehicleButtonByVinIfThisScreenIsPresent(String vin, long screenLoadWaitTime) {
        if (isPresent(screenLoadWaitTime)) {
            scrollRightIfVinNotPresent(vin);
            tapVehicleButtonByVin(vin);
        }
        else {
            System.out.println("NOTE: Above 'element not found' message is not an error if there is only 1 car on account.");
        }
    }

    public boolean checkIfVinIsPresent(String vin) {
        return elementExists.byXpath("//android.widget.TextView[@text='VIN: " + vin + "']", 10);
    }

    public String getPackageTextByVin(String vin) {
        return attribute.getTextValueByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']" +
                "/.." +
                "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tv_vehicle_package']");
    }

    public String getModelTextByVin(String vin) {
        return attribute.getTextValueByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']" +
                "/.." +
                "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tv_vehicle_name']");
    }

    public String getNicknameTextByVin(String vin) {
        return attribute.getTextValueByXpath("/" +
                "/android.widget.TextView[@text='VIN: " + vin + "']" +
                "/.." +
                "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tv_vehicle_name_test']");
    }

    /**
     * Checks if the selected VIN is present. If not, scrolls screen to right.
     * @param vin VIN to check.
     */
    public void scrollRightIfVinNotPresent(String vin) {
        if (!checkIfVinIsPresent(vin)) {
            System.out.println("NOTE: Above 'element not found' message is likely not an error.");
            swipe.fromRightEdge(1);
        }
    }

    /**
     * Checks a string to see if it matches the package settings for a car.
     * @param packageText Text to check for packages
     * @param hasConnectedCare Whether it should contain Connected Care
     * @param hasRemote Whether it should contain Remote
     * @param hasGuidance Whether it should contain Guidance
     * @return False if text contains a package it shouldn't, or if it doesn't contain a package it should. True otherwise.
     */
    public boolean checkIfPackageTextIsCorrect(String packageText, boolean hasConnectedCare, boolean hasRemote, boolean hasGuidance) {
        return ((packageText.contains("Connected Care") == hasConnectedCare) &&
                (packageText.contains("Remote") == hasRemote) &&
                (packageText.contains("Guidance") == hasGuidance));
    }

    /**
     * Checks if the screen is present. Uses standard wait time.
     * @return Whether screen is present or not.
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Checks if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait.
     * @return Whether screen is present or not.
     */
    public boolean isPresent(long waitTime) {
        return subtitleTextView.getTextValue(waitTime).equals("Select Vehicle");
    }
}