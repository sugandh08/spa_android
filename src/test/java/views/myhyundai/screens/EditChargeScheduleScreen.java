package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class EditChargeScheduleScreen extends Screen {
    public Element title;
    public Element sun;
    public Element mon;
    public Element tue;
    public Element wed;
    public Element thr;
    public Element fri;
    public Element sat;
    public Element temperature;
    public Element defrost;
    public Element done;
    public Element done1;
    public Element minimiseDrivertime;
    public Element temperatureMaintain;
    public Element acseekbar;


    public EditChargeScheduleScreen(AppiumDriver driver)
       {
           super(driver);
           title = new Element(driver,"subTitleTxt");
           sun = new Element(driver,"sunday");
           mon = new Element(driver,"monday");
           tue = new Element(driver,"tuesday");
           wed = new Element(driver,"wednesday");
           thr = new Element(driver,"thursday");
           fri = new Element(driver,"friday");
           sat = new Element(driver,"saturday");
           temperature = new Element(driver, "sbTemp");
           minimiseDrivertime = new Element(driver,"ivDepartureDown");
           defrost = new Element(driver,"sbDefrost");
           done = new Element(driver,"rippleDone");
           done1 = new Element(driver,"rippleDone");
           temperatureMaintain = new Element(driver,"ivTempDown");
           acseekbar = new Element(driver,"sbSocSeekBar");

       }

    public boolean isPresent() {
        String TitleText = title.getTextValue();
        return TitleText.equals("Set Schedule");
    }

    public String xpath_DrivingHour(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    }

    public String xpath_DrivingMinute(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options2']";
    }

    public String xpath_DrivingAmPm(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options3']";
    }

    public String xpath_ACChargeLimit()
    {
        return "//android.widget.SeekBar[@resource-id='com.stationdm.bluelink:id/sbSocSeekBar']";
    }

}
