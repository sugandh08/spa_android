package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class SetDepartureTimeScheduleScreen extends Screen {
    public Element title;
    public Element schedule1;
    public Element schedule2;
    public Element OffPeakSchedule;
    public Element setSchedule1;
    public Element setSchedule2;
    public Element schedule1Time;
    public Element schedule1TimeAmPm;
    public Element getTemperature;
    public Element sunday;
    public Element departureOnOFF;
    public Element submitbutton;

    public SetDepartureTimeScheduleScreen(AppiumDriver driver)
    {
        super(driver);
        title = new Element(driver,"subTitleTxt");
        schedule1 = new Element(driver,"sbSchedule1");
        schedule2 = new Element(driver,"sbSchedule2");
        OffPeakSchedule =new Element(driver, "sbOffPeakSchedule");
        setSchedule1 = new Element(driver,"clSchedule1");
        setSchedule2 = new Element(driver, "clSchedule2");
        schedule1Time = new Element(driver, "tvSchedule1Time");
        schedule1TimeAmPm =new Element(driver, "tvSchedule1TimeA");
        getTemperature = new Element(driver,"tvSchedule1Temp");
        sunday = new Element(driver,"sunday");
        departureOnOFF = new Element(driver,"");
        submitbutton = new Element(driver,"rippleSubmit");


    }


    public boolean isPresent() {
        String TitleText = title.getTextValue();
        return TitleText.equals("Set Schedule");
    }

    public String departureTimeDetailsDashboard(){
        return attribute.getTextValueByXpath("(//android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/btnSchedule']/*[@class='android.widget.TextView'])[1]");
    }

    public String departureOnOFF(){
        return attribute.getTextValueByXpath("(//android.widget.LinearLayout[@resource-id='com.stationdm.bluelink:id/btnSchedule']/*[@class='android.widget.TextView'])[2]");

    }
}
