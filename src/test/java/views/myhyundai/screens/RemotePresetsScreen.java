package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.openqa.selenium.WebElement;
import views.Screen;

public class RemotePresetsScreen extends Screen {

    public Element subTitleTextView;
    public Element remoteStartWithoutPresetsButton;
    public Element remoteStartWithoutPresetsText;
    public Element presetscreen;
    public Element PresetScreenSummer;
    public Element PresetScreenWinter;
    public Element Temperaturetoggle;

    public RemotePresetsScreen(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "", "Remote Start");
        remoteStartWithoutPresetsButton = new Element(driver, "clStartWithoutPreset", "");
        remoteStartWithoutPresetsText = new Element(driver, "tvPresetTitle", "Start Vehicle Without Presets");
        presetscreen = new Element(driver, "tvRemotePresetText");
        PresetScreenSummer = new Element(driver, "tvPresetName");
        PresetScreenWinter = new Element(driver, "tvPresetName", "Winter");
        Temperaturetoggle = new Element(driver,"sbTempSwitch");
    }

    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().equals("Remote Start");
    }

    public boolean isRemoteStartPresetsPresent(int Index) {
        return elementExists.byXpath("(//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@class='android.view.ViewGroup'])[" + Index + "]");
    }

    public void editRemotePresets(int Index) {
        tap.elementByXpath("//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@class='android.view.ViewGroup'][" + Index + "]//*[@resource-id='com.stationdm.bluelink:id/ivEditPreset']");
    }

    public void clickOnPresetStartButton(int Index) {
        tap.elementByXpath("//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@class='android.view.ViewGroup'][" + Index + "]//*[@resource-id='com.stationdm.bluelink:id/btnPresetStart']");
    }

    public String xpathOfPresetsName(int Index) {
        return "//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@class='android.view.ViewGroup'][" + Index + "]//*[@resource-id='com.stationdm.bluelink:id/tvPresetName']";
    }

    public String xpathOfFrontDefrosterIcon(int Index) {
        return "(//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@resource-id='com.stationdm.bluelink:id/ivFrontDefroster'])[" + Index + "]";
    }

    public String xpathOfHeatedFeaturesIcon(int Index) {
        return "(//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@resource-id='com.stationdm.bluelink:id/ivMirrorsDefroster'])[" + Index + "]";
    }

    public String xpathOfTemperatureValue(int Index) {
        return "(//*[@resource-id='com.stationdm.bluelink:id/rvPresettingList']//*[@resource-id='com.stationdm.bluelink:id/tvTempValue'])[" + Index + "]";
    }

    public boolean getPresetsSettingsStates(boolean temperatureState, boolean frontDefrosterState, boolean heatedFeaturesState, int Index) {

        if (attribute.getSelectedValueByXpath(xpathOfTemperatureValue(Index)) == temperatureState) {
            return true;
        }

        if (attribute.getSelectedValueByXpath(xpathOfFrontDefrosterIcon(Index)) == frontDefrosterState) {
            return true;
        }

        if (attribute.getSelectedValueByXpath(xpathOfHeatedFeaturesIcon(Index)) == heatedFeaturesState) {
            return true;
        } else
            return false;
    }


}


