package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.HomeScreen;

/**
 * This is the home screen that is displayed when logged in with a PHEV vehicle.
 * NOTE that this screen is NOT displayed for HEV vehicles (those use the Gas home screen).
 */
public class HybridHomeScreen extends HomeScreen {
    public Element totalRangeDistanceTextView;
    public Element electricRangeDistanceTextView;
    public Element pluginStatus;
    public Element ChargingStatus;

    public Element batteryGaugemeter;
    public Element batteryLevel;
    public Element startCharge;
    public Element remoteStatus;



    /**
     * HybridHomeScreen constructor
     * @param driver Appium driver
     */
    public HybridHomeScreen(AppiumDriver driver) {
        super(driver);

        totalRangeDistanceTextView = new Element(driver,"tvElectricAndGasText");
        electricRangeDistanceTextView = new Element(driver, "tvElectricText");
        batteryGaugemeter = new Element(driver, "pbFuelLevel");
        batteryLevel = new Element(driver, "tvFuelText");
        pluginStatus = new Element(driver,"cbVoltage");
        ChargingStatus = new Element(driver,"cbTime");
        startCharge = new Element(driver,"btnStartCharge");
        remoteStatus = new Element(driver,"tvRemoteStatus");
    }
    public String RemoteActions ="//*[@class='android.widget.ImageView' and ./parent::*[@id='remoteFragment']]";
    public boolean RemoteActionslogo(){
        return elementExists.byXpath(RemoteActions);
    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        // wait for a half second to avoid stale element
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            //
        }
        return totalRangeDistanceTextView.elementExists(waitTime) && electricRangeDistanceTextView.elementExists();
    }

}
