package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import library.Element;
import library.ElementExists;
import org.openqa.selenium.Dimension;
import sun.awt.windows.ThemeReader;
import views.Screen;

/**
 * This is the My POI screen that is accessible through the map menus
 */
public class PoiAddToFavoritePlacesScreen_Gen2Point5 extends Screen {


    public Element searchEditText;
    public Element searchIcon;
    public Element addressText;
    public Element addToFavoritePlacesButton;
    public Element cancelButton;


    public PoiAddToFavoritePlacesScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        searchEditText = new Element(driver, "tvPOISearch");
        searchIcon = new Element(driver, "ivSearchPOI");
        addressText=new Element(driver,"tvAddressContent");
        addToFavoritePlacesButton=new Element(driver,"btnAddToFav");
        cancelButton=new Element(driver,"favourite_places_bt_cancel");

    }

    public void searchPOI(String address) {
        searchEditText.enterText(address);

       searchIcon.tap();
    }

    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return searchEditText.getTextValue().equals("POI Search");
    }
}
