package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.HomeScreen;
import views.myhyundai.fragments.RemoteCarControls;

/**
 * The home screen that is displayed when logged in to a Gas vehicle or an HEV vehicle. This screen
 * will not display if logged in to an AEEV or PHEV vehicle.
 */
public class GasHomeScreen extends HomeScreen 
{
    public RemoteCarControls remoteCommands;
    public Element vehicleStatusButton;
    public Element remoteStartButton;
    public Element VehicleConnectivityWarning;
    public Element VehicleConnectivityWarningBttn;
    /**
     * GasHomeScreen constructor
     * @param driver Appium driver
     */
    public GasHomeScreen(AppiumDriver driver) {
        super(driver);

        remoteCommands = new RemoteCarControls(driver);
        vehicleStatusButton = new Element(driver, "btnVehicleStatus","Vehicle Status");
        remoteStartButton = new Element(driver, "btnRemoteStart","Remote Start");
        VehicleConnectivityWarning = new Element(driver,"tvConnectTitle","Vehicle Connectivity Warning");
        VehicleConnectivityWarningBttn = new Element(driver, "ivConnectArrow");
    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        // Is different from electric because elements are accessed from the home screen, not the home button.
        return isPresent(5);
    }

    /**
     * Test if the screen is present
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        // avoid stale element
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            //
        }
        return remoteStartButton.elementExists(waitTime);
    }

    public Boolean IsVehicleConnectivityWarningPresent()
    {
        return elementExists.byXpath("//*[@id='tvContent' and @resource-id='com.stationdm.bluelink:id/tvContent']");
    }

}