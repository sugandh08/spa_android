package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Settings screen that is accessed through the Side menu.
 */
public class SettingsScreen extends Screen {
    public Element subtitleTextView;
    public Element notificationsTextView;

    public Element notificationDeviceNameTextView;
    public Element receiveNotificationsOnDeviceToggle;
    public Element registeredDevicesButton;

    public Element connectedCareExpandButton;
    public Element remoteExpandButton;

    public Element connectedCareToggleAllText;
    public Element connectedCareToggleAllEmail;
    public Element remoteToggleAllText;
    public Element remoteToggleAllEmail;

    public Element preferredContactInformationButton;

    public Element changePinButton;

    public final String automaticCollisionNotificationText = "AUTOMATIC COLLISION NOTIFICATION(ACN)";
    public final String sosEmergencyAssistanceText = "SOS EMERGENCY ASSISTANCE";
    public final String automaticDtcText = "AUTOMATIC DTC";
    public final String maintenanceAlertText = "MAINTENANCE ALERT";

    public final String panicNotificationText = "PANIC NOTIFICATION";
    public final String alarmNotificationText = "ALARM NOTIFICATION";
    public final String hornAndLightsText = "HORN AND LIGHTS";
    public final String remoteEngineStartStopText = "REMOTE ENGINE START/STOP";
    public final String remoteDoorLockUnlockText = "REMOTE DOOR LOCK/UNLOCK";
    public final String curfewAlertText = "CURFEW ALERT";
    public final String valetAlertText = "VALET ALERT";
    public final String geofenceAlertText = "GEO-FENCE ALERT";
    public final String speedAlertText = "SPEED ALERT";

    public SettingsScreen(AppiumDriver driver) {
        super(driver);
        subtitleTextView = new Element(driver, "subTitleTxt");
        notificationsTextView = new Element(driver, "tv_user_title");

        notificationDeviceNameTextView = new Element(driver, "tvDeviceName");
        receiveNotificationsOnDeviceToggle = new Element(driver, "sbNotificationSwitch");
        registeredDevicesButton = new Element(driver, "tvNotificationAccount");

        connectedCareExpandButton = new Element(driver, "tvNotificationName");
        remoteExpandButton = new Element(driver, "tvNotificationName");

        connectedCareToggleAllText = new Element(driver, "tvAllSMS");
        connectedCareToggleAllEmail = new Element(driver, "tvAllEmail");
        remoteToggleAllText = new Element(driver, "tvAllSMS");
        remoteToggleAllEmail = new Element(driver, "tvAllEmail");

        preferredContactInformationButton = new Element(driver, "notification_refered_contact_txt");

        changePinButton = new Element(driver, "settings_changepin");
    }

    /**
     * Builds an xpath to the email toggle element using the setting name
     * @param settingName The relative setting to the toggle element
     * @return The xpath to the toggle element
     */
    private String getXpathOfEmailToggle(String settingName) {
        return "/" +
                "/android.widget.TextView[@text='" + settingName + "']" +
                "/.." +
                "/android.widget.ImageView[@resource-id='com.stationdm.bluelink:id/notification_setting_item_email']";
    }

    /**
     * Builds an xpath to the sms toggle element using the setting name
     * @param settingName The relative setting to the toggle element
     * @return The xpath to the toggle element
     */
    private String getXpathOfSmsToggle(String settingName) {
        return "/" +
                "/android.widget.TextView[@text='" + settingName + "']" +
                "/.." +
                "/android.widget.ImageView[@resource-id='com.stationdm.bluelink:id/notification_setting_item_sms']";
    }

    /**
     * Checks if the chosen email notification element exists
     * @param settingName The relative setting to the email notification element
     * @return True if the element exists, false otherwise
     */
    public boolean checkIfEmailNotificationStateOfSettingExists(String settingName) {
        return elementExists.byXpath(getXpathOfEmailToggle(settingName));
    }

    /**
     * Checks if the chosen sms notification element exists
     * @param settingName The relative setting to the sms notification element
     * @return True if the element exists, false otherwise
     */
    public boolean checkIfSmsNotificationStateOfSettingExists(String settingName) {
        return elementExists.byXpath(getXpathOfSmsToggle(settingName));
    }

    /**
     * Gets the selected or unselected state of the chosen email notification element
     * @param settingName The relative setting to the email notification element
     * @return true or false that corresponds to the notification being enabled or disabled
     */
    public boolean getEmailNotificationStateOfSetting(String settingName) {
        return attribute.getSelectedValueByXpath(getXpathOfEmailToggle(settingName));
    }

    /**
     * Gets the selected or unselected state of the chosen sms notification element
     * @param settingName The relative setting to the sms notification element
     * @return true or false that corresponds to the notification being enabled or disabled
     */
    public boolean getSmsNotificationStateOfSetting(String settingName) {
        return attribute.getSelectedValueByXpath(getXpathOfSmsToggle(settingName));
    }

    /**
     * Taps the selected email notification element to toggle its value
     * @param settingName The relative setting to the email notification element
     */
    public void toggleEmailNotificationOfSetting(String settingName) {
        tap.elementByXpath(getXpathOfEmailToggle(settingName));
    }

    /**
     * Taps the selected sms notification element to toggle its value
     * @param settingName The relative setting to the sms notification element
     */
    public void toggleSmsNotificationOfSetting(String settingName) {
        tap.elementByXpath(getXpathOfSmsToggle(settingName));
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subtitleTextView.getTextValue(waitTime).equals("Settings") &&
                notificationsTextView.getTextValue().equals("NOTIFICATIONS");
    }
}
