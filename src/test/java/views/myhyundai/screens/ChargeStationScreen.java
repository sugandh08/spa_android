package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.MapScreen;

/**
 * This is the Charge Station screen that is accessible through the map menus while logged in to a
 * PHEV or AEEV vehicle.
 */
public class ChargeStationScreen extends MapScreen {
    public Element title;
    public Element nearbyChargingStationsLabelText;
    public Element arrowIcon;
    public Element chargeStationLocationText;
    public Element sendButton1;
    public Element sendButton2;
    public Element saveAndDeleteButton;
    public Element saveAndDeleteButtonText;
    public Element searchEditTextbox;
    public Element searchGlassIcon;
    public Element chargeStationListIcon;
    public Element address;
    public ChargeStationScreen(AppiumDriver driver)
    {
        super(driver);
        title = new Element(driver,"tvNote");
        nearbyChargingStationsLabelText = new Element(driver, "tvNote","Nearby Charging Stations");
        arrowIcon = new Element(driver, "iv_popdetail_arrow","");
        chargeStationLocationText = new Element(driver, "titleTextView","");
        sendButton1 = new Element(driver, "rl_popdetail_bottom","");
        sendButton2 = new Element(driver, "sendToCarSmallButton","");
        saveAndDeleteButton = new Element(driver, "map_detail_delete_ll","");
        saveAndDeleteButtonText = new Element(driver, "saveOrDeleteTextView","");
        searchEditTextbox = new Element(driver, "etQueryWords","POI Search");
        searchGlassIcon = new Element(driver, "mapMainSearchIcon","");
        chargeStationListIcon = new Element(driver, "ivMapListIcon","");
        address = new Element(driver, "addressTextView");

    }


    public boolean isPresent()
    {
        return searchEditText.getTextValue().equals("Charge Stations");
    }
}
