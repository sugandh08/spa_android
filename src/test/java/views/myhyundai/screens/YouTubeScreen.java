package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the YouTube screen that is accessed within the YouTube app by any video link in the app that
 * opens a YouTube video. This screen is the screen where the video actually plays. Mostly used to
 * check the video title.
 */
public class YouTubeScreen extends Screen {
    public Element videoTitleTextView;

    public YouTubeScreen(AppiumDriver driver) {
        super(driver);

        videoTitleTextView = new Element(driver, "com.google.android.youtube:id/title");
    }

    public boolean isPresent() {
        return videoTitleTextView.elementExists();
    }
}
