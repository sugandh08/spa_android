package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Terms & Conditions screen that is a view of the Terms & Conditions document and is
 * accessed by tapping to view the Terms & Conditions when logging into the app or through the
 * About & Support screen.
 */
public class TermsAndConditionsScreen extends Screen {

    public Element pageTitleTextView;

    public TermsAndConditionsScreen(AppiumDriver driver) {
        super(driver);
        pageTitleTextView = new Element(driver,"subTitleTxt");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (pageTitleTextView.getTextValue().contains("Terms & Conditions"));
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }
}
