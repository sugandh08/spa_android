package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.HomeScreen;
import views.myhyundai.fragments.EVRemoteCommands;

/**
 * The Electric home screen that is displayed when logged in to an AEEV vehicle.
 */

public class ElectricHomeScreen extends HomeScreen {
    public EVRemoteCommands homeOptions;

    public Element rangeTextView;
    public Element batteryStatusTextView;
    public Element pluginStatusTextView;
    public Element chargingStatusTextView;
    public Element setScheduleButton;
    public Element scheduleTitleTextView;
    public Element scheduleDetailTextView;
    public Element startStopChargeButton;
    public Element longRequestMessage;
    public Element closeButton;
    public Element remoteStartButton;
    public Element vehicleStatusButton;
    public Element refreshButton;
    public Element StartChargeButton;
    public Element remoteStatus;
    public Element departureSchedule;
    public Element refreshTime;
    public Element SetSchedule;
    public Element successfullpopupmessage;
    public Element pluggedNotpluggedin;
    public Element StartDrivingarrow;
    public Element TaptosetSchedule2;
    public Element TaptosetSchedule1;




    /**
     * ElectricHomeScreen constructor
     * @param driver Appium driver
     */
    public ElectricHomeScreen(AppiumDriver driver) {
        super(driver);

        homeOptions = new EVRemoteCommands(driver);

        rangeTextView = new Element(driver,"tvRangeText");
        batteryStatusTextView = new Element(driver,"ae_battery_status_txt");
        pluginStatusTextView = new Element(driver, "cbVoltage");
        chargingStatusTextView = new Element(driver, "cbTime");
        setScheduleButton = new Element(driver,"btnSchedule");
        scheduleTitleTextView = new Element(driver, "ae_schedule_title_txt");
        scheduleDetailTextView = new Element(driver, "ae_schedule_detail_txt");
        startStopChargeButton = new Element(driver, "btnStartCharge","StartCharge");
        StartChargeButton = new Element(driver, "btnStartCharge");
        refreshButton= new Element(driver, "ivRefresh");
        vehicleStatusButton=new Element(driver,"btnVehicleStatus");
        longRequestMessage=new Element(driver,"android:id/message");
        closeButton=new Element(driver,"android:id/button2");
        remoteStartButton=new Element(driver, "btnRemoteStart");
        remoteStatus = new Element(driver,"tvRemoteStatus");
        departureSchedule = new Element(driver,"btnSchedule");
        refreshTime = new Element(driver, "tvRefreshTime");
        SetSchedule = new Element(driver,"subTitleTxt");
        successfullpopupmessage = new Element(driver,"message");
        pluggedNotpluggedin = new Element(driver,"cbVoltage");
        StartDrivingarrow = new Element(driver,"arrow");
        TaptosetSchedule2 = new Element(driver,"tvSchedule2Time");
        TaptosetSchedule1 = new Element(driver,"tvSchedule1Time");
    }

    /**
     * Test if the screen is present. Uses standard wait time.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        // wait for a half second to avoid stale element
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            //
        }
        return pluginStatusTextView.elementExists(waitTime);
    }

    public boolean isLongRequestPopUpPresent(long waitTime)
    {

        // wait for a half second to avoid stale element
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            //
        }
        return longRequestMessage.elementExists();
    }

    public String nextDepartureText_Xpath="(//*[@id='btnSchedule']/*[@class='android.widget.TextView'])[1]";

   // public String selected = "(//android.view.ViewGroup[@resource-id='com.stationdm.bluelink:id/clMainLayout']/*[@class='android.widget.ImageView'])[3] ";
    public Boolean selected = elementExists.byXpath("(//android.view.ViewGroup[@resource-id='com.stationdm.bluelink:id/clMainLayout']/*[@class='android.widget.ImageView'])[3]");

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }

    public void clickOnOkButton(){ tap.elementByXpath("//*[@class='android.widget.Button' and @text='OK']");}

    public String StartDriving1 = "//*[@id='departure_edit_title_switch' and (./preceding-sibling::* | ./following-sibling::*)[@text='Start Driving 1']]";
    public String StartDriving2 = "//*[@id='departure_edit_title_switch' and (./preceding-sibling::* | ./following-sibling::*)[@text='Start Driving 2']]";

    public boolean StartDriving1toggle()
    {
        return elementExists.byXpath(StartDriving1);
    }

    public boolean StartDriving2toggle()
    {
        return elementExists.byXpath(StartDriving2);
    }

}
