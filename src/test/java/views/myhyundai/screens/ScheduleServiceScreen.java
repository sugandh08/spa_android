package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Schedule Service web site that is accessible through either the Side menu, or through
 * the Car Care menu on the home screen.
 */
public class ScheduleServiceScreen extends Screen {
    public Element subTitleTextView;
    public Element Dealername;

    public ScheduleServiceScreen(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "subTitleTxt");
        Dealername = new Element(driver, "ext-component-1");
    }

    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.getTextValue().equals("Car Care Scheduling");
    }


}