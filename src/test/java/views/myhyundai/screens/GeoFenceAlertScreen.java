package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The Geo-Fence alert screen that is accessible through the Alert Settings screen.
 */
public class GeoFenceAlertScreen extends Screen {
    public Element subtitleTextView;
    public Element informationButton;

    public Element searchLocationButton;
    public Element searchLocationEditText;
    public Element currentLocationButton;
    public Element geoFenceNameEditText;
    public Element milesEditText;
    public Element inclusiveRadioButton;
    public Element exclusiveRadioButton;
    public Element circleRadioButton;
    public Element rectangleRadioButton;
    public Element saveButton;
    public Element deleteButton;

    public GeoFenceAlertScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "subTitleTxt");
        informationButton = new Element(driver, "subTitleRightIcon");
        deleteButton = new Element(driver, "llDelete");
        searchLocationButton = new Element(driver, "ivGeoFenceSearch");
        searchLocationEditText = new Element(driver, "tvGeoFenceSearch");
        currentLocationButton = new Element(driver, "geofence_currentloc");
        geoFenceNameEditText = new Element(driver, "etGeoFenceName");
        milesEditText = new Element(driver, "etGeoFenceMiles");
        inclusiveRadioButton = new Element(driver, "rbGeoFenceInclusive");
        exclusiveRadioButton = new Element(driver, "rbGeoFenceExclusive");
        circleRadioButton = new Element(driver, "geofence_circle_rb");
        rectangleRadioButton = new Element(driver, "rbGeoFenceRect");
        saveButton = new Element(driver, "llSave");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Geo-Fence Alert") && geoFenceNameEditText.elementExists();
    }

    public void clickOnDeleteButton()
    {
        tap.elementByXpath("//*[@class='android.widget.TextView']//*[@resource-if='com.stationdm.bluelink:id/alert_geofence_delete_ll']//*[@class='android.widget.LinearLayout']");
    }
}
