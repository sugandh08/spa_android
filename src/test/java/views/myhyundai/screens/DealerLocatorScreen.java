package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.MapScreen;

/**
 * This is the dealer locator screen that is accessible through the map menus.
 */


public class DealerLocatorScreen extends MapScreen {
    public Element dealerLocatorLabelText;
    public Element dealerLocationLabelText;
    public Element sendButton1;
    public Element sendButton2;
    public Element saveOrDeleteButton;
    public Element scheduleServiceLink;
    public Element shopAccessories;
    public Element huntingtonbeachhyundaiLink;
    public Element arrowButton;
    public Element searchEditText;
    public Element searchGlassIcon;
    public Element mapListIcon;
    public Element address;
    public Element preferredDealerStarIcon;
    public Element dealerName;
    public Element miles;
    public Element starRating;
    public Element locationWithIcon;
    public Element sendToCar;
    public Element setAsPreferredPopUp;
    public Element preferredPopupMsg;
    public Element okBtnPopup;
    public Element cancelBtbPopUp;
    public Element Reviewlink;
    public Element RateYourExperience;
    public Element ManageSubscription;



    public DealerLocatorScreen(AppiumDriver driver) {
        super(driver);
        dealerLocatorLabelText = new Element(driver, "tvNote","Dealer Locator");
        dealerLocationLabelText = new Element(driver, "titleTextView","");
        sendButton1 = new Element(driver, "rl_popdetail_bottom","");
        sendButton2 = new Element(driver, "sendToCarSmallButton","");
        saveOrDeleteButton = new Element(driver, "","");
        scheduleServiceLink = new Element(driver, "tvScheduleService","Schedule Service");
        shopAccessories = new Element(driver, "popdetail_accessories_txt","Shop Accessories");
        huntingtonbeachhyundaiLink = new Element(driver, "popdetail_link_txt","www.huntingtonbeachhyundai.com");
        arrowButton = new Element(driver, "ivArrow","");
        searchEditText = new Element(driver, "etQueryWords","Enter Zip");
        searchGlassIcon = new Element(driver, "mapMainSearchIcon","");
        mapListIcon = new Element(driver, "ivMapListIcon","");
        address = new Element(driver, "addressTextView");
        preferredDealerStarIcon =new Element(driver,"favorCheckbox");
        dealerName =new Element(driver,"tvTitle","");
        miles =new Element(driver,"tvAVGFCL");
        starRating =new Element(driver,"rbRatingBar");
        locationWithIcon =new Element(driver,"tvAddress");
        sendToCar =new Element(driver,"tvSend2Car");
        setAsPreferredPopUp =new Element(driver,"alertTitle","Set as Preferred Dealer");
        preferredPopupMsg =new Element(driver,"message");
        okBtnPopup =new Element(driver,"button1");
        cancelBtbPopUp =new Element(driver,"button2");
        Reviewlink = new Element(driver, "tvReview");
        RateYourExperience = new Element(driver, "subTitleTxt", "Rate Your Experience");
        ManageSubscription = new Element(driver,"tvLeftSubTitle","Manage Subscription");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return dealerLocatorLabelText.getTextValue().equals("Dealer Locator");
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }

    public String xpathOfPreferredPopup() {
        return " //*[@resource-id='android:id/message' and //*[@text='Confirms this as your chosen Hyundai dealer for scheduling appointments and dealer communication.']]";
    }

    private String xpathCancelPopup = "//*[@class='android.widget.Button' and @text='Cancel']";

    public boolean CancelBtnExists() {
        return elementExists.byXpath(xpathCancelPopup);
    }

    private String xpathOKPopup = "//*[@class='android.widget.Button' and @text='OK']";

    public boolean OKBtnExists() {
        return elementExists.byXpath(xpathOKPopup);
    }

    public void clickOnOKBtn() {
        tap.elementByXpath(xpathOKPopup);
    }

    public void clickOnCancelBtn() {
        tap.elementByXpath(xpathCancelPopup);
    }
    private String androidBackBtn ="//*[@class='android.widget.ImageButton']";
    public void clickOnAndroidBtn()
    {
        tap.elementByXpath(androidBackBtn);
    }

    private String DismissBtn ="//*[@class='android.widget.Button' and @text='Dismiss']";

    public boolean DismissBtnPresent()
    {
        return elementExists.byXpath(DismissBtn);
    }
    private String ManageSubscriptionBtn ="//*[@class='android.widget.Button' and @text='Manage Subscription']";

    public void tapManageSubscription()
    {
        tap.elementByXpath(ManageSubscriptionBtn);
    }

    public boolean ManageSubscriptionBtnPresent()
    {
        return elementExists.byXpath(ManageSubscriptionBtn);
    }
    private String MessageContent ="//*[@class='android.widget.TextView' and @text='Guidance package is required to use this feature. Please visit MyHyundai.com to add this package.']";

    public boolean MessageContentPresent()
    {
        return elementExists.byXpath(MessageContent);
    }

}