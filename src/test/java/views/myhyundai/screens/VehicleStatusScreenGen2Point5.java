package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.openqa.selenium.ElementNotInteractableException;
import views.Screen;

/**
 * This is the Vehicle Status screen that is accessed on Gen 2 Gas or HEV vehicles by tapping the
 * button in the center of the home page, or on PHEV or AEEV vehicles by tapping the (i)/Information
 * button in the Remote Commands menu on the home page.
 */
public class VehicleStatusScreenGen2Point5 extends Screen
{
    public Element subtitleTextView;
    public Element lastUpdatedText;
    public Element lastUpdateDateTextView;
    public Element lastUpdateTimeText;
    public Element refreshButton;
    public Element brakeStatus;
    public Element washerStatus;
    public Element smartKeyBatteryStatus;
    public Element doorStatus;
    public Element engineStatus;
    public Element climateStatus;
    public Element heatStatus;
    public Element frontDefrostStatus;
    public Element RefreshLoaderText;
    public Element popupContentBesideVehicleDoor;
    public Element textGettingVehicleStatus;
    public Element Lights;
    public Element Lightsstatus;
    public Element FluidLevels;
    public Element OilStatus;
    public Element SmartKeyBattery;
    public Element VehicleConnectivityWarning;
    public Element VehicleConnectivityWarningtext;


    public VehicleStatusScreenGen2Point5(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "subTitleTxt");
        lastUpdatedText =new Element(driver, "","Last Updated");
        lastUpdateDateTextView = new Element(driver, "tvUpdateDate");
        lastUpdateTimeText=new Element(driver,"tvUpdateTime");
        refreshButton = new Element(driver, "ivRefresh");

        doorStatus=new Element(driver,"tvVehicleDoor");
        engineStatus=new Element(driver,"tvVehicleEngine");
        climateStatus=new Element(driver,"tvVehicleClimate");
        frontDefrostStatus=new Element(driver,"tvVehicleFrontDefrost");
        RefreshLoaderText=new Element(driver,"dialogSubtitle");
        popupContentBesideVehicleDoor =new Element(driver,"dialogContent");
        textGettingVehicleStatus =new Element(driver,"dialogTitle");
        Lights = new Element(driver,"remote_vehicle_status_light_title");
        Lightsstatus =  new Element(driver,"tvLightStatus");
        FluidLevels = new Element(driver,"tvFuelLevelTitle");
        SmartKeyBattery = new Element(driver,"tvSmartKeyTitle");
        smartKeyBatteryStatus = new Element(driver,"tvSmartKeyStatus");
        VehicleConnectivityWarning = new Element(driver,"tvConnectWarning");
        VehicleConnectivityWarningtext = new Element(driver,"tvContent");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Vehicle Status");
    }

    /**
     * Grabs the text value of the command name of the first entry in the list. If the element can't be found, returns
     * empty string.
     * @return Name of the most recent command, or empty string if not found.
     */
    public String getRefreshUpdateTime() {
        return lastUpdateTimeText.getTextValue();
    }

    public boolean isOilStatusOk()
    {
        return OilStatus.getTextValue().equals("OK");

    }

    public boolean isBrakeStatusOk()

    {
        return brakeStatus.getTextValue().equals("OK");
    }

    public boolean isWasherStatusOk()
    {

        return washerStatus.getTextValue().equals("OK");
    }
    public boolean isSmartKeyBatteryStatusOk()
    {
        return smartKeyBatteryStatus.getTextValue().equals(" OK");
    }

   // public String heatseat1="//*[@id='seat_heat_img' and ./parent::*[./parent::*[./parent::*[@id='seatSettingViewFL']]]]";
    public String heatseat2="(//*[@id='seat_heat_img' and @x='787']";
   // public String ventseat1="//*[@id='seat_vent_img' and ./parent::*[./parent::*[./parent::*[@id='seatSettingViewFL']]]]";
    public String ventseat2="(//*[@id='seat_vent_img' and @x='787']";

   //public boolean heatedseatonepresent() { return elementExists.byXpath(heatseat1); }
    public boolean heatedseattwopresent() { return elementExists.byXpath(heatseat2); }
   //public boolean ventseatonepresent() { return elementExists.byXpath(ventseat1); }
    public boolean ventseattwopresent() { return elementExists.byXpath(ventseat2); }
}
