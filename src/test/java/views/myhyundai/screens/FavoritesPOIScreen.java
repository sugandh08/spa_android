package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.myhyundai.baseviews.MapScreen;

/**
 * This is the My POI screen that is accessible through the map menus
 */
public class FavoritesPOIScreen extends MapScreen {
    public Element poiSearchEditTextbox;
    public Element poiSearchGlassIcon;
    public Element mapListIcon;
    public Element favoritePointsOfInterestLabelText;
    public Element arrowButton;
    public Element deleteAndSaveButton;
    public Element saveAndDeleteButtonText;

    public FavoritesPOIScreen(AppiumDriver driver) {
        super(driver);
        poiSearchEditTextbox = new Element(driver, "etQueryWords","POI Search");
        poiSearchGlassIcon = new Element(driver, "mapMainSearchIcon","");
        mapListIcon = new Element(driver, "ivMapListIcon","");
        favoritePointsOfInterestLabelText = new Element(driver, "tvNote","Favorite Points of Interest");
        arrowButton = new Element(driver, "iv_popdetail_arrow");
        deleteAndSaveButton= new Element(driver, "map_detail_delete_ll");
        saveAndDeleteButtonText = new Element(driver, "saveOrDeleteTextView","");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return searchEditText.getTextValue().equals("My POI");
    }

    public String xpathOfFavoritePOI(int atIndex){
    //    (//*[@id='com.stationdm.bluelink:id/map_search_xlist']/*/*/*/*/*/*/*[@id='map_search_item_title'])[1]
        return ("//*[@resource-id='com.stationdm.bluelink:id/map_search_xlist']//*[@index='" + atIndex + "']");
    }

    public boolean poiExistsToList(int atIndex) {
      return   elementExists.byXpath("//*[@resource-id='com.stationdm.bluelink:id/map_search_xlist']//*[@index='" + atIndex + "']");
    }
    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }
}
