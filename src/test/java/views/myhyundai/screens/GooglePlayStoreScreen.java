package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Google Play Store screen that is accessible through the Hyundai Virtual Guide, and
 * possibly one other area in the app if you're switching from MyHyundai to Genesis.
 * The screen is for the actual app page in the store.
 */
public class GooglePlayStoreScreen extends Screen {
    public Element appTitleTextView;
    public Element appAuthorTextView;

    public GooglePlayStoreScreen(AppiumDriver driver) {
        super(driver);

        appTitleTextView = new Element(driver, "com.android.vending:id/title_title");
        appAuthorTextView = new Element(driver, "com.android.vending:id/title_creator");
    }

    public boolean isPresent() {
        return appTitleTextView.elementExists() && appAuthorTextView.elementExists();
    }
}
