package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class OffPeakScheduleScreen extends Screen 
{
    public Element title;
    public Element chargeToggle;
    public Element weekendDown;
    public Element weekdays;
    public Element saveButton;
    public Element weekDayTime;
    public Element weekendTime;
    public Element Offpeakscheduletoggle;
    public Element Scheduleonetoggle;
    public Element Scheduletwotoggle;
    public Element StartDrivingSchedule1;
    public Element StartDrivingSchedule2;
    public Element Done;
    public Element Submit;
    public Element offpeakchargingpriority;
    public Element offpeakchargingonly;
    public Element Save;
    public Element Starttime;
    public Element Endtime;
    public Element Schedule1text;
    public Element Schedule2text;



    public OffPeakScheduleScreen(AppiumDriver driver) {
        super(driver);
        title = new Element(driver, "subTitleTxt");
        chargeToggle = new Element(driver,"sbDetail");
        weekendDown = new Element(driver,"ivWeekendsDown");
        weekdays = new Element(driver,"ivWeekdaysDown");
        saveButton = new Element(driver,"rippleSave");
        weekDayTime = new Element(driver,"tvWeekdaysTime");
        weekendTime = new Element(driver,"tvWeekendsTime");
        Offpeakscheduletoggle = new Element(driver,"sbOffPeakSchedule");
        Scheduleonetoggle = new Element(driver,"sbSchedule1");
        Scheduletwotoggle = new Element(driver,"sbSchedule2");
        StartDrivingSchedule1 = new Element(driver,"tvSchedule1Name");
        StartDrivingSchedule2 = new Element(driver,"tvSchedule2Name");
        Done = new Element(driver,"rippleDone");
        Submit = new Element(driver,"rippleSubmit");
        offpeakchargingpriority = new Element(driver,"tvScheduleOffPeakOption");
        offpeakchargingonly = new Element(driver,"mOnlyCheckImage");
        Save = new Element(driver,"osev_offpeak_bottom_rl");
        Starttime = new Element(driver,"tvScheduleWeekdaysTimeArea");
        Endtime = new Element(driver,"tvScheduleWeekendsTimeArea");
        Schedule1text = new Element(driver,"tvSchedule1Time");
        Schedule2text = new Element(driver,"tvSchedule2Time");
    }

    public boolean isPresent()
    {
        return title.getTextValue().equals("Off-Peak Schedule");

    }

    public boolean OffPeakScheduleisPresent()
    {
        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='Off-Peak Schedule']");
    }

    public boolean ScheduleOneisPresent()
    {
        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='Schedule 1']");
    }

    public boolean ScheduleTwoisPresent()
    {
        return elementExists.byXpath("//*[@class='android.widget.TextView' and @text='Schedule 2']");
    }

    public String xpath_Minute()
    {
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options1']";
    }

    public String xpath_WeekendMinute(){
        return "//android.view.View[@resource-id='com.stationdm.bluelink:id/options2']";
    }
}
