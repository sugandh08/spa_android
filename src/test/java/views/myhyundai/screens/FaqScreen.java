package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * The FAQ screen accessed through the About & Support screen.
 */
public class FaqScreen extends Screen {
    public Element back;
    public Element blueLinkFaq;
    public Element carCareFaq;

    public Element blueLinkFaqQuestion1;

    public Element carCareFaqQuestion1;

    public FaqScreen(AppiumDriver driver) {
        super(driver);

        String faqQuestionId = "faq_item_question";

        back = new Element(driver,"main_title_left_icon","");
        blueLinkFaq = new Element(driver,"","Blue Link FAQ");
        carCareFaq = new Element(driver,"","Car Care FAQ");

        blueLinkFaqQuestion1 = new Element(driver, faqQuestionId,
                "What vehicle features do I need to have access to Remote Start & how long will the vehicle run after starting?");

        carCareFaqQuestion1 = new Element(driver, faqQuestionId,
                "Why is it important to maintain my vehicle?");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (blueLinkFaq.elementExists() && carCareFaq.elementExists());
    }

    /**
     * Check if blue link faq is selected and if the correct faq item is showing underneath it.
     * @return True if present, false if not.
     */
    public boolean isBlueLinkFaqPresent() {
        return blueLinkFaq.getSelectedValue() && blueLinkFaqQuestion1.elementExists();
    }

    /**
     * Check if car care faq is selected and if the correct faq item is showing underneath it.
     * @return True if present, false if not.
     */
    public boolean isCarCareFaqPresent() {
        return carCareFaq.getSelectedValue() && carCareFaqQuestion1.elementExists();
    }
}
