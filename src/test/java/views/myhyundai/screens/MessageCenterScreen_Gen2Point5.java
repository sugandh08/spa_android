package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class MessageCenterScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public  Element deleteLink;
    public  Element tabDeleted;
    public Element tabInbox;
    public Element refreshIcon;
    public Element loadingCircle;
    public  Element noMsgToDisplay;
    public  Element moveToInboxLink;
    public Element btnContactUs;
    public Element contactUsPage;

    public MessageCenterScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "subTitleTxt","Message Center");
        deleteLink =new Element(driver,"tvDelete");
        tabDeleted =new Element(driver,"","DELETED");
        tabInbox =new Element(driver,"","INBOX");
        refreshIcon = new Element(driver,"subTitleRightIcon");
        loadingCircle=new Element(driver,"circleProgress");
        noMsgToDisplay =new Element(driver,"tvNoMessage");
        moveToInboxLink =new Element(driver,"tvMoveInbox");
        btnContactUs =new Element(driver,"btnContactUS","CONTACT US");
        contactUsPage =new Element(driver,"","Consumer Assistance Center");

    }




    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("Message Center");
    }

}
