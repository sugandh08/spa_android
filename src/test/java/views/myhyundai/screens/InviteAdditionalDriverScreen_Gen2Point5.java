package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

import java.util.Random;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class InviteAdditionalDriverScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public Element backButton;
    public Element sendInvitationButton;
    public Element cancelButton;
    public Element firstNameTextField;
    public Element lastNameTextField;
    public Element emailTextField;
    public Element selectContact;


    public InviteAdditionalDriverScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "subTitleTxt");
        backButton = new Element(driver, "main_title_left_icon");
        sendInvitationButton = new Element(driver, "btnSendInvitation");
        cancelButton = new Element(driver, "bt_cancel");
        firstNameTextField = new Element(driver, "etFirstName");
        lastNameTextField = new Element(driver, "etLastName");
        emailTextField = new Element(driver, "etEmail");
        selectContact = new Element(driver,"tvInviteFromContacts");

    }

    public void sendAdditionalDriverInvite(String fName,String lName, String email) {
        firstNameTextField.enterText(fName);
        lastNameTextField.enterText(lName);



        emailTextField.enterText(email);

        sendInvitationButton.tap();


    }

    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().contains("Invite Additional Driver");
    }


}
