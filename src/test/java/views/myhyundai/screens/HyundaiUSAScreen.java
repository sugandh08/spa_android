package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Hyundai USA web site that is accessible through the About & Support web site
 */
public class HyundaiUSAScreen extends Screen {
    public Element subTitleText;
    /**
     * Standard Screen constructor.
     *
     * @param driver The Appium driver for automation controls. Each screen has custom actions that need access
     *               to this driver.
     */
    public HyundaiUSAScreen(AppiumDriver driver) {
        super(driver);
        subTitleText = new Element(driver, "subTitleTxt");
    }

    public boolean isPresent() {
        return subTitleText.elementExists();
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }

}
