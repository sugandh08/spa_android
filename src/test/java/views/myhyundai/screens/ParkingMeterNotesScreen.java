package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Parking Meter Notes screen that is accessible through the Parking Meter screen by
 * pressing the Notes button
 */
public class ParkingMeterNotesScreen extends Screen {
    private Element subtitleTextView;
    public Element backButton;
    public Element notesEditText;
    public Element saveButton;

    public ParkingMeterNotesScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver,"sub_title_txt");
        backButton = new Element(driver,"main_title_left_icon");
        notesEditText = new Element(driver,"edit_notes");
        saveButton = new Element(driver,"ll_parking_meter_save");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (subtitleTextView.getTextValue().equals("Parking Meter Notes") &&
                saveButton.elementExists() &&
                notesEditText.elementExists());
    }

    public void addParkingMeterNotes()
    {
        notesEditText.enterText("Add text to parking meter notes");
        saveButton.tap();
    }
}