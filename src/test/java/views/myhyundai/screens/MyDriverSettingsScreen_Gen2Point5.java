package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.apache.tools.ant.taskdefs.Echo;
import views.Screen;

/**
 * This is the profile screen that is accessible through the Side menu by tapping the car name or VIN
 * section at the very bottom of the menu.
 */
public class MyDriverSettingsScreen_Gen2Point5 extends Screen {


    public Element subTitleTextView;
    public Element backButton;
    public Element settingsPanel;
    public Element favoritePlacesPanel;
    public Element twentyFourHoursOnButton;
    public Element twentyFourHoursOffButton;
    public Element saveButton;
    public Element cancelButton;
    public Element useMapScreenVolButton;
    public Element useMapScreenVolButtonOrKnob;
    public Element seatEasyAccessExtended;
    public Element seatEasyAccessNormal;
    public Element seatEasyAccessOff;
    public Element distanceSpeedKm;
    public Element distanceSpeedMi;
    public Element tempertureF;
    public Element temperatureC;


    public MyDriverSettingsScreen_Gen2Point5(AppiumDriver driver) {
        super(driver);

        subTitleTextView = new Element(driver, "subTitleTxt");
        backButton = new Element(driver, "main_title_left_icon");
        settingsPanel = new Element(driver, "", " SETTINGS ");
        favoritePlacesPanel = new Element(driver, "", " FAVORITE PLACES ");
        twentyFourHoursOnButton = new Element(driver, "", "on");
        twentyFourHoursOffButton = new Element(driver, "", "off");
        saveButton = new Element(driver, "btnSave");
        cancelButton = new Element(driver, "btnCancel");
        useMapScreenVolButton = new Element(driver, "", "Use Map Screen Volume Buttons");
        useMapScreenVolButtonOrKnob = new Element(driver, "", "Use Map Screen Volume Buttons or Volume Knob");
        seatEasyAccessExtended = new Element(driver, "", "Extended");
        seatEasyAccessNormal = new Element(driver, "", "Normal");
        seatEasyAccessOff = new Element(driver, "", "Off");
        distanceSpeedKm=new Element(driver,"","Kilometer (Km)");
        distanceSpeedMi=new Element(driver,"","Mile (mi)");
        tempertureF=new Element(driver,"","Fahrenheit");
        temperatureC=new Element(driver,"","Celsius");
    }


    /**
     * Builds an xpath to the Accordian Expand element using the setting name
     *
     * @param settingName The relative setting to the toggle element
     * @return The xpath to the Accordian Expand element
     */
    private String getXpathOfAccordianExpandButton(String settingName) {

        return "/" +
                "/android.widget.TextView[@text='" + settingName + "']";
    }

    public void tapAccordianExpandButton(String settingName) {
        tap.elementByXpath(getXpathOfAccordianExpandButton(settingName));
    }

    private String getXpathOfToggle(String settingName) {
        return "//*[@resource-id='com.stationdm.bluelink:id/sbToggleSwitch' and (./preceding-sibling::* | ./following-sibling::*)[./*[@text='"+settingName+"']]]";
    }

    public void tapToggle(String settingName) {
        tap.elementByXpath(getXpathOfToggle(settingName));
    }

    public boolean getCheckedValueOfToggle(String settingName) {
        return attribute.getCheckedValueByXpath(getXpathOfToggle(settingName));
    }

    /**
     * Test if the screen is present.
     *
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().contains("Driver Settings");
    }


    public void setDateAndTimeSettings(boolean gps, boolean daylightSaving, String twentyFourHoursOnOff) {
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("GPS Time")) ^ gps) {
            tapToggle("GPS Time");
        }

        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Daylight Saving Time")) ^ daylightSaving) {
            tapToggle("Daylight Saving Time");
        }

        switch (twentyFourHoursOnOff) {
            case "on":
                twentyFourHoursOnButton.tap();
                break;
            case "off":
                twentyFourHoursOffButton.tap();
                break;

            default:
                System.out.println("Invalid choice selected");
        }


    }

    public void setSoundSettings(boolean navigationGuidance, boolean proximityWarning, boolean navigationDuringCalls, boolean startUpVolumeLimit, boolean useMapScreenVolumeButton, boolean usMapScreenVolumeAndKnobButton) {
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Navigation Guidance")) ^ navigationGuidance) {
            tapToggle("Navigation Guidance");
        }

        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Proximity Warning")) ^ proximityWarning) {
            tapToggle("Proximity Warning");
        }
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Navigation During Calls")) ^ navigationDuringCalls) {
            tapToggle("Navigation During Calls");
        }
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Start - up Volume Limit")) ^ startUpVolumeLimit) {
            tapToggle("Start - up Volume Limit");
        }

        if (useMapScreenVolButton.getCheckedValue() ^ useMapScreenVolumeButton) {

            useMapScreenVolButton.tap();


        }
        if (useMapScreenVolButtonOrKnob.getCheckedValue() ^ usMapScreenVolumeAndKnobButton) {

            useMapScreenVolButtonOrKnob.tap();

        }


    }

    public void setVehicleSettings(boolean gearPositionPopUp, boolean wiperLightsDisplay, boolean icyRoadWarning, boolean welcomeSound, boolean seatPositionChangeAlert, String seatEasyAccess, boolean wirelessChargingSystem) {
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Gear Position Pop - Up")) ^ gearPositionPopUp) {
            tapToggle("Gear Position Pop - Up");
        }

        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Wiper / Lights Display")) ^ wiperLightsDisplay) {
            tapToggle("Wiper / Lights Display");
        }
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Icy Road Warning")) ^ icyRoadWarning) {
            tapToggle("Icy Road Warning");
        }
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Welcome Sound")) ^ welcomeSound) {
            tapToggle("Welcome Sound");
        }

        swipe.fromBottomEdge(1);
        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Seat Position Change Alert")) ^ seatPositionChangeAlert) {
            tapToggle("Seat Position Change Alert");
        }

        switch (seatEasyAccess) {
            case "Extended":
                seatEasyAccessExtended.tap();
                break;
            case "Normal":
                seatEasyAccessNormal.tap();
                break;
            case "Off":
                seatEasyAccessOff.tap();
                break;
            default:
                System.out.println("Invalid choice selected");
        }
        swipe.fromBottomEdge(1);

        if (attribute.getCheckedValueByXpath(getXpathOfToggle("Wireless Charging System")) ^ wirelessChargingSystem) {
            tapToggle("Wireless Charging System");
        }
    }


    public void setUnitSettings(String distanceSpeed, String temp) {


        switch (distanceSpeed) {
            case "km":
                distanceSpeedKm.tap();
                break;
            case "mi":
                distanceSpeedMi.tap();
                break;
            default:
                System.out.println("Invalid choice selected");
        }

        switch (temp) {
            case "F":
                tempertureF.tap();
                break;
            case "C":
                temperatureC.tap();
                break;
            default:
                System.out.println("Invalid choice selected");
        }

    }
}
