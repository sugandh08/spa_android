package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Hyundai Accessories web site that is reached through the About & Support screen.
 */
public class AccessoriesScreen extends Screen {

    public Element backButton;
    public Element subTitleTextView;
    public Element webPreviousImageView;
    public Element webNextImageView;
    public Element webRefreshImageView;
    public Element webStopImageView;

    public AccessoriesScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        subTitleTextView = new Element(driver, "subTitleTxt");
        webPreviousImageView = new Element(driver,"iv_webview_previous");
        webNextImageView = new Element(driver,"iv_webview_next");
        webRefreshImageView = new Element(driver,"iv_webview_refresh");
        webStopImageView = new Element(driver,"iv_webview_stop");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subTitleTextView.elementExists() && subTitleTextView.getTextValue().equals("Accessories");
    }
}
