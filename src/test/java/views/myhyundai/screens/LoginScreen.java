package views.myhyundai.screens;

import config.Account;
import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Login screen that is displayed after launching the app, which allows users to
 * enter login data or enter Demo mode.
 */
public class LoginScreen extends Screen {
	  private static final String RESOURCE_ID_PREFIX = "com.stationdm.bluelink:id/";
    public Element usernameEditText;
    public Element passwordEditText;
    public Element keepMeLoggedInCheckbox;
    public Element guestLoginButton;
    public Element loginButton;
    public Element guestvehicle;
    public Element guestVehicleyearlist;
    public Element guestVehiclenamelist;
    public Element guestselectvehicle;
    public Element guestvehiclename;
    public Element guestvehiclesecond;
    public Element guestvehicleyear;
    public Element Displayaudio;
    public Element next;
    public Element guestheader;
    public Element TermsandConditiontext;

    /**
     * LoginScreen constructor
     * @param driver Appium driver
     */

    public LoginScreen(AppiumDriver driver) {
        super(driver);

        usernameEditText = new Element(driver,"emailView");
        passwordEditText = new Element(driver,"passwordView");
        keepMeLoggedInCheckbox = new Element(driver,"keepBox", "Keep me logged in");
        /*  guestLoginButton = new Element(driver,"guestLoginButton");
        loginButton = new Element(driver,"loginButton");*/
        guestLoginButton = new Element(driver, "guestLoginButton","Guest Login");
        loginButton = new Element(driver,"loginButton","Login");
        guestvehicle = new Element(driver,"ivCircleBg");
        guestVehicleyearlist = new Element(driver,"ccSelectVehicleHeaderlist");
        guestVehiclenamelist = new Element(driver,"mExpandableListView");
        guestselectvehicle = new Element(driver,"demo_skip_selectvec_btn");
        guestvehiclename = new Element(driver,"","ELANTRA");
        guestvehicleyear = new Element(driver,"","2016");
        guestvehiclesecond = new Element(driver,"","ELANTRA GT");
        Displayaudio = new Element(driver,"cc_headunit_item_img");
        next = new Element(driver,"tvRight");
        guestheader = new Element(driver,"tvLeftSubTitle");
        TermsandConditiontext = new Element(driver,"message");

    }

    /**
     * Login with credentials defined in an Account object
     * @param account The Account object containing the username and password
     */
    public void loginWithAccount(Account account) {

        this.usernameEditText.enterText(account.email);
        this.passwordEditText.enterText(account.password);
        loginButton.tap();
    }

    public void loginWithkeepmeloggedinAccount(Account account) {

        this.usernameEditText.enterText(account.email);
        this.passwordEditText.enterText(account.password);
        keepMeLoggedInCheckbox.tap();
        loginButton.tap();
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
      /* return (usernameEditText.elementExists(waitTime) && passwordEditText.elementExists(waitTime));*/
    	 return guestLoginButton.elementExists() && loginButton.elementExists();
    }

    public String Guestlogin ="//android.widget.TextView[@text='GUEST']";
    public boolean GuestloginPagepresent()
    {
        return elementExists.byXpath(Guestlogin);
    }

    public String Switchvehicle = "//*[@id='menu_txt' and @text='Switch Vehicle']";
    public void tapSwitchvehicle()
    {
        tap.elementByXpath(Switchvehicle);
    }

    public void clickOnHamburgerMenuButton() {
        tap.elementByXpath("//*[@content-desc='Navigate up']", 7000);
    }


}