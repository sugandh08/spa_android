package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class StartDrivingSettingsScreen extends Screen {

    public Element subTitleTextView;
    public Element saveButton;

    /**
     * Standard Screen constructor.
     *
     * @param driver The Appium driver for automation controls. Each screen has custom actions that need access
     *               to this driver.
     */
    public StartDrivingSettingsScreen(AppiumDriver driver) {
        super(driver);
        subTitleTextView = new Element(driver, "sub_title_txt");
        saveButton = new Element(driver, "osev_departure_save_txt");
      }

      private final String startDriving1Toggle_Xpath = "//*[@resource-id='com.stationdm.bluelink:id/departure_edit_title_switch' and (./preceding-sibling::* | ./following-sibling::*)[@text='Start Driving 1']]";
      private final String startDriving2Toggle_Xpath = "//*[@resource-id='com.stationdm.bluelink:id/departure_edit_title_switch' and (./preceding-sibling::* | ./following-sibling::*)[@text='Start Driving 2']]";
      public final String startDriving1HourWheel_Xpath = "//*[@resource-id='com.stationdm.bluelink:id/optionspicker']//*[@resource-id='com.stationdm.bluelink:id/options1']";
      public final String startDriving1MinuteWheel_Xpath = "//*[@resource-id='com.stationdm.bluelink:id/optionspicker']//*[@resource-id='com.stationdm.bluelink:id/options2']";
      public final String startDriving1AMPMWheel_Xpath = "//*[@resource-id='com.stationdm.bluelink:id/optionspicker']//*[@resource-id='com.stationdm.bluelink:id/options3']";

    public void setStartDrivingSettingsStates(boolean startDriving1Toggle, boolean startDriving2Toggle) {
        if ( attribute.getCheckedValueByXpath(startDriving1Toggle_Xpath)^ startDriving1Toggle) {
            tap.elementByXpath(startDriving1Toggle_Xpath);
        }

        if (attribute.getCheckedValueByXpath(startDriving2Toggle_Xpath) ^ startDriving2Toggle) {
            tap.elementByXpath(startDriving2Toggle_Xpath);
        }
    }

    public boolean isPresent() {
        return subTitleTextView.elementExists();
    }
}
