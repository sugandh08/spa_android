package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import org.apache.tools.ant.taskdefs.Echo;
import views.Screen;

import java.security.interfaces.ECKey;

/**
 * This is the Alert Settings screen that is reached through the Side menu.
 */
public class AlertSettingsScreen extends Screen {
    public Element backButton;
    public Element subTitleTextView;
    public Element speedAlertToggle;
    public Element valetAlertToggle;
    public Element curfewAlertToggle;
    public Element geoFenceAlertToggle;
    public Element speedAlertButton;
    public Element speedAlertValue;
    public Element valetAlertButton;
    public Element valetAlertValue;
    public Element curfewAlertButton;
    public Element addNewCurfewAlertButton;
    public Element geoFenceAlertButton;
    public Element addNewGeoFenceAlertButton;
    public Element saveButton;
    public Element deleteGeoFenceButton;
    public Element SpeedAlertlogo;
    public Element ValetAlertlogo;
    public Element CurfewAlertlogo;
    public Element GeofenceAlertlogo;

    public AlertSettingsScreen(AppiumDriver driver) {
        super(driver);

        deleteGeoFenceButton = new Element(driver,"llDelete");
        subTitleTextView = new Element(driver, "subTitleTxt");
        speedAlertToggle = new Element(driver, "swSpeedSwitch");
        valetAlertToggle = new Element(driver, "swValetSwitch");
        curfewAlertToggle = new Element(driver, "swCurfewSwitch");
        geoFenceAlertToggle = new Element(driver, "swGeoFenceSwitch");
        speedAlertButton = new Element(driver, "vSpeedClickView");
        speedAlertValue = new Element(driver, "tvSpeedValue");
        valetAlertButton = new Element(driver, "vValetClickView");
        valetAlertValue= new Element(driver, "tvValetValue","");
        curfewAlertButton = new Element(driver, "tvCurfewText", "");
        addNewCurfewAlertButton =new Element(driver, "","+ Add New Curfew Alert");
        geoFenceAlertButton = new Element(driver, "", "Set up geo-fence boundaries for your vehicle to stay within or not to enter.");
        addNewGeoFenceAlertButton = new Element(driver, "tvGeoFenceAdd","+ Add New Geo-Fence Alert");
        saveButton = new Element(driver,"llSave");
        SpeedAlertlogo = new Element(driver,"ivSpeedIcon");
        ValetAlertlogo = new Element(driver,"ivValetIcon");
        CurfewAlertlogo = new Element(driver,"ivCurfewIcon");
        GeofenceAlertlogo = new Element(driver,"ivGeoFenceIcon");


    }

    /**
     * Test if the screen is present. Uses standard wait time
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        System.out.println(subTitleTextView.elementExists(waitTime));
        return subTitleTextView.elementExists(waitTime) && subTitleTextView.getTextValue().equals("Alert Settings");
    }

    public String xpathOfExisitingGeoFenceAlert(int atIndex){
        return  "//android.widget.LinearLayout[@index='"+atIndex+"']";
    }
    public boolean getGeoFenceAlertLimit(int atIndex){
       return elementExists.byXpath(xpathOfExisitingGeoFenceAlert(atIndex)); }


    public void clickOnGeoFenceAlert(int atIndex){
        tap.elementByXpath(xpathOfExisitingGeoFenceAlert(atIndex));   }



        private String xpathOfExistingCurfewAlert="(//*[@resource-id='com.stationdm.bluelink:id/tvCurfewFrom'])[1]";

    public boolean checkIfCurfewAlertAlreadyAdded(){
       return elementExists.byXpath(xpathOfExistingCurfewAlert);  }

    public void clickOnCurfewAlert(){
        tap.elementByXpath(xpathOfExistingCurfewAlert);
    }



       public String xpathCheckGeoFenceAlert(){
        return  "(//*[@id='rvGeoFence']/*/*/*[@id='tvGeoFenceShape'])[1]";
       }


    public String xpathOfFromCurfewAlert="(//*[@resource-id='com.stationdm.bluelink:id/tvCurfewFrom'])[1]";
    }
