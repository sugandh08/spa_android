package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Hyundai Hope on Wheels web site that is accessible through the About & Support screen.
 */
public class HyundaiHopeOnWheelsScreen extends Screen {
    public Element backButton;
    public Element pageTitleTextView;
    public Element webPreviousButton;
    public Element webNextButton;
    public Element webRefreshButton;
    public Element webStopButton;

    public HyundaiHopeOnWheelsScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        pageTitleTextView = new Element(driver,"sub_title_txt");
        webPreviousButton = new Element(driver,"iv_webview_previous");
        webNextButton = new Element(driver,"iv_webview_next");
        webRefreshButton = new Element(driver,"iv_webview_refresh");
        webStopButton = new Element(driver,"iv_webview_stop");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (pageTitleTextView.getTextValue().contains("Hyundai Hope On Wheels") &&
                webPreviousButton.elementExists());
    }
}
