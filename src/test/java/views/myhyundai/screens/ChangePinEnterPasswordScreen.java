package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the password entry screen that is part of the PIN reset process.
 */
public class ChangePinEnterPasswordScreen extends Screen {
    public Element subtitleTextView;
    public Element passwordTitleTextView;
    public Element enterPasswordEditText;
    public Element submitButton;

    public ChangePinEnterPasswordScreen(AppiumDriver driver) {
        super(driver);

        subtitleTextView = new Element(driver, "sub_title_txt");
        passwordTitleTextView = new Element(driver, "tv_remote_status");
        enterPasswordEditText = new Element(driver, "password");
        submitButton = new Element(driver, "remote_setting_submit");
    }

    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("Change PIN") && passwordTitleTextView.elementExists();
    }
}
