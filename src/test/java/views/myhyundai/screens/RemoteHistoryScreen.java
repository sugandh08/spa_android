package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

import javax.swing.plaf.synth.SynthEditorPaneUI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

/**
 * This is the Remote History screen that is accessible from the home screen by tapping the button
 * in the top right.
 */
public class RemoteHistoryScreen extends Screen {
    public Element subtitleTextView;
    public Element remoteCommandNameTextView;
    public Element remoteCommandDateTextView;
    public Element remoteCommandTimeTextView;
    public Element remoteCommandPassPendingStatusTextView;
    public Element remoteCommandFailStatusImageView;
    public Element remoteCommandFailedDescription;

    public RemoteHistoryScreen(AppiumDriver driver) {
        super(driver);
        subtitleTextView = new Element(driver, "subTitleTxt");
        remoteCommandNameTextView = new Element(driver, "tvRemoteName");
        remoteCommandDateTextView = new Element(driver, "tvDate");
        remoteCommandTimeTextView = new Element(driver, "tvTime");
        remoteCommandPassPendingStatusTextView = new Element(driver, "tvRemoteResult");
        remoteCommandFailStatusImageView = new Element(driver, "ivShowErrorContent");
        remoteCommandFailedDescription = new Element(driver, "tvDescription");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return subtitleTextView.getTextValue(waitTime).equals("Remote History");
    }

    // root xpath
    public String mostRecentCommandXpathRoot= "/" +
                "/androidx.recyclerview.widget.RecyclerView[@resource-id='com.stationdm.bluelink:id/rvRemoteHistory']" +
                "/android.view.ViewGroup[@index='0']";

    // details xpaths (Remote command type, date, time)
    private String mostRecentCommandStatusContainerXpath = mostRecentCommandXpathRoot+
            "/android.widget.RelativeLayout[@resource-id='com.stationdm.bluelink:id/rlRightLayout']";
    private String mostRecentCommandNameXpath = mostRecentCommandXpathRoot +
            "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvRemoteName']";
    private String mostRecentCommandDateTextXpath = mostRecentCommandXpathRoot +
            "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvDate']";
    private String mostRecentCommandTimeTextXpath = mostRecentCommandXpathRoot +
            "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvTime']";

    // status xpaths
    private String mostRecentCommandResultTextXpath = mostRecentCommandStatusContainerXpath +
            "/android.widget.TextView[@resource-id='com.stationdm.bluelink:id/tvRemoteResult']";
    private String mostRecentCommandStatusIconXpath = mostRecentCommandStatusContainerXpath +
            "/android.widget.ImageView[@resource-id='com.stationdm.bluelink:id/ivShowErrorContent']";

    /**
     * Grabs the text value of the command date of the first entry in the list. If the element can't be found, returns
     * empty string.
     *
     * @return Name of the most recent command, or empty string if not found.
     */
    public String getMostRecentCommandDate() {
        return elementExists.byXpath(mostRecentCommandDateTextXpath) ?
                attribute.getTextValueByXpath(mostRecentCommandDateTextXpath) : "";
    }

    /**
     * Grabs the text value of the command name of the first entry in the list. If the element can't be found, returns
     * empty string.
     *
     * @return Name of the most recent command, or empty string if not found.
     */
    public String getMostRecentCommandTime() {
        return elementExists.byXpath(mostRecentCommandTimeTextXpath) ?
                attribute.getTextValueByXpath(mostRecentCommandTimeTextXpath) : "";
    }

    /**
     * Grabs the text value of the command name of the first entry in the list. If the element can't be found, returns
     * empty string.
     *
     * @return Name of the most recent command, or empty string if not found.
     */
    public String getMostRecentCommandName() {
        return elementExists.byXpath(mostRecentCommandNameXpath) ?
                attribute.getTextValueByXpath(mostRecentCommandNameXpath) : "";
    }

    /**
     * Grabs the text value of the status of hte first entry in the list. If the element can't be found, returns
     * empty string. If the element is the failure icon, returns "Fail".
     *
     * @return Status of the most recent command ("Fail" if it's a failed command), or empty string if not found.
     */
    public String getMostRecentCommandStatus() {
        if (elementExists.byXpath(mostRecentCommandResultTextXpath)) {
            return attribute.getTextValueByXpath(mostRecentCommandResultTextXpath);
        } else if (elementExists.byXpath(mostRecentCommandStatusIconXpath)) {
            // need a custom return because it shows an icon, not text, for some reason
            return "Fail";
        } else {
            // test case will fail, elements could not be found on screen
            return "";
        }
    }


}








