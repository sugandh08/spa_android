package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the About & Support screen, which is mainly reached through the Side menu.
 */
public class AboutAndSupportScreen extends Screen {
    // Write up screen for 'Bluetooth Videos'
    // Write up screen for 'FAQ' (needs scroll down)
    // Write up screen for 'Terms & Conditions' (needs scroll down)

    public Element back;
    public Element subtitle;

    // Blue Link section
    public Element bluelinkLogo;
    public Element blueLinkText;
    public Element talkToBlueLinkAgent;
    public Element blueLinkInformation;
    public Element blueLinkHowToVideos;

    // Bluetooth section
    public Element bluetoothText;
    public Element bluetoothAndMultimediaSupport;
    public Element bluetoothVideos;

    // Guides section
    public Element guidesText;
    public Element Tutorials;
    public Element RemoteStartPresets;
    public Element Description;
    public Element gettingStartedGuide;
    public Element indicatorGuide;
    public Element ownersManual;
    public Element maintenanceInformation;
    public Element warrantyInfo;

    //Hyundai Resources
    public  Element hyundaiResourceText;
    public  Element gettingStarted;
    public  Element blueLink;
    public  Element vehicleHealth;
    public  Element manualsAndWarranties;
    public  Element bluetoothAssistance;
    public  Element multimediaAndNavigation;
    public  Element vehicleOperation;
    public  Element generalInformation;
    public Element BluelinkTextunderHyundairesource;

    // Links section
    public Element safetyRecallCheck;
    public Element linksText;
    public Element myHyundaiCom;
    public Element subTitleTxt;
    public Element hyundaiAccessories;
    public Element hyundaiPartsAndCollisionInfo;
    public Element hyundaiHopeOnWheels;
    public Element hyundaiUSA;

    // Contact Us section
    public  Element contactUsText;
    public Element emailAppSupport;
    public Element emailCustomerService;
    public Element emailincidentfeaturename;
    public Element Next;
    public Element Finish;

    // Version Framework section
    public Element versionText;
    public Element faq;
    public Element termsAndConditions;
    public Element privacyPolicy;
    public Element bluelinkFAQ;

    //Popup confirmation
    public Element Popupalert;
    public Element Cancel;
    public Element Continue;

    //Page title
    public Element Title;

    public AboutAndSupportScreen(AppiumDriver driver) {
        super(driver);

        back = new Element(driver,"main_title_left_icon");
        subtitle = new Element(driver, "sub_title_txt");
        talkToBlueLinkAgent = new Element(driver,"about_callagent_txt");

        // Blue Link
        bluelinkLogo = new Element(driver,"subTitleIcon");
        blueLinkText = new Element(driver, "", "blueLinkText");
        blueLinkInformation = new Element(driver,"tvInformation") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        blueLinkHowToVideos = new Element(driver,"tvHowToVideos") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };

        // Bluetooth
        bluetoothText = new Element(driver, "","BLUETOOTH");
        bluetoothAndMultimediaSupport = new Element(driver, "tvBtMmSupport","");
        bluetoothVideos = new Element(driver, "tvBluetoothVideo","Bluetooth Videos") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };

        // Guides
        guidesText= new Element(driver,"tvGettingStartedGuide","GUIDES");
        Tutorials= new Element(driver, "tvTutorial");
        RemoteStartPresets = new Element(driver, "tvRemoteStartPreset");
        Description=new Element(driver,"tvDescription");
        gettingStartedGuide  = new Element(driver,"tvGettingStartedGuide","Getting Started Guide");
        indicatorGuide = new Element(driver,"tvIndicatorGuide") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        ownersManual = new Element(driver,"tvOwnersManual") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        maintenanceInformation = new Element(driver,"tvMaintenanceInfo") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        warrantyInfo = new Element(driver,"tvWarrantyInfo") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };

        //Hyundai Resources
        hyundaiResourceText = new Element(driver, "" , "HYUNDAI RESOURCES");
        gettingStarted = new Element(driver, "tvGettingStarted" , "Getting Started");
        blueLink = new Element(driver, "" , "Bluelink");
        vehicleHealth = new Element(driver, "" , "Vehicle Health");
        manualsAndWarranties = new Element(driver, "" , "Manuals & Warranties");
        bluetoothAssistance = new Element(driver, "" , "Bluetooth Assistance");
        multimediaAndNavigation = new Element(driver, "" , "Multimedia and Navigation");
        vehicleOperation = new Element(driver, "" , "Vehicle Operation");
        generalInformation = new Element(driver, "" , "General Information");
        BluelinkTextunderHyundairesource = new Element(driver,"tvLeftSubTitle");

        // Links
        linksText = new Element(driver, "" , "LINKS");
        myHyundaiCom = new Element(driver,"about_myhyundai_txt") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        hyundaiUSA = new Element(driver,"tvHyundaiUsa") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        hyundaiAccessories = new Element(driver,"about_link_accessories_txt") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        hyundaiPartsAndCollisionInfo = new Element(driver,"about_link_awareness_txt") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        hyundaiHopeOnWheels = new Element(driver,"about_link_hope_txt") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        safetyRecallCheck = new Element(driver,"tvSafetyRecall");

        // Contact Us

        contactUsText = new Element(driver, "" , "CONTACT US");
        emailAppSupport = new Element(driver,"tvEmailSupport") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        emailCustomerService = new Element(driver,"tv_email_contanct") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        emailincidentfeaturename = new Element(driver,"tvFeatureName");
        Next = new Element(driver,"tvNext");
        Finish = new Element(driver,"tvFinish");

        // Version
        versionText = new Element(driver, "about_version_txt" ,"");
        faq = new Element(driver, "tvFAQ") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        termsAndConditions = new Element(driver, "about_tc_ll") {
            @Override
            public void tap() {
                super.tapThenWait();
            }
        };
        privacyPolicy =new Element(driver, "tvPrivacyPolicy");
        bluelinkFAQ = new Element(driver,"","Bluelink FAQ");

        //Popupalert
        Popupalert = new Element(driver,"alertTitle");
        Cancel = new Element(driver,"android:id/button2");
        Continue = new Element(driver,"android:id/button1");

        //Page title
        Title = new Element(driver,"tvLeftSubTitle");
    }

    /**
     * Scroll to the top of the screen.
     */
    public void scrollToTop() {
        swipe.downCustomPercentage(1,
                .2,
                .8,
                .5);
    }

    /**
     * Scroll to the middle of the screen.
     */
    public void scrollDown() {
        swipe.upCustomPercentage(1,
                .75,
                .3,
                .5);
    }

    /**
     * Scrolls down the screen to find the desired element. Stops scrolling once the element is found.
     * Note: the scrolling is only done in 3 blocks: top (default view), middle, and bottom. A more accurate
     * scrolling method might need to be built to work with all screen sizes and scaling.
     * @param element element to look for in this screen.
     */
    public void scrollToElementSection(Element element) {
        final int MAX_SCROLLS = 3;
        for (int i = 0; i < MAX_SCROLLS; i++) {
            if (element.elementExists(1)) {
                break;
            }
            else {
                scrollDown();
                System.out.println("NOTE: above element not found is likely not an error.\n");
            }
        }
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }

    public void clickBackKeyPadBtn()
    {
        tap.tapAtCoordinate(723,2214);
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return (subtitle.getTextValue().equals("About & Support"));
    }
}