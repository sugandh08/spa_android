package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the Warranty Info PDF view screen that is accessed through the About & Support screen.
 */
public class WarrantyInfoScreen extends Screen {
    public Element subtitle;

    public WarrantyInfoScreen(AppiumDriver driver) {
        super(driver);

        subtitle = new Element(driver, "sub_title_txt");
    }

    public boolean isPresent() {
        return subtitle.getTextValue().equalsIgnoreCase("warranty info");
    }
}
