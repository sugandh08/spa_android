package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import library.ElementExists;
import org.openqa.selenium.WebElement;
import views.myhyundai.baseviews.HomeScreen;
import views.myhyundai.fragments.EVRemoteCommands;

import java.util.List;

/**
 * The OS Electric home screen that is displayed when logged in to an OSEV vehicle.
 */
public class OSElectricHomeScreen extends HomeScreen {
    public EVRemoteCommands homeOptions;

    public Element pluginStatusTextView;
    public Element setScheduleButton_os;
    public Element schedule_info;

   /* public Element rangeTextView;
    public Element batteryStatusTextView;
    public Element chargingStatusTextView;
    public Element scheduleTitleTextView;
    public Element scheduleDetailTextView;
    public Element startStopChargeButton;*/

    /**
     * ElectricHomeScreen constructor
     * @param driver Appium driver
     */
    public OSElectricHomeScreen(AppiumDriver driver) {
        super(driver);

        homeOptions = new EVRemoteCommands(driver);

               setScheduleButton_os=new Element(driver,"ae_schedule_detail_txt");

        schedule_info=new Element(driver,"ae_schedule_info_ll");


        pluginStatusTextView = new Element(driver, "ae_plugin_status_txt");
/*
        rangeTextView = new Element(driver,"ae_range_range_txt");
        batteryStatusTextView = new Element(driver,"ae_battery_status_txt");

        chargingStatusTextView = new Element(driver, "ae_charging_status_txt");

        scheduleTitleTextView = new Element(driver, "ae_schedule_title_txt");
        scheduleDetailTextView = new Element(driver, "ae_schedule_detail_txt");
        startStopChargeButton = new Element(driver, "ae_charge_startstop_btn");*/
    }

    /**
     * Test if the screen is present. Uses standard wait time.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the screen is present.
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the screen is present
     */
    public boolean isPresent(long waitTime) {
        // wait for a half second to avoid stale element
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            //
        }
        return pluginStatusTextView.elementExists(waitTime) || setScheduleButton_os.elementExists(waitTime);
    }


}
