package views.myhyundai.screens;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the MyHyundai web site that is accessible through the About & Support screen
 */
public class MyHyundaiComScreen extends Screen {
    public Element backButton;
    public Element subtitleTextView;
    public Element welcomeTextView;
    public Element registerButton;
    public Element loginButton;
    public Element webPreviousButton;
    public Element webNextButton;
    public Element webRefreshButton;
    public Element webStopButton;

    public MyHyundaiComScreen(AppiumDriver driver) {
        super(driver);

        backButton = new Element(driver,"main_title_left_icon");
        subtitleTextView = new Element(driver,"sub_title_txt");
        welcomeTextView = new Element(driver, "", "WELCOME TO MYHYUNDAI");
        registerButton = new Element(driver, "", "REGISTER");
        loginButton = new Element(driver, "", "LOG IN");
        webPreviousButton = new Element(driver,"iv_webview_previous");
        webNextButton = new Element(driver,"iv_webview_next");
        webRefreshButton = new Element(driver,"iv_webview_refresh");
        webStopButton = new Element(driver,"iv_webview_stop");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return subtitleTextView.getTextValue().equals("MyHyundai.com") && welcomeTextView.elementExists(10);
    }
}
