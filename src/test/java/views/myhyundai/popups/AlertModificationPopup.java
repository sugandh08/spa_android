package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import tests.myhyundai.ev.EVLogin;
import views.Screen;

public class AlertModificationPopup extends Screen {
    public Element closeButton;
    public Element alertModificationText;
    public Element okButton;
    /**
     * Standard Screen constructor.
     *
     * @param driver The Appium driver for automation controls. Each screen has custom actions that need access
     *               to this driver.
     */
    public AlertModificationPopup(AppiumDriver driver) {
        super(driver);
        closeButton = new Element(driver, "closeButton");
        alertModificationText = new Element(driver, "dialogContent","Alert modifications will take a few minutes to process before the change will appear on this device.");
        okButton = new Element(driver, "button1","OK");
    }

    @Override
    public boolean isPresent() {
        return alertModificationText.elementExists();
    }
}
