package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when entering an incorrect popup repeatedly on the Car Finder screen. Supports
 * both 5 minute lockouts (3 incorrect attempts) and 10 minute lockouts (6 incorrect attempts / 2
 * sets of 3)
 */
public class PinLockedCarFinderPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    private String pinLockedFiveMinutes = "Your PIN has been locked for 5 minutes. [IDM_PIN_603]";
    private String pinLockedTenMinutes = "Your PIN has been locked for 10 minutes. [IDM_PIN_604]";

    public PinLockedCarFinderPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        String popupMessageText = messageTextView.getTextValue();
        return popupMessageText.equals(pinLockedFiveMinutes) || popupMessageText.equals(pinLockedTenMinutes);
    }
}
