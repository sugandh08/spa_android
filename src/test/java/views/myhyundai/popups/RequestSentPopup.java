package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class RequestSentPopup extends Screen {
    public Element closeButton;
    public Element requestSentText;
    public Element okButton;
    public Element dontShowAgainButton;

    public RequestSentPopup(AppiumDriver driver)
    {
        super(driver);
        closeButton = new Element(driver, "closeButton");
        requestSentText = new Element(driver, "dialogTitle");
        okButton = new Element(driver, "button1");
        dontShowAgainButton = new Element(driver, "button2");
    }

    public boolean isPresent() {
        String popupMessageText = requestSentText.getTextValue();
        return popupMessageText.equals("Request Sent");
    }

}

