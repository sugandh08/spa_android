package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class DeleteConfirmationPopup extends Screen {
    public Element messageTextView;
    public Element okButton;
    public Element cancelButton;

    public DeleteConfirmationPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "message");
        okButton = new Element(driver, "button1","Delete");
        cancelButton = new Element(driver, "button2","Cancel");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.elementExists() && messageTextView.getTextValue().contains("want to delete");  }

        public void clickOnDeleteButton(){
        tap.elementByXpath("//android.widget.Button[@resource-id='android:id/button1']");
        }
}
