package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when you select the Call Roadside option in the side menu.
 */
public class CallRoadsideAssistancePopup extends Screen {
    public Element alertTitleTextView;
    public Element messageTextView;
    public Element callButton;
    public Element cancelButton;

    /**
     * CallRoadsideAssistancePopup constructor
     * @param driver Appium driver
     */
    public CallRoadsideAssistancePopup(AppiumDriver driver) {
        super(driver);

        alertTitleTextView = new Element(driver, "com.stationdm.bluelink:id/alertTitle");
        messageTextView = new Element(driver,"android:id/message");
        callButton = new Element(driver,"android:id/button1");
        cancelButton = new Element(driver,"android:id/button2");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Would you like to exit the app and call Roadside Assistance?");
    }
}
