package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is a popup that shows in a Gen1 vehicle when the selected feature is not supported.
 */
public class FeatureNotSupportedPopup extends Screen {
    public Element messageTextView;
    public Element closeButton;
    public Element checkAvailabilityButton;

    public FeatureNotSupportedPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        closeButton = new Element(driver, "android:id/button2");
        checkAvailabilityButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        System.out.println(messageTextView.getTextValue());
        return messageTextView.getTextValue().equals("This feature is not included in your package. Would you like more information?");
    }

    public boolean isPresentRemoteStartNotSupported() {
        System.out.println(messageTextView.getTextValue());
        return messageTextView.getTextValue().equals("We're sorry, your vehicle does not support this feature. [GEN]");
    }
}
