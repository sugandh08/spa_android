package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup is shown when a feature is selected in demo mode that is not available in the demo.
 */
public class FeatureNotAvailableInDemoPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public FeatureNotAvailableInDemoPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().equalsIgnoreCase("this feature is not available in demo mode.");
    }
}
