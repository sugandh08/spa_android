package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when the PIN reset process was completed successfully.
 */
public class ChangePinSuccessfulPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public ChangePinSuccessfulPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Change PIN Successful.");
    }
}
