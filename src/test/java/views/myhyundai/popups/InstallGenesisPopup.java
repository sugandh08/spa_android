package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when you log in to a Genesis vehicle and don't have the Genesis app installed.
 */
public class InstallGenesisPopup extends Screen {
    public Element messageTextView;
    public Element installButton;
    public Element closeButton;

    public InstallGenesisPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        installButton = new Element(driver, "android:id/button1");
        closeButton = new Element(driver, "android:id/button2");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Your vehicle is not supported by the MyHyundai App. Would you like to install the Genesis app for your vehicle?");
    }
}
