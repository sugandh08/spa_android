package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that is used for the result of all remote_genesis_old commands. Can sometimes take up to 3
 * minutes to show up (wicked fast!)
 */
public class RemoteCommandResultPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public RemoteCommandResultPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        String messageText = messageTextView.getTextValue(waitTime);
        return messageText.contains("Your request for ") || messageText.contains("processed");
    }
}
