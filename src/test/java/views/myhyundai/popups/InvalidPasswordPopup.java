package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when entering a valid email but an invalid password in the login page.
 */
public class InvalidPasswordPopup extends Screen {
    public Element messageTextView;
    public Element okButton;
    public Element resetPasswordButton;

    /**
     * InvalidPasswordPopup constructor
     * @param driver Appium driver
     */
    public InvalidPasswordPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        okButton = new Element(driver,"android:id/button1");
        resetPasswordButton = new Element(driver, "android:id/button2");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Incorrect username or password [IDM_401_1]");
    }
}
