package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when setting a parking meter and when the reminder time is greater than the
 * parking meter time.
 */
public class ParkingMeterAlertTimeGreaterThanMeterTimePopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public ParkingMeterAlertTimeGreaterThanMeterTimePopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        okButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Alert time should be less than meter time.");
    }
}