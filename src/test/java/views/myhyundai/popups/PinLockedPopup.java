package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when entering an incorrect popup repeatedly on remote_genesis_old commands or other standard
 * PIN entry locations. Supports both 5 minute lockouts (3 incorrect attempts) and 10 minute
 * lockouts (6 incorrect attempts / 2 sets of 3)
 */
public class PinLockedPopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element changePinButton;
    public Element okButton;

    private String pinLocked = "Your PIN has been locked for";
    //private String pinLockedFiveMinutes = "Your PIN has been locked for 5 minutes. [COV_PIN_603]";
    //private String pinLockedTenMinutes = "Your PIN has been locked for 10 minutes. [COV_PIN_604]";

    public PinLockedPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        cancelButton = new Element(driver, "android:id/button2");
        changePinButton = new Element(driver, "android:id/button1");
        okButton=new Element(driver,"android:id/button1");
    }

    public boolean isPresent() {
        String popupMessageText = messageTextView.getTextValue();
        return popupMessageText.contains(pinLocked);
    }
}
