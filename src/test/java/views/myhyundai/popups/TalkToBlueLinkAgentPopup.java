package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when tapping the Talk to Blue Link Agent option in the About & Support screen.
 */
public class TalkToBlueLinkAgentPopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element callButton;

    public TalkToBlueLinkAgentPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        cancelButton = new Element(driver,"android:id/button2");
        callButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Would you like to contact Hyundai Blue Link?");
    }
}