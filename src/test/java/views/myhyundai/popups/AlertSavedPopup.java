package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that should show up when pressing the Save button in the alert settings screen.
 */
public class AlertSavedPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public AlertSavedPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Successfully Saved");
    }
}
