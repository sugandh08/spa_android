package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows after selecting a vehicle from the vehicle select screen after logging in to the app.
 */
public class TermsAndConditionsPopup extends Screen {
    public Element messageTextView;
    public Element viewDetailsButton;
    public Element acceptButton;
    public Element dismissButton;
    public Element usingtheapp;
    /**
     * TermsAndConditionsPopup constructor
     * @param driver Appium driver
     */
    public TermsAndConditionsPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        viewDetailsButton = new Element(driver,"android:id/button2");
        acceptButton = new Element(driver,"android:id/button1");
        dismissButton = new Element(driver, "","Dismiss");
        usingtheapp = new Element(driver,"permission_allow_foreground_only_button");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */

    public boolean isPresent() {
        return messageTextView.getTextValue().equalsIgnoreCase("Do you agree to the Blue Link Terms & Conditions?");
    }

    public void clickOnBackButton(){
        tap.elementByXpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    }
}
