package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when going into the Parking Meter screen while a timer is already set, then
 * pressing the reset button.
 */
public class ParkingMeterTimerResetPopup extends Screen {
    public Element messageTextView;
    public Element noButton;
    public Element yesButton;

    public ParkingMeterTimerResetPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        noButton = new Element(driver,"android:id/button2");
        yesButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Are you sure you want to cancel this alarm?");
    }
}