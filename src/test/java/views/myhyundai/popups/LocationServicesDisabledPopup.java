package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when you attempt to use a map feature but the device has Location services disabled.
 */
public class LocationServicesDisabledPopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element settingsButton;

    /**
     * LocationServicesDisabledPopup constructor
     * @param driver Appium driver
     */
    public LocationServicesDisabledPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        cancelButton = new Element(driver,"android:id/button2");
        settingsButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("MyHyundai App needs to access your current location. Please enable location services in your device settings to use map features.");
    }
}
