package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup is used in Genesis tests when selecting a MyHyundai vehicle and switching over
 * to the MyHyundai app. Since Appium can't handle the permission messages for apps that aren't the
 * 'default' app, we need to accept them in the test script.
 */
public class PermissionsPopup extends Screen {
    public Element allowButton;
    public Element denyButton;

    /**
     * LoginScreen constructor
     * @param driver Appium driver
     */
    public PermissionsPopup(AppiumDriver driver) {
        super(driver);

        allowButton = new Element(driver,"permission_allow_button");
        denyButton = new Element(driver,"permission_deny_button");
    }

    /**
     * Test if the screen is present.
     * @return Whether or not the screen is present
     */
    public boolean isPresent() {
        return allowButton.elementExists();
    }
}
