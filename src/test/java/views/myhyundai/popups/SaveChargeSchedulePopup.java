package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when pressing the Save button in the charge schedule settings screen.
 */
public class SaveChargeSchedulePopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element saveButton;

    public SaveChargeSchedulePopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        cancelButton = new Element(driver, "android:id/button2");
        saveButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).equals("Save charge schedule?");
    }
}
