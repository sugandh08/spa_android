package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class DevicePermissionPopup extends Screen {

    public Element allowButton;
    public Element denyButton;
    public Element permissionMessage;

    public DevicePermissionPopup(AppiumDriver driver) {
        super(driver);

        allowButton = new Element(driver,"permission_allow_button");
        denyButton = new Element(driver,"permission_deny_button");
        permissionMessage = new Element(driver,"permission_message");

        }

    @Override
    public boolean isPresent() {
        return allowButton.elementExists();
    }

    public void clickOnDeny(){
        tap.elementByXpath("//*[@text='DENY']");
    }

    public void clickOnAllow(){
        tap.elementByXpath("//*[@text='ALLOW']");
    }

    public void clickCancelLocationService()
    {
        tap.elementByXpath("//*[@text='Cancel']");
    }


}
