package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when user selects that they forgot their PIN.
 */
public class ForgotPinPopup extends Screen {
    public Element titleTextView;
    public Element messageTextView;
    public Element cancelButton;
    public Element changePinButton;

    public ForgotPinPopup(AppiumDriver driver) {
        super(driver);

        titleTextView = new Element(driver, "alertTitle");
        messageTextView = new Element(driver, "android:id/message");
        cancelButton = new Element(driver, "android:id/button2");
        changePinButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Would you like to set a new PIN using your MyHyundai password?");
    }
}
