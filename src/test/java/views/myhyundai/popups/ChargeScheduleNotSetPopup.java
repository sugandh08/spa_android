package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is an annoying popup that shows up right when a PHEV or AEEV home screen is loaded, if a
 * charge schedule is not set or if the charge schedule setting was not loaded into the app properly.
 */
public class ChargeScheduleNotSetPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public ChargeScheduleNotSetPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).equals("You have not set a charge schedule in the MyHyundai app. If you have set one in the vehicle, please click refresh.");
    }
}
