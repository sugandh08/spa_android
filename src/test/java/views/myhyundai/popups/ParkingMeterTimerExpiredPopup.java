package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when a parking meter expires.
 */
public class ParkingMeterTimerExpiredPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public ParkingMeterTimerExpiredPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        okButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists. Uses standard wait time
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return isPresent(5);
    }

    /**
     * Test if the popup exists
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Whether or not the popup is present
     */
    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).equals("Your parking meter has expired.");
    }
}
