package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import tests.myhyundai.ev.EVLogin;
import views.Screen;

public class PriorFeatureRequestPopup extends Screen {
    public Element closeButton;
    public Element messageText;
    public Element okButton;
    /**
     * Standard Screen constructor.
     *
     * @param driver The Appium driver for automation controls. Each screen has custom actions that need access
     *               to this driver.
     */
    public PriorFeatureRequestPopup(AppiumDriver driver) {
        super(driver);
        closeButton = new Element(driver, "closeButton","");
        messageText = new Element(driver, "dialogContent","A prior feature request is still processing. Please wait and try again later.");
        okButton = new Element(driver, "button1","OK");
    }

    public boolean isPresent() {
        return messageText.getTextValue().contains("A prior feature request is still processing");
    }
}
