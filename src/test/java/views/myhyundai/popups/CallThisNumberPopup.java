package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when you tap a landline, mobile, or emergency landline/mobile
 * phone number in the Profile screen.
 */
public class CallThisNumberPopup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element callButton;

    /**
     * CallThisNumberPopup constructor
     * @param driver Appium driver
     */
    public CallThisNumberPopup(AppiumDriver driver) {
        super(driver);

        // Message contains "Do you want to call this phone number xxx-xxx-xxxx?"
        messageTextView = new Element(driver,"android:id/message");
        cancelButton = new Element(driver,"android:id/button2");
        callButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().contains("Do you want to call this phone number");
    }
}
