package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows when you attempt to send a Remote Stop when the vehicle is not in Remote Start mode.
 */
public class RemoteStopOnlyDuringRemoteStartPopup extends Screen {
    public Element messageTextView;
    public Element okButton;

    public RemoteStopOnlyDuringRemoteStartPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Remote Stop for your vehicle cannot be processed. Remote Stop can only be used during Remote Start session. [HT_539]");
    }
}
