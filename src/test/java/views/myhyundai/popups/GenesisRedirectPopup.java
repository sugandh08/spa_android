package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * Popup that shows when logging in to an account with only Genesis vehicles, or when selecting a
 * Genesis vehicle in the vehicle selection page.
 */
public class GenesisRedirectPopup extends Screen {
    public Element messageTextView;
    public Element closeButton;
    public Element openButton;

    /**
     * GenesisRedirectPopup
     * @param driver Appium driver
     */
    public GenesisRedirectPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver,"android:id/message");
        closeButton = new Element(driver,"android:id/button2");
        openButton = new Element(driver,"android:id/button1");
    }

    /**
     * Test if the popup exists.
     * @return Whether or not the popup is present
     */
    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Would you like to open the Genesis app for your vehicle?");
    }
}
