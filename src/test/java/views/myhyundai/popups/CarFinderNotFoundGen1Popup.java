package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This is the popup that shows when Car Finder fails to find a Gen 1 vehicle.
 */
public class CarFinderNotFoundGen1Popup extends Screen {
    public Element messageTextView;
    public Element cancelButton;
    public Element continueButton;

    public CarFinderNotFoundGen1Popup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        cancelButton = new Element(driver, "android:id/button2");
        continueButton = new Element(driver,  "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).equals("The vehicle could not be located. Would you like to manually save your vehicle location?");
    }
}
