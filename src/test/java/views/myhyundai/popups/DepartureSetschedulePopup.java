package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class DepartureSetschedulePopup extends Screen {
    public Element messageTextView;
    public Element okButton;
    public Element saveButton;

    public DepartureSetschedulePopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        okButton = new Element(driver, "android:id/button1");
        saveButton = new Element(driver,"btnSave");
    }


    public boolean isPresent() {
        return messageTextView.getTextValue().equals("Save charge schedule?");
    }

}
