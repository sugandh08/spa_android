package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

/**
 * This popup shows after a charge schedule change is confirmed by the server/vehicle.
 */
public class SetChargeScheduleSuccessPopup extends Screen {
    public Element messageTextView;
    public Element saveButton;

    public SetChargeScheduleSuccessPopup(AppiumDriver driver) {
        super(driver);

        messageTextView = new Element(driver, "android:id/message");
        saveButton = new Element(driver, "android:id/button1");
    }

    public boolean isPresent() {
        return isPresent(5);
    }

    public boolean isPresent(long waitTime) {
        return messageTextView.getTextValue(waitTime).contains("SET ELECTRIC CHARGE SCHEDULE has been processed successfully");
    }
}
