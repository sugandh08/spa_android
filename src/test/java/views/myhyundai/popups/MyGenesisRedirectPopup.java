package views.myhyundai.popups;

import io.appium.java_client.AppiumDriver;
import library.Element;
import views.Screen;

public class MyGenesisRedirectPopup extends Screen {

    public MyGenesisRedirectPopup(AppiumDriver driver) {
        super(driver);

    }

    @Override
    public boolean isPresent() {
        return false;
    }

    public void clickOnOpenButton()
    {
        tap.elementByXpath("//*[@text='Open']");
    }

    public boolean installIsDisplay()
    {
        return elementExists.byXpath("//*[@text='Install']");
    }

    public void clickOnInstall()
    {
        tap.elementByXpath("//*[@text='Install']");
    }

    public void clickOnCloseButton()
    {
        tap.elementByXpath("//*[@text='Close']");
    }


}
