package views;

import config.ConfigurationHelper;
import config.GenesisProfiles;
import config.MyHyundaiProfiles;
import io.appium.java_client.AppiumDriver;

/**
 * Allows the user to access any information, screens, elements, etc.
 */
public class AppController {
    public AppFunctions appFunctions;
    public GenesisProfiles genesisProfiles;
    public MyHyundaiProfiles myHyundaiProfiles;

    private ConfigurationHelper configurationHelper;

    // File names for uninstalling and installing the other app to load in a fresh state when switching to it.
    private String myHyundaiAndroidFileName;
    private String myHyundaiiOSFileName;
    private String genesisAndroidFileName;
    private String genesisiOSFileName;

    /**
     * AppController constructor
     * @param driver Appium driver
     */
    public AppController(AppiumDriver driver) {
        configurationHelper = new ConfigurationHelper();

        appFunctions = new AppFunctions(driver);
        genesisProfiles = configurationHelper.getGenesisProfiles();
        myHyundaiProfiles = configurationHelper.getMyHyundaiProfiles();

        myHyundaiAndroidFileName = configurationHelper.getAppFileName(ConfigurationHelper.AppNames.MYHYUNDAI_ANDROID);
        myHyundaiiOSFileName = configurationHelper.getAppFileName(ConfigurationHelper.AppNames.MYHYUNDAI_IOS);
        genesisAndroidFileName = configurationHelper.getAppFileName(ConfigurationHelper.AppNames.GENESIS_ANDROID);
        genesisiOSFileName = configurationHelper.getAppFileName(ConfigurationHelper.AppNames.GENESIS_IOS);
    }

    public String getTestVersion() {
        return configurationHelper.getTestVersion();
    }

    public String getMyHyundaiAndroidFileName() {
        return myHyundaiAndroidFileName;
    }

    public String getMyHyundaiiOSFileName() {
        return myHyundaiiOSFileName;
    }

    public String getGenesisAndroidFileName() {
        return genesisAndroidFileName;
    }

    public String getGenesisiOSFileName() {
        return genesisiOSFileName;
    }
}