package views;

import io.appium.java_client.AppiumDriver;
import library.TouchActions;

/**
 * A GUI screen on an app. Inherited by every screen class.
 */
public abstract class Screen extends TouchActions {
    /**
     * Standard Screen constructor.
     * @param driver The Appium driver for automation controls. Each screen has custom actions that need access
     *               to this driver.
     */
    public Screen(AppiumDriver driver) {
        super(driver);
    }

    /**
     * Checks if this screen is currently showing on the app.
     * @return True if it is showing, false if it isn't. Default return is false.
     */
    public abstract boolean isPresent();
}