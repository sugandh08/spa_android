package library;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.concurrent.TimeUnit;

/**
 * Contains different ways of determining if an element is on screen.
 */
public class ElementExists {
    private AppiumDriver driver;

    /**
     * elementExists constructor
     * @param driver Appium driver
     */
    public ElementExists(AppiumDriver driver) {
        this.driver = driver;
    }

    private boolean elementExistsAction(By by, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            return convertToBoolean(driver.findElement(by).isDisplayed());
        }
        catch(NoSuchElementException e){
            String exceptionMessage = e.getMessage();
//            String printout = exceptionMessage.substring(0, exceptionMessage.indexOf("(WARNING:")) +
//                    "\n" + exceptionMessage.substring(exceptionMessage.indexOf("***"));
            System.out.println(exceptionMessage);
            return false;
        }
    }

    /**
     * Find element by id.
     * @param elementId Element id
     * @return Whether or not the element is on screen
     */
    public boolean byId(String elementId, long waitTime) {
        return elementExistsAction(By.id(elementId), waitTime);
    }

    /**
     * Uses the appiumdriver to attempt to find an element by xpath. Default wait time used
     * @param xpath  Xpath that the driver uses to search
     * @return True if the element is found, false otherwise
     */
    public boolean byXpath(String xpath) {
        return byXpath(xpath, 5);
    }

    /**
     * Uses the appiumdriver to attempt to find an element by xpath with a custom wait time
     * @param xpath  Xpath that the driver uses to search
     * @param waitTime Amount of time to implicitly wait for the search
     * @return True if the element is found, false otherwise
     */
    public boolean byXpath(String xpath, long waitTime) {
        return elementExistsAction(By.xpath(xpath), waitTime);
    }

    private boolean convertToBoolean(Object returnValue) {
        if(returnValue.toString().equals("true")) {
            return true;
        }
        else {
            return false;
        }
    }
}
