package library;

import io.appium.java_client.AppiumDriver;

/**
 * Defines the accessible properties and actions of an element.
 */
public class Element {
    private String id = "";
    private String text = "";

    private EnterText enterText;

    private enum MethodsToUse {Id, Text}
    private MethodsToUse methodToUse;

    public Tap tap;
    public Attribute attribute;
    public ElementExists elementExists;


    /**
     * Element constructor for just an ID. Pipes everything into Element(driver, id, text) constructor.
     * @param driver Appium driver
     * @param id Element id
     */
    public Element(AppiumDriver driver, String id) {
        this(driver, id, "");
    }

    /**
     * Element constructor
     * @param driver Appium driver
     * @param id Element id
     * @param text Element text
     */
    public Element(AppiumDriver driver, String id, String text) {
        this.id = id;
        this.text = text;

        enterText = new EnterText(driver);
        tap = new Tap(driver);
        attribute = new Attribute(driver);
        elementExists = new ElementExists(driver);

        if(!this.id.isEmpty()) {
            methodToUse = MethodsToUse.Id;
        }
        else if(!this.text.isEmpty()) {
            methodToUse = MethodsToUse.Text;

        }
    }

    private String xpathCallSetup() {
        switch(methodToUse) {
            case Text:
                return "//*[@text='" + text + "']";

            default:
                return "";
        }
    }

    /**
     * Tap the element. Uses standard wait time
     */
    public void tap() {
        tap(5);
    }

    /**
     * Tap the element
     * @param waitTime Time (in seconds) to implicitly wait
     */
    public void tap(long waitTime) {
        runTap(waitTime);
    }

    /**
     * logic behind tap() and waitThenTap() to allow per-instance overrides of tap() that call Thread.sleep.
     */
    private void runTap() {
        runTap(5);
    }

    /**
     * logic behind tap() and waitThenTap() to allow per-instance overrides of tap() that call Thread.sleep.
     * @param waitTime Time (in seconds) to implicitly wait
     */
    private void runTap(long waitTime) {
        if (methodToUse.equals(MethodsToUse.Id)) {
            tap.elementById(id, waitTime);
        }
        else {
            tap.elementByXpath(xpathCallSetup(), waitTime);
        }
    }

    /**
     * Sleeps for a second, then runs tap(). This is meant to be called by certain elements that will override their
     * inherited tap() method, if they always need a sleep before calling it to work properly in automation.
     */
    protected void waitThenTap() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // do nothing, test will fail
        }
        runTap();
    }

    /**
     * Runs tap(), then sleeps for a second. This is meant to be called by certain elements that will override their
     * inherited tap() method, if they always need a sleep after calling it to work properly in automation.
     */
    protected void tapThenWait() {
        runTap();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // do nothing, test will fail
        }
    }

    /**
     * Taps at a specific x,y screen location
     * @param x X coordinate to tap at, increases from left to right on the screen.
     * @param y Y coordinate to tap at, increases from top to bottom on the screen.
     */
    public void tapAtCoordinate(int x, int y) {
        tap.tapAtCoordinate(x, y);
    }

    /**
     * Enter text for the element.
     * @param text text to add to the element
     */
    public void enterText(String text) {
        enterText.byId(id, text);
    }

    /**
     * Appium driver checks the current screen to see if the element can be found. Uses default wait time.
     * @return True if element is found, false otherwise
     */
    public boolean elementExists() {
        return elementExists(5);
    }

    /**
     * Appium driver checks the current screen to see if the element can be found
     * @param waitTime amount of time to implicitly wait for the element
     * @return True if element is found, false otherwise
     */
    public boolean elementExists(long waitTime) {
        if(methodToUse.equals(MethodsToUse.Id)) {
            return elementExists.byId(id, waitTime);
        }
        else {
            return elementExists.byXpath(xpathCallSetup(), waitTime);
        }
    }

    /**
     * Retrieve the value of the 'text' attribute for the element. Uses standard wait time.
     * @return Value of the 'text' attribute
     */
    public String getTextValue() {
        return getTextValue(5);
    }

    /**
     * Retrieve the value of the 'text' attribute for the element.
     * @param waitTime Time (in seconds) to implicitly wait.
     * @return Value of the 'text' attribute
     */
    public String getTextValue(long waitTime) {
        if(methodToUse.equals(MethodsToUse.Id)) {
            return attribute.getTextValueById(id, waitTime);
        }
        else {
            // Skip searching for text
            if(!methodToUse.equals(MethodsToUse.Text)) {
                return attribute.getTextValueByXpath(xpathCallSetup(), waitTime);
            }
            else {
                //System.out.println("You can't retrieve the text value of an element that does not have a searchable id value.");
                return "";
            }
        }
    }

    /**
     * Retrieve the value of the 'selected' attribute for the element.
     * @return Value of 'selected' attribute
     */
    public boolean getSelectedValue() {
        if(methodToUse.equals(MethodsToUse.Id)) {
            return attribute.getSelectedValueById(id);
        }
        else {
            return attribute.getSelectedValueByXpath(xpathCallSetup());
        }
    }

    /**
     * Retrieve the value of the 'clickable' attribute for the element
     * @return Value of the 'clickable' attribute
     */
    public boolean getClickableValue() {
        if(methodToUse.equals(MethodsToUse.Id)) {
            return attribute.getClickableValueById(id);
        }
        else {
            return attribute.getClickableValueByXpath(xpathCallSetup());
        }
    }

    /**
     * Retrieve the value of the 'checked' attribute for the element.
     * @return Value of 'checked' attribute
     */
    public boolean getCheckedValue() {
        if(methodToUse.equals(MethodsToUse.Id)) {
            return attribute.getCheckedValueById(id);
        }
        else {
            return attribute.getCheckedValueByXpath(xpathCallSetup());
        }
    }
}