package library;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Rectangle;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Contains methods for simulating swiping gestures.
 */
public class Swipe {

    private AppiumDriver driver;

    // Holds the dimensions of the screen
    private Dimension size;

    private int yAxis;
    private int xAxis;

    private int startPoint;
    private int endPoint;
    private int anchorPoint;

    private double startPercentage = 0.5;
    private double percentageModifier = 0.2;

    private double anchorPercentageShort = 0.2;
    private double anchorPercentageLong = 0.5;

    /**
     * Swipe constructor
     * @param driver Appium driver
     */
    public Swipe(AppiumDriver driver) {
        this.driver = driver;

        size = driver.manage().window().getSize();
        yAxis = size.height;
        xAxis = size.width;
    }

    /**
     * Simulates a user swiping up.
     * @param numberOfSwipes How many times the user swipes
     */
    public void up(int numberOfSwipes) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * startPercentage);
        endPoint = (int) (yAxis * (startPercentage - percentageModifier));
        anchorPoint = (int) (xAxis * anchorPercentageShort);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping up.
     * @param numberOfSwipes How many times the user swipes
     * @param customStartPercentage Percentage representing where the swipe action starts on the vertical axis
     * @param lengthOfSwipePercentage Percentage subtracted from customStartPercentage to represent
     *                                    where the swipe action ends on the screen
     * @param customAnchorPercentage Percentage representing a axis value that doesn't change
     */
    public void upCustomPercentage(int numberOfSwipes, double customStartPercentage, double lengthOfSwipePercentage,
                   double customAnchorPercentage) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * customStartPercentage);
        endPoint = (int) (yAxis * (customStartPercentage - lengthOfSwipePercentage));
        anchorPoint = (int) (xAxis * customAnchorPercentage);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping up.
     * @param numberOfSwipes How many times the user swipes
     * @param startPoint Start point of swiping on screen
     * @param endPoint End point of swiping on screen
     * @param anchorPoint  Axis value that doesn't change
     */
    public void upCustomElement(int numberOfSwipes, int startPoint, int endPoint, int anchorPoint) {
        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping down.
     * @param numberOfSwipes How many times the user swipes
     */
    public void down(int numberOfSwipes) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * startPercentage);
        endPoint = (int) (yAxis * (startPercentage + percentageModifier));
        anchorPoint = (int) (xAxis * anchorPercentageShort);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping down.
     * @param numberOfSwipes How many times the user swipes
     * @param customStartPercentage Percentage representing where the swipe action starts on the vertical axis
     * @param lengthOfSwipePercentage Percentage added to customStartPercentage to represent
     *                                    where the swipe action ends on the screen
     * @param customAnchorPercentage Percentage representing a axis value that doesn't change
     */
    public void downCustomPercentage(int numberOfSwipes, double customStartPercentage, double lengthOfSwipePercentage,
                     double customAnchorPercentage) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * customStartPercentage);
        endPoint = (int) (yAxis * (customStartPercentage + lengthOfSwipePercentage));
        anchorPoint = (int) (xAxis * customAnchorPercentage);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping down.
     * @param numberOfSwipes How many times the user swipes
     * @param startPoint Start point of swiping on screen
     * @param endPoint End point of swiping on screen
     * @param anchorPoint  Axis value that doesn't change
     */
    public void downCustomElement(int numberOfSwipes, int startPoint, int endPoint, int anchorPoint) {
        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping right.
     * @param numberOfSwipes How many times the user swipes
     */
    public void right(int numberOfSwipes) {
        // Move along x axis
        // Keep y value same
        startPoint = (int) (xAxis * startPercentage);
        endPoint = (int) (xAxis * (startPercentage + percentageModifier));
        anchorPoint = (int) (yAxis * anchorPercentageShort);

        swipeAction(numberOfSwipes, startPoint, endPoint, anchorPoint, anchorPoint);
    }

    /**
     * Simulates a user swiping left.
     * @param numberOfSwipes How many times the user swipes
     */
    public void left(int numberOfSwipes) {
        // Move along x axis
        // Keep y value same
        startPoint = (int) (xAxis * startPercentage);
        endPoint = (int) (xAxis * (startPercentage - percentageModifier));
        anchorPoint = (int) (yAxis * anchorPercentageShort);

        swipeAction(numberOfSwipes, startPoint, endPoint, anchorPoint, anchorPoint);
    }

    /**
     * Simulates a user swiping from the left edge.
     * @param numberOfSwipes How many times the user swipes
     */
    public void fromLeftEdge(int numberOfSwipes) {
        // Move along x axis
        // Keep y value same
        startPoint = (int) (xAxis * 0.1);
        endPoint = (int) (xAxis * (startPercentage + percentageModifier));
        anchorPoint = (int) (yAxis * startPercentage);

        swipeAction(numberOfSwipes, startPoint, endPoint, anchorPoint, anchorPoint);
    }

    /**
     * Simulates a user swiping from the right edge.
     * @param numberOfSwipes How many times the user swipes
     */
    public void fromRightEdge(int numberOfSwipes) {
        // Move along x axis
        // Keep y value same
        startPoint = (int) (xAxis * (startPercentage + percentageModifier));
        endPoint = (int) (xAxis * 0.2);
        anchorPoint = (int) (yAxis * startPercentage);

        swipeAction(numberOfSwipes, startPoint, endPoint, anchorPoint, anchorPoint);
    }

    /**
     * Simulates a user swiping from the top edge.
     * @param numberOfSwipes How many times the user swipes
     */
    public void fromTopEdge(int numberOfSwipes) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * 0.1);
        endPoint = (int) (yAxis * (startPercentage + percentageModifier));
        anchorPoint = (int) (xAxis * startPercentage);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates a user swiping from the bottom edge.
     * @param numberOfSwipes How many times the user swipes
     */
    public void fromBottomEdge(int numberOfSwipes) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * (startPercentage + percentageModifier));
        endPoint = (int) (yAxis * 0.4);
        anchorPoint = (int) (xAxis * startPercentage);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }

    /**
     * Simulates the swipe action and repeats if necessary. Uses default wait time.
     * @param numberOfSwipes How many times the user swipes
     * @param xStart Start value of x coordinate
     * @param xEnd End value of x coordinate
     * @param yStart Start value of y coordinate
     * @param yEnd End value of y coordinate
     */
    private void swipeAction(int numberOfSwipes, int xStart, int xEnd, int yStart, int yEnd) {
        swipeAction(numberOfSwipes, xStart, xEnd, yStart, yEnd, 5);
    }

    /**
     * Simulates the swipe action and repeats if necessary
     * @param numberOfSwipes How many times the user swipes
     * @param xStart Start value of x coordinate
     * @param xEnd End value of x coordinate
     * @param yStart Start value of y coordinate
     * @param yEnd End value of y coordinate
     * @param waitTime Time (in seconds) to implicitly wait
     */
    private void swipeAction(int numberOfSwipes, int xStart, int xEnd, int yStart, int yEnd, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        for(int i=0;i<numberOfSwipes;i++) {
            new TouchAction(driver)
                    .press(new PointOption().withCoordinates(xStart, yStart))
                    .waitAction()
                    .moveTo(new PointOption().withCoordinates(xEnd, yEnd))
                    .release()
                    .perform();
        }
    }


    /**
     * Swipes down a from middle of screen for the chosen number of s entries
     * @param scrollWheelXpath The xpath of the scroll  element
     * @param entriesToScroll Number of entries to scroll on the scroll element
     */
    public void swipeScrollFromMiddleDown(String scrollWheelXpath, int entriesToScroll) {
        Rectangle dimensions = driver.findElementByXPath(scrollWheelXpath).getRect();

        int xCoordinate = dimensions.x + (int)(dimensions.width * 0.5);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.9);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.30);

        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
    }
}