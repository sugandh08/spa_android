package library;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

/**
 * Contains different ways of finding an element to enter text.
 */
public class EnterText {

    private AppiumDriver driver;

    /**
     * EnterText constructor
     * @param driver Appium driver
     */
    public EnterText(AppiumDriver driver) {
        this.driver = driver;
    }

    /**
     * Enters a text value into the selected element. Uses default wait time.
     * @param by The search method to find the element with
     * @param text The text value to enter into the element
     */
    private void enterTextAction(By by, String text) {
        enterTextAction(by, text, 5);
    }

    /**
     * Enters a text value into the selected element
     * @param by The search method to find the element with
     * @param text The text value to enter into the element
     * @param waitTime Time (in seconds) to implicitly wait
     */
    private void enterTextAction(By by, String text, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            driver.findElement(by).sendKeys(text);
        }
        catch(Exception e) {

        }
    }

    /**
     * Find element by id and send text
     * @param elementId Element id
     * @param text text to send
     */
    public void byId(String elementId, String text) {
        enterTextAction(By.id(elementId), text);
    }

    /**
     * Find element by xpath and send text
     * @param elementXpath Element xpath
     * @param text text to send
     */
    public void byXpath(String elementXpath, String text) {
        enterTextAction(By.xpath(elementXpath), text);
    }

    }
