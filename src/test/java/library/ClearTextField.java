package library;

import io.appium.java_client.AppiumDriver;

public class ClearTextField {

    private AppiumDriver driver;

    public ClearTextField(AppiumDriver driver) {
        this.driver = driver;
    }

    /**
     * Find element by xpath and clear text field
     * @param elementXpath Element xpath
     */
    public void clearTextByXpath(String elementXpath)
    {
        driver.findElementByXPath(elementXpath).clear();
    }
    public void clearTextById(String elementID)
    {
       driver.findElementById(elementID).clear();
    }
}
