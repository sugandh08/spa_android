package library;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.concurrent.TimeUnit;

/**
 * Contains different ways of getting attribute information.
 */
public class Attribute {
    private AppiumDriver driver;

    private String platformName;

    private final String TEXT = "text";
    private final String CHECKED = "checked";
    private final String SELECTED = "selected";
    private final String CLICKABLE = "clickable";

    /**
     * Attribute constructor
     * @param driver Appium driver
     */
    public Attribute(AppiumDriver driver) {
        this.driver = driver;
        this.platformName = driver.getCapabilities().getCapability("platformName").toString().toLowerCase();
    }

    /**
     * Gets an attribute from the selected element using the appium driver. Uses standard wait time.
     * @param by How to find the element
     * @param elementAttribute The attribute to retrieve
     * @return The value of the requested attribute
     */
    private String attributeAction(By by, String elementAttribute) {
        return attributeAction(by, elementAttribute, 5);
    }

    /**
     * Gets an attribute from the selected element using the appium driver
     * @param by How to find the element
     * @param elementAttribute The attribute to retrieve
     * @param waitTime Time (in seconds) to implicitly wait. -1 to disable the wait
     * @return The value of the requested attribute
     */
    private String attributeAction(By by, String elementAttribute, long waitTime) {
        // use waitTime -1 if no wait is necessary
        if (waitTime >= 0) {
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
        }

        try {
            String attribute = driver.findElement(by).getAttribute(elementAttribute);
            if (attribute == null) {
                attribute = "";
            }
            return attribute;
        }
        catch (NoSuchElementException e) {
            String exceptionMessage = e.getMessage();
//            String printout = exceptionMessage.substring(0, exceptionMessage.indexOf("(WARNING:")) +
//                    "\n" + exceptionMessage.substring(exceptionMessage.indexOf("***"));
            System.out.println(exceptionMessage);
            return "";
        }
    }

    /**
     * Find element by id and return its value. Uses standard wait time.
     * @param elementId Element id
     * @param elementAttribute Element attribute
     * @return Value of the element attribute
     */
    public String byId(String elementId, String elementAttribute) {
        return byId(elementId, elementAttribute, 5);
    }

    /**
     * Find element by id and return its value.
     * @param elementId Element id
     * @param elementAttribute Element attribute
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Value of the element attribute
     */
    public String byId(String elementId, String elementAttribute, long waitTime) {
        return attributeAction(By.id(elementId), elementAttribute, waitTime);
    }

    /**
     * Find element by xpath and return its value. Uses standard wait time
     * @param elementXpath Element xpath
     * @param elementAttribute Element attribute
     * @return Value of the element attribute
     */
    public String byXpath(String elementXpath, String elementAttribute) {
        return byXpath(elementXpath, elementAttribute, 5);
    }

    /**
     * Find element by xpath and return its value.
     * @param elementXpath Element xpath
     * @param elementAttribute Element attribute
     * @param waitTime Time (in seconds) to implicitly wait. Use -1 to disable the wait
     * @return Value of the element attribute
     */
    public String byXpath(String elementXpath, String elementAttribute, long waitTime) {
        return attributeAction(By.xpath(elementXpath), elementAttribute, waitTime);
    }

    /**
     * Retrieve the value of the 'text' attribute by element id. Uses standard wait time.
     * @param elementId Element id
     * @return Value of the 'text' attribute
     */
    public String getTextValueById(String elementId) {
        return getTextValueById(elementId, 5);
    }

    /**
     * Retrieve the value of the 'text' attribute by element id.
     * @param elementId Element id
     * @param waitTime Time (in seconds) to implicitly wait.
     * @return Value of the 'text' attribute
     */
    public String getTextValueById(String elementId, long waitTime) {
        String androidPrefix = "android:id/";
        if (platformName.equals("ios")) {
            if (elementId.contains(androidPrefix)) {
                elementId = elementId.replaceAll(androidPrefix, "");
            }
        }
        return byId(elementId, TEXT, waitTime);
    }

    /**
     * Retrieve the value of the 'text' attribute by element xpath. Uses standard wait time.
     * @param elementXpath Element xpath
     * @return Value of the 'text' attribute
     */
    public String getTextValueByXpath(String elementXpath) {
        return getTextValueByXpath(elementXpath, 5);
    }

    /**
     * Retrieve the value of the 'text' attribute by element xpath.
     * @param elementXpath Element xpath
     * @param waitTime Time (in seconds) to implicitly wait. Use -1 to disable the wait
     * @return Value of the 'text' attribute
     */
    public String getTextValueByXpath(String elementXpath, long waitTime) {
        return byXpath(elementXpath, TEXT, waitTime);
    }

    /**
     * Retrieve the value of the 'selected' attribute by element id.
     * @param elementId Element id
     * @return Value of 'selected' attribute
     */
    public boolean getSelectedValueById(String elementId) {
        return convertToBoolean(byId(elementId, SELECTED));
    }

    /**
     * Retrieve the value of the 'selected' attribute by element xpath.
     * @param elementXpath Element xpath
     * @return Value of 'selected' attribute
     */
    public boolean getSelectedValueByXpath(String elementXpath) {
        return convertToBoolean(byXpath(elementXpath, SELECTED));
    }

    /**
     * Retrieve the value of the 'clickable' attribute by element id.
     * @param elementId Element id
     * @return Value of the 'clickable' attribute
     */
    public boolean getClickableValueById(String elementId) {
        return convertToBoolean(byId(elementId, CLICKABLE));
    }

    /**
     * Retrieve the value of the 'clickable' attribute by element xpath
     * @param elementXpath Element xpath
     * @return Value of the 'clickable' attribute
     */
    public boolean getClickableValueByXpath(String elementXpath) {
        return convertToBoolean(byXpath(elementXpath, CLICKABLE));
    }

    /**
     * Retrieve the value of the 'checked' attribute by element id.
     * @param elementId Element id
     * @return Value of 'checked' attribute
     */
    public boolean getCheckedValueById(String elementId) {
        return convertToBoolean(byId(elementId, CHECKED));
    }

    /**
     * Retrieve the value of the 'checked' attribute by element xpath.
     * @param elementXpath Element xpath
     * @return Value of 'checked' attribute
     */
    public boolean getCheckedValueByXpath(String elementXpath) {
        return convertToBoolean(byXpath(elementXpath, CHECKED));
    }

    private boolean convertToBoolean(String returnValue) {
        if(returnValue.equals("true")) {
            return true;
        }
        else {
            return false;
        }
    }
}
