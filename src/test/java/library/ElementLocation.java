package library;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import java.util.concurrent.TimeUnit;

public class ElementLocation {

    private AppiumDriver driver;
    private Layouts layouts;
    private Property property;

    // Holds the dimensions of the screen
    private Dimension size;
    private int yAxis;
    private int xAxis;

    /**
     * ElementLocation constructor
     * @param driver Appium driver
     */
    public ElementLocation(AppiumDriver driver, Layouts layouts, Property property) {
        this.driver = driver;
        this.layouts = layouts;
        this.property = property;

        size = driver.manage().window().getSize();
        yAxis = size.height;
        xAxis = size.width;
    }

    /**
     * Gets the X value of the coordinate of the top left corner of the requested element
     * @param by The method used to search for the element
     * @return X value of top left corner of element
     */
    private int elementLocationX(By by) {
        return elementLocationX(by, 5);
    }

    /**
     * Gets the X value of the coordinate of the top left corner of the requested element
     * @param by The method used to search for the element
     * @param waitTime Time (in seconds) to implicitly wait
     * @return X value of top left corner of element
     */
    private int elementLocationX(By by, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            return driver.findElement(by).getLocation().x;
        }
        catch(Exception e) {
            return 0;
        }
    }

    /**
     * Gets the Y value of the coordinate of the top left corner of the requested element
     * @param by The method used to search for the element
     * @return Y value of top left corner of element
     */
    private int elementLocationY(By by) {
        return elementLocationY(by, 5);
    }

    /**
     * Gets the Y value of the coordinate of the top left corner of the requested element
     * @param by The method used to search for the element
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Y value of top left corner of element
     */
    private int elementLocationY(By by, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            return driver.findElement(by).getLocation().y;
        }
        catch(Exception e) {
            return 0;
        }
    }

    /**
     * Gets the Height value of the requested element
     * @param by The method used to search for the element
     * @return Height value of element
     */
    private int elementLocationHeight(By by) {
        return elementLocationHeight(by, 5);
    }

    /**
     * Gets the Height value of the requested element
     * @param by The method used to search for the element
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Height value of element
     */
    private int elementLocationHeight(By by, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            return driver.findElement(by).getSize().getHeight();
        }
        catch(Exception e) {
            return 0;
        }
    }

    /**
     * Gets the width value of the requested element
     * @param by The method used to search for the element
     * @return Width value of element
     */
    private int elementLocationWidth(By by) {
        return elementLocationWidth(by, 5);
    }

    /**
     * Gets the width value of the requested element
     * @param by The method used to search for the element
     * @param waitTime Time (in seconds) to implicitly wait
     * @return Width value of element
     */
    private int elementLocationWidth(By by, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            return driver.findElement(by).getSize().getWidth();
        }
        catch(Exception e) {
            return 0;
        }
    }

    /**
     * @param elementId ID of the element to find
     * @return X value of the coordinate of the top left corner of the requested element
     */
    public int xById(String elementId) {
        return elementLocationX(By.id(elementId));
    }

    /**
     * @param elementId ID of the element to find
     * @return Y value of the coordinate of the top left corner of the requested element
     */
    public int yById(String elementId) {
        return elementLocationY(By.id(elementId));
    }

    /**
     * @param elementId ID of the element to find
     * @return Height value of the coordinate of the requested element
     */
    public int heightById(String elementId) {
        return elementLocationHeight(By.id(elementId));
    }

    /**
     * @param elementId ID of the element to find
     * @return Width value of the coordinate of the requested element
     */
    public int widthById(String elementId) {
        return elementLocationWidth(By.id(elementId));
    }

    /**
     * @param elementText Text of the element to find
     * @return X value of the coordinate of the top left corner of the requested element
     */
    public int xByXpath(String elementText) {
        return elementLocationX(By.xpath(elementText));
    }

    /**
     * @param elementText Text of the element to find
     * @return Y value of the coordinate of the top left corner of the requested element
     */
    public int yByXpath(String elementText) {
        return elementLocationY(By.xpath(elementText));
    }

    /**
     * @param elementText Text of the element to find
     * @return Height value of the coordinate of the requested element
     */
    public int heightByXpath(String elementText) {
        return elementLocationHeight(By.xpath(Layouts.START + elementText));
    }

    /**
     * @param elementText Text of the element to find
     * @return Width value of the coordinate of the requested element
     */
    public int widthByXpath(String elementText) {
        return elementLocationWidth(By.xpath(Layouts.START + elementText));
    }

    /**
     * @return Pixel height of the screen
     */
    public int screenHeight() {
        return yAxis;
    }

    /**
     * @return Pixel width of the screen
     */
    public int screenWidth() {
        return xAxis;
    }
}