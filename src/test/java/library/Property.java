package library;

// Class meant for setting up strings for finding elements
public class Property {

    /**
     * Index property
     * @param index Index value
     * @return Index string statement
     */
    public String indexEquals(int index) {
        return "@index='" + index + "'";
    }

    // Resource-Id must be the full id found, including 'com.'
    /**
     * Resource-Id property
     * @param id Resource-Id value
     * @return Resource-Id string statement
     */
    public String resourceId(String id) {
        return "@resource-id='" + id + "'";
    }
}
