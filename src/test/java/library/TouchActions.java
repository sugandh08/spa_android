package library;

import io.appium.java_client.AppiumDriver;

/**
 * Available actions for a screen.
 */
public class TouchActions {
    public EnterText enterText;
    public Tap tap;
    public Attribute attribute;
    public ElementExists elementExists;
    public Swipe swipe;
    public ClearTextField clearTextField;

    /**
     * Screen constructor
     * @param driver Appium driver
     */
    public TouchActions(AppiumDriver driver) {
        enterText = new EnterText(driver);
        tap = new Tap(driver);
        attribute = new Attribute(driver);
        elementExists = new ElementExists(driver);
        swipe = new Swipe(driver);
        clearTextField = new ClearTextField(driver);
    }
}
