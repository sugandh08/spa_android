package library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * AutoDetect automatically detects the phone being used and sets up the capability
 * variables to be used when Appium connects to the phone.
 */
public class AutoDetect {

    private static final String WINDOWS = "windows";
    private static final String MAC = "mac";

    // Holds the detected OS name
    private String osDetected = "";

    // Capabilities variables
    private String platformName = "";
    private String platformVersion = "";
    private String deviceName = "";

    // Commands for Windows
    private String[] cmdDeviceName = {"cmd.exe", "/C", "adb devices"};
    private String[] cmdPlatformVersion = {"cmd.exe", "/C", "adb shell getprop ro.build.version.release"};

    // Commands for Android on Mac
    private String[] bashAndroidDeviceName = {"/bin/bash", "-c", "adb devices"};
    private String[] bashAndroidPlatformVersion = {"/bin/bash", "-c", "adb shell getprop ro.build.version.release"};

    // Commands for iOS on Mac
    private String[] bashiOSDeviceInfo = {"/bin/bash", "-c", "xcrun simctl list"};

    /**
     * Automatically configure variables for Appium for Android or iOS.
     */
    public void setup()
    {
        retrieveOS();

        // If Windows
        if(osDetected.equals(WINDOWS)) {
            platformName = "Android";
            deviceName(cmdDeviceName);
            platformVersion(cmdPlatformVersion);
        }
        // Otherwise, Mac
        else {
            platformName = "Android";
            deviceName(bashAndroidDeviceName);

            // If Android
            // '*' return is seen on Mac
            if(!deviceName.equals("") && !deviceName.equals("*"))
            {
                platformVersion(bashAndroidPlatformVersion);
            }
            // Otherwise, iOS
            else
            {
                platformName = "iOS";
                iOSDeviceInfo(bashiOSDeviceInfo);
            }
        }

        // Last step, print info to be used by Appium
        print(platformName);
        print(deviceName);
        print(platformVersion);
    }

    /**
     * Find the OS type of the system to determine which commands to use.
     */
    private void retrieveOS() {
        try {
            // Get the name of the system OS
            osDetected = System.getProperty("os.name");
        }
        catch(Throwable t) {
            System.out.println("Couldn't retrieve the OS name. Error: " + t);
        }

        if(osDetected == null && osDetected.isEmpty()) {
            osDetected = "";
        }
        else if(osDetected.toLowerCase().contains(WINDOWS)) {
            osDetected = WINDOWS;
        }
        else {
            osDetected = MAC;
        }
    }

    /**
     * Finds the assigned phone name of an Android phone.
     * @param command String[] containing commands
     */
    private void deviceName(String[] command) {
        try {
            // setup command and startButton process
            ProcessBuilder pb = new ProcessBuilder(command);
            pb.redirectErrorStream(true);
            Process p = pb.start();

            // Redirect the error stream
            BufferedReader stdin = new BufferedReader(new InputStreamReader((p.getInputStream())));
            String line;

            // Read the redirected error stream
            while((line = stdin.readLine()) != null) {
                // First line contains the word 'List'
                if(line.contains("List")) {
                    if((line = stdin.readLine()) != null) {
                        // Split the string by spaces
                        String[] parts = line.split("\\s+");
                        deviceName = parts[0];
                        break;
                    }
                }
            }

            // Stop the process
            p.destroy();
        }
        catch (IOException ex) {
            System.out.println("Problem getting device name" + ex);
        }
    }

    /**
     * Finds the phone version of an Android phone.
     * @param command String[] containing commands
     */
    private void platformVersion(String[] command) {
        try {
            // setup command and startButton process
            ProcessBuilder pb = new ProcessBuilder(command);
            pb.redirectErrorStream(true);
            Process p = pb.start();

            // Redirect the error stream
            BufferedReader stdin = new BufferedReader(new InputStreamReader((p.getInputStream())));
            String line;

            if((line = stdin.readLine()) != null) {
                platformVersion = line;
            }

            // Stop the process
            p.destroy();
        }
        catch (IOException ex) {
            System.out.println("Problem getting the platform version. Error: " + ex);
        }
    }

    /**
     * Finds the device name and phone version for an iOS phone.
     * @param command String[] containing commands
     */
    private void iOSDeviceInfo(String[] command) {
        String iOSVersion = "";
        try {
            // setup command and startButton process
            ProcessBuilder pb = new ProcessBuilder(command);
            pb.redirectErrorStream(true);
            Process p = pb.start();

            // Redirect the error stream
            BufferedReader stdin = new BufferedReader(new InputStreamReader((p.getInputStream())));
            String line;

            // Clearing variable
            deviceName = "";

            // Read a line when it isn't null
            while((line = stdin.readLine()) != null) {
                // Captures the version of the booted device
                if(line.contains("-- iOS")) {
                    iOSVersion = line;
                }

                // Finds the first booted device
                if(line.contains("Booted")) {
                    // Split the string by spaces
                    List<String> parts = new ArrayList<String>(Arrays.asList(line.split("\\s+")));

                    // Remove empty elements
                    parts.removeAll(Collections.singleton(""));

                    // Iterate the list
                    for(int i=0;i<parts.size();i++) {
                        // Check for parenthesis
                        if(parts.get(i).contains("(")) {
                            break;
                        }

                        // Add a space after each word found
                        if(i != 0) {
                            deviceName += " ";
                        }

                        deviceName += parts.get(i);
                    }

                    // Needed a way to break the while loop
                    // after finding the device name
                    if(!deviceName.equals("")) {
                        break;
                    }
                }
            }

            // Retrieve the name of the phone
            if(iOSVersion != null && !iOSVersion.equals("") && !iOSVersion.equals("*")) {
                // Split the string by spaces
                List<String> parts = new ArrayList<String>(Arrays.asList(iOSVersion.split("\\s+")));

                // Remove empty elements
                parts.removeAll(Collections.singleton(""));

                // Iterate the list
                for(int i=0;i<parts.size();i++) {
                    // If the element contains a letter or number
                    if(parts.get(i).matches(".*[0-9].*")) {
                        platformVersion = parts.get(i);
                        break;
                    }
                }
            }
        }
        catch (IOException ex) {
            System.out.println("Problem getting iOS device information. Error: " + ex);
        }
    }

    /**
     * Get the platform name. Will determine phone OS based
     * on system OS.
     * @return Platform name
     */
    public String getPlatformName(){
        return platformName;
    }

    /**
     * Retrieves the device name.
     * @return Device name
     */
    public String getDeviceName(){
        return deviceName;
    }

    /**
     * Retrieves the platform version of the phone to be used
     * for testing.
     * @return Platform version
     */
    public String getPlatformVersion(){
        return platformVersion;
    }

    /**
     * Print to the system.
     * @param info String to print to the system
     */
    private void print(String info){
        System.out.println(info);
    }
}
