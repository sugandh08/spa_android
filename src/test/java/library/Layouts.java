package library;

// All layouts get information from property
public class Layouts {

    // This is specific for Android, don't need file.separator
    /**
     * Any id or xpath equalsSearch starts with an extra forward slash
     * @return Forward slash
     */
    public static final String START = "/";

    // Android specific call
    /**
     * String for Linear Layout in Android
     * @param property Property call for Linear Layout
     * @return Linear Layout string statement
     */
    public String linearLayout(String property) {
        return "/android.widget.LinearLayout[" + property + "]";
    }

    // Android specific call
    /**
     * String for Relative Layout in Android
     * @param property Property call for Relative Layout
     * @return Relative Layout string statement
     */
    public String relativeLayout(String property) {
        return "/android.widget.RelativeLayout[" + property + "]";
    }

    // Android specific call
    /**
     * String for Frame Layout in Android
     * @param property Property call for Frame Layout
     * @return Frame Layout string statement
     */
    public String frameLayout(String property) {
        return "/android.widget.FrameLayout[" + property + "]";
    }

    // Android specific call
    /**
     * String for Recycler View in Android
     * @param property Property call for Recycler View
     * @return Recycler View string statement
     */
    public String recyclerView(String property) {
        return "/android.support.v7.widget.RecyclerView[" + property + "]";
    }

    // Android specific call
    /**
     * String for Text View in Android
     * @param property Property call for Text View
     * @return Text View string statement
     */
    public String textView(String property) {
        return "/android.widget.TextView[" + property + "]";
    }

    // Android specific call
    /**
     * String for Image View in Android
     * @param property Property call for Image View
     * @return Image View string statement
     */
    public String imageView(String property) {
        return "/android.widget.ImageView[" + property + "]";
    }

    // Android specific call
    /**
     * String for Tab Widget in Android
     * @param property Property call for Tab Widget
     * @return Tab Widget string statement
     */
    public String tabWidget(String property) {
        return "/android.widget.TabWidget[" + property + "]";
    }

    // Android specific call
    /**
     * String for Number Picker Widget in Android
     * @param property Property call for Number Picker Widget
     * @return Number Picker Widget string statement
     */
    public String numberPickerWidget(String property) {
        return "/android.widget.NumberPicker[" + property + "]";
    }

    /**
     * String for equalsSearch
     * @param property Property call for equals
     * @return Equals string statement
     */
    public String equalsSearch(String property) {
        // Need to use Property methods with 'xxxContains'
        return "/*[@text='" + property + "']";
    }
}
