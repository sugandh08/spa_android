package library;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.concurrent.TimeUnit;

/**
 * Contains different ways of finding an element to click on.
 */
public class Tap {
    private AppiumDriver driver;

    /**
     * tap constructor
     * @param driver Appium driver
     */
    public Tap(AppiumDriver driver) {
        this.driver = driver;
    }

    /**
     * Uses driver click() on the selected element. Uses default wait time.
     * @param by Element search method to use
     * @return True if the element was found and clicked, false otherwise
     */
    private boolean tapAction(By by) {
        return tapAction(by, 5);
    }

    /**
     * Uses driver click() on the selected element. Uses default wait time.
     * @param by Element search method to use
     * @param waitTime Time (in seconds) to implicitly wait
     * @return True if the element was found and clicked, false otherwise
     */
    private boolean tapAction(By by, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        try {
            driver.findElement(by).click();
            return true;
        }
        catch(NoSuchElementException e) {
            String exceptionMessage = e.getMessage();
//            String printout = exceptionMessage.substring(0, exceptionMessage.indexOf("(WARNING:")) +
//                    "\n" + exceptionMessage.substring(exceptionMessage.indexOf("***"));
            System.out.println(exceptionMessage);
            return false;
        }
    }

    /**
     * Find element by id and tap on it. Uses standard wait time
     * @param elementId Element id
     * @return True if element was tapped, false otherwise
     */
    public boolean elementById(String elementId) {
        return elementById(elementId, 5);
    }

    /**
     * Find element by id and tap on it.
     * @param elementId Element id
     * @param waitTime Time (in seconds) to implicitly wait
     * @return True if element was tapped, false otherwise
     */
    public boolean elementById(String elementId, long waitTime) {
        return tapAction(By.id(elementId), waitTime);
    }

    /**
     * Find element by text and tap on it. Uses standard wait time
     * @param elementXpath xpath used to find the element
     * @return True if element was tapped, false otherwise
     */
    public boolean elementByXpath(String elementXpath) {
        return elementByXpath(elementXpath, 5);
    }

    /**
     * Find element by text and tap on it
     * @param elementXpath xpath used to find the element
     * @param waitTime Time (in seconds) to implicitly wait
     * @return True if element was tapped, false otherwise
     */
    public boolean elementByXpath(String elementXpath, long waitTime) {
        return tapAction(By.xpath(elementXpath), waitTime);
    }

    /**
     * Tap on a location on the screen.
     * @param x X coordinate
     * @param y Y coordinate
     * @return
     */
    public boolean tapAtCoordinate(int x, int y) {
        new TouchAction(driver)
                .tap(new PointOption().withCoordinates(x, y))
                .perform();
        return true;
    }
}
