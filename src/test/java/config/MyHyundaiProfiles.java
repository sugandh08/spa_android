package config;

public class MyHyundaiProfiles {

    public Profile gen1Standard;

    public Profile gen2GasOrHevStandard;

    public Profile gen2HybridStandard;

    public Profile gen2ElectricStandard;

    public Profile gen2OSElectricStandard;



    public Profile twoGen2;

    public Profile withSecondaryDriver;

    public Profile gen2Point5;

    public Profile withDTCs;

    public Profile withoutDTCs;

    public Profile myHyundaiAndGenesisVehicles;

    public Profile genesisVehicleOnly;

    public Profile landlinePrimaryPhone;

    public Profile mobilePrimaryPhone;

    public Profile notificationEmailDifferentThanLogin;

    public Profile notificationEmailSameAsLogin;

    public Profile allContactInfoFilledOut;

    public Profile hasPois;

    public Profile allMvrElectricalComponents;

    public Profile Gen2ElectricStandard;
}
