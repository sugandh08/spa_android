package config;

public class Vehicle {

    public String vin;

    public String model;

    public String nickname;

    public PowerType powerType;

    public String generation;

    public App app;

    public boolean hasConnectedCare;

    public boolean hasRemote;

    public boolean hasGuidance;

    public enum PowerType {
        GAS,
        HEV,
        PHEV,
        AEEV,
        OSEV
    }

    public enum CarPackage {
        CONNECTED_CARE,
        REMOTE,
        GUIDANCE
    }

    public enum App {
        GENESIS,
        MYHYUNDAI
    }
}
