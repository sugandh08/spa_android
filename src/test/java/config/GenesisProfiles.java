package config;

public class GenesisProfiles {

    public Profile gen2GasStandard;

    public Profile twoGen2;

    public Profile withSecondaryDriver;

    public Profile gen2Point5;

    public Profile withDTCs;

    public Profile withoutDTCs;

    public Profile genesisAndMyHyundaiVehicles;

    public Profile myHyundaiVehicleOnly;

    public Profile landlinePrimaryPhone;

    public Profile mobilePrimaryPhone;

    public Profile notificationEmailDifferentThanLogin;

    public Profile notificationEmailSameAsLogin;

    public Profile allContactInfoFilledOut;

    public Profile hasPois;

    public Profile allMvrElectricalComponents;
}
