package config;

public class Profile {

    public Account primaryAccount;

    public Account secondaryAccount;

    public String primaryVin;

    public String secondaryVin;

}
