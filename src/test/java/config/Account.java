package config;

import java.util.HashMap;

public class Account {
    public String email;

    public String password;

    public String pin;

    public String firstName;

    public String lastName;

    /**
     * Map containing the vehicles tied to this account. The key is the VIN.
     */
    public HashMap<String, Vehicle> vehicles;

    public String notificationEmail;

    public String landlinePhone;

    public String mobilePhone;

    public String addressLine1;

    public String addressLine2;

    public String emergencyContactName;

    public String emergencyContactLandlinePhone;

    public String emergencyContactMobilePhone;

    public String emergencyContactEmail;

    public String securityAnswer;

}
