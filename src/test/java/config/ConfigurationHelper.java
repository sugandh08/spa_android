package config;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.HashMap;


public class ConfigurationHelper {
    public enum AppNames {
        MYHYUNDAI_ANDROID("MyHyundaiAndroid"),
        MYHYUNDAI_IOS("MyHyundaiiOS"),
        GENESIS_ANDROID("GenesisAndroid"),
        GENESIS_IOS("GenesisiOS");

        private final String appName;

        AppNames(final String appName) {
            this.appName = appName;
        }

        @Override
        public String toString() {
            return appName;
        }
    }

    private Document config;

    private HashMap<String, Vehicle> vehicles;
    private HashMap<String, Account> accounts;

    public ConfigurationHelper() {
        String filePath = "./config.xml";
        try {
            InputSource inputSource = new InputSource(new InputStreamReader(new FileInputStream(new File(filePath))));
            this.config = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(inputSource);
        } catch (FileNotFoundException fnfe) {
            System.err.println("Configuration file not present. 'config.xml' should be placed at the root of the project folder.");
            fnfe.printStackTrace();
        } catch (ParserConfigurationException pce) {
            System.err.println("Something went wrong when creating the DocumentBuilder");
            pce.printStackTrace();
        } catch (SAXException se) {
            System.err.println("Parsing error.");
            se.printStackTrace();
        } catch (IOException ioe) {
            System.err.println("IO error.");
            ioe.printStackTrace();
        }
    }

    public String getAppFileName(AppNames appName) {
        NodeList appFileNodes = this.config.getElementsByTagName(appName.toString() + "AppFile");
        if (appFileNodes.getLength() >= 1) {
            Node appFileNode = appFileNodes.item(0);
            return appFileNode.getTextContent();
        }
        System.err.println("No tag '" + appName.toString() + "AppFile' found in the configuration file.");
        return null;
    }

    public MyHyundaiProfiles getMyHyundaiProfiles() {
        String MYHYUNDAI = "MyHyundai";
        // Get the accounts
        HashMap<String, Account> accounts = this.getAccounts();

        // Load each profile into a Profiles object
        MyHyundaiProfiles profiles = new MyHyundaiProfiles();
        profiles.gen1Standard = this.getProfile("Gen1Standard", MYHYUNDAI, accounts);
        profiles.gen2GasOrHevStandard = this.getProfile("Gen2GasOrHevStandard", MYHYUNDAI, accounts);
        profiles.gen2HybridStandard = this.getProfile("Gen2HybridStandard", MYHYUNDAI, accounts);
        profiles.gen2ElectricStandard = this.getProfile("Gen2ElectricStandard", MYHYUNDAI, accounts);
        profiles.gen2OSElectricStandard = this.getProfile("Gen2OSElectricStandard", MYHYUNDAI, accounts);
        profiles.twoGen2 = this.getProfile("TwoGen2", MYHYUNDAI, accounts);
        profiles.withSecondaryDriver = this.getProfile("WithSecondaryDriver",MYHYUNDAI, accounts);
        profiles.gen2Point5 = this.getProfile("Gen2Point5",MYHYUNDAI, accounts);

        //profiles.withDTCs = this.getProfile("WithDTCs",MYHYUNDAI, accounts);
        profiles.withoutDTCs = this.getProfile("WithoutDTCs",MYHYUNDAI, accounts);
        profiles.myHyundaiAndGenesisVehicles = this.getProfile("MyHyundaiAndGenesisVehicles",MYHYUNDAI, accounts);
        profiles.genesisVehicleOnly = this.getProfile("GenesisVehicleOnly",MYHYUNDAI, accounts);
        profiles.landlinePrimaryPhone =  this.getProfile("LandlinePrimaryPhone", MYHYUNDAI, accounts);
        profiles.mobilePrimaryPhone =  this.getProfile("MobilePrimaryPhone", MYHYUNDAI, accounts);
        profiles.notificationEmailDifferentThanLogin =  this.getProfile("NotificationEmailDifferentThanLogin", MYHYUNDAI, accounts);
        profiles.notificationEmailSameAsLogin =  this.getProfile("NotificationEmailSameAsLogin", MYHYUNDAI, accounts);
        profiles.allContactInfoFilledOut = this.getProfile("AllContactInfoFilledOut", MYHYUNDAI, accounts);
        profiles.hasPois = this.getProfile("HasPOIs", MYHYUNDAI, accounts);
        profiles.allMvrElectricalComponents = this.getProfile("AllMvrElectricalComponents", MYHYUNDAI, accounts);

        return profiles;
    }

    public GenesisProfiles  getGenesisProfiles() {
        String GENESIS = "Genesis";
        // Get the accounts
        HashMap<String, Account> accounts = this.getAccounts();

        // Load each profile into a Profiles object
        GenesisProfiles profiles = new GenesisProfiles();
        profiles.gen2GasStandard = this.getProfile("Gen2GasStandard", GENESIS, accounts);
        profiles.twoGen2 = this.getProfile("TwoGen2", GENESIS, accounts);
        profiles.withSecondaryDriver = this.getProfile("WithSecondaryDriver", GENESIS, accounts);
        profiles.gen2Point5=this.getProfile("Gen2Point5", GENESIS,accounts);
        //profiles.withDTCs = this.getProfile("WithDTCs", GENESIS, accounts);
        profiles.withoutDTCs = this.getProfile("WithoutDTCs", GENESIS, accounts);
        profiles.genesisAndMyHyundaiVehicles = this.getProfile("GenesisAndMyHyundaiVehicles", GENESIS, accounts);
        profiles.myHyundaiVehicleOnly = this.getProfile("MyHyundaiVehicleOnly", GENESIS, accounts);
        profiles.landlinePrimaryPhone = this.getProfile("LandlinePrimaryPhone", GENESIS, accounts);
        profiles.mobilePrimaryPhone = this.getProfile("MobilePrimaryPhone", GENESIS, accounts);
        profiles.notificationEmailDifferentThanLogin = this.getProfile("NotificationEmailDifferentThanLogin", GENESIS, accounts);
        profiles.notificationEmailSameAsLogin = this.getProfile("NotificationEmailSameAsLogin", GENESIS, accounts);
        profiles.allContactInfoFilledOut = this.getProfile("AllContactInfoFilledOut", GENESIS, accounts);
        profiles.hasPois = this.getProfile("HasPOIs", GENESIS, accounts);
        profiles.allMvrElectricalComponents = this.getProfile("AllMvrElectricalComponents", GENESIS, accounts);

        return profiles;
    }

    private Profile getProfile(String name, String forApp, HashMap<String, Account> possibleAccounts) {
        NodeList profileNodes = this.config.getElementsByTagName(name);
        Node profileNode = null;

        for(int i = 0; i < profileNodes.getLength(); i++) {
            String profileParentName = profileNodes.item(i).getParentNode().getNodeName();
            String expectedParentName = forApp + "Profiles";

            if (profileParentName.equals(expectedParentName)) {
                profileNode = profileNodes.item(i);
            }
        }

        if (profileNode == null) {
            System.err.println("No tag '" + forApp + "Profiles' found in the configuration file.");
            return null;
        }

        Profile profile = new Profile();

        NodeList profileChildren = profileNode.getChildNodes();

        for (int i = 0; i < profileChildren.getLength(); i++) {
            Node child = profileChildren.item(i);
            String childName = child.getNodeName();
            String value = child.getTextContent();

            switch (childName) {
                case "PrimaryAccountEmail":
                    profile.primaryAccount = possibleAccounts.get(value);
                    break;

                case "SecondaryAccountEmail":
                    profile.secondaryAccount = possibleAccounts.get(value);
                    break;

                case "VIN":
                    if (profile.primaryVin == null) {
                        profile.primaryVin = value;
                    } else {
                        profile.secondaryVin = value;
                    }
                    break;
            }
        }

        return profile;
    }

    private HashMap<String, Account> getAccounts() {
        if (this.accounts == null) {
            this.accounts = this.loadAccounts(this.getVehicles());
        }
        return this.accounts;
    }

    private HashMap<String, Account> loadAccounts(HashMap<String, Vehicle> possibleVehicles) {
        HashMap<String, Account> accounts = new HashMap<>();
        NodeList accountNodes = this.config.getElementsByTagName("Account");

        for (int i = 0; i < accountNodes.getLength(); i++) {
            Account account = new Account();
            Node accountNode = accountNodes.item(i);

            if (accountNode.hasChildNodes()) {
                NodeList accountNodeChildren = accountNode.getChildNodes();

                for (int j = 0; j < accountNodeChildren.getLength(); j++) {
                    Node currentNode = accountNodeChildren.item(j);
                    String currentNodeName = currentNode.getNodeName();

                    switch (currentNodeName) {
                        case "Email":
                            account.email = currentNode.getTextContent();
                            break;

                        case "Password":
                            account.password = currentNode.getTextContent();
                            break;

                        case "PIN":
                            account.pin = currentNode.getTextContent();
                            break;

                        case "FirstName":
                            account.firstName = currentNode.getTextContent();
                            break;

                        case "LastName":
                            account.lastName = currentNode.getTextContent();
                            break;

                        case "Vehicles":
                            if (currentNode.hasChildNodes()) {
                                NodeList vinNodes = currentNode.getChildNodes();
                                HashMap<String, Vehicle> vehicles = new HashMap<>();

                                for (int k = 0; k < vinNodes.getLength(); k++) {
                                    Node vinNode = vinNodes.item(k);
                                    String vinNodeName = vinNode.getNodeName();

                                    if (vinNodeName.equals("VIN")) {
                                        String vin = vinNode.getTextContent();
                                        Vehicle v = possibleVehicles.get(vin);
                                        vehicles.put(vin, v);
                                    }
                                }

                                account.vehicles = vehicles;
                            }
                            else {
                                System.err.println("Found empty Vehicles tag under an Account tag.");
                            }
                            break;

                        case "NotificationEmail":
                            account.notificationEmail = currentNode.getTextContent();
                            break;

                        case "LandlinePhone":
                            account.landlinePhone = currentNode.getTextContent();
                            break;

                        case "MobilePhone":
                            account.mobilePhone = currentNode.getTextContent();
                            break;

                        case "AddressLine1":
                            account.addressLine1 = currentNode.getTextContent();
                            break;

                        case "AddressLine2":
                            account.addressLine2 = currentNode.getTextContent();
                            break;

                        case "EmergencyContactName":
                            account.emergencyContactName = currentNode.getTextContent();
                            break;

                        case "EmergencyContactLandlinePhone":
                            account.emergencyContactLandlinePhone = currentNode.getTextContent();
                            break;

                        case "EmergencyContactMobilePhone":
                            account.emergencyContactMobilePhone = currentNode.getTextContent();
                            break;

                        case "EmergencyContactEmail":
                            account.emergencyContactEmail = currentNode.getTextContent();
                            break;

                        case "SecurityAnswer":
                            account.securityAnswer = currentNode.getTextContent();
                            break;
                    }
                }
            }
            else {
                System.err.println("Found empty Account tag");
            }
            accounts.put(account.email, account);
        }

        return accounts;
    }

    private HashMap<String, Vehicle> getVehicles() {
        if (this.vehicles == null) {
            this.vehicles = this.loadVehicles();
        }
        return this.vehicles;
    }

    private HashMap<String, Vehicle> loadVehicles() {
        HashMap<String, Vehicle> vehicles = new HashMap<>();
        NodeList vehicleNodes = this.config.getElementsByTagName("Vehicle");

        for (int i = 0; i < vehicleNodes.getLength(); i++) {
            Vehicle vehicle = new Vehicle();
            Node vehicleNode = vehicleNodes.item(i);

            if (vehicleNode.hasChildNodes()) {
                NodeList vehicleNodeChildren = vehicleNode.getChildNodes();

                for (int j = 0; j < vehicleNodeChildren.getLength(); j++) {
                    Node currentNode = vehicleNodeChildren.item(j);
                    String currentNodeName = currentNode.getNodeName();

                    switch (currentNodeName) {
                        case "VIN":
                            vehicle.vin = currentNode.getTextContent();
                            break;
                        case "Model":
                            vehicle.model = currentNode.getTextContent();
                            break;
                        case "Nickname":
                            vehicle.nickname = currentNode.getTextContent();
                            break;
                        case "PowerType":
                            vehicle.powerType = Vehicle.PowerType.valueOf(currentNode.getTextContent().toUpperCase());
                            break;
                        case "Generation":
                            vehicle.generation = currentNode.getTextContent();
                            break;
                        case "App":
                            vehicle.app = Vehicle.App.valueOf(currentNode.getTextContent().toUpperCase());
                            break;
                        case "Packages":
                            // Go into the packages node
                            NodeList packageNodes = currentNode.getChildNodes();

                            for (int k = 0; k < packageNodes.getLength(); k++) {
                                Node packageNode = packageNodes.item(k);
                                String packageName = packageNode.getNodeName();

                                switch (packageName) {
                                    case "ConnectedCare":
                                        vehicle.hasConnectedCare = Boolean.parseBoolean(
                                                packageNode.getAttributes().getNamedItem("enabled").getNodeValue());
                                        break;
                                    case "Remote":
                                        vehicle.hasRemote = Boolean.parseBoolean(
                                                packageNode.getAttributes().getNamedItem("enabled").getNodeValue());
                                        break;
                                    case "Guidance":
                                        vehicle.hasGuidance = Boolean.parseBoolean(
                                                packageNode.getAttributes().getNamedItem("enabled").getNodeValue());
                                        break;
                            }
                        }
                    }
                }
                vehicles.put(vehicle.vin, vehicle); 
            } else {
                System.err.println("Found empty Vehicle tag");
            }
        }

        return vehicles;
    }

    public String getTestVersion() {
        NodeList versionNodes = this.config.getElementsByTagName("TestVersion");
        if (versionNodes.getLength() >= 1) {
            Node versionNode = versionNodes.item(0);
            return versionNode.getTextContent();
        }
        System.err.println("No tag 'TestRepetitions' found in the configuration file.");
        // Default to N/A
        return "N/A";
    }
}
