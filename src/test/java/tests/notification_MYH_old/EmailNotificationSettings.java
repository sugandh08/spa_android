package tests.notification_MYH_old;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. MobilePrimaryPhone

@Disabled
@Epic("Email Notification Settings Test")
@Feature("Email Notification Settings")
@DisplayName("Email Notification Settings")
public class EmailNotificationSettings extends TestController {
    /**
     * Logs in with primary account and primary VIN of given profile, opens settings page
     * @param profile Profile with the primary account and VIN to use
     */
    private void loginAndOpenSettings(Profile profile) {
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);

        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.settingsButton.tap();
        guiController.settingsScreen.isPresent(10);
    }

    @BeforeEach
    void setupTests() {
        loginAndOpenSettings(appController.myHyundaiProfiles.mobilePrimaryPhone);
    }

    @Nested
    @DisplayName("Connected Care settings are open")
    class ConnectedCareSettingsAreOpen {
        @BeforeEach
        void openConnectedCareSettings() {
            guiController.settingsScreen.connectedCareExpandButton.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.1 Pressing the toggle all email button in connected care toggles every connected care email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Pressing the toggle all email button in connected care toggles every connected care email setting")
        void togglingAllConnectedCareEmailSetsAllEmailsToSameState() {
            guiController.settingsScreen.connectedCareToggleAllEmail.tap();

            // get the enabled/disabled state of the 'toggle all email' element
            boolean toggleState = guiController.settingsScreen.connectedCareToggleAllEmail.getSelectedValue();

            // List of setting names in the connected care section
            String[] settingNames = {
                    guiController.settingsScreen.automaticCollisionNotificationText,
                    guiController.settingsScreen.sosEmergencyAssistanceText,
                    guiController.settingsScreen.automaticDtcText,
                    guiController.settingsScreen.maintenanceAlertText
            };

            // Check each connected care setting and make sure the email toggle matches the "toggle all" state
            boolean settingStatesCorrect = true;
            for (String settingName : settingNames) {
                if (!guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(settingName) ||
                        guiController.settingsScreen.getEmailNotificationStateOfSetting(settingName) != toggleState) {
                    settingStatesCorrect = false;
                    break;
                }
            }

            assertTrue(settingStatesCorrect);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.2 ACN email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify ACN email status is displayed")
        void acnEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.automaticCollisionNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.3 ACN email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify ACN email toggle changes email setting")
        void acnEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.automaticCollisionNotificationText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.4 SOS emergency assistance email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify SOS emergency assistance email status is displayed")
        void sosEmergencyAssistanceEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.sosEmergencyAssistanceText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.5 SOS emergency assistance email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify SOS emergency assistance email toggle changes email setting")
        void sosEmergencyAssistanceEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.sosEmergencyAssistanceText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.6 Automatic DTC email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Automatic DTC email status is displayed")
        void  automaticDtcEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.automaticDtcText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.7 Automatic DTC email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Automatic DTC email toggle changes email setting")
        void automaticDtcEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.automaticDtcText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.8 Maintenance Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Maintenance Alert email status is displayed")
        void maintenanceAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.maintenanceAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.9 Maintenance Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Maintenance Alert email toggle changes email setting")
        void maintenanceAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.maintenanceAlertText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }
    }

    @Nested
    @DisplayName("Remote settings are open")
    class RemoteSettingsAreOpen {
        @BeforeEach
        void openRemoteSettings() {
            guiController.settingsScreen.remoteExpandButton.tap();
            guiController.settingsScreen.swipe.fromBottomEdge(1);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.10 Pressing the toggle all email button in remote_genesis_old toggles every remote_genesis_old email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Pressing the toggle all email button in remote_genesis_old toggles every remote_genesis_old email setting")
        void togglingAllRemoteEmailSetsAllEmailsToSameState() {
            guiController.settingsScreen.remoteToggleAllEmail.tap();

            // get the enabled/disabled state of the 'toggle all email' element
            boolean toggleState = guiController.settingsScreen.remoteToggleAllEmail.getSelectedValue();

            // List of setting names in the connected care section
            String[] settingNames = {
                    guiController.settingsScreen.panicNotificationText,
                    guiController.settingsScreen.alarmNotificationText,
                    guiController.settingsScreen.hornAndLightsText,
                    guiController.settingsScreen.remoteEngineStartStopText,
                    guiController.settingsScreen.remoteDoorLockUnlockText,
                    guiController.settingsScreen.curfewAlertText,
                    guiController.settingsScreen.valetAlertText,
                    guiController.settingsScreen.geofenceAlertText,
                    guiController.settingsScreen.speedAlertText
            };

            // Check each connected care setting and make sure the email toggle matches the "toggle all" state
            boolean settingStatesCorrect = true;
            for (String settingName : settingNames) {
                if (!guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(settingName) ||
                        guiController.settingsScreen.getEmailNotificationStateOfSetting(settingName) != toggleState) {
                    settingStatesCorrect = false;
                    break;
                }
            }

            assertTrue(settingStatesCorrect);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.11 Panic Notification email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Panic Notification email status is displayed")
        void panicNotificationEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.panicNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.12 Panic Notification email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Panic Notification email toggle changes email setting")
        void panicNotificationEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.panicNotificationText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.13 Alarm Notification email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Alarm Notification email status is displayed")
        void alarmNotificationEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.alarmNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.14 Alarm Notification email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Alarm Notification email toggle changes email setting")
        void alarmNotificationEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.alarmNotificationText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.15 Horn and Lights email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Horn and Lights email status is displayed")
        void hornAndLightsEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.hornAndLightsText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.16 Horn and Lights email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Horn and Lights email toggle changes email setting")
        void hornAndLightsEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.hornAndLightsText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.17 Remote Engine Start/Stop email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Engine Start/Stop email status is displayed")
        void remoteEngineStartStopEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.remoteEngineStartStopText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.18 Remote Engine Start/Stop email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Engine Start/Stop email toggle changes email setting")
        void remoteEngineStartStopEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.remoteEngineStartStopText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.19 Remote Door Lock/Unlock email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Door Lock/Unlock email status is displayed")
        void remoteDoorLockUnlockEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.remoteDoorLockUnlockText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.20 Remote Door Lock/Unlock email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Door Lock/Unlock email toggle changes email setting")
        void remoteDoorLockUnlockEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.remoteDoorLockUnlockText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.21 Curfew Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Curfew Alert email status is displayed")
        void curfewAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.curfewAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.22 Curfew Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Curfew Alert email toggle changes email setting")
        void curfewAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.curfewAlertText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.23 Valet Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Valet Alert email status is displayed")
        void valetAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.valetAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.24 Valet Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Valet Alert email toggle changes email setting")
        void valetAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.valetAlertText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.25 Geofence Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Geofence Alert email status is displayed")
        void geofenceAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.geofenceAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.26 Geofence Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Geofence Alert email toggle changes email setting")
        void geofenceAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.geofenceAlertText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.27 Speed Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify  Speed Alert email status is displayed")
        void speedAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreen.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreen.speedAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.28 Speed Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Speed Alert email toggle changes email setting")
        void speedAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreen.speedAlertText;
            boolean currentSetting = guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }
    }
}
