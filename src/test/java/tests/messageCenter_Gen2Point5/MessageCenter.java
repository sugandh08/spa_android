package tests.messageCenter_Gen2Point5;

import config.Profile;
import org.junit.jupiter.api.DisplayName;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Message Center Test")
public class MessageCenter extends TestController {


    private void loginAndMessageCenter(Profile profile) {


        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);


        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);

homeScreen.messageCenterButton.tap(10);



        guiController.messageCenterScreen_gen2Point5.isPresent();
    }



















}
