package tests.myhyundai_smoke.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. OS Electric Vehicle

@Epic("POI Functions Ev Test")
@Feature("POI Functions Ev")
@DisplayName("POI Functions Ev")
public class PoiFunctionEv extends TestController {
    @Nested
    @DisplayName("Signed in as OS Electric standard")
    class Gen2GasStandard {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.myHyundaiProfiles.gen2OSElectricStandard;

            guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 30);
            appController.appFunctions.pauseTestExecution(5, 10000);
            guiController.osElectricHomeScreen.isPresent(60);
        }



        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.2.1 Charge Station is searchable")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Ev")
        @Description("MyHyundai - Verify Charge Station is searchable")
        void gasStationIsSearchable() {
            String zipCode = "92708";

           /* guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.osElectricHomeScreen,
                    guiController.osElectricHomeScreen.mapButton,
                    guiController.osElectricHomeScreen.mapOptions.chargeStationsButton
            );  commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.chargeStationScreen.searchEditText.enterText(zipCode);
            guiController.chargeStationScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.chargeStationScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertEquals(zipCode, guiController.chargeStationScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.2.2 Charge Stations are viewable in an expanded list")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Ev")
        @Description("MyHyundai - Verify Charge Stations are viewable in an expanded list")
        void gasStationsAreDisplayedInListView() {
            String zipCode = "92708";
/*
            guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.osElectricHomeScreen,
                    guiController.osElectricHomeScreen.mapButton,
                    guiController.osElectricHomeScreen.mapOptions.chargeStationsButton
            );  commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.chargeStationScreen.searchEditText.enterText(zipCode);
            guiController.chargeStationScreen.searchButton.tap();
            guiController.chargeStationScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.chargeStationScreen.listButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.chargeStationScreen.searchResultsListView.elementExists());
        }
    }



}
