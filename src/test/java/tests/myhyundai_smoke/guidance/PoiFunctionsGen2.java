package tests.myhyundai_smoke.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard
// 2. WithSecondaryDriver

@Epic("POI Functions Gen2 Test")
@Feature("POI Functions Gen2")
@DisplayName("POI Functions Gen2")
public class PoiFunctionsGen2 extends TestController {
    @Nested
    @DisplayName("Signed in as Gen 2 gas standard")
    class Gen2GasStandard {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;

            guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.1 POI Search box exists")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify POI Search box exists")
        void poiSearchBoxIsVisible() {
            /*guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.poiSearchButton,
                    guiController.poiSearchScreen
            ); commenting for now pushpa */
            assertTrue(guiController.poiSearchScreen.isPresent());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.2 POI Search results are viewable in an expanded list")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify POI Search results are viewable in an expanded list")
        void poiSearchResultsExpandedListShowsAnEntry() {
           /* guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.poiSearchButton,
                    guiController.poiSearchScreen
            ); commenting for now pushpa*/
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.poiSearchScreen.searchEditText.enterText("92708");
            guiController.poiSearchScreen.searchButton.tap();
            guiController.poiSearchScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.poiSearchScreen.listButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiSearchScreen.searchResultsListView.elementExists());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.3 My POIs are viewable in an expanded list")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify My POIs are viewable in an expanded list")
        void myPoisAreDisplayedInListView() {
            /*guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.myPoiButton
            );  commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);

            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.poiSearchScreen.listButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.favoritesPOIScreen.searchResultsListView.elementExists());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.4 Hyundai Dealer zip code is searchable")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify Hyundai Dealer zip code is searchable")
        void hyundaiDealerZipCodeIsSearchable() {
            String zipCode = "55555";
/*
            guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.hyundaiDealersButton
            );  commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
            guiController.dealerLocatorScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.dealerLocatorScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertEquals(zipCode, guiController.dealerLocatorScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.5 Hyundai Dealers are viewable in an expanded list")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify Hyundai Dealers are viewable in an expanded list")
        void hyundaiDealersAreDisplayedInListView() {
            String zipCode = "55555";
/*
            guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.hyundaiDealersButton
            );  commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
            guiController.dealerLocatorScreen.searchButton.tap();
            guiController.dealerLocatorScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.dealerLocatorScreen.listButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.dealerLocatorScreen.searchResultsListView.elementExists());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.6 Gas Station is searchable")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify Gas Station is searchable")
        void gasStationIsSearchable() {
            String zipCode = "92708";
/*
            guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.nearbyGasButton
            ); commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.nearbyGasScreen.searchEditText.enterText(zipCode);
            guiController.nearbyGasScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.nearbyGasScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertEquals(zipCode, guiController.nearbyGasScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.7 Gas Stations are viewable in an expanded list")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Gen2")
        @Description("MyHyundai - Verify Gas Stations are viewable in an expanded list")
        void gasStationsAreDisplayedInListView() {
            String zipCode = "55555";
/*
            guiController.selectExpandableItemSafelyFromHomeScreen(
                    guiController.gasHomeScreen,
                    guiController.gasHomeScreen.mapButton,
                    guiController.gasHomeScreen.mapOptions.nearbyGasButton
            );  commenting for now pushpa  */
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.nearbyGasScreen.searchEditText.enterText(zipCode);
            guiController.nearbyGasScreen.searchButton.tap();
            guiController.nearbyGasScreen.searchButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.nearbyGasScreen.listButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.nearbyGasScreen.searchResultsListView.elementExists());
        }
    }



}
