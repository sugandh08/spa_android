package tests.myhyundai_smoke.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControls;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Stop")
@Feature("Remote Stop")
@DisplayName("Remote Stop")
public class RemoteStop extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.1 Remote stop success")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote stop success")
    void remoteStopSuccess() {
        // start the car. gotta start it in order to stop it.
        guiController.remoteCarControls.clickOnRemoteStartButton();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 6);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // stop the car
      guiController.remoteCarControls.clickOnRemoteStopButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Control Stop") && resultText.contains("successful"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.2 Remote stop failure")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote stop failure")
    void remoteStopFailure() {
       guiController.remoteCarControls.clickOnRemoteStopButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
    }


}
