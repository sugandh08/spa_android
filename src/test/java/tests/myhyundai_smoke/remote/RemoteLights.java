package tests.myhyundai_smoke.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Lights Test")
@Feature("Remote Lights")
@DisplayName("Remote Lights")
public class RemoteLights extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.1 Remote lights success shows successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote lights success shows successful message")
    void successfulRemoteLightsShowsPopupMessage() {
        guiController.remoteCarControls.clickOnRemoteFlashLightsButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Lights Only") && resultText.contains("sent to vehicle"));
    }


}
