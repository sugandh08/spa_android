package tests.myhyundai_smoke.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControls;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Remote Vehicle Status Test")
@Feature("Remote Vehicle Status")
@DisplayName("Remote Vehicle Status")
public class RemoteVehicleStatus extends TestController {

    private Profile profile;
    private RemoteCarControls remoteCarControls;
    private Element remoteCommandButton;


    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControls=guiController.gasHomeScreen.remoteCommands;
        remoteCommandButton = guiController.gasHomeScreenGen2Point5.vehicleStatusButton;


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.1 Remote Vehicle  Status Refresh")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Vehicle  Status Refresh")
    void remoteVehicleStatusRefresh() {



        //tap the Vehicle Status button on Vehicle Dashboard
        remoteCommandButton.tap();

        appController.appFunctions.pauseTestExecution(2,5000);

        guiController.vehicleStatusScreenGen2Point5.isPresent();

        //get time of last vehicle Update
        String timeLastUpdate= guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();

        System.out.println("Last Updated Time:" +timeLastUpdate);

        //tap the Vehicle Status refresh icon
        guiController.vehicleStatusScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(4,10000);

        //get the time after status update
        String timeAfterUpdate=guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();
        System.out.println("Updated time:"+ timeAfterUpdate);

        assertTrue(!timeLastUpdate.equals(timeAfterUpdate),"Vehicle Status Refresh failed");


           }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.2 Remote Door Lock-Unlock Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remote Door Lock-Unlock Status Check")
    void remoteVehicleDoorStatusCheck() {

        //Send the Remote Door Lock Request
      guiController.remoteCarControls.clickOnRemoteLockButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESPONSE: " + resultText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();


        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();


        appController.appFunctions.pauseTestExecution(2,5000);


        //Check that vehicle Status shows Door status as Locked
        assertTrue(guiController.vehicleStatusScreenGen2Point5.doorStatus.getTextValue().equals("Locked"));

        appController.appFunctions.pauseTestExecution(1,5000);

        //Press android back button
        appController.appFunctions.tapAndroidBackButton();

        //Send the Remote Door Un Lock Request
       guiController.remoteCarControls.clickOnRemoteUnlockButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText2 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText2.contains("Door Unlock") && resultText2.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();


        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        appController.appFunctions.pauseTestExecution(2,5000);

        //Check that vehicle Status shows Door status as UnLocked
        assertTrue(guiController.vehicleStatusScreenGen2Point5.doorStatus.getTextValue().equals("Unlocked"));

        appController.appFunctions.pauseTestExecution(1,5000);


    }


}
