package tests.myhyundai_smoke.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControls;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Unlock Test")
@Feature("Remote Unlock")
@DisplayName("Remote Unlock")
public class RemoteUnlock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.4.1 Remote unlock request shows request successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote unlock request shows request successful message")
    void remoteUnlockRequestShowsRequestSuccessfulMessage() {
       guiController.remoteCarControls.clickOnRemoteUnlockButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESULT: " + resultText);
        assertTrue(resultText.contains("Door Unlock") && resultText.contains("successful"));
    }

    @Nested
    @DisplayName("7.4.3 Remote unlock with incorrect PIN three times")
    class RemoteUnlockWithIncorrectPinEnteredThreeTimes {
        @BeforeEach
        void sendRemoteUnlockWithIncorrectPinThreeTimes() {
          //  remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.unlockButton);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.4.3.1 Error popup is displayed saying Pin is locked")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify error popup is displayed saying Pin is locked")
        void errorPopupIsDisplayedSayingPinIsLocked() {
            assertTrue(guiController.pinLockedPopup.isPresent());
        }
    }
}
