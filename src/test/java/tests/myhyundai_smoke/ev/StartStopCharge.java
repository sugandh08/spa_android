package tests.myhyundai_smoke.ev;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2ElectricStandard
@Epic("Start Stop Charge")
@Feature("Start Stop Charge")
public class StartStopCharge extends TestController{
    private Profile profile;


    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2OSElectricStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(30);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.3.1 Pressing Start Charge shows remote request success popup")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop Charge")
    @Description("MyHyundai - Verify Pressing Start Charge shows remote request success popup")
    void successfulRemoteChargeStartShowsSuccessMessage() {
        setupTests();
        System.out.println("NOTE: this test only works if the car is plugged in");
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("START REMOTE ELECTRIC CHARGE") && resultText.contains("now charging"));

        guiController.electricHomeScreen.refreshButton.tap();

        appController.appFunctions.pauseTestExecution(1,10000);
        // if it is charging, stop the charge
        if (guiController.electricHomeScreen.startStopChargeButton.getTextValue().equals("Stop Charge")) {
            guiController.electricHomeScreen.startStopChargeButton.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            guiController.remoteCommandResultPopup.isPresent(60);
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.3.2 Pressing Stop Charge shows remote request success popup")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop Charge")
    @Description("MyHyundai - Verify Pressing Stop Charge shows remote request success popup")
    void successfulRemoteChargeStopShowsSuccessMessage() {
        setupTests();
        System.out.println("NOTE: this test only works if the car is plugged in");
        // start charging. need to have it charging in order to stop it.
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("START REMOTE ELECTRIC CHARGE") && resultText.contains("now charging"));
        guiController.remoteCommandResultPopup.okButton.tap();

        guiController.electricHomeScreen.refreshButton.tap();

        appController.appFunctions.pauseTestExecution(1,10000);
        // stop charging
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE STOP CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("STOP REMOTE ELECTRIC CHARGE") && resultText.contains("stopped charging"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.3.3 Pressing Start Charge while vehicle is not plugged in shows notification that vehicle is unplugged")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop Charge")
    @Description("MyHyundai -  Verify Pressing Start Charge while vehicle is not plugged in shows notification that vehicle is unplugged")
    void remoteChargeWhileUnpluggedShowsUnpluggedMessage() {

        profile = appController.myHyundaiProfiles.gen2ElectricStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(30);

        System.out.println("NOTE: this test only works if hte car is NOT plugged in");
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("START REMOTE ELECTRIC CHARGE") && resultText.contains("unplugged"));
    }
}
