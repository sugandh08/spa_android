package tests.myhyundai_smoke.ev;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Start Stop FATC")
@Feature("Start Stop FATC")
public class StartStopFatc extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2OSElectricStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(60);
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.4.1 Remote climate control on shows success message")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop FATC")
    @Description("MyHyundai - Verify Remote climate control on shows success message")
    void remoteClimateControlOnShowsSuccessMessage() {
        // send remote start
       /* guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.electricHomeScreen,
                guiController.electricHomeScreen.homeButton,
                guiController.electricHomeScreen.homeOptions.climateControlButton
        );  commenting for now pushpa  */
//        guiController.climateSettingsScreen.startButton.tap();
        guiController.climateSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE CLIMATE CONTROL START RESPONSE: " + resultText);
        assertTrue(resultText.contains("REMOTE CLIMATE CONTROL START") && resultText.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        appController.appFunctions.pauseTestExecution(6, 10000);

        // once the climate control is running, send the remote_genesis_old stop so that the test ends properly
      /*  guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.electricHomeScreen,
                guiController.electricHomeScreen.homeButton,
                guiController.electricHomeScreen.homeOptions.climateControlButton
        );  commenting for now pushpa  */
        guiController.climateSettingsScreen.stopButton.tap();
        guiController.climateSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.print("REMOTE CLIMATE CONTROL STOP RESPONSE: " + resultText);

        // if the result failed, need to wait out the 10 minute remote_genesis_old start
        if (!resultText.contains("REMOTE CLIMATE CONTROL STOP") && resultText.contains("successful")) {
            appController.appFunctions.pauseTestExecution(6, 10000);
        }
    }
}
