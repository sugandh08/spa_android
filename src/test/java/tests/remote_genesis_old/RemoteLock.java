package tests.remote_genesis_old;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Lock Test")
@Feature("Remote Lock")
@DisplayName("Remote Lock")
public class RemoteLock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreen.lockButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
        // Used as a wait timer for transitioning to remote_genesis_old features screen
        guiController.remoteFeaturesScreen.isPresent();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.3.1 Remote door lock success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify remote_genesis_old door lock success with Gen2")
    void remoteDoorLockSuccessWithGen2() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESPONSE: " + resultText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));
    }


    //BELOW CODE IS COMMENTED AND ADDED IN THE REMOTE UNLOCK TEST CLASS

//    @Nested
//    class PinResetsAfterTest {
//        @AfterEach
//        void resetPin() {
//            remoteTestsHelper.resetPin(incorrectPin);
//        }
//
//        @RepeatedTest(TEST_REPETITIONS)
//        @DisplayName("7.3.2 Remote door lock with incorrect PIN shows incorrect PIN notification")
//        void remoteDoorLockWithIncorrectPinShowsIncorrectPinNotification() {
//            remoteCommandButton.tap();
//            guiController.enterPinScreen.enterPin(incorrectPin);
//
//            // Check if the PIN locked popup appears
//            if(guiController.pinLockedPopup.isPresent()){
//                // Reset PIN
//                resetPin();
//                // Repeating the above steps now that we know the pin was reset
//                guiController.homeScreen.remoteButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//            }
//
//            assertTrue(guiController.incorrectPinPopup.isPresent());
//            if(guiController.incorrectPinPopup.isPresent()){
//                guiController.incorrectPinPopup.okButton.tap();
//            }
//        }
//
//        @Nested
//        @DisplayName("7.3.3 Remote door lock with incorrect PIN three times")
//        class RemoteDoorLockWithIncorrectPinEnteredThreeTimes {
//            @BeforeEach
//            void setPinStateToLocked() {
//                remoteCommandButton.tap();
//                remoteTestsHelper.setPinStateToLocked(incorrectPin);
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.3.3.1 Error popup is displayed saying how to reset PIN")
//            void errorPopupIsDisplayedSayingHowToResetPin() {
//                assertTrue(guiController.pinLockedPopup.isPresent());
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.3.3.2 User is restored access to remote_genesis_old commands after 5 minutes")
//            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
//                guiController.pinLockedPopup.cancelButton.tap();
//                appController.appFunctions.pauseTestExecution(36, 10000);
//
//                // Can't tap lock button, so need to go to another screen
//                // before tapping on lock button
//                guiController.remoteFeaturesScreen.unlockButton.tap();
//                guiController.enterPinScreen.cancelButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//
//                assertTrue(guiController.incorrectPinPopup.isPresent());
//
//                if (guiController.incorrectPinPopup.isPresent()) {
//                    guiController.incorrectPinPopup.okButton.tap();
//                }
//            }
//        }
//    }
}
