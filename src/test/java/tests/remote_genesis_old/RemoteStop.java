package tests.remote_genesis_old;

import config.Profile;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


public class RemoteStop extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreen.stopButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
    }



    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.1 Remote stop failure")
    void remoteStopFailure() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
    }


    //BELOW CODE IS COMMENTED AND ADDED IN THE REMOTE UNLOCK TEST CLASS

//    @Nested
//    class PinResetsAfterTest {
//        @AfterEach
//        void resetPin() {
//            remoteTestsHelper.resetPin(incorrectPin);
//        }
//
//        @RepeatedTest(TEST_REPETITIONS)
//        @DisplayName("7.2.3 Remote stop with incorrect PIN shows incorrect PIN notification")
//        void remoteStopWithIncorrectPinShowsIncorrectPinNotification() {
//            remoteCommandButton.tap();
//            guiController.enterPinScreen.enterPin(incorrectPin);
//
//            // Check if the PIN locked popup appears
//            if(guiController.pinLockedPopup.isPresent()){
//                // Reset PIN
//                resetPin();
//                // Repeating the above steps now that we know the pin was reset
//                guiController.homeScreen.remoteButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//            }
//
//            assertTrue(guiController.incorrectPinPopup.isPresent());
//            if(guiController.incorrectPinPopup.isPresent()){
//                guiController.incorrectPinPopup.okButton.tap();
//            }
//        }
//
//        @Nested
//        @DisplayName("7.2.4 Remote stop with incorrect PIN three times")
//        class RemoteStopWithIncorrectPinEnteredThreeTimes {
//            @BeforeEach
//            void setPinStateToLocked() {
//                remoteCommandButton.tap();
//                remoteTestsHelper.setPinStateToLocked(incorrectPin);
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.2.4.1 Error popup is displayed saying how to reset PIN")
//            void errorPopupIsDisplayedSayingHowToResetPin(){
//                assertTrue(guiController.pinLockedPopup.isPresent());
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.2.4.2 User is restored access to remote_genesis_old commands after 5 minutes")
//            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
//                guiController.pinLockedPopup.cancelButton.tap();
//                appController.appFunctions.pauseTestExecution(36, 10000);
//
//                // Can't tap stop button, so need to go to another screen
//                // before tapping on stop button
//                guiController.remoteFeaturesScreen.unlockButton.tap();
//                guiController.enterPinScreen.cancelButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//
//                assertTrue(guiController.incorrectPinPopup.isPresent());
//
//                if (guiController.incorrectPinPopup.isPresent()) {
//                    guiController.incorrectPinPopup.okButton.tap();
//                }
//            }
//        }
//    }
}
