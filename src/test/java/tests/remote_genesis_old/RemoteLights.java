package tests.remote_genesis_old;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Lights Test")
@Feature("Remote Lights")
@DisplayName("Remote Lights")
public class RemoteLights extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreen.flashLightsButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreen.hornLightOptionsButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.1 Remote lights success shows successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify remote_genesis_old lights success shows successful message")
    void successfulRemoteLightsShowsPopupMessage() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Lights Only") && resultText.contains("sent to vehicle"));
    }


    //BELOW CODE IS COMMENTED AND ADDED IN THE REMOTE UNLOCK TEST CLASS

   /* @Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin);
        }

       @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.2 Remote lights with incorrect PIN shows incorrect PIN notification")
        void remoteLightsWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);

            // Check if the PIN locked popup appears
            if(guiController.pinLockedPopup.isPresent()){
                // Reset PIN
                resetPin();
                // Repeating the above steps now that we know the pin was reset
                guiController.homeScreen.remoteButton.tap();
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
            }

            assertTrue(guiController.incorrectPinPopup.isPresent());
            if(guiController.incorrectPinPopup.isPresent()){
                guiController.incorrectPinPopup.okButton.tap();
            }
        }*/

}
