package tests.remote_genesis_old;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Unlock Test")
@Feature("Remote Unlock")
@DisplayName("Remote Unlock")
public class RemoteUnlock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreen.unlockButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.4.1 Remote unlock request shows request successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote_genesis_old unlock request shows request successful message")
    void remoteUnlockRequestShowsRequestSuccessfulMessage() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText.contains("Door Unlock") && resultText.contains("successful"));
    }

    @Nested
    class PinLockTest {
       /* @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin);
        }*/

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.4.2 Remote unlock with incorrect PIN shows incorrect PIN notification")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Command")
        @Description("Genesis - Verify remote_genesis_old unlock with incorrect PIN shows incorrect PIN notification")
        void remoteUnlockWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);

           /* // Check if the PIN locked popup appears
            if(guiController.pinLockedPopup.isPresent()){
                // Reset PIN
              //  resetPin();
                // Repeating the above steps now that we know the pin was reset
                guiController.homeScreen.remoteButton.tap();
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
            }*/

            assertTrue(guiController.incorrectPinPopup.isPresent());
            if(guiController.incorrectPinPopup.isPresent()){
                guiController.incorrectPinPopup.okButton.tap();
            }
        }

        @Nested
        @DisplayName("7.4.3 Remote unlock with incorrect PIN three times")
        class RemoteUnlockWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void setPinStateToLocked() {
                remoteCommandButton.tap();
                remoteTestsHelper.setPinStateToLocked(incorrectPin);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.4.3.1 Error popup is displayed saying Pin is locked")
            @Severity(SeverityLevel.BLOCKER)
            @Story("Test Remote Command")
            @Description("Genesis - Verify error popup is displayed saying Pin is locked")
            void errorPopupIsDisplayedSayingPinIsLocked() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.4.3.2 User is restored access to remote_genesis_old commands after 5 minutes")
            @Severity(SeverityLevel.BLOCKER)
            @Story("Test Remote Command")
            @Description("Genesis - Verify user is restored access to remote_genesis_old commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.okButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);

                // Can't tap unlock button, so need to go to another screen
                // before tapping on unlock button
                guiController.remoteFeaturesScreen.lockButton.tap();
                guiController.enterPinScreen.cancelButton.tap();
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);

                assertTrue(guiController.incorrectPinPopup.isPresent());

                if (guiController.incorrectPinPopup.isPresent()) {
                    guiController.incorrectPinPopup.okButton.tap();
                }
            }
        }
    }
}
