package tests.remote_genesis_old;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote History Test")
@Feature("Remote History")
@DisplayName("Remote History")
public class RemoteHistory extends TestController {
    private Profile profile;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.isPresent(30);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.1 A remote_genesis_old command is displayed")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a remote_genesis_old command is displayed.")
    void aRemoteCommandIsDisplayed() {
        // send a remote_genesis_old command (we choose remote_genesis_old stop because it will fail)
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreen.stopButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);

        String messageTimeToCompare = remoteTestsHelper.getCurrentHour() + ":" + remoteTestsHelper.getCurrentMinute();
        System.out.println(messageTimeToCompare);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed."));
        guiController.remoteCommandResultPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);

        // check that the remote_genesis_old command is displayed in history
        appController.appFunctions.tapAndroidBackButton();
        guiController.remoteFeaturesScreen.requestHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);
        guiController.requestHistoryScreen.isPresent(10);

        String timeInCurrentMessage = guiController.requestHistoryScreen.getMostRecentCommandTime().substring(0,5);
        //assertTrue(remoteTestsHelper.compareMessageTime(timeInCurrentMessage, messageTimeToCompare));
        assertEquals("PendingOrFail", guiController.requestHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.2 A successful remote_genesis_old command displays 'SUCCESS'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a successful remote_genesis_old command displays 'SUCCESS'")
    void aSuccessfulRemoteCommandDisplaySuccess() {
        // send a remote_genesis_old command and wait for the success message
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreen.hornLightOptionsButton.tap();
        guiController.remoteFeaturesScreen.flashLightsButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Lights Only") && resultText.contains("sent to vehicle"));
        guiController.remoteCommandResultPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);
        // check that the most recent remote_genesis_old command in remote_genesis_old history shows 'success'
        appController.appFunctions.tapAndroidBackButton();
        guiController.remoteFeaturesScreen.requestHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(1,2000);
        guiController.requestHistoryScreen.isPresent(10);
        // Can't check command name in Genesis - Make sure we have the correct command
        //assertEquals("Lights", guiController.requestHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
        assertEquals("SUCCESS", guiController.requestHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.3 A failed remote_genesis_old command displays '?'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a failed remote_genesis_old command displays '?'")
    void aFailedRemoteCommandDisplaysQuestionMark() {
        // send a remote_genesis_old command and wait for the failure message
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreen.stopButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
        guiController.remoteCommandResultPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);
        // check that the most recent remote_genesis_old command in remote_genesis_old history shows failure icon
        appController.appFunctions.tapAndroidBackButton();
        guiController.remoteFeaturesScreen.requestHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(1,2000);
        guiController.requestHistoryScreen.isPresent(10);
        // Can't check command name in Genesis - Make sure we have the correct command
        //assertEquals("Stop", guiController.requestHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
        assertEquals("PendingOrFail", guiController.requestHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.4 A pending remote_genesis_old command displays '?'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a pending remote_genesis_old command displays '?'")
    void aPendingRemoteCommandDisplaysPending() {
        // send a remote_genesis_old command, then check remote_genesis_old history after a couple seconds to give it time to process
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreen.hornLightOptionsButton.tap();
        guiController.remoteFeaturesScreen.flashLightsButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);

        appController.appFunctions.pauseTestExecution(1,5000);
        // check that the most recent remote_genesis_old command in remote_genesis_old history shows "Pending"
        appController.appFunctions.tapAndroidBackButton();
        guiController.remoteFeaturesScreen.requestHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(1,2000);
        guiController.requestHistoryScreen.isPresent(10);
        // Can't check command name in Genesis - Make sure we have the correct command
        //assertEquals("Lights", guiController.requestHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
        assertEquals("PendingOrFail", guiController.requestHistoryScreen.getMostRecentCommandStatus());

        // wait for the remote_genesis_old command to complete
        guiController.remoteCommandResultPopup.isPresent(180);
    }
}
