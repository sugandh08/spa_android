package tests.remote_genesis_old;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Horn And Lights Test")
@Feature("Remote Horn And Lights")
@DisplayName("Remote Horn And Lights")
public class RemoteHornAndLights extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreen.hornAndLightsButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreen.hornLightOptionsButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.1 Remote horn and lights success shows successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify remote_genesis_old horn and lights success shows successful message")
    void successfulRemoteHornAndLightsShowsPopupMessage() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE HORN AND LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Horn & Lights") && resultText.contains("sent to vehicle"));
    }

  /*  @Nested
    class PinResetsAfterTest {
       *//* @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin);
        }*//*

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.6.2 Remote horn and lights with incorrect PIN shows incorrect PIN notification")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Command")
        @Description("Genesis - Verify remote_genesis_old horn and lights with incorrect PIN shows incorrect PIN notification")
        void remoteHornAndLightsWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);

            // Check if the PIN locked popup appears
            if(guiController.pinLockedPopup.isPresent()){
                // Reset PIN
                //resetPin();
                // Repeating the above steps now that we know the pin was reset
                guiController.homeScreen.remoteButton.tap();
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
            }

            assertTrue(guiController.incorrectPinPopup.isPresent());
            if(guiController.incorrectPinPopup.isPresent()){
                guiController.incorrectPinPopup.okButton.tap();
            }
        }
//BELOW CODE IS COMMENTED AND ADDED IN THE REMOTE UNLOCK TEST CLASS

//        @Nested
//        @DisplayName("7.6.3 Remote horn and lights with incorrect PIN three times")
//        class RemoteHornAndLightsWithIncorrectPinEnteredThreeTimes {
//            @BeforeEach
//            void setPinStateToLocked() {
//                guiController.remoteFeaturesScreen.hornLightOptionsButton.tap();
//                remoteCommandButton.tap();
//                remoteTestsHelper.setPinStateToLocked(incorrectPin);
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.6.3.1 Error popup is displayed saying how to reset PIN")
//            void errorPopupIsDisplayedSayingHowToResetPin() {
//                assertTrue(guiController.pinLockedPopup.isPresent());
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.6.3.2 User is restored access to remote_genesis_old commands after 5 minutes")
//            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
//                guiController.pinLockedPopup.cancelButton.tap();
//                appController.appFunctions.pauseTestExecution(36, 10000);
//
//                // Can't tap horn/light button, so need to go to another screen
//                // before tapping on horn/light button
//                guiController.remoteFeaturesScreen.unlockButton.tap();
//                guiController.enterPinScreen.cancelButton.tap();
//                guiController.remoteFeaturesScreen.hornLightOptionsButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//
//                assertTrue(guiController.incorrectPinPopup.isPresent());
//
//                if (guiController.incorrectPinPopup.isPresent()) {
//                    guiController.incorrectPinPopup.okButton.tap();
//                }
//            }
//        }
    }*/
}
