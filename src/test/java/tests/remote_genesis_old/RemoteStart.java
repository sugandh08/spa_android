package tests.remote_genesis_old;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Start Test")
@Feature("Remote Start")
@DisplayName("Remote Start")
public class RemoteStart extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreen.startButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
    }

   @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.1 Remote start success of Gen2 with Climate OFF/Defrost OFF")
   @Severity(SeverityLevel.BLOCKER)
   @Story("Test Remote Command")
   @Description("Genesis - Verify remote_genesis_old start success of Gen2 with Climate OFF/Defrost OFF")
    void remoteStartSuccessOfGen2WithClimateOffDefrostOff() {
        remoteCommandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);

       // wait a second for the engine duration text to update
       appController.appFunctions.pauseTestExecution(1, 1000);

       // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
       int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(false,false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESPONSE: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

       // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
       appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.2 Remote start success of Gen2 with Climate ON/Defrost OFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify remote_genesis_old start success of Gen2 with Climate ON/Defrost OFF")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOff() {
        remoteCommandButton.tap();
           guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true,false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }


//    @Nested
//    class PinResetsAfterTest {
//        @AfterEach
//        void resetPin() {
//            remoteTestsHelper.resetPin(incorrectPin);
//        }
//
//        @RepeatedTest(TEST_REPETITIONS)
//        @DisplayName("7.1.4 Remote start with incorrect PIN shows incorrect PIN notification")
//        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
//            remoteCommandButton.tap();
//            guiController.remoteStartSettingsScreen.submitButton.tap();
//            guiController.enterPinScreen.isPresent(10);
//            guiController.enterPinScreen.enterPin(incorrectPin);
//
//            // Check if the PIN locked popup appears
//            if(guiController.pinLockedPopup.isPresent()){
//                // Reset PIN
//                resetPin();
//                // Repeating the above steps now that we know the pin was reset
//                guiController.homeScreen.remoteButton.tap();
//                remoteCommandButton.tap();
//                guiController.remoteStartSettingsScreen.submitButton.tap();
//                guiController.enterPinScreen.isPresent(10);
//                guiController.enterPinScreen.enterPin(incorrectPin);
//            }
//            assertTrue(guiController.incorrectPinPopup.isPresent());
//            if(guiController.incorrectPinPopup.isPresent()){
//                guiController.incorrectPinPopup.okButton.tap();
//            }
//        }
//
//        @Nested
//        @DisplayName("7.1.5 Remote start with incorrect PIN three times")
//        class RemoteStartWithIncorrectPinEnteredThreeTimes {
//            @BeforeEach
//            void setPinStateToLocked() {
//                remoteCommandButton.tap();
//                guiController.remoteStartSettingsScreen.submitButton.tap();
//                remoteTestsHelper.setPinStateToLocked(incorrectPin);
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.1.5.1 Error popup is displayed saying how to reset PIN")
//            void errorPopupIsDisplayedSayingHowToResetPin() {
//                assertTrue(guiController.pinLockedPopup.isPresent());
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.1.5.2 User is restored access to remote_genesis_old commands after 5 minutes")
//            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
//                guiController.pinLockedPopup.cancelButton.tap();
//                appController.appFunctions.pauseTestExecution(36, 10000);
//
//                // Can't tap start button, so need to go to another screen
//                // before tapping on start button
//                guiController.remoteFeaturesScreen.unlockButton.tap();
//                guiController.enterPinScreen.cancelButton.tap();
//                remoteCommandButton.tap();
//                guiController.remoteStartSettingsScreen.submitButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//
//                assertTrue(guiController.incorrectPinPopup.isPresent());
//
//                if (guiController.incorrectPinPopup.isPresent()) {
//                    guiController.incorrectPinPopup.okButton.tap();
//                }
//            }
//        }
//    }
}
