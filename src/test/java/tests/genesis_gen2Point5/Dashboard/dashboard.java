package tests.genesis_gen2Point5.Dashboard;

import config.Profile;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class dashboard extends TestController {

    Profile profile;
    private Element remoteCommandButton;
    private Object String;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.refreshButton;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 User can see Unlock button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Unlock button on Dashboard")
    void UnlockbuttonOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.unlockButton.elementExists(), "Verify Unlock button is present in dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 User can see lock button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see lock button on Dashboard")
    void lockbuttonOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 User can see Quick Start button on Dashboard"
            + "Verify user can see Remote Start button in the dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Quick Start button on Dashboard")
    void StartbuttonOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.startbutton.elementExists(), "Verify Start button is prresent in dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 User can see add event button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see add event button on Dashboard")
    void addeventOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.addevent.elementExists(), "Verify add event present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.5 User can see POI on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see POI on Dashboard")
    void POIOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.elementExists(), "Verify POI present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.6 User can see Weather icon on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Weather icon on Dashboard")
    void WeathericonOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.weathericon.elementExists(), "Verify Weather icon present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.7 User can see Location on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Location on Dashboard")
    void LocationOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.location.elementExists(), "Verify Location present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 User can see Location on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Location on Dashboard")
    void WeatherInfoandTempOnDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.weatherinfo.elementExists(), "Verify Weather info present in the dashboard screen");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.weatherTemp.elementExists(), "Verify Temperature present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.9 Verify user can see Remote Features screen when click on remote Start button in the home screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Remote Features screen when click on remote Start button in the home screen")
    void Remotefeaturescreen() {
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.remotefeaturescreen.elementExists(), "Verified Remote preset screen is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.10 User can see a list of closest POI in the search field when enter" +
            "Verify user can search for a POI ")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see a list of closest POI in the search field when enter")
    void ClosestPOIinSearchField() {
        String placename = "Los Angeles";
        String zipcode = "90002";
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.tap();
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.enterText(placename);
        String POI = guiController.getTextUsingXpath("(//*[@resource-id='com.stationdm.genesis:id/tv_placename'])[1]");
        assertEquals(placename, POI, "Verified placename equals to zipcode searched");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.11 Verify the Contact us link on Message center to redirect to Contact us page on CWP")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify the Contact us link on Message center to redirect to Contact us page on CWP")
    void RedirecttoContactusPageFromDashboard() {
        //Click on MessageCentre icon in Dashboard
        guiController.remoteFeaturesScreenGen2Point5.messageCentre.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        //Click on Contactus Present in MessageCentre
        guiController.remoteFeaturesScreenGen2Point5.contactus.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.ContactusPage.elementExists(), "Verified Contact us Page on CWP ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.12 Verify user can see a prompt message with Cancel and Add Event button when click on it")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - User can see a prompt message with Cancel and Add Event button when click on it")
    void AddeventPromptMessagewithCancelandAddeventButton() {
        guiController.remoteFeaturesScreenGen2Point5.addevent.tap();
        guiController.remoteFeaturesScreenGen2Point5.gotitbutton.tap();
        guiController.remoteFeaturesScreenGen2Point5.addevent.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Addeventtitle.elementExists());
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.addeventPromptmsg.elementExists(), "Verified Addevent message is displayed");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AddeventButton.elementExists(), "Add button present");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.CancelButton.elementExists(), "Cancel button present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.13 Verify user gets prompt message on search of POI in the home screen when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on search of POI in the home screen when deny permissions")
    void PromptmessageonPOIsearchDashboard() {
        String placename = "Los Angeles";
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.tap();
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.enterText(placename);
        guiController.remoteFeaturesScreenGen2Point5.POIsearchlisttitle.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.14 Verify user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    void PromptmessageonPOIsearchMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.TapPOISearchinmaps();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.15 Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    void PromptmessageonCarFinderMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.CarFinder.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.16 Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    void PromptmessageonFavoritesMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.TapFavoritesinmaps();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.17 Verify user gets prompt message on tap of Dealer Locator in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Dealer Locator in the Maps fly options when deny permissions")
    void PromptmessageonDealerLocatorMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.DealerLocator.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.18 Verify user gets prompt message on tap of Charge station in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Charge stations in the Maps fly options when deny permissions")
    void PromptmessageonChargestationMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.ChargeFuelstations.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.19 Verify user gets prompt message on tap of Fuel stations in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Fuel stations in the Maps fly options when deny permissions")
    void PromptmessageonFuelstationMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.ChargeFuelstations.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.20 Verify user gets location permission prompt message on tap of GPS icon in the map")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets location permission prompt message on tap of GPS icon in the map")
    void PromptmessageongpsiconunderMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.Gpsicon.tap();
        System.out.println(guiController.remoteFeaturesScreenGen2Point5.Gpsicon.elementExists());
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.21 Verify user can see the Quick Start Tutorial on home screen - first time user")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Quick Start Tutorial")
    @Description("Genesis - Verify user can see the Quick Start Tutorial on home screen - first time user")
    void QuickstartTutorial() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Quickstarttutorial.elementExists(), "Verified Quick Start tutorial screen is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.21 Verify it navigates to Settings screen by default when tap on Go to Settings option in the Quick Start Tutorial")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Quick Start Tutorial")
    @Description("Genesis - Verify it navigates to Settings screen by default when tap on Go to Settings option in the Quick Start Tutorial")
    void gotosettings() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Quickstarttutorial.elementExists(), "Verified Quick Start tutorial screen is displayed");
        guiController.remoteFeaturesScreenGen2Point5.Gotosettings.tap();
        assertTrue(guiController.menuScreen.Settingpage.elementExists(), "Verified Setting page is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.22 Verify by default - Quick start toggle button shows OFF")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Quick Start Tutorial")
    @Description("Genesis - Verify by default - Quick start toggle button shows OFF")
    void QuickStartToggleOff() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Quickstarttutorial.elementExists(), "Verified Quick Start tutorial screen is displayed");
        guiController.remoteFeaturesScreenGen2Point5.Gotosettings.tap();
        appController.appFunctions.pauseTestExecution(2, 2000);
        assertTrue(guiController.menuScreen.Settingpage.elementExists(), "Verified Setting page is displayed");
        assertFalse(guiController.menuScreen.Quickstarttoggle.getCheckedValue(), "Verified quick start toggle button is OFF by default");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.23 Regular press on Remote start button")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Dashboard")
    @Description("MyHyundai - Regular press on Remote start button")
    void remotestartpinscreen() {
        guiController.homeScreen.menuButton.tap();
        guiController.menuScreen.settingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        if (!guiController.menuScreen.Quickstarttoggle.getCheckedValue()) {
            guiController.menuScreen.Quickstarttoggle.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.homeScreen.hamburgerbutton.tap();
        guiController.menuScreen.Menubackheadderbutton.tap();
        // guiController.menuScreen.backbutton.tap();
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        assertTrue(guiController.enterPinScreen.enterPINText.elementExists(), "Verified PIN screen is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.24 Verify quick start command when quick start toggle is on and by no preset is set as default.")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Dashboard")
    @Description("MyHyundai - Verify quick start command when quick start toggle is on and by no preset is set as default.")
    void remotelocksuccessfuldashboard() {
        guiController.homeScreen.menuButton.tap();
        guiController.menuScreen.settingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        if (!guiController.menuScreen.Quickstarttoggle.getCheckedValue()) {
            guiController.menuScreen.Quickstarttoggle.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.homeScreen.hamburgerbutton.tap();
        guiController.menuScreen.Menubackheadderbutton.tap();
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        assertTrue(guiController.enterPinScreen.enterPINText.elementExists(), "Verified PIN screen is displayed");
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"), "Verified the successfull popup");
        appController.appFunctions.pauseTestExecution(1, 2000);


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.25 Tap on Quick Unlock button with success popup")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Tap on Quick Unlock  button")
    void quickunlockdashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.unlockButton.elementExists(), "Verify Unlock button is present in dashboard");
        guiController.remoteFeaturesScreenGen2Point5.unlockButton.tap();
        assertTrue(guiController.enterPinScreen.enterPINText.elementExists(), "Verified PIN screen is displayed");
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText.contains("Remote Door Unlock") && resultText.contains("successful"));

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.26 Verify the pop up when Previous command in progress.")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify the pop up when Previous command in progress.")
    void RemoteActioninprogress() {
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Remoteactioninprogress.elementExists(), "Verified Remote action in progress popup is displayed");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Remoteactioninprogresscontent.elementExists(), "Verified A prior request is still processing.Please wait and try again later message displayed  ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.DismissButton.elementExists(), "Verified Dismiss button is displayed in popup");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.27 Verify the pop up appeared in vehicle status after tapping on warning icon next to DOORS.")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify the pop up appeared in vehicle status after tapping on warning icon next to DOORS.")
    void doorwarningicon() {
        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        //doorstatus & Locked include's the warning icon
        guiController.remoteFeaturesScreenGen2Point5.doorsStatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.Locked.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorwarning.elementExists(), "Verified warning popup is displayed");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorwarningtxt.getTextValue().contains("If a door was unlocked or opened before the vehicle was turned off"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.28 Verify user can see Service Valet option in the Schedule Service")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Valet in Schedule Service ")
    @Description("Genesis - Verify user can see Service Valet option in the Schedule Service")
    void valetschedulescreen() {
        String zipCode = "20852";
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.dealerLocatorButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.mapScreen.detailArrow.tap();
        if (guiController.mapScreen.dealerLocation.getTextValue().contains(zipCode)) {
            System.out.println("if condition");
            appController.appFunctions.tapAndroidBackButton();
            appController.appFunctions.tapAndroidBackButton();
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
        } else {
            System.out.println("else condition");
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(1, 4000);
            guiController.mapScreen.detailArrow.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.mapScreen.dealerLocation.elementExists());
            guiController.mapScreen.setAsPreferredIcon.tap();
            guiController.remoteFeaturesScreenGen2Point5.Homescreenbutton.tap();
           // assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
          //  assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.29 Verify user can see Schedule Valet Pick-up screen when click on  Schedule valet Pick-up link")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Valet in Schedule Service ")
    @Description("Genesis - Verify user can see Schedule Valet Pick-up screen when click on  Schedule valet Pick-up link")
    void ScheduleValetPickUpScreen() {
        String zipCode = "22301";
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.dealerLocatorButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.mapScreen.detailArrow.tap();
        if (guiController.mapScreen.dealerLocation.getTextValue().contains(zipCode)) {
            System.out.println("if condition");
            appController.appFunctions.tapAndroidBackButton();
            appController.appFunctions.tapAndroidBackButton();
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
            guiController.scheduleServiceScreen.schedulevaletpickup.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.scheduleServiceScreen.scheduleservicevaletscreen.elementExists(), "Verified Schedule service valet screen is displayed");
        } else {
            System.out.println("else condition");
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(1, 4000);
            guiController.mapScreen.detailArrow.tap();
          //  assertTrue(guiController.mapScreen.dealerLocation.getTextValue().contains(zipCode));
            guiController.mapScreen.setAsPreferredIcon.tap();
            guiController.remoteFeaturesScreenGen2Point5.Homescreenbutton.tap();
          //  assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
         //   assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
            guiController.scheduleServiceScreen.schedulevaletpickup.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
         //   assertTrue(guiController.scheduleServiceScreen.scheduleservicevaletscreen.elementExists(), "Verified Schedule service valet screen is displayed");
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.30 Verify it closes the pop up message when tap on cancel link")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Valet in Schedule Service ")
    @Description("Genesis - Verify it closes the pop up message when tap on cancel link")
    void valetscreencancelbutton() {
        String zipCode = "20852";
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.dealerLocatorButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.mapScreen.detailArrow.tap();
        if (guiController.mapScreen.dealerLocation.getTextValue().contains(zipCode)) {
            System.out.println("if condition");
            appController.appFunctions.tapAndroidBackButton();
            appController.appFunctions.tapAndroidBackButton();
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
            guiController.mapScreen.Cancel.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
        } else {
            System.out.println("else condition");
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(1, 4000);
            guiController.mapScreen.detailArrow.tap();
          //  assertTrue(guiController.mapScreen.dealerLocation.getTextValue().contains(zipCode));
            guiController.mapScreen.setAsPreferredIcon.tap();
            guiController.remoteFeaturesScreenGen2Point5.Homescreenbutton.tap();
           // assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
          //  assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
            guiController.mapScreen.Cancel.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
          //  assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.31 Verify Schedule drop-off screen displays when click on Schedule drop-off link")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Drop off in Schedule Service ")
    @Description("Genesis -Verify Schedule drop-off screen displays when click on Schedule drop-off link")
    void ScheduleDropoffScreen() {
        String zipCode = "22301";
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.dealerLocatorButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.mapScreen.detailArrow.tap();
        if (guiController.mapScreen.dealerLocation.getTextValue().contains(zipCode)) {
            System.out.println("if condition");
            appController.appFunctions.tapAndroidBackButton();
            appController.appFunctions.tapAndroidBackButton();
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule drop off option is displayed");
            guiController.scheduleServiceScreen.scheduledropoff.tap();
            appController.appFunctions.pauseTestExecution(3, 6000);
            assertTrue(guiController.scheduleServiceScreen.isScheduleservicescreenpresent(), "Verified Schedule service drop off screen is displayed");
        } else {
            System.out.println("else condition");
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(1, 4000);
            guiController.mapScreen.detailArrow.tap();
            guiController.mapScreen.dealerLocation.elementExists();
            guiController.mapScreen.setAsPreferredIcon.tap();
            guiController.remoteFeaturesScreenGen2Point5.Homescreenbutton.tap();
           // assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
            guiController.remoteFeaturesScreenGen2Point5.Schedule.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
          //  assertTrue(guiController.scheduleServiceScreen.schedulevaletpickup.elementExists(), "Verified schedule valet screen is displayed");
            guiController.scheduleServiceScreen.scheduledropoff.tap();
            appController.appFunctions.pauseTestExecution(3, 6000);
          //  assertTrue(guiController.scheduleServiceScreen.isScheduleservicescreenpresent(), "Verified Schedule service valet screen is displayed");
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.32 Verify remote presets options showing correctly via Event remote start button")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Remote Start event ")
    @Description("Genesis -Verify remote presets options showing correctly via Event remote start button")
    void remotestartpresetthroughevent() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.eventname.elementExists(), "Verify Event name exist in dashboard");
        guiController.remoteFeaturesScreenGen2Point5.eventname.tap();
        guiController.remoteFeaturesScreenGen2Point5.nextbuttonevent.tap();
        guiController.remoteFeaturesScreenGen2Point5.Eventnext.tap();
        guiController.termsAndConditionsPopup.exitButton.tap(10);
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.remoteFeaturesScreenGen2Point5.Remotestartevent.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.remotePresetsScreen.isPresent();
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(4));
        assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.33 Tap on Lock  button")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Tap on Lock  button")
    void lockbuttonDashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(), "Verify lock button is present in dashboard");
        guiController.remoteFeaturesScreenGen2Point5.lockButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESPONSE: " + resultText);
        assertTrue(resultText.contains("Remote Door Lock") && resultText.contains("successful"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.34 Verify SVM Doors & Trunk Closed text visible on Android")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify SVM Doors & Trunk Closed text visible on Android")
    void DoorsandTrunkclosed() {
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.SVMbar.tap();
        appController.appFunctions.pauseTestExecution(2,5000);
        assertTrue(guiController.mapScreen.DoorsandTrunkClosed.elementExists(),"Verify Doors & Trunk are Closed visible");
    }
    
    
}
