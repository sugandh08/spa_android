package tests.genesis_gen2Point5.userProfile_Gen2Point5;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;
import views.genesis.screens.HomeScreen;
import java.text.ParseException;
import java.util.Random;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayName("User Profile Information")
public class UserProfileInformation extends TestController {

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);
    String email = "test_user" + randomInt + "@mailnesia.com";
    String fName = "Test" + randomInt;
    String lName = "User" + randomInt;
    String addressLine1= randomInt + " Talbert Avenue" ;


    private void loginAndOpenUserProfile(Profile profile) {


        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);


        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);

        homeScreen.menuButton.tap();


        guiController.menuScreen.profileButton.tap();


        guiController.profileScreen_gen2Point5.isPresent(30);


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.1 Invite Secondary Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Invite Secondary Driver")
    @Description("Genesis - Verify inviting Secondary Driver")
    void inviteSecondaryDriver() throws ParseException, InterruptedException {

        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.secondaryDriverTextView.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        if (guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver1.elementExists()) {
            guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver1.tap();
        } else {
            guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver2.tap();
        }

        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.secondaryDriverInvitationScreen_gen2Point5.isPresent(), "Secondary driver invitation screen not present");
        guiController.secondaryDriverInvitationScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + fName + " " + lName + "']").contains(fName), "Secondary driver not added");

        //    assertTrue(guiController.secondaryDriverScreen_gen2Point5.getXpathOfSecondaryDriverNameText(fName,lName).contains(fName), "Secondary driver not added");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.2 Delete Secondary Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Delete Secondary Driver")
    @Description("Genesis - Verify Deleting Secondary Driver")
    void deleteSecondaryDriver() throws ParseException, InterruptedException {

        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.secondaryDriverTextView.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        if (guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver1.elementExists()) {
            guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver1.tap();
        } else {
            guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver2.tap();
        }

        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.secondaryDriverInvitationScreen_gen2Point5.isPresent(), "Secondary driver invitation screen not present");
        guiController.secondaryDriverInvitationScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + fName + " " + lName + "']").contains(fName), "Secondary driver not added");

        //    assertTrue(guiController.secondaryDriverScreen_gen2Point5.getXpathOfSecondaryDriverNameText(fName,lName).contains(fName), "Secondary driver not added");

        guiController.secondaryDriverScreen_gen2Point5.clickOnSecondaryDriverDeleteButton(fName, lName);
        appController.appFunctions.pauseTestExecution(1, 5000);

        // assertFalse(guiController.getTextUsingXpath(guiController.secondaryDriverScreen_gen2Point5.getXpathOfSecondaryDriverNameText(fName,lName)).contains(fName),"Secondary driver not deleted.");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.3 Edit Secondary Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Secondary Driver Edit Functionality")
    @Description("Genesis - Secondary Driver Edit Functionality")
    void editInformationSecondaryDriver() throws ParseException {
        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.secondaryDriverTextView.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        if (guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver1.elementExists()) {
            guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver1.tap();
        } else {
            guiController.secondaryDriverScreen_gen2Point5.addSecondaryDriver2.tap();
        }

        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.secondaryDriverInvitationScreen_gen2Point5.isPresent(), "Secondary driver invitation screen not present");
        guiController.secondaryDriverInvitationScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + fName + " " + lName + "']").contains(fName), "Secondary driver not added");
        guiController.secondaryDriverScreen_gen2Point5.clickOnDriverEditButton(fName, lName);

        assertTrue(guiController.editSecondaryDriverInformationScreen_gen2Point5.emailText.getTextValue().equals(email), "Secondary driver Edit Screen is not present");
        guiController.editSecondaryDriverInformationScreen_gen2Point5.secondaryDriverEditInformation();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.4 Edit Vehicle Information NickName")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Edit NickName of Vehicle Information")
    @Description("Genesis - Edit Vehicle Information NickName")
    void editVehicleInformation() {

        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.clickOnVehicleInformationEditButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.editVehicleNickNameScreen_gen2Point5.nickNameField.tap();
        for (int i = 0; i < 4; i++) {
            appController.appFunctions.tapAndroidKeypadBackSpace();
        }
        appController.appFunctions.tapAndroidBackButton();
        String vehicleNickNameText = guiController.editVehicleNickNameScreen_gen2Point5.nickNameField.getTextValue();
        guiController.editVehicleNickNameScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(vehicleNickNameText.equals(guiController.profileScreen_gen2Point5.vehicleInformationNickNameText.getTextValue()), "Vehicle nickname is not edited");

        //revert nickname changes
        guiController.profileScreen_gen2Point5.clickOnVehicleInformationEditButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.editVehicleNickNameScreen_gen2Point5.nickNameField.tap();
        guiController.editVehicleNickNameScreen_gen2Point5.nickNameField.enterText("2021");
        appController.appFunctions.tapAndroidBackButton();
        guiController.editVehicleNickNameScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.5 Edit Personal Information")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Edit Personal Information")
    @Description("Genesis - Edit Personal Information")
    void editPersonalInformation() {
        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
        if (guiController.profileScreen_gen2Point5.profileInformationText.elementExists()) {
            guiController.profileScreen_gen2Point5.personalInformationEditButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }
        assertTrue(guiController.editPersonalInformationScreen_gen2Point5.subTitleTextView.getTextValue().equals("EDIT PERSONAL INFORMATION"), "Personal information Screen not displayed");

        guiController.editPersonalInformationScreen_gen2Point5.updatePersonalInformation(fName, lName);
        guiController.editPersonalInformationScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.profileScreen_gen2Point5.profileFirstNameText.getTextValue().equals(fName), "Profile First name is not updated");
        assertTrue(guiController.profileScreen_gen2Point5.profileLastNameText.getTextValue().equals(lName), "Profile Last name is not updated");

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.6 Edit Contact Information")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Edit Contact Information")
    @Description("Genesis - Edit Contact Information")
    void editContactInformation() {
        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.swipe.up(4);
        if(guiController.profileScreen_gen2Point5.contactInformationText.elementExists())
        {
            guiController.profileScreen_gen2Point5.contactInformationEditButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.editContactInformationScreen_gen2Point5.subTitleTextView.getTextValue().equals("EDIT CONTACT INFORMATION"),"Edit Contact Information Screen is not present");
        guiController.editContactInformationScreen_gen2Point5.updateContactInformation(addressLine1);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.profileScreen_gen2Point5.addressContactInformation.getTextValue().contains(addressLine1),"Contact information is not Edited successfully");
        appController.appFunctions.pauseTestExecution(1, 3000);
  }
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.7 Edit Emergency Contact Information")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Edit Emergency Contact Information")
    @Description("Genesis - Edit Emergency Contact Information")
    void editEmergencyContactInformation() {
        String relationship = "Colleague";

        Profile profile = appController.genesisProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(4);
        if (guiController.profileScreen_gen2Point5.emergencyContactInformationText.elementExists())
        {
            guiController.profileScreen_gen2Point5.emergencyContactInformationEditButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
        }
        assertTrue(guiController.editEmergencyInformationScreen_gen2Point5.subTitleTextView.getTextValue().equals("EDIT EMERGENCY INFORMATION"),"Edit Emergency Information is not present");

        guiController.editEmergencyInformationScreen_gen2Point5.updateEmergencyInformation(fName,lName,relationship,email);
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.profileScreen_gen2Point5.emergencyContactInformationFirstName.getTextValue().equals(fName),"Emergency Information is not updated Successfully" );

    }

}
