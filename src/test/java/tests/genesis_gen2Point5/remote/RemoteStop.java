package tests.genesis_gen2Point5.remote;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


public class RemoteStop extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2Point5;
        //incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        //remoteCommandButton = guiController.remoteStartSettingsScreenGen2Point5.stopButton;


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.1 Remote stop success")
    void remoteStopSuccess() {
        int PresetIndex = 2;
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 01);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.SUBMIT.tap();
        guiController.remotePresetsScreen.clickOnPresetStartButton(PresetIndex);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"), "Verified the successfull popup");
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
        guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
        guiController.remoteFeaturesScreenGen2Point5.stop.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultStop = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultStop);
        assertTrue(resultStop.contains("Remote Control Stop") && resultStop.contains("successful"), "Verified the successfull popup");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.2 Remote stop failure")
    void remoteStopFailure() {
        guiController.remoteFeaturesScreenGen2Point5.stop.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
    }



    //BELOW CODE IS COMMENTED AND ADDED IN THE REMOTE UNLOCK TEST CLASS

//    @Nested
//    class PinResetsAfterTest {
//        @AfterEach
//        void resetPin() {
//            remoteTestsHelper.resetPin(incorrectPin);
//        }
//
//        @RepeatedTest(TEST_REPETITIONS)
//        @DisplayName("7.2.3 Remote stop with incorrect PIN shows incorrect PIN notification")
//        void remoteStopWithIncorrectPinShowsIncorrectPinNotification() {
//            remoteCommandButton.tap();
//            guiController.enterPinScreen.enterPin(incorrectPin);
//
//            // Check if the PIN locked popup appears
//            if(guiController.pinLockedPopup.isPresent()){
//                // Reset PIN
//                resetPin();
//                // Repeating the above steps now that we know the pin was reset
//                guiController.homeScreen.remoteButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//            }
//
//            assertTrue(guiController.incorrectPinPopup.isPresent());
//            if(guiController.incorrectPinPopup.isPresent()){
//                guiController.incorrectPinPopup.okButton.tap();
//            }
//        }
//
//        @Nested
//        @DisplayName("7.2.4 Remote stop with incorrect PIN three times")
//        class RemoteStopWithIncorrectPinEnteredThreeTimes {
//            @BeforeEach
//            void setPinStateToLocked() {
//                remoteCommandButton.tap();
//                remoteTestsHelper.setPinStateToLocked(incorrectPin);
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.2.4.1 Error popup is displayed saying how to reset PIN")
//            void errorPopupIsDisplayedSayingHowToResetPin(){
//                assertTrue(guiController.pinLockedPopup.isPresent());
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.2.4.2 User is restored access to remote_genesis_old commands after 5 minutes")
//            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
//                guiController.pinLockedPopup.cancelButton.tap();
//                appController.appFunctions.pauseTestExecution(36, 10000);
//
//                // Can't tap stop button, so need to go to another screen
//                // before tapping on stop button
//                guiController.remoteFeaturesScreen.unlockButton.tap();
//                guiController.enterPinScreen.cancelButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//
//                assertTrue(guiController.incorrectPinPopup.isPresent());
//
//                if (guiController.incorrectPinPopup.isPresent()) {
//                    guiController.incorrectPinPopup.okButton.tap();
//                }
//            }
//        }
//    }
}
