package tests.genesis_gen2Point5.remote;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RemoteVehicleStatus extends TestController {

    Profile profile;
    private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;

        remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.refreshButton;


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
        guiController.homeScreen.remoteButton.tap();

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.1 Remote Vehicle Status Refresh")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify Remote Vehicle Status Refresh")
    void successfulRemoteLightsShowsPopupMessage() {

        //tap the Vehicle Status button on Remote Dashboard
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        appController.appFunctions.pauseTestExecution(1, 5000);

        //get the time after status update
        String timeAfterUpdate = guiController.remoteFeaturesScreenGen2Point5.lastUpdateDateTextView.getTextValue();
        System.out.println("Updated time:" + timeAfterUpdate);

    }

  /*  @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.2 Remote Vehicle Status Fluid Level Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Remote Vehicle Status Fluid Level Check")
    void remoteVehicleStatusFluidLevelCheck() {


        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        appController.appFunctions.pauseTestExecution(1,5000);

        guiController.remoteFeaturesScreenGen2Point5.swipe.swipeScrollFromMiddleDown(guiController.remoteFeaturesScreenGen2Point5.light_Panel_XPATH,10);


        //check the fluid levels status
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isOilStatusOk(),"Oil Status is not OK");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isBrakeStatusOk(),"Brake Status is not OK");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isWasherStatusOk(),"Washer Status is not OK");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isSmartKeyBatteryStatusOk(),"Smart Key Battery Status is not OK");

    }*/


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.3 Remote Door Lock-Unlock Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Remote Door Lock-Unlock Status Check")
    void remoteVehicleDoorStatusCheck() {

        //Send the Remote Door Lock Request
        guiController.remoteFeaturesScreenGen2Point5.lock.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESPONSE: " + resultText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();

        guiController.homeScreen.remoteButton.tap();
        //tap the Vehicle Status button on Remote Dashboard
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(2, 10000);

        //Check that vehicle Status shows Door status as Locked
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorsStatus.getTextValue().equals("LOCKED"));

        appController.appFunctions.tapAndroidBackButton();

        //Send the Remote DoorUn Lock Request
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.unlock.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText2 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText2.contains("Door Unlock") && resultText2.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        //tap the Vehicle Status button on Remote Dashboard
        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(2, 10000);

        //Check that vehicle Status shows Door status as UnLocked
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorsStatus.getTextValue().equals("UNLOCKED"));
        appController.appFunctions.pauseTestExecution(1, 5000);
    }


   /* @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.4 Remote Climate  Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Remote Climate  Status Check")
    void remoteClimateStatusCheck() {

        //Send the Remote Start Request
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.startButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);


        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true,true,true,true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.ventSeat_1.tap();
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);

        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESPONSE: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();

        //tap Android Back button
        appController.appFunctions.tapAndroidBackButton();
        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        String VehicleTemp=guiController.remoteFeaturesScreenGen2Point5.climateStatus.getTextValue();
        //Check that vehicle Status shows temp
        assertTrue(VehicleTemp.contains("72"));
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.frontDefrostStatus.equals("ON"));

        appController.appFunctions.pauseTestExecution(1,5000);

        //Send the Remote Stop Request
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.stopButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText1);
        assertTrue(resultText.contains("Control Stop") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();

        //tap Android Back button
        appController.appFunctions.tapAndroidBackButton();
        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        //Check that vehicle Status shows temperature as OFF
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.climateStatus.equals("OFF"));
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.frontDefrostStatus.equals("OFF"));

        appController.appFunctions.pauseTestExecution(1,5000);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.5 Remote Engine On-Off Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Remote Engine On-Off  Status Check")
    void remoteEngineStatusCheck() {

        //Send the Remote Start Request
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.startButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);


        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true,true,true,true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.ventSeat_1.tap();
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);

        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESPONSE: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();

        //tap Android Back button
        appController.appFunctions.tapAndroidBackButton();
        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        //Check that vehicle Status shows Engine status as ON
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.engineStatus.equals("ON"));

        appController.appFunctions.pauseTestExecution(1,5000);

        //Send the Remote Stop Request
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.stopButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText1);
        assertTrue(resultText.contains("Control Stop") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();

        //tap Android Back button
        appController.appFunctions.tapAndroidBackButton();
        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        //Check that vehicle Status shows Engine Status as Off
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorsStatus.equals("OFF"));

        appController.appFunctions.pauseTestExecution(1,5000);

    }

*/

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.6 Verify the redesigned vehicle status page(Collapse view)")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Status")
    @Description("Genesis - Verify the redesigned vehicle status page(Collapse view)")
    void vehiclestatuspageCollapseview()
    {
        //Click on Vehicle status present in remote actions
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.swipe.up(2);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Engine.elementExists(), "Engine present in vehicle status page");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.ClimateIcon.elementExists(), "Climate Icon present in vehicle status page");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.DoorIcon.elementExists(), "Door Icon present in vehicle status page");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.7 Verify the redesigned Vehicle Status Page Expand View Under Lights")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Status")
    @Description("Genesis - Verify the redesigned Vehicle Status Page Expand View Under Lights")
    void VehicleStatusPageExpandViewUnderLights()
    {
        //Click on Vehicle status present in remote actions
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.swipe.up(1);
       // assertTrue(guiController.remoteFeaturesScreenGen2Point5.Lights.getTextValue().contains("LIGHTS"), "Lights present in vehicle status page");
        //Tap on lights expand button
        guiController.remoteFeaturesScreenGen2Point5.Lightexpandbutton.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Lightsexpandtext.getTextValue().contains("All lights operational"), "Verify All lights operational is displayed under lights");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.8 Verify the redesigned Vehicle Status Page Expand View Under Fluid Levels")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Status")
    @Description("Genesis - Verify the redesigned Vehicle Status Page Expand View Under Fluid Levels")
    void VehicleStatusPageExpandViewUnderFluidLevels()
    {
        //Click on Vehicle status present in remote actions
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.swipe.up(2);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.fluidlevels.getTextValue().contains("FLUID LEVELS"), "Fluid levels present in vehicle status page");
        //Tap on fluid level expand button
        guiController.remoteFeaturesScreenGen2Point5.fluidlevelexpandbutton.tap();
        guiController.remoteFeaturesScreenGen2Point5.swipe.up(1);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isOilStatusOk(),"Verify Oil:OK with icon is displayed ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isBrakeStatusOk(),"Verify Brake:OK with icon is displayed");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isWasherStatusOk(),"Verify Washer:OK with icon is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.9 Verify the redesigned Vehicle Status Page Expand View Under Smart Key Battery")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Status")
    @Description("Genesis - Verify the redesigned Vehicle Status Page Expand View Under Smart Key Battery")
    void VehicleStatusPageExpandViewUnderSmartKeyBattery()
    {
        //Click on Vehicle status present in remote actions
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.swipe.up(2);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.smartkey.getTextValue().contains("SMART KEY"), "Smart key present in vehicle status page");
        //Tap on Smart key expand button
        guiController.remoteFeaturesScreenGen2Point5.smartkeyexpandbutton.tap();
        guiController.remoteFeaturesScreenGen2Point5.swipe.up(1);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.isSmartKeyBatteryStatusOk(),"Verify Battery OK with icon is displayed");
    }

}
