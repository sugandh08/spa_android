package tests.genesis_gen2Point5.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Unlock Test")
@Feature("Remote Unlock")
@DisplayName("Remote Unlock")
public class RemoteUnlock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;


        //remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.unlockButton;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.unlock.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.4.1 Remote unlock request shows request successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote unlock request shows request successful message")
    void remoteUnlockRequestShowsRequestSuccessfulMessage() {
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText.contains("Door Unlock") && resultText.contains("successful"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.4.1 Verify Remote Unlock with invalid PIN.")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Remote Unlock with invalid PIN.")
    void remoteUnlockRequestincorrectpin() {
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        guiController.enterPinScreen.enterPin(incorrectPin);
        assertTrue(guiController.remoteCommandResultPopup.messageTextView.getTextValue(20).contains("Incorrect PIN"),"Verified entered the incorrect pin");


    }

}
