package tests.genesis_gen2Point5.remote;

import com.sun.java.accessibility.util.GUIInitializedListener;
import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote History Test")
@Feature("Remote History")
@DisplayName("Remote History")
public class RemoteHistory extends TestController {
    private Profile profile;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.isPresent(30);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.1 A remote command is displayed")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a remote command is displayed.")
    void aRemoteCommandIsDisplayed() {

        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.lock.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE DOOR LOCK: " + resultText);
        assertTrue(resultText.contains("Door") && resultText.contains("Your request"));
        guiController.remoteCommandResultPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);

        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.RemoteHistoryicon.tap();
        guiController.requestHistoryScreen.isPresent(10);

        // Can't check command name in Genesis - Make sure we have the correct command
        //assertEquals("Lights", guiController.requestHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful

        assertEquals("SUCCESS", guiController.requestHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.2 A successful remote command displays 'SUCCESS'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a successful remote command displays 'SUCCESS'")
    void aSuccessfulRemoteCommandDisplaySuccess() {

        // send a remote command and wait for the success message

        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.lock.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE DOOR LOCK: " + resultText);
        assertTrue(resultText.contains("Door") && resultText.contains("Your request"));
        guiController.remoteCommandResultPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);

        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.RemoteHistoryicon.tap();
        guiController.requestHistoryScreen.isPresent(10);

        // Can't check command name in Genesis - Make sure we have the correct command
        //assertEquals("Lights", guiController.requestHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful

        assertEquals("SUCCESS", guiController.requestHistoryScreen.getMostRecentCommandStatus());

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.3 A failed remote command displays '?'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify a failed remote command displays '?'")
    void FailedRemoteCommandDisplaysQuestionMark() {
        // send a remote command and wait for the failure message
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.stop.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
        guiController.remoteCommandResultPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1,4000);
        // check that the most recent remote command in remote history shows failure icon
        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        guiController.remoteFeaturesScreenGen2Point5.RemoteHistoryicon.tap();
        guiController.requestHistoryScreen.isPresent(10);

        // Can't check command name in Genesis - Make sure we have the correct command
        //assertEquals("Stop", guiController.requestHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful

        assertEquals("Fail", guiController.requestHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.4 A pending remote command displays 'PENDING'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - 7.7.4 A pending remote command displays 'PENDING'")
    void PendingRemoteCommandDisplaysPending() {
        // send a remote command, then check remote history after a couple seconds to give it time to process
        guiController.remoteFeaturesScreenGen2Point5.lockButton.tap();
        appController.appFunctions.pauseTestExecution(1,5000);
        // check that the most recent remote command in remote history shows "Pending"
        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.RemoteHistoryicon.tap();
        guiController.requestHistoryScreen.isPresent(10);
        guiController.requestHistoryScreen.remotehistorytop.tap();
        assertEquals("Pending", guiController.requestHistoryScreen.getMostRecentCommandStatus());
        // wait for the remote command to complete
        guiController.remoteCommandResultPopup.isPresent(180);
    }

}
