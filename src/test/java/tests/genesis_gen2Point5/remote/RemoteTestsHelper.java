package tests.genesis_gen2Point5.remote;

import config.Profile;
import views.AppController;
import views.genesis.GuiController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

public class RemoteTestsHelper {
    private AppController appController;
    private GuiController guiController;
    private Profile profile;
    private int hour;
    private int minute;

    // Need to map 3-letter zone ID's due to java deprecation
    private Map<String, String> timeZonesDayLightSavings = new HashMap<>();
    private Map<String, String> timeZonesNotDayLightSavings = new HashMap<>();

    /**
     * Remote command enum for use with getRemoteCommandMessage() and any other potential similar functions.
     * Currently NOT USED.
     */
    public enum RemoteCommands {
        START("start"),
        STOP("stop"),
        LOCK("lock"),
        UNLOCK("unlock"),
        LIGHTS("lights"),
        HORN_AND_LIGHTS("hornandlights");

        private final String commandText;

        RemoteCommands(final String commandText) {
            this.commandText = commandText;
        }

        @Override
        public String toString() {
            return commandText;
        }
    }

    /**
     * Constructor
     * @param appController A reference to the app controller
     * @param guiController A reference to the GUI controller
     * @param profile The default profile to use for methods in this helper class
     */
    public RemoteTestsHelper(AppController appController, GuiController guiController, Profile profile) {
        this.appController = appController;
        this.guiController = guiController;
        this.profile = profile;

        timeZonesNotDayLightSavings.put("America/Los_Angeles","PST");
        timeZonesDayLightSavings.put("America/Los_Angeles","PDT");
    }

    /**
     * Sends incorrect remote_genesis_old commands until the PIN lockout popup is on screen.
     * Must be called from the home screen or PIN locked popup.
     * @param incorrectPin Any PIN that is not the correct account PIN
     */
    public void resetPin(String incorrectPin) {
        setPinStateToLocked(incorrectPin);

        // change pin process
        // enter password
        guiController.pinLockedPopup.changePinButton.tap();
        appController.appFunctions.pauseTestExecution(1,1000);
        guiController.changePinEnterPasswordScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        guiController.changePinEnterPasswordScreen.enterPasswordEditText.enterText(profile.primaryAccount.password);
        guiController.changePinEnterPasswordScreen.submitButton.tap();
        // enter security answer
        appController.appFunctions.pauseTestExecution(1,1000);
        guiController.changePinEnterSecurityAnswerScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        guiController.changePinEnterSecurityAnswerScreen.enterSecurityAnswerEditText
                .enterText(profile.primaryAccount.securityAnswer);
        guiController.changePinEnterSecurityAnswerScreen.submitButton.tap();
        // enter and confirm new pin
        guiController.changePinEnterNewPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.changePinEnterNewPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.changePinEnterNewPinScreen.saveButton.tap();
        guiController.changePinSuccessfulPopup.okButton.tap(10);
    }

    /**
     * Sends the chosen remote_genesis_old command three times using an incorrect PIN. Leaves the PIN lockout popup open at the end.
     * @param incorrectPin Any PIN that is not the correct account PIN
     */
    public void setPinStateToLocked(String incorrectPin) {
        // we use i < 3 because the app is designed to lock out after three attempts. If something goes wrong,
        // we don't want to get stuck in the loop
        final int MAX_ATTEMPTS = 3;
        int i = 0;
        while (!guiController.pinLockedPopup.isPresent() && i < MAX_ATTEMPTS) {
            guiController.enterPinScreen.enterPin(incorrectPin);
            if (guiController.incorrectPinPopup.isPresent()) {
                guiController.incorrectPinPopup.okButton.tap();
            }
            i++;
        }
    }

    /**
     * Currently unused method to build a string for the chosen command. Will maybe be used in the future if
     * timestamp comparisons are requested.
     * @param remoteCommand A string of the remote_genesis_old command being sent
     * @param successful A boolean of whether the remote_genesis_old command is expected to be successful or not.
     * @return The entire string that should match the remote_genesis_old command result popup message.
     */
    public String getRemoteCommandMessage(String remoteCommand, boolean successful) {
        String remoteCommandTimeText = getTimeInAppFormat();
        // TODO figure out what "unsuccessful" text actually is
        String successfulText = (successful) ? "successful." : "not processed.";
        String resultMessageText = "Your request for Remote ";

        switch (remoteCommand.toLowerCase().replaceAll("\\s+", "")) {
            case "start":
                if (successful) {
                    resultMessageText += "Start with Climate Control on " + remoteCommandTimeText + " was " +
                            successfulText + "  Your vehicle ignition is now on.  " +
                            "Please ensure it is in a well ventilated area.";
                }
                else {

                }
                break;

            case "stop":
                if (successful) {
                    resultMessageText += "Control Stop on " + remoteCommandTimeText + " was " + successfulText;
                }
                else {
                    resultMessageText = "Remote Stop for your vehicle cannot be processed. Remote Stop can only be used during Remote Start session. [HT_539]";
                }
                break;

            case "lock":
                resultMessageText += "Door Lock on " + remoteCommandTimeText + " was " + successfulText;
                if (!successful) {
                    resultMessageText += " The feature is currently unavailable. Please try again later.";
                }
                break;

            case "unlock":
                resultMessageText += "Door Unlock on " + remoteCommandTimeText + " was " + successfulText;
                if (!successful) {
                    resultMessageText += " The feature is currently unavailable. Please try again later.";
                }
                break;

            case "lights":
                resultMessageText += "Lights Only on " + remoteCommandTimeText + " was sent to vehicle.";
                if (!successful) {
                    resultMessageText += " The feature is currently unavailable. Please try again later.";
                }
                break;

            case "hornandlights":
                resultMessageText += "Horn & Lights on " + remoteCommandTimeText + " was sent to vehicle.";
                break;
        }

        return resultMessageText;
    }

    /**
     * Gets a current timestamp and returns it as a string in the format used in the MyHyundai app.
     * @return
     */
    private String getTimeInAppFormat() {
        // TODO find a way to have dynamic timezone in relation to the location of the vehicle
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        Instant now = Instant.now();
        ZonedDateTime zonedDateTime = now.atZone(zoneId);
        String zTime = zonedDateTime.toString();

        String dateTime = zTime.substring(5,7) + "/" + zTime.substring(8,10) + "/" + zTime.substring(0,4);
        hour = Integer.parseInt(zTime.substring(11,13));
        String minute = zTime.substring(14,16);

        // determine meridian and adjust to 12 hour clock.
        String ampm;
        if (hour < 12) {
            ampm = "am";
        }
        else {
            ampm = "pm";
            hour = hour - 12;
        }
        // check for midnight or noon to be 12:MM instead of 00:MM
        if (hour == 0) {
            hour = 12;
        }

        // Need to convert hour back to string because
        // there needs to be 2 characters
        String hourConverted = Integer.toString(hour);
        if (hourConverted.length() < 2) {
            hourConverted = "0" + hourConverted;
        }

        String timeZoneThreeLetter;
        // Check if the time zone is currently in daylight savings
        if(zoneId.getRules().isDaylightSavings(now)){
            timeZoneThreeLetter = timeZonesDayLightSavings.get(zoneId.getId());
        }
        else {
            timeZoneThreeLetter = timeZonesNotDayLightSavings.get(zoneId.getId());
        }

        return dateTime + " at " + hourConverted + ":" + minute + " " + ampm + " " + timeZoneThreeLetter;
    }

    /**
     * Compare 2 times in format 00:00 that are within 2 minutes of each other
     * @param messageTime Time of message
     * @param timeToCompare Time to compare against
     * @return Are the times close enough together
     */
    public boolean compareMessageTime(String messageTime, String timeToCompare){
        // Assumes time format 00:00
        int messageTimeHour = Integer.parseInt(messageTime.substring(0,2));
        int messageTimeMinute = Integer.parseInt(messageTime.substring(3));
        int timeToCompareHour = Integer.parseInt(timeToCompare.substring(0,2));
        int timeToCompareMinute = Integer.parseInt(timeToCompare.substring(3));

        if(messageTimeHour == timeToCompareHour){
            if(messageTimeMinute < (timeToCompareMinute + 2)){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get the current hour in a time zone
     * @return Hour
     */
    public String getCurrentHour(){
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        Instant now = Instant.now();
        ZonedDateTime zonedDateTime = now.atZone(zoneId);
        String zTime = zonedDateTime.toString();

        //String dateTime = zTime.substring(5,7) + "/" + zTime.substring(8,10) + "/" + zTime.substring(0,4);
        hour = Integer.parseInt(zTime.substring(11,13));

        if (hour > 12) {
            hour = hour - 12;
        }
        // check for midnight or noon to be 12:MM instead of 00:MM
        if (hour == 0) {
            hour = 12;
        }

        String hourConverted = Integer.toString(hour);
        if (hourConverted.length() < 2) {
            hourConverted = "0" + hourConverted;
        }

        return hourConverted;
    }

    /**
     * Get the current minute in a time zone
     * @return Minute
     */
    public String getCurrentMinute(){
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        Instant now = Instant.now();
        ZonedDateTime zonedDateTime = now.atZone(zoneId);
        String zTime = zonedDateTime.toString();

        //String dateTime = zTime.substring(5,7) + "/" + zTime.substring(8,10) + "/" + zTime.substring(0,4);
        String minute = zTime.substring(14,16);
        return minute;
    }
}
