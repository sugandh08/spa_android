package tests.genesis_gen2Point5.remote;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class RemoteStart extends TestController {

    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
        guiController.homeScreen.remoteButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.1 Remote start success of Gen2 with Climate ON/Defrost ON/HeatedServicesON/HeatedSeatON")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOnHeatedServicesOnHeatedSeatsOn() {
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.startButton.tap();

        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);

        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreenGen2Point5.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.heatedSeat_1.tap();

        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESPONSE: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.2 Remote start success of Gen2 with Climate ON/Defrost ON/HeatedServicesON/VentSeatON")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOnHeatedServicesOnVentSeatsOn() {
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.startButton.tap();

        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);

        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreenGen2Point5.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.ventSeat_1.tap();
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);

        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESPONSE: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.2.2 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("Genesis - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
            guiController.remoteStartSettingsScreenGen2Point5.startButton.tap();
            guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);

            // wait a second for the engine duration text to update
            appController.appFunctions.pauseTestExecution(1, 1000);

            // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
            int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreenGen2Point5.engineDurationTimeTextView.getTextValue().replace(" min", ""));
            guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);

            guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
            guiController.remoteStartSettingsScreenGen2Point5.heatedSeat_1.tap();

            guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
            guiController.remoteStartSettingsScreen.submitButton.tap();

            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }


}
