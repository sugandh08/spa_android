package tests.myh_app_refresh_regression_suite.UserProfile_Gen2Point5;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.GuiController;
import views.myhyundai.baseviews.HomeScreen;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayName("Gen 2.5- Vehicle Status Page")
public class Dashboard extends TestController {
    private Profile profile;

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);
    String email = "test_user" + randomInt + "@mailnesia.com";

    @BeforeEach
    void setupTests()
    {
        profile = appController.myHyundaiProfiles.withSecondaryDriver;
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);
        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 Verify Digital Key Icon")
    @Severity(SeverityLevel.NORMAL)
    @Story("Vehicle status Information")
    @Description("MyHyundai -  Verify Digital Key Icon")
    void DigitalKeyIcon()
    {
        assertTrue(guiController.profileScreen_gen2Point5.DigitalKey.elementExists(),"Digital key icon present");
        guiController.profileScreen_gen2Point5.ClickonHamburgerMenuButton();
        assertTrue(guiController.menuScreen.DigitalKey.elementExists(),"Digital Key is present in hamburger");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 Verify Seat toggle with all the heat and Cold options in the screen\n")
    @Severity(SeverityLevel.NORMAL)
    @Story("Preset Screen Verification")
    @Description("MyHyundai -  Verify Seat toggle with all the heat and Cold options in the screen")
    void SeattogglewithheatandColdoptions()
    {
        guiController.gasHomeScreenGen2Point5.RemoteStartButton.tap();
        guiController.remotePresetsScreen.editRemotePresets(1);
        guiController.remoteStartSettingsScreen.seatstogglebutton.tap();
        guiController.remoteStartSettingsScreen.seatdownbutton.tap();
        //assertTrue(guiController.remoteStartSettingsScreen.LeftcoldbuttonisPresent(),"Verified left cold button is present");
        //assertTrue(guiController.remoteStartSettingsScreen.RightcoldbuttonisPresent(),"Verified right cold button is present");
        assertTrue(guiController.remoteStartSettingsScreen.LeftHeatbuttonisPresent(),"Verified left heat button is present");
        //assertTrue(guiController.remoteStartSettingsScreen.RightHeatbuttonisPresent(),"Verified right heat button is present");
    }
}
