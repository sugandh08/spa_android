package tests.myh_app_refresh_regression_suite.UserProfile_Gen2Point5;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.Screen;
import views.myhyundai.baseviews.HomeScreen;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayName("Gen 2.5- Vehicle Status Page")
public class VehicleStatus extends TestController {
    private Profile profile;

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);
    String email = "test_user" + randomInt + "@mailnesia.com";

    @BeforeEach
    void setupTests()
    {
        profile = appController.myHyundaiProfiles.withSecondaryDriver;
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);
        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 Verify the Redesigned Vehicle Status Page(Collapse view)")
    @Severity(SeverityLevel.NORMAL)
    @Story("Vehicle status Information")
    @Description("MyHyundai -  Verify the redesigned vehicle status page(Collapse view)")
    void vehiclestatuspageCollapseview()
    {
        guiController.profileScreen_gen2Point5.Vehiclestatus.tap();
        appController.appFunctions.pauseTestExecution(1,5000);
        guiController.vehicleStatusScreenGen2Point5.swipe.up(1);
        assertTrue(guiController.vehicleStatusScreenGen2Point5.Lights.getTextValue().contains("Lights"),"Verify Lights text available");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.FluidLevels.getTextValue().contains("Fluid Levels"),"Verify Fluid Levels text available");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.SmartKeyBattery.getTextValue().contains("Smart Key Battery"),"Verify Smart Key Battery text available");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.2 Verify the Lights in Vehicle status page(Expand view)")
    @Severity(SeverityLevel.NORMAL)
    @Story("Vehicle status Information")
    @Description("MyHyundai -  Verify the Lights in Vehicle status page")
    void vehiclestatusLightsview() {
        guiController.vehicleStatusScreenGen2Point5.swipe.up(1);
        guiController.profileScreen_gen2Point5.Vehiclestatus.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.vehicleStatusScreenGen2Point5.Lights.getTextValue().contains("Lights"), "Verify Lights text available");
        guiController.vehicleStatusScreenGen2Point5.Lights.tap();
        assertTrue(guiController.vehicleStatusScreenGen2Point5.Lightsstatus.getTextValue().contains("All lights operational"),"Verify Lights Status");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.3 Verify the Fluid Levels In Vehicle status page(Expand view)")
    @Severity(SeverityLevel.NORMAL)
    @Story("Vehicle status Information")
    @Description("MyHyundai -  Verify the Fluid Levels In Vehicle status page")
    void vehiclestatusfluidLevelsview() {
        guiController.profileScreen_gen2Point5.Vehiclestatus.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.vehicleStatusScreenGen2Point5.swipe.up(1);
        assertTrue(guiController.vehicleStatusScreenGen2Point5.FluidLevels.getTextValue().contains("Fluid Levels"),"Verify Fluid Levels text available");
        guiController.vehicleStatusScreenGen2Point5.FluidLevels.tap();
        guiController.vehicleStatusScreenGen2Point5.swipe.up(1);
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isOilStatusOk(),"Verify OilStatus with OK icon");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isBrakeStatusOk(),"Verify Brake with OK icon");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isWasherStatusOk(),"Verify Washer with OK icon");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.4 Verify the Smartkey button In Vehicle Status Page(Expand view)")
    @Severity(SeverityLevel.NORMAL)
    @Story("Vehicle status Information")
    @Description("MyHyundai -  Verify Vehicle Status Smartkey Battery View")
    void vehiclestatusSmartkeybatteryview() {
        guiController.profileScreen_gen2Point5.Vehiclestatus.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.vehicleStatusScreenGen2Point5.swipe.up(1);
        guiController.vehicleStatusScreenGen2Point5.SmartKeyBattery.tap();
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isSmartKeyBatteryStatusOk(),"verify SmartKey Status");
    }
}
