package tests.myh_app_refresh_regression_suite.UserProfile_Gen2Point5;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Favorite Places Test")
public class FavoritePlaces extends TestController {

    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.withSecondaryDriver;
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);
        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(70);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.3.1 Add Home address to Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify adding home address to Favorite Places")
    void addHomeAddressToFavoritePlaces() {
        String homeAddress = "Irvine 92708";
        String title = "Home";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        if (guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title) == true) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title), "Add Icon is not displayed");
        }
        else
            {
                assertTrue(guiController.favoritePlacesScreen_gen2Point5.isClearTextDisplayed(title) == true,"Clear Icon is not displayed");
                guiController.favoritePlacesScreen_gen2Point5.clickClearText(title);
                assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title), "Add Icon is not displayed");
            }

            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(title);
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(homeAddress);
            appController.appFunctions.pauseTestExecution(1, 3000);
            String resultAddress = guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue();
           assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
          guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
            guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            appController.appFunctions.tapAndroidBackButton();
          guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                    "Tab not switched to Favorite Places");
            assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                    getXpathOfFavoritePlacesAddressText(title)).contains(resultAddress), "Home Address not saved");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.2 Edit Home address in Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify editing home address in Favorite Places")
    void EditHomeAddressInFavoritePlaces() {

        String homeAddress = "CA 92708";
        String title = "Home";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        if (guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(title)) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(title), "Edit Text is not displayed");
        }
        else {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(title);
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(homeAddress);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }

            guiController.favoritePlacesScreen_gen2Point5.editAddress(title);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");

            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(homeAddress);
            appController.appFunctions.pauseTestExecution(1, 3000);
           assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
           guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        String resultEditedAddress = guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlacesAddressText(title));
           guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
            guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
          appController.appFunctions.pauseTestExecution(1, 5000);
            appController.appFunctions.tapAndroidBackButton();

            guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                    "Tab not switched to Favorite Places");

            assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                    getXpathOfFavoritePlacesAddressText(title)).contains(resultEditedAddress), "Home Address not saved");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.3.3 Add Work address to Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify adding Work address to Favorite Places")
    void addWorkToFavoritePlaces() {

        String workAddress = "Irvine 92708";
        String title = "Work";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        if (guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title) == true) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title), "Add Icon is not displayed");
        }
        else
        {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isClearTextDisplayed(title) == true,"Clear Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickClearText(title);
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title), "Add Icon is not displayed");

        }

        guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(title);
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(workAddress);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultAddress = guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue();
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(title)).contains(resultAddress), "Work Address not saved");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.4 Edit Work address in Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify editing Work address in Favorite Places")
    void EditWorkAddressInFavoritePlaces() {

        String workAddress = "CA 92708";
        String title = "Work";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        if (guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(title)) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(title), "Edit Text is not displayed");
        }
        else {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(title), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(title);
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(workAddress);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }

        guiController.favoritePlacesScreen_gen2Point5.editAddress(title);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");

        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(workAddress);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        String resultEditedAddress = guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlacesAddressText(title));
        guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
         appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(title)).contains(resultEditedAddress), "Work Address not saved");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.3.5 Add Favorite Place address 1 to Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify adding Favorite Place address 1 to Favorite Places")
    void addFavoritePlace1ToFavoritePlaces() {

        String address = "Irvine 92708";
        String index = "3";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
        String titleText=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));



        if (guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText) == true) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText);

        }
        else
        {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isClearTextDisplayed(titleText) == true,"Clear Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickClearText(titleText);
            String titleText2=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));

            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText2), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText2);

        }

        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.elementExists(), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        String titleText3=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));

        guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultAddress=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlacesAddressText(titleText3));
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(titleText3)).contains(resultAddress), "Work Address not saved");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.6 Edit Favorite Place address 1 in Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify editing Favorite Place address 1 in Favorite Places")
    void EditFavoritePlace1InFavoritePlaces() {

        String address = "CA 92708";
        String index = "3";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        String titleText=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));


        if (guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(titleText)) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(titleText), "Edit Text is not displayed");

        }
        else {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText);
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }

        guiController.favoritePlacesScreen_gen2Point5.editAddress(titleText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");

        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(titleText)).contains(address), "Work Address not saved");




    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.3.7 Add Favorite Place address 2 to Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify adding Favorite Place address 2 to Favorite Places")
    void addFavoritePlace2ToFavoritePlaces() {

        String address = "Irvine 92708";
        String index = "2";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        String titleText=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));




        if (guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText) == true) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText);

        }
        else
        {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isClearTextDisplayed(titleText) == true,"Clear Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickClearText(titleText);
            String titleText2=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));

            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText2), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText2);

        }

        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        String titleText3=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));

        guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);


        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(titleText3)).contains(address), "Work Address not saved");




    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.8 Edit Favorite Place address 2 in Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify editing Favorite Place address 2 in Favorite Places")
    void EditFavoritePlace2InFavoritePlaces() {

        String address = "CA 92708";
        String index = "2";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        String titleText=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));



        if (guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(titleText)) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(titleText), "Edit Text is not displayed");
        }
        else {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText);
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }

        guiController.favoritePlacesScreen_gen2Point5.editAddress(titleText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");

        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.favoritePlacesScreen_gen2Point5.swipe.fromBottomEdge(2);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);


        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(titleText)).contains(address), "Work Address not saved");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.3.9 Add Favorite Place address 3 to Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify adding Favorite Place address 3 to Favorite Places")
    void addFavoritePlace3ToFavoritePlaces() {

        String address = "Irvine 92708";
        String index = "2";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(2);


        String titleText=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));


        if (guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText) == true) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText);

        }
        else
        {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isClearTextDisplayed(titleText) == true,"Clear Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickClearText(titleText);
            String titleText2=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));

            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText2), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText2);
        }

        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        String titleText3=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));

        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(2);


        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(titleText3)).contains(address), "Work Address not saved");




    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.10 Edit Favorite Place address 3 in Favorite Places")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Favorite Places")
    @Description("MyHyundai - Verify editing Favorite Place address 3 in Favorite Places")
    void EditFavoritePlace3InFavoritePlaces() {

        String address = "CA 92708";
        String index = "2";

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(2);

        String titleText=guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.getXpathOfFavoritePlaceTitleByIndex(index));


        if (guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(titleText)) {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isEditTextDisplayed(titleText), "Edit Text is not displayed");
        }
        else {
            assertTrue(guiController.favoritePlacesScreen_gen2Point5.isAddIconDisplayed(titleText), "Add Icon is not displayed");
            guiController.favoritePlacesScreen_gen2Point5.clickAddIcon(titleText);
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
            guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }

        guiController.favoritePlacesScreen_gen2Point5.editAddress(titleText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.isPresent(), "Add To Favorites Screen not loaded");

        guiController.poiAddToFavoritePlacesScreen_gen2Point5.searchPOI(address);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiAddToFavoritePlacesScreen_gen2Point5.addressText.getTextValue().contains(address), "Address not found");
        guiController.poiAddToFavoritePlacesScreen_gen2Point5.addToFavoritePlacesButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.favoritePlacesScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.favoritePlacesPanel.tap();
        assertTrue(guiController.favoritePlacesScreen_gen2Point5.favoritePlacesPanel.getSelectedValue() == true,
                "Tab not switched to Favorite Places");

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(2);


        assertTrue(guiController.getTextUsingXpath(guiController.favoritePlacesScreen_gen2Point5.
                getXpathOfFavoritePlacesAddressText(titleText)).contains(address), "Work Address not saved");


    }
}
