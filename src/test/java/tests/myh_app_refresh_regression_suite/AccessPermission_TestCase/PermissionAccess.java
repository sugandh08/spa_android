package tests.myh_app_refresh_regression_suite.AccessPermission_TestCase;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Permission Access")
@Feature("Permission Access")
@DisplayName("Test Permission Access")

public class PermissionAccess extends TestController {
    private Profile profile;
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        guiController.devicePermissionPopup.clickOnDeny();

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1 Verify user gets prompt message on search of POI in the home screen when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test user gets prompt message on search of POI in the home screen when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on search of POI in the home screen when deny permissions")
    void PromptMessageOnPOIScreen(){
        //---Kindly update in TestSetup , autoGrantPermissions set false (line-48)

        String searchText = "Fountain Valley";
        guiController.electricHomeScreen.poiSearchTextbox.enterText(searchText);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.electricHomeScreen.poiSearchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='DENY']").contains("DENY"));
        assertTrue(guiController.getTextUsingXpath("//*[@text='ALLOW']").contains("ALLOW"));
        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.poiSearchScreen.poiSearchEditTextbox.enterText(searchText);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();

        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiSearchScreen.poiLocationText.getTextValue().contains(searchText));

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.2 Verify user gets location permission prompt message on tap of GPS icon in the map")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test  user gets location permission prompt message on tap of GPS icon in the map")
    @Description("MyHyundai - Verify user gets location permission prompt message on tap of GPS icon in the map")
    void PromptMessageOnMap()
    {
        guiController.mapOptions.clickOnFavoritesButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnDeny();
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='MyHyundai App needs to access your current location. Please enable location services in your device settings to use map features.']").contains("App needs to access your current location"));
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.devicePermissionPopup.clickCancelLocationService();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.mapOptions.gpsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.mapOptions.gpsButton.tap();
        assertTrue(guiController.favoritesPOIScreen.favoritePointsOfInterestLabelText.elementExists());
    }



    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3 Verify user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    void PromptMessageOnMapPOI()
    {
        guiController.mapOptions.clickOnPOISearchButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        String searchText = "Fountain Valley";
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.poiSearchScreen.poiSearchEditTextbox.enterText(searchText);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();

        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiSearchScreen.poiLocationText.getTextValue().contains(searchText));


    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4 Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    void PromptMessageOnCarFinder()
    {
        guiController.mapOptions.clickOnCarFinderButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        //-----Verify Loader message-----
        String ExpectedMessage = "Please allow up to one minute to";
        //System.out.println("--->"+guiController.carFinderScreen.loaderMessage.getTextValue());
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.carFinderScreen.loaderMessage.getTextValue().contains(ExpectedMessage));

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.5 Verify user gets prompt message on tap of Nearby Fuel in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Verify user gets prompt message on tap of Nearby Fuel in the Maps fly options when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on tap of Nearby Fuel in the Maps fly options when deny permissions")
    void PromptMessageOnNearByFuel()
    {
        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.mapOptions.clickOnNearbyFuelButton();
        assertTrue(guiController.nearbyGasScreen.nearbyGasStationsLabelText.elementExists());

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.6 Verify user gets prompt message on tap of Charge Station in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Verify user gets prompt message on tap of Charge Station in the Maps fly options when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on tap of Charge Station in the Maps fly options when deny permissions")
    void PromptMessageOnChargeStation()
    {
        guiController.mapOptions.clickOnChargeStationsButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        String zipCode="92804";
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.chargeStationScreen.searchEditTextbox.enterText(zipCode);
        guiController.chargeStationScreen.searchGlassIcon.tap();
        guiController.chargeStationScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        assertTrue(guiController.chargeStationScreen.chargeStationLocationText.elementExists());


    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.7 Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    void PromptMessageOnFavorites()
    {
        guiController.mapOptions.clickOnFavoritesButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.favoritesPOIScreen.favoritePointsOfInterestLabelText.elementExists());


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.8 Verify user gets prompt message on tap of Dealers in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Verify user gets prompt message on tap of Dealers in the Maps fly options when deny permissions")
    @Description("MyHyundai - Verify user gets prompt message on tap of Dealers in the Maps fly options when deny permissions")
    void PromptMessageOnDealers()
    {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text=concat('Allow MyHyundai to access this device', \"'\", 's location?')]").contains("access this device's location"));

        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.dealerLocatorScreen.isPresent());

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.9 Verify user gets prompt message to access pictures and record video when user try to add profile picture")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test  user gets prompt message to access pictures and record video when user try to add profile picture")
    @Description("MyHyundai - Verify user gets prompt message to access pictures and record video when user try to add profile picture")
    void PromptMessageOnAddProfilePicture()
    {
        guiController.homeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.menuScreen.userProfileImage.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='Allow MyHyundai to take pictures and record video?']").contains("take pictures and record video?"));

        //---After Allow the permission---
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='Select source']").contains("Select source"));


    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.10 Verify user gets prompt message to access contacts when user try to select contacts to Invite Additional Driver")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test  user gets prompt message to access contacts when user try to select contacts to Invite Additional Driver")
    @Description("MyHyundai - Verify user gets prompt message to access contacts when user try to select contacts to Invite Additional Driver")
    void PromptMessageOnAccessContactToInviteAdditionalDriver()
    {
        guiController.homeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.menuScreen.viewProfileButton.tap();

        appController.appFunctions.pauseTestExecution(1, 5000);
        if(!guiController.profileScreen_gen2Point5.inviteAdditionalDriverButton.elementExists()) {
            guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(7);
            System.out.println("++++++++"+guiController.profileScreen_gen2Point5.additionalDriverCount.getTextValue());
        }
        if(guiController.profileScreen_gen2Point5.additionalDriverCount.getTextValue().contains("4"))
        {
            System.out.println("----Reached Additional Driver Limit pf 4----");
            guiController.profileScreen_gen2Point5.clickRemoveDriverButton();
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.profileScreen_gen2Point5.yesButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
        }

        guiController.profileScreen_gen2Point5.inviteAdditionalDriverButton.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.inviteAdditionalDriverScreen_gen2Point5.selectContact.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath(" //*[@text='Allow MyHyundai to access your contacts?']").contains("access your contacts?"));
        appController.appFunctions.pauseTestExecution(1, 2000);

        //---After Allow the permission---
        guiController.devicePermissionPopup.clickOnAllow();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='Choose a contact']").contains("Choose a contact"));


    }









}
