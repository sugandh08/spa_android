package tests.myh_app_refresh_regression_suite.phev_vehicle.remote_actions;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Stop")
@Feature("Remote Stop")
@DisplayName("Remote Stop")
public class RemoteStop extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.5.1 Remote stop success")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote stop success")
    void remoteStopSuccess() {
        // start the car. need start it in order to stop it.
        guiController.remoteCarControls.clickOnRemoteStartButton();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 6);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();


        //assertTrue(guiController.hybridHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // stop the car
        guiController.remoteCarControls.clickOnRemoteStopButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.hybridHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Stop"));
        resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Control Stop") && resultText.contains("successful"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.5.2 Remote stop failure")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote stop failure")
    void remoteStopFailure() {
        guiController.remoteCarControls.clickOnRemoteStopButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 10000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("5.5.3 Remote stop with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  remote stop with incorrect PIN shows incorrect PIN notification")
        void remoteStopWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteCarControls.clickOnRemoteStopButton();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

   /* @Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.stopButton);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.2.3 Remote stop with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify remote_genesis_old stop with incorrect PIN shows incorrect PIN notification")
        void remoteStopWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }

        @Nested
        @DisplayName("7.2.4 Remote stop with incorrect PIN three times")
        class RemoteStopWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteStopWithIncorrectPinThreeTimes() {
                remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.stopButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.2.4.1 Error popup is displayed saying how to reset PIN")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying how to reset PIN")
            void errorPopupIsDisplayedSayingHowToResetPin(){
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.2.4.2 User is restored access to remote_genesis_old commands after 5 minutes")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote_genesis_old commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.cancelButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }
    }*/
}
