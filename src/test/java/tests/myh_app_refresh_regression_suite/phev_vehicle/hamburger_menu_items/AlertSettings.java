package tests.myh_app_refresh_regression_suite.phev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard
@Epic("Alert Settings Test")
@Feature("Alert Settings")
@DisplayName("Alert Settings")
public class AlertSettings extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 5000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        guiController.alertSettingsScreen.isPresent(60);
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.1 Toggling Speed Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Speed Alert setting does not change the setting value")
    void togglingSpeedAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.speedAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.2 Toggling Valet Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Valet Alert setting does not change the setting value")
    void togglingValetAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.valetAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.3 Toggling Curfew Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Curfew Alert setting does not change the setting value")
    void togglingCurfewAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.curfewAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.4 Toggling Geo-Fence Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Geo-Fence Alert setting does not change the setting value")
    void togglingGeoFenceAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.geoFenceAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.5 Selecting to set up a Speed Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Speed Alert opens correct settings page")
    void selectingToOpenSpeedAlertOpensCorrectSettingsPage() {
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath,2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit= guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("SPEED ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Speed Alert") && resultText.contains("successful"),"Speed Alert saved successfully");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.6 Selecting to set up a Valet Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Valet Alert opens correct settings page")
    void selectingToOpenValetAlertOpensCorrectSettingsPage() {
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.7 Selecting to set up a Curfew Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Curfew Alert opens correct settings page")
    void selectingToOpenCurfewAlertOpensCorrectSettingsPage() {
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSettingsScreen.curfewAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.curfewAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.8 Selecting to set up a Geo-Fence Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Geo-Fence Alert opens correct settings page")
    void selectingToOpenGeoFenceAlertOpensCorrectSettingsPage() {
        appController.appFunctions.pauseTestExecution(1, 3000);
        //--Update addNewGeoFenceAlertButton & swipe
        guiController.alertSettingsScreen.swipe.up(3);
        guiController.alertSettingsScreen.addNewGeoFenceAlertButton.tap();

        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.geoFenceAlertScreen.isPresent());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.9 Verify Geo-Fence Alert Update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Geo-Fence Alert Update")
    @Description("MyHyundai - Verify Geo-Fence Alert Update")
    void GeoFenceAlertUpdate() {
        String geoFenceName = "New Geo Fence" + Math.random();
        Element toggle = guiController.alertSettingsScreen.geoFenceAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        if(!toggleValue)
        {
            toggle.tap();
        }
        guiController.alertSettingsScreen.swipe.up(3);
        appController.appFunctions.pauseTestExecution(1, 3000);
        if (guiController.alertSettingsScreen.getGeoFenceAlertLimit(1)) {
            guiController.alertSettingsScreen.clickOnGeoFenceAlert(1);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
        guiController.geoFenceAlertScreen.milesEditText.enterText("50");
        guiController.geoFenceAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
        }
        else
        {
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.geoFenceAlertScreen.isPresent());
            guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);

        }
        //---Tap on Save button on Alert Screen
        guiController.alertSettingsScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertModificationPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSettingsScreen.swipe.up(3);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.10 Verify Curfew Alert Update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Curfew Alert Update")
    @Description("MyHyundai - Verify Curfew Alert Update")
    void CurfewAlertUpdate() {
        Element toggle = guiController.alertSettingsScreen.curfewAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        if(!toggleValue)
        {
            toggle.tap();
        }
        if (guiController.alertSettingsScreen.checkIfCurfewAlertAlreadyAdded()) {
            System.out.println("Clicking on existed curfew alert ");
            guiController.alertSettingsScreen.clickOnCurfewAlert();
        }else {
            guiController.alertSettingsScreen.addNewCurfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.curfewAlertScreen.curfewFromButton.tap();

            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            String getFromValue = guiController.curfewAlertScreen.getFromCurfewAlertValue.getTextValue();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            String getToValue = guiController.curfewAlertScreen.getToCurfewAlertValue.getTextValue();

            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();

        }
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.curfewAlertScreen.curfewFromButton.tap();

        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String getFromValue = guiController.curfewAlertScreen.getFromCurfewAlertValue.getTextValue();

        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String getToValue = guiController.curfewAlertScreen.getToCurfewAlertValue.getTextValue();

        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSavedPopup.okButton.tap();

        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSettingsScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertModificationPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + getFromValue + "']").contains(getFromValue));
            assertTrue(guiController.getTextUsingXpath("//*[@text='" + getToValue + "']").contains(getToValue));


        }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.11 Verify Speed Alert Update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Speed Alert Update")
    @Description("MyHyundai - Verify Speed Alert Update")
    void SpeedAlertUpdate(){
        Element toggle = guiController.alertSettingsScreen.speedAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        if(!toggleValue)
        {
            toggle.tap();
        }
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSavedPopup.okButton.tap();

        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSettingsScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertModificationPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();

        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.12 Verify Valet Alert Update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Valet Alert Update")
    @Description("MyHyundai - Verify Valet Alert Update")
    void ValetAlertUpdate(){

        Element toggle = guiController.alertSettingsScreen.valetAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        if(!toggleValue)
        {
            toggle.tap();
        }
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.valetAlertScreen.distanceLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.valetAlertScreen.doneButton.tap();
        String valetLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.valetAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSavedPopup.okButton.tap();

        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSettingsScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertModificationPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();

        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.alertSettingsScreen.valetAlertValue.getTextValue().contains(valetLimit));


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.12 Verify Brand Identity Renewal")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Valet Alert Update")
    @Description("MyHyundai - Verify Brand Identity Renewal")
    void Logoidentity(){
        assertTrue(guiController.alertSettingsScreen.SpeedAlertlogo.elementExists(),"Speed alert logo is displayed");
        assertTrue(guiController.alertSettingsScreen.ValetAlertlogo.elementExists(),"Valet ALERT Logo is displayed");
        assertTrue(guiController.alertSettingsScreen.GeofenceAlertlogo.elementExists(),"Geofrence alert logo is displayed");
        assertTrue(guiController.alertSettingsScreen.CurfewAlertlogo.elementExists(),"Curfew alert logo is displayed");
    }

}

