package tests.myh_app_refresh_regression_suite.phev_vehicle.car_Care;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DealerServiceOffers extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.2.1 Dealer Service Offers")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealer Service Offers")
    @Description("MyHyundai - Verify Dealer Service Offers")
    void dealerServiceOffers() {
        guiController.homeScreen.carCareButton.tap();
        assertTrue(guiController.carCareOptions.dealerServiceOffersIsDisplayed());
    }
}
