package tests.myh_app_refresh_regression_suite.phev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote History Test")
@Feature("Remote History")
@DisplayName("Remote History")
public class RemoteHistory extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        guiController.hybridHomeScreen.isPresent(30);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.5.1 A remote command is displayed")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a remote command is displayed.")
    void aRemoteCommandIsDisplayed() {
        // send a remote command (we choose remote stop because it will fail)
       guiController.remoteCarControls.clickOnRemoteStopButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
        guiController.remoteCommandResultPopup.okButton.tap();
        // check that the remote command is displayed in history
        guiController.hybridHomeScreen.remoteHistoryButton.tap();
        guiController.remoteHistoryScreen.isPresent(10);
        assertEquals("Stop", guiController.remoteHistoryScreen.getMostRecentCommandName());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.5.2 A successful remote command displays 'SUCCESS'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a successful remote command displays 'SUCCESS'")
    void aSuccessfulRemoteCommandDisplaySuccess() {
        // send a remote command and wait for the success message
        guiController.remoteCarControls.clickOnRemoteFlashLightsButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 20000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Lights Only") && resultText.contains("sent to vehicle"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // check that the most recent remote command in remote history shows 'success'
        guiController.hybridHomeScreen.remoteHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(1, 20000);
        guiController.remoteHistoryScreen.isPresent(10);
        // Make sure we have the correct command
        assertEquals("Lights", guiController.remoteHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
       assertEquals("Success", guiController.remoteHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.5.3 A failed remote command displays '?'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a failed remote command displays '?'")
    void aFailedRemoteCommandDisplaysQuestionMark() {
        // send a remote command and wait for the failure message
        guiController.remoteCarControls.clickOnRemoteStopButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.requestSentPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // check that the most recent remote command in remote history shows failure icon
        guiController.hybridHomeScreen.remoteHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(1, 20000);
        guiController.remoteHistoryScreen.isPresent(10);
        // Make sure we have the correct command
        assertEquals("Stop", guiController.remoteHistoryScreen.getMostRecentCommandName());
        guiController.remoteHistoryScreen.tap.elementByXpath(guiController.remoteHistoryScreen.mostRecentCommandXpathRoot);
        // if we do, make sure it reflects that it was fail
        assertEquals("Fail", guiController.remoteHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.5.4 A pending remote command displays 'PENDING'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a pending remote command displays 'PENDING'")
    void aPendingRemoteCommandDisplaysPending() {
        // send a remote command, then check remote history after a couple seconds to give it time to process
        guiController.remoteCarControls.clickOnRemoteFlashLightsButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.requestSentPopup.okButton.tap();
        // check that the most recent remote command in remote history shows "Pending"
        guiController.hybridHomeScreen.remoteHistoryButton.tap();
        guiController.remoteHistoryScreen.isPresent(10);
        // Make sure we have the correct command
       assertEquals("Lights", guiController.remoteHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
       assertEquals("Pending", guiController.remoteHistoryScreen.getMostRecentCommandStatus());

        // wait for the remote command to complete
        guiController.remoteCommandResultPopup.isPresent(180);
    }
}
