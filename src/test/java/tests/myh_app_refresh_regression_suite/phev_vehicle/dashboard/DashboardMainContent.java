package tests.myh_app_refresh_regression_suite.phev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Greeting Text, Vehicle Modal and Weather Icon")
@Feature("Greeting Text, Vehicle Modal and Weather Icon")
@DisplayName("Test Greeting Text, Vehicle Modal and Weather Icon on dashboard")
public class DashboardMainContent extends TestController {
    private Profile profile;
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(1, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Verify Vehicle Name on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Name on dashboard")
    @Description("MyHyundai - Verify Vehicle Name on dashboard")
    void vehicleModal(){
        guiController.homeScreen.clickOnHamburgerMenuButton();
        String vehicleModal= guiController.menuScreen.vehicleNameTextView.getTextValue();
        guiController.menuScreen.closeButton.tap();
        assertTrue(guiController.hybridHomeScreen.vehicleModelName.getTextValue().contains(vehicleModal));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.2 Verify Greeting Text on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Greeting Text on dashboard")
    @Description("MyHyundai - Verify Greeting Text on dashboard")
    void greetingText(){
        assertTrue(guiController.getTextUsingXpath(guiController.hybridHomeScreen.greetingTextXpath).contains("Good"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.3 Verify Weather Icon on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Weather Icon on dashboard")
    @Description("MyHyundai - Verify Weather Icon on dashboard")
    void weatherIcon(){
        assertTrue(guiController.hybridHomeScreen.weatherIcon.elementExists());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.4 Verify Total Range distance  on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Total Range distance on dashboard")
    @Description("MyHyundai - Verify Total Range distance on dashboard")
    void TotalRangeDistance(){
        assertTrue(guiController.hybridHomeScreen.totalRangeDistanceTextView.elementExists());

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.5 Verify Electric Range distance  on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Electric Range distance on dashboard")
    @Description("MyHyundai - Verify Electric Range distance on dashboard")
    void ElectricRangeDistance(){
        assertTrue(guiController.hybridHomeScreen.electricRangeDistanceTextView.elementExists());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.6 Verify battery Gauge meter  on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test battery Gauge meter on dashboard")
    @Description("MyHyundai - Verify battery Gauge meter on dashboard")
    void BatteryGaugemeter(){
        assertTrue(guiController.hybridHomeScreen.batteryGaugemeter.elementExists());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.7 Verify battery Level  on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test battery Level on dashboard")
    @Description("MyHyundai - Verify battery Level on dashboard")
    void batteryLevel(){
        assertTrue(guiController.hybridHomeScreen.batteryLevel.elementExists());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.8 Verify plugin Status  on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test plugin Status on dashboard")
    @Description("MyHyundai - Verify plugin Status on dashboard")
    void pluginStatus(){
        assertTrue(guiController.hybridHomeScreen.pluginStatus.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.9 Verify Charging Status  on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Charging Status on dashboard")
    @Description("MyHyundai - Verify Charging Status on dashboard")
    void ChargingStatus(){

        assertTrue(guiController.hybridHomeScreen.ChargingStatus.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.9 Verify Brand Identity Renewal")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Charging Status on dashboard")
    @Description("MyHyundai - Verify Brand Identity Renewal")
    void RemoteActionslogo(){
        assertTrue(guiController.hybridHomeScreen.RemoteActionslogo());
    }
}
