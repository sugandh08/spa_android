package tests.myh_app_refresh_regression_suite.phev_vehicle.dashboard;
import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.apache.tools.ant.types.selectors.SelectSelector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChargeSchedule extends TestController {

    private Profile profile;
    private Element StartChargeButton;
    private String incorrectPin;
    WebDriverWait wait;
    WebElement element;


    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 Start Charge")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Start Charge")
    @Description("MyHyundai - Verify Start Charge")
    void vehicleStartChargeWithValidPin() {

        //tap the Start Charge on Vehicle Dashboard
        guiController.electricHomeScreen.StartChargeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 9000);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();

    }

    @Nested
    class vehicleStartChargeWithInvalidPin {
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.2 Start Charge with incorrect PIN shows incorrect PIN notification")
        @Story("Test Start Charge with incorrect PIN")
        @Description("Start Charge with incorrect PIN shows incorrect PIN notification")
        void StartChargeWithIncorrectPinShowsIncorrectPinNotification() {
            StartChargeButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 PHEV vehicle Departure schedule")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Departure time schedule")
    @Description("MyHyundai - PHEV vehicle Departure time schedule")
    void vehicleDepartureTimeWithSchedule1() {
        //tap the Remote Start on Vehicle Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.departureSchedule.tap();

        //--tap on sunday
        if (!guiController.editChargeScheduleScreen.sun.getCheckedValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        guiController.departureSetschedulePopup.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        //---Again click on departure schedule-
        guiController.electricHomeScreen.departureSchedule.tap();
        //---saved value stored in String from SetSchedule page
        String Time1 = guiController.setDepartureTimeScheduleScreen.setSchedule1.getTextValue();
        guiController.electricHomeScreen.clickOnBackButton();

        //----Dashboard screen
        assertTrue(guiController.setDepartureTimeScheduleScreen.departureTimeDetailsDashboard().contains(Time1));
        appController.appFunctions.pauseTestExecution(1, 2000);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 Verify Off-Peak Schedule toggle button")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Off-Peak Schedule toggle")
    @Description("MyHyundai - Verify Off-Peak Schedule toggle button")
    void OffPeakScheduletoggle() {
        //Click on Set schedule button
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        assertTrue(guiController.offPeakScheduleScreen.OffPeakScheduleisPresent(), "Off-Peak Schedule is present");
        assertTrue(guiController.offPeakScheduleScreen.Offpeakscheduletoggle.elementExists(), "Off-Peak aSchedule toggle is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.5 Verify Schedule One with toggle button")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Schedule One toggle")
    @Description("MyHyundai - Verify Schedule One with toggle button")
    void ScheduleOne() {
        //Click on Set Schedule one button
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");
        assertTrue(guiController.offPeakScheduleScreen.Scheduleonetoggle.elementExists(), "Schedule one toggle is present");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.6 Verify Schedule Two with toggle button")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Schedule One toggle")
    @Description("MyHyundai - Verify Schedule Two with toggle button")
    void ScheduleTwo() {
        //Click on Set Schedule two button
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");
        assertTrue(guiController.offPeakScheduleScreen.Scheduletwotoggle.elementExists(), "Schedule two toggle is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.7 Set Charge Schedule success Charge Schedule 1 Only")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 1 Only")
    void SetChargeSchedulesuccessChargeScheduleOne() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        //Click on Schedule 1
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();

        //tap on monday if it is not selected
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue())
        {
            guiController.editChargeScheduleScreen.mon.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();

        //Verifying the changes saved or not
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified Dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Monday is selected");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 Set Charge Schedule success Charge Schedule 2 Only")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 2 Only")
    void SetChargeSchedulesuccessChargeScheduleTwo() {

        //tap Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");

        ////Click on the Schedule two toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        //Click on Schedule 2
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //tap on monday if it is not selected
        if (!guiController.editChargeScheduleScreen.mon.getSelectedValue())
        {
            guiController.editChargeScheduleScreen.mon.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();

        //Verifying the changes saved or not
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.mon.getSelectedValue(),"Verified Monday is selected");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.9 Set Charge Schedule success Charge Schedule 1 One day only")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 1 One day only")
    void SetChargeScheduleChargeScheduleOneonedayonly() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();

        //--tap on sunday only
        if (!guiController.editChargeScheduleScreen.sun.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (guiController.editChargeScheduleScreen.mon.getSelectedValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }
        if (guiController.editChargeScheduleScreen.tue.getSelectedValue()) {
            guiController.editChargeScheduleScreen.tue.tap();
        }
        if (guiController.editChargeScheduleScreen.wed.getSelectedValue()) {
            guiController.editChargeScheduleScreen.wed.tap();
        }
        if (guiController.editChargeScheduleScreen.thr.getSelectedValue()) {
            guiController.editChargeScheduleScreen.thr.tap();
        }
        if (guiController.editChargeScheduleScreen.fri.getSelectedValue()) {
            guiController.editChargeScheduleScreen.fri.tap();
        }
        if (guiController.editChargeScheduleScreen.sat.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sat.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getSelectedValue(),"Verified Sunday is selected");
        assertFalse(guiController.editChargeScheduleScreen.mon.getSelectedValue(),"Verified Monday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.tue.getSelectedValue(),"Verified Tuesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.wed.getSelectedValue(),"Verified Wednesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.thr.getSelectedValue(),"Verified Thursday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.fri.getSelectedValue(),"Verified Friday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.sat.getSelectedValue(),"Verified Saturday is not selected");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.10 Set Charge Schedule success Charge Schedule 1 and 2 both")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 1 and 2 both")
    void SetChargeSchedulesuccessChargeScheduleOneandScheduleTwo() {

        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();

        //Schedule1
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        //Click on Schedule 1
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();

        //tap on monday if it is not selected
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue())
        {
            guiController.editChargeScheduleScreen.mon.tap();
        }

        //Click on Done button to save the time for schedule 1
        guiController.offPeakScheduleScreen.Done.tap();

        //Schedule2
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");

        //Click on the Schedule two toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        //Click on Schedule 2
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //tap on tuesday if it is not selected
        if (!guiController.editChargeScheduleScreen.tue.getClickableValue())
        {
            guiController.editChargeScheduleScreen.tue.tap();
        }

        //Click on Done button to save the time for schedule 2
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying the changes saved or not
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Monday is selected");
        guiController.offPeakScheduleScreen.Done.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Monday is selected");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.11 Set Charge Schedule success Charge Schedule 2 One day only")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 2 One day only")
    void SetChargeScheduleChargeScheduleTwoonedayonly() {

        //tap Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");

        //Click on the Schedule two toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //--tap on sunday only
        if (!guiController.editChargeScheduleScreen.sun.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (guiController.editChargeScheduleScreen.mon.getSelectedValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }
        if (guiController.editChargeScheduleScreen.tue.getSelectedValue()) {
            guiController.editChargeScheduleScreen.tue.tap();
        }
        if (guiController.editChargeScheduleScreen.wed.getSelectedValue()) {
            guiController.editChargeScheduleScreen.wed.tap();
        }
        if (guiController.editChargeScheduleScreen.thr.getSelectedValue()) {
            guiController.editChargeScheduleScreen.thr.tap();
        }
        if (guiController.editChargeScheduleScreen.fri.getSelectedValue()) {
            guiController.editChargeScheduleScreen.fri.tap();
        }
        if (guiController.editChargeScheduleScreen.sat.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sat.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getSelectedValue(),"Verified Sunday is selected");
        assertFalse(guiController.editChargeScheduleScreen.mon.getSelectedValue(),"Verified Monday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.tue.getSelectedValue(),"Verified Tuesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.wed.getSelectedValue(),"Verified Wednesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.thr.getSelectedValue(),"Verified Thursday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.fri.getSelectedValue(),"Verified Friday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.sat.getSelectedValue(),"Verified Saturday is not selected");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.12 Set Charge Schedule success Charge Schedule 1 Multiple days ")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 1 Multiple days")
    void SetChargeScheduleChargeScheduleOneMultipledays() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();

        //Click on multiple days if it is not selected
        if (!guiController.editChargeScheduleScreen.sun.getClickableValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }
        if (!guiController.editChargeScheduleScreen.tue.getClickableValue()) {
            guiController.editChargeScheduleScreen.tue.tap();
        }
        if (!guiController.editChargeScheduleScreen.wed.getClickableValue()) {
            guiController.editChargeScheduleScreen.wed.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();


        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.tue.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.wed.getClickableValue(),"Verified Sunday is selected");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.13 Set Charge Schedule success Charge Schedule 2 Multiple day only")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 2 Multiple day only")
    void SetChargeScheduleChargeScheduleTwoMultipledays()
    {

        //tap Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");

        //Click on the Schedule two toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //Click on multiple days if it is not selected
        if (!guiController.editChargeScheduleScreen.sun.getClickableValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }
        if (!guiController.editChargeScheduleScreen.tue.getClickableValue()) {
            guiController.editChargeScheduleScreen.tue.tap();
        }
        if (!guiController.editChargeScheduleScreen.wed.getClickableValue()) {
            guiController.editChargeScheduleScreen.wed.tap();
        }


        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.tue.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.wed.getClickableValue(),"Verified Sunday is selected");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.14 User should be able to Save the changes only for Schedule 1 Start Driving time")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify User should be able to Save the changes only for Schedule 1 Start Driving time")
    void SuccessChargeScheduleOneStartDriving() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();

        //Click on multiple days if it is not selected
        if (!guiController.editChargeScheduleScreen.sun.getClickableValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();


        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Sunday is selected");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.15 User should be able to Save the changes only for Schedule 2 Start Driving time")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify User should be able to Save the changes only for Schedule 2 Start Driving time")
    void SuccessChargeScheduleTwoStartDriving() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //Click on multiple days if it is not selected
        if (!guiController.editChargeScheduleScreen.sun.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getClickableValue(),"Verified Sunday is selected");
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Sunday is selected");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.16 Set Charge Schedule Off Peak Toggle with charge schedule 1 ON")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule Off Peak Toggle with charge schedule 1 ON")
    void ChargeScheduleOffPeakTogglewithchargescheduleOneON()
    {

        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 12000);
        assertTrue(guiController.offPeakScheduleScreen.OffPeakScheduleisPresent(),"Verified Off-Peak Schedule is present");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        if (guiController.offPeakScheduleScreen.Scheduletwotoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        //Click on Off-Peak toggle if it is off
        if (!guiController.offPeakScheduleScreen.Offpeakscheduletoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Offpeakscheduletoggle.tap();
        }

        guiController.offPeakScheduleScreen.offpeakchargingpriority.tap();

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        //Verify timming is set only for off-peak charging hours only
        if(!guiController.offPeakScheduleScreen.offpeakchargingonly.getSelectedValue()){
            guiController.offPeakScheduleScreen.offpeakchargingonly.tap();
        };

        //Click on save the to save the timing settings
        guiController.offPeakScheduleScreen.Save.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying off peak schedule is submitted
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.offPeakScheduleScreen.Scheduleonetoggle.getCheckedValue(),"Verified Schedule one is ON");
        assertFalse(guiController.offPeakScheduleScreen.Scheduletwotoggle.getCheckedValue(),"Verified Schedule 2 is OFF");
        assertTrue(guiController.offPeakScheduleScreen.Offpeakscheduletoggle.getCheckedValue(),"Verified Off-Peak toggle is ON");
        assertTrue(guiController.offPeakScheduleScreen.Starttime.getTextValue().contains("M"),"Verified Starttime having some AM/PM timing");
        assertTrue(guiController.offPeakScheduleScreen.Endtime.getTextValue().contains("M"),"Verified Endtime having some AM/PM timing");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.17 Set Charge Schedule Off Peak Toggle with charge schedule 2 ON")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule Off Peak Toggle with charge schedule 2 ON")
    void ChargeScheduleOffPeakTogglewithchargescheduleTwoON()
    {
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 12000);
        assertTrue(guiController.offPeakScheduleScreen.OffPeakScheduleisPresent(),"Verified Off-Peak Schedule is present");

        //Click on the Schedule one toggle if it is Off
        if (guiController.offPeakScheduleScreen.Scheduleonetoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        //Click on Off-Peak toggle if it is off
        if (!guiController.offPeakScheduleScreen.Offpeakscheduletoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Offpeakscheduletoggle.tap();
        }

        guiController.offPeakScheduleScreen.offpeakchargingpriority.tap();

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        //Verify timming is set only for off-peak charging hours only
        if(!guiController.offPeakScheduleScreen.offpeakchargingonly.getSelectedValue()){
            guiController.offPeakScheduleScreen.offpeakchargingonly.tap();
        };

        //Click on save the to save the timing settings
        guiController.offPeakScheduleScreen.Save.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying off peak schedule is submitted
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertFalse(guiController.offPeakScheduleScreen.Scheduleonetoggle.getCheckedValue(),"Verified Schedule one is OFF");
        assertTrue(guiController.offPeakScheduleScreen.Scheduletwotoggle.getCheckedValue(),"Verified Schedule 2 is ON");
        assertTrue(guiController.offPeakScheduleScreen.Offpeakscheduletoggle.getCheckedValue(),"Verified Off-Peak toggle is ON");
        assertTrue(guiController.offPeakScheduleScreen.Starttime.getTextValue().contains("M"),"Verified Starttime having some AM/PM timing");
        assertTrue(guiController.offPeakScheduleScreen.Endtime.getTextValue().contains("M"),"Verified Endtime having some AM/PM timing");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.18 Set Charge Schedule Off Peak Toggle with charge schedule 1 and 2 ON")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule Off Peak Toggle with charge schedule 1 and 2 ON")
    void ChargeScheduleOffPeakTogglewithchargescheduleOneTwoON()
    {
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 12000);
        assertTrue(guiController.offPeakScheduleScreen.OffPeakScheduleisPresent(),"Verified Off-Peak Schedule is present");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        //Click on Off-Peak toggle if it is off
        if (!guiController.offPeakScheduleScreen.Offpeakscheduletoggle.getCheckedValue()){
            guiController.offPeakScheduleScreen.Offpeakscheduletoggle.tap();
        }

        guiController.offPeakScheduleScreen.offpeakchargingpriority.tap();

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        //Verify timming is set only for off-peak charging hours only
        if(!guiController.offPeakScheduleScreen.offpeakchargingonly.getSelectedValue()){
            guiController.offPeakScheduleScreen.offpeakchargingonly.tap();
        };

        //Click on save the to save the timing settings
        guiController.offPeakScheduleScreen.Save.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying off peak schedule is submitted
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.offPeakScheduleScreen.Scheduleonetoggle.getCheckedValue(),"Verified Schedule one is OFF");
        assertTrue(guiController.offPeakScheduleScreen.Scheduletwotoggle.getCheckedValue(),"Verified Schedule 2 is ON");
        assertTrue(guiController.offPeakScheduleScreen.Offpeakscheduletoggle.getCheckedValue(),"Verified Off-Peak toggle is ON");
        assertTrue(guiController.offPeakScheduleScreen.Starttime.getTextValue().contains("M"),"Verified Starttime having some AM/PM timing");
        assertTrue(guiController.offPeakScheduleScreen.Endtime.getTextValue().contains("M"),"Verified Endtime having some AM/PM timing");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.19 Set Charge Schedule success Charge Schedule 1 Only")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 1 Only")
    void ChargeSchedulesuccessVehicleUnplugged() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");

        //Click on the Schedule one toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduleonetoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduleonetoggle.tap();
        }

        //Click on Schedule 1
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();

        //tap on monday if it is not selected
        if (!guiController.editChargeScheduleScreen.mon.getClickableValue())
        {
            guiController.editChargeScheduleScreen.mon.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();

        //Verifying the changes saved or not
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified Dashboard screen is present");
        assertTrue(guiController.electricHomeScreen.pluggedNotpluggedin.getTextValue().contains("Not Plugged In"),"Verified vehicle not plugged in");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule1.tap();
        assertTrue(guiController.editChargeScheduleScreen.mon.getClickableValue(),"Verified Monday is selected");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.20 Verify user can see Tap to Set label by default when no schedule set for Schedule 1")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Charge time schedule")
    @Description("MyHyundai - Verify Set Charge Schedule success Charge Schedule 1 Only")
    void TaptosetSchedule1only() {
        //tap the Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleOneisPresent(), "Schedule one is displayed");
        assertTrue(guiController.offPeakScheduleScreen.Schedule1text.getTextValue().contains("Tap to set"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.21 Verify user can see Tap to Set label by default when no schedule set for Schedule 2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Charge time schedule")
    @Description("MyHyundai - Verify user can see Tap to Set label by default when no schedule set for Schedule 2")
    void TaptosetSchedule2() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.electricHomeScreen.TaptosetSchedule2.getTextValue().contains("Tap to set"),"Verify TaptoSet is displayed in Schedule2");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.22 Verify user can see Tap to Set label by default when no schedule set for Schedule 1")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Charge time schedule")
    @Description("MyHyundai - Verify user can see Tap to Set label by default when no schedule set for Schedule 1")
    void TaptosetSchedule1() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.electricHomeScreen.TaptosetSchedule1.getTextValue().contains("Tap to set"),"Verify TaptoSet is displayed in Schedule2");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.23 PHEV Set Charge Schedule success Vehicle Plugged-in")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - PHEV Set Charge Schedule success Vehicle Plugged-in")
    void SetChargeScheduleSuccess() {
        assertTrue(guiController.electricHomeScreen.pluginStatusTextView.getTextValue().contains("Plugged In"),"verified vehicle not plugged in");

        //tap Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");

        //Click on the Schedule two toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //--tap on sunday only
        if (!guiController.editChargeScheduleScreen.sun.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (guiController.editChargeScheduleScreen.mon.getSelectedValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }
        if (guiController.editChargeScheduleScreen.tue.getSelectedValue()) {
            guiController.editChargeScheduleScreen.tue.tap();
        }
        if (guiController.editChargeScheduleScreen.wed.getSelectedValue()) {
            guiController.editChargeScheduleScreen.wed.tap();
        }
        if (guiController.editChargeScheduleScreen.thr.getSelectedValue()) {
            guiController.editChargeScheduleScreen.thr.tap();
        }
        if (guiController.editChargeScheduleScreen.fri.getSelectedValue()) {
            guiController.editChargeScheduleScreen.fri.tap();
        }
        if (guiController.editChargeScheduleScreen.sat.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sat.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getSelectedValue(),"Verified Sunday is selected");
        assertFalse(guiController.editChargeScheduleScreen.mon.getSelectedValue(),"Verified Monday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.tue.getSelectedValue(),"Verified Tuesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.wed.getSelectedValue(),"Verified Wednesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.thr.getSelectedValue(),"Verified Thursday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.fri.getSelectedValue(),"Verified Friday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.sat.getSelectedValue(),"Verified Saturday is not selected");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.24 PHEV Set Charge Schedule success Vehicle Unplugged")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Charge time schedule")
    @Description("MyHyundai - PHEV Set Charge Schedule success Vehicle Unplugged")
    void SetChargeScheduleSuccessunplugged() {
        assertTrue(guiController.electricHomeScreen.pluginStatusTextView.getTextValue().contains("Not Plugged In"),"verified vehicle not plugged in");

        //tap Charge schedules on Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.offPeakScheduleScreen.ScheduleTwoisPresent(), "Schedule two is displayed");

        //Click on the Schedule two toggle if it is Off
        if (!guiController.offPeakScheduleScreen.Scheduletwotoggle.getClickableValue()){
            guiController.offPeakScheduleScreen.Scheduletwotoggle.tap();
        }

        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();

        //--tap on sunday only
        if (!guiController.editChargeScheduleScreen.sun.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sun.tap();
        }
        if (guiController.editChargeScheduleScreen.mon.getSelectedValue()) {
            guiController.editChargeScheduleScreen.mon.tap();
        }
        if (guiController.editChargeScheduleScreen.tue.getSelectedValue()) {
            guiController.editChargeScheduleScreen.tue.tap();
        }
        if (guiController.editChargeScheduleScreen.wed.getSelectedValue()) {
            guiController.editChargeScheduleScreen.wed.tap();
        }
        if (guiController.editChargeScheduleScreen.thr.getSelectedValue()) {
            guiController.editChargeScheduleScreen.thr.tap();
        }
        if (guiController.editChargeScheduleScreen.fri.getSelectedValue()) {
            guiController.editChargeScheduleScreen.fri.tap();
        }
        if (guiController.editChargeScheduleScreen.sat.getSelectedValue()) {
            guiController.editChargeScheduleScreen.sat.tap();
        }

        //Click on Done button to save the time schedule
        guiController.offPeakScheduleScreen.Done.tap();

        //Click on submit button to set the charge schedule
        guiController.offPeakScheduleScreen.Submit.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //Verifying Schedule is selected for One day only
        assertTrue(guiController.electricHomeScreen.isPresent(),"Verified dashboard screen is present");
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.offPeakScheduleScreen.StartDrivingSchedule2.tap();
        assertTrue(guiController.editChargeScheduleScreen.sun.getSelectedValue(),"Verified Sunday is selected");
        assertFalse(guiController.editChargeScheduleScreen.mon.getSelectedValue(),"Verified Monday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.tue.getSelectedValue(),"Verified Tuesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.wed.getSelectedValue(),"Verified Wednesday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.thr.getSelectedValue(),"Verified Thursday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.fri.getSelectedValue(),"Verified Friday is not selected");
        assertFalse(guiController.editChargeScheduleScreen.sat.getSelectedValue(),"Verified Saturday is not selected");
    }

}