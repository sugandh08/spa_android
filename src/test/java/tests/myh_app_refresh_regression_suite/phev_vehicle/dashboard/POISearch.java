package tests.myh_app_refresh_regression_suite.phev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class POISearch extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.1 Verify POI via search textbox on dashboard ")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test POI via search textbox on dashboard")
    @Description("MyHyundai - Verify POI via search textbox on dashboard")
    void searchPOI() {
        String searchText = "Fountain Valley";
        guiController.hybridHomeScreen.poiSearchTextbox.enterText(searchText);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.hybridHomeScreen.poiSearchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.poiSearchScreen.pointOfInterestLabelText.elementExists();
        assertTrue(guiController.poiSearchScreen.poiLocationText.getTextValue().contains(searchText));
    }
    }
