package tests.myh_app_refresh_regression_suite.phev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

public class StartStopCharge extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(1, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.1 Start/Stop Charge")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Start/Stop Charge on dashboard")
    @Description("MyHyundai - Start/Stop Charge on dashboard")
    void RemoteStartStopCharge(){
        if(guiController.hybridHomeScreen.startCharge.getTextValue().equals("Start Charge")) {
            //----Start Charge----
            guiController.hybridHomeScreen.startCharge.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
            System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
          }else
         {
             //---Stop Charge
            guiController.hybridHomeScreen.startCharge.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
            System.out.println("REMOTE STOP CHARGE RESPONSE: " + resultText);
        }


    }


}
