package tests.myh_app_refresh_regression_suite.phev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Epic("Remote Start Test")
@Feature("Remote Start")
@DisplayName("Remote Start")
public class RemoteStart extends TestController {
    private Profile profile;
    private String incorrectPin;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.1 Remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithClimateOffDefrostOffHeatedServicesOff() {
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(false, false, false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.2 Remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithClimateOnDefrostOffHeatedServicesOff() {
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true, false, false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.6.3 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.electricHomeScreen.remoteStartButton.tap();
            guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
            guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);
            // wait a second for the engine duration text to update
            appController.appFunctions.pauseTestExecution(1, 1000);
            // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
            int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
            guiController.remoteStartSettingsScreen.setStartSettingsStates(false, false, false);
            guiController.remoteStartSettingsScreen.submitButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.4 Remote start Pre-conditions")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote start Pre-conditions")
    @Description("MyHyundai - Verify Remote start Pre-conditions")
    void RemoteStartPreCondition()
    {
        //---Click on Remote Start on Dashboard
        guiController.electricHomeScreen.remoteStartButton.tap();
        System.out.println("--"+guiController.remoteStartSettingsScreen.preConditionButton.getTextValue());
        //---Click on Edit Preset 1st option
        guiController.remotePresetsScreen.editRemotePresets(1);
        assertTrue(guiController.remoteStartSettingsScreen.preConditionButton.getTextValue().contains("pre-conditions"));
       //---Tap on precondition
        guiController.remoteStartSettingsScreen.preConditionButton.tap();
        assertTrue(guiController.remoteStartSettingsScreen.oK.getTextValue().contains("OK"));
        //---Tap on ok button on Remote Start Pre-Condition
        guiController.remoteStartSettingsScreen.oK.tap();
        assertTrue(guiController.remoteStartSettingsScreen.subtitleTextView.getTextValue().contains("Remote Start"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.5 Verify presets are getting updated successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify presets are getting updated successfully")
    void presetsettingupdated() {
        // Click on remote Start on Dashboard
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remotePresetsScreen.editRemotePresets(1);
        // click on Engine Duration expand button to set the duration
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 4);
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.climateSettingsScreen.TEMPERATURE_XPATH,15);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true, true, true);
        //Click on submit button
        guiController.climateSettingsScreen.ClickonSubmitButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        // click on 1st setting button to edit the preset
        guiController.remotePresetsScreen.editRemotePresets(1);
        assertTrue(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().contains("min"),"Verified engine duration contains min");
        assertTrue(guiController.remoteStartSettingsScreen.temperatureToggleButton.getClickableValue(),"Verified Temperature toggle button is on");
        assertTrue(guiController.remoteStartSettingsScreen.frontDefrosterToggleButton.getClickableValue(),"Verified front defroster toggle button is on");
        assertTrue(guiController.remoteStartSettingsScreen.heatedFeaturesToggleButton.getClickableValue(),"Verified heated features toggle button is on");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.5 Verify successful remote start of without presets")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify successful remote start of without presets")
    void successfulremotestartofwithoutpresets() {
        // Click on remote Start on Dashboard
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remotePresetsScreen.remoteStartWithoutPresetsText.tap();
        // click on Engine Duration expand button to set the duration
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 4);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.climateSettingsScreen.TEMPERATURE_XPATH,15);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true, true, true);
        //Click on submit button
        guiController.climateSettingsScreen.ClickonSubmitButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        //Tap on ok button after entering the pin
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(3, 5000);
        guiController.electricHomeScreen.clickOnOkButton();
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start - Success"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.5 Verify presets remote start reflected correctly in vehicle status")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify presets remote start reflected correctly in vehicle status")
    void remotestartreflectedcorrectlyinvehiclestatus() {
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remotePresetsScreen.editRemotePresets(1);
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 4);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelDown(guiController.climateSettingsScreen.TEMPERATURE_XPATH,05);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true, true, true);
        guiController.climateSettingsScreen.ClickonSubmitButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.remoteStartSettingsScreen.presetstartbuttn.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        //Tap on ok button after entering the pin
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(4, 10000);
        guiController.electricHomeScreen.clickOnOkButton();
        guiController.electricHomeScreen.vehicleStatusButton.tap();
        assertTrue( guiController.vehicleStatusScreen.climate.getTextValue().contains("HI"),"Verified HI temperature on vehicle");
        assertTrue( guiController.vehicleStatusScreen.engine.getTextValue().contains("ON"),"Verified Engine is ON");
        assertTrue( guiController.vehicleStatusScreen.heating.getTextValue().contains(" ON"),"Verified heated features is ON");
        assertTrue( guiController.vehicleStatusScreen.frontdefrost.getTextValue().contains("ON"),"Verified Front Defroster is ON");
    }
}

