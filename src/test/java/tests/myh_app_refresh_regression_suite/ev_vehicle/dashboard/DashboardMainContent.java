package tests.myh_app_refresh_regression_suite.ev_vehicle.dashboard;

import com.sun.java.accessibility.util.GUIInitializedListener;
import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.*;

@Epic("Greeting Text, Vehicle Modal and Weather Icon")
@Feature("Greeting Text, Vehicle Modal and Weather Icon")
@DisplayName("Test Greeting Text, Vehicle Modal and Weather Icon on dashboard")
public class DashboardMainContent extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Verify Vehicle Name on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Name on dashboard")
    @Description("MyHyundai - Verify Vehicle Name on dashboard")
    void vehicleModal() {
        guiController.homeScreen.clickOnHamburgerMenuButton();
        String vehicleModal = guiController.menuScreen.vehicleNameTextView.getTextValue();
        guiController.menuScreen.closeButton.tap();
        System.out.println("hamburger vehicle modl---" + vehicleModal);
        System.out.println("dashbord vehicle modl---" + guiController.gasHomeScreen.vehicleModelName.getTextValue());
        assertTrue(guiController.electricHomeScreen.vehicleModelName.getTextValue().contains(vehicleModal));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.2 Verify Greeting Text on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Greeting Text on dashboard")
    @Description("MyHyundai - Verify Greeting Text on dashboard")
    void greetingText() {
        assertTrue(guiController.getTextUsingXpath(guiController.electricHomeScreen.greetingTextXpath).contains("Good"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.3 Verify clicking On Refresh Icon Updates Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test clicking On Refresh Icon Updates Dashboard")
    @Description("MyHyundai - Verify clicking On Refresh Icon Updates Dashboard")
    void clickingOnRefreshIconUpdatesDashboard() {
        String RefreshTime = guiController.electricHomeScreen.refreshTime.getTextValue();
        guiController.electricHomeScreen.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(4, 7000);
        //get the time after refresh
        String afterRefreshTime = guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();
        assertTrue(!RefreshTime.equals(afterRefreshTime), "Vehicle Status Refresh failed");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.4 Verify Weather Icon on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Weather Icon on dashboard")
    @Description("MyHyundai - Verify Weather Icon on dashboard")
    void weatherIcon() {
        assertTrue(guiController.electricHomeScreen.weatherIcon.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.5 Home screen shows vehicle is not plugged in")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test EV Dashboard")
    @Description("MyHyundai - Verify Home screen shows vehicle is not plugged in")
    void homeScreenShowsVehicleNotPluggedIn() {
        assertEquals("Not Plugged In", guiController.electricHomeScreen.pluginStatusTextView.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.6 Home screen shows vehicle is not charging")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test EV Dashboard")
    @Description("MyHyundai - Verify Home screen shows vehicle is not charging")
    void homeScreenShowsVehicleNotCharging() {
        assertEquals("Not Charging", guiController.electricHomeScreen.chargingStatusTextView.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.7 Verify ChatboxLink navigation to Hyundai Virtual assistant")
    @Severity(SeverityLevel.NORMAL)
    @Story("Hyundai Virtual Assistant Chatbox")
    @Description("MyHyundai - Verify ChatboxLink navigation to Hyundai Virtual assistant")
    void ChatBotLink() {
        guiController.homeScreen.remoteHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        System.out.println(guiController.getTextUsingXpath("//*[@class='android.view.View' and @text='Hyundai Virtual Assistant']"));
        assertTrue(guiController.homeScreen.isHyundaiVirtualAssistantHeaderPresent(), "Verified ChatBot Link navigates to Hyundai Virtual Assistant page");
        String savedMsg = guiController.getTextUsingXpath("//*[@class='android.view.View' and @text='Blue Link']");
        System.out.println(savedMsg);
        guiController.homeScreen.clickOnBlueLink();
        appController.appFunctions.pauseTestExecution(2, 6000);
        }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.8 Verify Blue Link Subscription is removed from Burger menu")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Blue Link Subscription is removed from Burger menu")
    void Bluelinkinburgermenu() {
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        assertFalse(guiController.menuScreen.Bluelinksubscription.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.9 Remove Virtual Assistant Header from App for Chatbot Functionality")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remove Virtual Assistant Header from App for Chatbot Functionality")
    void RemoveVirtualAssistant() {
        guiController.homeScreen.remoteHistoryButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        //Print the Header of the Virtual Assistant page which is "Hyundai Virtual Assistant" not "Virtual Assistant"
        System.out.println(guiController.getTextUsingXpath("//*[@class='android.view.View' and @text='Hyundai Virtual Assistant']"));
        assertTrue(guiController.homeScreen.isHyundaiVirtualAssistantHeaderPresent(), "Verified ChatBot Link navigates to Hyundai Virtual Assistant page");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.10 Verify DK Icon from secondary driver on dashboard Screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify DK Icon from secondary driver on dashboard Screen")
    void DKiconandDigitalkey() {
        assertTrue(guiController.homeScreen.DKicon.elementExists(),"DK icon exist in dashboard");
        guiController.homeScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.menuScreen.DigitalKey.elementExists(),"Digital Key exist in hamburger");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.10 Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    void BlulinkunderVehicleConnectivitywarning(){
        assertTrue(guiController.homeScreen.carCareButton.elementExists(),"Verified home screen is present");
        assertTrue(guiController.homeScreen.ConnectivityWarning.elementExists(),"Verified Connectivity warning is displayed in homescreen");
        guiController.homeScreen.ConnectivityWarningBluelinkpresent();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.11 Verify the safety warning pop up when 'Keep me logged in' disbaled")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify the safety warning pop up when 'Keep me logged in' disbaled")
    void Safetywarning(){
    assertTrue(guiController.homeScreen.SafetyWarningtitle.elementExists(),"Safetywarning popup is displayed");
    appController.appFunctions.pauseTestExecution(1,3000);
    assertTrue(guiController.homeScreen.SafetyWarningContentisPresent(),"Safetywarning Content is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.12 Verify the safety warning pop up when 'Keep me logged in' enabled")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify the safety warning pop up when 'Keep me logged in' enabled")
    void Safetywarningkeepmeloggedin(){
        assertTrue(guiController.homeScreen.SafetyWarningtitle.elementExists(),"Safetywarning popup is displayed");
        appController.appFunctions.pauseTestExecution(1,3000);
        assertTrue(guiController.homeScreen.SafetyWarningContentisPresent(),"Safetywarning Content is displayed");
        assertTrue(guiController.homeScreen.Dismiss.elementExists(),"Dismiss button is displayed");
        assertTrue(guiController.homeScreen.LearnMore.elementExists(),"LearnMore button is displayed");
        guiController.homeScreen.Dismiss.tap();
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.electricHomeScreen.swipe.up(2);
        guiController.menuScreen.signOutButton.tap();
        guiController.alertSavedPopup.okButton.tap();
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        assertFalse(guiController.homeScreen.SafetyWarningtitle.elementExists(),"Safetywarning popup is not displayed");
        appController.appFunctions.pauseTestExecution(1,3000);
    }
}