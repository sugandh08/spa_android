package tests.myh_app_refresh_regression_suite.ev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2ElectricStandard
@Epic("Start Stop Charge")
@Feature("Start Stop Charge")
public class StartCharge extends TestController{
    private Profile profile;
    private String incorrectPin;
    WebDriverWait wait;
    WebElement element;


    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(30);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.1 Pressing Start Charge shows remote request success popup")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop Charge")
    @Description("MyHyundai - Verify Pressing Start Charge shows remote request success popup")
    void successfulRemoteChargeStartShowsSuccessMessage() {
        setupTests();
        System.out.println("NOTE: this test only works if the car is plugged in");
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Charge Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("START REMOTE ELECTRIC CHARGE") && resultText.contains("now charging"));
        guiController.electricHomeScreen.refreshButton.tap();

        appController.appFunctions.pauseTestExecution(1,10000);
        // if it is charging, stop the charge
        if (guiController.electricHomeScreen.startStopChargeButton.getTextValue().equals("Stop Charge")) {
            guiController.electricHomeScreen.startStopChargeButton.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            guiController.remoteCommandResultPopup.isPresent(60);
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.2 Pressing Stop Charge shows remote request success popup")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop Charge")
    @Description("MyHyundai - Verify Pressing Stop Charge shows remote request success popup")
    void successfulRemoteChargeStopShowsSuccessMessage() {
        setupTests();
        System.out.println("NOTE: this test only works if the car is plugged in");
        // start charging. need to have it charging in order to stop it.
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Charge Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("START REMOTE ELECTRIC CHARGE") && resultText.contains("now charging"));
        guiController.remoteCommandResultPopup.okButton.tap();

        guiController.electricHomeScreen.refreshButton.tap();

        appController.appFunctions.pauseTestExecution(1,10000);
        // stop charging
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE STOP CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("STOP REMOTE ELECTRIC CHARGE") && resultText.contains("stopped charging"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.3 Pressing Start Charge while vehicle is not plugged in shows notification that vehicle is unplugged")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Start Stop Charge")
    @Description("MyHyundai -  Verify Pressing Start Charge while vehicle is not plugged in shows notification that vehicle is unplugged")
    void remoteChargeWhileUnpluggedShowsUnpluggedMessage() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(30);

        System.out.println("NOTE: this test only works if the car is NOT plugged in");
        guiController.electricHomeScreen.startStopChargeButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Charge Start"));

        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(60);
        System.out.println("REMOTE START CHARGE RESPONSE: " + resultText);
        assertTrue(resultText.contains("START REMOTE ELECTRIC CHARGE") && resultText.contains("unplugged"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.4 Start Charge with valid pin")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Start Charge")
    @Description("MyHyundai - Verify Start Charge")
    void vehicleStartChargeWithValidPin() {
        setupTests();
        //tap the Start Charge on Vehicle Dashboard
        guiController.electricHomeScreen.StartChargeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 9000);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();

    }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName(" 2.7.5 Start Charge with incorrect PIN shows incorrect PIN notification")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Start Charge with incorrect PIN")
        @Description("Start Charge with incorrect PIN shows incorrect PIN notification")
        void StartChargeWithIncorrectPinShowsIncorrectPinNotification() {
            setupTests();
            guiController.electricHomeScreen.StartChargeButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.6 Start Charge in Status Bar")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Start Charge")
    @Description("MyHyundai - Verify Start Charge")
    void vehicleStartCharge()
    {
        setupTests();
        assertTrue(guiController.electricHomeScreen.startStopChargeButton.elementExists(),"verify the Start Charge button in dashboard");
        //tap the Start Charge on Vehicle Dashboard
        guiController.electricHomeScreen.StartChargeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 9000);
        assertTrue(guiController.electricHomeScreen.remoteStatus.getTextValue().contains("Start Charge"),"Verify Start Charge status popup");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.7.7 Stop Charge in Status Bar")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Stop Charge")
    @Description("MyHyundai - Verify Stop Charge")
    void vehicleStopCharge() {

        //tap the Stop Charge on Vehicle Dashboard
        assertTrue(guiController.electricHomeScreen.startStopChargeButton.elementExists());
        // guiController.electricHomeScreen.StopChargeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 9000);
        assertTrue(guiController.electricHomeScreen.remoteStatus.getTextValue().contains("Start Charge"), "Verify Start Charge status popup");

    }
}
