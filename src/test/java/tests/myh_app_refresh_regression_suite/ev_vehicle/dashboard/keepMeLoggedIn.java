package tests.myh_app_refresh_regression_suite.ev_vehicle.dashboard;

import com.sun.java.accessibility.util.GUIInitializedListener;
import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.*;

@Epic("Greeting Text, Vehicle Modal and Weather Icon")
@Feature("Greeting Text, Vehicle Modal and Weather Icon")
@DisplayName("Test Greeting Text, Vehicle Modal and Weather Icon on dashboard")
public class keepMeLoggedIn extends TestController {
    private Profile profile;

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.1 Verify the safety warning pop up when 'Keep me logged in' enabled")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify the safety warning pop up when 'Keep me logged in' enabled")
    void Safetywarningkeepmeloggedin(){
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithKeepmeloggedinSelectVehicle(profile.primaryAccount, profile.primaryVin);
        assertTrue(guiController.homeScreen.SafetyWarningtitle.elementExists(),"Safetywarning popup is displayed");
        appController.appFunctions.pauseTestExecution(1,3000);
        assertTrue(guiController.homeScreen.SafetyWarningContentisPresent(),"Safetywarning Content is displayed");
        assertTrue(guiController.homeScreen.Dismiss.elementExists(),"Dismiss button is displayed");
        assertTrue(guiController.homeScreen.LearnMore.elementExists(),"LearnMore button is displayed");
        guiController.homeScreen.Dismiss.tap();
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.electricHomeScreen.swipe.up(2);
        guiController.menuScreen.signOutButton.tap();
        guiController.alertSavedPopup.okButton.tap();
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        assertFalse(guiController.homeScreen.SafetyWarningtitle.elementExists(),"Safetywarning popup is not displayed");
        appController.appFunctions.pauseTestExecution(1,3000);
    }
}