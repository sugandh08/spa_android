package tests.myh_app_refresh_regression_suite.ev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2ElectricStandard

@Epic("Charge Management")
@Feature("Charge Management")
@DisplayName("Dashboard- Charge Management Test")
    public class ChargeManagement extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 30);
        guiController.electricHomeScreen.isPresent(60);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 Set Up Charge Management Schedule and go back without editing/saving schedule")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify to Set Up Charge Management Schedule and go back without editing/saving schedule")
    void goBackWithoutEditingSchedule() {
        String departureText = guiController.electricHomeScreen.setScheduleButton.getTextValue();
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.chargeManagementScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(departureText.equals(guiController.electricHomeScreen.setScheduleButton.getTextValue()));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 Setting charge to Start Driving 1 only shows Schedule 1 information on home screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Remote Charge Scheduling")
    @Description("MyHyundai - Verify Setting charge to Start Driving 1 only with Climate Off And Off-Peak Charging Off shows Schedule 1 information on home screen")
    void settingChargeWithClimateOffAndPeakChargingOffShowsCorrectInformationOnHomeScreen() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.chargeManagementScreen.isPresent(30);
        guiController.chargeManagementScreen.startDrivingSection.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.startDrivingSettingsScreen.isPresent();
        guiController.swipeScrollWheelUp(guiController.startDrivingSettingsScreen.startDriving1HourWheel_Xpath, 4);
        guiController.swipeScrollWheelDown(guiController.startDrivingSettingsScreen.startDriving1MinuteWheel_Xpath, 4);
        guiController.swipeScrollWheelDown(guiController.startDrivingSettingsScreen.startDriving1AMPMWheel_Xpath, 1);
        guiController.startDrivingSettingsScreen.setStartDrivingSettingsStates(true, true);
        guiController.startDrivingSettingsScreen.saveButton.tap();
        String departureTime1 = guiController.chargeManagementScreen.departureTime1Text.getTextValue();
        guiController.chargeManagementScreen.setClimateAndOffPeakChargingStates(true, true);
        guiController.chargeManagementScreen.submitButton.tap();
        guiController.setChargeScheduleSuccessPopup.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 7000);
        assertTrue(guiController.getTextUsingXpath(guiController.electricHomeScreen.nextDepartureText_Xpath).contains(departureTime1));

        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(70);
        System.out.println("SET ELECTRIC CHARGE SCHEDULE: " + resultText);
        assertTrue(resultText.contains("SET ELECTRIC CHARGE SCHEDULE") && resultText.contains("successfully"));

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 Setting charge to Off-Peak Schedule")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Off-Peak Schedule")
    @Description("MyHyundai - Verify Setting charge to Off-Peak Schedule")
    void settingChargeWithOffPeakSchedule() {

        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        if (!guiController.setDepartureTimeScheduleScreen.schedule1.getCheckedValue()) {
            guiController.setDepartureTimeScheduleScreen.schedule1.tap();
        }

        boolean checked = guiController.chargeManagementScreen.offPeakSchedule.getCheckedValue();

        // System.out.println("--"+checked+"--");
        if (!checked) {
            guiController.chargeManagementScreen.offPeakSchedule.tap();
            assertTrue(guiController.getTextUsingXpath("//*[@text='Off-Peak Schedule']").contains("Off-Peak Schedule"));

            if (!guiController.chargeManagementScreen.offPeakWeekend.getCheckedValue()) {
                guiController.chargeManagementScreen.offPeakWeekend.tap();

            }

            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.offPeakScheduleScreen.chargeToggle.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            //---Tap on Weekdays
            guiController.offPeakScheduleScreen.weekdays.tap();
            guiController.swipeScrollWheelUp(guiController.offPeakScheduleScreen.xpath_Minute(), 1);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.swipeScrollWheelDown(guiController.offPeakScheduleScreen.xpath_Minute(), 2);
            guiController.offPeakScheduleScreen.weekdays.tap();

            //---Tap on Weekend----
            guiController.offPeakScheduleScreen.weekendDown.tap();
            guiController.swipeScrollWheelUp(guiController.offPeakScheduleScreen.xpath_Minute(), 1);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.swipeScrollWheelDown(guiController.offPeakScheduleScreen.xpath_Minute(), 2);
            guiController.offPeakScheduleScreen.weekendDown.tap();

            String weekday = guiController.offPeakScheduleScreen.weekDayTime.getTextValue();
            String Weekend = guiController.offPeakScheduleScreen.weekendTime.getTextValue();

            //---Tap on save button on off-peak schedule---
            guiController.offPeakScheduleScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);

            String weekdaySchedule = guiController.chargeManagementScreen.scheduleWeekday.getTextValue();
            String WeekendSchedule = guiController.chargeManagementScreen.scheduleWeekend.getTextValue();

            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));

            //---Tap on save button on set schedule
            guiController.chargeManagementScreen.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 12000);
            guiController.setChargeScheduleSuccessPopup.saveButton.tap();

            //---Verify on Dashboard---
            assertTrue(guiController.electricHomeScreen.selected.equals("True"));

            //---Verify after set schedule
            guiController.electricHomeScreen.setScheduleButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));

            // Boolean selected = guiController.electricHomeScreen.selected.isSelected();

        } else {
            appController.appFunctions.pauseTestExecution(1, 5000);
            String weekdaySchedule = guiController.chargeManagementScreen.scheduleWeekday.getTextValue();
            String WeekendSchedule = guiController.chargeManagementScreen.scheduleWeekend.getTextValue();
            guiController.chargeManagementScreen.offPeakWeekend.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            String weekday = guiController.offPeakScheduleScreen.weekDayTime.getTextValue();
            String Weekend = guiController.offPeakScheduleScreen.weekendTime.getTextValue();

            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));

            //---Tap on save button on set schedule
            guiController.chargeManagementScreen.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 6000);
            guiController.setChargeScheduleSuccessPopup.saveButton.tap();
            //---Verify on Dashboard---
            assertTrue(guiController.electricHomeScreen.selected.equals("True"));


            //---Verify after set schedule
            guiController.electricHomeScreen.setScheduleButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));


        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 Verify DC Charge Limit settings")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify DC Charge Limit setting available in Charge Schedules")
    void DCChargeLimit() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.swipe.up(1);
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitSetting.getTextValue().contains("Charge Limit Settings"), "Verify Charge Limit Setting exist");
        assertTrue(guiController.setChargeScheduleScreen.isDCChargeLimitPresent(), "Verify DC Charge Limit ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.5 Verify AC Charge Limit settings")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify AC Charge Limit setting available in Charge Schedules")
    void ACChargeLimit() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.swipe.up(1);
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitSetting.getTextValue().contains("Charge Limit Settings"), "Verify Charge Limit Setting exist");
        assertTrue(guiController.setChargeScheduleScreen.isACChargeLimitPresent(), "Verify AC Charge Limit ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.6 Verify Charge Limit settings")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify Charge Limit settings Text message")
    void ChargeLimit() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.swipe.up(1);
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitSetting.elementExists());
        //Click on Charge Limit Settings
        guiController.setChargeScheduleScreen.ChargeLimitSetting.tap();
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitText.getTextValue().contains("Charging will be terminated when the vehicle reaches the battery level chosen for each charger type"), "Verify Charge Limit message");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.7 Set Charge Schedule success Charge Schedule 1 Temperature OFF")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Set Charge Schedule success Charge Schedule 1 Temperature OFF")
    void ChargeSchedule1TempOFF() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");

        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        guiController.setChargeScheduleScreen.StartDrivingtime.tap();


        if (!guiController.getxpathCheckedValue("(//android.widget.CompoundButton[@id='departure_edit_title_switch'])[2]")) {
            guiController.setChargeScheduleScreen.tapStartDriving2();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        //Click on multiple days if it is not selected

        if (!guiController.setChargeScheduleScreen.Monday.getClickableValue()) {
            guiController.setChargeScheduleScreen.Monday.tap();
        }

        guiController.setChargeScheduleScreen.Save.tap();

        if (guiController.setChargeScheduleScreen.Climatetoggle.getClickableValue()) {
            guiController.setChargeScheduleScreen.Climatetoggle.tap();
        }

        guiController.setChargeScheduleScreen.submitButton.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //--Successfull
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Electric Charge Schedule") && resultText.contains("successfully"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 Set Charge Schedule success Vehicle Plugged-in")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Set Charge Schedule success Vehicle Plugged-in")
    void ChargeSchedulevehiclepluggedin() {
        assertTrue(guiController.electricHomeScreen.pluggedNotpluggedin.getTextValue().contains("Plugged In"), "Verified vehicle plugged in");
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.StartDrivingtime.tap();

        if (!guiController.setChargeScheduleScreen.StartDrivingone.getCheckedValue()) {
            guiController.setChargeScheduleScreen.StartDrivingone.tap();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        if (!guiController.setChargeScheduleScreen.Monday.getClickableValue()) {
            guiController.setChargeScheduleScreen.Monday.tap();
        }

        guiController.setChargeScheduleScreen.Save.tap();

        guiController.setChargeScheduleScreen.submitButton.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //--Successfull
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("START RESULT: " + resultText);
        assertTrue(resultText.contains("Electric Charge Schedule") && resultText.contains("successfully"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 AE Set Charge Schedule Off Peak Times Charge only during Off Peak Hours ON")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - AE Set Charge Schedule Off Peak Times Charge only during Off Peak Hours ON")
    void OffpeakTimeChargeOnlyScheduling() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        if (!guiController.setChargeScheduleScreen.offpeakChargingtoggle.getCheckedValue()) {
            guiController.setChargeScheduleScreen.offpeakChargingtoggle.tap();
        }

        guiController.setChargeScheduleScreen.offpeaktimetext.tap();
        guiController.setChargeScheduleScreen.offpeakChargingpriority.tap();
        guiController.setChargeScheduleScreen.Save.tap();

        guiController.setChargeScheduleScreen.submitButton.tap();
        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 AE Set Charge Schedule Off Peak Times Charge only during Off Peak Hours OFF")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - AE Set Charge Schedule Off Peak Times Charge only during Off Peak Hours OFF")
    void OffpeakTimeChargeOnlySchedulingOFF() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        if (!guiController.setChargeScheduleScreen.offpeakChargingtoggle.getCheckedValue()) {
            guiController.setChargeScheduleScreen.offpeakChargingtoggle.tap();
        }

        guiController.setChargeScheduleScreen.offpeaktimetext.tap();
        guiController.setChargeScheduleScreen.offpeakChargingOnly.tap();
        guiController.setChargeScheduleScreen.Save.tap();

        guiController.setChargeScheduleScreen.submitButton.tap();
        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 Set Charge Schedule success Vehicle UnPlugged")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Set Charge Schedule success Vehicle UnPlugged")
    void ChargeSchedulevehicleNotpluggedin() {
        assertTrue(guiController.electricHomeScreen.pluggedNotpluggedin.getTextValue().contains("Not Plugged In"), "Verified vehicle Not plugged in");
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.StartDrivingtime.tap();

        if (!guiController.setChargeScheduleScreen.StartDrivingone.getCheckedValue()) {
            guiController.setChargeScheduleScreen.StartDrivingone.tap();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(), 2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(), 1);

        if (!guiController.setChargeScheduleScreen.Monday.getClickableValue()) {
            guiController.setChargeScheduleScreen.Monday.tap();
        }

        guiController.setChargeScheduleScreen.Save.tap();

        guiController.setChargeScheduleScreen.submitButton.tap();

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //--Successfull
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("START RESULT: " + resultText);
        assertTrue(resultText.contains("Electric Charge Schedule") && resultText.contains("successfully"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 Verify user can set Off-Peak Schedule and save the changes")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify user can set Off-Peak Schedule and save the changes")
    void OffpeakSchedueOFF() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        if (!guiController.setChargeScheduleScreen.offpeakChargingtoggle.getCheckedValue()) {
            guiController.setChargeScheduleScreen.offpeakChargingtoggle.tap();
        }

        guiController.setChargeScheduleScreen.offpeaktimetext.tap();
        guiController.setChargeScheduleScreen.offpeakChargingOnly.tap();
        guiController.setChargeScheduleScreen.Save.tap();

        guiController.setChargeScheduleScreen.submitButton.tap();
        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.9 Verify the content on AC/DC limit page for Configured VINs for Recall, and have a min SOC value returned")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify the content on AC/DC limit page for Configured VINs for Recall, and have a min SOC value returned")
    void DCChargeLimitContentFull() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.swipe.up(1);
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitSetting.getTextValue().contains("Charge Limit Settings"), "Verify Charge Limit Setting exist");
        assertTrue(guiController.setChargeScheduleScreen.isDCChargeLimitPresent(), "Verify DC Charge Limit ");
        guiController.setChargeScheduleScreen.DCChargeLimittext.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.setChargeScheduleScreen.Usetheslidertext.elementExists(), "Use the slider text is present");
        assertTrue(guiController.setChargeScheduleScreen.SafetyWarning.elementExists(), "Safety warning text is present");
        guiController.setChargeScheduleScreen.tapBackbutton();
        assertTrue(guiController.setChargeScheduleScreen.isACChargeLimitPresent(), "Verify DC Charge Limit ");
        guiController.setChargeScheduleScreen.ACChargeLimittext.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.setChargeScheduleScreen.Usetheslidertext.elementExists(), "Use the slider text is present");
        assertTrue(guiController.setChargeScheduleScreen.SafetyWarning.elementExists(), "Safety warning text is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.10 Verify the content on AC/DC limit page for Non-Configured VINs with min SOC value returned")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify the content on AC/DC limit page for Non-Configured VINs with min SOC value returned")
    void ACDCChargeLimitContentFull() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.swipe.up(1);
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitSetting.getTextValue().contains("Charge Limit Settings"), "Verify Charge Limit Setting exist");
        assertTrue(guiController.setChargeScheduleScreen.isDCChargeLimitPresent(), "Verify DC Charge Limit ");
        guiController.setChargeScheduleScreen.DCChargeLimittext.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.setChargeScheduleScreen.Usetheslidertext.elementExists(), "Use the slider text is present");
        assertTrue(guiController.setChargeScheduleScreen.SafetyWarning.elementExists(), "Safety warning text is present");
        guiController.setChargeScheduleScreen.tapBackbutton();
        assertTrue(guiController.setChargeScheduleScreen.isACChargeLimitPresent(), "Verify DC Charge Limit ");
        guiController.setChargeScheduleScreen.ACChargeLimittext.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.setChargeScheduleScreen.Usetheslidertext.elementExists(), "Use the slider text is present");
        assertTrue(guiController.setChargeScheduleScreen.SafetyWarning.elementExists(), "Safety warning text is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.11 Verify the content on AC/DC limit page for Non-Configured VINs with no min SOC value returned")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify the content on AC/DC limit page for Non-Configured VINs with min SOC value returned")
    void ACDCUseTheSliderContentFull() {
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.setChargeScheduleScreen.swipe.up(1);
        assertTrue(guiController.setChargeScheduleScreen.ChargeLimitSetting.getTextValue().contains("Charge Limit Settings"), "Verify Charge Limit Setting exist");
        assertTrue(guiController.setChargeScheduleScreen.isDCChargeLimitPresent(), "Verify DC Charge Limit ");
        guiController.setChargeScheduleScreen.DCChargeLimittext.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.setChargeScheduleScreen.Usetheslidertext.elementExists(), "Use the slider text is present");
        guiController.setChargeScheduleScreen.tapBackbutton();
        assertTrue(guiController.setChargeScheduleScreen.isACChargeLimitPresent(), "Verify DC Charge Limit ");
        guiController.setChargeScheduleScreen.ACChargeLimittext.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.setChargeScheduleScreen.Usetheslidertext.elementExists(), "Use the slider text is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.11 Verify the content on AC/DC limit page for Non-Configured VINs with no min SOC value returned")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify the content on AC/DC limit page for Non-Configured VINs with min SOC value returned")
    void TaptosetTextunderoffpeakcharging() {
        guiController.homeScreen.Dismiss.tap();
        assertTrue(guiController.electricHomeScreen.setScheduleButton.elementExists(), "verify Charge Schedules button is present");
        //Click on Charge Schedules
        guiController.electricHomeScreen.setScheduleButton.tap();
        assertTrue(guiController.setChargeScheduleScreen.offpeakChargingtoggle.elementExists(), "Off peak charging is displayed");
        assertTrue(guiController.setChargeScheduleScreen.Taptoset.getTextValue().contains("Tap to set"), "Tap to set is displayed");
    }

    /*@RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.11 Veify the user able to turn OFF both driving times toggle.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Veify the user able to turn OFF both driving times toggle.")
    void DrivingTimetoggle() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.electricHomeScreen.StartDrivingarrow.tap();
        assertTrue(guiController.electricHomeScreen.StartDriving1toggle(),"toggle is displayed");
        assertTrue(guiController.electricHomeScreen.StartDriving2toggle(),"toggle is displayed");
    }*/
}