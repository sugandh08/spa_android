package tests.myh_app_refresh_regression_suite.ev_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Remote Start Test")
@Feature("Remote Start")
@DisplayName("Remote Start")
public class RemoteStart extends TestController {
    private Profile profile;
    private String incorrectPin;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.1 Remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithClimateOffDefrostOffHeatedServicesOff() {
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(false, false, false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.2 Remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithClimateOnDefrostOffHeatedServicesOff() {
        guiController.electricHomeScreen.remoteStartButton.tap();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true, false, false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.6.3 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.electricHomeScreen.remoteStartButton.tap();
            guiController.remoteStartSettingsScreen.submitButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.6.1 Verify 'HI' temperature on vehicle via remote actions")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify 'HI' temperature on vehicle")
    void HItemperatureOnVehicle() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        guiController.remotePresetsScreen.remoteStartWithoutPresetsText.tap();
        guiController.swipeScrollWheelDown(guiController.climateSettingsScreen.TEMPERATURE_XPATH,22);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.climateSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();
        guiController.electricHomeScreen.vehicleStatusButton.tap();

    }
}
