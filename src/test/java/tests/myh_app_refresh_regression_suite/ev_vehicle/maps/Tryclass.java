package tests.myh_app_refresh_regression_suite.ev_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Tryclass extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.1  Dealers Page redirected successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealers Page redirected successfully")
    void getexample() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(3, 3000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"));
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.scheduleServiceLink.elementExists();
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
       // System.out.println(guiController.dealerLocatorScreen.gettext());
        String text =guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='DOWNEY HYUNDAI']");
        System.out.println(text);
    }
}