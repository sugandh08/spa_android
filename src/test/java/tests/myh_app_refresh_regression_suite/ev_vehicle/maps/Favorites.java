package tests.myh_app_refresh_regression_suite.ev_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Favorites extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.5.1 Favorite POI")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Favorite POI")
    @Description("MyHyundai - Verify delete Favorite POI")
    void deleteFavoritePOI(){
        guiController.mapOptions.clickOnFavoritesButton();
        assertTrue(guiController.favoritesPOIScreen.favoritePointsOfInterestLabelText.elementExists());
        guiController.favoritesPOIScreen.mapListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        if (guiController.favoritesPOIScreen.poiExistsToList(0)){
            guiController.favoritesPOIScreen.tap.elementByXpath(guiController.favoritesPOIScreen.xpathOfFavoritePOI(0));
            appController.appFunctions.pauseTestExecution(1, 3000);
        }
            guiController.favoritesPOIScreen.arrowButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.favoritesPOIScreen.deleteAndSaveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.favoritesPOIScreen.saveAndDeleteButtonText.getTextValue().equalsIgnoreCase("Save"));
    }
}
