package tests.myh_app_refresh_regression_suite.ev_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Dealers extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.1  Dealers Page redirected successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealers Page redirected successfully")
    void dealersPageRenderedSuccessfully() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.2  Send Dealer Location to car")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Send Dealer Location to car")
    void sendDealerLocationToCar() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocationLabelText.elementExists());
        guiController.dealerLocatorScreen.sendButton1.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.3  Search Dealer Location using ZipCode")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Search Dealer Location using ZipCode")
    void searchDealerLocationUsingZipCode() {
        String zipCode = "92844";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.address.getTextValue().contains(zipCode));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.4  Search Dealer Location is showing in list")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealer Location is showing in list")
    void dealerLocationIsShowingInMapList() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String locationText = guiController.dealerLocatorScreen.dealerLocationLabelText.getTextValue();
        guiController.dealerLocatorScreen.mapListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + locationText + "']").contains(locationText));
        appController.appFunctions.pauseTestExecution(1, 2000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.5  verify dealer Schedule Service Link Is Clickable")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify dealer Schedule Service Link Is Clickable")
    void dealerScheduleServiceLinkIsClickable() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.scheduleServiceLink.elementExists();
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        assertTrue(guiController.scheduleServiceScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.6  Verify dealer shop accessories link is clickable")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify dealer shop accessories link is clickable")
    void dealerShopAccessoriesLinkIsClickable() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.shopAccessories.elementExists();
        guiController.dealerLocatorScreen.shopAccessories.tap();
        assertTrue(guiController.accessoriesScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.7 Verify Preferred dealer under dealer locator screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Preferred dealer under dealer locator screen")
    void preferredDealerUnderDealerLocatorScreen() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        assertTrue(guiController.dealerLocatorScreen.dealerName.elementExists(), "Verified dealer name is displayed");
        assertTrue(guiController.dealerLocatorScreen.miles.getTextValue().contains("mi"), "Verified distance in miles displayed");
        assertTrue(guiController.dealerLocatorScreen.starRating.elementExists(), "Verified star rating is displayed.");
        assertTrue(guiController.dealerLocatorScreen.locationWithIcon.elementExists(), "Verified dealer address with location  icon is displayed");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is highlighted.");
        assertTrue(guiController.dealerLocatorScreen.arrowButton.elementExists(), "Verified up arrow button is displayed ");
        guiController.dealerLocatorScreen.arrowButton.tap();
        assertTrue(guiController.dealerLocatorScreen.scheduleServiceLink.elementExists(), "Verified Service schedule link is displayed.");
        assertTrue(guiController.dealerLocatorScreen.sendToCar.elementExists(), "Verified send to Car is displayed");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.9 Verify  making non-preferred dealer as preferred dealer under dealer locator screen by entering Zip Code")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  making non-preferred dealer as preferred dealer under dealer locator screen by entering Zip Code")
    void makingNonPreferredDealerAsPreferredDealer() {
        String zipCode = "92844";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 8000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        System.out.println(nonPreferredDealerName);
        guiController.dealerLocatorScreen.preferredDealerStarIcon.tap();
        assertTrue(guiController.dealerLocatorScreen.setAsPreferredPopUp.elementExists(), "Set As preferred pop up is displayed");
        System.out.println("Before pop up");
        appController.appFunctions.pauseTestExecution(1, 10000);
        /*String text = guiController.getTextUsingXpath(guiController.dealerLocatorScreen.xpathOfPreferredPopup());
        assertTrue(text.contains("Confirms this as your"),"");*/
        //assertTrue(guiController.dealerLocatorScreen.preferredPopupMsg.elementExists(),"");
       /* System.out.println("After pop up");
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.okBtnPopup.elementExists(), "Ok btn is displayed");
        System.out.println("OK pop up");
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.cancelBtbPopUp.elementExists(), "Cancel Btn is displayed");
        System.out.println("Cancel up");*/
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.dealerLocatorScreen.okBtnPopup.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String makingNonPreferredDealerNameAsPreferred = guiController.dealerLocatorScreen.dealerName.getTextValue();
        System.out.println(makingNonPreferredDealerNameAsPreferred);
        assertEquals(makingNonPreferredDealerNameAsPreferred, nonPreferredDealerName, " New search dealer should become preferred dealer is displayed");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is highlighted.");
        guiController.dealerLocatorScreen.backButton.tap();
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 8000);
        String dealerNameAfterBack = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertEquals(dealerNameAfterBack, makingNonPreferredDealerNameAsPreferred, "The new selected dealer detail screen is displayed");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.8 Verify  non-preferred dealer under dealer locator screen by entering Zip Code")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  non-preferred dealer under dealer locator screen by entering Zip Code")
    void nonPreferredDealerUnderDealerLocatorEnteringZipcode() {
        String zipCode = "90001";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 8000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertTrue(!preferredDealerName.equals(nonPreferredDealerName), ". New Dealer detail screen should be visible according to the search result is displayed");
        assertTrue(guiController.dealerLocatorScreen.dealerName.elementExists(), "Verified dealer name is displayed");
        assertTrue(guiController.dealerLocatorScreen.miles.getTextValue().contains("mi"), "Verified distance in miles displayed");
        assertTrue(guiController.dealerLocatorScreen.starRating.elementExists(), "Verified star rating is displayed.");
        assertTrue(guiController.dealerLocatorScreen.locationWithIcon.elementExists(), "Verified dealer address with location  icon is displayed");
        assertTrue(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is not highlighted.");
        assertTrue(guiController.dealerLocatorScreen.arrowButton.elementExists(), "Verified up arrow button is displayed ");
        guiController.dealerLocatorScreen.arrowButton.tap();
        assertTrue(guiController.dealerLocatorScreen.scheduleServiceLink.elementExists(), "Verified Service schedule link is displayed.");
        assertTrue(guiController.dealerLocatorScreen.sendToCar.elementExists(), "Verified send to Car is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.10 Verify  Schedule Service screen for preferred Dealer")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Schedule Service screen for preferred Dealer")
    void scheduleServiceForPreferredDealer() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        String preferredDealerTxt = preferredDealerName.toUpperCase();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        String dealerNameFromScheduleServiceScreen = guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='" + preferredDealerTxt + "']");
        System.out.println(dealerNameFromScheduleServiceScreen);
        assertEquals(dealerNameFromScheduleServiceScreen, preferredDealerTxt, "Schedule Service page should open with the detail of same selected dealer present is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.11 Verify  Schedule Service screen for non-preferred Dealer by entering Zip Code")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Schedule Service screen for non-preferred Dealer by entering Zip Code")
    void scheduleServiceForNonPreferredDealer() {
        String zipCode = "90001";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        String nonPreferDealerNameTxt = nonPreferredDealerName.toUpperCase();
        System.out.println(nonPreferDealerNameTxt);
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        String dealerNameFromScheduleServiceScreen = guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='" + nonPreferDealerNameTxt + "']");
        System.out.println(dealerNameFromScheduleServiceScreen);
        assertEquals(dealerNameFromScheduleServiceScreen, nonPreferDealerNameTxt, "Schedule Service page  open with the detail of same selected dealer present is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.12 Verify  Schedule Service screen for new preferred Dealer by entering Zip Code")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Schedule Service screen for new preferred Dealer by entering Zip Code")
    void scheduleServiceForNewPreferredDealer() {
        String zipCode = "90001";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertTrue(!preferredDealerName.equals(nonPreferredDealerName), ". New Dealer detail screen should be visible according to the search result is displayed");
        guiController.dealerLocatorScreen.preferredDealerStarIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String text = guiController.getTextUsingXpath(guiController.dealerLocatorScreen.xpathOfPreferredPopup());
        assertTrue(text.contains("Confirms this as your"), "");
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.OKBtnExists(), "");
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.CancelBtnExists(), "");
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.dealerLocatorScreen.clickOnOKBtn();
        String makingNonPreferredDealerNameAsPreferred = guiController.dealerLocatorScreen.dealerName.getTextValue();
        System.out.println(makingNonPreferredDealerNameAsPreferred);
        assertEquals(makingNonPreferredDealerNameAsPreferred, nonPreferredDealerName, ". New search dealer should become preferred dealer is displayed");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is highlighted.");
        guiController.dealerLocatorScreen.clickOnAndroidBtn();
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 8000);
        String dealerNameAfterBack = guiController.dealerLocatorScreen.dealerName.getTextValue();
        String dealerNameTxtAfterBack = dealerNameAfterBack.toUpperCase();
        System.out.println(dealerNameTxtAfterBack);
        assertEquals(dealerNameAfterBack, makingNonPreferredDealerNameAsPreferred, "The new selected dealer detail screen is displayed");
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        String dealerNameFromScheduleServiceScreen = guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='" + dealerNameTxtAfterBack + "']");
        System.out.println(dealerNameFromScheduleServiceScreen);
        assertEquals(dealerNameFromScheduleServiceScreen, dealerNameTxtAfterBack, "Schedule Service page  open with the detail of same selected dealer present is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.13 Verify  Cancel functionality when select start icon to set new dealer as preferred dealer")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Cancel functionality when select start icon to set new dealer as preferred dealer")
    void cancelFunctionalityForNewPreferredDealer() {
        String zipCode = "90001";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertTrue(!preferredDealerName.equals(nonPreferredDealerName), ". New Dealer detail screen should be visible according to the search result is displayed");
        guiController.dealerLocatorScreen.preferredDealerStarIcon.tap();
        assertTrue(guiController.dealerLocatorScreen.setAsPreferredPopUp.elementExists(), "Set As preferred pop up is displayed");
        assertTrue(guiController.dealerLocatorScreen.preferredPopupMsg.elementExists(), "Message is displayed");
        assertTrue(guiController.dealerLocatorScreen.okBtnPopup.elementExists(), "Ok btn is displayed");
        assertTrue(guiController.dealerLocatorScreen.cancelBtbPopUp.elementExists(), "Cancel Btn is displayed");
        guiController.dealerLocatorScreen.cancelBtbPopUp.tap();
        assertFalse(guiController.dealerLocatorScreen.setAsPreferredPopUp.elementExists(), "Set As Preferred pop up is not displayed");
        String preferredDealerAfterCancelPopUp = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertEquals(preferredDealerAfterCancelPopUp, nonPreferredDealerName, "Verified not set the new dealer as preferred dealer");
        assertTrue(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), "Star icon is not highlighted");
        guiController.dealerLocatorScreen.backButton.tap();
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 8000);
        String dealerNameAfterBack = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertEquals(dealerNameAfterBack, preferredDealerName, "The old dealer detail screen is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.14 Verify the rating under preferred dealer page")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify the rating under preferred dealer page")
    void RatingUnderPreferredDealer() {
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        // Click on Review Link
        guiController.dealerLocatorScreen.Reviewlink.tap();
        appController.appFunctions.pauseTestExecution(2, 2000);
        assertTrue(guiController.dealerLocatorScreen.RateYourExperience.getTextValue().contains("Rate Your Experience"), "Verify Rate Your Experience should be visible");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.15 Verify the pop up message after clicking on Send to car under Dealer Locator")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify the pop up message after clicking on Send to car under Dealer Locator")
    void DismissandManageSubscription() {
    	//Click on Dealers
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        //Click on Arrow up button
        guiController.dealerLocatorScreen.arrowButton.tap();
        assertTrue(guiController.dealerLocatorScreen.sendToCar.elementExists(), "Verified send to Car is displayed");
        //Click on Send to CAR link
        guiController.dealerLocatorScreen.sendToCar.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.DismissBtnPresent(),"Dismiss Button is displayed ");
        assertTrue(guiController.dealerLocatorScreen.ManageSubscriptionBtnPresent(),"Manage subscription button is displayed");
        assertTrue(guiController.dealerLocatorScreen.MessageContentPresent(),"Guidance package is required to use this feature. Please visit MyHyundai.com to add this package. is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.16 Verify the link after clicking on Manage subscription button")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify the link after clicking on Manage subscription button")
    void ManageSubscription() {
        //Click on Dealers
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        //Click on Arrow up button
        guiController.dealerLocatorScreen.arrowButton.tap();
        assertTrue(guiController.dealerLocatorScreen.sendToCar.elementExists(), "Verified send to Car is displayed");
        //Click on Send to CAR link
        guiController.dealerLocatorScreen.sendToCar.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.DismissBtnPresent(),"Dismiss Button is displayed ");
        assertTrue(guiController.dealerLocatorScreen.ManageSubscriptionBtnPresent(),"Manage subscription button is displayed");
        assertTrue(guiController.dealerLocatorScreen.MessageContentPresent(),"Guidance package is required to use this feature. Please visit MyHyundai.com to add this package. is displayed");
        guiController.dealerLocatorScreen.tapManageSubscription();
        assertTrue(guiController.dealerLocatorScreen.ManageSubscription.elementExists(),"Verified Manage subscription page is displayed ");
    }


}