package tests.myh_app_refresh_regression_suite.ev_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChargeStations extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.2.1 Verify Nearby Charge Stations page is redirected successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Nearby Charge Stations page is redirected successfully")
    void nearbyFuelPageRedirectSuccessfully() {
        guiController.mapOptions.clickOnChargeStationsButton();
        //---
        appController.appFunctions.pauseTestExecution(1, 5000);
       assertTrue(guiController.chargeStationScreen.nearbyChargingStationsLabelText.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.2.2 Send charge station location to car")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Charge Station")
    @Description("MyHyundai - Verify Send charge station location to car")
    void sendChargeStationLocationToCar() {
        String zipCode="92844";
        guiController.mapOptions.clickOnChargeStationsButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.chargeStationScreen.searchEditTextbox.enterText(zipCode);
        guiController.chargeStationScreen.searchGlassIcon.tap();
        guiController.chargeStationScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.chargeStationScreen.sendButton1.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.2.3 Save gas location")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Save gas location")
    void saveChargeStationLocation() {
        String zipCode="92844";
        guiController.mapOptions.clickOnChargeStationsButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.chargeStationScreen.searchEditTextbox.enterText(zipCode);
        guiController.chargeStationScreen.searchGlassIcon.tap();
        guiController.chargeStationScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.chargeStationScreen.arrowIcon.tap();
        guiController.chargeStationScreen.saveAndDeleteButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        if (guiController.getTextUsingXpath("//*[@resource-id='android:id/message']").contains("location has already been saved")) {
            System.out.println("Gas location is already saved");
        }
        else {
            assertTrue(guiController.chargeStationScreen.saveAndDeleteButtonText.getTextValue().equalsIgnoreCase("Delete"));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.2.4 Charge Station Location Is Searchable Using ZipCode")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Charge Stations")
    @Description("MyHyundai - Verify Charge Station Location Is Searchable Using ZipCode")
    void chargeStationLocationIsSearchableUsingZipCode() {
        String zipCode="92804";
        guiController.mapOptions.clickOnChargeStationsButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.chargeStationScreen.searchEditTextbox.enterText(zipCode);
        guiController.chargeStationScreen.searchGlassIcon.tap();
        guiController.chargeStationScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        assertTrue(guiController.chargeStationScreen.chargeStationLocationText.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.2.5 Charge Station Location Is Searchable Using ZipCode And Is Showing In ListView")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Charge Station")
    @Description("MyHyundai - Verify Charge Station Location Is Searchable Using ZipCode And Is Showing In ListView")
    void chargeStationLocationIsShowingInListView() {
        guiController.mapOptions.clickOnChargeStationsButton();
        appController.appFunctions.pauseTestExecution(1, 7000);
        String locationText= guiController.chargeStationScreen.chargeStationLocationText.getTextValue();
        guiController.chargeStationScreen.chargeStationListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='"+locationText+"']").contains(locationText));
    }
    }
