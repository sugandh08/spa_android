package tests.myh_app_refresh_regression_suite.ev_vehicle.remote_actions;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2ElectricStandard


@Epic("Remote Start Test")
@Feature("Remote Start")
@DisplayName("Remote Start")
public class RemoteStart extends TestController {
    private Profile profile;
    private String incorrectPin;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.1 Remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithDefrostOffHeatedFeaturesOff() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        guiController.swipeScrollWheelUp(guiController.climateSettingsScreen.TEMPERATURE_XPATH, 4);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.climateSettingsScreen.setStartSettingsStates(false, false);
        guiController.climateSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("5.4.2 Remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
        void remoteStartSuccessWithDefrostOnHeatedServicesOn() {
            guiController.remoteCarControls.clickOnRemoteStartButton();
            guiController.swipeScrollWheelUp(guiController.climateSettingsScreen.TEMPERATURE_XPATH, 3);

            // wait a second for the engine duration text to update
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.climateSettingsScreen.setStartSettingsStates(true, true);
            guiController.climateSettingsScreen.submitButton.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            guiController.requestSentPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.electricHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE START RESULT: " + resultText);
            assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("5.4.3 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteCarControls.clickOnRemoteStartButton();
            guiController.remoteStartSettingsScreen.submitButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

   /* @Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.startButton);
        }

        @Nested
        @DisplayName("7.1.5 Remote start with incorrect PIN three times")
        class RemoteStartWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteStartWithIncorrectPinThreeTimes() {
                remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.startButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.1.5.1 Error popup is displayed saying how to reset PIN")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying how to reset PIN")
            void errorPopupIsDisplayedSayingHowToResetPin() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.1.5.2 User is restored access to remote_genesis_old commands after 5 minutes")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote_genesis_old commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.cancelButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                clickOnRemoteStart();
                guiController.remoteStartSettingsScreen.submitButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }
    }*/

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.4 Verify Engine Duration Removed on Remote preset screen for EV")
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Engine Duration Removed on Remote preset screen for EV")
    void EngineDurationRemoved() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        guiController.remotePresetsScreen.editRemotePresets(1);
        assertFalse(guiController.climateSettingsScreen.EngineDurationIsPresent(), "Verified Engine duration is not present in remote preset screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.5 Verify Presets options are displayed on tapping Remote Start Button via Fly-By Menu")
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Presets options are displayed on tapping Remote Start Button via Fly-By Menu")
    void Presetoptions() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.isPresent();
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
        //guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(4));
        assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.5 Verify user is able to save all presets settings one by one")
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify user is able to save all presets settings one by one")
    void presetsgettingupdatedsuccessfully() {

        //guiController.remoteCarControls.clickOnRemoteStartButton();
        //        appController.appFunctions.pauseTestExecution(1, 5000);
        //          for (int i=1;i<5;i++){
        //        guiController.remotePresetsScreen.editRemotePresets(i);
        //        appController.appFunctions.pauseTestExecution(1, 1000);
        //        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 3);
        //        appController.appFunctions.pauseTestExecution(1, 1000);
        //        guiController.remoteStartSettingsScreen.setStartSettingsState(true,true,true);
        //        guiController.remoteStartSettingsScreen.clickonSubmit();
        //        appController.appFunctions.pauseTestExecution(1, 3000);
        //        assertTrue(guiController.remotePresetsScreen.getPresetsSettingsStates(true,true,true,i));
        //}
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.editRemotePresets(1);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsState(true, true, true);
        guiController.remoteStartSettingsScreen.clickonSubmit();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.remotePresetsScreen.getPresetsSettingsStates(true, true, true, 1));
        guiController.remotePresetsScreen.editRemotePresets(2);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsState(true, true, true);
        guiController.remoteStartSettingsScreen.clickonSubmit();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.remotePresetsScreen.getPresetsSettingsStates(true, true, true, 2));
        guiController.remotePresetsScreen.editRemotePresets(3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsState(true, true, false);
        guiController.remoteStartSettingsScreen.clickonSubmit();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.remotePresetsScreen.getPresetsSettingsStates(true, true, false, 3));
        guiController.remotePresetsScreen.editRemotePresets(4);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreen.setStartSettingsState(true, true, false);
        guiController.remoteStartSettingsScreen.clickonSubmit();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.remotePresetsScreen.getPresetsSettingsStates(true, true, false, 4));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.6 Verify rename submit to save on remote preset screen")
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify rename submit to save on remote preset screen")
    void RemoteSavebutton() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remotePresetsScreen.editRemotePresets(1);
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(guiController.remoteStartSettingsScreen.SavebuttonPresent(), "Verified Save text exist");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.7 Verify the ON/OFF toggle for EV Climate Start")
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify the ON/OFF toggle for EV Climate Start")
    void OnOffToggle() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remotePresetsScreen.editRemotePresets(1);
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(guiController.remotePresetsScreen.Temperaturetoggle.elementExists(),"Verified temperature toggle is present");
        }

}