package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyDealer extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(2, 1000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(2, 1000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.1 Verify MyDealer under Hamburger")
    @Severity(SeverityLevel.NORMAL)
    @Story("Hamburger Settings")
    @Description("MyHyundai - Verify MyDealer under Hamburger")
    void MyDealer()
    {
        assertTrue(guiController.menuScreen.MyDealer.getTextValue().contains("My Dealer"), "Verify My Dealer link present in hamburger ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.2 Verify Preferred dealer under dealer locator screen from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Preferred dealer under dealer locator screen from hamburger MyDealer link")
    void preferredDealerUnderDealerLocatorScreenfromhamburger() {
        //Click on MyDealers present in hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        assertTrue(guiController.dealerLocatorScreen.dealerName.elementExists(), "Verified dealer name is displayed");
        assertTrue(guiController.dealerLocatorScreen.miles.getTextValue().contains("mi"), "Verified distance in miles displayed");
        assertTrue(guiController.dealerLocatorScreen.starRating.elementExists(), "Verified star rating is displayed.");
        assertTrue(guiController.dealerLocatorScreen.locationWithIcon.elementExists(), "Verified dealer address with location  icon is displayed");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is highlighted.");
        assertTrue(guiController.dealerLocatorScreen.arrowButton.elementExists(), "Verified up arrow button is displayed ");
        //Click on up arrow
        guiController.dealerLocatorScreen.arrowButton.tap();
        assertTrue(guiController.dealerLocatorScreen.scheduleServiceLink.elementExists(), "Verified Service schedule link is displayed.");
        assertTrue(guiController.dealerLocatorScreen.sendToCar.elementExists(), "Verified send to Car is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.3 Verify  non-preferred dealer under dealer locator screen by entering Zip Code from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  non-preferred dealer under dealer locator screen by entering Zip Code from hamburger MyDealer link")
    void nonPreferredDealerUnderDealerLocatorEnteringZipcodefromhamburger() {
        String zipCode = "90001";
        //Click on MyDealers present in hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        //Click on glass icon to search Dealer by Zipcode
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertTrue(!preferredDealerName.equals(nonPreferredDealerName), ". New Dealer detail screen should be visible according to the search result is displayed");
        assertTrue(guiController.dealerLocatorScreen.dealerName.elementExists(), "Verified dealer name is displayed");
        assertTrue(guiController.dealerLocatorScreen.miles.getTextValue().contains("mi"), "Verified distance in miles displayed");
        assertTrue(guiController.dealerLocatorScreen.starRating.elementExists(), "Verified star rating is displayed.");
        assertTrue(guiController.dealerLocatorScreen.locationWithIcon.elementExists(), "Verified dealer address with location  icon is displayed");
        assertTrue(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is not highlighted.");
        assertTrue(guiController.dealerLocatorScreen.arrowButton.elementExists(), "Verified up arrow button is displayed ");
        //Click on up arrow
        guiController.dealerLocatorScreen.arrowButton.tap();
        assertTrue(guiController.dealerLocatorScreen.scheduleServiceLink.elementExists(), "Verified Service schedule link is displayed.");
        assertTrue(guiController.dealerLocatorScreen.sendToCar.elementExists(), "Verified send to Car is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.4 Verify  making non-preferred dealer as preferred dealer under dealer locator screen by entering Zip Code from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  making non-preferred dealer as preferred dealer under dealer locator screen by entering Zip Code from hamburger MyDealer link")
    void makingNonPreferredDealerAsPreferredDealerfromhamburger() {
        String zipCode = "20001";
        //Click on MyDealers present in hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        //Click on glass icon to search Dealer by Zipcode
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        System.out.println(nonPreferredDealerName);
        //Click on star icon to make a preferred dealer.
        guiController.dealerLocatorScreen.preferredDealerStarIcon.tap();
        assertTrue(guiController.dealerLocatorScreen.setAsPreferredPopUp.elementExists(), "Set As preferred pop up is displayed");
        System.out.println("Before pop up");
        appController.appFunctions.pauseTestExecution(1, 10000);
        appController.appFunctions.pauseTestExecution(1, 3000);
        //Click on OK button to make the Dealer preferred dealer
        guiController.dealerLocatorScreen.okBtnPopup.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String makingNonPreferredDealerNameAsPreferred = guiController.dealerLocatorScreen.dealerName.getTextValue();
        System.out.println(makingNonPreferredDealerNameAsPreferred);
        assertEquals(makingNonPreferredDealerNameAsPreferred, nonPreferredDealerName, " New search dealer should become preferred dealer is displayed");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is highlighted.");
        //Click on Back button
        guiController.dealerLocatorScreen.clickOnAndroidBtn();
        //click on Hamburger menu option
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        //Click on MyDealers present in Hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String dealerNameAfterBack = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertEquals(dealerNameAfterBack, makingNonPreferredDealerNameAsPreferred, "The new selected dealer detail screen is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.5 Verify  Schedule Service screen for preferred Dealer from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Schedule Service screen for preferred Dealer from hamburger MyDealer link")
    void scheduleServiceForPreferredDealerfromhamburger() {
        //Click on MyDealers present in Hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        String preferredDealerTxt = preferredDealerName.toUpperCase();//It will store the prefeereddealername in Uppercase.
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        // Click on up arrow
        guiController.dealerLocatorScreen.arrowButton.tap();
        //Click on Schedule service present in dealers info
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        String dealerNameFromScheduleServiceScreen = guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='" + preferredDealerTxt + "']");
        System.out.println(dealerNameFromScheduleServiceScreen);
        assertEquals(dealerNameFromScheduleServiceScreen, preferredDealerTxt, "Schedule Service page should open with the detail of same selected dealer present is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.6 Verify  Schedule Service screen for non-preferred Dealer by entering Zip Code from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Schedule Service screen for non-preferred Dealer by entering Zip Code from hamburger MyDealer link")
    void scheduleServiceForNonPreferredDealerfromhamburger() {
        String zipCode = "90001";
        //Click on MyDealers present in Hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        //Click on glass icon to search Dealer by Zipcode
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        String nonPreferDealerNameTxt = nonPreferredDealerName.toUpperCase();
        System.out.println(nonPreferredDealerName);
        System.out.println(nonPreferDealerNameTxt);
        //Click on arrow up
        guiController.dealerLocatorScreen.arrowButton.tap();
        //Click on Schedule service present in dealers info
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(3, 3000);
        String dealerNameFromScheduleServiceScreen = guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='" + nonPreferDealerNameTxt + "']");
        System.out.println(dealerNameFromScheduleServiceScreen);
        assertEquals(dealerNameFromScheduleServiceScreen, nonPreferDealerNameTxt, "Schedule Service page  open with the detail of same selected dealer present is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.7 Verify  Schedule Service screen for new preferred Dealer by entering Zip Code from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify  Schedule Service screen for new preferred Dealer by entering Zip Code from hamburger MyDealer link")
    void scheduleServiceForNewPreferredDealerfromhamburger() {
        String zipCode = "90001";
        //Click on MyDealers present in Hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        //Click on glass icon to search Dealer by Zipcode
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertTrue(!preferredDealerName.equals(nonPreferredDealerName), ". New Dealer detail screen should be visible according to the search result is displayed");
        //Click on star icon to make a preferred dealer.
        guiController.dealerLocatorScreen.preferredDealerStarIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String text = guiController.getTextUsingXpath(guiController.dealerLocatorScreen.xpathOfPreferredPopup());
        assertTrue(text.contains("Confirms this as your"), "Verify the preferred dealer confirmation popup");
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.OKBtnExists(), "Verified ok button exist in popup ");
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.CancelBtnExists(), "Verified Cancel button exist in popup");
        appController.appFunctions.pauseTestExecution(1, 3000);
        //Click on Ok button in the confirmation message popup
        guiController.dealerLocatorScreen.clickOnOKBtn();
        String makingNonPreferredDealerNameAsPreferred = guiController.dealerLocatorScreen.dealerName.getTextValue();
        System.out.println(makingNonPreferredDealerNameAsPreferred);
        assertEquals(makingNonPreferredDealerNameAsPreferred, nonPreferredDealerName, ". New search dealer should become preferred dealer is displayed");
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified star icon is highlighted.");
        //Click on back button
        guiController.dealerLocatorScreen.clickOnAndroidBtn();
        //Click on hamburger icon
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        //Click on MyDealers present in hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        String dealerNameAfterBack = guiController.dealerLocatorScreen.dealerName.getTextValue();
        String dealerNameTxtAfterBack = dealerNameAfterBack.toUpperCase();
        System.out.println(dealerNameTxtAfterBack);
        assertEquals(dealerNameAfterBack, makingNonPreferredDealerNameAsPreferred, "The new selected dealer detail screen is displayed");
        //Click on up arrow
        guiController.dealerLocatorScreen.arrowButton.tap();
        //Click on Schedule service present in dealers info
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        String dealerNameFromScheduleServiceScreen = guiController.getTextUsingXpath("//*[@class='android.widget.TextView' and @text='" + dealerNameTxtAfterBack + "']");
        System.out.println(dealerNameFromScheduleServiceScreen);
        assertEquals(dealerNameFromScheduleServiceScreen, dealerNameTxtAfterBack, "Schedule Service page  open with the detail of same selected dealer present is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.8 Verify Cancel functionality when select start icon to set new dealer as preferred dealer from hamburger MyDealer link")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Cancel functionality when select start icon to set new dealer as preferred dealer from hamburger MyDealer link")
    void cancelFunctionalityForNewPreferredDealerfromhamburger() {
        String zipCode = "20001";
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"), "Verified Dealer Locator screen should be visible");
        String preferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertFalse(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), " Verified Preferred dealer should appear on Dealer locator screen.");
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        //Click on glass icon to search Dealer by Zipcode
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String nonPreferredDealerName = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertTrue(!preferredDealerName.equals(nonPreferredDealerName), ". New Dealer detail screen should be visible according to the search result is displayed");
        //Click on star icon to make a preferred dealer.
        guiController.dealerLocatorScreen.preferredDealerStarIcon.tap();
        assertTrue(guiController.dealerLocatorScreen.setAsPreferredPopUp.elementExists(), "Set As preferred pop up is displayed");
        assertTrue(guiController.dealerLocatorScreen.preferredPopupMsg.elementExists(), "Message is displayed");
        assertTrue(guiController.dealerLocatorScreen.okBtnPopup.elementExists(), "Ok btn is displayed");
        assertTrue(guiController.dealerLocatorScreen.cancelBtbPopUp.elementExists(), "Cancel Btn is displayed");
       //Click on Cancel button present in popup message
        guiController.dealerLocatorScreen.cancelBtbPopUp.tap();
        assertFalse(guiController.dealerLocatorScreen.setAsPreferredPopUp.elementExists(), "Set As Preferred pop up is not displayed");
        String preferredDealerAfterCancelPopUp = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertEquals(preferredDealerAfterCancelPopUp, nonPreferredDealerName, "Verified not set the new dealer as preferred dealer");
        assertTrue(guiController.dealerLocatorScreen.preferredDealerStarIcon.getClickableValue(), "Star icon is not highlighted");
        //Click on Back button
        guiController.dealerLocatorScreen.clickOnAndroidBtn();
        //Click on Hamburger menu option
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        //Click on MyDealers present on hamburger
        guiController.menuScreen.MyDealer.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        String dealerNameAfterBack = guiController.dealerLocatorScreen.dealerName.getTextValue();
        assertEquals(dealerNameAfterBack, preferredDealerName, "The old dealer detail screen is displayed");
    }

}