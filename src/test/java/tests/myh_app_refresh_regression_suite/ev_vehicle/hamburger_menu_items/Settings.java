package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Settings extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(2, 1000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(2, 1000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.8.1 Verify Setting under Hamburger")
    @Severity(SeverityLevel.NORMAL)
    @Story("Hamburger Settings")
    @Description("MyHyundai - Verify Setting under Hamburger")
    void Settings()
    {
        assertTrue(guiController.menuScreen.settingsButton.elementExists());
        guiController.menuScreen.settingsButton.tap();
        assertTrue(guiController.menuScreen.settings.getTextValue().contains("Settings"), "Verify Settings screen");
    }

}