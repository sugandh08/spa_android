package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class VirtualAssistantHamburger extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(2, 1000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Verify Hamburger Virtual assistant navigation to Hyundai Virtual assistant")
    @Severity(SeverityLevel.NORMAL)
    @Story("Hyundai Virtual Assistant Chatbox")
    @Description("MyHyundai - Verify Hamburger Virtual assistant navigation to Hyundai Virtual assistant")
    void VirtualAssistantFull() {
        guiController.menuScreen.swipe.fromTopEdge(2);
        assertTrue(guiController.menuScreen.VirtualAssistant.elementExists(),"Verify Virtual Assistant is available in Hamburger");
        //Click on Virtual Assistant
        guiController.menuScreen.VirtualAssistant.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.homeScreen.isHyundaiVirtualAssistantHeaderPresent(), "Verify Hamburger Virtual assistant navigates to Hyundai Virtual Assistant page");
        String savedMsg = guiController.getTextUsingXpath("//*[@class='android.view.View' and @text='Blue Link']");
        guiController.homeScreen.clickOnBlueLink();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.homeScreen.clickonTermsofUse();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.homeScreen.isTermsofUsePagePresent(), "Verify Terms Of Use page");
        System.out.println("Terms of use page open");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.2 Verify Hamburger Virtual assistant navigation to Hyundai Virtual assistant")
    @Severity(SeverityLevel.NORMAL)
    @Story("Hyundai Virtual Assistant Chatbox")
    @Description("MyHyundai - Verify Hamburger Virtual assistant navigation to Hyundai Virtual assistant")
    void VirtualAssistant() {
        guiController.menuScreen.swipe.fromTopEdge(2);
        assertTrue(guiController.menuScreen.VirtualAssistant.elementExists(),"Verify Virtual Assistant is available in Hamburger");
        //Click on Virtual Assistant
        guiController.menuScreen.VirtualAssistant.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.homeScreen.isHyundaiVirtualAssistantHeaderPresent(), "Verify Hamburger Virtual assistant navigates to Hyundai Virtual Assistant page");
        String savedMsg = guiController.getTextUsingXpath("//*[@class='android.view.View' and @text='Blue Link']");
        guiController.homeScreen.clickOnBlueLink();
        appController.appFunctions.pauseTestExecution(2, 3000);
    }
}
