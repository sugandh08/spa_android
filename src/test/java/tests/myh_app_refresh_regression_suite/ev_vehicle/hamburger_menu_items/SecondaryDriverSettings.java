package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayName("Secondary Driver Test ")
public class SecondaryDriverSettings extends TestController {
    private Profile profile;

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);
    String email = "test_user" + randomInt + "@mailnesia.com";

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);
        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(70);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.1 Edit Permissions for Secondary Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify editing Permissions for Secondary Driver")
    void editPermissionsOfAdditionalDriver() {

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);
        guiController.profileScreen_gen2Point5.clickEditPermissionsButton("1");
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.editPermissionScreen_gen2Point5.isPresent());
        String toggleValue = guiController.editPermissionScreen_gen2Point5.clickYesNoRadioButton("VALET ALERT");

        if (toggleValue.equals("yes")) {
            Boolean toggleValueBeforeSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfYesRadioButton("VALET ALERT");
            guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
            guiController.editPermissionScreen_gen2Point5.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            appController.appFunctions.tapAndroidBackButton();
            guiController.profileScreen_gen2Point5.clickEditPermissionsButton("1");
            appController.appFunctions.pauseTestExecution(1, 5000);
            Boolean toggleValueAfterSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfYesRadioButton("VALET ALERT");
            System.out.println("After submit" + toggleValueAfterSubmit);
            assertTrue(toggleValueBeforeSubmit.equals(toggleValueAfterSubmit),
                    "Permissions not Updated");
        } else {
            Boolean toggleValueBeforeSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfNoRadioButton("VALET ALERT");
            System.out.println("before submit" + toggleValueBeforeSubmit);
            guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
            guiController.editPermissionScreen_gen2Point5.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            appController.appFunctions.tapAndroidBackButton();
            guiController.profileScreen_gen2Point5.clickEditPermissionsButton("1");
            appController.appFunctions.pauseTestExecution(1, 4000);
            Boolean toggleValueAfterSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfNoRadioButton("VALET ALERT");
            System.out.println("After submit" + toggleValueAfterSubmit);
            assertTrue(toggleValueBeforeSubmit.equals(toggleValueAfterSubmit),
                    "Permissions not Updated");
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.2 Invite Additional Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify inviting Additional Driver")
    void inviteAdditionalDriver() {
        String fName = "test";
        String lName = "user";

        guiController.profileScreen_gen2Point5.clickRemoveDriverButton();

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(4);
        guiController.profileScreen_gen2Point5.inviteAdditionalDriverButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);

        assertTrue(guiController.inviteAdditionalDriverScreen_gen2Point5.isPresent());
        guiController.inviteAdditionalDriverScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);
        appController.appFunctions.pauseTestExecution(1, 8000);
        assertTrue(guiController.getTextUsingXpath("(//*[@resource-id='com.stationdm.bluelink:id/rvAdditionalDrivers']/*/*/*/*[@resource-id='com.stationdm.bluelink:id/tvEmail' and @text='" + email + "'])[1]").equals(email), "Invitation Not sent");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.3 Remove Additional Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify removing Additional Driver")
    void removeAdditionalDriver() {
        String fName = "test";
        String lName = "user";
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(4);
        guiController.profileScreen_gen2Point5.inviteAdditionalDriverButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);

        assertTrue(guiController.inviteAdditionalDriverScreen_gen2Point5.isPresent());

        guiController.inviteAdditionalDriverScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);

        appController.appFunctions.pauseTestExecution(1, 10000);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
        String index = guiController.getTextUsingXpath(guiController.profileScreen_gen2Point5.getxPathOfIndexUsingEmail(email));

        assertTrue(guiController.getTextUsingXpath(guiController.profileScreen_gen2Point5.getXpathOfAdditionalDriverEmail(index)).equals(email)
                , "Invitation Not sent");
        guiController.profileScreen_gen2Point5.clickRemoveDriverButtonUsingEmail(email);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.profileScreen_gen2Point5.yesButton.tap();

        for (int i = 0; i < 3; i++) {

            appController.appFunctions.pauseTestExecution(1, 5000);
        }

        assertFalse(guiController.getTextUsingXpath(guiController.
                profileScreen_gen2Point5.getXpathOfAdditionalDriverEmail(index)).equals(email), "Driver not removed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.4 Additional Driver Message Content")
    @Severity(SeverityLevel.NORMAL)
    @Story("User Profile Information")
    @Description("MyHyundai - Additional Driver Message Content")
    void AdditionalDriverMessageContent()
    {
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(6);
        assertTrue(guiController.profileScreen_gen2Point5.ReadMoreTxt.getTextValue().contains("Read more"),"Verify Read more Content");
        appController.appFunctions.pauseTestExecution(1,3000);
        // Click on Read More link present in Additional Drivers
        guiController.profileScreen_gen2Point5.ReadMoreTxt.tap();
        appController.appFunctions.pauseTestExecution(1,3000);
        assertTrue(guiController.getTextUsingXpath("//*[@resource-id='android:id/message']").contains("As a primary Blue Link subscriber"), "verify the content of Additional Drivers");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.5 Renewal Date Under BlueLink Package")
    @Severity(SeverityLevel.NORMAL)
    @Story("User Profile Information")
    @Description("MyHyundai - Renewal Date Under BlueLink Package")
    void RenewalDateUnderBlueLink()
    {
        String tvTitle = guiController.getTextUsingXpath("(//*[@resource-id='com.stationdm.bluelink:id/tvTitle'])[2]");
        assertTrue(guiController.getTextUsingXpath("(//*[@resource-id='com.stationdm.bluelink:id/tvTitle'])[2]").contains("BLUE LINK PACKAGES"),"Verify the Bluelink package existance");
        assertTrue(guiController.profileScreen_gen2Point5.renewalDateTxt.getTextValue().contains("Renewal Date"),"Verify Renewal Date content in BlueLink Packages");
    }

}

