package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserProfileInformation extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 1000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Validating Odometer at the top of menu screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Menu Screen")
    @Description("MyHyundai - Verify Odometer at the top of  menu screen")
    void odometerOnMenuScreen() {
        assertTrue(guiController.menuScreen.odometerTextView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Validating Odometer reading at the top of menu screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Menu Screen")
    @Description("MyHyundai - Verify Odometer reading at the top of  menu screen")
    void odometerReadingOnMenuScreen() {
        assertTrue(guiController.menuScreen.odometerTextView.getTextValue().contains("mi"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Menu Screen")
    @Description("MyHyundai - Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    void Bluelinkunderprofile() {
        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(70);
        assertTrue(guiController.profileScreen_gen2Point5.BluelinkPackages.elementExists(),"Verified Bluelink package is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Verify validation of Customer Address within Profile Screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Menu Screen")
    @Description("MyHyundai - Verify validation of Customer Address within Profile Screen")
    void StateoptionsunderEditcontact() {
        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(10);
        guiController.profileScreen_gen2Point5.swipe.up(6);
        assertTrue(guiController.profileScreen_gen2Point5.editcontactinformation.elementExists(),"Verified edit contact information is displayed");
        guiController.profileScreen_gen2Point5.editcontactinformation.tap();
        guiController.profileScreen_gen2Point5.state.tap();
        assertTrue(guiController.profileScreen_gen2Point5.stateoptions.elementExists(),"Verified stateoptions are displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Verify Edit Blue Link Packages is removed from Burger menu")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Menu Screen")
    @Description("MyHyundai - Verify Edit Blue Link Packages is removed from Burger menu")
    void EditBluelinkPackage() {
        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(10);
        assertFalse(guiController.menuScreen.EditBluelinkPackage.elementExists(), "Edit Bluelink package is not present");
    }

}