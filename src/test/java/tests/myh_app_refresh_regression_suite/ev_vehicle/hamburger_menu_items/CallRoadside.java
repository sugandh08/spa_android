package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CallRoadside extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(2, 1000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(2, 1000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.4 Verify Call Roadside")
    @Severity(SeverityLevel.NORMAL)
    @Story("Hamburger Settings")
    @Description("MyHyundai - Verify Call Roadside")
    void CallRoadside()
    {

        guiController.menuScreen.swipe.up(3);
        assertTrue(guiController.menuScreen.callRoadsideButton.elementExists(),"Call Roadside  Button is displayed");
        //Click on Call Roadside
        guiController.menuScreen.callRoadsideButton.tap();
        assertTrue(guiController.menuScreen.CallRoadsideScreenisPresent(),"Call Roadside Popup is displayed");
        appController.appFunctions.pauseTestExecution(1,4000);
        assertTrue(guiController.menuScreen.CallBtnExists(),"Call Button is displayed");
        assertTrue(guiController.menuScreen.CancelBtnExists(),"Cancel Button is displayed");
        //Click on Call Button
        guiController.menuScreen.TapCallButton();
        appController.appFunctions.pauseTestExecution(1,2000);
        assertTrue(guiController.menuScreen.callingScreenExists(),"Verify Call screen with phone number is displayed");
    }

}