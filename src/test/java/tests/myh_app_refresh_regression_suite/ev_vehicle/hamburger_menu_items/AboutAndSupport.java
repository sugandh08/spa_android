package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AboutAndSupport extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.aboutAndSupportButton.tap();
        guiController.aboutAndSupportScreen.isPresent();
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 Validating Blue Link section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Blue Link Section on About & Support Page")
    void blueLinkSection() {
        String blueLinkInfo = guiController.aboutAndSupportScreen.blueLinkInformation.getTextValue();
        String blueLinkHowToVideo = guiController.aboutAndSupportScreen.blueLinkHowToVideos.getTextValue();
        guiController.aboutAndSupportScreen.blueLinkText.elementExists();
        guiController.aboutAndSupportScreen.blueLinkInformation.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.blueLinkInformationScreen.subtitleTextView.getTextValue().equals("Blue Link Info"));
        guiController.blueLinkInformationScreen.clickOnBackButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.aboutAndSupportScreen.blueLinkHowToVideos.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.blueLinkHowToVideosScreen.subtitle.getTextValue().equals("Blue Link How-To Videos"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.2 Validating Bluetooth section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Bluetooth Section on About & Support Page")
    void bluetoothSection() {
        String bluetoothAndMultimediaSupportText = guiController.aboutAndSupportScreen.bluetoothAndMultimediaSupport.getTextValue();
        assertTrue(guiController.aboutAndSupportScreen.bluetoothAndMultimediaSupport.getTextValue().equals(bluetoothAndMultimediaSupportText));

        guiController.aboutAndSupportScreen.bluetoothVideos.tap();

        if (guiController.getTextUsingXpath("//android.widget.TextView[@resource-id='android:id/message']").contains("Content unavailable at this time.")) {
            System.out.println("Content unavailable - Car Care Bluetooth button");
        }
        //sometimes error popup shows on clicking bluetooth option
        else {
            guiController.bluetoothScreen.subTitleText.elementExists();
            assertTrue(guiController.scheduleServiceScreen.subTitleTextView.getTextValue().contains("Bluetooth"));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.3 Validating GUIDES section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify GUIDES Section on About & Support Page")
    void guidesSection() {
        String gettingStartedGuide = guiController.aboutAndSupportScreen.guidesText.getTextValue();
        assertTrue(guiController.aboutAndSupportScreen.indicatorGuide.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.ownersManual.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.maintenanceInformation.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.warrantyInfo.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.gettingStartedGuide.getTextValue().equals(gettingStartedGuide));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.4 Validating hyundai resources section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify hyundai resources Section on About & Support Page")
    void hyundaiResourceSection() {
        guiController.aboutAndSupportScreen.swipe.up(2);
        guiController.aboutAndSupportScreen.hyundaiResourceText.elementExists();

        //Verify Getting Started
        guiController.aboutAndSupportScreen.gettingStarted.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.gettingStartedScreen.subTitleText.getTextValue().equals("Getting Started"));
        guiController.gettingStartedScreen.clickOnBackButton();

        //Verify Blue Link
        guiController.aboutAndSupportScreen.blueLink.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.blueLinkScreen.subTitleText.getTextValue().equals("Blue Link"));
        guiController.blueLinkScreen.clickOnBackButton();

        //Verify Vehicle Health
        guiController.aboutAndSupportScreen.vehicleHealth.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.vehicleHealthScreen.subTitleText.getTextValue().equals("Vehicle Health"));
        guiController.vehicleHealthScreen.clickOnBackButton();

        //Verify Manuals & Warranties
        guiController.aboutAndSupportScreen.manualsAndWarranties.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.manualsAndWarrantiesScreen.subTitleText.getTextValue().equals("Manuals & Warranties"));
        guiController.manualsAndWarrantiesScreen.clickOnBackButton();

        //Verify Bluetooth Assistance
        guiController.aboutAndSupportScreen.bluetoothAssistance.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.bluetoothAssistanceScreen.subTitleText.getTextValue().equals("Bluetooth Assistance"));
        guiController.bluetoothAssistanceScreen.clickOnBackButton();

        //Verify Multimedia and Navigation
        guiController.aboutAndSupportScreen.multimediaAndNavigation.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.multimediaAndNavigationScreen.subTitleText.getTextValue().equals("Multimedia and Navigation"));
        guiController.multimediaAndNavigationScreen.clickOnBackButton();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.5 Validating LINKS section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify LINKS Section on About & Support Page")
    void LinksSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        guiController.aboutAndSupportScreen.linksText.elementExists();
        guiController.aboutAndSupportScreen.hyundaiUSA.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.hyundaiUSAScreen.subTitleText.getTextValue().equals("Hyundai USA"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.6 Validating Contact Us section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Contact Us Section on About & Support Page")
    void contactUsSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String emailAppSupport = guiController.aboutAndSupportScreen.emailAppSupport.getTextValue();
        if (guiController.aboutAndSupportScreen.contactUsText.elementExists()) {
            assertTrue(guiController.aboutAndSupportScreen.emailAppSupport.getTextValue().equals(emailAppSupport));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Validating Version section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify version Section on About & Support Page")
    void versionSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String faqText = guiController.aboutAndSupportScreen.faq.getTextValue();
        if (guiController.aboutAndSupportScreen.versionText.elementExists()) {
            assertTrue(guiController.aboutAndSupportScreen.faq.getTextValue().equals(faqText));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.8 Validating Privacy  policy on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify privacy policy on About & Support Page")
    void privacyPolicyLinkSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        assertTrue(guiController.aboutAndSupportScreen.privacyPolicy.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.9 Verify User Tutorial Screen under About and support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify User Tutorial Screen under About and support")
    void UserTutorialScreen() {
        assertTrue(guiController.aboutAndSupportScreen.Tutorials.elementExists(), "Tutorial screen is present in About and Support");
        //Click on Tutorial Screen
        guiController.aboutAndSupportScreen.Tutorials.tap();
        assertTrue(guiController.aboutAndSupportScreen.RemoteStartPresets.elementExists(), "Verify Remote start presets is present");
        //Click on Remote Start Presets
        guiController.aboutAndSupportScreen.RemoteStartPresets.tap();
        assertTrue(guiController.aboutAndSupportScreen.Description.getTextValue().contains("There are 4 available presets"), "Verify Tutorial screen under tutorials");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.10 Verify MyH - Link to Safety recall page from within app through SSO")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify MyH - Link to Safety recall page from within app through SSO")
    void safetyrecallcheck() {
        guiController.aboutAndSupportScreen.swipe.up(5);
        assertTrue(guiController.aboutAndSupportScreen.safetyRecallCheck.elementExists(), "Verified Safety Recall check is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.11 Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    void Bluelinkunderemailappsupport() {
        guiController.aboutAndSupportScreen.swipe.up(5);
        guiController.aboutAndSupportScreen.emailAppSupport.elementExists();
        guiController.aboutAndSupportScreen.emailAppSupport.tap();
        guiController.aboutAndSupportScreen.emailincidentfeaturename.elementExists();
        guiController.aboutAndSupportScreen.emailincidentfeaturename.tap();
        for (int i = 0; i < 4; i++) {
            guiController.aboutAndSupportScreen.Next.tap();
        }
        guiController.aboutAndSupportScreen.Finish.tap();
        assertTrue(guiController.aboutAndSupportScreen.emailincidentfeaturename.getTextValue().contains("Bluelink"));
        //Bluelink under faq
        guiController.aboutAndSupportScreen.clickOnBackButton();
        guiController.aboutAndSupportScreen.faq.elementExists();
        guiController.aboutAndSupportScreen.faq.tap();
        assertTrue(guiController.aboutAndSupportScreen.bluelinkFAQ.elementExists(), "Verified Bluelink FAQ is present");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.12 Verify the updated tutorial copy text")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify the updated tutorial copy text")
    void tutorialtext() {
        guiController.aboutAndSupportScreen.Tutorials.tap();
        guiController.aboutAndSupportScreen.RemoteStartPresets.tap();
        guiController.aboutAndSupportScreen.swipe.fromRightEdge(2);
        guiController.aboutAndSupportScreen.swipe.left(2);
        assertTrue(guiController.aboutAndSupportScreen.Description.getTextValue().contains("In Settings, you can rename your preset, make it your default preset, and adjust Climate Start settings based on your vehicle’s supported features."), "Tutorial text is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.13 Get confirmation from users before download of files from Guides section and fix page title")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Get confirmation from users before download of files from Guides section and fix page title")
    void GettingStarted() {
        assertTrue(guiController.aboutAndSupportScreen.guidesText.elementExists(), "Verified Getting started is present");
        guiController.aboutAndSupportScreen.guidesText.tap();
        assertTrue(guiController.aboutAndSupportScreen.Popupalert.elementExists(), "Verified popup alert displayed");
        assertTrue(guiController.aboutAndSupportScreen.Cancel.elementExists(), "Verified Cancel button is present");
        assertTrue(guiController.aboutAndSupportScreen.Continue.elementExists(), "Verified Continue button is displayed");
        guiController.aboutAndSupportScreen.Continue.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.aboutAndSupportScreen.guidesText.tap();
        assertTrue(guiController.aboutAndSupportScreen.Title.getTextValue().contains("Getting Started Guide"), "Verified Getting Started guide page is shown");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.14 Verify the pop up and title text under Guides section for Indicator Guide")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify the pop up and title text under Guides section for Indicator Guide")
    void IndicatorGuide() {
        assertTrue(guiController.aboutAndSupportScreen.indicatorGuide.elementExists(), "Verified Indicator guide is present");
        guiController.aboutAndSupportScreen.indicatorGuide.tap();
        assertTrue(guiController.aboutAndSupportScreen.Popupalert.elementExists(), "Verified popup alert displayed");
        assertTrue(guiController.aboutAndSupportScreen.Cancel.elementExists(), "Verified Cancel button is present");
        assertTrue(guiController.aboutAndSupportScreen.Continue.elementExists(), "Verified Continue button is displayed");
        guiController.aboutAndSupportScreen.Continue.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.aboutAndSupportScreen.indicatorGuide.tap();
        assertTrue(guiController.aboutAndSupportScreen.Title.getTextValue().contains("Indicator Guide"), "Verified Indicator Guide guide page is shown");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.15 Verify the pop up and title text under Guides section for Owner's manual Guide")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify the pop up and title text under Guides section for Owner's manual Guide")
    void OwnersManual() {
        assertTrue(guiController.aboutAndSupportScreen.ownersManual.elementExists(), "Verified ownersManual is present");
        guiController.aboutAndSupportScreen.ownersManual.tap();
        assertTrue(guiController.aboutAndSupportScreen.Popupalert.elementExists(), "Verified popup alert displayed");
        assertTrue(guiController.aboutAndSupportScreen.Cancel.elementExists(), "Verified Cancel button is present");
        assertTrue(guiController.aboutAndSupportScreen.Continue.elementExists(), "Verified Continue button is displayed");
        guiController.aboutAndSupportScreen.Continue.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.aboutAndSupportScreen.ownersManual.tap();
        assertTrue(guiController.aboutAndSupportScreen.Title.getTextValue().contains("Owner's Manual"), "Verified ownersManual page is shown");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.16 Verify the pop up and title text under Guides section for mainatenance information ")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify the pop up and title text under Guides section for mainatenance information ")
    void MaintenanceInformation() {
        assertTrue(guiController.aboutAndSupportScreen.maintenanceInformation.elementExists(), "Verified Maintenance Information is present");
        guiController.aboutAndSupportScreen.maintenanceInformation.tap();
        assertTrue(guiController.aboutAndSupportScreen.Popupalert.elementExists(), "Verified popup alert displayed");
        assertTrue(guiController.aboutAndSupportScreen.Cancel.elementExists(), "Verified Cancel button is present");
        assertTrue(guiController.aboutAndSupportScreen.Continue.elementExists(), "Verified Continue button is displayed");
        guiController.aboutAndSupportScreen.Continue.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.aboutAndSupportScreen.maintenanceInformation.tap();
        assertTrue(guiController.aboutAndSupportScreen.Title.getTextValue().contains("Maintenance Information"), "Verified Maintenance Information page is shown");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.17 Verify the pop up and title text under Guides section for Warranty Info")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify the pop up and title text under Guides section for Warranty Info")
    void Warantyinfo() {
        assertTrue(guiController.aboutAndSupportScreen.warrantyInfo.elementExists(), "Verified Warranty Info is present");
        guiController.aboutAndSupportScreen.warrantyInfo.tap();
        assertTrue(guiController.aboutAndSupportScreen.Popupalert.elementExists(), "Verified popup alert displayed");
        assertTrue(guiController.aboutAndSupportScreen.Cancel.elementExists(), "Verified Cancel button is present");
        assertTrue(guiController.aboutAndSupportScreen.Continue.elementExists(), "Verified Continue button is displayed");
        guiController.aboutAndSupportScreen.Continue.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.aboutAndSupportScreen.warrantyInfo.tap();
        assertTrue(guiController.aboutAndSupportScreen.Title.getTextValue().contains("Warranty Info"), "Verified Warranty Info page is shown");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.17 Verify the pop up and title text under Guides section for Warranty Info")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify the pop up and title text under Guides section for Warranty Info")
    void BluelinktextunderHyundaiResources() {
        assertTrue(guiController.aboutAndSupportScreen.blueLink.elementExists(),"Bluelink is displayed");
        guiController.aboutAndSupportScreen.blueLink.tap();
        appController.appFunctions.pauseTestExecution(1,5000);
        assertTrue(guiController.aboutAndSupportScreen.BluelinkTextunderHyundairesource.elementExists(),"Verified bluelink header is present");
    }

}