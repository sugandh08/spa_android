package tests.myh_app_refresh_regression_suite.ev_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. MobilePrimaryPhone


@Epic("App Notification Settings Test Gen2Point5")
@Feature("App Notification Settings Gen2Point5")
@DisplayName("App Notification Settings Gen2Point5")
public class AppNotificationSettings extends TestController {
    /**
     * Logs in with primary account and primary VIN of given profile, opens settings page
     * @param profile Profile with the primary account and VIN to use
     */
    private void loginAndOpenSettings(Profile profile) {
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.settingsButton.tap();
        guiController.settingsScreenGen2Point5.isPresent(60);
    }

    @BeforeEach
    void loginAndOpenSettingsWithMobilePhoneAccount() {
        loginAndOpenSettings(appController.myHyundaiProfiles.gen2ElectricStandard);
    }

    @Nested
    @DisplayName("Connected Care settings are open")
    class ConnectedCareSettingsAreOpen {
        @BeforeEach
        void openConnectedCareSettings() {

            guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Connected Care");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.1 Pressing the toggle all app button in connected care toggles every connected care app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Pressing the toggle all app button in connected care toggles every connected care app setting")
        void togglingAllConnectedCareAppSetsAllAppsToSameState() {

            guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
            // get the enabled/disabled state of the 'toggle all app' element
            boolean toggleState = guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue();

            // List of setting names in the connected care section
            String[] settingNames = {
                    guiController.settingsScreenGen2Point5.automaticCollisionNotificationText,
                    guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText,
                    guiController.settingsScreenGen2Point5.automaticDtcText,
                    guiController.settingsScreenGen2Point5.monthlyVehicleHealthReportText,
                    guiController.settingsScreenGen2Point5.maintenanceAlertText
            };

            // Check each connected care setting and make sure the app toggle matches the "toggle all" state
            boolean settingStatesCorrect = true;
            for (String settingName : settingNames) {
                if (guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingName) != toggleState) {
                    settingStatesCorrect = false;
                    break;
                }
            }

            assertTrue(settingStatesCorrect);

            if(guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.2 ACN app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify ACN app status is displayed")
        void acnAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.automaticCollisionNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.3 ACN app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify ACN app toggle changes app setting")
        void acnAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.automaticCollisionNotificationText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.4 SOS emergency assistance app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify SOS emergency assistance app status is displayed")
        void sosEmergencyAssistanceAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.5 SOS emergency assistance app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify SOS emergency assistance app toggle changes app setting")
        void sosEmergencyAssistanceAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.6 Automatic DTC app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Automatic DTC app status is displayed")
        void automaticDtcAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.automaticDtcText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.7 Automatic DTC app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Automatic DTC app toggle changes app setting")
        void automaticDtcAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.automaticDtcText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.8 Monthly Vehicle Health Report app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Monthly Vehicle Health Report app status is displayed")
        void monthlyVehicleHealthReportAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.monthlyVehicleHealthReportText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.9 Monthly Vehicle Health Report app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Monthly Vehicle Health Report app toggle changes app setting")
        void monthlyVehicleHealthReportAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.monthlyVehicleHealthReportText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.10 Maintenance Alert app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Maintenance Alert app status is displayed")
        void maintenanceAlertAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.maintenanceAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.11 Maintenance Alert app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Maintenance Alert app toggle changes app setting")
        void maintenanceAlertAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.maintenanceAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }
    }

    @Nested
    @DisplayName("Remote settings are open")
    class RemoteSettingsAreOpen {
        @BeforeEach
        void openRemoteSettings()
        {
            guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Remote");
            guiController.settingsScreenGen2Point5.swipe.fromBottomEdge(1);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.12 Pressing the toggle all app button in remote toggles every remote app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Pressing the toggle all app button in remote toggles every remote app setting")
        void togglingAllRemoteAppSetsAllAppsToSameState() {
            guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();



            // get the enabled/disabled state of the 'toggle all app' element
            boolean toggleState = guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue();

            // List of setting names in the connected care section
            String[] settingNames = {
                    guiController.settingsScreenGen2Point5.panicNotificationText,
                    guiController.settingsScreenGen2Point5.alarmNotificationText,
                    guiController.settingsScreenGen2Point5.hornAndLightsText,
                    guiController.settingsScreenGen2Point5.remoteEngineStartStopText,
                    guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText,
                    guiController.settingsScreenGen2Point5.curfewAlertText,
                    guiController.settingsScreenGen2Point5.valetAlertText,
                    guiController.settingsScreenGen2Point5.geofenceAlertText,
                    guiController.settingsScreenGen2Point5.speedAlertText
            };

            // Check each connected care setting and make sure the app toggle matches the "toggle all" state
            boolean settingStatesCorrect = true;
            for (String settingName : settingNames) {
                if (!guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(settingName) ||
                        guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingName) != toggleState) {
                    settingStatesCorrect = false;
                    break;
                }
            }

            assertTrue(settingStatesCorrect);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.13 Panic Notification app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Panic Notification app status is displayed")
        void panicNotificationAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.panicNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.14 Panic Notification app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Panic Notification app toggle changes app setting")
        void panicNotificationAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.panicNotificationText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.15 Alarm Notification app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Alarm Notification app status is displayed")
        void alarmNotificationAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.alarmNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.16 Alarm Notification app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Alarm Notification app toggle changes app setting")
        void alarmNotificationAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.alarmNotificationText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.17 Horn and Lights app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Horn and Lights app status is displayed")
        void hornAndLightsAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.hornAndLightsText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.18 Horn and Lights app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Horn and Lights app toggle changes app setting")
        void hornAndLightsAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.hornAndLightsText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.19 Remote Engine Start/Stop app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Engine Start/Stop app status is displayed")
        void remoteEngineStartStopAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.remoteEngineStartStopText));
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.20 Remote Engine Start/Stop app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Engine Start/Stop app toggle changes app setting")
        void remoteEngineStartStopAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.remoteEngineStartStopText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.21 Remote Door Lock/Unlock app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Door Lock/Unlock app status is displayed")
        void remoteDoorLockUnlockAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.22 Remote Door Lock/Unlock app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Door Lock/Unlock app toggle changes app setting")
        void remoteDoorLockUnlockAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
      if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
      {
          guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
          appController.appFunctions.pauseTestExecution(1,3000);
      }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.23 Curfew Alert app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Curfew Alert app status is displayed")
        void curfewAlertAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.curfewAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.24 Curfew Alert app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Curfew Alert app toggle changes app setting")
        void curfewAlertAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.curfewAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.25 Valet Alert app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Valet Alert app status is displayed")
        void valetAlertAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.valetAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.26 Valet Alert app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Valet Alert app toggle changes app setting")
        void valetAlertAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.valetAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.27 Geofence Alert app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Geofence Alert app status is displayed")
        void geofenceAlertAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.geofenceAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.28 Geofence Alert app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Geofence Alert app toggle changes app setting")
        void geofenceAlertAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.geofenceAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.29 Speed Alert app status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify  Speed Alert app status is displayed")
        void speedAlertAppStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfAppNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.speedAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.3.30 Speed Alert app toggle changes app setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Speed Alert app toggle changes app setting")
        void speedAlertAppToggleChangesAppSetting() {
            String settingText = guiController.settingsScreenGen2Point5.speedAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleAppNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getAppNotificationStateOfSetting(settingText) != currentSetting);
            if(guiController.settingsScreenGen2Point5.remoteToggleAllApp.getSelectedValue()==false)
            {
                guiController.settingsScreenGen2Point5.remoteToggleAllApp.tap();
                appController.appFunctions.pauseTestExecution(1,3000);
            }
        }
    }
}
