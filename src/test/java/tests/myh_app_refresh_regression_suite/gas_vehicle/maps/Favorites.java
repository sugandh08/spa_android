package tests.myh_app_refresh_regression_suite.gas_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Favorites extends TestController {
    private Profile profile;

    private static int getRandomIntegerBetweenRange(int min, double max){
        int x = (int)(Math.random()*((max-min)+1))+min;
        return x;
    }
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.1 Favorite POI page Rendered Successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Favorite POI")
    @Description("MyHyundai - Verify Favorite POI page Rendered Successfully")
    void favoritePOIPageRenderedSuccessfully(){
        guiController.mapOptions.clickOnFavoritesButton();
        assertTrue(guiController.favoritesPOIScreen.favoritePointsOfInterestLabelText.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.4.2 Favorite POI is deleted successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Favorite POI")
    @Description("MyHyundai - Verify Favorite POI is deleted successfully")
    void deleteFavoritePOI(){
        guiController.mapOptions.clickOnFavoritesButton();
        assertTrue(guiController.favoritesPOIScreen.favoritePointsOfInterestLabelText.elementExists());
        guiController.favoritesPOIScreen.mapListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        if (guiController.favoritesPOIScreen.poiExistsToList(0)){
            guiController.favoritesPOIScreen.tap.elementByXpath(guiController.favoritesPOIScreen.xpathOfFavoritePOI(0));
            appController.appFunctions.pauseTestExecution(1, 3000);
        }
            guiController.favoritesPOIScreen.arrowButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.favoritesPOIScreen.deleteAndSaveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.favoritesPOIScreen.saveAndDeleteButtonText.getTextValue().equalsIgnoreCase("Save"));
    }
}
