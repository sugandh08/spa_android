package tests.myh_app_refresh_regression_suite.gas_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Dealers extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.3.1  Dealers Page redirected successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealers Page redirected successfully")
    void dealersPageRenderedSuccessfully(){
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocatorLabelText.getTextValue().contains("Dealer Locator"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.3.2  Send Dealer Location to car")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Send Dealer Location to car")
    void sendDealerLocationToCar(){
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.dealerLocationLabelText.elementExists());
        guiController.dealerLocatorScreen.sendButton1.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.3.3  Search Dealer Location using ZipCode")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Search Dealer Location using ZipCode")
    void searchDealerLocationUsingZipCode(){
        String zipCode="92844";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        guiController.dealerLocatorScreen.searchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.dealerLocatorScreen.address.getTextValue().contains(zipCode));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.3.4  Search Dealer Location is showing in list")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealer Location is showing in list")
    void dealerLocationIsShowingInMapList(){
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        String locationText=guiController.dealerLocatorScreen.dealerLocationLabelText.getTextValue();
        guiController.dealerLocatorScreen.mapListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='"+locationText+"']").contains(locationText));
        appController.appFunctions.pauseTestExecution(1, 2000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.3.5  Search Dealer Location is showing in list")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealer Location is showing in list")
    void dealerScheduleServiceLinkIsClickable(){
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.scheduleServiceLink.elementExists();
        guiController.dealerLocatorScreen.scheduleServiceLink.tap();
        assertTrue(guiController.scheduleServiceScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.3.6  Search Dealer Location is showing in list")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dealers Locator")
    @Description("MyHyundai - Verify Dealer Location is showing in list")
    void dealerShopAccessoriesLinkIsClickable(){
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.dealerLocatorScreen.arrowButton.tap();
        guiController.dealerLocatorScreen.shopAccessories.elementExists();
        guiController.dealerLocatorScreen.shopAccessories.tap();
        assertTrue(guiController.accessoriesScreen.isPresent());
    }
}
