package tests.myh_app_refresh_regression_suite.gas_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CollisionRepairCenter extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.1 Collision Repair Center")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Collision Repair Center")
    @Description("MyHyundai - Verify Collision Repair Center redirected successfully")
    void collisionRepairCenter() {
        guiController.mapOptions.clickOnCollisionRepairCenterButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.hyundaiPartsAndCollisionInfoScreen.isPresent());
    }
    }
