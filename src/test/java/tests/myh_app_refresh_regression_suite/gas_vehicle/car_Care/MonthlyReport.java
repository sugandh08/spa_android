package tests.myh_app_refresh_regression_suite.gas_vehicle.car_Care;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Monthly Vehicle Health Report Test")
@Feature("CarCare_MonthlyReport")
@DisplayName("Monthly Vehicle Health Report")
public class MonthlyReport extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.1 successful redirection of monthly vehicle health report")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Monthly Vehicle Health Report")
    @Description("MyHyundai - Verify successful redirection of Monthly Vehicle Health Report")
    void monthlyVehicleHealthReport(){
        guiController.carCareOptions.clickOnMonthlyReportButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.monthlyVehicleReportScreen.subTitleText.elementExists();
        assertTrue(guiController.monthlyVehicleReportScreen.vehicleHealthData.getTextValue().contains("No Monthly Vehicle Health Report Available"),"No MVR available");
    }

}
