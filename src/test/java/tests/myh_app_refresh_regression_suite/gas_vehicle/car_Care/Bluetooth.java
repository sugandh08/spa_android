package tests.myh_app_refresh_regression_suite.gas_vehicle.car_Care;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Bluetooth extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.1 successful redirection of Bluetooth")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Bluetooth")
    @Description("MyHyundai - Verify successful redirection of Bluetooth")
    void bluetooth(){
         guiController.carCareOptions.clickOnBluetoothButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        if(guiController.bluetoothScreen.elementExists.byXpath("//android.widget.TextView[@resource-id='android:id/message']")){
            System.out.println("Content unavailable - Car Care Bluetooth button");
        }
        //sometimes error popup shows on clicking bluetooth option
        else  {
            guiController.bluetoothScreen.subTitleText.elementExists();
            assertTrue(guiController.scheduleServiceScreen.subTitleTextView.getTextValue().contains("Bluetooth"));
        }
    }
}
