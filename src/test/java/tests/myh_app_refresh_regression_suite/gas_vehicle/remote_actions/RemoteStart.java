package tests.myh_app_refresh_regression_suite.gas_vehicle.remote_actions;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

 
@Epic("Remote Start Test")
@Feature("Remote Start") 
@DisplayName("Remote Start")
public class RemoteStart extends TestController {
    private Profile profile;
    private String incorrectPin;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.1 Remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate OFF/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithClimateOffDefrostOffHeatedServicesOff() {
    	 guiController.gasHomeScreen.remoteStartButton.tap();
         appController.appFunctions.pauseTestExecution(1, 3000);
         guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
         guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
         guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);

         // wait a second for the engine duration text to update
         appController.appFunctions.pauseTestExecution(1, 1000);
         // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
         int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
         guiController.remoteStartSettingsScreen.setStartSettingsStates(false,false,false);
         guiController.remoteStartSettingsScreen.submitButton.tap();
         guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
         appController.appFunctions.pauseTestExecution(1, 10000);
         guiController.requestSentPopup.okButton.tap();
         appController.appFunctions.pauseTestExecution(1, 3000);
         assertTrue(guiController.gasHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
          String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
         System.out.println("REMOTE START RESULT: " + resultText);
         assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.2 Remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success with Climate ON/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessWithClimateOnDefrostOffHeatedServicesOff() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true,false,false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.gasHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }
    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.3 All four presets and 'start vehicle without presets' options are showing successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Presets Are Showing")
    @Description("MyHyundai - Verify all four presets and 'start vehicle without presets' options are showing successfully")
    void allRemotePresetsOptionsAreShowingSuccessfully() {
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.isPresent();
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(4));
        assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
    }
    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.5  Remote start success via Preset")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote start success via preset")
    @Description("MyHyundai - Verify Remote start success via preset")
    void presetRemoteStartSuccess() {
        int PresetIndex = 3;
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.clickOnPresetStartButton(PresetIndex);

        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.gasHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Start"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

    }
    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.5  Rename Remote start presets")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Rename remote start presets")
    @Description("MyHyundai - Verify Rename remote start presets")
    void renameRemotePresets() {
        int PresetIndex = 1;
         String Rename="TestRename";
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
        guiController.remoteStartSettingsScreen.renamePresets();
        appController.appFunctions.pauseTestExecution(1, 5000);
         assertTrue(guiController.getTextUsingXpath(guiController.remotePresetsScreen.xpathOfPresetsName(PresetIndex)).equals(Rename));

          }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("5.4.3 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteCarControls.clickOnRemoteStartButton();
            guiController.remoteStartSettingsScreen.submitButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 10000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }
    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.4.5   Default Functionality of Remote start presets")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Default Functionality of Remote start presets")
    @Description("MyHyundai - Verify Default Functionality of Remote start presets")
    void defaultRemotePresets() {
        int PresetIndex = 1;
        guiController.remoteCarControls.clickOnRemoteStartButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
        guiController.remoteStartSettingsScreen.swipe.down(2);
        guiController.remoteStartSettingsScreen.setDefaultPresetState(true);
        appController.appFunctions.pauseTestExecution(1, 2000);
    }

   /* @Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.startButton);
        }

        @Nested
        @DisplayName("7.1.5 Remote start with incorrect PIN three times")
        class RemoteStartWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteStartWithIncorrectPinThreeTimes() {
                remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.startButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.1.5.1 Error popup is displayed saying how to reset PIN")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying how to reset PIN")
            void errorPopupIsDisplayedSayingHowToResetPin() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.1.5.2 User is restored access to remote_genesis_old commands after 5 minutes")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote_genesis_old commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.cancelButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                clickOnRemoteStart();
                guiController.remoteStartSettingsScreen.submitButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }
    }*/



}
