package tests.myh_app_refresh_regression_suite.gas_vehicle.remote_actions;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Lock Test")
@Feature("Remote Lock")
@DisplayName("Remote Lock")
public class RemoteLock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.3.1 Remote door lock success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote door lock success with Gen2")
    void remoteDoorLockSuccessWithGen2() {
        guiController.remoteCarControls.clickOnRemoteLockButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.requestSentPopup.okButton.tap();
        assertTrue(guiController.gasHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Lock"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESULT: " + resultText);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("5.3.2 Remote Lock with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  Remote Lock with incorrect PIN shows incorrect PIN notification")
        void remoteLocksWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteCarControls.clickOnRemoteLockButton();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

    /*@Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.lockButton);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.3.2 Remote door lock with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify remote door lock with incorrect PIN shows incorrect PIN notification")
        void remoteDoorLockWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }

        @Nested
        @DisplayName("7.3.3 Remote door lock with incorrect PIN three times")
        class RemoteDoorLockWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteDoorLockWithIncorrectPinThreeTimes() {
                remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.lockButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.3.3.1 Error popup is displayed saying how to reset PIN")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying how to reset PIN")
            void errorPopupIsDisplayedSayingHowToResetPin() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.3.3.2 User is restored access to remote commands after 5 minutes")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.cancelButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }
    }*/
}
