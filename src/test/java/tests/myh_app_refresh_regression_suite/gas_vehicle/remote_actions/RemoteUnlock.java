package tests.myh_app_refresh_regression_suite.gas_vehicle.remote_actions;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Unlock Test")
@Feature("Remote Unlock")
@DisplayName("Remote Unlock")
public class RemoteUnlock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("5.6.1 Remote unlock request shows request successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote unlock request shows request successful message")
    void remoteUnlockRequestShowsRequestSuccessfulMessage() {
        guiController.remoteCarControls.clickOnRemoteUnlockButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.gasHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Remote Unlock"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESULT: " + resultText);
        assertTrue(resultText.contains("Door Unlock") && resultText.contains("successful"));
    }

   @Nested
    class PinLockTest {
        /*@AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.unlockButton);
        }*/

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("5.6.2 Remote unlock with incorrect PIN shows incorrect PIN notification")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify remote unlock with incorrect PIN shows incorrect PIN notification")
        void remoteUnlockWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteCarControls.clickOnRemoteUnlockButton();
            guiController.enterPinScreen.enterPin(incorrectPin);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }

     /*   @Nested
        @DisplayName("7.4.3 Remote unlock with incorrect PIN three times")
        class RemoteUnlockWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteUnlockWithIncorrectPinThreeTimes() {
          //      remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.unlockButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.4.3.1 Error popup is displayed saying Pin is locked")
            @Severity(SeverityLevel.BLOCKER)
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying Pin is locked")
            void errorPopupIsDisplayedSayingPinIsLocked() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.4.3.2 User is restored access to remote commands after 5 minutes")
            @Severity(SeverityLevel.BLOCKER)
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.okButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                guiController.remoteCarControls.clickOnRemoteUnlockButton();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }*/
    }
}
