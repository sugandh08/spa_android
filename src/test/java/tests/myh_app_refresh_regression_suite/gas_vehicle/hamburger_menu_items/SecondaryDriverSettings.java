package tests.myh_app_refresh_regression_suite.gas_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayName("User Profile Information")
public class SecondaryDriverSettings extends TestController {

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);
    String email = "test_user" + randomInt + "@mailnesia.com";


    private void loginAndOpenUserProfile(Profile profile) {


        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);


        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);

        homeScreen.clickOnHamburgerMenuButton();

        guiController.menuScreen.viewProfileButton.tap();


        guiController.profileScreen_gen2Point5.isPresent(60);


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.1 Edit Permissions for Secondary Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify editing Permissions for Secondary Driver")
    void editPermissionsOfAdditionalDriver() {

        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.clickEditPermissionsButton("1");

        appController.appFunctions.pauseTestExecution(1, 4000);

        assertTrue(guiController.editPermissionScreen_gen2Point5.isPresent());


        String toggleValue = guiController.editPermissionScreen_gen2Point5.clickYesNoRadioButton("SEND TO CAR POI");

        if (toggleValue.equals("yes")) {
            Boolean toggleValueBeforeSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfYesRadioButton("SEND TO CAR POI");


            guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

            guiController.editPermissionScreen_gen2Point5.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);


            guiController.editPermissionScreen_gen2Point5.backButton.tap();

            guiController.profileScreen_gen2Point5.clickEditPermissionsButton("1");

            appController.appFunctions.pauseTestExecution(1, 4000);

            Boolean toggleValueAfterSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfYesRadioButton("SEND TO CAR POI");
            System.out.println("After submit" + toggleValueAfterSubmit);

            assertTrue(toggleValueBeforeSubmit.equals(toggleValueAfterSubmit),
                    "Permissions not Updated");


        } else {
            Boolean toggleValueBeforeSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfNoRadioButton("SEND TO CAR POI");
            System.out.println("before submit" + toggleValueBeforeSubmit);

            guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

            guiController.editPermissionScreen_gen2Point5.submitButton.tap();

            appController.appFunctions.pauseTestExecution(1, 4000);

            guiController.editPermissionScreen_gen2Point5.backButton.tap();

            guiController.profileScreen_gen2Point5.clickEditPermissionsButton("1");

            appController.appFunctions.pauseTestExecution(1, 4000);

            Boolean toggleValueAfterSubmit = guiController.editPermissionScreen_gen2Point5.getCheckedValueOfNoRadioButton("SEND TO CAR POI");
            System.out.println("After submit" + toggleValueAfterSubmit);

            assertTrue(toggleValueBeforeSubmit.equals(toggleValueAfterSubmit),
                    "Permissions not Updated");
        }

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.2 Invite Additional Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify inviting Additional Driver")
    void inviteAdditionalDriver() {


        String fName = "test";
        String lName = "user";

        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(3);

        guiController.profileScreen_gen2Point5.inviteAdditionalDriverButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);

        assertTrue(guiController.inviteAdditionalDriverScreen_gen2Point5.isPresent());

        guiController.inviteAdditionalDriverScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);

        appController.appFunctions.pauseTestExecution(1, 5000);

        assertTrue(guiController.getTextUsingXpath("//android.widget.TextView[@resource-id='com.stationdm.bluelink:id/additional_driver_email' and @text='" + email + "']").equals(email)
                , "Invitation Not sent");


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.7.3 Remove Additional Driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify removing Additional Driver")
    void removeAdditionalDriver() {
        String fName = "test";
        String lName = "user";

        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(4);
        guiController.profileScreen_gen2Point5.inviteAdditionalDriverButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);

        assertTrue(guiController.inviteAdditionalDriverScreen_gen2Point5.isPresent());

        guiController.inviteAdditionalDriverScreen_gen2Point5.sendAdditionalDriverInvite(fName, lName, email);

        appController.appFunctions.pauseTestExecution(1, 5000);
        appController.appFunctions.pauseTestExecution(1, 5000);

        String index = guiController.getTextUsingXpath(guiController.profileScreen_gen2Point5.getxPathOfIndexUsingEmail(email));

        assertTrue(guiController.getTextUsingXpath(guiController.profileScreen_gen2Point5.getXpathOfAdditionalDriverEmail(index)).equals(email)
                , "Invitation Not sent");


        guiController.profileScreen_gen2Point5.clickRemoveDriverButtonUsingEmail(email);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.profileScreen_gen2Point5.yesButton.tap();

        for (int i = 0; i < 3; i++) {

            appController.appFunctions.pauseTestExecution(1, 5000);
        }


        assertFalse(guiController.getTextUsingXpath(guiController.
                        profileScreen_gen2Point5.getXpathOfAdditionalDriverEmail(index)).equals(email), "Driver not removed");
    }

}
