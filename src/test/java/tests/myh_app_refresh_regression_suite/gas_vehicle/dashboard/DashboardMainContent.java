package tests.myh_app_refresh_regression_suite.gas_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Greeting Text, Vehicle Modal and Weather Icon")
@Feature("Greeting Text, Vehicle Modal and Weather Icon")
@DisplayName("Test Greeting Text, Vehicle Modal and Weather Icon on dashboard")
public class DashboardMainContent extends TestController {
    private Profile profile;
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 Verify Vehicle Name on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Name on dashboard")
    @Description("MyHyundai - Verify Vehicle Name on dashboard")
    void vehicleModal(){
        guiController.homeScreen.clickOnHamburgerMenuButton();
        String vehicleModal= guiController.menuScreen.vehicleNameTextView.getTextValue();
        guiController.menuScreen.closeButton.tap();
        assertTrue(guiController.gasHomeScreen.vehicleModelName.getTextValue().contains(vehicleModal));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 Verify Greeting Text on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Greeting Text on dashboard")
    @Description("MyHyundai - Verify Greeting Text on dashboard")
    void greetingText(){
        assertTrue(guiController.getTextUsingXpath(guiController.gasHomeScreen.greetingTextXpath).contains("Good"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 Verify Weather Icon on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Weather Icon on dashboard")
    @Description("MyHyundai - Verify Weather Icon on dashboard")
    void weatherIcon(){
        assertTrue(guiController.gasHomeScreen.weatherIcon.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 Verify Vehicle Connectivity Warning on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Warning Text on dashboard")
    @Description("MyHyundai - Verify Vehicle Connectivity Warning")
    void VehicleConnectivityWarning()
    {
        assertTrue(guiController.gasHomeScreen.VehicleConnectivityWarning.getTextValue().contains("Vehicle Connectivity Warning"),"Verify Vehicle Connectivity Warning is present in dashboard");
        //Click on Vehicle Connectivity Warning button
        guiController.gasHomeScreen.VehicleConnectivityWarningBttn.tap();
        assertTrue(guiController.gasHomeScreen.IsVehicleConnectivityWarningPresent(),"Verify Vehicle Connectivity Warning Content is present");
    }

}
