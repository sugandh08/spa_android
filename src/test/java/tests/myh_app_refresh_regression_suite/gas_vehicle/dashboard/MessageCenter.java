package tests.myh_app_refresh_regression_suite.gas_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

@DisplayName("Message Center Test")
public class MessageCenter extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Verify Message Center")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Message Center")
    @Description("MyHyundai - Verify Message Center")
    void messageCenter() {
        guiController.gasHomeScreen.messageCenterButton.tap(50);
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
    }



















}
