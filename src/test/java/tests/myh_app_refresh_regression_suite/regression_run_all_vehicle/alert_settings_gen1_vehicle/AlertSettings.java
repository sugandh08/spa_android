package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.alert_settings_gen1_vehicle;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2Point5

@Epic("Alert Settings Test")
@Feature("Alert Settings")
@DisplayName("Alert Settings")
public class AlertSettings extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 5000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        guiController.alertSettingsScreen.isPresent(60);
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.1 Toggling Speed Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Speed Alert setting does not change the setting value")
    void togglingSpeedAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.speedAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.2 Toggling Valet Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Valet Alert setting does not change the setting value")
    void togglingValetAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.valetAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.3 Toggling Curfew Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Curfew Alert setting does not change the setting value")
    void togglingCurfewAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.curfewAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.4 Toggling Geo-Fence Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Geo-Fence Alert setting does not change the setting value")
    void togglingGeoFenceAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.geoFenceAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.5 Verify Speed alert is updated successfully.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Speed Alert")
    @Description("MyHyundai - Verify Speed alert is updated successfully.")
    void updateSpeedAlert() {
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("SPEED ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.6 Verify Speed alert is updated successfully.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Valet Alert")
    @Description("MyHyundai - Verify Valet alert is updated successfully.")
    void updateValetAlert() {
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.valetAlertScreen.distanceLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.valetAlertScreen.doneButton.tap();
        String valetLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.valetAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.alertSettingsScreen.valetAlertValue.getTextValue().contains(valetLimit));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Valet ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.7 Verify Curfew alert is added successfully.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Curfew Alert")
    @Description("MyHyundai - Verify Curfew alert is added successfully.")
    void addNewCurfewAlert() {
        if (guiController.alertSettingsScreen.addNewCurfewAlertButton.elementExists()) {
            guiController.alertSettingsScreen.addNewCurfewAlertButton.tap();
        } else {
            guiController.alertSettingsScreen.curfewAlertButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.curfewAlertScreen.curfewFromButton.tap();
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();

        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String curfewFromTextValue = guiController.curfewAlertScreen.curfewToButton.getTextValue();
        String curfewToTextValue = guiController.curfewAlertScreen.curfewToButton.getTextValue();
        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + curfewFromTextValue + "']").equals(curfewFromTextValue));
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + curfewToTextValue + "']").equals(curfewToTextValue));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Curfew ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Curfew ") && resultText.contains("successfully"), "Curfew Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.8 Verify Geo-Fence alert is added successfully.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Geo-Fence Alert")
    @Description("MyHyundai - Geo-Fence alert is added successfully.")
    void addNewGeoFenceAlert() {
        guiController.alertSettingsScreen.swipe.up(2);
        assertFalse(guiController.alertSettingsScreen.getGeoFenceAlertLimit(5), "Geo-Fence alert has reached maximum limit");
        if (guiController.alertSettingsScreen.addNewGeoFenceAlertButton.elementExists()) {
            guiController.alertSettingsScreen.addNewGeoFenceAlertButton.tap();
        } else {
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
        }

        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.geoFenceAlertScreen.isPresent());
        guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.geoFenceAlertScreen.searchLocationButton.tap();
        guiController.geoFenceAlertScreen.searchLocationButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        String geoFenceName = "New Geo Fence" + Math.random();
        guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
        guiController.geoFenceAlertScreen.milesEditText.enterText("50");
        guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
        guiController.geoFenceAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Geo-Fence ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Geo-Fence ") && resultText.contains("successfully"), "Geo-Fence Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.9 Delete Geo-Fence alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Geo-Fence Alert")
    @Description("MyHyundai - Delete Geo-Fence alert ")
    void deleteGeoFenceAlert() {
        String geoFenceName = "New Geo Fence" + Math.random();
        //first check if already geo fence alert is added
        guiController.alertSettingsScreen.swipe.up(2);
        if (guiController.alertSettingsScreen.getGeoFenceAlertLimit(0)) {
            guiController.alertSettingsScreen.clickOnGeoFenceAlert(0);
        } else {
            // if there is no geo fence, then add one
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.geoFenceAlertScreen.isPresent());
            guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
        }
        //delete geo fence alert
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.geoFenceAlertScreen.deleteButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.deleteConfirmationPopup.clickOnDeleteButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        // Assertions.assertNotEquals(guiController.getTextUsingXpath("//*[@text='"+geoFenceName+"']"),geoFenceName,"geo fence deleted successfully");
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(2000);
        System.out.println("Geo-Fence ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Geo-Fence Preference") && resultText.contains("successfully"), "Geo-Fence Alert is not deleted successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.8 Delete Curfew alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Curfew Alert")
    @Description("MyHyundai - Delete Curfew alert ")
    void deleteCurfewAlert() {
        //check if any curfew alert already added
        if (guiController.alertSettingsScreen.checkIfCurfewAlertAlreadyAdded()) {

            System.out.println("Clicking on existed curfew alert ");
        } else {
            //add a curfew alert
            guiController.alertSettingsScreen.addNewCurfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            guiController.curfewAlertScreen.curfewFromButton.tap();
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
        }

        String curfewFromTextValue = guiController.getTextUsingXpath(guiController.alertSettingsScreen.xpathOfFromCurfewAlert);
        guiController.alertSettingsScreen.clickOnCurfewAlert();
        guiController.curfewAlertScreen.deleteButton.tap();
        guiController.deleteConfirmationPopup.clickOnDeleteButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(2000);
        System.out.println("Curfew ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Curfew ") && resultText.contains("successfully"), "Curfew Alert is not deleted successfully");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.9 Verify Geo-Fence Alert Update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Geo-Fence Alert Update")
    @Description("MyHyundai - Verify Geo-Fence Alert Update")
    void GeoFenceAlertUpdate() {
        String geoFenceName = "New Geo Fence" + Math.random();
        Element toggle = guiController.alertSettingsScreen.geoFenceAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        if(!toggleValue)
        {
            toggle.tap();
        }
        guiController.alertSettingsScreen.swipe.up(3);
        appController.appFunctions.pauseTestExecution(1, 3000);
        if (guiController.alertSettingsScreen.getGeoFenceAlertLimit(1)) {
            guiController.alertSettingsScreen.clickOnGeoFenceAlert(1);
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
        }
        else
        {
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.geoFenceAlertScreen.isPresent());
            guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);

        }
        //---Tap on Save button on Alert Screen
        guiController.alertSettingsScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertModificationPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSettingsScreen.swipe.up(3);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.10 Verify Curfew Alert Update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Verify Curfew Alert Update")
    @Description("MyHyundai - Verify Curfew Alert Update")
    void CurfewAlertUpdate() {
        Element toggle = guiController.alertSettingsScreen.curfewAlertToggle;
        boolean toggleValue = toggle.getCheckedValue();
        if(!toggleValue)
        {
            toggle.tap();
        }
        if (guiController.alertSettingsScreen.checkIfCurfewAlertAlreadyAdded()) {
            System.out.println("Clicking on existed curfew alert ");
            guiController.alertSettingsScreen.clickOnCurfewAlert();
        }else {
            guiController.alertSettingsScreen.addNewCurfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.curfewAlertScreen.curfewFromButton.tap();

            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            String getFromValue = guiController.curfewAlertScreen.getFromCurfewAlertValue.getTextValue();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            String getToValue = guiController.curfewAlertScreen.getToCurfewAlertValue.getTextValue();

            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();

        }
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.curfewAlertScreen.curfewFromButton.tap();

        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String getFromValue = guiController.curfewAlertScreen.getFromCurfewAlertValue.getTextValue();

        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String getToValue = guiController.curfewAlertScreen.getToCurfewAlertValue.getTextValue();

        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSavedPopup.okButton.tap();

        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSettingsScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertModificationPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + getFromValue + "']").contains(getFromValue));
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + getToValue + "']").contains(getToValue));


    }

}
