package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.maps;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("POI Search")
@Feature("POI Search")
@DisplayName("POI Search")
public class POISearch extends TestController {
    private Profile profile;

    private static int getRandomIntegerBetweenRange(int min, double max){
        int x = (int)(Math.random()*((max-min)+1))+min;
        return x;
    }

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.6.1 POI Search Display on Map")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test POI Search")
    @Description("MyHyundai - Verify POI Search page is rendered successfully")
    void poiSearchPageRenderedSuccessfully(){
        guiController.mapOptions.clickOnPOISearchButton();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.poiSearchScreen.pointOfInterestLabelText.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.6.2 POI Search Functionality")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test search functionality on POI Search")
    @Description("MyHyundai - Verify search functionality on POI Search")
    void searchLocationOnPOISearch(){
        String searchText = "Fountain Valley";
        guiController.mapOptions.clickOnPOISearchButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.poiSearchScreen.poiSearchEditTextbox.enterText(searchText);
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.poiSearchScreen.poiLocationText.getTextValue().contains(searchText));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.6.3 Send POI to Car")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Send POI to Car")
    @Description("MyHyundai - Verify Send POI to Car")
    void sendPOIToCar(){
        guiController.mapOptions.clickOnPOISearchButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.poiSearchScreen.poiSearchEditTextbox.enterText("Fountain Valley");
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.poiSearchScreen.sendButton1.tap();
        }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.6.4 POI Search ")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Save and Delete POI to Car")
    @Description("MyHyundai - Save and Delete POI to Car")
    void saveAndDeletePOIToCar(){
        String searchText = "new "+getRandomIntegerBetweenRange(10,99);
        guiController.mapOptions.clickOnPOISearchButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.poiSearchScreen.poiSearchEditTextbox.enterText(searchText);
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        guiController.poiSearchScreen.poiSearchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 7000);
        guiController.poiSearchScreen.arrowIcon.tap();
        String saveAndDeleteText = guiController.poiSearchScreen.saveAndDeleteButtonText.getTextValue();
        if(saveAndDeleteText.equals("Save")){
            guiController.poiSearchScreen.saveAndDeleteButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            }
        else {
            guiController.poiSearchScreen.saveAndDeleteButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(guiController.poiSearchScreen.saveAndDeleteButtonText.getTextValue().equals("POI Deleted successfully"));
        }
        //assertTrue(guiController.poiSearchScreen.saveAndDeleteButtonText.getTextValue().equals("Delete"),"POI Saved successfully");
        String getTextOfPOi=guiController.poiSearchScreen.poiLocationText.getTextValue();
        //Verify POI added successfully on myPOi screen
        guiController.poiSearchScreen.clickOnBackButton();
        guiController.mapOptions.clickOnFavoritesButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.favoritesPOIScreen.mapListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='"+getTextOfPOi+"']").equals(getTextOfPOi));
    }
}
