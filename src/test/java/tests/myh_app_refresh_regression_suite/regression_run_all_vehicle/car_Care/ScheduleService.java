package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.car_Care;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Car Care-successful redirection of Schedule Service")
public class ScheduleService extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.6.1 Car Care-successful redirection of Schedule Service")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Schedule Service")
    @Description("MyHyundai - Verify successful redirection of Schedule Service")
    void carScheduleService(){
        guiController.carCareOptions.clickOnScheduleServiceButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.scheduleServiceScreen.subTitleTextView.elementExists();
        assertTrue(guiController.scheduleServiceScreen.subTitleTextView.getTextValue().contains("Car Care Scheduling"));
    }

}
