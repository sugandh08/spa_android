package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Greeting Text, Vehicle Modal and Weather Icon")
@Feature("Greeting Text, Vehicle Modal and Weather Icon")
@DisplayName("Test Greeting Text, Vehicle Modal and Weather Icon on dashboard")
public class DashboardMainContent extends TestController {
    private Profile profile;
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Verify Vehicle Name on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Name on dashboard")
    @Description("MyHyundai - Verify Vehicle Name on dashboard")
    void vehicleModal(){
        guiController.homeScreen.clickOnHamburgerMenuButton();
        String vehicleModal= guiController.menuScreen.vehicleNameTextView.getTextValue();
        guiController.menuScreen.closeButton.tap();
        System.out.println("hamburger vehicle modl---"+vehicleModal);
        System.out.println("dashbord vehicle modl---"+guiController.gasHomeScreen.vehicleModelName.getTextValue());
        assertTrue(guiController.electricHomeScreen.vehicleModelName.getTextValue().contains(vehicleModal));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.2 Verify Greeting Text on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Greeting Text on dashboard")
    @Description("MyHyundai - Verify Greeting Text on dashboard")
    void greetingText(){
        assertTrue(guiController.getTextUsingXpath(guiController.electricHomeScreen.greetingTextXpath).contains("Good"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.3 Verify clicking On Refresh Icon Updates Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test clicking On Refresh Icon Updates Dashboard")
    @Description("MyHyundai - Verify clicking On Refresh Icon Updates Dashboard")
    void clickingOnRefreshIconUpdatesDashboard(){
        String RefreshTime = guiController.electricHomeScreen.refreshTime.getTextValue();
        guiController.electricHomeScreen.refreshButton.tap();
         appController.appFunctions.pauseTestExecution(4,7000);
        //get the time after refresh
        String afterRefreshTime=guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();
        assertTrue(!RefreshTime.equals(afterRefreshTime),"Vehicle Status Refresh failed");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.4 Verify Weather Icon on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Weather Icon on dashboard")
    @Description("MyHyundai - Verify Weather Icon on dashboard")
    void weatherIcon(){
        assertTrue(guiController.electricHomeScreen.weatherIcon.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.5 Home screen shows vehicle is not plugged in")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test EV Dashboard")
    @Description("MyHyundai - Verify Home screen shows vehicle is not plugged in")
    void homeScreenShowsVehicleNotPluggedIn() {
        assertEquals("Not Plugged In", guiController.electricHomeScreen.pluginStatusTextView.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.6 Home screen shows vehicle is not charging")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test EV Dashboard")
    @Description("MyHyundai - Verify Home screen shows vehicle is not charging")
    void homeScreenShowsVehicleNotCharging() {
        assertEquals("Not Charging", guiController.electricHomeScreen.chargingStatusTextView.getTextValue());
    }
}
