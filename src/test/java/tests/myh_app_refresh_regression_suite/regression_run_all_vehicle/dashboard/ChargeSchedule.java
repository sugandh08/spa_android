package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.dashboard;
import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChargeSchedule extends TestController {

    private Profile profile;
    private Element StartChargeButton;
    private String incorrectPin;
    WebDriverWait wait;
    WebElement element;


    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 Start Charge")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Start Charge")
    @Description("MyHyundai - Verify Start Charge")
    void vehicleStartChargeWithValidPin() {

        //tap the Start Charge on Vehicle Dashboard
        guiController.electricHomeScreen.StartChargeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 9000);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.requestSentPopup.okButton.tap();

    }

    @Nested
    class vehicleStartChargeWithInvalidPin {
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("2.1.3 Start Charge with incorrect PIN shows incorrect PIN notification")
        @Story("Test Start Charge with incorrect PIN")
        @Description("Start Charge with incorrect PIN shows incorrect PIN notification")
        void StartChargeWithIncorrectPinShowsIncorrectPinNotification() {
            StartChargeButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 PHEV vehicle Departure schedule")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test PHEV vehicle Departure time schedule")
    @Description("MyHyundai - PHEV vehicle Departure time schedule")
    void vehicleDepartureTimeWithSchedule1() {
        //tap the Remote Start on Vehicle Dashboard
        appController.appFunctions.pauseTestExecution(1, 12000);
         guiController.electricHomeScreen.departureSchedule.tap();

        //--tap on sunday
        if(!guiController.editChargeScheduleScreen.sun.getCheckedValue())
        {
            guiController.editChargeScheduleScreen.sun.tap();
        }

        //--Scroll the time
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingHour(),2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingMinute(),2);
        guiController.swipeScrollWheelUp(guiController.editChargeScheduleScreen.xpath_DrivingAmPm(),1);

        guiController.departureSetschedulePopup.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);

        //--Tap on save button on popup
        guiController.departureSetschedulePopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        //---Again click on departure schedule-
        guiController.electricHomeScreen.departureSchedule.tap();
        //---saved value stored in String from SetSchedule page
        String Time1 = guiController.setDepartureTimeScheduleScreen.setSchedule1.getTextValue();
        guiController.electricHomeScreen.clickOnBackButton();

        //----Dashboard screen
        assertTrue(guiController.setDepartureTimeScheduleScreen.departureTimeDetailsDashboard().contains(Time1));
        appController.appFunctions.pauseTestExecution(1, 2000);

    }

}
