package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2ElectricStandard

@Epic("Charge Management")
@Feature("Charge Management")
@DisplayName("Dashboard- Charge Management Test")
    public class ChargeManagement extends TestController{
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 30);
        guiController.electricHomeScreen.isPresent(60);
        }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 Set Up Charge Management Schedule and go back without editing/saving schedule")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify to Set Up Charge Management Schedule and go back without editing/saving schedule")
    void goBackWithoutEditingSchedule() {
        String departureText= guiController.electricHomeScreen.setScheduleButton.getTextValue();
        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.chargeManagementScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(departureText.equals(guiController.electricHomeScreen.setScheduleButton.getTextValue()));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 Setting charge to Start Driving 1 only shows Schedule 1 information on home screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Remote Charge Scheduling")
    @Description("MyHyundai - Verify Setting charge to Start Driving 1 only with Climate Off And Off-Peak Charging Off shows Schedule 1 information on home screen")
    void settingChargeWithClimateOffAndPeakChargingOffShowsCorrectInformationOnHomeScreen() {
        guiController.electricHomeScreen.setScheduleButton.tap();
        guiController.chargeManagementScreen.isPresent(30);
        guiController.chargeManagementScreen.startDrivingSection.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.startDrivingSettingsScreen.isPresent();
        guiController.swipeScrollWheelUp(guiController.startDrivingSettingsScreen.startDriving1HourWheel_Xpath,4);
        guiController.swipeScrollWheelDown(guiController.startDrivingSettingsScreen.startDriving1MinuteWheel_Xpath,4);
        guiController.swipeScrollWheelDown(guiController.startDrivingSettingsScreen.startDriving1AMPMWheel_Xpath,1);
        guiController.startDrivingSettingsScreen.setStartDrivingSettingsStates(true,true);
        guiController.startDrivingSettingsScreen.saveButton.tap();
        String departureTime1 = guiController.chargeManagementScreen.departureTime1Text.getTextValue();
        guiController.chargeManagementScreen.setClimateAndOffPeakChargingStates(true,true);
        guiController.chargeManagementScreen.submitButton.tap();
        guiController.setChargeScheduleSuccessPopup.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 7000);
        assertTrue(guiController.getTextUsingXpath(guiController.electricHomeScreen.nextDepartureText_Xpath).contains(departureTime1));

        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(70);
        System.out.println("SET ELECTRIC CHARGE SCHEDULE: " + resultText);
        assertTrue(resultText.contains("SET ELECTRIC CHARGE SCHEDULE") && resultText.contains("successfully"));

    }



    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 Setting charge to Off-Peak Schedule")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Off-Peak Schedule")
    @Description("MyHyundai - Verify Setting charge to Off-Peak Schedule")
    void settingChargeWithOffPeakSchedule() {

        guiController.electricHomeScreen.setScheduleButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        if(!guiController.setDepartureTimeScheduleScreen.schedule1.getCheckedValue()) {
            guiController.setDepartureTimeScheduleScreen.schedule1.tap();
        }

        boolean checked = guiController.chargeManagementScreen.offPeakSchedule.getCheckedValue();

       // System.out.println("--"+checked+"--");
        if(!checked) {
            guiController.chargeManagementScreen.offPeakSchedule.tap();
            assertTrue(guiController.getTextUsingXpath("//*[@text='Off-Peak Schedule']").contains("Off-Peak Schedule"));

            if(!guiController.chargeManagementScreen.offPeakWeekend.getCheckedValue())
            {
                guiController.chargeManagementScreen.offPeakWeekend.tap();

            }

            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.offPeakScheduleScreen.chargeToggle.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            //---Tap on Weekdays
            guiController.offPeakScheduleScreen.weekdays.tap();
            guiController.swipeScrollWheelUp(guiController.offPeakScheduleScreen.xpath_Minute(), 1);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.swipeScrollWheelDown(guiController.offPeakScheduleScreen.xpath_Minute(), 2);
            guiController.offPeakScheduleScreen.weekdays.tap();

            //---Tap on Weekend----
            guiController.offPeakScheduleScreen.weekendDown.tap();
            guiController.swipeScrollWheelUp(guiController.offPeakScheduleScreen.xpath_Minute(), 1);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.swipeScrollWheelDown(guiController.offPeakScheduleScreen.xpath_Minute(), 2);
            guiController.offPeakScheduleScreen.weekendDown.tap();

            String weekday = guiController.offPeakScheduleScreen.weekDayTime.getTextValue();
            String Weekend = guiController.offPeakScheduleScreen.weekendTime.getTextValue();

            //---Tap on save button on off-peak schedule---
            guiController.offPeakScheduleScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);

            String weekdaySchedule = guiController.chargeManagementScreen.scheduleWeekday.getTextValue();
            String WeekendSchedule = guiController.chargeManagementScreen.scheduleWeekend.getTextValue();

            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));

            //---Tap on save button on set schedule
            guiController.chargeManagementScreen.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 12000);
            guiController.setChargeScheduleSuccessPopup.saveButton.tap();

            //---Verify on Dashboard---
            assertTrue(guiController.electricHomeScreen.selected.equals("True"));

            //---Verify after set schedule
            guiController.electricHomeScreen.setScheduleButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));

         // Boolean selected = guiController.electricHomeScreen.selected.isSelected();

        }else
        {
            appController.appFunctions.pauseTestExecution(1, 5000);
            String weekdaySchedule = guiController.chargeManagementScreen.scheduleWeekday.getTextValue();
            String WeekendSchedule = guiController.chargeManagementScreen.scheduleWeekend.getTextValue();
            guiController.chargeManagementScreen.offPeakWeekend.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            String weekday = guiController.offPeakScheduleScreen.weekDayTime.getTextValue();
            String Weekend = guiController.offPeakScheduleScreen.weekendTime.getTextValue();

            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));

            //---Tap on save button on set schedule
            guiController.chargeManagementScreen.submitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 6000);
            guiController.setChargeScheduleSuccessPopup.saveButton.tap();
            //---Verify on Dashboard---
            assertTrue(guiController.electricHomeScreen.selected.equals("True"));


            //---Verify after set schedule
            guiController.electricHomeScreen.setScheduleButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            assertTrue(weekday.equals(weekdaySchedule) && Weekend.equals(WeekendSchedule));



        }
    }



    }


