package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. MobilePrimaryPhone


@Epic("Email Notification Settings Test")
@Feature("Email Notification Settings")
@DisplayName("Email Notification Settings")
public class EmailNotificationSettings extends TestController {
    /**
     * Logs in with primary account and primary VIN of given profile, opens settings page
     * @param profile Profile with the primary account and VIN to use
     */
    private void loginAndOpenSettings(Profile profile) {
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);

        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.settingsButton.tap();
        guiController.settingsScreenGen2Point5.isPresent(10);
    }

    @BeforeEach
    void setupTests() {
        loginAndOpenSettings(appController.myHyundaiProfiles.gen2ElectricStandard);
    }

    @Nested
    @DisplayName("Connected Care settings are open")
    class ConnectedCareSettingsAreOpen {
        @BeforeEach
        void openConnectedCareSettings() {

            guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Connected Care");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.1 Pressing the toggle all email button in connected care toggles every connected care email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Pressing the toggle all email button in connected care toggles every connected care email setting")
        void togglingAllConnectedCareEmailSetsAllEmailsToSameState() {
            guiController.settingsScreenGen2Point5.connectedCareToggleAllEmail.tap();

            // get the enabled/disabled state of the 'toggle all email' element
            boolean toggleState = guiController.settingsScreenGen2Point5.connectedCareToggleAllEmail.getSelectedValue();

            // List of setting names in the connected care section
            String[] settingNames = {
                    guiController.settingsScreenGen2Point5.automaticCollisionNotificationText,
                    guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText,
                    guiController.settingsScreenGen2Point5.automaticDtcText,
                    guiController.settingsScreenGen2Point5.monthlyVehicleHealthReportText,
                    guiController.settingsScreenGen2Point5.maintenanceAlertText
            };

            // Check each connected care setting and make sure the email toggle matches the "toggle all" state
            boolean settingStatesCorrect = true;
            for (String settingName : settingNames) {
                if (!guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(settingName) ||
                        guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingName) != toggleState) {
                    settingStatesCorrect = false;
                    break;
                }
            }

            assertTrue(settingStatesCorrect);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.2 ACN email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify ACN email status is displayed")
        void acnEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.automaticCollisionNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.3 ACN email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify ACN email toggle changes email setting")
        void acnEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.automaticCollisionNotificationText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.4 SOS emergency assistance email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify SOS emergency assistance email status is displayed")
        void sosEmergencyAssistanceEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.5 SOS emergency assistance email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify SOS emergency assistance email toggle changes email setting")
        void sosEmergencyAssistanceEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.6 Automatic DTC email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Automatic DTC email status is displayed")
        void  automaticDtcEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.automaticDtcText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.7 Automatic DTC email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Automatic DTC email toggle changes email setting")
        void automaticDtcEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.automaticDtcText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.8 Monthly Vehicle Health Report email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Monthly Vehicle Health Report email status is displayed")
        void  monthlyVehicleHealthReportEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.monthlyVehicleHealthReportText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.9 Monthly Vehicle Health Report email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Monthly Vehicle Health Report email toggle changes email setting")
        void monthlyVehicleHealthReportEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.automaticDtcText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.10 Maintenance Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Maintenance Alert email status is displayed")
        void maintenanceAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.maintenanceAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.11 Maintenance Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Connected Care Settings")
        @Description("MyHyundai - Verify Maintenance Alert email toggle changes email setting")
        void maintenanceAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.maintenanceAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }
    }

    @Nested
    @DisplayName("Remote settings are open")
    class RemoteSettingsAreOpen {
        @BeforeEach
        void openRemoteSettings() {
            guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Remote");
            guiController.settingsScreenGen2Point5.swipe.fromBottomEdge(1);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.12 Pressing the toggle all email button in remote toggles every remote email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Pressing the toggle all email button in remote toggles every remote email setting")
        void togglingAllRemoteEmailSetsAllEmailsToSameState() {
            guiController.settingsScreenGen2Point5.remoteToggleAllEmail.tap();

            // get the enabled/disabled state of the 'toggle all email' element
            boolean toggleState = guiController.settingsScreenGen2Point5.remoteToggleAllEmail.getSelectedValue();

            // List of setting names in the connected care section
            String[] settingNames = {
                    guiController.settingsScreenGen2Point5.panicNotificationText,
                    guiController.settingsScreenGen2Point5.alarmNotificationText,
                    guiController.settingsScreenGen2Point5.hornAndLightsText,
                    guiController.settingsScreenGen2Point5.remoteEngineStartStopText,
                    guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText,
                    guiController.settingsScreenGen2Point5.curfewAlertText,
                    guiController.settingsScreenGen2Point5.valetAlertText,
                    guiController.settingsScreenGen2Point5.geofenceAlertText,
                    guiController.settingsScreenGen2Point5.speedAlertText
            };

            // Check each connected care setting and make sure the email toggle matches the "toggle all" state
            boolean settingStatesCorrect = true;
            for (String settingName : settingNames) {
                if (!guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(settingName) ||
                        guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingName) != toggleState) {
                    settingStatesCorrect = false;
                    break;
                }
            }

            assertTrue(settingStatesCorrect);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.13 Panic Notification email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Panic Notification email status is displayed")
        void panicNotificationEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.panicNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.14 Panic Notification email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Panic Notification email toggle changes email setting")
        void panicNotificationEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.panicNotificationText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.15 Alarm Notification email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Alarm Notification email status is displayed")
        void alarmNotificationEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.alarmNotificationText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.16 Alarm Notification email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Alarm Notification email toggle changes email setting")
        void alarmNotificationEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.alarmNotificationText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.17 Horn and Lights email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Horn and Lights email status is displayed")
        void hornAndLightsEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.hornAndLightsText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.18 Horn and Lights email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Horn and Lights email toggle changes email setting")
        void hornAndLightsEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.hornAndLightsText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.19 Remote Engine Start/Stop email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Engine Start/Stop email status is displayed")
        void remoteEngineStartStopEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.remoteEngineStartStopText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.20 Remote Engine Start/Stop email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Engine Start/Stop email toggle changes email setting")
        void remoteEngineStartStopEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.remoteEngineStartStopText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreen.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreen.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.21 Remote Door Lock/Unlock email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Door Lock/Unlock email status is displayed")
        void remoteDoorLockUnlockEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.22 Remote Door Lock/Unlock email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Remote Door Lock/Unlock email toggle changes email setting")
        void remoteDoorLockUnlockEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.23 Curfew Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Curfew Alert email status is displayed")
        void curfewAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.curfewAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.24 Curfew Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Curfew Alert email toggle changes email setting")
        void curfewAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.curfewAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.25 Valet Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Valet Alert email status is displayed")
        void valetAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.valetAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.26 Valet Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Valet Alert email toggle changes email setting")
        void valetAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.valetAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.27 Geofence Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Geofence Alert email status is displayed")
        void geofenceAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.geofenceAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.28 Geofence Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Geofence Alert email toggle changes email setting")
        void geofenceAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.geofenceAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.29 Speed Alert email status is displayed")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify  Speed Alert email status is displayed")
        void speedAlertEmailStatusIsDisplayed() {
            assertTrue(guiController.settingsScreenGen2Point5.checkIfEmailNotificationStateOfSettingExists(
                    guiController.settingsScreenGen2Point5.speedAlertText));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("3.4.30 Speed Alert email toggle changes email setting")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Remote Settings")
        @Description("MyHyundai - Verify Speed Alert email toggle changes email setting")
        void speedAlertEmailToggleChangesEmailSetting() {
            String settingText = guiController.settingsScreenGen2Point5.speedAlertText;
            boolean currentSetting = guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText);

            guiController.settingsScreenGen2Point5.toggleEmailNotificationOfSetting(settingText);
            assertTrue(guiController.settingsScreenGen2Point5.getEmailNotificationStateOfSetting(settingText) != currentSetting);
        }
    }
}
