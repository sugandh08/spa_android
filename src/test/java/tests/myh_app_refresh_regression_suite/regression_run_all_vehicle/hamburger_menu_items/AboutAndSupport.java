package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AboutAndSupport extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.aboutAndSupportButton.tap();
        guiController.aboutAndSupportScreen.isPresent();
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 Validating Blue Link section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Blue Link Section on About & Support Page")
    void blueLinkSection() {
        String blueLinkInfo= guiController.aboutAndSupportScreen.blueLinkInformation.getTextValue();
        String blueLinkHowToVideo= guiController.aboutAndSupportScreen.blueLinkHowToVideos.getTextValue();
        guiController.aboutAndSupportScreen.blueLinkText.elementExists();
        guiController.aboutAndSupportScreen.blueLinkInformation.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.blueLinkInformationScreen.subtitleTextView.getTextValue().equals("Blue Link Info"));
        guiController.blueLinkInformationScreen.clickOnBackButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.aboutAndSupportScreen.blueLinkHowToVideos.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.blueLinkHowToVideosScreen.subtitle.getTextValue().equals("Blue Link How-To Videos"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.2 Validating Bluetooth section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Bluetooth Section on About & Support Page")
    void bluetoothSection() {
        String bluetoothAndMultimediaSupportText = guiController.aboutAndSupportScreen.bluetoothAndMultimediaSupport.getTextValue();
        assertTrue(guiController.aboutAndSupportScreen.bluetoothAndMultimediaSupport.getTextValue().equals(bluetoothAndMultimediaSupportText));

        guiController.aboutAndSupportScreen.bluetoothVideos.tap();

        if(guiController.getTextUsingXpath("//android.widget.TextView[@resource-id='android:id/message']").contains("Content unavailable at this time.")){
            System.out.println("Content unavailable - Car Care Bluetooth button");
        }
        //sometimes error popup shows on clicking bluetooth option
        else  {
            guiController.bluetoothScreen.subTitleText.elementExists();
            assertTrue(guiController.scheduleServiceScreen.subTitleTextView.getTextValue().contains("Bluetooth"));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.3 Validating GUIDES section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify GUIDES Section on About & Support Page")
    void guidesSection() {
        String gettingStartedGuide = guiController.aboutAndSupportScreen.guidesText.getTextValue();
        assertTrue(guiController.aboutAndSupportScreen.indicatorGuide.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.ownersManual.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.maintenanceInformation.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.warrantyInfo.elementExists());
        assertTrue(guiController.aboutAndSupportScreen.gettingStartedGuide.getTextValue().equals(gettingStartedGuide));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.4 Validating hyundai resources section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify hyundai resources Section on About & Support Page")
    void hyundaiResourceSection() {
        guiController.aboutAndSupportScreen.swipe.up(2);
        guiController.aboutAndSupportScreen.hyundaiResourceText.elementExists();

        //Verify Getting Started
        guiController.aboutAndSupportScreen.gettingStarted.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.gettingStartedScreen.subTitleText.getTextValue().equals("Getting Started"));
        guiController.gettingStartedScreen.clickOnBackButton();

        //Verify Blue Link
        guiController.aboutAndSupportScreen.blueLink.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.blueLinkScreen.subTitleText.getTextValue().equals("Blue Link"));
        guiController.blueLinkScreen.clickOnBackButton();

        //Verify Vehicle Health
        guiController.aboutAndSupportScreen.vehicleHealth.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.vehicleHealthScreen.subTitleText.getTextValue().equals("Vehicle Health"));
        guiController.vehicleHealthScreen.clickOnBackButton();

        //Verify Manuals & Warranties
        guiController.aboutAndSupportScreen.manualsAndWarranties.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.manualsAndWarrantiesScreen.subTitleText.getTextValue().equals("Manuals & Warranties"));
        guiController.manualsAndWarrantiesScreen.clickOnBackButton();

        //Verify Bluetooth Assistance
        guiController.aboutAndSupportScreen.bluetoothAssistance.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.bluetoothAssistanceScreen.subTitleText.getTextValue().equals("Bluetooth Assistance"));
        guiController.bluetoothAssistanceScreen.clickOnBackButton();

        //Verify Multimedia and Navigation
        guiController.aboutAndSupportScreen.multimediaAndNavigation.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.multimediaAndNavigationScreen.subTitleText.getTextValue().equals("Multimedia and Navigation"));
        guiController.multimediaAndNavigationScreen.clickOnBackButton();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.5 Validating LINKS section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify LINKS Section on About & Support Page")
    void LinksSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        guiController.aboutAndSupportScreen.linksText.elementExists();
        guiController.aboutAndSupportScreen.hyundaiUSA.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.hyundaiUSAScreen.subTitleText.getTextValue().equals("Hyundai USA"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.6 Validating Contact Us section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Contact Us Section on About & Support Page")
    void contactUsSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String emailAppSupport = guiController.aboutAndSupportScreen.emailAppSupport.getTextValue();
        if(guiController.aboutAndSupportScreen.contactUsText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.emailAppSupport.getTextValue().equals(emailAppSupport));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.7 Validating Version section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify version Section on About & Support Page")
    void versionSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String faqText = guiController.aboutAndSupportScreen.faq.getTextValue();
        if(guiController.aboutAndSupportScreen.versionText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.faq.getTextValue().equals(faqText));
        }
    }

}
