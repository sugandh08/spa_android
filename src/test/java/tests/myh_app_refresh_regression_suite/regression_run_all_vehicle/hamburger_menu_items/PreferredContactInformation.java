package tests.myh_app_refresh_regression_suite.regression_run_all_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. NotificationEmailSameAsLogin
// 2. NotificationEmailDifferentThanLogin
// 3. MobilePrimaryPhone
// 4. LandlinePrimaryPhone

@Epic("Profile- Contact Information Test")
@Feature("Contact Information")
@DisplayName("Contact Information")
public class PreferredContactInformation extends TestController {
    private Profile profile;
    /**
     * Logs in with the chosen account, selects the chosen vehicle, then navigates to the Contact
     * profile page.
     *
     */
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);
        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(70);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.1 When notification email is same as account email, notification email is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification email is same as account email, notification email is displayed")
    void notificationEmailDisplayedWhenNotificationEmailSameAsAccountEmail() {
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(profile.primaryAccount.notificationEmail.equalsIgnoreCase(guiController.profileScreen_gen2Point5.notificationEmail.getTextValue()));
    }
/*
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.2 When notification email is different than account email, notification email is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification email is different than account email, notification email is displayed")
    void notificationEmailDisplayedWhenNotificationEmailDifferentThanAccountEmail() {
        Profile profile = appController.myHyundaiProfiles.notificationEmailDifferentThanLogin;

        loginAndGoToPreferredContactSettings(profile.primaryAccount, profile.primaryVin);
        String expectedEmail = "Email: " + profile.primaryAccount.email;
        assertFalse(expectedEmail.equals(guiController.preferredContactInformationScreen.preferredContactEmailTextView.getTextValue()));
    }*/

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.6.3 When notification phone is mobile, notification phone number is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification phone is mobile, notification phone number is displayed")
    void notificationPhoneDisplayedWhenNotificationPhoneIsMobile() {
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertEquals(profile.primaryAccount.mobilePhone,guiController.profileScreen_gen2Point5.mobileNumber.getTextValue());
    }

   /* @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.4 When notification phone is landline, notification phone number is not displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification phone is landline, notification phone number is not displayed")
    void notificationPhoneDisplayedWhenNotificationPhoneIsLandline() {
        Profile profile = appController.myHyundaiProfiles.landlinePrimaryPhone;

        loginAndGoToPreferredContactSettings(profile.primaryAccount, profile.primaryVin);
        System.out.println(guiController.preferredContactInformationScreen.preferredContactPhoneNumberTextView.getTextValue());

       assertFalse(guiController.preferredContactInformationScreen.preferredContactPhoneNumberTextView.getTextValue().equals("Phone: "+profile.primaryAccount.landlinePhone));

    }*/
}
