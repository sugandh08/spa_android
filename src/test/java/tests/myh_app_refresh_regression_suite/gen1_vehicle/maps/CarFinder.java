package tests.myh_app_refresh_regression_suite.gen1_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CarFinder extends TestController {
    private String incorrectPin;
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.1.1 Verify car finder with Correct Pin")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Car Finder")
    @Description("MyHyundai - Verify car finder with Correct Pin")
    void carFinderWithCorrectPin(){
        guiController.mapOptions.clickOnCarFinderButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 20000);
        if(guiController.getTextUsingXpath("//*[@resource-id='android:id/message']").contains("Your request is taking longer than normal to respond,")){
            guiController.carFinderScreen.tap.elementByXpath("//*[@resource-id='android:id/button2']");
            assertTrue(guiController.carFinderScreen.titleTextView.elementExists());
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.1.2 Verify car finder with Incorrect Pin")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Car Finder")
    @Description("MyHyundai - Verify car finder with Incorrect Pin")
    void carFinderWithIncorrectPinShowsIncorrectPinNotification(){
        guiController.mapOptions.clickOnCarFinderButton();
        guiController.enterPinScreen.enterPin(incorrectPin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.incorrectPinPopup.isPresent());
    }
}
