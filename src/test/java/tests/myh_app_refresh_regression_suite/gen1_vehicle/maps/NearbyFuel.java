package tests.myh_app_refresh_regression_suite.gen1_vehicle.maps;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class NearbyFuel extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.5.1 Verify Nearby Fuel page is redirected successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Nearby Fuel page is redirected successfully")
    void nearbyFuelPageRedirectSuccessfully() {
        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.nearbyGasScreen.nearbyGasStationsLabelText.elementExists();
        assertTrue(guiController.nearbyGasScreen.gasLocationText.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.5.1 Send gas location to car")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Send gas location to car")
    void sendGasLocationToCar() {
        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.nearbyGasScreen.gasLocationText.elementExists());
        guiController.nearbyGasScreen.sendButton1.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.5.2 Save gas location")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Save gas location")
    void saveGasLocation() {
        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.nearbyGasScreen.gasLocationText.elementExists());
        guiController.nearbyGasScreen.arrowIcon.tap();
        guiController.nearbyGasScreen.saveAndDeleteButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        if (guiController.getTextUsingXpath("//*[@resource-id='android:id/message']").contains("location has already been saved")) {
            System.out.println("Gas location is already saved");
        }
        else {
            assertTrue(guiController.nearbyGasScreen.saveAndDeleteButtonText.getTextValue().equalsIgnoreCase("Delete"));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.5.3 Gas Location Is Searchable Using ZipCode")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Gas Location Is Searchable Using ZipCode")
    void gasLocationIsSearchableUsingZipCode() {
        String zipCode="92844";
        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.nearbyGasScreen.gasSearchEditTextbox.enterText(zipCode);
        guiController.nearbyGasScreen.gasSearchGlassIcon.tap();
        guiController.nearbyGasScreen.gasSearchGlassIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        System.out.println("address----"+guiController.nearbyGasScreen.address.getTextValue());
        assertTrue(guiController.nearbyGasScreen.address.getTextValue().contains(zipCode));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("4.5.4 Gas Location Is Searchable Using ZipCode And Is Showing In ListView")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Nearby Fuel")
    @Description("MyHyundai - Verify Gas Location Is Searchable Using ZipCode And Is Showing In ListView")
    void gasLocationIsShowingInListView() {
        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 7000);
        String locationText= guiController.nearbyGasScreen.gasLocationText.getTextValue();
        guiController.nearbyGasScreen.gasListIcon.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='"+locationText+"']").contains(locationText));
    }
    }
