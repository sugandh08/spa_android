package tests.myh_app_refresh_regression_suite.gen1_vehicle.car_Care;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Diagnostics Test")
@Feature("CarCare_Diagnostics")
@DisplayName("Diagnostics")
public class Diagnostics extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.1 successful redirection of Diagnostics car care option")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Diagnostics")
    @Description("MyHyundai - Verify successful redirection of Diagnostics car care option")
    void diagnosticsReport(){
        guiController.carCareOptions.clickOnDiagnosticsButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.diagnosticReportScreen.subTitleText.elementExists();
        assertTrue(guiController.diagnosticReportScreen.systemsStatus.getTextValue().contains("normal"),"Diagnostics Report is normal");
    }
}
