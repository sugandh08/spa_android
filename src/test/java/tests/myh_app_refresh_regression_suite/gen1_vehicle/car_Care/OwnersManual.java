package tests.myh_app_refresh_regression_suite.gen1_vehicle.car_Care;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class OwnersManual extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.5.1 Owner's Manual")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Owner's Manual")
    @Description("MyHyundai - Verify Owner's Manual")
    void ownerManual() {
        guiController.homeScreen.carCareButton.tap();
       assertTrue(guiController.carCareOptions.ownersManualIsDisplayed());
    }
}
