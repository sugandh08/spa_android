package tests.myh_app_refresh_regression_suite.gen1_vehicle.dashboard;

import config.Profile;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Message Center Test")
public class MessageCenter extends TestController {
	private Profile profile;

	@BeforeEach
	void setupTests() {
		profile = appController.myHyundaiProfiles.gen1Standard;
		guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify Message Center")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai - Verify Message Center")
	void messageCenter() {
		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify delete link on every inbox message")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai - Verify delete link on every inbox messages")
	void deleteLinkOnInboxMsg() {

		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");

		List<MobileElement> lstInboxMsg = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstInboxMsg.size());
		List<MobileElement> lstDeleteLink = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvDelete']");
		System.out.println(lstDeleteLink.size());
		assertEquals(lstDeleteLink.size(), lstInboxMsg.size(), "Verified delete link displays on every inbox message");

	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify  deleted message moving to deleted section when delete")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai - Verify  deleted message moving to deleted section when delete")
	void deletedMsgMoveInboxToDeleteSection() {

		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
		List<MobileElement> lstInboxMsg = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstInboxMsg.size());
		String inboxTxt = guiController.getTextUsingXpath(
				"(//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent'])[1]");
		System.out.println(inboxTxt);
		guiController.messageCenterScreen_gen2Point5.deleteLink.tap(50);
		guiController.messageCenterScreen_gen2Point5.tabDeleted.tap(50);
		String deletedTxt = guiController.getTextUsingXpath(
				"(//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent'])[1]");
		System.out.println(deletedTxt);
		assertEquals(inboxTxt, deletedTxt, "Validated Verify  deleted message moving to deleted section when delete");
		guiController.messageCenterScreen_gen2Point5.tabInbox.tap(50);
		List<MobileElement> lstInboxMsgAfterDelete = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstInboxMsgAfterDelete.size());
		assertEquals(lstInboxMsg.size() - 1, lstInboxMsgAfterDelete.size(), "Validated the list size of Inbox");
	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify Refresh button functionality on Inbox tab")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai -Verify Refresh button functionality on Inbox tab")
	void refreshBtnOnInboxTab() {

		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
		guiController.messageCenterScreen_gen2Point5.tabInbox.elementExists();
		List<MobileElement> lstInboxMsg = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstInboxMsg.size());
		assertTrue(guiController.messageCenterScreen_gen2Point5.refreshIcon.elementExists(),
				"Validated Refresh icon displayed");
		guiController.messageCenterScreen_gen2Point5.refreshIcon.tap(50);
		assertTrue(guiController.messageCenterScreen_gen2Point5.loadingCircle.elementExists(),
				"Validated Refreshing the inbox page");
		List<MobileElement> lstInboxMsgAfterRefresh = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstInboxMsgAfterRefresh.size());
		assertEquals(lstInboxMsgAfterRefresh.size(), lstInboxMsg.size(),
				"Validated Refresh button functionality on Inbox tab");
	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify Refresh button functionality on Delete tab")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai -Verify Refresh button functionality on Delete tab")
	void refreshBtnOnDeleteTab() {

		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
		guiController.messageCenterScreen_gen2Point5.tabDeleted.elementExists();
		guiController.messageCenterScreen_gen2Point5.tabDeleted.tap();
		List<MobileElement> lstDeletedMsg = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstDeletedMsg.size());
		assertTrue(guiController.messageCenterScreen_gen2Point5.refreshIcon.elementExists(),
				"Validated Refresh icon displayed");
		guiController.messageCenterScreen_gen2Point5.refreshIcon.tap(50);
		assertTrue(guiController.messageCenterScreen_gen2Point5.loadingCircle.elementExists(),
				"Validated Refreshing the page");
		List<MobileElement> lstDeletedMsgAfterRefresh = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstDeletedMsgAfterRefresh.size());
		assertEquals(lstDeletedMsgAfterRefresh.size(), lstDeletedMsg.size(),
				"Validated Refresh button functionality on Delete tab");
	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify Verify 'No message to display' message on Inbox tab")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai -Verify 'No message to display' message on Inbox tab")
	void NoMsgToDisplayOnInboxTab() {

		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
		guiController.messageCenterScreen_gen2Point5.tabInbox.elementExists();
		List<MobileElement> lstInboxMsg = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstInboxMsg.size());
		if (!lstInboxMsg.isEmpty()) {
			for (int index = 0; index <= lstInboxMsg.size() - 1; index++) {
				guiController.messageCenterScreen_gen2Point5.deleteLink.tap(50);

			}

		} else {
			assertTrue(guiController.messageCenterScreen_gen2Point5.noMsgToDisplay.elementExists(),
					"Validated 'No message to display' message on Inbox tab");
		}

	}

	@RepeatedTest(TEST_REPETITIONS)
	@DisplayName("2.2.1 Verify Verify 'No message to display' message on Deleted tab")
	@Severity(SeverityLevel.BLOCKER)
	@Story("Test Message Center")
	@Description("MyHyundai -Verify 'No message to display' message on Deleted tab")
	void NoMsgToDisplayOnDeletedTab() {

		guiController.gasHomeScreen.messageCenterButton.tap(50);
		appController.appFunctions.pauseTestExecution(1, 5000);
		guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
		guiController.messageCenterScreen_gen2Point5.tabDeleted.elementExists();
		guiController.messageCenterScreen_gen2Point5.tabDeleted.tap();
		List<MobileElement> lstDeletedMsg = guiController
				.getTextsUsingXpath("//android.widget.TextView[@resource-id ='com.stationdm.bluelink:id/tvContent']");
		System.out.println(lstDeletedMsg.size());
		if (!lstDeletedMsg.isEmpty()) {
			for (int index = 0; index <= lstDeletedMsg.size() - 1; index++) {
				guiController.messageCenterScreen_gen2Point5.moveToInboxLink.tap(50);

			}
		} else {
			assertTrue(guiController.messageCenterScreen_gen2Point5.noMsgToDisplay.elementExists(),
					" Validated 'No message to display' message on Deleted tab");
		}

	}
	 @RepeatedTest(TEST_REPETITIONS)
	    @DisplayName("2.2.1 Verify the Contact Us page in Message center")
	    @Severity(SeverityLevel.BLOCKER)
	    @Story("Test Message Center")
	    @Description("MyHyundai -Verify the Contact Us page in Message center")
	    void contactUsPageInMsgCenter() {

	        guiController.gasHomeScreen.messageCenterButton.tap(50);
	        appController.appFunctions.pauseTestExecution(1, 5000);
	        guiController.messageCenterScreen_gen2Point5.subTitleTextView.getTextValue().contains("Message Center");
	        assertTrue(guiController.messageCenterScreen_gen2Point5.btnContactUs.elementExists(),"Validated contact us button displayed");
	        guiController.messageCenterScreen_gen2Point5.btnContactUs.tap();
	       // String contactUsPage = guiController.getTextUsingXpath("//android.view.View[@text='Consumer Assistance Center']");
	        assertTrue(guiController.messageCenterScreen_gen2Point5.contactUsPage.elementExists(),"Validated Contact us  page is displayed");
	    }

}