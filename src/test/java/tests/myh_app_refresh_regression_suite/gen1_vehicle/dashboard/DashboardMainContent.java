package tests.myh_app_refresh_regression_suite.gen1_vehicle.dashboard;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Greeting Text, Vehicle Modal and Weather Icon")
@Feature("Greeting Text, Vehicle Modal and Weather Icon")
@DisplayName("Test Greeting Text, Vehicle Modal and Weather Icon on dashboard")
public class DashboardMainContent extends TestController {
    private Profile profile;
    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 Verify Vehicle Name on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Vehicle Name on dashboard")
    @Description("MyHyundai - Verify Vehicle Name on dashboard")
    void vehicleModal(){
        guiController.homeScreen.clickOnHamburgerMenuButton();
        String vehicleModal= guiController.menuScreen.vehicleNameTextView.getTextValue();
        guiController.menuScreen.closeButton.tap();
        System.out.println("hamburger vehicle modl---"+vehicleModal);
        System.out.println("dashbord vehicle modl---"+guiController.gasHomeScreen.vehicleModelName.getTextValue());
        assertTrue(guiController.gasHomeScreen.vehicleModelName.getTextValue().contains(vehicleModal));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 Verify Greeting Text on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Greeting Text on dashboard")
    @Description("MyHyundai - Verify Greeting Text on dashboard")
    void greetingText(){
        assertTrue(guiController.getTextUsingXpath(guiController.gasHomeScreen.greetingTextXpath).contains("Good"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 Verify Weather Icon on dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Weather Icon on dashboard")
    @Description("MyHyundai - Verify Weather Icon on dashboard")
    void weatherIcon(){
        assertTrue(guiController.gasHomeScreen.weatherIcon.elementExists());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 Verify Remote lock on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote lock on dashboard")
    @Description("MyHyundai - Verify Remote lock on Dashboard")
    void RemotelockDashboard()
    {
        assertTrue(guiController.gasHomeScreen.Remotelock.elementExists(),"Verified Remote lock button is present on dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.5 Verify the remote status not appeared")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Status on dashboard")
    @Description("MyHyundai - Verify the remote status not appeared")
    void RemoteStatusNotAppeared()
    {
        assertTrue(guiController.gasHomeScreen.Remotelock.elementExists(),"Verified Remote lock button is present on dashboard");
        //Click on Remote Lock command on Dashboard
        guiController.gasHomeScreen.Remotelock.tap();
        appController.appFunctions.pauseTestExecution(2,5000);
        assertFalse(guiController.gasHomeScreen.successfulpopup.elementExists(),"Verify the remote status not appeared");
    }


}
