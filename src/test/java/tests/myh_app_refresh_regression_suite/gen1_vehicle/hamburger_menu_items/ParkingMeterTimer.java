package tests.myh_app_refresh_regression_suite.gen1_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParkingMeterTimer extends TestController {

    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.parkingMeterTimerButton.tap();
        guiController.parkingMeterScreen.isPresent();
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.5.1 Validating Parking Meter Timer")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Parking Meter Timer")
    @Description("MyHyundai - Verify able to set Parking meter time and Reminder")
    void setParkingMeterTimeAndReminder() {

        //if already parking time is set, then reset the time first
        if(guiController.parkingMeterTimeSetScreen.meterTimeSetText.elementExists()){
            guiController.parkingMeterTimeSetScreen.resetParkingMeter();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }
        //set parking time
        guiController.swipeScrollWheelDown(guiController.parkingMeterScreen.METER_TIME_HOUR_XPATH,4);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.parkingMeterScreen.METER_TIME_MINUTE_XPATH,4);
        appController.appFunctions.pauseTestExecution(1, 1000);
        //set parking reminder time
        guiController.swipeScrollWheelDown(guiController.parkingMeterScreen.REMINDER_HOUR_XPATH,3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.swipeScrollWheelDown(guiController.parkingMeterScreen.REMINDER_MINUTE_XPATH,3);
        appController.appFunctions.pauseTestExecution(1, 1000);
        String meterTimeValue= guiController.parkingMeterScreen.meterTimeValueTextView.getTextValue();
        //add parking meter notes
        guiController.parkingMeterScreen.notesEditText.tap();
       assertTrue(guiController.parkingMeterNotesScreen.isPresent());
       guiController.parkingMeterNotesScreen.addParkingMeterNotes();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.parkingMeterScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);

        //validate parking meter time set successfully
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.parkingMeterTimerButton.tap();
        assertTrue(guiController.parkingMeterTimeSetScreen.meterTimeSetText.getTextValue().equals(meterTimeValue));
        appController.appFunctions.pauseTestExecution(1, 1000);
    }
}
