package tests.myh_app_refresh_regression_suite.gen1_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayName("User Profile Information")
public class Userprofile extends TestController {
    private Profile profile;

    @BeforeEach
    void loginAndOpenUserProfile() {

        profile = appController.myHyundaiProfiles.gen1Standard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.viewProfileButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.profileScreen_gen2Point5.isPresent(60);

    }

    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.9.1 Edit Emergency Contact Button is not showing on user profile screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test User Profile Information")
    @Description("MyHyundai - Verify Edit Emergency Contact Button is not showing on user profile screen")
    void EditEmergencyContactButton()
    {
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(6);
        assertTrue(guiController.profileScreen_gen2Point5.EmergencyContactInformation.elementExists(), "Verify Emergency Contact information is present");
        assertFalse(guiController.profileScreen_gen2Point5.EditEmergencyContact.getTextValue().contains("Edit Emergency Contact Information"), "Verified Edit Emergency Contact Information button not exist");
    }
}
