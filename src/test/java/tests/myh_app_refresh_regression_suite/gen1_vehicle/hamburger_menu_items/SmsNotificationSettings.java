package tests.myh_app_refresh_regression_suite.gen1_vehicle.hamburger_menu_items;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. MobilePrimaryPhone
// 2. LandlinePrimaryPhone


@Epic("SMS Notification Settings Test")
@Feature("SMS Notification Settings")
@DisplayName("SMS Notification Settings")
public class SmsNotificationSettings extends TestController {
    /**
     * Logs in with primary account and primary VIN of given profile, opens settings page
     * @param profile Profile with the primary account and VIN to use
     */
    private void loginAndOpenSettings(Profile profile) {
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
               homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.settingsButton.tap();
        guiController.settingsScreenGen2Point5.isPresent(10);
    }

    @Nested
    @DisplayName("Settings page opened with mobile phone account")
    class SettingsPageOpenedWithMobilePhoneAccount {
        @BeforeEach
        void setupTests() {
            loginAndOpenSettings(appController.myHyundaiProfiles.gen1Standard);
        }

        @Nested
        @DisplayName("Connected Care settings are open")
        class ConnectedCareSettingsAreOpen {
            @BeforeEach
            void openConnectedCareSettings() {


                guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Connected Care");
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.2 Pressing the toggle all SMS button in connected care toggles every connected care SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify pressing the toggle all SMS button in connected care toggles every connected care SMS setting")
            void togglingAllConnectedCareSmsSetsAllSmsToSameState() {
                guiController.settingsScreenGen2Point5.connectedCareToggleAllText.tap();

                // get the enabled/disabled state of the 'toggle all SMS' element
                boolean toggleState = guiController.settingsScreenGen2Point5.connectedCareToggleAllText.getSelectedValue();

                // List of setting names in the connected care section
                String[] settingNames = {
                        guiController.settingsScreenGen2Point5.automaticCollisionNotificationText,
                        guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText,
                        guiController.settingsScreenGen2Point5.automaticDtcText,
                        guiController.settingsScreenGen2Point5.monthlyVehicleHealthReportText,
                        guiController.settingsScreenGen2Point5.maintenanceAlertText
                };

                // Check each connected care setting and make sure the SMS toggle matches the "toggle all" state
                boolean settingStatesCorrect = true;
                for (String settingName : settingNames) {
                    if (!guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(settingName) ||
                            guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingName) != toggleState) {
                        settingStatesCorrect = false;
                        break;
                    }
                }

                assertTrue(settingStatesCorrect);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.3 ACN SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify ACN SMS status is displayed")
            void acnSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.automaticCollisionNotificationText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.4 ACN SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify ACN SMS toggle changes SMS setting")
            void acnSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.automaticCollisionNotificationText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.5 SOS emergency assistance SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify SOS emergency assistance SMS status is displayed")
            void sosEmergencyAssistanceSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.6 SOS emergency assistance SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify SOS emergency assistance SMS toggle changes SMS setting")
            void sosEmergencyAssistanceSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.sosEmergencyAssistanceText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.7 Automatic DTC SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify Automatic DTC SMS status is displayed")
            void automaticDtcSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.automaticDtcText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.8 Automatic DTC SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify Automatic DTC SMS toggle changes SMS setting")
            void automaticDtcSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.automaticDtcText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.9 Monthly Vehicle Health Report SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify Monthly Vehicle Health Report SMS status is displayed")
            void monthlyVehicleHealthReportSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.automaticDtcText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.10 Monthly Vehicle Health Report SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify Monthly Vehicle Health Report  SMS toggle changes SMS setting")
            void monthlyVehicleHealthReportSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.automaticDtcText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }


            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.11 Maintenance Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify Maintenance Alert SMS status is displayed")
            void maintenanceAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.maintenanceAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.12 Maintenance Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("MyHyundai - Verify Maintenance Alert SMS toggle changes SMS setting")
            void maintenanceAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.maintenanceAlertText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }
        }

        @Nested
        @DisplayName("Remote settings are open")
        class RemoteSettingsAreOpen {
            @BeforeEach
            void openRemoteSettings() {
                guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Remote");
                guiController.settingsScreenGen2Point5.swipe.fromBottomEdge(1);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.13 Pressing the toggle all SMS button in remote toggles every remote SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Pressing the toggle all SMS button in remote  toggles every remote SMS setting")
            void togglingAllRemoteSmsSetsAllSmssToSameState() {
                guiController.settingsScreenGen2Point5.remoteToggleAllText.tap();

                // get the enabled/disabled state of the 'toggle all SMS' element
                boolean toggleState = guiController.settingsScreenGen2Point5.remoteToggleAllText.getSelectedValue();

                // List of setting names in the connected care section
                String[] settingNames = {
                        guiController.settingsScreenGen2Point5.panicNotificationText,
                        guiController.settingsScreenGen2Point5.alarmNotificationText,
                        guiController.settingsScreenGen2Point5.hornAndLightsText,
                        guiController.settingsScreenGen2Point5.remoteEngineStartStopText,
                        guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText,
                        guiController.settingsScreenGen2Point5.curfewAlertText,
                        guiController.settingsScreenGen2Point5.valetAlertText,
                        guiController.settingsScreenGen2Point5.geofenceAlertText,
                        guiController.settingsScreenGen2Point5.speedAlertText
                };

                // Check each connected care setting and make sure the SMS toggle matches the "toggle all" state
                boolean settingStatesCorrect = true;
                for (String settingName : settingNames) {
                    if (!guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(settingName) ||
                            guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingName) != toggleState) {
                        settingStatesCorrect = false;
                        break;
                    }
                }

                assertTrue(settingStatesCorrect);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.14 Panic Notification SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Panic Notification SMS status is displayed")
            void panicNotificationSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.panicNotificationText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.15 Panic Notification SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Panic Notification SMS toggle changes SMS setting")
            void panicNotificationSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.panicNotificationText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.16 Alarm Notification SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Alarm Notification SMS status is displayed")
            void alarmNotificationSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.alarmNotificationText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.17 Alarm Notification SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Alarm Notification SMS toggle changes SMS setting")
            void alarmNotificationSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.alarmNotificationText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.18 Horn and Lights SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Horn and Lights SMS status is displayed")
            void hornAndLightsSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.hornAndLightsText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.19 Horn and Lights SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Horn and Lights SMS toggle changes SMS setting")
            void hornAndLightsSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.hornAndLightsText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.20 Remote Engine Start/Stop SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Remote Engine Start/Stop SMS status is displayed")
            void remoteEngineStartStopSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.remoteEngineStartStopText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.21 Remote Engine Start/Stop SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Remote Engine Start/Stop SMS toggle changes SMS setting")
            void remoteEngineStartStopSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.remoteEngineStartStopText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.22 Remote Door Lock/Unlock SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Remote Door Lock/Unlock SMS status is displayed")
            void remoteDoorLockUnlockSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.23 Remote Door Lock/Unlock SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Remote Door Lock/Unlock SMS toggle changes SMS setting")
            void remoteDoorLockUnlockSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.remoteDoorLockUnlockText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.24 Curfew Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Curfew Alert SMS status is displayed")
            void curfewAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.curfewAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.25 Curfew Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Curfew Alert SMS toggle changes SMS setting")
            void curfewAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.curfewAlertText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.26 Valet Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Valet Alert SMS status is displayed")
            void valetAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.valetAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.27 Valet Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Valet Alert SMS toggle changes SMS setting")
            void valetAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.valetAlertText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.28 Geofence Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Geofence Alert SMS status is displayed")
            void geofenceAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.geofenceAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.29 Geofence Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Geofence Alert SMS toggle changes SMS setting")
            void geofenceAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.geofenceAlertText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.30 Speed Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Speed Alert SMS status is displayed")
            void speedAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreenGen2Point5.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreenGen2Point5.speedAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("3.8.31 Speed Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("MyHyundai - Verify Speed Alert SMS toggle changes SMS setting")
            void speedAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreenGen2Point5.speedAlertText;
                boolean currentSetting = guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreenGen2Point5.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreenGen2Point5.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }
        }
    }

   /* @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Phoneline set to Landline prevents SMS toggle")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Connected Care Settings")
    @Description("MyHyundai - Verify Phoneline set to Landline prevents SMS toggle")
    void landlinePhonePreventsSmsToggle() {

        Profile profile= appController.myHyundaiProfiles.landlinePrimaryPhone;
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        if (guiController.electricHomeScreen.isLongRequestPopUpPresent(15))
        {
            System.out.println("Request taking too long pop up");
            guiController.electricHomeScreen.closeButton.tap();
        }
        homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.settingsButton.tap();
        guiController.settingsScreenGen2Point5.isPresent(10);
       // loginAndOpenSettings(appController.myHyundaiProfiles.landlinePrimaryPhone);
        guiController.settingsScreenGen2Point5.tapAccordianExpandButton("Connected Care");
        System.out.println(guiController.settingsScreenGen2Point5.connectedCareToggleAllText.getClickableValue());
        assertFalse(guiController.settingsScreenGen2Point5.connectedCareToggleAllText.getClickableValue());
    }*/
}
