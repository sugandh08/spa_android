package tests.myh_app_refresh_regression_suite.LoginVariations;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2ElectricStandard

@Epic("Login Terms and Conditions Test")
@Feature("Login Terms and Conditions")
@DisplayName("Login Terms and Conditions")
public class LoginTermsAndConditions extends TestController {



    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.2.1 View Terms and Conditions at the time of Login")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Terms and Conditions")
    @Description("MyHyundai - Verify viewing Terms and Conditions at the time of Login")
    void LoginAndViewTermsAndConditions() {
        Profile profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
      /*  appController.appFunctions.uninstallMyHyundaiAndroid();
        appController.appFunctions.installApp(appController.getMyHyundaiAndroidFileName()); */

        appController.appFunctions.launchApp();
        guiController.loginScreen.loginWithAccount(profile.primaryAccount);
        appController.appFunctions.pauseTestExecution(1,6000);
        guiController.termsAndConditionsPopup.viewDetailsButton.tap();
        appController.appFunctions.pauseTestExecution(1,10000);
        assertTrue(guiController.termsAndConditionsScreen.isPresent());
        guiController.termsAndConditionsScreen.clickOnBackButton();
        guiController.termsAndConditionsPopup.acceptButton.tap();
        //appController.appFunctions.pauseTestExecution(1,10000);
        appController.appFunctions.pauseTestExecution(1,3000);
        assertTrue(guiController.getTextUsingXpath(guiController.electricHomeScreen.greetingTextXpath).contains("Good"));

    }
}
