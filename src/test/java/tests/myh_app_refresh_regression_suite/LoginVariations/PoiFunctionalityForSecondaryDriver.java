package tests.myh_app_refresh_regression_suite.LoginVariations;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Epic("POI Functions for Secondary Driver Test")
@Feature("POI Functions for Secondary Driver Test")
@DisplayName("POI Functions for Secondary Driver Test")
public class PoiFunctionalityForSecondaryDriver extends TestController {

    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.withSecondaryDriver;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.secondaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.1 POIs are searchable when signed in as secondary driver")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Functions Gen2 for Secondary Driver")
    @Description("MyHyundai - Verify POIs are searchable when signed in as secondary driver")
    void secondaryDriverPoiAreSearchable() {
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiSearchScreen.isPresent());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.2 POIs are viewable in an expanded list when signed in as secondary driver")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Functions Gen2 for Secondary Driver")
    @Description("MyHyundai - Verify POIs are viewable in an expanded list when signed in as secondary driver")
    void secondaryDriverSearchedPoisAreDisplayedInListView() {
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.poiSearchScreen.searchEditText.enterText("mcdonalds");
        guiController.poiSearchScreen.searchButton.tap();
        guiController.poiSearchScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.poiSearchScreen.listButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.poiSearchScreen.searchResultsListView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.3 Hyundai Dealer zip code is searchable when signed in as secondary driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen2 for Secondary Driver")
    @Description("MyHyundai - Verify Hyundai Dealer zip code is searchable when signed in as secondary driver")
    void secondaryDriverHyundaiDealerZipCodeIsSearchable() {
        String zipCode = "55555";
        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.dealerLocatorScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertEquals(zipCode, guiController.dealerLocatorScreen.searchEditText.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.4 Hyundai Dealers are viewable in an expanded list when signed in as secondary driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen2 for Secondary Driver")
    @Description("MyHyundai - Verify Hyundai Dealers are viewable in an expanded list when signed in as secondary driver")
    void    secondaryDriverHyundaiDealersAreDisplayedInListView() {
        String zipCode = "55555";

        guiController.mapOptions.clickOnDealersButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchButton.tap();
        guiController.dealerLocatorScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.dealerLocatorScreen.listButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.searchResultsListView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.5 Nearby Gas is searchable when signed in as secondary driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen2 for Secondary Driver")
    @Description("MyHyundai - Verify Nearby Gas is searchable when signed in as secondary driver")
    void secondaryDriverNearbyGasIsSearchable() {
        String zipCode = "55555";

        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.nearbyGasScreen.searchEditText.enterText(zipCode);
        guiController.nearbyGasScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.nearbyGasScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertEquals(zipCode, guiController.nearbyGasScreen.searchEditText.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.4.6 Nearby Gas is viewable in an expanded list when signed in as secondary driver")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen2 for Secondary Driver")
    @Description("MyHyundai - Verify Nearby Gas is viewable in an expanded list when signed in as secondary driver")
    void secondaryDriverNearbyGasIsDisplayedInListView() {
        String zipCode = "55555";

        guiController.mapOptions.clickOnNearbyFuelButton();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.nearbyGasScreen.searchEditText.enterText(zipCode);
        guiController.nearbyGasScreen.searchButton.tap();
        guiController.nearbyGasScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.nearbyGasScreen.listButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.nearbyGasScreen.searchResultsListView.elementExists());
    }



}
