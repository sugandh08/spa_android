package tests.myh_app_refresh_regression_suite.LoginVariations;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. gen2ElectricStandard

@Epic("Guest Login")
@Feature("Guest Login")
public class GuestLogin extends TestController {
    private Profile profile;

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.1 Verify user can login to app using Guest Login")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can login to app using Guest Login")
    void guestlogin() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestvehicle.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap(20);
        assertTrue(guiController.homeScreen.remoteHistoryButton.elementExists(), "Verified home screen is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.2 Verify user can switch Demo vehicle using guest login")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can switch Demo vehicle using guest login")
    void guestloginswitchVehicle() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestvehicle.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap(20);
        assertTrue(guiController.homeScreen.remoteHistoryButton.elementExists(), "Verified home screen is present");
        guiController.homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.switchVehicleButton.tap();
        //  guiController.loginScreen.tap2NDVEHICLE();
        assertTrue(guiController.homeScreen.remoteHistoryButton.elementExists(), "Verified home screen is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.3 Verify user can see vehicle year and models for car care information and support")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can see vehicle year and models for car care information and support")
    void guestloginVehicleYearandName() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.4 Verify user can see vehicle year and models for car care information and support")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can see vehicle year and models for car care information and support")
    void selectguestloginVehicleYearandName() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        assertTrue(guiController.loginScreen.guestheader.getTextValue().contains("2016"), "Verified guest login page is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.5 Verify user can search POI for the selected vehicle")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can search POI for the selected vehicle")
    void guestloginPOI() {
        String searchText = "Fountain Valley";
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        guiController.GuestloginScreen.POISearch.tap();
        guiController.GuestloginScreen.POISearch.enterText(searchText);
        guiController.electricHomeScreen.poiSearchGlassIcon.tap();
        guiController.termsAndConditionsPopup.usingtheapp.tap();
        guiController.GuestloginScreen.searchbar.tap();
        guiController.GuestloginScreen.searchbar.enterText(searchText);
        guiController.GuestloginScreen.POIsearchglass.tap();
        guiController.GuestloginScreen.POIsearchglass.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.poiSearchScreen.pointOfInterestLabelText.elementExists();
        assertTrue(guiController.poiSearchScreen.poiLocationText.getTextValue().contains(searchText));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.6 Verify user can download Owner's Manual for the selected vehicle")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can download Owner's Manual for the selected vehicle")
    void guestloginOwnersManual() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        guiController.GuestloginScreen.AboutandSupport.tap();
        guiController.GuestloginScreen.OwnersManual.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        guiController.GuestloginScreen.OwnersManual.tap();
        assertTrue(guiController.GuestloginScreen.OwnersManualHeader.getTextValue().contains("Owner's Manual"), "Verified Owners Manuals page is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.7 Verify user can search for a dealer location")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can search for a dealer location")
    void guestloginDealer() {
        String zipcode = "90002";
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        guiController.GuestloginScreen.DealerLocator.tap();
        guiController.termsAndConditionsPopup.usingtheapp.tap();
        guiController.GuestloginScreen.searchbar.tap();
        guiController.GuestloginScreen.searchbar.enterText(zipcode);
        guiController.GuestloginScreen.clickSearchKeyPadBtn();
        assertTrue(guiController.GuestloginScreen.DealerLocatorheader.getTextValue().contains("Dealer Locator"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.8 Verify user can Call Roadside service")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can Call Roadside service")
    void guestloginCallRoadside() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        guiController.GuestloginScreen.CallRoadside.tap();
        assertTrue(guiController.GuestloginScreen.Callroadsideassistance.elementExists(), "Verified Call roaadside assistance is present");
        assertTrue(guiController.GuestloginScreen.CallbuttonisPresent(), "Verified Call button is present");
        assertTrue(guiController.GuestloginScreen.CancelbuttonisPresent(), "Verified Cancel button is preset");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.9 Verify user can see car care option in the bottom navigation bar")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can see car care option in the bottom navigation bar")
    void guestloginCarCare() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        assertTrue(guiController.GuestloginScreen.CarCare.elementExists(), "Verified Car Care is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.10 Verify user can see  Home option in the Hamburger menu")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can see  Home option in the Hamburger menu")
    void guestloginHome() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        assertTrue(guiController.GuestloginScreen.Home.elementExists(), "Verified Home is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.11 Verify user can see MAP option in Dashboard")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can see MAP option in Dashboard")
    void guestloginMap() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        assertTrue(guiController.GuestloginScreen.Map.elementExists(), "Verified Map is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.12 Verify user can see the blue link information/FAQ/Video's via Blue Link option")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can see the blue link information/FAQ/Video's via Blue Link option")
    void guestloginFAQVideosBluelink() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES

        //Verify user can see About & Support
        guiController.loginScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.GuestloginScreen.AboutandSupport.elementExists(), "Verified About and Support exist");

        //Verify user can see the blue link information/FAQ/Video's via Blue Link option
        guiController.GuestloginScreen.AboutandSupport.tap();
        assertTrue(guiController.GuestloginScreen.BluelinkInformation.elementExists(), "Verified Bluelink is Displayed");
        assertTrue(guiController.GuestloginScreen.BluetoothVideos.elementExists(), "Verified Bluetooth Videos is Displayed");
        guiController.GuestloginScreen.swipe.up(5);
        assertTrue(guiController.GuestloginScreen.FAQ.elementExists(), "Verified FAQ is present");
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();

        //Verify user can schedule service
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.GuestloginScreen.ScheduleService.elementExists(), "Verified Schedule service is present in hamburger menu");
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();

        //Verify user can schedule Service via car care
        guiController.GuestloginScreen.CarCare.tap();
        assertTrue(guiController.GuestloginScreen.ScheduleService.elementExists(), "Verified Schedule service is present in Car care");

        //Verify user can see Bluetooth option
        assertTrue(guiController.GuestloginScreen.Bluetooth.elementExists(), "Verified Bluetooth is displayed in car care");
        guiController.GuestloginScreen.CarCare.tap();

        //Verify user can see Nearby gas via Map
        guiController.GuestloginScreen.Map.tap();
        assertTrue(guiController.GuestloginScreen.NearbyGas.elementExists(), "Verified Nearby gas is present in maps");
        guiController.GuestloginScreen.NearbyGas.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(guiController.GuestloginScreen.NearbyGasStations.elementExists(), "Verified Near by GAS stations is displayed");
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        guiController.GuestloginScreen.Remindmelater.tap();

        //Verify user can search POI using Map
        assertTrue(guiController.GuestloginScreen.DashboardMaps.elementExists(), "Verified Dashboard Maps is Present");
        guiController.GuestloginScreen.DashboardMaps.tap();
        guiController.GuestloginScreen.clickOnPOISearchButton();
        String searchText = "Fountain Valley";
        guiController.GuestloginScreen.searchbar.tap();
        guiController.GuestloginScreen.searchbar.enterText(searchText);
        guiController.GuestloginScreen.POIsearchglass.tap();
        guiController.GuestloginScreen.POIsearchglass.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.poiSearchScreen.pointOfInterestLabelText.elementExists();
        assertTrue(guiController.poiSearchScreen.poiLocationText.getTextValue().contains(searchText));

        //Verify user can search Hyundai Dealers via Map
        guiController.GuestloginScreen.DashboardMaps.tap();
        guiController.GuestloginScreen.clickOnDealersButton();
        assertTrue(guiController.GuestloginScreen.DealerLocatorheader.elementExists(), "Verified Dealer locator page is displayed");
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();

        //Verify user can see Burger menu in the top left screen
        guiController.GuestloginScreen.HamburgerMenuisPresent();

        //Verify user can Schedule service via Burger Menu
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.GuestloginScreen.ScheduleService.elementExists(), "Verified Schedule service is present in hamburger menu");
        guiController.GuestloginScreen.ScheduleService.tap();
        guiController.GuestloginScreen.Ok.tap();

        //Verify user can see Accessories via Burger Menu
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.GuestloginScreen.Accessories.elementExists(), "Verified Accessories is present in hamburger");
        guiController.GuestloginScreen.Accessories.tap();
        assertTrue(guiController.GuestloginScreen.Accessoriespageheader.elementExists(), "Verified accessories page is displayed");
        assertTrue(guiController.GuestloginScreen.AccessoriespageChoosehyundai.elementExists(), "Verified accessories page is displayed");
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 4000);

        //Verify user can search Dealer location via Burger Menu
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.GuestloginScreen.MyDealer.elementExists(), "Verified My Dealer is present in hamburger");
        guiController.GuestloginScreen.MyDealer.tap();
        assertTrue(guiController.GuestloginScreen.DealerLocatorheader.elementExists(), "Verified Dealer locator page is displayed");
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        appController.appFunctions.pauseTestExecution(1, 4000);

        //Verify user can Call Roadside service via Burger Menu
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        guiController.GuestloginScreen.swipe.up(2);
        assertTrue(guiController.menuScreen.callRoadsideButton.elementExists(), "Call Roadside  Button is displayed");
        guiController.menuScreen.callRoadsideButton.tap();
        assertTrue(guiController.menuScreen.CallRoadsideScreenisPresent(), "Call Roadside Popup is displayed");
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.menuScreen.CallBtnExists(), "Call Button is displayed");
        assertTrue(guiController.menuScreen.CancelBtnExists(), "Cancel Button is displayed");
        guiController.menuScreen.TapCancelButton();
        guiController.GuestloginScreen.Close.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);

        //Verify user can logout
        guiController.GuestloginScreen.clickOnHamburgerMenuButton();
        guiController.GuestloginScreen.swipe.up(2);
        assertTrue(guiController.GuestloginScreen.Signout.elementExists(), "Verified Signout is present in hamburger");
        guiController.GuestloginScreen.Signout.tap();
        guiController.GuestloginScreen.Ok.tap();
        assertTrue(guiController.GuestloginScreen.isPresent(), "Verified successfully signout");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.13 Verify user can switch vehicle via Burger Menu")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify user can switch vehicle via Burger Menu")
    void SwitchDemovehicle() {
        guiController.loginScreen.guestLoginButton.tap();
        assertTrue(guiController.loginScreen.GuestloginPagepresent(), "Verified GuestLogin page displayed");
        guiController.loginScreen.guestselectvehicle.tap();
        assertTrue(guiController.loginScreen.guestVehicleyearlist.elementExists(), "Years are displayed");
        assertTrue(guiController.loginScreen.guestVehiclenamelist.elementExists(), "Names are displayed");
        guiController.loginScreen.guestvehicleyear.tap();
        guiController.loginScreen.guestvehiclename.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        guiController.loginScreen.clickOnHamburgerMenuButton();
        guiController.loginScreen.tapSwitchvehicle();
        guiController.loginScreen.guestvehiclesecond.tap();
        guiController.loginScreen.Displayaudio.tap();
        guiController.loginScreen.next.tap();
        guiController.termsAndConditionsPopup.acceptButton.tap();//Tap YES
        assertTrue(guiController.loginScreen.guestheader.getTextValue().contains("ELANTRA GT"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.14 Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    @Severity(SeverityLevel.MINOR)
    @Story("Guest Login")
    @Description("MyHyundai - Verify Change Blue Link to Bluelink throughout the app as part of Brand renewal")
    void Bluelinktermrenewal() {
        profile = appController.myHyundaiProfiles.gen2HybridStandard;
        guiController.login(profile.primaryAccount, profile.primaryVin);
        assertTrue(guiController.loginScreen.TermsandConditiontext.getTextValue().contains("Bluelink"));
    }
}