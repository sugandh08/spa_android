package tests;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import library.AutoDetect;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Setups up Appium instance before any tests are run
 */
public class TestSetup {

    private AppiumDriver driver;
    private final static String LOCAL_APPIUM_SERVER = "http://0.0.0.0:4723/wd/hub";

    /**
     * Sets up Appium
     * @param appActivity Appium appActivity
     * @param appPackage Appium appPackage
     * @param appWaitActivity Appium appWaitActivity
     * @param appWaitPackage Appium appWaitPackage
     * @throws MalformedURLException
     */
    public void Setup(String appActivity, String appPackage, String appWaitActivity, String appWaitPackage) throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        AutoDetect autoDetect = new AutoDetect();

        // Determine the phone to be tested
        autoDetect.setup();

        // General Appium server capabilities
        capabilities.setCapability("platformName",      autoDetect.getPlatformName());
        capabilities.setCapability("platformVersion",   autoDetect.getPlatformVersion());

        // Android
        if (autoDetect.getPlatformName().equalsIgnoreCase("android")) {
            capabilities.setCapability("automationName", "UiAutomator2");
            capabilities.setCapability("appPackage", appPackage);
            capabilities.setCapability("deviceName", autoDetect.getDeviceName());
            capabilities.setCapability("appActivity",       appActivity);
            capabilities.setCapability("appWaitActivity",   appWaitActivity);
            capabilities.setCapability("appWaitPackage",    appWaitPackage);
            capabilities.setCapability("autoGrantPermissions", "true");
            capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, "true");
            //capabilities.setCapability( capabilityNa, "true");

            driver = new AndroidDriver<>(new URL(LOCAL_APPIUM_SERVER), capabilities);

        }
        // iOS
        else if (autoDetect.getPlatformName().equalsIgnoreCase("ios")) {
            capabilities.setCapability("automationName", "XCUITest");
            capabilities.setCapability("appPackage", "com.hyundaiusa.bluelink2");
            capabilities.setCapability("isHeadless", "false");
            capabilities.setCapability("udid", "c376a245e7c6844f84afeb876afb1adb59c4e721"); // UPDATE THIS WITH THE PHONE'S UDID
            capabilities.setCapability("deviceName", "Test");

            driver = new IOSDriver(new URL(LOCAL_APPIUM_SERVER), capabilities);
        }
    }

    /**
     * @return The created Appium driver with set capabilities.
     */
    public AppiumDriver getDriver() {
        return driver;
    }
}