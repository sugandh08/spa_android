package tests.notification_MYG_old;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;
import views.genesis.screens.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. MobilePrimaryPhone
// 2. LandlinePrimaryPhone
@Disabled
@Epic("SMS Notification Settings Test")
@Feature("SMS Notification Settings")
@DisplayName("SMS Notification Settings")
public class SmsNotificationSettings extends TestController {
    /**
     * Logs in with primary account and primary VIN of given profile, opens settings page
     * @param profile Profile with the primary account and VIN to use
     */
    private void loginAndOpenSettings(Profile profile) {
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        homeScreen.menuButton.tap();
        guiController.menuScreen.settingsButton.tap();
        guiController.settingsScreen.isPresent(60);
    }

    @Nested
    @DisplayName("Settings page opened with mobile phone account")
    class SettingsPageOpenedWithMobilePhoneAccount {
        @BeforeEach
        void setupTests() {
            loginAndOpenSettings(appController.genesisProfiles.mobilePrimaryPhone);
        }

        @Nested
        @DisplayName("Connected Care settings are open")
        class ConnectedCareSettingsAreOpen {
            @BeforeEach
            void openConnectedCareSettings() {
                guiController.settingsScreen.connectedCareExpandButton.tap();
                guiController.settingsScreen.swipe.fromBottomEdge(1);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.2 Pressing the toggle all SMS button in connected care toggles every connected care SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify pressing the toggle all SMS button in connected care toggles every connected care SMS setting")
            void togglingAllConnectedCareSmsSetsAllSmssToSameState() {
                guiController.settingsScreen.connectedCareToggleAllText.tap();

                // get the enabled/disabled state of the 'toggle all SMS' element
                boolean toggleState = guiController.settingsScreen.connectedCareToggleAllText.getSelectedValue();

                // List of setting names in the connected care section
                String[] settingNames = {
                        guiController.settingsScreen.automaticCollisionNotificationText,
                        guiController.settingsScreen.sosEmergencyAssistanceText,
                        guiController.settingsScreen.automaticDtcText,
                        guiController.settingsScreen.maintenanceAlertText
                };

                // Check each connected care setting and make sure the SMS toggle matches the "toggle all" state
                boolean settingStatesCorrect = true;
                for (String settingName : settingNames) {
                    if (guiController.settingsScreen.getSmsNotificationStateOfSetting(settingName) != toggleState) {
                        settingStatesCorrect = false;
                        break;
                    }
                }

                assertTrue(settingStatesCorrect);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.3 ACN SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify ACN SMS status is displayed")
            void acnSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.automaticCollisionNotificationText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.4 ACN SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify ACN SMS toggle changes SMS setting")
            void acnSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.automaticCollisionNotificationText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.5 SOS emergency assistance SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify SOS emergency assistance SMS status is displayed")
            void sosEmergencyAssistanceSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.sosEmergencyAssistanceText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.6 SOS emergency assistance SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify SOS emergency assistance SMS toggle changes SMS setting")
            void sosEmergencyAssistanceSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.sosEmergencyAssistanceText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }


            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.7 Automatic DTC SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify Automatic DTC SMS status is displayed")
            void automaticDtcSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.automaticDtcText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.8 Automatic DTC SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify Automatic DTC SMS toggle changes SMS setting")
            void automaticDtcSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.automaticDtcText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.9 Maintenance Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify Maintenance Alert SMS status is displayed")
            void maintenanceAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.maintenanceAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.10 Maintenance Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Connected Care Settings")
            @Description("Genesis - Verify Maintenance Alert SMS toggle changes SMS setting")
            void maintenanceAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.maintenanceAlertText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }
        }

        @Nested
        @DisplayName("Remote settings are open")
        class RemoteSettingsAreOpen {
            @BeforeEach
            void openRemoteSettings() {
                guiController.settingsScreen.remoteExpandButton.tap();
                guiController.settingsScreen.swipe.fromBottomEdge(2);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.11 Pressing the toggle all SMS button in remote_genesis_old toggles every remote_genesis_old SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Pressing the toggle all SMS button in remote_genesis_old toggles every remote_genesis_old SMS setting")
            void togglingAllRemoteSmsSetsAllSmssToSameState() {
                guiController.settingsScreen.remoteToggleAllText.tap();

                // get the enabled/disabled state of the 'toggle all SMS' element
                boolean toggleState = guiController.settingsScreen.remoteToggleAllText.getSelectedValue();

                // List of setting names in the connected care section
                String[] settingNames = {
                        guiController.settingsScreen.panicNotificationText,
                        guiController.settingsScreen.alarmNotificationText,
                        guiController.settingsScreen.hornAndLightsText,
                        guiController.settingsScreen.remoteEngineStartStopText,
                        guiController.settingsScreen.remoteDoorLockUnlockText,
                        guiController.settingsScreen.curfewAlertText,
                        guiController.settingsScreen.valetAlertText,
                        guiController.settingsScreen.geofenceAlertText,
                        guiController.settingsScreen.speedAlertText
                };

                // Check each connected care setting and make sure the SMS toggle matches the "toggle all" state
                boolean settingStatesCorrect = true;
                for (String settingName : settingNames) {
                    if (guiController.settingsScreen.getSmsNotificationStateOfSetting(settingName) != toggleState) {
                        settingStatesCorrect = false;
                        break;
                    }
                }

                assertTrue(settingStatesCorrect);
            }


            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.12 Panic Notification SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Panic Notification SMS status is displayed")
            void panicNotificationSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.panicNotificationText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.13 Panic Notification SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Panic Notification SMS toggle changes SMS setting")
            void panicNotificationSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.panicNotificationText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.14 Alarm Notification SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Alarm Notification SMS status is displayed")
            void alarmNotificationSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.alarmNotificationText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.15 Alarm Notification SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Alarm Notification SMS toggle changes SMS setting")
            void alarmNotificationSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.alarmNotificationText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.16 Horn and Lights SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Horn and Lights SMS status is displayed")
            void hornAndLightsSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.hornAndLightsText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.17 Horn and Lights SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Horn and Lights SMS toggle changes SMS setting")
            void hornAndLightsSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.hornAndLightsText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.18 Remote Engine Start/Stop SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Remote Engine Start/Stop SMS status is displayed")
            void remoteEngineStartStopSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.remoteEngineStartStopText));
            }


            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.19 Remote Engine Start/Stop SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Remote Engine Start/Stop SMS toggle changes SMS setting")
            void remoteEngineStartStopSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.remoteEngineStartStopText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.20 Remote Door Lock/Unlock SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Remote Door Lock/Unlock SMS status is displayed")
            void remoteDoorLockUnlockSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.remoteDoorLockUnlockText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.21 Remote Door Lock/Unlock SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Remote Door Lock/Unlock SMS toggle changes SMS setting")
            void remoteDoorLockUnlockSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.remoteDoorLockUnlockText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.22 Curfew Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Curfew Alert SMS status is displayed")
            void curfewAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.curfewAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.23 Curfew Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Curfew Alert SMS toggle changes SMS setting")
            void curfewAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.curfewAlertText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.24 Valet Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Valet Alert SMS status is displayed")
            void valetAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.valetAlertText));
            }


            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.25 Valet Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Valet Alert SMS toggle changes SMS setting")
            void valetAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.valetAlertText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.26 Geofence Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Geofence Alert SMS status is displayed")
            void geofenceAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.geofenceAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.27 Geofence Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Geofence Alert SMS toggle changes SMS setting")
            void geofenceAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.geofenceAlertText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.28 Speed Alert SMS status is displayed")
            @Severity(SeverityLevel.MINOR)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Speed Alert SMS status is displayed")
            void speedAlertSmsStatusIsDisplayed() {
                assertTrue(guiController.settingsScreen.checkIfSmsNotificationStateOfSettingExists(
                        guiController.settingsScreen.speedAlertText));
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("2.2.29 Speed Alert SMS toggle changes SMS setting")
            @Severity(SeverityLevel.CRITICAL)
            @Story("Test Remote Settings")
            @Description("Genesis - Verify Speed Alert SMS toggle changes SMS setting")
            void speedAlertSmsToggleChangesSmsSetting() {
                String settingText = guiController.settingsScreen.speedAlertText;
                boolean currentSetting = guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText);

                guiController.settingsScreen.toggleSmsNotificationOfSetting(settingText);
                assertTrue(guiController.settingsScreen.getSmsNotificationStateOfSetting(settingText) != currentSetting);
            }
        }
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.2.1 Phoneline set to Landline prevents SMS toggle")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Connected Care Settings")
    @Description("Genesis - Verify Phoneline set to Landline prevents SMS toggle")
    void landlinePhonePreventsSmsToggle() {
        loginAndOpenSettings(appController.genesisProfiles.landlinePrimaryPhone);
        guiController.settingsScreen.connectedCareExpandButton.tap();
        assertFalse(guiController.settingsScreen.connectedCareToggleAllText.getClickableValue());
    }
}
