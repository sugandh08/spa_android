package tests.myhyundai.connectedcare;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen1Standard


@Epic("Gen1 MVR Report Test")
@Feature("Gen1 MVR Report")
public class Gen1MvrReports extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.1 MVR history page shows a MVR report")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Gen1 MVR Report")
    @Description("MyHyundai - Verify MVR history page shows a MVR report")
    void mvrHistoryPageShowsMvrReport() {
        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
       /* guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.carCareButton,
                guiController.gasHomeScreen.carCareOptions.monthlyVehicleReport
        ); commenting for now pushpa */
        if (guiController.monthlyVehicleReportScreen.isPresent(30)) {
            guiController.monthlyVehicleReportScreen.previousReportsButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1,4000);
        assertTrue(guiController.monthlyVehicleReportHistoryScreen.oldMvrFirstEntryContainerView.elementExists(),
                "This failure can be for a couple reasons. The most likely is that the test got caught on the" +
                        "Car Care expandable options list after selecting it on the home screen. Also, it will fail" +
                        "if there is no entries in the MVR history, which is possible depending on the vehicle. The " +
                        "least likely reason is that the app actually bugged out, and the test was an actual fail.");
    }
}
