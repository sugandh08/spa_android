package tests.myhyundai.login;

import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Login Errors And Token")
@Feature("Login Errors And Token")
public class LoginErrorsAndToken extends TestController {
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.1 Entering invalid email shows invalid email popup")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Errors And Token")
    @Description("MyHyundai - Verify Entering invalid email shows invalid email popup")
    void invalidEmailLoginShowsInvalidEmailPopup() {
        guiController.loginScreen.usernameEditText.enterText("invalid email");
        guiController.loginScreen.passwordEditText.enterText("invalid password");
        guiController.loginScreen.loginButton.tap();
        assertTrue(guiController.invalidUsernamePopup.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.2 Entering invalid password shows invalid password popup")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Errors And Token")
    @Description("MyHyundai - Verify Entering invalid password shows invalid password popup")
    void invalidPasswordLoginShowsInvalidPasswordPopup() {
        guiController.loginScreen.usernameEditText.enterText(appController.myHyundaiProfiles.gen2GasOrHevStandard.primaryAccount.email);
        guiController.loginScreen.passwordEditText.enterText("invalid password");
        guiController.loginScreen.loginButton.tap();
        assertTrue(guiController.invalidPasswordPopup.isPresent());
    }
}
