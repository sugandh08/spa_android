package tests.myhyundai.login;

import config.Profile;
import config.Vehicle;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

// Profiles Used:
// 1. Gen1Standard
// 2. TwoGen2
// 3. MyHyundaiAndGenesisVehicles
// 4. GenesisVehicleOnly
// 5. WithSecondaryDriver
// 6. Gen2ElectricStandard
// 7. Gen2HybridStandard
// 8. Gen2GasOrHevStandard

@Epic("Login Account Variations")
@Feature("Login Account Variations")
class LoginAccountVariations extends TestController {
    @Nested
    @DisplayName("1.1.1 Logging into an account and selecting a Gen 1 vehicle shows correct Gen 1 options")
    class AccountWithOnlyGen1VehicleShowsGen1Options {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;

            guiController.loginAndGoToHomePageWithSelectVehicle(
                    profile.primaryAccount, profile.primaryVin, 60);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.1.1 Remote stop shows not supported popup")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Remote stop shows not supported popup")
        void remoteStopShowsNotSupportedPopup() {
            guiController.remoteCarControls.clickOnRemoteStopButton();
            assertTrue(guiController.featureNotSupportedPopup.isPresent());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.1.2 Remote start shows not supported popup")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Remote start shows not supported popup")
        void remoteStartShowsNotSupportPopup() {
            guiController.remoteCarControls.clickOnRemoteStartButton();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.featureNotSupportedPopup.isPresentRemoteStartNotSupported());
        }
    }

    @Nested
    @DisplayName("1.1.2 Account with two Gen 2 vehicles allows user to select and switch vehicles")
    class VehicleSelectionOnAccountWithTwoGen2Vehicles {
        private Profile profile;
        private Vehicle primaryVehicle;
        private Vehicle secondaryVehicle;
        private HomeScreen primaryVehicleHomeScreen;
        private HomeScreen secondaryVehicleHomeScreen;

        @BeforeEach
        void setupTests() {

            profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
            guiController.loginAndGoToHomePageWithSelectVehicle(
                    profile.primaryAccount, profile.primaryVin, 60);
            System.out.println("))))))))))))))))))---------->"+profile);
          //  guiController.loginAndGoToHomePageWithSelectVehicle(profile.secondaryAccount, profile.primaryVin);
        //    profile = appController.myHyundaiProfiles.twoGen2;
            primaryVehicle = profile.primaryAccount.vehicles.get(profile.primaryVin);
            System.out.println("primaryVehicle---------->"+primaryVehicle);
            secondaryVehicle = profile.primaryAccount.vehicles.get(profile.secondaryVin);
            System.out.println("secondaryVehicle---------->"+secondaryVehicle);
            primaryVehicleHomeScreen = guiController.getHomeScreenByPowerType(primaryVehicle.powerType);
            secondaryVehicleHomeScreen = guiController.getHomeScreenByPowerType(secondaryVehicle.powerType);

            guiController.loginScreen.loginWithAccount(profile.primaryAccount);
            appController.appFunctions.pauseTestExecution(3, 6000);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.2.1 Selecting first vehicle opens home page for first vehicle")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Selecting first vehicle opens home page for first vehicle")
        void selectingFirstVehicleOpensHomePageOfFirstVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(primaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();

            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            primaryVehicleHomeScreen.clickOnHamburgerMenuButton();
            assertTrue(guiController.menuScreen.vinTextView.getTextValue().contains(primaryVehicle.vin));
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.2.2 Selecting second vehicle opens home page for second vehicle")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Selecting second vehicle opens home page for second vehicle")
        void selectingSecondVehicleOpensHomePageOfSecondVehicle() {

            guiController.selectVehicleScreen.tapVehicleButtonByVin(secondaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                System.out.println("Charge settings not set popup detected");
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            secondaryVehicleHomeScreen.clickOnHamburgerMenuButton();
            assertTrue(guiController.menuScreen.vinTextView.getTextValue().contains(secondaryVehicle.vin));
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.2.3 Switching to second vehicle opens home page for second vehicle")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Switching to second vehicle opens home page for second vehicle")
        void switchingToSecondVehicleOpensHomePageForSecondVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(primaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            primaryVehicleHomeScreen.clickOnHamburgerMenuButton();
            guiController.menuScreen.switchVehicleButton.tap();
            guiController.selectVehicleScreen.tapVehicleButtonByVin(secondaryVehicle.vin);
            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            secondaryVehicleHomeScreen.clickOnHamburgerMenuButton();
            assertTrue(guiController.menuScreen.vinTextView.getTextValue().contains(secondaryVehicle.vin));
        }


        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.2.4 Switching to first vehicle opens home page for first vehicle")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Switching to first vehicle opens home page for first vehicle")
        void switchingToFirstVehicleOpensHomePageForFirstVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(secondaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            primaryVehicleHomeScreen.clickOnHamburgerMenuButton();
            guiController.menuScreen.switchVehicleButton.tap();
            guiController.selectVehicleScreen.tapVehicleButtonByVin(primaryVehicle.vin);
            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            primaryVehicleHomeScreen.clickOnHamburgerMenuButton();
            assertTrue(guiController.menuScreen.vinTextView.getTextValue().contains(primaryVehicle.vin));
        }
    }

    @Disabled // TODO currently don't have an account that matches this
    @Nested
    @DisplayName("1.1.3 Account with Gen 2 vehicle and Genesis vehicle has correct functionality")
    class AccountWithGen2AndGenesisVehiclesFunctionsCorrectly {
        private Profile profile;
        private Vehicle gen2Vehicle;
        private Vehicle genesisVehicle;
        private HomeScreen gen2HomeScreen;

        @BeforeEach
        void setupTests() {
            profile = appController.myHyundaiProfiles.myHyundaiAndGenesisVehicles;
            gen2Vehicle = profile.primaryAccount.vehicles.get(profile.primaryVin);
            genesisVehicle = profile.primaryAccount.vehicles.get(profile.secondaryVin);
            gen2HomeScreen = guiController.getHomeScreenByPowerType(gen2Vehicle.powerType);

            guiController.loginScreen.loginWithAccount(profile.primaryAccount);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.3.1 Selecting Gen 2 MyHyundai vehicle opens homepage for that vehicle")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Selecting Gen 2 MyHyundai vehicle opens homepage for that vehicle")
        void selectingGen2OpensCorrectHomepage() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(gen2Vehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            if (guiController.chargeScheduleNotSetPopup.isPresent()) {
                guiController.chargeScheduleNotSetPopup.okButton.tap();
            }
            gen2HomeScreen.clickOnHamburgerMenuButton();
            assertTrue(guiController.menuScreen.vinTextView.getTextValue().contains(gen2Vehicle.vin));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.3.2 Selecting Genesis vehicle prompts to open Genesis app")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Selecting Genesis vehicle prompts to open Genesis app")
        void selectingGenesisPromptsToOpenGenesisApp() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(genesisVehicle.vin);
            assertTrue(guiController.genesisRedirectPopup.isPresent(), "Note that the Genesis vehicle must " +
                    "be set up properly to trigger the switch to the Genesis app. This failure can occur if it is " +
                    "not set up properly. Not necessarily indicative of problems with the app.");
        }
    }

    @Nested
    @DisplayName("1.1.4 Logging in to account with only a Genesis vehicle prompts to switch to Genesis app")
    class AccountWithOnlyGenesisVehiclePromptsToSwitchApps {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.myHyundaiProfiles.genesisVehicleOnly;

            guiController.loginScreen.loginWithAccount(profile.primaryAccount);
            appController.appFunctions.pauseTestExecution(2, 6000);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.4.1 Redirect prompt appears")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Redirect prompt appears")
        void redirectPromptAppears() {
            assertTrue(guiController.genesisRedirectPopup.isPresent());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.4.2 Redirect prompt open button opens Genesis app")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Redirect prompt open button opens Genesis app")
        void redirectPromptOpenButtonOpensGenesisApp() {
            // reinstall genesis to make sure there is no cached/saved data in it.
            appController.appFunctions.uninstallGenesisAndroid();
            appController.appFunctions.installApp(appController.getGenesisAndroidFileName());

            guiController.genesisRedirectPopup.openButton.tap();
            assertTrue(guiController.genesisIntroductionScreen.isPresent());

            // back out of the genesis app
            appController.appFunctions.tapAndroidBackButton();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.4.3 Redirect prompt close button closes popup")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("MyHyundai - Verify Redirect prompt close button closes popup")
        void redirectPromptCloseButtonDismissesPopup() {
            guiController.genesisRedirectPopup.closeButton.tap();
            assertFalse(guiController.genesisRedirectPopup.isPresent());
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.5 Logging in as secondary driver opens shared vehicle home page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Account Variations")
    @Description("MyHyundai - Verify Logging in as secondary driver opens shared vehicle home page")
    void loggingInAsSecondaryDriverOpensCorrectHomeScreen() {
        Profile profile = appController.myHyundaiProfiles.withSecondaryDriver;
        // using "vehicleFromPrimaryAccount" because the vehicle should be the exact same on both accounts,
        // however to make sure the test throws no false positives, use information from the primary account
        // while logging in on the secondary account. If any steps fail, they didn't have the same vehicle or
        // the vehicle data is stored incorrectly on the app side.
        Vehicle vehicleFromPrimaryAccount = profile.primaryAccount.vehicles.get(profile.primaryVin);
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(vehicleFromPrimaryAccount.powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.secondaryAccount, vehicleFromPrimaryAccount.vin);
        if (guiController.chargeScheduleNotSetPopup.isPresent()) {
            guiController.chargeScheduleNotSetPopup.okButton.tap();
        }
        homeScreen.clickOnHamburgerMenuButton();
        assertTrue(guiController.menuScreen.vinTextView.getTextValue().contains(vehicleFromPrimaryAccount.vin));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.6 Logging in and selecting an AE-EV vehicle opens AE-EV home page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Account Variations")
    @Description("MyHyundai - Verify Logging in and selecting an AE-EV vehicle opens AE-EV home page")
    void selectingAEEVVehicleOpensAEEVHomePage() {
        // Extra checks to make sure AEEV config is correct.
        Profile profile = new Profile();
        try {
            profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        } catch (NullPointerException npe) {
            System.out.println("No AEEV information found in config. Or, AEEV config information is incorrect.");
        }

        assumeTrue(profile.primaryAccount != null);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        if (guiController.chargeScheduleNotSetPopup.isPresent()) {
            guiController.chargeScheduleNotSetPopup.okButton.tap();
        }
        appController.appFunctions.pauseTestExecution(5, 10000);
        assertTrue(guiController.electricHomeScreen.isPresent(30));
    }

    @Disabled // TODO disabled until we get the lfp125 account back
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.7 Logging in and selecting a PHEV vehicle opens PHEV home page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Account Variations")
    @Description("MyHyundai - Verify Logging in and selecting a PHEV vehicle opens PHEV home page")
    void selectingPHEVVehicleOpensPHEVHomePage() {
        // Extra checks to make sure PHEV config is correct.
        Profile profile = new Profile();
        try {
            profile = appController.myHyundaiProfiles.gen2HybridStandard;
        } catch (NullPointerException npe) {
            System.out.println("No PHEV information found in config. Or, PHEV config information is incorrect.");
        }

        assumeTrue(profile.primaryAccount != null);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        if (guiController.chargeScheduleNotSetPopup.isPresent()) {
            guiController.chargeScheduleNotSetPopup.okButton.tap();
        }
        System.out.println("\nNOTE: Above 'element not found' message is likely not an error");
        assertTrue(guiController.hybridHomeScreen.isPresent(30));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.8 Logging in and selecting a gen 2 gas vehicle opens gen 2 gas home page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Account Variations")
    @Description("MyHyundai - Verify Logging in and selecting a gen 2 gas vehicle opens gen 2 gas home page")
    void selectingGen2GasVehicleOpensGen2GasHomePage() {
        Profile profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        assertTrue(guiController.gasHomeScreen.isPresent(30));
    }
}