package tests.myhyundai.login;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("Login Terms and Conditions Test")
@Feature("Login Terms and Conditions")
@DisplayName("Login Terms and Conditions")
public class LoginTermsAndConditions extends TestController {



    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.2.1 View Terms and Conditions at the time of Login")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Terms and Conditions")
    @Description("MyHyundai - Verify viewing Terms and Conditions at the time of Login")
    void LoginAndViewTermsAndConditions() {
        Profile profile = appController.myHyundaiProfiles.gen2OSElectricStandard;


        appController.appFunctions.uninstallMyHyundaiAndroid();
        appController.appFunctions.installApp(appController.getMyHyundaiAndroidFileName());

        appController.appFunctions.launchApp();


        guiController.loginScreen.loginWithAccount(profile.primaryAccount);

        appController.appFunctions.pauseTestExecution(1,6000);

        guiController.termsAndConditionsPopup.viewDetailsButton.tap();

        appController.appFunctions.pauseTestExecution(1,10000);
        assertTrue(guiController.termsAndConditionsScreen.isPresent());


        appController.appFunctions.tapAndroidBackButton();



        guiController.termsAndConditionsPopup.acceptButton.tap();


        //appController.appFunctions.pauseTestExecution(1,10000);



       guiController.osElectricHomeScreen.isPresent(20);



    }
}
