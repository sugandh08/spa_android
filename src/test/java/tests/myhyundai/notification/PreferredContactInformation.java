package tests.myhyundai.notification;

import config.Account;
import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

// Profiles Used:
// 1. NotificationEmailSameAsLogin
// 2. NotificationEmailDifferentThanLogin
// 3. MobilePrimaryPhone
// 4. LandlinePrimaryPhone

@Epic("Preferred Contact Information Test")
@Feature("Preferred Contact Information")
@DisplayName("Preferred Contact Information")
public class PreferredContactInformation extends TestController {
    /**
     * Logs in with the chosen account, selects the chosen vehicle, then navigates to the Preferred Contact
     * Settings page.
     * @param account account to login with
     * @param vin vin to select
     */
    void loginAndGoToPreferredContactSettings(Account account, String vin) {
        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(account.vehicles.get(vin).powerType);

        guiController.loginAndGoToHomePageWithSelectVehicle(account, vin);
        appController.appFunctions.pauseTestExecution(5, 10000);
                homeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.settingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        guiController.settingsScreen.preferredContactInformationButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.1 When notification email is same as account email, notification email is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification email is same as account email, notification email is displayed")
    void notificationEmailDisplayedWhenNotificationEmailSameAsAccountEmail() {
        Profile profile = appController.myHyundaiProfiles.notificationEmailSameAsLogin;

        loginAndGoToPreferredContactSettings(profile.primaryAccount, profile.primaryVin);

        assertEquals("Email: " + profile.primaryAccount.notificationEmail,
                guiController.preferredContactInformationScreen.preferredContactEmailTextView.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.2 When notification email is different than account email, notification email is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification email is different than account email, notification email is displayed")
    void notificationEmailDisplayedWhenNotificationEmailDifferentThanAccountEmail() {
        Profile profile = appController.myHyundaiProfiles.notificationEmailDifferentThanLogin;

        loginAndGoToPreferredContactSettings(profile.primaryAccount, profile.primaryVin);
        String expectedEmail = "Email: " + profile.primaryAccount.email;
        assertFalse(expectedEmail.equals(guiController.preferredContactInformationScreen.preferredContactEmailTextView.getTextValue()));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.3 When notification phone is mobile, notification phone number is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification phone is mobile, notification phone number is displayed")
    void notificationPhoneDisplayedWhenNotificationPhoneIsMobile() {
        Profile profile = appController.myHyundaiProfiles.mobilePrimaryPhone;

        loginAndGoToPreferredContactSettings(profile.primaryAccount, profile.primaryVin);


        assertEquals("Phone: " + profile.primaryAccount.mobilePhone,
                guiController.preferredContactInformationScreen.preferredContactPhoneNumberTextView.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.3.4 When notification phone is landline, notification phone number is not displayed")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Preferred Contact Information")
    @Description("MyHyundai - Verify when notification phone is landline, notification phone number is not displayed")
    void notificationPhoneDisplayedWhenNotificationPhoneIsLandline() {
        Profile profile = appController.myHyundaiProfiles.landlinePrimaryPhone;

        loginAndGoToPreferredContactSettings(profile.primaryAccount, profile.primaryVin);
        System.out.println(guiController.preferredContactInformationScreen.preferredContactPhoneNumberTextView.getTextValue());

       assertFalse(guiController.preferredContactInformationScreen.preferredContactPhoneNumberTextView.getTextValue().equals("Phone: "+profile.primaryAccount.landlinePhone));

    }
}
