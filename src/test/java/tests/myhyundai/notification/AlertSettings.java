package tests.myhyundai.notification;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Alert Settings Test")
@Feature("Alert Settings")
@DisplayName("Alert Settings")
public class AlertSettings extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);

        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        guiController.alertSettingsScreen.isPresent(60);
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.1 Toggling Speed Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Speed Alert setting does not change the setting value")
    void togglingSpeedAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.speedAlertToggle;

        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.2 Toggling Valet Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Valet Alert setting does not change the setting value")
    void togglingValetAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.valetAlertToggle;

        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.3 Toggling Curfew Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Curfew Alert setting does not change the setting value")
    void togglingCurfewAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.curfewAlertToggle;

        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.4 Toggling Geo-Fence Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Toggling Geo-Fence Alert setting does not change the setting value")
    void togglingGeoFenceAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.geoFenceAlertToggle;

        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        appController.appFunctions.tapAndroidBackButton();
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.5 Selecting to set up a Speed Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Speed Alert opens correct settings page")
    void selectingToOpenSpeedAlertOpensCorrectSettingsPage() {
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.6 Selecting to set up a Valet Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Valet Alert opens correct settings page")
    void selectingToOpenValetAlertOpensCorrectSettingsPage() {
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.7 Selecting to set up a Curfew Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Curfew Alert opens correct settings page")
    void selectingToOpenCurfewAlertOpensCorrectSettingsPage() {
        guiController.alertSettingsScreen.curfewAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.curfewAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.8 Selecting to set up a Geo-Fence Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("MyHyundai - Verify Selecting to set up a Geo-Fence Alert opens correct settings page")
    void selectingToOpenGeoFenceAlertOpensCorrectSettingsPage() {
        guiController.alertSettingsScreen.geoFenceAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.geoFenceAlertScreen.isPresent());
    }
}
