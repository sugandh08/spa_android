package tests.myhyundai.ev;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2ElectricStandard


@Epic("EV Login")
@Feature("Ev Login")
public class EVLogin extends TestController{
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2OSElectricStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 30);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(60);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.1 AE-EV home screen appears after login")
    @Severity(SeverityLevel.MINOR)
    @Story("Test EV Login")
    @Description("MyHyundai - Verify AE-EV home screen appears after login")
    void aeevHomeScreenAppearsAfterLogin() {
        assertTrue(guiController.electricHomeScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.2 Selecting Charge Stations opens map view with Enter Zip or City in search box")
    @Severity(SeverityLevel.MINOR)
    @Story("Test EV Login")
    @Description("MyHyundai - Verify Selecting Charge Stations opens map view with Enter Zip or City in search box")
    void selectingChargeStationOpensMapWithChargeStationInSearchBox() {
      /*  guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.electricHomeScreen,
                guiController.electricHomeScreen.mapButton,
                guiController.electricHomeScreen.mapOptions.chargeStationsButton
        );  commenting for now pushpa */
        appController.appFunctions.pauseTestExecution(1,5000);
        assertEquals("Enter Zip or City", guiController.chargeStationScreen.searchEditText.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.3 The state of the electric vehicle is displayed in MVR")
    @Severity(SeverityLevel.MINOR)
    @Story("Test EV Login")
    @Description("MyHyundai - Verify The state of the electric vehicle is displayed in MVR")
    void electricVehicleStateDisplayedInMvr() {
      /*  guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.electricHomeScreen,
                guiController.electricHomeScreen.carCareButton,
                guiController.electricHomeScreen.carCareOptions.monthlyVehicleReport
        );  commenting for now pushpa */
        appController.appFunctions.pauseTestExecution(1,5000);
        assertTrue(guiController.monthlyVehicleReportScreen.electricVehicleTextView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.4 Home screen shows vehicle is not plugged in")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test EV Login")
    @Description("MyHyundai - Verify Home screen shows vehicle is not plugged in")
    void homeScreenShowsVehicleNotPluggedIn() {
        assertTrue(guiController.electricHomeScreen.pluginStatusTextView.getTextValue().contains("Not Plugged In"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.5 Home screen shows vehicle is not charging")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test EV Login")
    @Description("MyHyundai - Verify Home screen shows vehicle is not charging")
    void homeScreenShowsVehicleNotCharging() {
        assertTrue(guiController.electricHomeScreen.chargingStatusTextView.getTextValue().contains("Not Charging"));
    }
}
