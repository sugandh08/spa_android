package tests.myhyundai.ev;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2 OS ElectricStandard

@Epic("Charge Management")
@Feature("Charge Management")
public class ChargeManagement extends TestController{
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2OSElectricStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 30);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.osElectricHomeScreen.isPresent(60);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.5.1 Set Up Charge Management Schedule and go back without editing/saving schedule")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Charge Management")
    @Description("MyHyundai - Verify to Set Up Charge Management Schedule and go back without editing/saving schedule")
    void osevGoBackWithoutSavingSchedule() {

        assertTrue(guiController.osElectricHomeScreen.isPresent());
       System.out.println(guiController.osElectricHomeScreen.setScheduleButton_os.elementExists(10));

       for(int i=0;i<3;i++)
       {
           guiController.osElectricHomeScreen.setScheduleButton_os.tap(10);
       }
        assertTrue( guiController.chargeManagementScreen.isPresent(60));
        appController.appFunctions.tapAndroidBackButton();
        assertTrue( guiController.osElectricHomeScreen.isPresent(60));
    }


}
