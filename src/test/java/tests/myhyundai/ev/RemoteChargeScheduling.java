package tests.myhyundai.ev;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Remote Charge Scheduling")
@Feature("Remote Charge Scheduling")

public class RemoteChargeScheduling extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2ElectricStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.electricHomeScreen.isPresent(60);
        guiController.electricHomeScreen.scheduleDetailTextView.tap();
        guiController.electricHomeScreen.scheduleDetailTextView.tap();
        guiController.setChargeScheduleScreen.isPresent(90);
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.2.1 Setting charge to Schedule 1 only shows Schedule 1 information on home screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Remote Charge Scheduling")
    @Description("MyHyundai - Verify Setting charge to Schedule 1 only shows Schedule 1 information on home screen")
    void settingChargeToSchedule1OnlyShowsCorrectInformationOnHomeScreen() {
        String schedule1Details = guiController.setChargeScheduleScreen.schedule1Time.getTextValue() +
                guiController.setChargeScheduleScreen.schedule1Meridian.getTextValue() +
                " ON";

        // make sure schedule 1 is toggled on
        if (!guiController.setChargeScheduleScreen.schedule1Toggle.getCheckedValue()) {
            guiController.setChargeScheduleScreen.schedule1Toggle.tap();
        }
        // make sure schedule 2 is toggled off
        if (guiController.setChargeScheduleScreen.schedule2Toggle.getCheckedValue()) {
            guiController.setChargeScheduleScreen.schedule2Toggle.tap();
        }
        // make sure schedule off peak is toggled off
        if (guiController.setChargeScheduleScreen.scheduleOffPeakToggle.getCheckedValue()) {
            guiController.setChargeScheduleScreen.scheduleOffPeakToggle.tap();
        }
        // save
        guiController.setChargeScheduleScreen.submitButton.tap();
        guiController.saveChargeSchedulePopup.saveButton.tap();
        guiController.setChargeScheduleScreen.isPresent();
        guiController.setChargeScheduleScreen.backButton.tap();
        guiController.electricHomeScreen.isPresent();
        assertTrue(guiController.setChargeScheduleSuccessPopup.isPresent(180));
        guiController.setChargeScheduleSuccessPopup.saveButton.tap();
        guiController.electricHomeScreen.isPresent();
        assertEquals("Schedule 1", guiController.electricHomeScreen.scheduleTitleTextView.getTextValue());
        assertEquals(schedule1Details, guiController.electricHomeScreen.scheduleDetailTextView.getTextValue());
    }
}
