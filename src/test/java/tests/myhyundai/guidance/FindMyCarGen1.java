package tests.myhyundai.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen1Standard

@Epic("Find My Car Gen1 Test")
@Feature("Find My Car Gen1")
@DisplayName("Find My Car Gen1")
public class FindMyCarGen1 extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;

        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.7.1 Entering correct PIN opens Car Finder screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Find My Car Gen1")
    @Description("MyHyundai - Verify Entering correct PIN opens Car Finder screen")
    void enteringCorrectPinOpensCarFinderScreen() {
       /* guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.homeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.carFinderButton
        ); commenting for now pushpa  */
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        assertTrue(guiController.carFinderNotFoundGen1Popup.isPresent(60) ||
                guiController.carFinderScreen.isPresent());
    }
}
