package tests.myhyundai.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;




import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen1Standard


@Epic("POI Functions Gen1 Test")
@Feature("POI Functions Gen1")
@DisplayName("POI Functions Gen1")
public class PoiFunctionsGen1 extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen1Standard;

        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 80);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.1 Selecting POI Search opens POI Search page")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify Selecting POI Search opens POI Search page")
    void selectingPoiSearchOpensPoiSearchPage() {
       /* guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.poiSearchButton,
                guiController.poiSearchScreen
        ); commenting for now pushpa */
        assertTrue(guiController.poiSearchScreen.isPresent());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.2 Selecting POI Search list view shows POI search results")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify Selecting POI Search list view shows POI search results")
    void selectingPoiSearchListViewShowsAnItemInList() {
      /*  guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.poiSearchButton,
                guiController.poiSearchScreen
        ); commenting for now pushpa */
        guiController.poiSearchScreen.searchEditText.enterText("mcdonalds");
        guiController.poiSearchScreen.searchButton.tap();
        guiController.poiSearchScreen.searchButton.tap();
        guiController.poiSearchScreen.listButton.tap();
        assertTrue(guiController.poiSearchScreen.searchResultsListView.elementExists());
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.3 Selecting My POIs list view shows My POI results in list")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify Selecting My POIs list view shows My POI results in list")
    void selectingMyPoisListViewShowsAnItemInList() {
        /*guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.myPoiButton
        ); commenting for now pushpa */
        guiController.poiSearchScreen.listButton.tap();
        assertTrue(guiController.favoritesPOIScreen.searchResultsListView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.4 User is able to search for a Hyundai Dealer")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify User is able to search for a Hyundai Dealer")
    void hyundaiDealerSearchIsEditableAndButtonWorks() {
        String zipCode = "55555";

       /* guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.hyundaiDealersButton
        );  commenting for now pushpa*/
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchButton.tap();
        guiController.dealerLocatorScreen.searchButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertEquals(zipCode, guiController.dealerLocatorScreen.searchEditText.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.5 Selecting Hyundai Dealers list view shows search results in list")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify Selecting Hyundai Dealers list view shows search results in list")
    void selectingHyundaiDealersListViewShowsAnItemInList() {
        String zipCode = "55555";

      /*  guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.hyundaiDealersButton
        ); commenting for now pushpa*/
        guiController.dealerLocatorScreen.searchEditText.enterText(zipCode);
        guiController.dealerLocatorScreen.searchButton.tap();
        guiController.dealerLocatorScreen.searchButton.tap();
        guiController.dealerLocatorScreen.listButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.dealerLocatorScreen.searchResultsListView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.6 User is able to search for nearby gas stations")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify User is able to search for nearby gas stations")
    void nearbyGasSearchIsEditableAndButtonWorks() {
        String zipCode = "55555";
/*
        guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.nearbyGasButton
        ); commenting for now pushpa */
        guiController.nearbyGasScreen.searchEditText.enterText(zipCode);
        guiController.nearbyGasScreen.searchButton.tap();
        guiController.nearbyGasScreen.searchButton.tap();
        assertEquals(zipCode, guiController.nearbyGasScreen.searchEditText.getTextValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.2.7 Sending Nearby Gas list view shows search results in list")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Functions Gen1")
    @Description("MyHyundai - Verify Sending Nearby Gas list view shows search results in list")
    void selectingNearbyGasListViewShowsAnItemInList() {
        String zipCode = "55555";
/*
        guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.nearbyGasButton
        ); commenting for now pushpa */
        appController.appFunctions.pauseTestExecution(1,3000);
        guiController.nearbyGasScreen.searchEditText.enterText(zipCode);
        guiController.nearbyGasScreen.searchButton.tap();
        guiController.nearbyGasScreen.searchButton.tap();
        guiController.nearbyGasScreen.listButton.tap();
        appController.appFunctions.pauseTestExecution(1,3000);
        assertTrue(guiController.nearbyGasScreen.searchResultsListView.elementExists());
    }



    }


