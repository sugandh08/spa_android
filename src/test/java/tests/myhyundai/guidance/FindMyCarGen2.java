package tests.myhyundai.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Find My Car Gen2 Test")
@Feature("Find My Car Gen2")
@DisplayName("Find My Car Gen2")
public class FindMyCarGen2 extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.6.1 Entering correct PIN opens Car Finder screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Find My Car Gen2")
    @Description("MyHyundai - Verify Entering correct PIN opens Car Finder screen")
    void enteringCorrectPinOpensCarFinderScreen() {
        /*guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.carFinderButton
        );  commenting for now pushpa  */
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        if (guiController.outOfRangePopup.isPresent(60)) {
            guiController.outOfRangePopup.cancelButton.tap();
        }
        assertTrue(guiController.carFinderScreen.isPresent());
        appController.appFunctions.pauseTestExecution(5, 10000);
    }


    /*@RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.6.2 Entering an incorrect PIN 3 times locks PIN")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Find My Car Gen2")
    @Description("MyHyundai - Verify Entering an incorrect PIN 3 times locks PIN")
    void enteringIncorrectPinThreeTimesLocksPin() {
        String incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);

        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.carFinderButton
        );
        guiController.enterPinScreen.enterPin(incorrectPin);
        guiController.incorrectPinCarFinderPopup.okButton.tap();
        guiController.carFinderScreen.homeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.carFinderButton
        );
        guiController.enterPinScreen.enterPin(incorrectPin);
        guiController.incorrectPinCarFinderPopup.okButton.tap();
        guiController.carFinderScreen.homeButton.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.mapButton,
                guiController.gasHomeScreen.mapOptions.carFinderButton
        );
        guiController.enterPinScreen.enterPin(incorrectPin);
            assertTrue(guiController.pinLockedCarFinderPopup.isPresent());

        // reset PIN
        if (guiController.pinLockedCarFinderPopup.isPresent()) {
            guiController.pinLockedCarFinderPopup.okButton.tap();
            guiController.carFinderScreen.homeButton.tap();
            guiController.resetLockedPinFromHomeScreen(profile.primaryAccount, profile.primaryVin);
        }
    }*/
}
