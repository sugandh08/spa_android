package tests.myhyundai.remote_MYH;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard


@Epic("Remote Start Test")
@Feature("Remote Start")
@DisplayName("Remote Start")
public class RemoteStart extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
      //  remoteCommandButton = guiController.gasHomeScreen.remoteCommands.startButton;
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.1 Remote start success of Gen2 with Climate OFF/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success of Gen2 with Climate OFF/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessOfGen2WithClimateOffDefrostOffHeatedServicesOff() {
        remoteCommandButton.tap();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(false,false,false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.2 Remote start success of Gen2 with Climate ON/Defrost OFF/HeatedServicesOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success of Gen2 with Climate ON/Defrost OFF/HeatedServicesOFF")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOffHeatedServicesOff() {
        remoteCommandButton.tap();
        guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreen.setStartSettingsStates(true,false,false);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.2.2 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();

            guiController.remoteStartSettingsScreen.engineDurationExpandButton.tap();
            guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
            // wait a second for the engine duration text to update
            appController.appFunctions.pauseTestExecution(1, 1000);
            // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
            int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
            guiController.remoteStartSettingsScreen.setStartSettingsStates(false,false,false);
            guiController.remoteStartSettingsScreen.submitButton.tap();

            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

   /* @Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.startButton);
        }

        @Nested
        @DisplayName("7.1.5 Remote start with incorrect PIN three times")
        class RemoteStartWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteStartWithIncorrectPinThreeTimes() {
                remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.startButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.1.5.1 Error popup is displayed saying how to reset PIN")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying how to reset PIN")
            void errorPopupIsDisplayedSayingHowToResetPin() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.1.5.2 User is restored access to remote_genesis_old commands after 5 minutes")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote_genesis_old commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.cancelButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                remoteCommandButton.tap();
                guiController.remoteStartSettingsScreen.submitButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }
    }*/



}
