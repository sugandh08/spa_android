package tests.myh_gen2Point5.remote_MYH;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Horn And Lights Test")
@Feature("Remote Horn And Lights")
@DisplayName("Remote Horn And Lights")
public class RemoteHornAndLights extends TestController {
    private Profile profile;
    private String incorrectPin;
    private RemoteTestsHelper remoteTestsHelper;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteTestsHelper = new RemoteTestsHelper(appController, guiController, profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.1 Remote horn and lights success shows successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote horn and lights success shows successful message")
    void successfulRemoteHornAndLightsShowsPopupMessage() {
        guiController.remoteCarControls.clickOnRemoteHornAndLightsButton();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.requestSentPopup.okButton.tap();
        assertTrue(guiController.gasHomeScreen.remoteCommandStatusLabel.getTextValue().contains("Horn and Lights"));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE HORN AND LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Horn & Lights") && resultText.contains("sent to vehicle"));
    }


    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.6.2 Remote horn and lights with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify  horn and lights with incorrect PIN shows incorrect PIN notification")
        void remoteHornAndLightsWithIncorrectPinShowsIncorrectPinNotification() {
            guiController.remoteCarControls.clickOnRemoteHornAndLightsButton();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }

  /*  @Nested
    class PinResetsAfterTest {
        @AfterEach
        void resetPin() {
            remoteTestsHelper.resetPin(incorrectPin, guiController.gasHomeScreen.remoteCommands.hornAndLightsButton);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.6.2 Remote horn and lights with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("MyHyundai - Verify remote_genesis_old horn and lights with incorrect PIN shows incorrect PIN notification")
        void remoteHornAndLightsWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
        }

        @Nested
        @DisplayName("7.6.3 Remote horn and lights with incorrect PIN three times")
        class RemoteHornAndLightsWithIncorrectPinEnteredThreeTimes {
            @BeforeEach
            void sendRemoteHornAndLightsWithIncorrectPinThreeTimes() {
                remoteTestsHelper.setPinStateToLocked(incorrectPin, guiController.gasHomeScreen.remoteCommands.hornAndLightsButton);
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.6.3.1 Error popup is displayed saying how to reset PIN")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify error popup is displayed saying how to reset PIN")
            void errorPopupIsDisplayedSayingHowToResetPin() {
                assertTrue(guiController.pinLockedPopup.isPresent());
            }

            @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("7.6.3.2 User is restored access to remote_genesis_old commands after 5 minutes")
            @Story("Test Remote Command")
            @Description("MyHyundai - Verify user is restored access to remote_genesis_old commands after 5 minutes")
            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
                guiController.pinLockedPopup.cancelButton.tap();
                appController.appFunctions.pauseTestExecution(36, 10000);
                remoteCommandButton.tap();
                guiController.enterPinScreen.enterPin(incorrectPin);
                assertTrue(guiController.incorrectPinPopup.isPresent());
            }
        }
    }*/
}
