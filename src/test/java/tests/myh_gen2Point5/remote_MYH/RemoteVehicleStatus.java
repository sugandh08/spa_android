package tests.myh_gen2Point5.remote_MYH;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControls;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Remote Vehicle Status Test")
@Feature("Remote Vehicle Status")
@DisplayName("Remote Vehicle Status")
public class RemoteVehicleStatus extends TestController {

    private Profile profile;;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.1 Remote Vehicle  Status Refresh")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Vehicle  Status Refresh")
    void remoteVehicleStatusRefresh() {
        //tap the Vehicle Status button on Vehicle Dashboard
       guiController.gasHomeScreen.vehicleStatusButton.tap();
        appController.appFunctions.pauseTestExecution(2,5000);
        guiController.vehicleStatusScreenGen2Point5.isPresent();

        //get time of last vehicle Update
        String timeLastUpdate= guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();
        System.out.println("Last Updated Time:" +timeLastUpdate);
        //tap the Vehicle Status refresh icon
        guiController.vehicleStatusScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(4,10000);
        //verify getting vehicle status loader text
        assertTrue(guiController.vehicleStatusScreenGen2Point5.RefreshLoaderText.getTextValue().contains("vehicle's status"));
        //get the time after status update
        String timeAfterUpdate=guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();
        System.out.println("Updated time:"+ timeAfterUpdate);
        assertTrue(!timeLastUpdate.equals(timeAfterUpdate),"Vehicle Status Refresh failed");
           }

   /* @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.2 Remote Vehicle Status Fluid Level Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remote Vehicle Status Fluid Level Check")
    void remoteVehicleStatusFluidLevelCheck() {


        //tap the Vehicle Status button on Vehicle Dashboard
        guiController.gasHomeScreen.vehicleStatusButton.tap();

        guiController.vehicleStatusScreenGen2Point5.isPresent();

        guiController.vehicleStatusScreenGen2Point5.swipe.fromBottomEdge(2);

        //check the fluid levels status
       // assertTrue(guiController.vehicleStatusScreenGen2Point5.isOilStatusOk(),"Oil Status is not OK");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isBrakeStatusOk(),"Brake Status is not OK");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isWasherStatusOk(),"Washer Status is not OK");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isSmartKeyBatteryStatusOk(),"Smart Key Battery Status is not OK");

    }  need to check for vehicle having fluid property as dn8, will fix later*/


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.3 Remote Door Lock-Unlock Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remote Door Lock-Unlock Status Check")
    void remoteVehicleDoorStatusCheck() {
        //Send the Remote Door Lock Request
        guiController.remoteCarControls.clickOnRemoteLockButton();
       guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.requestSentPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESPONSE: " + resultText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        //tap the Vehicle Status button on Remote Dashboard
        guiController.gasHomeScreen.vehicleStatusButton.tap();
        appController.appFunctions.pauseTestExecution(2,5000);

        //Check that vehicle Status shows Door status as Locked
        assertTrue(guiController.vehicleStatusScreenGen2Point5.doorStatus.getTextValue().equals("Locked"));

        appController.appFunctions.pauseTestExecution(1,5000);

        //Press android back button
        appController.appFunctions.tapAndroidBackButton();

        //Send the Remote Door UnLock Request
        guiController.remoteCarControls.clickOnRemoteUnlockButton();
         guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
         guiController.requestSentPopup.okButton.tap();
        String resultText2 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText2.contains("Door Unlock") && resultText2.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        //tap the Vehicle Status button on Remote Dashboard
        guiController.gasHomeScreen.vehicleStatusButton.tap();
        appController.appFunctions.pauseTestExecution(2,5000);

        //Check that vehicle Status shows Door status as UnLocked
        assertTrue(guiController.vehicleStatusScreenGen2Point5.doorStatus.getTextValue().equals("Unlocked"));
        appController.appFunctions.pauseTestExecution(1,5000);
    }


     @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.4 Remote Climate  Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remote Climate  Status Check")
    void remoteClimateStatusCheck() {

      /*//Send the Remote Start Request
        remoteCarControls.startButton.tap();



        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);


        guiController.remoteStartSettingsScreen.setStartSettingsStates(true,true,true);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));


        guiController.remoteCommandResultPopup.okButton.tap();


        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        appController.appFunctions.pauseTestExecution(2,5000);

        String VehicleTemp=guiController.vehicleStatusScreenGen2Point5.climateStatus.getTextValue();

        String frontDefrostText=guiController.vehicleStatusScreenGen2Point5.frontDefrostStatus.getTextValue();
        //Check that vehicle Status shows temp
        assertTrue(VehicleTemp.contains("72"));
        assertTrue(guiController.vehicleStatusScreenGen2Point5.heatStatus.getTextValue().equals("ON"));
        assertTrue(frontDefrostText.contains("ON"));

        appController.appFunctions.pauseTestExecution(1,5000);

        //tap Android Back button
        appController.appFunctions.tapAndroidBackButton();

        //Send the Remote Stop Request
        remoteCarControls.stopButton.tap();

        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText1);
        assertTrue(resultText.contains("Control Stop") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();*/

        //tap the Vehicle Status button on Remote Dashboard
     //    guiController.gasHomeScreen.vehicleStatusButton.tap();
        //appController.appFunctions.pauseTestExecution(2,5000);


        /*String frontDefrostText2=guiController.vehicleStatusScreenGen2Point5.frontDefrostStatus.getTextValue();
        System.out.println(frontDefrostText2);
        System.out.println(guiController.vehicleStatusScreenGen2Point5.climateStatus.elementExists());
        System.out.println(guiController.vehicleStatusScreenGen2Point5.climateStatus.getTextValue());
        System.out.println(guiController.vehicleStatusScreenGen2Point5.heatStatus.getTextValue());

        //Check that vehicle Status shows temperature as OFF
        assertTrue(guiController.vehicleStatusScreenGen2Point5.climateStatus.getTextValue().equals("OFF"));
        assertTrue(guiController.vehicleStatusScreenGen2Point5.heatStatus.getTextValue().equals("OFF"));
        assertTrue(frontDefrostText2.contains("OFF"));

        appController.appFunctions.pauseTestExecution(1,5000);*/

    }

    /*
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.5 Remote Engine On-Off Status Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remote Engine On-Off  Status Check")
    void remoteEngineStatusCheck() {

        //Send the Remote Start Request
        remoteCarControls.startButton.tap();

        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);


        guiController.remoteStartSettingsScreen.setStartSettingsStates(true,true,true);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));


        guiController.remoteCommandResultPopup.okButton.tap();


        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();


        //Check that vehicle Status shows engine status ON

        assertTrue(guiController.vehicleStatusScreenGen2Point5.engineStatus.equals("ON"));

        appController.appFunctions.pauseTestExecution(1,5000);

        //tap Android Back button
        appController.appFunctions.tapAndroidBackButton();

        //Send the Remote Stop Request
        remoteCarControls.stopButton.tap();

        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText1);
        assertTrue(resultText.contains("Control Stop") && resultText.contains("successful"));

        guiController.remoteCommandResultPopup.okButton.tap();

        //tap the Vehicle Status button on Remote Dashboard
        remoteCommandButton.tap();

        String frontDefrostText2=guiController.vehicleStatusScreenGen2Point5.frontDefrostStatus.getTextValue();

        //Check that vehicle Status shows engine as OFF
        assertTrue(guiController.vehicleStatusScreenGen2Point5.engineStatus.equals("OFF"));


        appController.appFunctions.pauseTestExecution(1,5000);

    }*/
}
