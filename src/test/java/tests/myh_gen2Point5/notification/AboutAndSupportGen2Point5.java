package tests.myh_gen2Point5.notification;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AboutAndSupportGen2Point5 extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        appController.appFunctions.pauseTestExecution(5, 10000);
        guiController.gasHomeScreen.clickOnHamburgerMenuButton();
        guiController.menuScreen.aboutAndSupportButton.tap();
        guiController.aboutAndSupportScreen.isPresent();
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.1 Validating Blue Link section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Blue Link Section on About & Support Page")
    void blueLinkSection() {
        String blueLinkInfo= guiController.aboutAndSupportScreen.blueLinkInformation.getTextValue();
        String blueLinkHowToVideo= guiController.aboutAndSupportScreen.blueLinkHowToVideos.getTextValue();
    if (guiController.aboutAndSupportScreen.blueLinkText.elementExists())
    {
        assertTrue(guiController.aboutAndSupportScreen.blueLinkInformation.getTextValue().equals(blueLinkInfo));
        assertTrue(guiController.aboutAndSupportScreen.blueLinkHowToVideos.getTextValue().equals(blueLinkHowToVideo));
    }

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.2 Validating Bluetooth section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Bluetooth Section on About & Support Page")
    void blueLInkSection() {

        guiController.aboutAndSupportScreen.blueLink.tap();


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.2 Validating Bluetooth section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Bluetooth Section on About & Support Page")
    void bluetoothSection() {
        String multimediaText = guiController.aboutAndSupportScreen.bluetoothAndMultimediaSupport.getTextValue();
        if(guiController.aboutAndSupportScreen.bluetoothText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.bluetoothAndMultimediaSupport.getTextValue().equals(multimediaText));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.3 Validating GUIDES section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify GUIDES Section on About & Support Page")
    void guidesSection() {
        String gettingstartedGuide = guiController.aboutAndSupportScreen.guidesText.getTextValue();
        if(guiController.aboutAndSupportScreen.guidesText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.gettingStartedGuide.getTextValue().equals(gettingstartedGuide));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.4 Validating hyundai resources section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify hyundai resources Section on About & Support Page")
    void hyundaiResourceSection() {
        String gettingStarted = guiController.aboutAndSupportScreen.hyundaiResourceText.getTextValue();
        if(guiController.aboutAndSupportScreen.hyundaiResourceText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.gettingStarted.getTextValue().equals(gettingStarted));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.5 Validating LINKS section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify LINKS Section on About & Support Page")
    void LinksSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String myHyundaiComText = guiController.aboutAndSupportScreen.myHyundaiCom.getTextValue();
        System.out.println(myHyundaiComText);
    if(guiController.aboutAndSupportScreen.linksText.elementExists())
    {
        assertTrue(guiController.aboutAndSupportScreen.myHyundaiCom.getTextValue().equals(myHyundaiComText));
    }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.6 Validating Contact Us section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify Contact Us Section on About & Support Page")
    void contactUsSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String emailAppSupport = guiController.aboutAndSupportScreen.emailAppSupport.getTextValue();
        if(guiController.aboutAndSupportScreen.contactUsText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.emailAppSupport.getTextValue().equals(emailAppSupport));
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.7 Validating Version section on About & Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About And Support")
    @Description("MyHyundai - Verify version Section on About & Support Page")
    void versionSection() {
        guiController.aboutAndSupportScreen.swipe.up(4);
        String faqText = guiController.aboutAndSupportScreen.faq.getTextValue();
        if(guiController.aboutAndSupportScreen.versionText.elementExists())
        {
            assertTrue(guiController.aboutAndSupportScreen.faq.getTextValue().equals(faqText));
        }
    }

}
