package tests.myh_gen2Point5.connectedcare;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Gen2.5 MVR Report Test")
@Feature("Gen2.5 MVR Report")
@DisplayName("Gen2.5 MVR Report")
public class Gen2_5MvrReports extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2Point5;
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 MVR history page shows a MVR report")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Gen2 MVR Report")
    @Description("MyHyundai - Verify MVR history page shows a MVR report")
    void mvrHistoryPageShowsMvrReport() {
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
       /* guiController.selectExpandableItemSafelyFromHomeScreen(
                guiController.gasHomeScreen,
                guiController.gasHomeScreen.carCareButton,
                guiController.gasHomeScreen.carCareOptions.monthlyVehicleReport
        ); commenting for now*/
        if (guiController.monthlyVehicleReportScreen.isPresent(30)) {
            guiController.monthlyVehicleReportScreen.previousReportsButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1,4000);
        assertTrue(guiController.monthlyVehicleReportHistoryScreen.oldMvrFirstEntryContainerView.elementExists(),
                "This failure can be for a couple reasons. The most likely is that the test got caught on the" +
                        "Car Care expandable options list after selecting it on the home screen. Also, it will fail" +
                        "if there is no entries in the MVR history, which is possible depending on the vehicle. The" +
                        " least likely reason is that the app actually bugged out, and the test was an actual fail.");
    }
}
