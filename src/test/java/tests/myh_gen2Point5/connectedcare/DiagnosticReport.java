package tests.myh_gen2Point5.connectedcare;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;

// Profiles Used:
// 1. Gen1Standard
// 2. Gen2GasOrHevStandard

@Epic("Diagnostic Report Test")
@Feature("Diagnostic Report")
@DisplayName("Diagnostic Report")
public class DiagnosticReport extends TestController {
    // NOTE: Diagnostics Report tests may randomly fail. No way to fix or predict when it will do so.
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.3.1 Diagnostics report for Gen 2.5 shows that all systems are normal")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Diagnostic Report")
    @Description("MyHyundai - Verify Diagnostics report for Gen 2.5 shows that all systems are normal")
    void gen2DiagnosticsReportShowsAllSystemsNormal() {
        Profile profile = appController.myHyundaiProfiles.gen2Point5;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        guiController.carCareOptions.clickOnDiagnosticsButton();
        appController.appFunctions.pauseTestExecution(1,5000);
        assertEquals("All systems normal", guiController.diagnosticReportScreen.systemsStatus.getTextValue(),
                "Status either showed that there was a vehicle issue (and thus the status did not read 'All" +
                        " systems normal', or the test got caught in navigation to the diagnostics report. It is " +
                        "very likely that the test got caught at the Car Care Options expandable menu from the " +
                        "home screen. It can get caught at that location randomly.");
    }
}
