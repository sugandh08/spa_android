package tests.myh_gen2Point5.userProfile_Gen2Point5;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.baseviews.HomeScreen;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("My Driver Settings Test")
public class MyDriverSettings extends TestController {


    private void loginAndOpenUserProfile(Profile profile) {


        HomeScreen homeScreen = guiController.getHomeScreenByPowerType(
                profile.primaryAccount.vehicles.get(profile.primaryVin).powerType);


        guiController.loginAndGoToHomePageWithSelectVehicle(
                profile.primaryAccount, profile.primaryVin, 60);

        homeScreen.clickOnHamburgerMenuButton();


        guiController.menuScreen.viewProfileButton.tap();
        guiController.profileScreen_gen2Point5.isPresent(10);
        System.out.println(guiController.profileScreen_gen2Point5.subTitleTextView.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.1 Edit My Driver Date and Time Settings")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test My Driver Settings")
    @Description("MyHyundai - Verify editing My Driver Date and Time Settings")
    void editMyDriverDateAndTimeSettings() {

        boolean gpsValue = true;
        boolean dayLightSavingValue = false;
        String twentyFourHoursOnOffValue = "on";


        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Date / Time Settings");


        guiController.myDriverSettingsScreen_gen2Point5.setDateAndTimeSettings(gpsValue, dayLightSavingValue, twentyFourHoursOnOffValue);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.myDriverSettingsScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Date / Time Settings");

        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("GPS Time")==gpsValue,
                "Date and Time settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Daylight Saving Time")==dayLightSavingValue,
                "Date and Time settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.twentyFourHoursOnButton.getCheckedValue()==true,
                "Date and Time settings not saved");


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.2 Edit My Driver Sound Settings")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test My Driver Settings")
    @Description("MyHyundai - Verify editing My Driver Sound Settings")
    void editMyDriverSoundSettings() {

        boolean navigationGuidanceValue = true;
        boolean proximityWarningValue = false;
        boolean navigationDuringCallsValue = false;
        boolean startUpVolumeLimitValue = true;
        boolean useMapScreenVolumeButton=false;
        boolean useMapScreenVolumeAndKnobButton=true;

        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Sound Settings");


        guiController.myDriverSettingsScreen_gen2Point5.setSoundSettings(navigationGuidanceValue, proximityWarningValue, navigationDuringCallsValue,startUpVolumeLimitValue,useMapScreenVolumeButton,useMapScreenVolumeAndKnobButton);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.myDriverSettingsScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Sound Settings");

        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Navigation Guidance")==navigationGuidanceValue,
                "Sound settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Proximity Warning")==proximityWarningValue,
                "Sound settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Navigation During Calls")==navigationDuringCallsValue,
                "Sound settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Start - up Volume Limit")==startUpVolumeLimitValue,
                "Sound settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.useMapScreenVolButton.getCheckedValue()==useMapScreenVolumeButton,
                "Sound settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.useMapScreenVolButtonOrKnob.getCheckedValue()==useMapScreenVolumeAndKnobButton,
                "Sound settings not saved");


    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.3 Edit My Driver Vehicle Settings")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test My Driver Settings")
    @Description("MyHyundai - Verify editing My Driver Vehicle Settings")
    void editMyDriverVehicleSettings() {

        boolean gearPositionPopUpValue = true;
        boolean wiperLightsDisplayValue = false;
        boolean icyRoadWarningValue = false;
        boolean welcomeSoundValue = true;
        boolean seatPositionChangeAlertValue=true;
        String seatEasyAccessValue="Extended";
        boolean wirelessChargingSystemValue=false;



        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Vehicle Settings");


        guiController.myDriverSettingsScreen_gen2Point5.setVehicleSettings(gearPositionPopUpValue, wiperLightsDisplayValue, icyRoadWarningValue,welcomeSoundValue,seatPositionChangeAlertValue,seatEasyAccessValue,wirelessChargingSystemValue);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.myDriverSettingsScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Vehicle Settings");

        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Gear Position Pop - Up")==gearPositionPopUpValue,
                "Vehicle settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Wiper / Lights Display")==wiperLightsDisplayValue,
                "Vehicle settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Icy Road Warning")==icyRoadWarningValue,
                "Vehicle settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Welcome Sound")==welcomeSoundValue,
                "Vehicle settings not saved");
        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(2);

        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Seat Position Change Alert")==seatPositionChangeAlertValue,
                "Vehicle settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.getCheckedValueOfToggle("Wireless Charging System")==wirelessChargingSystemValue,
                "Vehicle settings not saved");



    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.4 Edit My Driver Unit Settings")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test My Driver Settings")
    @Description("MyHyundai - Verify editing My Driver Unit Settings")
    void editMyDriverUnitSettings() {

        String distanceAndSpeedValue = "km";
        String temperatureValue="F";



        Profile profile = appController.myHyundaiProfiles.gen2Point5;
        loginAndOpenUserProfile(profile);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Unit Settings");


        guiController.myDriverSettingsScreen_gen2Point5.setUnitSettings(distanceAndSpeedValue,temperatureValue);

        guiController.profileScreen_gen2Point5.swipe.fromBottomEdge(1);

        guiController.myDriverSettingsScreen_gen2Point5.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

        appController.appFunctions.tapAndroidBackButton();

        guiController.profileScreen_gen2Point5.myDriverSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 8000);
        guiController.myDriverSettingsScreen_gen2Point5.isPresent();

        guiController.myDriverSettingsScreen_gen2Point5.tapAccordianExpandButton("Unit Settings");

        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.distanceSpeedKm.getCheckedValue()==true,
                "Unit settings not saved");
        assertTrue(guiController.myDriverSettingsScreen_gen2Point5.tempertureF.getCheckedValue()==true,
                "Unit settings not saved");




    }



}
