package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Horn And Lights Test")
@Feature("Remote Horn And Lights")
@DisplayName("Remote Horn And Lights")
public class RemoteHornAndLightsGen2Point5 extends TestController {
    private Profile profile;
     private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
               remoteCommandButton = guiController.gasHomeScreenGen2Point5.remoteCommands.hornAndLightsButton;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.1 Remote horn and lights success shows successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote horn and lights success shows successful message")
    void successfulRemoteHornAndLightsShowsPopupMessage() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE HORN AND LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Horn & Lights") && resultText.contains("sent to vehicle"));
    }

}
