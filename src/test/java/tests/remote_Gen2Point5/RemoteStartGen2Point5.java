package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;


import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard


@Epic("Remote Start Test")
@Feature("Remote Start")
@DisplayName("Remote Start")
public class RemoteStartGen2Point5 extends TestController {
    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;
    private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControlsGen2Point5=guiController.gasHomeScreenGen2Point5.remoteCommands;
        remoteCommandButton=guiController.gasHomeScreenGen2Point5.remoteCommands.startButton;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.1 Remote start success of Gen2 with ClimateON/DefrostON/HeatedServicesON/HeatedSeatON")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success of Gen2 with ClimateON/DefrostON/HeatedServicesON/HeatedSeatON")
    void remoteStartSuccessOfGen2WithClimateOffDefrostOffHeatedServicesOffHeatSeatOn() {
        remoteCarControlsGen2Point5.start_stopButton.tap();
        remoteCommandButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true,true,true,true);
        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.heatedSeat_1.tap();
        guiController.remoteStartSettingsScreenGen2Point5.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.2 Remote start success of Gen2 with ClimateOFF/DefrostOFF/HeatedServicesOFF/HeatSeatOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote start success of Gen2 with ClimateOFF/DefrostOFF/HeatedServicesOFF/HeatSeatOFF")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOffHeatedServicesOffHeatSeatOff() {
        remoteCarControlsGen2Point5.start_stopButton.tap();
        remoteCommandButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(false,false,false,false);
        guiController.remoteStartSettingsScreenGen2Point5.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.1.3 Remote start success of Gen2 with ClimateON/DefrostON/HeatedServicesON/HeatSeatAllOFF")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote_genesis_old start success of Gen2 with ClimateON/DefrostON/HeatedServicesON/HeatSeatAllOFF")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOnHeatedServicesOnHeatSeatAllOff() {
        remoteCarControlsGen2Point5.start_stopButton.tap();
        remoteCommandButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.engineDurationExpandButton.tap();
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH,9);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreen.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true,true,true,true);
        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.seatAllOffButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }






}
