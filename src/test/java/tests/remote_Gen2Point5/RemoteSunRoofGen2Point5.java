package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard


@Epic("Remote SunRoof Test")
@Feature("Remote SunRoof")
@DisplayName("Remote SunRoof")
public class RemoteSunRoofGen2Point5 extends TestController {
    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;
    private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControlsGen2Point5 = guiController.gasHomeScreenGen2Point5.remoteCommands;
        remoteCommandButton=remoteCarControlsGen2Point5.sunRoofButton;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.9.1 Remote SunRoof open success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify SunRoof open success with Gen2")
    void remoteSunRoofOpenSuccessWithGen2() {
         remoteCommandButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println(resultText);
        assertTrue(resultText.contains("Feature not provided"));
    }

   /* @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.9.2 Remote SunRoof close success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify SunRoof close success with Gen2")
    void remoteSunRoofCloseSuccessWithGen2() {
        remoteCarControlsGen2Point5.trunkButton.tap();
        remoteTrunkCloseButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESULT: " + resultText);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));
    }*/


}
