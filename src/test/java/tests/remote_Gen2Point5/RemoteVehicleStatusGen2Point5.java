package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Remote Vehicle Status Test")
@Feature("Remote Vehicle Status")
@DisplayName("Remote Vehicle Status")
public class RemoteVehicleStatusGen2Point5 extends TestController {

    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;
    private Element remoteCommandButton;
    private RemoteTestsHelperGen2Point5 remoteTestsHelperGen2Point5;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.withSecondaryDriver;
        remoteCarControlsGen2Point5=guiController.gasHomeScreenGen2Point5.remoteCommands;
        remoteCommandButton = guiController.gasHomeScreenGen2Point5.vehicleStatusButton;
        remoteTestsHelperGen2Point5=new RemoteTestsHelperGen2Point5(appController,guiController,profile);

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.1 Remote Vehicle  Status Refresh")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Vehicle  Status Refresh")
    void remoteVehicleStatusRefresh() {



        //tap the Vehicle Status button on Vehicle Dashboard
        remoteCommandButton.tap();

        appController.appFunctions.pauseTestExecution(2,5000);

        guiController.vehicleStatusScreenGen2Point5.isPresent();

        //get time of last vehicle Update
        String timeLastUpdate= guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();

        System.out.println("Last Updated Time:" +timeLastUpdate);

        //tap the Vehicle Status refresh icon
        guiController.vehicleStatusScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(4,10000);

        //get the time after status update
        String timeAfterUpdate=guiController.vehicleStatusScreenGen2Point5.getRefreshUpdateTime();
        System.out.println("Updated time:"+ timeAfterUpdate);

        assertTrue(!timeLastUpdate.equals(timeAfterUpdate),"Vehicle Status Refresh failed");


           }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.11.2 Remote Vehicle Status Fluid Level Check")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Remote Vehicle Status Fluid Level Check")
    void remoteVehicleStatusFluidLevelCheck() {


        //tap the Vehicle Status button on Vehicle Dashboard
        remoteCommandButton.tap();

        guiController.vehicleStatusScreenGen2Point5.isPresent();

        guiController.vehicleStatusScreenGen2Point5.swipe.fromBottomEdge(2);

        //check the fluid levels status
       // assertTrue(guiController.vehicleStatusScreenGen2Point5.isOilStatusOk(),"Oil Status is not OK");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isBrakeStatusOk(),"Brake Status is not OK");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isWasherStatusOk(),"Washer Status is not OK");
        assertTrue(guiController.vehicleStatusScreenGen2Point5.isSmartKeyBatteryStatusOk(),"Smart Key Battery Status is not OK");

    }
}
