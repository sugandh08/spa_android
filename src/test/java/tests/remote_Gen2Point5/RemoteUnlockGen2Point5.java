package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote Unlock Test")
@Feature("Remote Unlock")
@DisplayName("Remote Unlock")
public class RemoteUnlockGen2Point5 extends TestController {
    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;
       private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControlsGen2Point5 = guiController.gasHomeScreenGen2Point5.remoteCommands;
        remoteCommandButton = remoteCarControlsGen2Point5.unlockButton;


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.4.1 Remote unlock request shows request successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote unlock request shows request successful message")
    void remoteUnlockRequestShowsRequestSuccessfulMessage() {
        remoteCarControlsGen2Point5.doorLocksButton.tap();
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESULT: " + resultText);
        assertTrue(resultText.contains("Door Unlock") && resultText.contains("successful"));
    }


}
