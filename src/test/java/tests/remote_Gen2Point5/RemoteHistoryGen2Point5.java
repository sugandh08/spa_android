package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard

@Epic("Remote History Test")
@Feature("Remote History")
@DisplayName("Remote History")
public class RemoteHistoryGen2Point5 extends TestController {
    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControlsGen2Point5 = guiController.gasHomeScreenGen2Point5.remoteCommands;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        guiController.gasHomeScreenGen2Point5.isPresent(30);
    }



    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.1 A remote command is displayed")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a remote command is displayed.")
    void aRemoteCommandIsDisplayed() {
        // send a remote_genesis_old command (we choose remote_genesis_old stop because it will fail)
        remoteCarControlsGen2Point5.start_stopButton.tap();
        remoteCarControlsGen2Point5.stopButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // check that the remote_genesis_old command is displayed in history
        guiController.gasHomeScreenGen2Point5.remoteHistoryButton.tap();
        guiController.remoteHistoryScreen.isPresent(10);
 //       assertEquals("Stop", guiController.remoteHistoryScreen.getMostRecentCommandName());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.2 A successful remote command displays 'SUCCESS'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a successful remote command displays 'SUCCESS'")
    void aSuccessfulRemoteCommandDisplaySuccess() {
        // send a remote_genesis_old command and wait for the success message
        guiController.gasHomeScreenGen2Point5.remoteCommands.lightsButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Lights Only") && resultText.contains("sent to vehicle"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // check that the most recent remote_genesis_old command in remote_genesis_old history shows 'success'
        guiController.gasHomeScreenGen2Point5.remoteHistoryButton.tap();
        guiController.remoteHistoryScreen.isPresent(10);
        // Make sure we have the correct command
    //    assertEquals("Lights", guiController.remoteHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
        assertEquals("Success", guiController.remoteHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.3 A failed remote command displays '?'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a failed remote command displays '?'")
    void aFailedRemoteCommandDisplaysQuestionMark() {
        // send a remote_genesis_old command and wait for the failure message
        remoteCarControlsGen2Point5.start_stopButton.tap();
        remoteCarControlsGen2Point5.stopButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESULT: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
        guiController.remoteCommandResultPopup.okButton.tap();

        // check that the most recent remote_genesis_old command in remote_genesis_old history shows failure icon
        guiController.gasHomeScreenGen2Point5.remoteHistoryButton.tap();
        guiController.remoteHistoryScreen.isPresent(10);
        // Make sure we have the correct command
     //   assertEquals("Stop", guiController.remoteHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
        assertEquals("Fail", guiController.remoteHistoryScreen.getMostRecentCommandStatus());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.7.4 A pending remote command displays 'PENDING'")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify a pending remote command displays 'PENDING'")
    void aPendingRemoteCommandDisplaysPending() {
        // send a remote_genesis_old command, then check remote_genesis_old history after a couple seconds to give it time to process
        guiController.gasHomeScreenGen2Point5.remoteCommands.lightsButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        //appController.appFunctions.pauseTestExecution(1, 5000);

        // check that the most recent remote_genesis_old command in remote history shows "Pending"
        guiController.gasHomeScreenGen2Point5.remoteHistoryButton.tap();
        guiController.remoteHistoryScreen.isPresent(10);
        // Make sure we have the correct command
   //     assertEquals("Lights", guiController.remoteHistoryScreen.getMostRecentCommandName());
        // if we do, make sure it reflects that it was successful
        assertEquals("Pending", guiController.remoteHistoryScreen.getMostRecentCommandStatus());

        // wait for the remote command to complete
        guiController.remoteCommandResultPopup.isPresent(180);
    }
}
