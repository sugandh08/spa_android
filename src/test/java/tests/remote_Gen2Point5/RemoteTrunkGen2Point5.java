package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard


@Epic("Remote Trunk Test")
@Feature("Remote Trunk")
@DisplayName("Remote Trunk")
public class RemoteTrunkGen2Point5 extends TestController {
    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;
    private Element remoteTrunkOpenButton;
    private Element remoteTrunkCloseButton;

    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControlsGen2Point5 = guiController.gasHomeScreenGen2Point5.remoteCommands;
        remoteTrunkOpenButton = remoteCarControlsGen2Point5.openTrunkButton;
        remoteTrunkCloseButton=remoteCarControlsGen2Point5.closeTrunkButton;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.8.1 Remote trunk open success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify trunk open success with Gen2")
    void remoteTrunkOpenSuccessWithGen2() {
        remoteCarControlsGen2Point5.trunkButton.tap();
        remoteTrunkOpenButton.tap();

        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println(resultText);
        assertTrue(resultText.contains("Your vehicle does not support this feature."));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.8.2 Remote trunk close success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify trunk close success with Gen2")
    void remoteTrunkCloseSuccessWithGen2() {
        remoteCarControlsGen2Point5.trunkButton.tap();
        remoteTrunkCloseButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println(resultText);
        assertTrue(resultText.contains("Your vehicle does not support this feature."));
    }


}
