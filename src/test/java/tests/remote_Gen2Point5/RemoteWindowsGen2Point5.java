package tests.remote_Gen2Point5;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.myhyundai.TestController;
import views.myhyundai.fragments.RemoteCarControlsGen2Point5;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasOrHevStandard


@Epic("Remote Windows Test")
@Feature("Remote Windows")
@DisplayName("Remote Windows")
public class RemoteWindowsGen2Point5 extends TestController {
    private Profile profile;
    private RemoteCarControlsGen2Point5 remoteCarControlsGen2Point5;


    @BeforeEach
    void setupTests() {
        profile = appController.myHyundaiProfiles.gen2GasOrHevStandard;
        remoteCarControlsGen2Point5 = guiController.gasHomeScreenGen2Point5.remoteCommands;


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.10.1 Remote Windows open success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Windows open success with Gen2")
    void remoteWindowsOpenSuccessWithGen2() {
        remoteCarControlsGen2Point5.openWindowsButton.tap();


        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println(resultText);
        assertTrue(resultText.contains("Feature not provided"));
    }

  /*  @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.10.2 Remote Windows close success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify Windows close success with Gen2")
    void remoteWindowsCloseSuccessWithGen2() {
        remoteCarControlsGen2Point5.openWindowsButton.tap();

        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println(resultText);
        assertTrue(resultText.contains("Feature not provided"));
    }*/


}
