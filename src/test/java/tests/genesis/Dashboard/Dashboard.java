package tests.genesis.Dashboard;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.*;

public class Dashboard extends TestController {

    Profile profile;
    private Element remoteCommandButton;
    private Object String;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.refreshButton;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.1 User can see Unlock button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Unlock button on Dashboard")
    void UnlockbuttonOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.unlockButton.elementExists(),"Verify Unlock button is present in dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.2 User can see lock button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see lock button on Dashboard")
    void lockbuttonOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.lockButton.elementExists(),"Verify lock button is present in dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.3 User can see  Start button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Quick Start button on Dashboard")
    void StartbuttonOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.startbutton.elementExists(),"Verify Start button is prresent in dashboard");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 User can see add event button on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see add event button on Dashboard")
    void addeventOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.addevent.elementExists(),"Verify add event present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.5 User can see POI on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see POI on Dashboard")
    void POIOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.elementExists(),"Verify POI present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.6 User can see Weather icon on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Weather icon on Dashboard")
    void WeathericonOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.weathericon.elementExists(),"Verify Weather icon present in the dashboard screen");
    }

//    @RepeatedTest(TEST_REPETITIONS)
//    @DisplayName("2.1.7 User can see Location on Dashboard")
//    @Severity(SeverityLevel.BLOCKER)
//    @Story("Test Dashboard Command")
//    @Description("Genesis - Verify user can see Location on Dashboard")
//    void LocationOnDashboard()
//    {
//        assertTrue(guiController.remoteFeaturesScreenGen2Point5.location.elementExists(),"Verify Location present in the dashboard screen");
//    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.8 User can see Location on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Location on Dashboard")
    void WeatherInfoandTempOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.weatherinfo.elementExists(),"Verify Weather info present in the dashboard screen");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.weatherTemp.elementExists(),"Verify Temperature present in the dashboard screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.9 Verify user can see Remote Features screen when click on remote Start button in the home screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see Remote Features screen when click on remote Start button in the home screen")
    void Remotefeaturescreen() {
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.remotefeaturescreen.elementExists(), "Verified Remote preset screen is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.10 User can see a list of closest POI in the search field when enter" +
                    "Verify user can search for a POI ")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see a list of closest POI in the search field when enter")
    void ClosestPOIinSearchField() {
        String placename = "Los Angeles";
        String zipcode = "90002";
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.tap();
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.enterText(zipcode);
        String POI = guiController.getTextUsingXpath("//android.widget.TextView[@resource-id='com.stationdm.genesis:id/tv_placename'][1]");
        assertEquals(placename, POI, "Verified placename equals to zipcode searched");
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.11 Verify the Conatct us link on Message center to redirect to Contact us page on CWP")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify the Conatct us link on Message center to redirect to Contact us page on CWP")
    void RedirecttoContactusPageFromDashboard()
    {
        //Click on MessageCentre icon in Dashboard
        guiController.remoteFeaturesScreenGen2Point5.messageCentre.tap();
        //Click on Contactus Present in MessageCentre
        guiController.remoteFeaturesScreenGen2Point5.contactus.tap();
        appController.appFunctions.pauseTestExecution(2,2000);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.ContactusPage.elementExists(),"Verified Contact us Page on CWP ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.12 Verify user can see a prompt message with Cancel and Add Event button when click on it")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - User can see a prompt message with Cancel and Add Event button when click on it")
    void AddeventPromptMessagewithCancelandAddeventButton()
    {
        guiController.remoteFeaturesScreenGen2Point5.addevent.tap();
        guiController.remoteFeaturesScreenGen2Point5.forAddEventBtn.tap();
        appController.appFunctions.pauseTestExecution(2,2000);
        String textValue = guiController.remoteFeaturesScreenGen2Point5.addeventPromptmsg.getTextValue();
        System.out.println(textValue);
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.addeventPromptmsg.elementExists(),"Verified Addevent message is displayed");
    }
    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.13 Verify user gets prompt message on search of POI in the home screen when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on search of POI in the home screen when deny permissions")
    void PromptmessageonPOIsearchDashboard() {
        String placename = "Los Angeles";
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.tap();
        guiController.remoteFeaturesScreenGen2Point5.HomescreenPOI.enterText(placename);
        guiController.remoteFeaturesScreenGen2Point5.POIsearchlisttitle.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.14 Verify user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of POI Search in the Maps fly options when deny permissions")
    void PromptmessageonPOIsearchMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.TapPOISearchinmaps();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.15 Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Car Finder in the Maps fly options when deny permissions")
    void PromptmessageonCarFinderMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.CarFinder.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.16 Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Favorites in the Maps fly options when deny permissions")
    void PromptmessageonFavoritesMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.TapFavoritesinmaps();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.17 Verify user gets prompt message on tap of Dealer Locator in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Dealer Locator in the Maps fly options when deny permissions")
    void PromptmessageonDealerLocatorMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.DealerLocator.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.18 Verify user gets prompt message on tap of Charge station in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Charge stations in the Maps fly options when deny permissions")
    void PromptmessageonChargestationMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.ChargeFuelstations.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.19 Verify user gets prompt message on tap of Fuel stations in the Maps fly options when deny permissions")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets prompt message on tap of Fuel stations in the Maps fly options when deny permissions")
    void PromptmessageonFuelstationMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.ChargeFuelstations.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.20 Verify user gets location permission prompt message on tap of GPS icon in the map")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test POI Search")
    @Description("Genesis - Verify user gets location permission prompt message on tap of GPS icon in the map")
    void PromptmessageongpsiconunderMaps() {
        guiController.remoteFeaturesScreenGen2Point5.Maps.tap();
        guiController.termsAndConditionsPopup.denybutton.tap();
        guiController.termsAndConditionsPopup.TapCancel();
        guiController.remoteFeaturesScreenGen2Point5.Gpsicon.tap();
        System.out.println(guiController.remoteFeaturesScreenGen2Point5.Gpsicon.elementExists());
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.elementExists(), "Verified Allow or Deny popup occurs ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.AllowDenyPopup.getTextValue().contains("Allow Genesis to access this device's location?"));
        guiController.termsAndConditionsPopup.denybutton.tap();
    }
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.25 Tap on Quick Unlock button with success popup")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Tap on Quick Unlock  button")
    void quickunlockdashboard() {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.unlockButton.elementExists(), "Verify Unlock button is present in dashboard");
        guiController.remoteFeaturesScreenGen2Point5.unlockButton.tap();
        assertTrue(guiController.enterPinScreen.enterPINText.elementExists(),"Verified PIN screen is displayed");
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText.contains("Remote Door Unlock") && resultText.contains("successful"));

    }

    
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.26 Verify the pop up when Previous command in progress.")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify the pop up when Previous command in progress.")
    void RemoteActioninprogress()
    {
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1,2000);
        guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Remoteactioninprogress.elementExists(),"Vrified Remote action in progress popup is displayed");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.Remoteactioninprogresscontent.elementExists(),"Verified A prior request is still processing.Please wait and try again later message displayed  ");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.DismissButton.elementExists(),"Verified Dismiss button is displayed in popup");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.27 Verify the pop up appeared in vehicle status after tapping on warning icon next to DOORS.")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify the pop up appeared in vehicle status after tapping on warning icon next to DOORS.")
    void doorwarningicon(){
        guiController.homeScreen.remoteButton.tap();
        guiController.homeScreen.vehiclestatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
        appController.appFunctions.pauseTestExecution(2,3000);
        //doorstatus & Locked include's the warning icon
        guiController.remoteFeaturesScreenGen2Point5.doorsStatus.tap();
        guiController.remoteFeaturesScreenGen2Point5.Locked.tap();
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorwarning.elementExists(),"Verified warning popup is displayed");
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.doorwarningtxt.getTextValue().contains("If a door was unlocked or opened before the vehicle was turned off"));
    }
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.28 Verify user can add event")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - User can add Event ")
    void verifyUserCanAddEvent()
    {    String eventName = "My Event";
        guiController.remoteFeaturesScreenGen2Point5.addevent.tap();
        guiController.remoteFeaturesScreenGen2Point5.forAddEventBtn.tap();
        guiController.remoteFeaturesScreenGen2Point5.AddeventButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.addEventCurrentDateBtn.tap();
        guiController.remoteFeaturesScreenGen2Point5.addEventCurrentDateBtn.tap();
        guiController.remoteFeaturesScreenGen2Point5.addEventTitel.enterText(eventName);
        guiController.remoteFeaturesScreenGen2Point5.eventSaveBtn.tap();
        appController.appFunctions.pauseTestExecution(2,2000);
        String textValue = guiController.remoteFeaturesScreenGen2Point5.addEventCurrentDateBtn.getTextValue();
        System.out.println(textValue);
       assertTrue(guiController.remoteFeaturesScreenGen2Point5.eventDateAndTime.elementExists(),"Verified Event is Saved");
    }

    @BeforeEach
    void setUpTests() {
        profile = appController.genesisProfiles.gen2Point5;
       // remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.refreshButton;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
    }
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.1.4 User can see valet Mode on Dashboard")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Dashboard Command")
    @Description("Genesis - Verify user can see  valet Mode on Dashboard")
    void valetMpdeOnDashboard()
    {
        assertTrue(guiController.remoteFeaturesScreenGen2Point5.addevent.elementExists(),"Verify add event present in the dashboard screen");
    }



}