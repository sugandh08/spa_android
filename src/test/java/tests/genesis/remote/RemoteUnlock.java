package tests.genesis.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;


import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Unlock Test")
@Feature("Remote Unlock")
@DisplayName("Remote Unlock")
public class RemoteUnlock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);

        remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.unlockButton;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
      //  guiController.remoteFeaturesScreenGen2Point5.doorLocksButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.4.1 Remote unlock request shows request successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("MyHyundai - Verify remote unlock request shows request successful message")
    void remoteUnlockRequestShowsRequestSuccessfulMessage() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE UNLOCK RESPONSE: " + resultText);
        assertTrue(resultText.contains("Door Unlock") && resultText.contains("successful"));
    }


}
