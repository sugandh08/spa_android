package tests.genesis.remote;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;


import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


public class RemoteStop extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2Point5;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteStartSettingsScreenGen2Point5.stopButton;
        guiController.homeScreen.remoteButton.tap();
       // guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();




    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.1 Remote stop success")
    void remoteStopSuccess() {
        appController.appFunctions.pauseTestExecution(1, 2000);
       // guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.startButton.tap();

        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 6);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);

        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreenGen2Point5.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.ventSeat_1.tap();

        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESPONSE: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
        guiController.remoteCommandResultPopup.okButton.tap();

        appController.appFunctions.tapAndroidBackButton();
        guiController.remoteFeaturesScreenGen2Point5.start_stopButton.tap();
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText1);
        assertTrue(resultText1.contains("Control Stop") && resultText1.contains("successful"));
    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.2.2 Remote stop failure")
    void remoteStopFailure() {
        remoteCommandButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE STOP RESPONSE: " + resultText);
        assertTrue(resultText.contains("Remote Stop for your vehicle cannot be processed"));
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.6.2 Remote stop with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("Genesis - Verify  remote stop with incorrect PIN shows incorrect PIN notification")
        void remoteStopWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE STOP RESPONSE: " + resultText1);
            appController.appFunctions.pauseTestExecution(1, 3000);
            // assertTrue(guiController.incorrectPinPopup.isPresent());
            assertTrue(resultText1.contains("Remote Stop for your vehicle cannot be processed"));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify the stages displaying the progress for Remote Stop")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote start success via preset")
        @Description("Genesis - Verify the stages displaying the progress for Remote Stop")
        void verifyStagesForRemoteStop() {

            guiController.remoteFeaturesScreenGen2Point5.stop.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE STOP RESPONSE: " + resultText1);


            //assertTrue(guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue().contains("Success"),"Showing Success");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify the stages displaying the progress for Remote Lock")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote start success via preset")
        @Description("Genesis - Verify the stages displaying the progress for Remote Lock")
        void verifyStagesForRemoteLock() {

            guiController.remoteFeaturesScreenGen2Point5.lock.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE LOCK RESPONSE: " + resultText1);


        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify the stages displaying the progress for Remote UnLock")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote start success via preset")
        @Description("Genesis - Verify the stages displaying the progress for Remote UnLock")
        void verifyStagesForRemoteUnLock() {

            guiController.remoteFeaturesScreenGen2Point5.unlock.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
            String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE LOCK RESPONSE: " + resultText1);


        }
    }

    //BELOW CODE IS COMMENTED AND ADDED IN THE REMOTE UNLOCK TEST CLASS

//    @Nested
//    class PinResetsAfterTest {
//        @AfterEach
//        void resetPin() {
//            remoteTestsHelper.resetPin(incorrectPin);
//        }
//
//        @RepeatedTest(TEST_REPETITIONS)
//        @DisplayName("7.2.3 Remote stop with incorrect PIN shows incorrect PIN notification")
//        void remoteStopWithIncorrectPinShowsIncorrectPinNotification() {
//            remoteCommandButton.tap();
//            guiController.enterPinScreen.enterPin(incorrectPin);
//
//            // Check if the PIN locked popup appears
//            if(guiController.pinLockedPopup.isPresent()){
//                // Reset PIN
//                resetPin();
//                // Repeating the above steps now that we know the pin was reset
//                guiController.homeScreen.remoteButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//            }
//
//            assertTrue(guiController.incorrectPinPopup.isPresent());
//            if(guiController.incorrectPinPopup.isPresent()){
//                guiController.incorrectPinPopup.okButton.tap();
//            }
//        }
//
//        @Nested
//        @DisplayName("7.2.4 Remote stop with incorrect PIN three times")
//        class RemoteStopWithIncorrectPinEnteredThreeTimes {
//            @BeforeEach
//            void setPinStateToLocked() {
//                remoteCommandButton.tap();
//                remoteTestsHelper.setPinStateToLocked(incorrectPin);
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.2.4.1 Error popup is displayed saying how to reset PIN")
//            void errorPopupIsDisplayedSayingHowToResetPin(){
//                assertTrue(guiController.pinLockedPopup.isPresent());
//            }
//
//            @RepeatedTest(TEST_REPETITIONS)
//            @DisplayName("7.2.4.2 User is restored access to remote_genesis_old commands after 5 minutes")
//            void userRemoteCommandAccessRestoredAfterFiveMinutes() {
//                guiController.pinLockedPopup.cancelButton.tap();
//                appController.appFunctions.pauseTestExecution(36, 10000);
//
//                // Can't tap stop button, so need to go to another screen
//                // before tapping on stop button
//                guiController.remoteFeaturesScreen.unlockButton.tap();
//                guiController.enterPinScreen.cancelButton.tap();
//                remoteCommandButton.tap();
//                guiController.enterPinScreen.enterPin(incorrectPin);
//
//                assertTrue(guiController.incorrectPinPopup.isPresent());
//
//                if (guiController.incorrectPinPopup.isPresent()) {
//                    guiController.incorrectPinPopup.okButton.tap();
//                }
//            }
//        }
//    }
}
