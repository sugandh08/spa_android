package tests.genesis.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Lock Test")
@Feature("Remote Lock")
@DisplayName("Remote Lock")
public class RemoteLock extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.lockButton;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();

       // guiController.remoteFeaturesScreenGen2Point5.doorLocksButton.tap();

        //guiController.remoteFeaturesScreenGen2Point5.doorLocksButton.tap();

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.3.1 Remote door lock success with Gen2")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify remote door lock success with Gen2")
    void remoteDoorLockSuccessWithGen2() {
        remoteCommandButton.tap();
       // guiController.remoteStartSettingsScreenGen2Point5.lockButton.tap();
       guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE LOCK RESPONSE: " + resultText);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(resultText.contains("Door Lock") && resultText.contains("successful"));
    }
    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.3.2 Remote Lock with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("Genesis - Verify  Remote Lock with incorrect PIN shows incorrect PIN notification")
        void remoteLocksWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }
}
