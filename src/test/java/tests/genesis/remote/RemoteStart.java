package tests.genesis.remote;

import config.Profile;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;


import static org.junit.jupiter.api.Assertions.assertTrue;


public class RemoteStart extends TestController {

    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;


    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        //incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
        guiController.homeScreen.remoteButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.1 Remote start success of Gen2 with Climate ON/Defrost ON/HeatedServicesON/HeatedSeatON via remote start without presets")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOnHeatedServicesOnHeatedSeatsOn() {
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);

        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreenGen2Point5.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.heatedSeat_1.tap();

        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);

        String startresultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println(startresultText);
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(startresultText.contains(" Remote Start with Climate Control") && startresultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        // appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.2 Remote start success of Gen2 with Climate ON/Defrost ON/HeatedServicesON/VentSeatON via remote start without presets")
    void remoteStartSuccessOfGen2WithClimateOnDefrostOnHeatedServicesOnVentSeatsOn() {
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);

        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);

        // grab the integer value of the engine duration text so we know how long to wait at the end of the tests
        int engineRunTime = Integer.parseInt(guiController.remoteStartSettingsScreenGen2Point5.engineDurationTimeTextView.getTextValue().replace(" min", ""));
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);

        guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
        guiController.remoteStartSettingsScreenGen2Point5.ventSeat_1.tap();

        guiController.remoteStartSettingsScreen.submitButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);

        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        //System.out.println("REMOTE START RESPONSE: " + resultText);
//        assertTrue(resultText.contains("We received an alert that your 2021 GENESIS") && resultText.contains("KMUHB5SB1LU000494"));
//        guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
        String startresultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        appController.appFunctions.pauseTestExecution(2, 3000);
        assertTrue(startresultText.contains(" Remote Start with Climate Control") && startresultText.contains("successful"));

        // wait for the engine to turn off. Wait 10 seconds for 6 times per minute... simple math
        // appController.appFunctions.pauseTestExecution(engineRunTime * 6, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.3 All four presets and 'start vehicle without presets' options are showing successfully")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Presets Are Showing")
    @Description("Genesis - Verify all four presets and 'start vehicle without presets' options are showing successfully")
    void allRemotePresetsOptionsAreShowingSuccessfully() {
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.isPresent();
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
        assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.4 Save Remote Preset")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Presets are getting saved")
    @Description("Genesis - Verify User is able to save remote preset successfully")
    void saveRemotePreset() {
        int PresetIndex = 2;
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
        guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);
        // wait a second for the engine duration text to update
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(2);
        guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);
        guiController.remoteStartSettingsScreen.submitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 7000);
        assertTrue(guiController.remotePresetsScreen.getPresetsSettingsStates(true, true, PresetIndex));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.5  Remote start success via Preset")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote start success via preset")
    @Description("Genesis - Verify Remote start success via preset")
    void presetRemoteStartSuccess() {
        int PresetIndex = 2;
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.clickOnPresetStartButton(PresetIndex);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
        assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"));
        appController.appFunctions.pauseTestExecution(1, 5000);
        //assertTrue(guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue().contains("Success"),"Showing Success");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.6   Default Functionality of Remote start presets")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Default Functionality of Remote start presets")
    @Description("MyHyundai - Verify Default Functionality of Remote start presets")
    void defaultRemotePresets() {
        int PresetIndex = 2;
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
        guiController.remoteStartSettingsScreen.setDefaultPresetState(true);
        guiController.remoteStartSettingsScreen.SUBMIT.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.5.7  Rename Remote start presets||Verify Presets name is editable")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Rename remote start presets")
    @Description("MyHyundai - Verify Rename remote start presets||Verify Presets name is editable")
    void renameRemotePresets() {
        int PresetIndex = 2;
        String Rename = "TestRename";
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remoteStartSettingsScreen.renamePresets();
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.getTextUsingXpath(guiController.remotePresetsScreen.xpathOfPresetsName(PresetIndex)).equals(Rename));

    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.8 Remote start with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("Genesis - Verify  remote start with incorrect PIN shows incorrect PIN notification")
        void remoteStartWithIncorrectPinShowsIncorrectPinNotification() {
            int PresetsIndex = 2;
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.clickOnPresetStartButton(PresetsIndex);
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());

        }
    }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.9 Verify the stages displaying the progress for Remote Start")
        @Story("Test Remote Command")
        @Description("Genesis - Verify the stages displaying the progress for Remote Start")
        void progressstageofRemoteStart() {
            //guiController.remoteFeaturesScreenGen2Point5.stop.tap();
            //appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.homeScreen.remoteButton.tap();
            guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remoteStartSettingsScreen.submitButton.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            appController.appFunctions.pauseTestExecution(1, 5000);
            String resultText = guiController.remoteFeaturesScreenGen2Point5.Pending.getTextValue();
            String PendingText = guiController.remoteFeaturesScreenGen2Point5.Pending.getTextValue(180);
            String SuccessText = guiController.remoteFeaturesScreenGen2Point5.Success.getTextValue(180);
            // String SendingText = guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue(180);

            System.out.println("REMOTE START RESPONSE: " + resultText);
            //  assertTrue(resultText.contains("Request sent"));
            //  assertTrue(PendingText.contains("Pending"));
            // assertTrue(SuccessText.contains("Success"));

            //guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue().contains("Sending Request");
            //guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue().contains("Request Sent");
            //guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue().contains("Pending");
            //guiController.remoteFeaturesScreenGen2Point5.Remotestatus.getTextValue().contains("Success");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.10 Verify HI & LO Temp status under Vehicle status.")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Vehicle status")
        @Description("MyHyundai - Verify HI & LO Temp status under Vehicle status.")
        void HIghLOwtempinVehicleStatus() {
            int PresetIndex = 2;
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
            guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);
            guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 01);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.remoteStartSettingsScreen.SUBMIT.tap();
            guiController.remotePresetsScreen.clickOnPresetStartButton(PresetIndex);
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            appController.appFunctions.pauseTestExecution(1, 5000);
            //  String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            //  System.out.println("REMOTE START RESULT: " + resultText);
            //assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"), "Verified the successfull popup");

            guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();

            //guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            guiController.homeScreen.remoteButton.tap();
            guiController.homeScreen.vehiclestatus.tap();
            guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
            appController.appFunctions.pauseTestExecution(3, 3000);
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.climateStatus.getTextValue().contains("LO"), "Verified HI temperature in Vehicle status screen");
            appController.appFunctions.tapAndroidBackButton();
            // guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            guiController.homeScreen.remoteButton.tap();

            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            guiController.homeScreen.vehiclestatus.tap();
            guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
            appController.appFunctions.pauseTestExecution(3, 3000);
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.climateStatus.elementExists(), "Verified HI temperature in Vehicle status screen");
            // appController.appFunctions.tapAndroidBackButton();
            guiController.remoteFeaturesScreenGen2Point5.Remote.tap();

            guiController.remoteFeaturesScreenGen2Point5.stop.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            String resultStop = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE STOP RESULT: " + resultStop);
            assertTrue(resultStop.contains("Remote Control Stop") && resultStop.contains("successful"), "Verified the successfull popup");
            guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
//        guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            guiController.homeScreen.remoteButton.tap();
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
            guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 20);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.remoteStartSettingsScreen.SUBMIT.tap();
            guiController.remotePresetsScreen.clickOnPresetStartButton(PresetIndex);
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            appController.appFunctions.pauseTestExecution(1, 5000);
            String result = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE START RESULT: " + result);
            assertTrue(result.contains("Start with Climate Control") && result.contains("successful"), "Verified the successfull popup");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
//        guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            guiController.homeScreen.remoteButton.tap();
            guiController.homeScreen.vehiclestatus.tap();
            guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
            appController.appFunctions.pauseTestExecution(3, 3000);
            assertTrue(guiController.remoteFeaturesScreenGen2Point5.climateStatus.getTextValue().contains("HI"), "Verified HI temperature in Vehicle status screen");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.10 Quick Start_RemotePreset")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Default Functionality of Remote start presets")
        @Description("MyHyundai - Verify Quick Start_RemotePreset")
        void defaultRemotePresetStartDefaultpresetSet() {
            int PresetIndex = 2;
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
            guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);
            guiController.swipeScrollWheelUp(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 01);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.remoteFeaturesScreenGen2Point5.swipe.up(4);
            guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);
            guiController.remoteStartSettingsScreenGen2Point5.seatDownImage.tap();
            guiController.remoteStartSettingsScreenGen2Point5.heatedSeat_1.tap();
            guiController.remoteStartSettingsScreen.SUBMIT.tap();
            appController.appFunctions.pauseTestExecution(1, 2000);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.10 Verify Rear and side mirror defroster and heated steering wheel are showing separatly under remote presets for Gen2.5 Vehicle")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Vehicle status")
        @Description("MyHyundai - Verify Rear and side mirror defroster and heated steering wheel are showing separatly under remote presets for Gen2.5 Vehicle")
        void Rearmirrorandheatedsteeringseparatly() {
            int PresetIndex = 1;
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.editRemotePresets(PresetIndex);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.remoteFeaturesScreenGen2Point5.swipe.up(2);
            assertTrue(guiController.remoteStartSettingsScreenGen2Point5.rearandsidemirrorsdefroster.elementExists(), "Verified Rear and Side mirror defroster is present");
            assertTrue(guiController.remoteStartSettingsScreenGen2Point5.heatedSteeringWheel.elementExists(), "Verified Heated steering wheel is present");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.11 Verify presets are getting updated successfully")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Vehicle status")
        @Description("MyHyundai - Verify presets are getting updated successfully")
        void presetsgettingupdatedsuccessfully() {
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            for (int i = 1; i < 5; i++) {
                guiController.remotePresetsScreen.editRemotePresets(i);
                appController.appFunctions.pauseTestExecution(1, 1000);
                guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 3);
                appController.appFunctions.pauseTestExecution(1, 1000);
                guiController.remoteFeaturesScreenGen2Point5.swipe.up(4);
                guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);
                guiController.remoteStartSettingsScreenGen2Point5.submitButton.tap();
                appController.appFunctions.pauseTestExecution(1, 3000);
                assertTrue(guiController.remotePresetsScreen.getPresetsSettingsState(true, true, true, true, i));

            }


        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.12 Verify vehicle starts successful via Remote preset start using all Preset option")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Vehicle status")
        @Description("MyHyundai - Verify vehicle starts successful via Remote preset start using all Preset option")
        void vehiclestartsuccessfullyviaremotepreset() {

            for (int i = 1; i < 5; i++) {
                guiController.remoteFeaturesScreenGen2Point5.start.tap();
                appController.appFunctions.pauseTestExecution(1, 5000);
                guiController.remotePresetsScreen.editRemotePresets(i);
                guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);
                guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 04);
                appController.appFunctions.pauseTestExecution(1, 1000);
                guiController.remotePresetsScreen.swipe.up(4);
                guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);
                guiController.remoteStartSettingsScreen.SUBMIT.tap();
                guiController.remotePresetsScreen.clickOnPresetStartButton(i);
                guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
                appController.appFunctions.pauseTestExecution(1, 5000);
                String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(400);
                System.out.println("REMOTE START RESULT: " + resultText);
                assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"), "Verified the successfull popup");
                appController.appFunctions.pauseTestExecution(1, 2000);
                guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
                guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
                guiController.homeScreen.vehiclestatus.tap();
                guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
                appController.appFunctions.pauseTestExecution(3, 3000);
                //assertTrue(guiController.remoteFeaturesScreenGen2Point5.climateStatus.getTextValue().contains("HI"), "Verified HI temperature in Vehicle status screen");
                //assertTrue(guiController.remoteFeaturesScreenGen2Point5.Heating.getTextValue().contains("On"));
                //assertTrue(guiController.remoteFeaturesScreenGen2Point5.frontDefrostStatus.getTextValue().contains("On"));
                //assertTrue(guiController.remoteFeaturesScreenGen2Point5.seats.getTextValue().contains("On"));
                appController.appFunctions.tapAndroidBackButton();
                guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
                guiController.remoteFeaturesScreenGen2Point5.stop.tap();
                appController.appFunctions.pauseTestExecution(1, 5000);
                String resultStop = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
                System.out.println("REMOTE STOP RESULT: " + resultStop);
                assertTrue(resultStop.contains("Remote Control Stop") && resultStop.contains("successful"), "Verified the successfull popup");
                guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
                guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            }
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.ENGINE_DURATION_XPATH, 3);
            guiController.swipeScrollWheelDown(guiController.remoteStartSettingsScreen.TEMPERATURE_XPATH, 20);
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.remoteStartSettingsScreenGen2Point5.setStartWithSeatsSettingsStates(true, true, true, true);
            guiController.remoteStartSettingsScreen.SUBMIT.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            appController.appFunctions.pauseTestExecution(1, 5000);
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
            System.out.println("REMOTE START RESULT: " + resultText);
            assertTrue(resultText.contains("Start with Climate Control") && resultText.contains("successful"), "Verified the successfull popup");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.remoteFeaturesScreenGen2Point5.TapOkbutton();
            guiController.remoteFeaturesScreenGen2Point5.Remote.tap();
            guiController.homeScreen.vehiclestatus.tap();
            guiController.remoteFeaturesScreenGen2Point5.refreshButton.tap();
            appController.appFunctions.pauseTestExecution(3, 3000);
            // assertTrue(guiController.remoteFeaturesScreenGen2Point5.climateStatus.getTextValue().contains("HI"), "Verified HI temperature in Vehicle status screen");
            //assertTrue(guiController.remoteFeaturesScreenGen2Point5.Heating.getTextValue().contains("On"));
            //assertTrue(guiController.remoteFeaturesScreenGen2Point5.frontDefrostStatus.getTextValue().contains("On"));
            //assertTrue(guiController.remoteFeaturesScreenGen2Point5.seats.getTextValue().contains("On"));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.13 Verify Presets options are displayed for secondary driver")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Presets Are Showing")
        @Description("Genesis - Verify Presets options are displayed for secondary driver")
        void allRemotePresetsOptionsAreShowingSuccessfullyforSecondarydriver() {
            profile = appController.genesisProfiles.gen2GasStandard;
            guiController.loginAndGoToHomePageWithSelectVehicle(profile.secondaryAccount, profile.primaryVin, 60);
            guiController.homeScreen.remoteButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.isPresent();
            assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
            assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
            guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
            assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
            assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.5.14 Verify Gen 2.0 and Gen 2.5 vehicles shows icons in presets supported by them")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test Dashboard Command")
        @Description("Genesis - Verify Gen 2.0 and Gen 2.5 vehicles shows icons in presets supported by them")
        void icons() {
            guiController.remoteFeaturesScreenGen2Point5.startbutton.tap();
            assertTrue(guiController.remoteStartSettingsScreen.seatdefrost.elementExists(), "Seat icon is present");
            assertTrue(guiController.remoteStartSettingsScreen.frontdefrost.elementExists(), "Front defrost icon is present");
            assertTrue(guiController.remoteStartSettingsScreen.Temperature.elementExists(), "Temperature icon is present");
            assertTrue(guiController.remoteStartSettingsScreen.TempSign.elementExists(), "Temp sign icon exist");
            assertTrue(guiController.remoteStartSettingsScreen.PresetStart.elementExists(), "Preset start icon is present");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify Presets options are displayed on tap Remote Start Button via nav bar")
        @Severity(SeverityLevel.BLOCKER)
        @Story("Test Remote Presets Are Showing")
        @Description("Genesis - Verify Presets options are displayed on tap Remote Start Button via nav bar")
        void verifyPresetsOptionsAreDisplayedUnderStart() {
            guiController.remoteFeaturesScreenGen2Point5.start.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.remotePresetsScreen.isPresent();
            assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
            assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
            guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
            assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
            assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
        }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("Verify remote presets start  on device that does not support biometric")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Presets Are Showing")
    @Description("Genesis - Verify remote presets start  on device that does not support biometric")
    void verifyPresetsOptionsWithNoBiometric() {
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.isPresent();
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
        assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());
        guiController.remotePresetsScreen.isPresent();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("Verify Presets options are displayed on tap Remote Start Button via nav bar")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Presets Are Showing")
    @Description("Genesis - Verify Presets options are displayed on tap Remote Start Button via nav bar")
    void verifyVehicelStartsSuccessfully() {
        int PresetIndex = 2;
        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.isPresent();
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(1));
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(2));
        guiController.remoteStartSettingsScreenGen2Point5.swipe.fromBottomEdge(1);
        assertTrue(guiController.remotePresetsScreen.isRemoteStartPresetsPresent(3));
        assertTrue(guiController.remotePresetsScreen.remoteStartWithoutPresetsButton.elementExists());

        guiController.remoteFeaturesScreenGen2Point5.start.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.remotePresetsScreen.clickOnPresetStartButton(PresetIndex);
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        appController.appFunctions.pauseTestExecution(1, 5000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE START RESULT: " + resultText);
    }


}




