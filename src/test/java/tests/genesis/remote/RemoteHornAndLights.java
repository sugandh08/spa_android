package tests.genesis.remote;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard


@Epic("Remote Horn And Lights Test")
@Feature("Remote Horn And Lights")
@DisplayName("Remote Horn And Lights")
public class RemoteHornAndLights extends TestController {
    private Profile profile;
    private String incorrectPin;
    private Element remoteCommandButton;



    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);

        remoteCommandButton = guiController.remoteFeaturesScreenGen2Point5.hornAndLightsButton;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.remoteButton.tap();
        guiController.remoteFeaturesScreenGen2Point5.hornLightOptionsButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("7.6.1 Remote horn and lights success  shows successful message")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Remote Command")
    @Description("Genesis - Verify remote horn and lights success shows successful message")
    void successfulRemoteHornAndLightsShowsPopupMessage() {
        remoteCommandButton.tap();
        guiController.remoteStartSettingsScreenGen2Point5.hornLightButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(180);
        System.out.println("REMOTE HORN AND LIGHTS RESULT: " + resultText);
        assertTrue(resultText.contains("Horn & Lights") && resultText.contains("sent to vehicle"));
    }

    @Nested
    class InvalidPinTest {

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("7.6.2 Remote horn and lights with incorrect PIN shows incorrect PIN notification")
        @Story("Test Remote Command")
        @Description("Genesis - Verify  horn and lights with incorrect PIN shows incorrect PIN notification")
        void remoteHornAndLightsWithIncorrectPinShowsIncorrectPinNotification() {
            remoteCommandButton.tap();
            guiController.remoteStartSettingsScreenGen2Point5.hornLightButton.tap();
            guiController.enterPinScreen.enterPin(incorrectPin);
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.incorrectPinPopup.isPresent());
        }
    }
}
