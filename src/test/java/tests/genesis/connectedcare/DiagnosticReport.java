package tests.genesis.connectedcare;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;


import static org.junit.jupiter.api.Assertions.assertEquals;

// Profiles Used:
// 1. Gen2GasStandard
 
@Epic("Diagnostic Report Test")
@Feature("Diagnostic Report")
@DisplayName("Diagnostic Report")
public class DiagnosticReport extends TestController { 


    // NOTE: Diagnostics Report tests may randomly fail. No way to fix or predict when it will do so.
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.1 Diagnostics report shows that all systems are normal")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Diagnostic Report")
    @Description("Genesis - Verify Diagnostics report shows that all systems are normal")
    void DiagnosticsReportShowsAllSystemsNormal() {
       Profile profile = appController.genesisProfiles.gen2GasStandard;

        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin);
        guiController.homeScreen.tapCarCare();
        guiController.homeScreen.tapMvrBtn();
        appController.appFunctions.pauseTestExecution(2, 10000);
        guiController.monthlyVehicleReportScreen.diagnosticTab.tap(10);
        appController.appFunctions.pauseTestExecution(1, 5000);
        System.out.println(guiController.monthlyVehicleReportScreen.systemStatus.getTextValue());
        assertEquals("SYSTEMS NORMAL!", guiController.monthlyVehicleReportScreen.systemStatus.getTextValue(),
                "Status either showed that there was a vehicle issue (and thus the status did not read 'All" +
                        " systems normal', or the test got caught in navigation to the diagnostics report.");

    }
}
