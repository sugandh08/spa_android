package tests.genesis.connectedcare;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("Gen2 MVR Report Test")
@Feature("Gen2 MVR Report")
@DisplayName("Gen2 MVR Report")  
public class Gen2MvrReports extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.1.1 MVR history page shows a MVR report")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Gen2 MVR Report")
    @Description("Genesis - Verify MVR history page shows a MVR report")
    void mvrHistoryPageShowsMvrReport() {
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
        guiController.homeScreen.tapCarCare();
        guiController.homeScreen.tapMvrBtn();
        appController.appFunctions.pauseTestExecution(2, 5000);
        guiController.monthlyVehicleReportScreen.historyTab.tap(10);
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(guiController.monthlyVehicleReportScreen.oldMvrFirstEntryContainerView.elementExists());
    }
}
