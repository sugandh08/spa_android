package tests.genesis.hamburger_menuitems;

import config.Profile;
import io.appium.java_client.MobileElement;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("Alert Settings")
@Feature("Alert Settings")
@DisplayName("Alert Settings")
public class AlertSettings extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.1 Verify Geofence alert after add")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Geofence alert after add")
    void Geofencealertafteradd() {

        String location = "Los Angeles, CA, USA";
        String alertname = "Alert" + Math.random();
        String miles = "40";
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 5000);
        guiController.alertSettingsScreen.swipe.up(4);
        assertTrue(guiController.alertSettingsScreen.geofenceAlertText.elementExists(), "Verified Geofence alert present");


        //Check weather toggle button is ON or NOT
        if (!guiController.alertSettingsScreen.geoFenceAlertToggle.getClickableValue()) {
            guiController.alertSettingsScreen.geoFenceAlertToggle.tap();
        }

        guiController.alertSettingsScreen.addNewGeoFenceAlertButton.tap();
        guiController.alertSettingsScreen.Geofencesearchbar.enterText(location);
        guiController.alertSettingsScreen.Geofencesearchicon.tap();
        guiController.alertSettingsScreen.Geofencesearchicon.tap();
        guiController.alertSettingsScreen.Geofenncealertname.enterText(alertname);
        guiController.alertSettingsScreen.Geofencealertmiles.enterText(miles);
        guiController.alertSettingsScreen.GeofenceExclusive.tap();
        guiController.alertSettingsScreen.GeofenceRectangularfromcenter.tap();
        guiController.alertSettingsScreen.GeofenceSave.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        System.out.println(guiController.alertSettingsScreen.okbutton());
        //assertTrue(guiController.alertSettingsScreen.okbutton(), "verified existence of OK button in saved successfully popup");
        guiController.alertSettingsScreen.Tapokbutton();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.alertSettingsScreen.swipe.up(2);
        appController.appFunctions.pauseTestExecution(2, 3000);

       // assertTrue(guiController.getTextUsingXpath("//android.widget.TextView[@text='" + alertname + "']").contains(alertname), "Verified Geofrencealert name in alert screen");
       // assertTrue(guiController.alertSettingsScreen.Geofencealertlocationtitle.getTextValue().contains("Los Angeles, CA, USA"), "Verified Geofrence location in alert screen");
        assertTrue(guiController.alertSettingsScreen.Geofencealertmilestitle.elementExists(), "Verified Geofrence miles in alert screen");
       // assertTrue(guiController.alertSettingsScreen.GeofencealertiIclusiveEclusivetitle.getTextValue().contains("EXCLUSIVE"), "Verified EXCLUSIVE in alert screen ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.2 Verify Geofence alert after update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Geofence alert after update")
    void Geofencealertafterupdate() {
        String location = "California";
        String alertname = "Alert" + Math.random();
        String miles = "60";
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 4000);
        guiController.alertSettingsScreen.swipe.up(4);
        assertTrue(guiController.alertSettingsScreen.geofenceAlertText.elementExists(), "Verified Geofence alert present");

        //Check weather toggle button is ON or NOT
        if (!guiController.alertSettingsScreen.geoFenceAlertToggle.getClickableValue()) {
            guiController.alertSettingsScreen.geoFenceAlertToggle.tap();
        }
        guiController.alertSettingsScreen.Geofencealerttitle.tap();
        guiController.alertSettingsScreen.Geofencesearchbar.enterText(location);
        guiController.alertSettingsScreen.Geofencesearchicon.tap();
        guiController.alertSettingsScreen.Geofencesearchicon.tap();
        guiController.alertSettingsScreen.Geofenncealertname.enterText(alertname);
        guiController.alertSettingsScreen.Geofencealertmiles.enterText(miles);
        guiController.alertSettingsScreen.GeofenceExclusive.tap();
        guiController.alertSettingsScreen.GeofenceRectangularfromcenter.tap();
        guiController.alertSettingsScreen.GeofenceSave.tap();
        appController.appFunctions.pauseTestExecution(2, 2000);
        System.out.println(guiController.alertSettingsScreen.okbutton());
        assertTrue(guiController.alertSettingsScreen.okbutton(), "verified existence of OK button in saved successfully popup");
        guiController.alertSettingsScreen.Tapokbutton();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.alertSettingsScreen.swipe.up(2);
      //  assertTrue(guiController.getTextUsingXpath("//android.widget.TextView[@text='" + alertname + "']").contains(alertname), "Verified updated Geofrencealert name in alert screen");
      //  assertTrue(guiController.alertSettingsScreen.Geofencealertlocationtitle.getTextValue().contains("California"), "Verified updated Geofrence location in alert screen");
        assertTrue(guiController.alertSettingsScreen.Geofencealertmilestitle.getTextValue().contains("60 MILES"), "Verified updated Geofrence miles in alert screen");
       // assertTrue(guiController.alertSettingsScreen.GeofencealertiIclusiveEclusivetitle.getTextValue().contains("EXCLUSIVE"), "Verified updated EXCLUSIVE in alert screen ");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.3 Verify Geofence alert after delete")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Geofence alert after delete")
    void Geofencealertafterdelete()
    {
        String location = "California";
        String alertname = "Alert2";
        String miles = "60";
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.alertSettingsScreen.swipe.up(4);

        assertTrue(guiController.alertSettingsScreen.geofenceAlertText.elementExists(), "Verified Geofence alert present");

        String name = guiController.alertSettingsScreen.Geofencealerttitle.getTextValue();
        System.out.println(name);

        //Check weather toggle button is ON or NOT
        if (!guiController.alertSettingsScreen.geoFenceAlertToggle.getClickableValue()) {
            guiController.alertSettingsScreen.geoFenceAlertToggle.tap();
        }


        /*guiController.alertSettingsScreen.addNewGeoFenceAlertButton.tap();
        guiController.alertSettingsScreen.Geofencesearchbar.enterText(location);
        guiController.alertSettingsScreen.Geofencesearchicon.tap();
        guiController.alertSettingsScreen.Geofencesearchicon.tap();
        guiController.alertSettingsScreen.Geofenncealertname.enterText(alertname);
        guiController.alertSettingsScreen.Geofencealertmiles.enterText(miles);
        guiController.alertSettingsScreen.GeofenceExclusive.tap();
        guiController.alertSettingsScreen.GeofenceRectangularfromcenter.tap();
        guiController.alertSettingsScreen.GeofenceSave.tap();
        appController.appFunctions.pauseTestExecution(2, 2000);
        System.out.println(guiController.alertSettingsScreen.okbutton());
        assertTrue(guiController.alertSettingsScreen.okbutton(), "verified existence of OK button in saved successfully popup");
        guiController.alertSettingsScreen.Tapokbutton();
        appController.appFunctions.pauseTestExecution(4, 4000);
*/

        //Tap on alertname defined above in alertscreen
        List<MobileElement>  lsttxt=   guiController.getTextsUsingXpath("(//android.widget.Linear[contains(@resource-id,'Layoutcom.stationdm.genesis:id/geofence_item_ll')])");
        for(MobileElement e:lsttxt)
        {
            if(e.getText().equalsIgnoreCase(name)){
                e.click();
                break;
            }
        }
        guiController.alertSettingsScreen.GeofenceDeletebutton.tap();
        System.out.println(guiController.alertSettingsScreen.deletebuttn());
        assertFalse(guiController.alertSettingsScreen.deletebuttn());
        guiController.alertSettingsScreen.TapDeletebutton();
        guiController.alertSettingsScreen.Tapokbutton();
      //  assertFalse(guiController.alertSettingsScreen.Geofencealerttitle.getTextValue().contains(name),"Verified alername does not exist in alert screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.4 Verify Curfew alert after add")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Curfew Alert")
    @Description("Genesis - Verify Curfew alert after add")
    void addNewCurfewAlert() {

        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.alertSettingsScreen.swipe.up(1);
        if (guiController.alertSettingsScreen.addNewCurfewAlertButton.elementExists()) {
            guiController.alertSettingsScreen.addNewCurfewAlertButton.tap();
        } else {
            guiController.alertSettingsScreen.curfewAlertButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.curfewAlertScreen.curfewFromButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 1);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 10);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 11);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String curfewFromTextValue = guiController.curfewAlertScreen.curfewFromTextView.getTextValue();
        System.out.println(curfewFromTextValue);
        String curfewToTextValue = guiController.curfewAlertScreen.curfewToTextView.getTextValue();
        System.out.println(curfewToTextValue);
        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Curfew ") && resultText.contains("successfully"), "Curfew Alert is saved successfully");
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + curfewFromTextValue + "']").equals(curfewFromTextValue));
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + curfewToTextValue + "']").equals(curfewToTextValue));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertSavedPopup.okButton.tap();
       // appController.appFunctions.pauseTestExecution(1, 4000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.5 Verify Curfew alert after delete")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Curfew Alert")
    @Description("Genesis - Verify Curfew alert after delete ")
    void deleteCurfewAlert() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.alertSettingsScreen.swipe.up(1);
        //check if any curfew alert already added
        if (guiController.alertSettingsScreen.ExistingCurfewAlert.elementExists()) {
            System.out.println("Clicking on existed curfew alert ");
        } else {
            //add a curfew alert
            guiController.alertSettingsScreen.curfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            guiController.curfewAlertScreen.curfewFromButton.tap();
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
        }

        guiController.alertSettingsScreen.ExistingCurfewAlert.tap();
        guiController.curfewAlertScreen.deleteButton.tap();
        guiController.deleteConfirmationPopup.clickOnDeleteButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        //Assertions.assertNotEquals(guiController.getTextUsingXpath(guiController.alertSettingsScreen.xpathOfFromCurfewAlert), curfewFromTextValue);
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertSavedPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(2000);
        System.out.println("Curfew ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Curfew") && resultText.contains("successfully"), "Curfew Alert is deleted successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.6 Verify Curfew alert after  update")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Curfew Alert")
    @Description("Genesis - Verify Curfew alert after  update ")
    void updateCurfewAlert() {

        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        guiController.alertSettingsScreen.swipe.up(1);

        //check if any curfew alert already added
        if (guiController.alertSettingsScreen.ExistingCurfewAlert.elementExists()) {
            System.out.println("Clicking on existed curfew alert ");
        } else {
            //add a curfew alert
            guiController.alertSettingsScreen.curfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            guiController.curfewAlertScreen.curfewFromButton.tap();
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 1);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            assertTrue(resultText1.contains("Your Curfew ") && resultText1.contains("successfully"), "Curfew Alert is saved successfully");
            guiController.alertSavedPopup.okButton.tap();

        }

        //Update Curfew Alert

        guiController.alertSettingsScreen.ExistingCurfewAlert.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.curfewAlertScreen.curfewFromButton.tap();
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();

        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Curfew ") && resultText.contains("successfully"), "Curfew Alert is saved successfully");
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.7 Verify Speed alert after add(If alert is not configured in log in account)")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Speed alert after add(If alert is not configured in log in account)")
    void addNewSpeedAlert() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettingsButton.tap();
        appController.appFunctions.pauseTestExecution(2, 3000);
        if (!guiController.alertSettingsScreen.speedAlertToggle.getCheckedValue()) {
            guiController.alertSettingsScreen.speedAlertToggle.tap();
        }
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
        guiController.alertSavedPopup.okButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.8 Update Speed alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Speed Alert")
    @Description("Genesis - Update Speed alert ")
    void updateSpeedAlert() {

        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettings.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);

        //check if any speed alert already added
        if (guiController.alertSettingsScreen.speedAlertButton.elementExists()) {
            System.out.println("Clicking on existed speed alert ");
        } else {
            //add a speed alert
            guiController.alertSettingsScreen.speedAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.speedAlertScreen.isPresent());
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.speedAlertScreen.speedLimitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
            guiController.speedAlertScreen.doneButton.tap();
            String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
            guiController.speedAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
        }

        //Update Speed Alert
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelDown(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("SPEED ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.9 Creating Valet Alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Creating Valet Alert Setting Value")
    void addNewValetAlert() {

        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettings.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        if (!guiController.alertSettingsScreen.valetAlertToggle.getCheckedValue()) {
            guiController.alertSettingsScreen.valetAlertToggle.tap();
        }
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.valetAlertScreen.distanceLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.valetAlertScreen.valetLimitWheelXpath, 2);
        guiController.valetAlertScreen.doneButton.tap();
        String valetLimit = guiController.valetAlertScreen.valetLimitTextView.getTextValue();
        guiController.valetAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.alertSettingsScreen.valetAlertValue.getTextValue().contains(valetLimit));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("3.2.10 Verify Valet alert after update")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Valet Alert")
    @Description("Genesis - Verify Valet alert after update")
    void updateValetAlert() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.alertSettings.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        //check if any speed alert already added
        if (guiController.alertSettingsScreen.valetAlertValue.elementExists()) {
            System.out.println("Clicking on existed valet alert ");
        } else {
            //add a Valet alert
            guiController.alertSettingsScreen.valetAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.valetAlertScreen.isPresent());
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.valetAlertScreen.distanceLimitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.swipeScrollWheelUp(guiController.valetAlertScreen.valetLimitWheelXpath, 2);
            guiController.valetAlertScreen.doneButton.tap();
            String valetLimit = guiController.valetAlertScreen.valetLimitTextView.getTextValue();
            guiController.valetAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 1000);
            assertTrue(guiController.alertSettingsScreen.valetAlertValue.getTextValue().contains(valetLimit));
            guiController.alertSettingsScreen.saveButton.tap();
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            System.out.println("Valet ALERT RESULT: " + resultText);
            assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");
        }
        //Update Valet Alert
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.valetAlertScreen.distanceLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.valetAlertScreen.valetLimitWheelXpath, 2);
        guiController.valetAlertScreen.doneButton.tap();
        guiController.valetAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSavedPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Valet ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");
    }


}