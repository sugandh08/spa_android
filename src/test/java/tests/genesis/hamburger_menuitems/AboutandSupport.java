package tests.genesis.hamburger_menuitems;

import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("About and Support")
@Feature("About and Support")
@DisplayName("About and Support")
public class AboutandSupport extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.1 rename 'Financing' to 'General Information' under About and Support Screen")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify rename 'Financing' to 'General Information' under About and Support Screen")
    void renameFinancingToGeneralInfo() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        guiController.alertSettingsScreen.swipe.up(3);
        assertTrue(guiController.aboutGenesisScreen.txtGeneralInfo.elementExists(), " Rename  'Financing' to 'General Information' under About and Support Screen is displayed.");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.2 changes in About and Support Genesis under hamburger")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the changes in Support and About Genesis under hamburger")
    void aboutAndSupportDisplayed() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.menuScreen.aboutAndSupport.elementExists(), " 'About and Support' link in one line in place of Support and About Genesis is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.3 sub menu under Guides section in About and Support.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the sub menu under Guides section in About and Support.")
    void subMenuUnderGuidesSection() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.headerGuides.elementExists(), "Verify Guide section is displayed.");
        assertTrue(guiController.aboutGenesisScreen.txtGettingStartedGuide.elementExists(), "Verify Getting starting guide is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtIndicatorGuide.elementExists(), "Verify the Indicator guide is displayed");
        assertTrue(guiController.aboutGenesisScreen.isOwnersManualDisplayed(), "Verify Owner manual is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtMaintenanceInfo.elementExists(), "Verify Maintenance info is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtWarrantyInfo.elementExists(), "verify Warranty info is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtVirtualGuide.elementExists(), "Verify Virtual guide is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.4 sub menu under Contact Us section in About and Support.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the sub menu under Contact us section in About and Support.")
    void subMenuUnderContactUsSection() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        guiController.alertSettingsScreen.swipe.up(5);
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        System.out.println("1");
        assertTrue(guiController.aboutGenesisScreen.headerContactUs.elementExists(), "Verify Contact us section is displayed.");
        System.out.println("2");
        assertTrue(guiController.aboutGenesisScreen.txtEmailAppSupport.elementExists(), "Verify Email Support guide is displayed");
        System.out.println("3");
        assertTrue(guiController.aboutGenesisScreen.txtConsumerAssistanceCentre.elementExists(), "Verify Consumer Assistance Center is displayed");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.5 sub menu under Version section in About and Support.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the sub menu under Version section in About and Support.")
    void subMenuUnderVersionSection() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.alertSettingsScreen.swipe.up(5);
        appController.appFunctions.pauseTestExecution(1, 3000);
       // String txt = guiController.getTextUsingXpath("(//*[@id='rvSupportAboutList']/*/*[@id='tvSupportAboutTitle'])[3]");
       // System.out.println(txt);
        assertTrue(guiController.aboutGenesisScreen.txtversion.elementExists(),"Verify Version section is displayed.");
        assertTrue(guiController.aboutGenesisScreen.txtFAQ.elementExists(), "Verify FAQ is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtTermsCondition.elementExists(), "Verify terms and condition is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtPrivacyPolicy.elementExists(), "Verify privacy policy is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.6 Verify user can see Genesis Resources Text in the support screen.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Genesis Resources Text in the support screen")
    void GenesisresourcesunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtgenesisResources.elementExists(), "Verified Genesis Resources present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.7 Verify user can see Email App Support")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Email App Support")
    void emailappsupportunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtEmailAppSupport.elementExists(), "Verified Email app support present in About and support screen");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.8 Verify user can see Connected Services option in the support screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Connected Services option in the support screen")
    void ConnectedServicesunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtconnectedservices.elementExists(), "Verified Connected Services present in About and support screen");

    }


    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.9 Verify user can see Terms and Conditions option in the support screen")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis -Verify user can see Terms and Conditions option in the support screen")
    void TermsandConditionsunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtTermsCondition.elementExists(), "Verified Terms and Condition present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.10 Verify user can see Genesis FAQ option")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Genesis FAQ option")
    void FAQunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtFAQ.elementExists(), "Verified FAQ present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.11 Verify user can see Owner's Manual.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Owner's Manual")
    void OwnersManualunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.isOwnersManualDisplayed(), "Verified Owner Manual present in About and Support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.12 Verify user can see Genesis Virtual Guide.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Genesis Virtual Guide")
    void GenesisVirtualGuideunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtVirtualGuide.elementExists(), "Verified Virtual guide present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.13 Verify user can see MyGenesis.com option in the About Genesis Screen.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see MyGenesis.com option in the About Genesis Screen")
    void MyGenesiscomunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtmygenesiscom.elementExists(), "Verified MyGenesis.com present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.14 Verify user can see Genesis Accessories option in the About Genesis Screen.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Genesis Accessories option in the About Genesis Screen")
    void GenesisAccessoriesunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.genesisAccessories.elementExists(), "Verified genesis accesories present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.15 Verify user can see Privacy Policy link in the Menu:Support screen.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Privacy Policy link in the Menu:Support screen")
    void PrivacyPolicyunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(5);
        assertTrue(guiController.aboutGenesisScreen.txtPrivacyPolicy.elementExists(), "Verified Privacy policy present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.16 Verify user can see Email App support screen when click on it")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Email App support screen when click on it")
    void emailappsupportpageunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtEmailAppSupport.elementExists(), "Verified Email app support present in About and support screen");
        guiController.aboutGenesisScreen.txtEmailAppSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.emailappsupportpage.elementExists(), "Verified Email App Support page displayed");
        assertTrue(guiController.aboutGenesisScreen.incidentinformation.elementExists(), "Verified incidentinformation displayed in email app page");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.17 Verify user can see Terms and Conditions page with info when click on it.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis -Verify user can see Terms and Conditions page with info when click on it")
    void TermsandConditionspageunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtTermsCondition.elementExists(), "Verified Terms and Condition present in About and support screen");
        guiController.aboutGenesisScreen.txtTermsCondition.tap();
        assertTrue(guiController.aboutGenesisScreen.termsandconditionpage.elementExists(), "Verified Terms and condition page displayed when click on it");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.18 Verify user can see FAQ questions when click on it")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see FAQ questions when click on it")
    void FAQpageunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtFAQ.elementExists(), "Verified FAQ present in About and support screen");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.19 Verify user can see FAQ questions when click on it")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see FAQ questions when click on it")
    void FAQquestionsunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtFAQ.elementExists(), "Verified FAQ present in About and support screen");
        guiController.aboutGenesisScreen.txtFAQ.tap();
        assertTrue(guiController.aboutGenesisScreen.getGenesisFAQ.elementExists(), "Verified Genesis FAQ is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.20 Verify user can download owner's manual")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can download owner's manual")
    void downloadOwnersManualunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.isOwnersManualDisplayed(), "Verified Owner Manual present in About and Support screen");
        guiController.aboutGenesisScreen.TapOwnerManual();
        appController.appFunctions.pauseTestExecution(1, 4000);
        guiController.aboutGenesisScreen.TapOwnerManual();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.aboutGenesisScreen.OwnerManualisPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.21 Verify it's directing to Genesis Virtual Guide app when click on it")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify it's directing to Genesis Virtual Guide app when click on it")
    void downloadGenesisVirtualGuideunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        assertTrue(guiController.aboutGenesisScreen.txtVirtualGuide.elementExists(), "Verified Virtual guide present in About and support screen");
        guiController.aboutGenesisScreen.txtVirtualGuide.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.aboutGenesisScreen.GenesisVirtualGuideAPPisPresent(), "Verified Genesis Virtual Guide App screen is present");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.22 Verify user can see MyGenesis.com web page when click on it.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see MyGenesis.com web page when click on it")
    void MyGenesiscomWebpageunderSupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(2);
        assertTrue(guiController.aboutGenesisScreen.txtmygenesiscom.elementExists(), "Verified MyGenesis.com present in About and support screen");
        guiController.aboutGenesisScreen.txtmygenesiscom.tap();
        appController.appFunctions.pauseTestExecution(2, 4000);
        assertTrue(guiController.aboutGenesisScreen.pagetitle.getTextValue().contains("MYGENESIS.COM"), "Verified MYGENESIS.COM page is displayed");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.23 Verify user can see Privacy Policy link in the Menu:Support screen.")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can see Privacy Policy link in the Menu:Support screen")
    void PrivacyPolicypage() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.headerAboutAndSupport.elementExists(), "Verify About and Support screen is displayed");
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtPrivacyPolicy.elementExists(), "Verified Privacy policy present in About and support screen");
        guiController.aboutGenesisScreen.txtPrivacyPolicy.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        assertTrue(guiController.aboutGenesisScreen.pagetitle.getTextValue().contains("PRIVACY POLICY"), "Verified Privacy Policy page is displayed");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.24 Verify the main links under About and Support.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the main links under in About and Support.")
    void MainlinkUnderaboutandsupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.txtGuides.elementExists(), "Guide section is present");
        assertTrue(guiController.aboutGenesisScreen.txtgenesisResources.elementExists(), "Genesis Resources section is present");
        guiController.aboutGenesisScreen.swipe.up(5);
        assertTrue(guiController.aboutGenesisScreen.txtLinks.elementExists(), "Links section is present");
        assertTrue(guiController.aboutGenesisScreen.txtContactus.elementExists(), "Contact us section is present");
        guiController.aboutGenesisScreen.swipe.up(1);
        assertTrue(guiController.aboutGenesisScreen.txtversion.elementExists(), "Version section is present");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.25 Verify the sub menu under Links section in About and Support.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the sub menu under Links section in About and Support.")
    void subMenuUnderlinkaboutandsupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtmygenesiscom.elementExists(), "MyGenesis.com is present under link section");
        assertTrue(guiController.aboutGenesisScreen.txtGenesisUSA.elementExists(), "GenesisUSA is present under link section");
        assertTrue(guiController.aboutGenesisScreen.genesisAccessories.elementExists(), "Genesis Accessories is present under link section");
        assertTrue(guiController.aboutGenesisScreen.txtGenesisCollissionCenter.elementExists(), "Genesis Collission Center is present under link section");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.25 Verify the sub menu under Genesis Resources section in About and Support.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify the sub menu under Genesis Resources section in About and Support.")
    void subMenuUnderResourcesaboutandsupport() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        guiController.aboutGenesisScreen.swipe.up(1);
        assertTrue(guiController.aboutGenesisScreen.txtGettingstarted.elementExists(), "Getting started is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtconnectedservices.elementExists(), "Connected services is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtVehicleHealth.elementExists(), "Vehicle Health is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtManualandWarranties.elementExists(), "Manual and Warranties is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtBluetoothAssistance.elementExists(), "Bluetooth Assistance is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtTechnologyandNavigation.elementExists(), " Technology and Navigation is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtAccessoriesandParts.elementExists(), "Accessories and Parts is present under Resources");
        assertTrue(guiController.aboutGenesisScreen.txtGeneralInfo.elementExists(), "GeneralInfo is present under Resources");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.26 Verify Service Valet option is removed under Burger Menu section")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify Service Valet option is removed under Burger Menu section")
    void ServiceValetremovedfromhamburger() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertFalse(guiController.menuScreen.ServiceValet.elementExists(),"Service Valet is not present in hamburger menu section");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.27 Verify user can enter incident information and sumbit the it")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test About and Support")
    @Description("Genesis - Verify user can enter incident information and sumbit the it")
    void incidentinformation() {
        guiController.homeScreen.menuButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.menuScreen.aboutAndSupport.tap();
        guiController.aboutGenesisScreen.swipe.up(4);
        assertTrue(guiController.aboutGenesisScreen.txtEmailAppSupport.elementExists(),"Email App support is present");
        guiController.aboutGenesisScreen.txtEmailAppSupport.tap();
        assertTrue(guiController.aboutGenesisScreen.incidentinformation.elementExists(),"incidentinformation page is shown");
        guiController.aboutGenesisScreen.Vehicleincidentinformation.tap();
        guiController.aboutGenesisScreen.swipe.fromRightEdge(6);
        guiController.aboutGenesisScreen.backbuttonapp.tap();
        assertTrue(guiController.aboutGenesisScreen.VehicleVinEmailAppSupport.getTextValue().contains("KMHGN"),"Verified vehicle vin is displayed");
    }

}