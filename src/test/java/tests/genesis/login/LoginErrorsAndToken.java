package tests.genesis.login;

import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard
 
@Epic("Login Errors And Token")
@Feature("Login Errors And Token")
public class LoginErrorsAndToken extends TestController {
    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.1 Entering invalid email shows 'please enter username for account' popup")
    @Severity(SeverityLevel.MINOR) 
    @Story("Test Login Errors And Token")
    @Description("Genesis - Verify Entering invalid email shows 'please enter username for account' popup")
    void invalidEmailLoginShowsInvalidEmailPopup() {
        if(guiController.introductionScreen.isPresent()) {
            guiController.introductionScreen.loginButton.tap();
        }
        try{
            Thread.sleep(2000);
            guiController.loginScreen.usernameEditText.enterText("invalid email");
            Thread.sleep(2000);
            //appController.appFunctions.tapAndroidBackButton();
            guiController.loginScreen.passwordEditText.enterText("invalid password");
            //appController.appFunctions.tapAndroidBackButton();
            Thread.sleep(2000);
            guiController.loginScreen.loginButton.tap();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
        assertTrue(guiController.pleaseEnterUsernameForAccountPopup.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.3.2 Entering invalid password shows 'please enter password' popup")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Errors And Token")
    @Description("Genesis - Verify Entering invalid password shows 'please enter password' popup")
    void invalidPasswordLoginShowsInvalidPasswordPopup() {
        if(guiController.introductionScreen.isPresent()) {
            guiController.introductionScreen.loginButton.tap();
        }

        try{
            Thread.sleep(2000);
            guiController.loginScreen.usernameEditText.enterText(appController.genesisProfiles.gen2GasStandard.primaryAccount.email);
            Thread.sleep(2000);
            //appController.appFunctions.tapAndroidBackButton();
            guiController.loginScreen.passwordEditText.enterText("invalid password");
           // appController.appFunctions.tapAndroidBackButton();
            Thread.sleep(2000);
            guiController.loginScreen.loginButton.tap();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }

        assertTrue(guiController.invalidPasswordPopup.isPresent());
    }
}
