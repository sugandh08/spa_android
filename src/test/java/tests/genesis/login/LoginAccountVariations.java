package tests.genesis.login;

import config.Profile;
import config.Vehicle;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;
import views.genesis.screens.HomeScreen;

import java.util.prefs.Preferences;

import static org.junit.jupiter.api.Assertions.*;

// Profiles Used:
// 1. TwoGen2 
// 2. GenesisAndMyHyundaiVehicles
// 3. MyHyundaiVehicleOnly
// 4. WithSecondaryDriver

@Epic("Login Account Variations")
@Feature("Login Account Variations") 
public class LoginAccountVariations extends TestController {
  /*  @Nested
    @DisplayName("1.1.1 Account with two Gen 2 vehicles allows user to select and switch vehicles")
    class VehicleSelectionOnAccountWithTwoGen2Vehicles {
        private Profile profile;
        private Vehicle primaryVehicle;
        private Vehicle secondaryVehicle;
        private HomeScreen primaryVehicleHomeScreen;
        private HomeScreen secondaryVehicleHomeScreen;

        @BeforeEach
        void setUpTests() {
            profile = appController.genesisProfiles.twoGen2;
            primaryVehicle = profile.primaryAccount.vehicles.get(profile.primaryVin);
            secondaryVehicle = profile.primaryAccount.vehicles.get(profile.secondaryVin);
            primaryVehicleHomeScreen = guiController.getHomeScreenByPowerType(primaryVehicle.powerType);
            secondaryVehicleHomeScreen = guiController.getHomeScreenByPowerType(secondaryVehicle.powerType);
            guiController.loginScreen.loginWithAccount(profile.primaryAccount);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.1.1 Selecting first vehicle opens home page for first vehicle")
        void selectingFirstVehicleOpensHomePageOfFirstVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(primaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            guiController.homeScreen.menuButton.tap();
            assertTrue(guiController.menuScreen.vin.getTextValue().contains(primaryVehicle.vin));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.1.2 Selecting second vehicle opens home page for second vehicle")
        void selectingSecondVehicleOpensHomePageOfSecondVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(secondaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            guiController.homeScreen.menuButton.tap();
            assertTrue(guiController.menuScreen.vin.getTextValue().contains(secondaryVehicle.vin));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.1.3 Switching to second vehicle opens home page for second vehicle")
        void switchingToSecondVehicleOpensHomePageForSecondVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(primaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            primaryVehicleHomeScreen.menuButton.tap();
            guiController.menuScreen.switchVehicleButton.tap();
            guiController.selectVehicleScreen.tapVehicleButtonByVin(secondaryVehicle.vin);
            secondaryVehicleHomeScreen.menuButton.tap();
            assertTrue(guiController.menuScreen.vin.getTextValue().contains(secondaryVehicle.vin));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.1.4 Switching to first vehicle opens home page for first vehicle")
        void switchingToFirstVehicleOpensHomePageForFirstVehicle() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(secondaryVehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            secondaryVehicleHomeScreen.menuButton.tap();
            guiController.menuScreen.switchVehicleButton.tap();
            guiController.selectVehicleScreen.tapVehicleButtonByVin(primaryVehicle.vin);
            primaryVehicleHomeScreen.menuButton.tap();
            assertTrue(guiController.menuScreen.vin.getTextValue().contains(primaryVehicle.vin));
        }
    }*/

   /* @Nested
    @DisplayName("1.1.2 Account with Gen 2 vehicle and Hyundai vehicle has correct functionality")
    class AccountWithGen2AndGenesisVehiclesFunctionsCorrectly {
        private Profile profile;
        private Vehicle gen2Vehicle;
        private Vehicle hyundaiVehicle;
        private HomeScreen gen2HomeScreen;

        @BeforeEach
        void setupTests() {
            profile = appController.genesisProfiles.genesisAndMyHyundaiVehicles;
            gen2Vehicle = profile.primaryAccount.vehicles.get(profile.primaryVin);
            hyundaiVehicle = profile.primaryAccount.vehicles.get(profile.secondaryVin);
            gen2HomeScreen = guiController.getHomeScreenByPowerType(gen2Vehicle.powerType);

            guiController.loginScreen.loginWithAccount(profile.primaryAccount);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.2.1 Selecting Genesis vehicle opens homepage for that vehicle")
        void selectingGen2OpensCorrectHomepage() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(gen2Vehicle.vin);
            guiController.termsAndConditionsPopup.acceptButton.tap();
            gen2HomeScreen.menuButton.tap();
            assertTrue(guiController.menuScreen.vin.getTextValue().contains(gen2Vehicle.vin));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.2.2 Selecting Hyundai vehicle prompts to open MyHyundai app")
        void selectingGenesisPromptsToOpenGenesisApp() {
            guiController.selectVehicleScreen.tapVehicleButtonByVin(hyundaiVehicle.vin);
            assertTrue(guiController.myHyundaiRedirectPopup.isPresent());
        }
    }*/

    @Nested
    @DisplayName("1.1.3 Logging in to account with only a Hyundai vehicle prompts to switch to MyHyundai app")
    class AccountWithOnlyGenesisVehiclePromptsToSwitchApps {
        private Profile profile;

        @BeforeEach
        void setupTests() {

            profile = appController.genesisProfiles.myHyundaiVehicleOnly;

            profile = appController.genesisProfiles.gen2GasStandard;


            guiController.loginScreen.loginWithAccount(profile.primaryAccount);
            appController.appFunctions.pauseTestExecution(1, 3000);
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.3.1 Redirect prompt appears")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("Genesis - Verify Redirect prompt appears")
        void redirectPromptAppears() {
            appController.appFunctions.pauseTestExecution(2, 10000);
            assertTrue(guiController.myHyundaiRedirectPopup.isPresent());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.3.2 Redirect prompt open button opens MyHyundai app")
        @Severity(SeverityLevel.MINOR)
        @Story("Test Login Account Variations")
        @Description("Genesis - Verify Redirect prompt open button opens MyHyundai app")
        void redirectPromptOpenButtonOpensMyHyundaiApp() {
            appController.appFunctions.pauseTestExecution(1, 10000);
            // reinstall myhyundai to make sure there is no cached/saved data in it.
          /*  appController.appFunctions.uninstallMyHyundaiAndroid();
            appController.appFunctions.installApp(appController.getMyHyundaiAndroidFileName());
*/
           guiController.myHyundaiRedirectPopup.openButton.tap();
          /*  guiController.permissionsPopup.isPresent();
            for(int i=0; i<4; i++){
                guiController.permissionsPopup.allowButton.tap();
            }*/

            assertTrue(guiController.myHyundaiLoginScreen.isPresent());

          /*  // back out of the myhyundai app
            appController.appFunctions.tapAndroidBackButton();*/
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1.3.3 Redirect prompt close button closes popup")
        void redirectPromptCloseButtonDismissesPopup() {
            appController.appFunctions.pauseTestExecution(1, 10000);
            if(guiController.myHyundaiRedirectPopup.isPresent()){
                guiController.myHyundaiRedirectPopup.closeButton.tap();
            }

            assertFalse(guiController.myHyundaiRedirectPopup.isPresent());
        }
  }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.4 Logging in as secondary driver opens shared vehicle home page")
    void loggingInAsSecondaryDriverOpensCorrectHomeScreen() {
        Profile profile = appController.genesisProfiles.withSecondaryDriver;
        // using "vehicleFromPrimaryAccount" because the vehicle should be the exact same on both accounts,
        // however to make sure the test throws no false positives, use information from the primary account
        // while logging in on the secondary account. If any steps fail, they didn't have the same vehicle or
        // the vehicle data is stored incorrectly on the app side.
        Vehicle vehicleFromPrimaryAccount = profile.primaryAccount.vehicles.get(profile.primaryVin);
        System.out.println(profile.primaryVin + guiController.menuScreen.vinUnderProfile.getTextValue());
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.secondaryAccount, vehicleFromPrimaryAccount.vin);
        System.out.println(vehicleFromPrimaryAccount.vin);
        guiController.homeScreen.menuButton.tap();
        guiController.menuScreen.profileButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        // assertTrue(guiController.menuScreen.vin.getTextValue().contains(vehicleFromPrimaryAccount.vin));
        assertTrue(guiController.menuScreen.vinUnderProfile.getTextValue().contains(profile.primaryVin));
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.1.5 Logging in and selecting a gen 2 gas vehicle opens gen 2 gas home page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Login Account Variations")
    @Description("Genesis - Verify Logging in and selecting a gen 2 gas vehicle opens gen 2 gas home page")
    void selectingGen2GasVehicleOpensGen2GasHomePage() {
    	  Profile profile = appController.genesisProfiles.gen2GasStandard;

          guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);

          guiController.homeScreen.menuButton.tap();
          guiController.menuScreen.profileButton.tap();
         // assertTrue(guiController.menuScreen.vin.getTextValue().contains(profile.primaryVin));
        System.out.println(profile.primaryVin);
        appController.appFunctions.pauseTestExecution(1, 2000);
          assertTrue(guiController.menuScreen.vinUnderProfile.getTextValue().contains(profile.primaryVin));


    }
}