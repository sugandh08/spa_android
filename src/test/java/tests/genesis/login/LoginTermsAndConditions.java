package tests.genesis.login;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;
 
// Profiles Used:
// 1. Gen2GasStandard

@Epic("Login Terms and Conditions Test")
@Feature("Login Terms and Conditions")
@DisplayName("Login Terms and Conditions")
public class LoginTermsAndConditions extends TestController {

 

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("1.2.1 View Terms and Conditions at the time of Login")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Terms and Conditions")
    @Description("Genesis - Verify viewing Terms and Conditions at the time of Login")
    void LoginAndViewTermsAndConditions() {
        Profile profile = appController.genesisProfiles.gen2GasStandard;
       /* // reinstall Genesis to make sure there is no cached/saved data in it.
        appController.appFunctions.uninstallGenesisAndroid();
        appController.appFunctions.installApp(appController.getGenesisAndroidFileName());

        appController.appFunctions.launchApp();*/

        //guiController.loginScreen.loginButton.tap();

        guiController.loginScreen.loginWithAccount(profile.primaryAccount);

        appController.appFunctions.pauseTestExecution(1,10000);

        guiController.termsAndConditionsPopup.viewButton.tap();

        appController.appFunctions.pauseTestExecution(1,10000);

        appController.appFunctions.tapAndroidBackButton();

        guiController.loginScreen.loginButton.tap();

        appController.appFunctions.pauseTestExecution(1, 4000);

        guiController.termsAndConditionsPopup.acceptButton.tap();

        appController.appFunctions.pauseTestExecution(1, 2000);

      guiController.termsAndConditionsPopup.exitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.loginScreen.allowButton.tap();
        // guiController.genesisServiceValetPopup.tapGotItIfThisPopupPresent(10);
       guiController.eventScreen.tapGotItIfThisPopupPresent(10);



       assertTrue(guiController.homeScreen.isPresent());









    }

}
