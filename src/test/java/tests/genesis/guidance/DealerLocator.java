package tests.genesis.guidance;
import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.*;

// Profiles Used:
// 1. Gen2GasStandard
// 2. WithSecondaryDriver

@Epic("DealerLocator Test")
@Feature("DealerLocator Gen2")
@DisplayName("DealerLocator")
public class DealerLocator extends TestController {
    @Nested
    @DisplayName("Signed in as Gen 2 gas standard")
    class Gen2GasStandard {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.genesisProfiles.gen2GasStandard;
            guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);
            guiController.homeScreen.mapButton.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.1 Dealer Locator")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify Preferred dealer under dealer locator screen")
        void preferredDealerUnderDealerLocatorScreen() {
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(1, 4000);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.setAsPreferredIcon.elementExists(), "Verify Preferred dealer should appear on Dealer locator screen.");
            appController.appFunctions.pauseTestExecution(1, 2000);
            assertTrue(guiController.mapScreen.dealerNameTxt.elementExists(), "Verify dealer name is displayed");
            appController.appFunctions.pauseTestExecution(1, 2000);
            assertTrue(guiController.mapScreen.dealerLocation.elementExists(), "Verify dealer location is displayed");
            appController.appFunctions.pauseTestExecution(1, 2000);
            assertTrue(guiController.mapScreen.scheduleServiceBtn.elementExists(), "Verify Schedule Service Button is displayed");
            assertTrue(guiController.mapScreen.sendToCarBtn.elementExists(), "Verify send to car button is displayed");
            assertTrue(guiController.mapScreen.detailArrow.elementExists(), "Verify the down arrow is displayed");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.2 Dealer Locator")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify non-preferred dealer under dealer locator screen by entering Zip Cod")
        void nonPreferredDealerUnderScreenByZipcode() {
            String zipCode = "20001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.setAsPreferredIcon.elementExists(), "Verify Preferred dealer should appear on Dealer locator screen.");
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            //  guiController.mapScreen.searchButton.tap(5);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.mapScreen.detailArrow.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            //  assertTrue(guiController.mapScreen.dealerNameTxt.elementExists(),"Verify dealer name is displayed");
            // appController.appFunctions.pauseTestExecution(1,3000);
            //  assertTrue(guiController.mapScreen.dealerLocation.elementExists(),"Verify dealer location is displayed");
            appController.appFunctions.pauseTestExecution(2, 5000);
            assertTrue(guiController.mapScreen.scheduleServiceBtn.elementExists(), "Verify Schedule Service Button is displayed");
            assertTrue(guiController.mapScreen.sendToCarBtn.elementExists(), "Verify send to car button is displayed");
            appController.appFunctions.pauseTestExecution(1, 2000);
            assertTrue(guiController.mapScreen.detailArrow.elementExists(), "Verify the down arrow is displayed");
            appController.appFunctions.pauseTestExecution(2, 2000);
            assertFalse(guiController.mapScreen.setAsPreferredIcon.getSelectedValue(), "Verify the set as preferred icon is not selected for non preferred dealer.");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.2 Dealer Locator")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify making non-preferred dealer as preferred dealer under dealer locator screen by entering Zip Code")
        void nonPreferredDealerMakingPreferredByZipcode() {
            String zipCode = "20001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.setAsPreferredIcon.getSelectedValue(), "Verify Preferred dealer should appear on Dealer locator screen.");
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(1, 4000);
            // guiController.mapScreen.searchButton.tap(5);
            guiController.mapScreen.detailArrow.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            String dealerName = guiController.mapScreen.dealerNameTxt.getTextValue();
            System.out.println(dealerName);
            assertTrue(guiController.mapScreen.dealerNameTxt.elementExists(), "Verify  New Dealer name is displayed");
            guiController.mapScreen.detailArrow.tap();
            guiController.mapScreen.setAsPreferredIcon.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.setAsPreferredIcon.getSelectedValue(), "Verify non Preferred dealer become Preferred dealer");
            appController.appFunctions.pauseTestExecution(2, 3000);
            appController.appFunctions.tapAndroidBackButton();
            guiController.homeScreen.mapButton.tap();
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrow.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            String dealerNameAfterBack = guiController.mapScreen.dealerNameTxt.getTextValue();
            assertEquals(dealerNameAfterBack, dealerName, "New search dealer should become preferred dealer");
            assertTrue(guiController.mapScreen.setAsPreferredIcon.getSelectedValue(), "New dealer set as preferred icon is selected");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify Schedule Service screen for preferred Dealer")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify Schedule Service screen for preferred Dealer")
        void preferredDealerNameOnScheduleService() {
            String zipCode = "20001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.setAsPreferredIcon.elementExists(), "Verify Preferred dealer should appear on Dealer locator screen.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            String dealerName = guiController.mapScreen.dealerNameTxt.getTextValue();
            System.out.println(dealerName);
        /*    guiController.mapScreen.setAsPreferredIcon.tap();
            appController.appFunctions.pauseTestExecution(2, 4000);*/
            guiController.mapScreen.scheduleServiceBtn.tap();
            appController.appFunctions.pauseTestExecution(2, 8000);
            String txtDealerNameOnSchedulePage = guiController.scheduleServiceScreen.txtDealerName.getTextValue();
            System.out.println(txtDealerNameOnSchedulePage);
            //  assertTrue(txtDealerNameOnSchedulePage.contains(dealerName),"Schedule Service page should open with the detail of same selected dealer present.");
        }
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify  Schedule Service screen for non-preferred Dealer by entering Zip Code")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify  Schedule Service screen for non-preferred Dealer by entering Zip Code")
        void nonPreferredDealerNameOnScheduleService() {
            String zipCode = "20001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.setAsPreferredIcon.elementExists(), "Verify Preferred dealer should appear on Dealer locator screen.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            String dealerName = guiController.mapScreen.dealerNameTxt.getTextValue();
            System.out.println(dealerName);
        /*    guiController.mapScreen.setAsPreferredIcon.tap();
            appController.appFunctions.pauseTestExecution(2, 4000);*/
            guiController.mapScreen.scheduleServiceBtn.tap();
            appController.appFunctions.pauseTestExecution(2, 8000);
            String txtDealerNameOnSchedulePage = guiController.scheduleServiceScreen.txtDealerName.getTextValue();
            System.out.println(txtDealerNameOnSchedulePage);
            //  assertTrue(txtDealerNameOnSchedulePage.contains(dealerName),"Schedule Service page should open with the detail of same selected dealer present.");
        }
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify  Schedule Service screen for new preferred Dealer by entering Zip Code")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify  Schedule Service screen for new preferred Dealer by entering Zip Code")
        void newPreferredDealerNameOnScheduleService() {
            String zipCode = "20001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.setAsPreferredIcon.elementExists(), "Verify Preferred dealer should appear on Dealer locator screen.");
            appController.appFunctions.pauseTestExecution(2, 3000);
            String dealerName = guiController.mapScreen.dealerNameTxt.getTextValue();
            System.out.println(dealerName);
        /*    guiController.mapScreen.setAsPreferredIcon.tap();
            appController.appFunctions.pauseTestExecution(2, 4000);*/
            guiController.mapScreen.scheduleServiceBtn.tap();
            appController.appFunctions.pauseTestExecution(2, 8000);
            String txtDealerNameOnSchedulePage = guiController.scheduleServiceScreen.txtDealerName.getTextValue();
            System.out.println(txtDealerNameOnSchedulePage);
            //  assertTrue(txtDealerNameOnSchedulePage.contains(dealerName),"Schedule Service page should open with the detail of same selected dealer present.");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can search dealer locations by entering zip code")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can search dealer locations by entering zip code")
        void canSearchDealerLocations() {
            String zipCode = "90001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(2, 9000);
            String dealerName = guiController.mapScreen.dealerNameTxt.getTextValue();
            System.out.println(dealerName);


        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can send dealer location to Genesis")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can send dealer location to Genesis")
        void dealerLocationToGenesis() {
            String zipCode = "90001";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"), "Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(2, 9000);
            String dealerName = guiController.mapScreen.dealerNameTxt.getTextValue();
            System.out.println(dealerName);
            guiController.mapScreen.detailArrow.tap();
            assertTrue(guiController.mapScreen.sendToCarBtn.elementExists(), "user can send dealer location to Genesis");
            guiController.mapScreen.sendToCarBtn.tap();

        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can add POI")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can add POI")
        void userCanAddPOI() {
            String zipCode = "90001";
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.AddPoi.elementExists(), "user can add POI");
            guiController.mapScreen.AddPoi.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can send POI to Genesis")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can send POI to Genesis")
        void poiToGenesis() {
            String zipCode = "90001";
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.SendtoCar.elementExists(), "user can send POI to Genesis");
            guiController.mapScreen.SendtoCar.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can see favorite dealer locations")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can see favorite dealer locations")
        void favDealerLocations() {
            assertTrue(guiController.mapScreen.favoritesTab.elementExists(), " user can see favorite dealer locations");
            guiController.mapScreen.favoritesTab.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can add to favorites")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can add to favorites")
        void userCanAddTofav() {
            String zipCode = "90001";
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.AddPoi.elementExists(), "user can add POI");
            guiController.mapScreen.AddPoi.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.favoritesTab.elementExists(), " user can see favorite dealer locations");
            guiController.mapScreen.favoritesTab.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.OpenFav.tap();
            assertTrue(guiController.mapScreen.FavItem.elementExists(), "user can add to favorites");


        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can delete from favorites")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can delete from favorites")
        void userCanDeleteFromFav() {
            assertTrue(guiController.mapScreen.favoritesTab.elementExists(), " user can see favorite dealer locations");
            guiController.mapScreen.favoritesTab.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.DeletePoi.elementExists(), "user can delete from favorites");
            guiController.mapScreen.DeletePoi.tap();

        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can navigate to maps")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can navigate to maps")
        void userCanNavToMapsFav() {
            assertTrue(guiController.mapScreen.favoritesTab.elementExists(), " user can see favorite dealer locations");
            guiController.mapScreen.favoritesTab.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.NavMap.elementExists(), "user can navigate to maps");
            guiController.mapScreen.NavMap.tap();

        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can send POI to Genesis")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can send POI to Genesis")
        void sendPOItoGenesisInFav() {
            assertTrue(guiController.mapScreen.favoritesTab.elementExists(), " user can see favorite dealer locations");
            guiController.mapScreen.favoritesTab.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.SendtoCar.elementExists(), "user can send POI to Genesis");
            guiController.mapScreen.SendtoCar.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can search My POI")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can search My POI")
        void userCanSearchMyPoiInFav() {
            assertTrue(guiController.mapScreen.favoritesTab.elementExists(), " user can see favorite dealer locations");
            guiController.mapScreen.favoritesTab.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            assertTrue(guiController.mapScreen.MyPoiSearch.elementExists(), "user can search My POI");

        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can search gas stations")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can search gas stations")
        void userCanSearchGasStations() {
            String zipCode = "90001";
            guiController.mapScreen.nearbyGasStationButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.pauseTestExecution(2, 4000);
            guiController.mapScreen.clickSearchKeyPadBtn();
            assertTrue(guiController.mapScreen.gasstationname.elementExists(), "user can search gas stations");


        }
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.3 Dealer Locator")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify User Can Make Calls")
        void userCanMakeCalls() {
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(1,4000);
           // System.out.println(guiController.mapScreen.headerDealerLocator.getTextValue() + "Varun Bhardwaj");
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"),"Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(1,4000);
            guiController.mapScreen.detailArrow.tap();

            assertTrue(guiController.mapScreen.Call.elementExists(),"Verify the call Icon is displayed");
            guiController.mapScreen.Call.tap();
            assertTrue(guiController.mapScreen.Callpopup.elementExists(),"Verify the popup is open for call");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("1.3 Dealer Locator")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify User Can Make Calls")
        void mapNavigation() {
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2,4000);
            // System.out.println(guiController.mapScreen.headerDealerLocator.getTextValue() + "Varun Bhardwaj");
            assertTrue(guiController.mapScreen.headerDealerLocator.getTextValue().equals("RETAILER LOCATOR"),"Verify dealer locator screen is displayed.");
            appController.appFunctions.pauseTestExecution(1,4000);
            assertTrue(guiController.mapScreen.nevigateLoacationBtn.elementExists(),"Verify Navigation Button is displayed");
            guiController.mapScreen.nevigateLoacationBtn.tap();
            assertTrue(guiController.mapScreen.destinationCoornidates.elementExists(),"Verify Destination Coordinates is displayed");
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can send POI to Genesis")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can send POI to Genesis")
        void PoiToGenesisGasStations() {
            guiController.mapScreen.nearbyGasStationButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.SendtoCar.elementExists(), "user can send POI to Genesis");
            guiController.mapScreen.SendtoCar.tap();


        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can navigate to maps")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can navigate to maps")
        void NavMapsGasStations() {
            guiController.mapScreen.nearbyGasStationButton.tap();
            appController.appFunctions.pauseTestExecution(2, 3000);
            guiController.mapScreen.detailArrowPoi.tap();
            assertTrue(guiController.mapScreen.NavMap.elementExists(), "user can navigate to maps");
            guiController.mapScreen.NavMap.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("Verify user can see enter PIN input screen when navigate to Car Finder")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test Dealer Locator Gen2")
        @Description("Genesis - Verify user can see enter PIN input screen when navigate to Car Finder")
        void enterPinInputScreenCarFinder() {
            guiController.mapScreen.carFinderButton.tap();
            guiController.mapScreen.carFinderButton.tap();
            guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
            appController.appFunctions.pauseTestExecution(2, 9000);
            assertTrue(guiController.mapScreen.titleTextView.getTextValue(20).equals("CAR FINDER"));

    }
}
}

