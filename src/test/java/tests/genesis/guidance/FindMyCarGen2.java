package tests.genesis.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("Find My Car Gen2 Test")
@Feature("Find My Car Gen2")
@DisplayName("Find My Car Gen2")  
public class FindMyCarGen2 extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests() {
        profile = appController.genesisProfiles.gen2GasStandard;
        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.6.1 Entering correct PIN opens Car Finder screen")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Test Find My Car Gen2")
    @Description("Genesis - Verify Entering correct PIN opens Car Finder screen")
    void  enteringCorrectPinOpensCarFinderScreen() {
        guiController.homeScreen.tapMapBtn();
        guiController.mapScreen.carFinderButton.tap();
        guiController.enterPinScreen.enterPin(profile.primaryAccount.pin);
        if (guiController.outOfRangePopup.isPresent(60)) {
            guiController.outOfRangePopup.cancelButton.tap();
        }
        assertTrue(guiController.mapScreen.titleTextView.getTextValue(20).equals("CAR FINDER"));
        appController.appFunctions.pauseTestExecution(5, 10000);
    }

   @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("6.6.2 Entering an incorrect PIN 3 times locks PIN")
   @Severity(SeverityLevel.CRITICAL)
   @Story("Test Find My Car Gen2")
   @Description("Genesis - Verify Entering an incorrect PIN 3 times locks PIN")
    void enteringIncorrectPinThreeTimesLocksPin() {
        String incorrectPin = guiController.enterPinScreen.getIncorrectPin(profile.primaryAccount);

        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.carFinderButton.tap();
        guiController.enterPinScreen.enterPin(incorrectPin);
        guiController.incorrectPinCarFinderPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.carFinderButton.tap();
        guiController.enterPinScreen.enterPin(incorrectPin);
        guiController.incorrectPinCarFinderPopup.okButton.tap();

        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.homeScreen.mapButton.tap();
        guiController.mapScreen.carFinderButton.tap();
        guiController.enterPinScreen.enterPin(incorrectPin);
        assertTrue(guiController.pinLockedPopup.isPresent());

        // reset PIN
        // enter password
        guiController.pinLockedPopup.changePinButton.tap();
        guiController.changePinEnterPasswordScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        guiController.changePinEnterPasswordScreen.enterPasswordEditText.enterText(profile.primaryAccount.password);
        guiController.changePinEnterPasswordScreen.submitButton.tap();
        // enter security answer
        guiController.changePinEnterSecurityAnswerScreen.isPresent();
        appController.appFunctions.tapAndroidBackButton();
        guiController.changePinEnterSecurityAnswerScreen.enterSecurityAnswerEditText
                .enterText(profile.primaryAccount.securityAnswer);
        guiController.changePinEnterSecurityAnswerScreen.submitButton.tap();
        // enter and confirm new pin
        guiController.changePinEnterNewPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.changePinEnterNewPinScreen.enterPin(profile.primaryAccount.pin);
        guiController.changePinEnterNewPinScreen.saveButton.tap();
        guiController.changePinSuccessfulPopup.okButton.tap();
    }
}
