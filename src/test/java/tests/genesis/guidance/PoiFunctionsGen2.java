package tests.genesis.guidance;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard 
// 2. WithSecondaryDriver

@Epic("POI Functions Gen2 Test")
@Feature("POI Functions Gen2")
@DisplayName("POI Functions Gen2") 
public class PoiFunctionsGen2 extends TestController {
    @Nested
    @DisplayName("Signed in as Gen 2 gas standard")
    class Gen2GasStandard {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.genesisProfiles.gen2GasStandard;
            guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin,60);
            guiController.homeScreen.tapMapBtn();
           // guiController.homeScreen.mapButton.tap();
        } 

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.1 POI Search box exists")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify POI Search box exists")
        void poiSearchBoxIsVisible() {
            guiController.mapScreen.poiSearchButton.tap();
            assertTrue(guiController.mapScreen.searchEditText.getTextValue().equals("POI Search"));
        }

       /* // TODO: Can't access drop down list, likely to remove
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.2 POI Search results are viewable in an expanded list")
        void poiSearchResultsExpandedListShowsAnEntry() {
            guiController.mapScreen.poiSearchButton.tap();
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText("mcdonalds");
            //appController.appFunctions.pauseTestExecution(1,1000);
            guiController.mapScreen.searchButton.tap();
            assertTrue(guiController.mapScreen.searchList.elementExists(10));
        }*/

        @RepeatedTest(TEST_REPETITIONS)
            @DisplayName("6.1.3 My POIs are viewable in an expanded list")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify My POIs are viewable in an expanded list")
        void myPoisAreDisplayedInListView() {
            guiController.mapScreen.myPoiButton.tap();
            guiController.mapScreen.favoritesTab.tap();
            if(!guiController.mapScreen.searchList.elementExists()){
                guiController.mapScreen.searchButton.tap();
            }
            assertTrue(guiController.mapScreen.searchList.elementExists(10));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.4 Dealer Locator zip code is searchable")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Dealer Locator zip code is searchable")
        void dealerLocatorZipCodeIsSearchable() {
            String zipCode = "92708";
             guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(3,10000);
            guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            //appController.appFunctions.tapAndroidKeyboardSearchButton();
            appController.appFunctions.pauseTestExecution(1,4000);
            guiController.mapScreen.searchButton.tap(5);
            assertEquals(zipCode, guiController.mapScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.5 Dealers are viewable in an expanded list")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Dealers are viewable in an expanded list")
        void DealersAreDisplayedInListView() {
            String zipCode = "92708";

            guiController.mapScreen.dealerLocatorButton.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            guiController.mapScreen.searchEditText.enterText(zipCode);
            appController.appFunctions.tapAndroidBackButton();
            guiController.mapScreen.searchButton.tap();
            assertTrue(guiController.mapScreen.searchList.elementExists(10));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.6 Gas Station is searchable")
        @Severity(SeverityLevel.NORMAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Gas Station is searchable")
        void gasStationIsSearchable() {
            String zipCode = "Gas Station";

            guiController.mapScreen.nearbyGasStationButton.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            guiController.mapScreen.searchButton.tap();
            assertEquals(zipCode, guiController.mapScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.7 Gas Stations are viewable in an expanded list")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Gas Stations are viewable in an expanded list")
        void gasStationsAreDisplayedInListView() {
            String zipCode = "92708";

            guiController.mapScreen.nearbyGasStationButton.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            guiController.mapScreen.searchButton.tap();
            assertTrue(guiController.mapScreen.searchList.elementExists(1));
        }
    }

  @Nested
    @DisplayName("Signed in as Secondary Driver")
    class SecondaryDriver {
        private Profile profile;

        @BeforeEach
        void setupTests() {
            profile = appController.genesisProfiles.withSecondaryDriver;
            guiController.loginAndGoToHomePageWithSelectVehicle(profile.secondaryAccount, profile.primaryVin);
          //  guiController.homeScreen.tapMapBtn();
            guiController.homeScreen.mapButton.tap();
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.8 POIs are searchable when signed in as secondary driver")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify POIs are searchable when signed in as secondary driver")
        void secondaryDriverPoisAreSearchable() {
           // guiController.homeScreen.tapMapBtn();
            guiController.mapScreen.poiSearchButton.tap();
            assertTrue(guiController.mapScreen.searchEditText.getTextValue().equals("POI Search"));
        }

       /* // TODO: Can't access drop down list, likely to remove
        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.9 POIs are viewable in an expanded list when signed in as secondary driver")
        void secondaryDriverSearchedPoisAreDisplayedInListView() {
            guiController.mapScreen.poiSearchButton.tap();
            guiController.mapScreen.searchEditText.enterText("55555");
            guiController.mapScreen.searchButton.tap();
            assertTrue(guiController.mapScreen.searchList.elementExists());
        }*/

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.10 Dealer Locator zip code is searchable when signed in as secondary driver")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Dealer Locator zip code is searchable when signed in as secondary driver")
        void secondaryDriverDealerLocatorZipCodeIsSearchable() {
            String zipCode = "92708";
            guiController.mapScreen.dealerLocatorButton.tap();
            appController.appFunctions.pauseTestExecution(2,5000);
           // guiController.mapScreen.searchEditText.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            //appController.appFunctions.tapAndroidKeyboardSearchButton();
            appController.appFunctions.pauseTestExecution(1,4000);
            guiController.mapScreen.searchButton.tap(5);
            assertEquals(zipCode, guiController.mapScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.11 Dealers are viewable in an expanded list when signed in as secondary driver")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Dealers are viewable in an expanded list when signed in as secondary driver")
        void secondaryDriverDealersAreDisplayedInListView() {
            String zipCode = "92708";

            guiController.mapScreen.dealerLocatorButton.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            guiController.mapScreen.searchButton.tap();
            assertTrue(guiController.mapScreen.searchList.elementExists(10));
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.12 Nearby Gas is searchable when signed in as secondary driver")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Nearby Gas is searchable when signed in as secondary driver")
        void secondaryDriverNearbyGasIsSearchable() {

            String zipCode = "92708";
            guiController.mapScreen.nearbyGasStationButton.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            guiController.mapScreen.searchButton.tap();
            assertEquals(zipCode, guiController.mapScreen.searchEditText.getTextValue());
        }

        @RepeatedTest(TEST_REPETITIONS)
        @DisplayName("6.1.13 Nearby Gas is viewable in an expanded list when signed in as secondary driver")
        @Severity(SeverityLevel.CRITICAL)
        @Story("Test POI Functions Gen2")
        @Description("Genesis - Verify Nearby Gas is viewable in an expanded list when signed in as secondary driver")
        void secondaryDriverNearbyGasIsDisplayedInListView() {
            String zipCode = "92708";

            guiController.mapScreen.nearbyGasStationButton.tap();
            guiController.mapScreen.searchEditText.enterText(zipCode);
            guiController.mapScreen.searchButton.tap();
            assertTrue(guiController.mapScreen.searchList.elementExists(10));
        }
    }
}
