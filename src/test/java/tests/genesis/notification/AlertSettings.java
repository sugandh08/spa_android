package tests.genesis.notification;
 
import config.Profile;
import io.qameta.allure.*;
import library.Element;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("Alert Settings Test")
@Feature("Alert Settings")
@DisplayName("Alert Settings")
public class AlertSettings extends TestController {
    private Profile profile;

    @BeforeEach 
    void setupTests() {

        profile = appController.genesisProfiles.mobilePrimaryPhone;

        profile = appController.genesisProfiles.gen2GasStandard;


        guiController.loginAndGoToHomePageWithSelectVehicle(profile.primaryAccount, profile.primaryVin, 60);

        guiController.homeScreen.menuButton.tap();
        guiController.menuScreen.alertSettingsButton.tap();
        // Used to wait for Alert Settings screen to load
        guiController.alertSettingsScreen.isPresent(60);
        appController.appFunctions.pauseTestExecution(1, 5000);
    }

    /**
     * Used to move away and return to Alert Settings screen.
     */
    void moveAwayAndReturnToAlertSettingsScreen() {
        appController.appFunctions.tapAndroidBackButton();
        guiController.homeScreen.menuButton.tap();
        guiController.menuScreen.alertSettingsButton.tap();
        // Used to wait for Alert Settings screen to load
        guiController.alertSettingsScreen.isPresent();
        appController.appFunctions.pauseTestExecution(1, 10000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.1 Toggling Speed Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Toggling Speed Alert setting does not change the setting value")
    void togglingSpeedAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.speedAlertToggle;

        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        appController.appFunctions.pauseTestExecution(1, 5000);
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        moveAwayAndReturnToAlertSettingsScreen();
        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(toggleValue == toggle.getCheckedValue() && toggle.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.2 Toggling Valet Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Toggling Valet Alert setting does not change the setting value")
    void togglingValetAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.valetAlertToggle;
        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        appController.appFunctions.pauseTestExecution(1, 5000);

        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        moveAwayAndReturnToAlertSettingsScreen();
        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(toggle.elementExists() && toggleValue == toggle.getCheckedValue());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.3 Toggling Curfew Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Toggling Curfew Alert setting does not change the setting value")
    void togglingCurfewAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.curfewAlertToggle;

        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        appController.appFunctions.pauseTestExecution(1, 5000);
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        moveAwayAndReturnToAlertSettingsScreen();
        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        appController.appFunctions.pauseTestExecution(1, 5000);
        assertTrue(toggleValue == toggle.getCheckedValue() && toggle.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.4 Toggling Geo-Fence Alert setting does not change the setting value")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Toggling Geo-Fence Alert setting does not change the setting value")
    void togglingGeoFenceAlertChangesValue() {
        Element toggle = guiController.alertSettingsScreen.geoFenceAlertToggle;

        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        boolean toggleValue = toggle.getCheckedValue();
        toggle.tap();
        moveAwayAndReturnToAlertSettingsScreen();
        guiController.alertSettingsScreen.scrollToElementSection(toggle);
        assertTrue(toggleValue == toggle.getCheckedValue() && toggle.elementExists());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.5 Selecting to set up a Speed Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Selecting to set up a Speed Alert opens correct settings page")
    void selectingToOpenSpeedAlertOpensCorrectSettingsPage() {
        Element elementToScrollTo = guiController.alertSettingsScreen.speedAlertButton;

        guiController.alertSettingsScreen.scrollToElementSection(elementToScrollTo);
        elementToScrollTo.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(guiController.speedAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.6 Selecting to set up a Valet Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Selecting to set up a Valet Alert opens correct settings page")
    void selectingToOpenValetAlertOpensCorrectSettingsPage() {
        Element elementToScrollTo = guiController.alertSettingsScreen.valetAlertButton;

        guiController.alertSettingsScreen.scrollToElementSection(elementToScrollTo);
        elementToScrollTo.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.7 Selecting to set up a Curfew Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Selecting to set up a Curfew Alert opens correct settings page")
    void selectingToOpenCurfewAlertOpensCorrectSettingsPage() {
        Element elementToScrollTo = guiController.alertSettingsScreen.scrollToElementSection(
                guiController.alertSettingsScreen.curfewAlertButton,
                guiController.alertSettingsScreen.addNewCurfewAlertButton);

        elementToScrollTo.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(guiController.curfewAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.8 Selecting to set up a Geo-Fence Alert opens correct settings page")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Verify Selecting to set up a Geo-Fence Alert opens correct settings page")
    void selectingToOpenGeoFenceAlertOpensCorrectSettingsPage() {
        Element elementToScrollTo = guiController.alertSettingsScreen.scrollToElementSection(
                guiController.alertSettingsScreen.geoFenceAlertButton,
                guiController.alertSettingsScreen.addNewGeoFenceAlertButton);

        elementToScrollTo.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        assertTrue(guiController.geoFenceAlertScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.9 Creating Speed Alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Creating Speed Alert Setting Value")
    void addNewSpeedAlert() {
        if (!guiController.alertSettingsScreen.speedAlertToggle.getCheckedValue()) {
            guiController.alertSettingsScreen.speedAlertToggle.tap();
        }
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
        guiController.alertSavedPopup.okButton.tap();
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.10 Creating Valet Alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Alert Settings")
    @Description("Genesis - Creating Valet Alert Setting Value")
    void addNewValetAlert() {
        if (!guiController.alertSettingsScreen.valetAlertToggle.getCheckedValue()) {
            guiController.alertSettingsScreen.valetAlertToggle.tap();
        }
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.valetAlertScreen.distanceLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.valetAlertScreen.valetLimitWheelXpath, 2);
        guiController.valetAlertScreen.doneButton.tap();
        String valetLimit = guiController.valetAlertScreen.valetLimitTextView.getTextValue();
        guiController.valetAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.alertSettingsScreen.valetAlertValue.getTextValue().contains(valetLimit));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.11 Verify Curfew alert is added successfully.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Curfew Alert")
    @Description("Genesis - Verify Curfew alert is added successfully.")
    void addNewCurfewAlert() {
        //guiController.alertSettingsScreen.swipe.up(2);
       // Element elementToScrollTo = guiController.alertSettingsScreen.addNewCurfewAlertButton;
        //guiController.alertSettingsScreen.scrollToElementSection(elementToScrollTo);
        if (guiController.alertSettingsScreen.addNewCurfewAlertButton.elementExists()) {
            guiController.alertSettingsScreen.addNewCurfewAlertButton.tap();
        } else {
            guiController.alertSettingsScreen.curfewAlertButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.curfewAlertScreen.curfewFromButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 1);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 10);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 11);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        String curfewFromTextValue = guiController.curfewAlertScreen.curfewFromTextView.getTextValue();
        System.out.println(curfewFromTextValue);
        String curfewToTextValue = guiController.curfewAlertScreen.curfewToTextView.getTextValue();
        System.out.println(curfewToTextValue);
        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Curfew ") && resultText.contains("successfully"), "Curfew Alert is saved successfully");
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + curfewFromTextValue + "']").equals(curfewFromTextValue));
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + curfewToTextValue + "']").equals(curfewToTextValue));
        guiController.alertSettingsScreen.saveButton.tap();
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
//        guiController.alertModificationPopup.okButton.tap();

        //System.out.println("Curfew ALERT RESULT: " + resultText);

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.12 Verify Geo-Fence alert is added successfully.")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Geo-Fence Alert")
    @Description("Genesis - Geo-Fence alert is added successfully.")
    void addNewGeoFenceAlert() {
        Element elementToScrollTo = guiController.alertSettingsScreen.geoFenceAlertButton;

        guiController.alertSettingsScreen.scrollToElementSection(elementToScrollTo);
        guiController.alertSettingsScreen.swipe.up(3);
        assertFalse(guiController.alertSettingsScreen.getGeoFenceAlertLimit(5), "Geo-Fence alert has reached maximum limit");
        if (guiController.alertSettingsScreen.addNewGeoFenceAlertButton.elementExists()) {
            guiController.alertSettingsScreen.addNewGeoFenceAlertButton.tap();
        } else {
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
        }
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.geoFenceAlertScreen.isPresent());
        guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
        appController.appFunctions.pauseTestExecution(1, 2000);
        guiController.geoFenceAlertScreen.searchLocationButton.tap();
        guiController.geoFenceAlertScreen.searchLocationButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        String geoFenceName = "New Geo Fence" + Math.random();
        guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
        guiController.geoFenceAlertScreen.milesEditText.enterText("50");
        guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
        guiController.geoFenceAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Geo-Fence ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Geo-Fence ") && resultText.contains("successfully"), "Geo-Fence Alert is not saved successfully");

        // guiController.alertSettingsScreen.saveButton.tap();
        // guiController.alertModificationPopup.okButton.tap();
         }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.13 Delete Geo-Fence alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Geo-Fence Alert")
    @Description("Genesis - Delete Geo-Fence alert ")
    void deleteGeoFenceAlert() {
        guiController.alertSettingsScreen.swipe.up(1);

        String geoFenceName = "New Geo Fence" + Math.random();
        //first check if already geo fence alert is added
         if (guiController.alertSettingsScreen.getGeoFenceAlertLimit(1)) {
            guiController.alertSettingsScreen.clickOnGeoFenceAlert(1);
        } else {
            // if there is no geo fence, then add one
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.geoFenceAlertScreen.isPresent());
            guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
             geoFenceName = "New Geo Fence" + Math.random();
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            System.out.println("Geo-Fence ALERT RESULT: " + resultText);
            assertTrue(resultText.contains("Your Geo-Fence ") && resultText.contains("successfully"), "Geo-Fence Alert is not saved successfully");
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.geoFenceAlertScreen.isPresent());
            guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));

        }

        //delete geo fence alert
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.geoFenceAlertScreen.deleteButton.tap();
        appController.appFunctions.pauseTestExecution(1, 6000);
        guiController.deleteConfirmationPopup.deleteButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        // Assertions.assertNotEquals(guiController.getTextUsingXpath("//*[@text='"+geoFenceName+"']"),geoFenceName,"geo fence deleted successfully");
        guiController.alertSettingsScreen.saveButton.tap();
        // guiController.alertModificationPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(2000);
        System.out.println("Geo-Fence ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Geo-Fence ") && resultText.contains("successfully"), "Geo-Fence Alert is not deleted successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.14 Delete Curfew alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Curfew Alert")
    @Description("Genesis - Delete Curfew alert ")
    void deleteCurfewAlert() {
        //check if any curfew alert already added
        if (guiController.alertSettingsScreen.ExistingCurfewAlert.elementExists()) {
            System.out.println("Clicking on existed curfew alert ");
        } else {
            //add a curfew alert
            guiController.alertSettingsScreen.curfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            guiController.curfewAlertScreen.curfewFromButton.tap();
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
        }

        //String curfewFromTextValue = guiController.getTextUsingXpath(guiController.alertSettingsScreen.xpathOfFromCurfewAlert);
        guiController.alertSettingsScreen.ExistingCurfewAlert.tap();
        guiController.curfewAlertScreen.deleteButton.tap();
        guiController.deleteConfirmationPopup.clickOnDeleteButton();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        //Assertions.assertNotEquals(guiController.getTextUsingXpath(guiController.alertSettingsScreen.xpathOfFromCurfewAlert), curfewFromTextValue);
        guiController.alertSettingsScreen.saveButton.tap();
         guiController.alertSavedPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(2000);
        System.out.println("Curfew ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Curfew") && resultText.contains("successfully"), "Curfew Alert is deleted successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.15 Update Geo-Fence alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Geo-Fence Alert")
    @Description("Genesis - Update Geo-Fence alert ")
    void updateGeoFenceAlert() {
        Element elementToScrollTo = guiController.alertSettingsScreen.geoFenceAlertButton;

        guiController.alertSettingsScreen.scrollToElementSection(elementToScrollTo);
        String geoFenceName = "New Geo Fence" + Math.random();
        //first check if already geo fence alert is added
        guiController.alertSettingsScreen.swipe.up(2);
        if (guiController.alertSettingsScreen.getGeoFenceAlertLimit(0)) {
            guiController.alertSettingsScreen.clickOnGeoFenceAlert(0);
        } else {
            // if there is no geo fence, then add one
            guiController.alertSettingsScreen.geoFenceAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.geoFenceAlertScreen.isPresent());
            guiController.geoFenceAlertScreen.searchLocationEditText.enterText("Fountain");
            appController.appFunctions.pauseTestExecution(1, 2000);
            guiController.geoFenceAlertScreen.searchLocationButton.tap();
            guiController.geoFenceAlertScreen.searchLocationButton.tap();

            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
            guiController.geoFenceAlertScreen.milesEditText.enterText("50");
            guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
            guiController.geoFenceAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            System.out.println("Geo-Fence ALERT RESULT: " + resultText);
            assertTrue(resultText.contains("Your Geo-Fence ") && resultText.contains("successfully"), "Geo-Fence Alert is not saved successfully");
        }
        //update geo fence alert
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.geoFenceAlertScreen.searchLocationEditText.enterText("New York");
        guiController.geoFenceAlertScreen.searchLocationButton.tap();
        guiController.geoFenceAlertScreen.searchLocationButton.tap();
        appController.appFunctions.pauseTestExecution(1, 6000);
        guiController.geoFenceAlertScreen.geoFenceNameEditText.enterText(geoFenceName);
        guiController.geoFenceAlertScreen.milesEditText.enterText("30");
        guiController.geoFenceAlertScreen.exclusiveRadioButton.tap();
        guiController.geoFenceAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 4000);
        assertTrue(guiController.getTextUsingXpath("//*[@text='" + geoFenceName + "']").contains(geoFenceName));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Geo-Fence ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Geo-Fence ") && resultText.contains("successfully"), "Geo-Fence Alert is not saved successfully");

    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.16 Update Curfew alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Curfew Alert")
    @Description("Genesis - Update Curfew alert ")
    void updateCurfewAlert() {
        //check if any curfew alert already added
        if (guiController.alertSettingsScreen.ExistingCurfewAlert.elementExists()) {
            System.out.println("Clicking on existed curfew alert ");
        } else {
            //add a curfew alert
            guiController.alertSettingsScreen.curfewAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 15000);
            guiController.curfewAlertScreen.curfewFromButton.tap();
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 1);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();

            guiController.curfewAlertScreen.curfewToButton.tap();
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
            guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
            guiController.curfewAlertScreen.doneButton.tap();
            guiController.curfewAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 10000);
            String resultText1 = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            assertTrue(resultText1.contains("Your Curfew ") && resultText1.contains("successfully"), "Curfew Alert is saved successfully");
            guiController.alertSavedPopup.okButton.tap();

            }

        //Update Curfew Alert

        guiController.alertSettingsScreen.ExistingCurfewAlert.tap();
        appController.appFunctions.pauseTestExecution(1, 1000);
        guiController.curfewAlertScreen.curfewFromButton.tap();
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelUp(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();

        guiController.curfewAlertScreen.curfewToButton.tap();
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.dayWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.hourWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.minuteWheelXpath, 2);
        guiController.swipeScrollWheelDown(guiController.curfewAlertScreen.meridianWheelXpath, 1);
        guiController.curfewAlertScreen.doneButton.tap();
        guiController.curfewAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        assertTrue(resultText.contains("Your Curfew ") && resultText.contains("successfully"), "Curfew Alert is saved successfully");
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.17 Update Speed alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Speed Alert")
    @Description("Genesis - Update Speed alert ")
    void updateSpeedAlert() {

        //check if any speed alert already added
        if (guiController.alertSettingsScreen.speedAlertButton.elementExists()) {
            System.out.println("Clicking on existed speed alert ");
        } else {
            //add a speed alert
            guiController.alertSettingsScreen.speedAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.speedAlertScreen.isPresent());
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.speedAlertScreen.speedLimitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.swipeScrollWheelUp(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
            guiController.speedAlertScreen.doneButton.tap();
            String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
            guiController.speedAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 4000);
            assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
}

        //Update Speed Alert
        guiController.alertSettingsScreen.speedAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.speedAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.speedAlertScreen.speedLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelDown(guiController.speedAlertScreen.speedLimitWheelXpath, 2);
        guiController.speedAlertScreen.doneButton.tap();
        String speedLimit = guiController.speedAlertScreen.speedLimitTextView.getTextValue();
        guiController.speedAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        guiController.alertSavedPopup.okButton.tap();
        appController.appFunctions.pauseTestExecution(1, 15000);
        assertTrue(guiController.alertSettingsScreen.speedAlertValue.getTextValue().contains(speedLimit));
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("SPEED ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Speed ") && resultText.contains("successfully"), "Speed Alert is not saved successfully");
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("2.4.18 Update Valet alert")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Valet Alert")
    @Description("Genesis - Update Valet alert ")
    void updateValetAlert() {
        //check if any speed alert already added
        if (guiController.alertSettingsScreen.valetAlertValue.elementExists()) {
            System.out.println("Clicking on existed valet alert ");
        } else {
            //add a Valet alert
            guiController.alertSettingsScreen.valetAlertButton.tap();
            appController.appFunctions.pauseTestExecution(1, 3000);
            assertTrue(guiController.valetAlertScreen.isPresent());
            appController.appFunctions.pauseTestExecution(1, 3000);
            guiController.valetAlertScreen.distanceLimitButton.tap();
            appController.appFunctions.pauseTestExecution(1, 5000);
            guiController.swipeScrollWheelUp(guiController.valetAlertScreen.valetLimitWheelXpath, 2);
            guiController.valetAlertScreen.doneButton.tap();
            String valetLimit = guiController.valetAlertScreen.valetLimitTextView.getTextValue();
            guiController.valetAlertScreen.saveButton.tap();
            appController.appFunctions.pauseTestExecution(1, 1000);
            guiController.alertSavedPopup.okButton.tap();
            appController.appFunctions.pauseTestExecution(1, 1000);
            assertTrue(guiController.alertSettingsScreen.valetAlertValue.getTextValue().contains(valetLimit));
            guiController.alertSettingsScreen.saveButton.tap();
            String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
            System.out.println("Valet ALERT RESULT: " + resultText);
            assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");
        }
        //Update Valet Alert
        guiController.alertSettingsScreen.valetAlertButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        assertTrue(guiController.valetAlertScreen.isPresent());
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.valetAlertScreen.distanceLimitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 5000);
        guiController.swipeScrollWheelUp(guiController.valetAlertScreen.valetLimitWheelXpath, 2);
        guiController.valetAlertScreen.doneButton.tap();
        guiController.valetAlertScreen.saveButton.tap();
        appController.appFunctions.pauseTestExecution(1, 3000);
        guiController.alertSavedPopup.okButton.tap();
        String resultText = guiController.remoteCommandResultPopup.messageTextView.getTextValue(200);
        System.out.println("Valet ALERT RESULT: " + resultText);
        assertTrue(resultText.contains("Your Valet ") && resultText.contains("successfully"), "Valet Alert is not saved successfully");

    }

}
