package tests.genesis.registration;

import config.Profile;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import tests.genesis.TestController;

import static org.junit.jupiter.api.Assertions.assertTrue;

// Profiles Used:
// 1. Gen2GasStandard

@Epic("Registration Test")
@Feature("Registration Settings")
@DisplayName("Registration Settings")
public class Registration extends TestController {
    private Profile profile;

    @BeforeEach
    void setupTests(){
        profile = appController.genesisProfiles.gen2GasStandard;
        if(guiController.introductionScreen.isPresent()) {
            guiController.introductionScreen.loginButton.tap();
        }
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.1 Tap Registration Button")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Registration")
    @Description("Genesis - Verify Tapping Registration Button")
    void tapRegistrationButton() {
        guiController.loginScreen.registerButton.tap();
        assertTrue(guiController.registrationPleaseEnterYourEmailAddressPopup.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.2 Tap Cancel On 'Enter Your Email Address' Popup")
    @Severity(SeverityLevel.MINOR)
    @Story("Test Registration")
    @Description("Genesis - Verify Tapping Cancel On 'Enter Your Email Address' Popup")
    void tapCancelOnEnterYourEmailAddressPopup() {
        guiController.loginScreen.registerButton.tap();
        guiController.registrationPleaseEnterYourEmailAddressPopup.cancelButton.tap();
        assertTrue(guiController.loginScreen.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.3 User Submits Already Registered Email")
    @Severity(SeverityLevel.NORMAL)
    @Story("Test Registration")
    @Description("Genesis - c")
    void userSubmitsAlreadyRegisteredEmail() {
        guiController.loginScreen.registerButton.tap();
        guiController.registrationPleaseEnterYourEmailAddressPopup.editText.enterText(profile.primaryAccount.email);
        guiController.registrationPleaseEnterYourEmailAddressPopup.submitButton.tap();
        appController.appFunctions.pauseTestExecution(1, 10000);
        assertTrue(guiController.registrationEmailAlreadyRegisteredPopup.isPresent());
    }

   /* @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.4 Tap 'Forgot Password' Button On 'Email Already Registered' Popup")
    void tapForgotPasswordButtonOnEmailAlreadyRegisteredPopup() {
        guiController.loginScreen.registerButton.tap();
        guiController.registrationPleaseEnterYourEmailAddressPopup.editText.enterText(profile.primaryAccount.email);
        guiController.registrationPleaseEnterYourEmailAddressPopup.submitButton.tap();
       guiController.registrationEmailAlreadyRegisteredPopup.forgotPasswordButton.tap();
        assertTrue(guiController.registrationResetPasswordPopup.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.5 Tap 'Cancel' Button On 'Email Already Registered' Popup")
    void tapCancelButtonOnEmailAlreadyRegisteredPopup() {
        guiController.loginScreen.registerButton.tap();
        guiController.registrationPleaseEnterYourEmailAddressPopup.editText.enterText(profile.primaryAccount.email);
        guiController.registrationPleaseEnterYourEmailAddressPopup.submitButton.tap();
        guiController.registrationEmailAlreadyRegisteredPopup.cancelButton.tap();
        assertTrue(guiController.registrationPleaseEnterYourEmailAddressPopup.isPresent());
    }

    @RepeatedTest(TEST_REPETITIONS)
    @DisplayName("8.1.6 Tap 'Login' Button On 'Email Already Registered' Popup")
    void tapLoginButtonOnEmailAlreadyRegisteredPopup() {
        guiController.loginScreen.registerButton.tap();
        guiController.registrationPleaseEnterYourEmailAddressPopup.editText.enterText(profile.primaryAccount.email);
        guiController.registrationPleaseEnterYourEmailAddressPopup.submitButton.tap();
        guiController.registrationEmailAlreadyRegisteredPopup.loginButton.tap();
        assertTrue(guiController.loginScreen.isPresent());
    }*/
}
