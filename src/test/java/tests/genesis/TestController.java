package tests.genesis;

import io.appium.java_client.MobileElement;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tests.TestSetup;
import views.AppController;
import views.genesis.GuiController;

import java.net.MalformedURLException;
import java.time.ZonedDateTime;

/**
 * Contains information for Appium to setup Genesis.
 * Handles the setup and teardown for the entire test set,
 * as well as any global actions before or after each test.
 *
 * Test-specific before/after actions are not handled here.
 */
public class TestController {
    private static TestSetup testSetup;

    public static AppController appController;
    public static GuiController guiController;

    // Number of test repetitions. Must be greater than 0. Standard value is 1.
    public static final int TEST_REPETITIONS = 1;
    private static String testVersion;

    /**
     * Sends the required Appium parameters to create an instance of TestSetup.
     * Retrieves the appium driver from TestSetup instance to create the App and GUI controllers.
     */
    @BeforeAll
    public static void setup() {
        String appActivity = ".GESplashActivity";
        String appPackage = "com.stationdm.genesis";
        String appWaitActivity = ".GEViewPagerActivity";
        String appWaitPackage = "com.stationdm.genesis";

        try {
            testSetup = new TestSetup();
            testSetup.Setup(appActivity, appPackage, appWaitActivity, appWaitPackage);
        } catch(MalformedURLException m) {
            //
        }

        appController = new AppController(testSetup.getDriver());
        guiController = new GuiController(testSetup.getDriver(), appController);

        testVersion = appController.getTestVersion();
    }

    /**
     * Prints out information at the beginning of every test.
     * Currently prints out Version and a timestamp
     */
    @BeforeEach
    public void printTestVersionAndDate() {
        System.out.println("\nTest Version: " + testVersion);
        System.out.println(ZonedDateTime.now() + "\n");
    }

    /**
     * Closes the app instance, clears the app data, then starts up the app again.
     */
    @BeforeEach
    public void resetApp() {
        appController.appFunctions.resetApp();
    }

    /**
     * Kills the appium driver and any connections to devices so that everything closes down
     * properly and quickly.
     */
    @AfterAll
    public static void stopDriver() {
        appController.appFunctions.stopDriver();
    }


}